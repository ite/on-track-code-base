<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!$_SESSION["eula_agree"] === true)
        header("Location: logout.php");

    //  Print Header
    print_header();

    //  Print Menu
    if ($_GET["menu"] == "")    print_menus("0", "home");
    else                        print_menus($_GET["menu"], "home");

    //  Print Footer
    print_footer();
?>
