<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (isset($_SESSION["company_id"]))
        unset($_SESSION["company_id"]);

    if (isset($_SESSION["company_name"]))
        unset($_SESSION["company_name"]);

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0");
?>
    <table width="100%">
        <tr>
            <td class="centerdata" valign="top">
                <form action="logout.php" method="post">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <strong>
                                    <font color="#999999">
                                        Sorry, your company account has been locked
                                    </font>
                                </strong>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <br/>
                            </td>
                        </tr>
                    </table>
                    <center><input type="submit" value="Return"/></center>
                </form>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>