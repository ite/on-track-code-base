<?php
    session_start();

    include("_db.php");

    $scriptStart = my_microtime();
?>
<html>
    <head>
        <title>
            On-Track - Recovery Script...
        </title>
    </head>
    <body>
        <?php
			$budget = q("SELECT B.id, B.month, B.year, CONCAT(C.name,' - ',E.name), B.planned, B.amount
				FROM Budget AS B 
				INNER JOIN Elements AS E ON B.element_id = E.id 
				INNER JOIN Category AS C ON E.parent_id = C.id
				WHERE B.year = '2011' AND B.month = '11' AND B.company_id = '1'
			");
			foreach ($budget as $b)      {
				$sum = 0;
				echo "".$b[0]." ".$b[1]." ".$b[2]." ".$b[3]." R".$b[4]." R".$b[5]."\n";
				$statements = q("SELECT s.id,s.date,s.description,s.amount 
					FROM Statement AS s 
					INNER JOIN StatementLink AS sl ON s.id = sl.statement_id 
					WHERE sl.element_id = ".$b[0]."
					ORDER BY date,amount"
				);
				if (is_array($statements))
				foreach ($statements as $s)      {
					$sum += (double)$s[3];
					echo "\t".$s[0]." ".$s[1]." ".$s[2]." R".$s[3]."\n";
				}
				echo "SUM = ".(number_format($sum*-1, 2, ".", ""))."\n\n";
				$new_val = (number_format($sum*-1, 2, ".", ""));
				$u = q("UPDATE Budget SET amount = '$new_val' WHERE id = ".$b[0]."");
			}

        ?>
        <form action="index.php" method="post">
            <center><input type="submit" value="Return"/></center>
        </form>
    </body>
</html>
