<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("fusion/FusionCharts.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");
        
    if (!hasAccess("REP_INCOME_FORECAST"))
        header("Location: noaccess.php");

    //  Set Report Generated Status
    $generated                                                          = "0";

    //  Create Global Arrays
    $total_time                                                         = array();
    $total_invoice                                                      = array();
    $total_average                                                      = array();
    $project_month_time_array                                           = array();
    $project_month_invoice_array                                        = array();
    $project_month_average_array                                        = array();

    function createXMLString($date_from, $months, $projects)
    {
        $xmlString                                                      = "";

        $xmlString                                                      = "<chart palette='1' caption='Project Income Forecast Report' shownames='1' showvalues='0' ".
                                                                            "numberPrefix='".$_SESSION["currency"]."' showSum='1' decimals='0' overlapColumns='0' ".
                                                                            "formatNumberScale='0' formatNumber='0' ".fusionChart().">";

        //  Create Categories
        $xmlString                                                      .= "<categories>";
        for ($i = 0; $i < $months; $i++)
            $xmlString                                                  .= "<category label='".getNextMonth($date_from, $i)."'></category>";
        $xmlString                                                      .= "</categories>";

        //  Create Arrays
        if (is_array($projects)) {
            foreach($projects as $project) {
                $total                                                  = $project[2] - (($project[2] * ($project[3] / 100)) + $project[6]) + $project[7] + ($project[2] * ($project[8] / 100));
                $invoices                                               = q("SELECT SUM(consulting + diverse_income + (consulting * (vat / 100)) + ".
                                                                            "(diverse_income * (vat / 100))) FROM LoadInvoices WHERE project_id = '".$project[0]."' ".
                                                                            "AND due_date <= '$date_from'");
                $available                                              = $total - $invoices;

                //  Monthly Allocation
                for ($i = 0; $i < $months; $i++)
                {
                    if (getNextMonthStart($date_from, $i) <= $project[5] && getNextMonthStart($date_from, $i) >= getMonthStart($project[4]))
                    {
                        if (($total / getMonthPeriod($project[4], $project[5])) > 0)
                        {
                            $project_month_time_array[$project[0]][getNextMonthStart($date_from, $i)]           = number_format(($total / getMonthPeriod($project[4], $project[5])), 2, ".", "");
                            $total_time[$i]                             += number_format(($total / getMonthPeriod($project[4], $project[5])), 2, ".", "");
                        }

                        if (($available / getMonthPeriod($project[4], $project[5])) > 0)
                        {
                            $project_month_invoice_array[$project[0]][getNextMonthStart($date_from, $i)]        = number_format(($available / getMonthPeriod($project[4], $project[5])), 2, ".", "");
                            $total_invoice[$i]                          += number_format(($available / getMonthPeriod($project[4], $project[5])), 2, ".", "");
                        }

                        if (((($total + $available) / 2) / getMonthPeriod($project[4], $project[5])) > 0)
                        {
                            $project_month_average_array[$project[0]][getNextMonthStart($date_from, $i)]        = number_format(((($total + $available) / 2) / getMonthPeriod($project[4], $project[5])), 2, ".", "");
                            $total_average[$i]                          += number_format(((($total + $available) / 2) / getMonthPeriod($project[4], $project[5])), 2, ".", "");
                        }
                    }
                }
            }
        }

        //  Create Dataset - Time Based
        $xmlString                                                      .= "<dataset seriesName='Time Based' color='0033FF' showValues='0'>";
        for ($i = 0; $i < $months; $i++)
            $xmlString                                                  .= "<set value='".number_format($total_time[$i], 2, ".", "")."'/>";
        $xmlString                                                      .= "</dataset>";
        //  Create Dataset - Invoice Based
        $xmlString                                                      .= "<dataset seriesName='Invoice Based' color='FF0033' showValues='0'>";
        for ($i = 0; $i < $months; $i++)
            $xmlString                                                  .= "<set value='".number_format($total_invoice[$i], 2, ".", "")."'/>";
        $xmlString                                                      .= "</dataset>";
        //  Create Dataset - Average
        $xmlString                                                      .= "<dataset seriesName='Average' color='33FF00' showValues='0'>";
        for ($i = 0; $i < $months; $i++)
            $xmlString                                                  .= "<set value='".number_format($total_average[$i], 2, ".", "")."'/>";
        $xmlString                                                      .= "</dataset>";

        $xmlString                                                      .= "</chart>";

        return $xmlString;
    }

    function createTable($date_from, $months, $projects, $name)
    {

        //  Create Arrays
        if (is_array($projects)) {
            foreach($projects as $project) {
                $total                                                  = $project[2] - (($project[2] * ($project[3] / 100)) + $project[6]) + $project[7] + ($project[2] * ($project[8] / 100));
                $invoices                                               = q("SELECT SUM(consulting + diverse_income + (consulting * (vat / 100)) + ".
                                                                            "(diverse_income * (vat / 100))) FROM LoadInvoices WHERE project_id = '".$project[0]."' ".
                                                                            "AND due_date <= '$date_from'");
                $available                                              = $total - $invoices;

                //  Monthly Allocation
                for ($i = 0; $i < $months; $i++)
                {
                    if (getNextMonthStart($date_from, $i) <= $project[5] && getNextMonthStart($date_from, $i) >= getMonthStart($project[4]))
                    {
                        if (($total / getMonthPeriod($project[4], $project[5])) > 0)
                        {
                            $project_month_time_array[$project[0]][getNextMonthStart($date_from, $i)]           = number_format(($total / getMonthPeriod($project[4], $project[5])), 2, ".", "");
                            $total_time[$i]                             += number_format(($total / getMonthPeriod($project[4], $project[5])), 2, ".", "");
                        }

                        if (($available / getMonthPeriod($project[4], $project[5])) > 0)
                        {
                            $project_month_invoice_array[$project[0]][getNextMonthStart($date_from, $i)]        = number_format(($available / getMonthPeriod($project[4], $project[5])), 2, ".", "");
                            $total_invoice[$i]                          += number_format(($available / getMonthPeriod($project[4], $project[5])), 2, ".", "");
                        }

                        if (((($total + $available) / 2) / getMonthPeriod($project[4], $project[5])) > 0)
                        {
                            $project_month_average_array[$project[0]][getNextMonthStart($date_from, $i)]        = number_format(((($total + $available) / 2) / getMonthPeriod($project[4], $project[5])), 2, ".", "");
                            $total_average[$i]                          += number_format(((($total + $available) / 2) / getMonthPeriod($project[4], $project[5])), 2, ".", "");
                        }
                    }
                }
            }
        }

        if ($name == "Time Based") {
            $array                                                      = $project_month_time_array;
            $total_array                                                = $total_time;
        }
        else if ($name == "Invoice Based") {
            $array                                                      = $project_month_invoice_array;
            $total_array                                                = $total_invoice;
        }
        else {
            $array                                                      = $project_month_average_array;
            $total_array                                                = $total_average;
        }

        $tableString                                                    = "";

        //  Table Header
        $headers                                                        = "<tr>
                                                                                        <td class='on-table-clear' colspan='6'>".$name."</td>
                                                                                        <td class='on-table-clear' colspan='".$months."'>Month</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <th>Project Name</th>
                                                                                        <th>Total<br/>Budget <i>(".$_SESSION["currency"].")</i></th>
                                                                                        <th>Available<br/>Budget <i>(".$_SESSION["currency"].")</i></th>
                                                                                        <th>Registration<br/>Date</th>
                                                                                        <th>Project<br/>Period</th>
                                                                                        <th>Payment per<br/>Month <i>(".$_SESSION["currency"].")</i></th>";

        for ($i = 0; $i < $months; $i++)
        {
            if ($i == ($months - 1))
                $headers                                                .= "<th>".getNextMonth($date_from, $i)."</a></th></tr>";
            else
                $headers                                                .= "<th>".getNextMonth($date_from, $i)."</a></th>";
        }

        //  Table Information
        $payment                                                        = 0;
        $display                                                        = "";

        if (is_array($projects)) {
            foreach ($projects as $project) {
                $display                                                .= "<tr>";

                $display                                                .= "<td>".$project[1]."</td>";
                //  Total Budget
                $display                                                .= "<td>".
                                                                            ($project[2] - ($project[2] * ($project[3] / 100)) + ($project[2] * ($project[8] / 100)))."</td>";
                //  Available Budget
                $total                                                  = $project[2] - (($project[2] * ($project[3] / 100)) + $project[6]) + $project[7] + ($project[2] * ($project[8] / 100));
                $invoices                                               = q("SELECT SUM(consulting + diverse_income + (consulting * (vat / 100)) + (diverse_income * (vat / 100))) ".
                                                                            "FROM LoadInvoices WHERE project_id = '".$project[0]."' AND due_date <= '$date_from'");                
                if ($name == "Time Based")
                    $available                                          = $total;
                else if ($name == "Invoice Based")
                    $available                                          = $total - $invoices;
                else
                    $available                                          = (($total - $invoices) / 2);
                
                $fontColor = "";
                 if($available <0)
                    $fontColor = "style='color:#FF0000'";

                $display                                                .= "<td ".$fontColor ." class='rightdata'>".number_format($available, 2, ".", "")."</td>";
                //  Start Date
                $display                                                .= "<td class='rightdata'>".$project[4]."</td>";
                //  Project Period
                $display                                                .= "<td class='rightdata'>".
                                                                            getMonthPeriod($project[4], $project[5])."</td>";
                //  Payment per Month
                if (($available / getMonthPeriod($project[4], $project[5])) > 0)
                {
                    $payment                                            += number_format(($available / getMonthPeriod($project[4], $project[5])), 2, ".", "");
                    $display                                            .= "<td class='rightdata'>".
                                                                            number_format(($available / getMonthPeriod($project[4], $project[5])), 2, ".", "")."</td>";
                }
                else
                    $display                                            .= "<td class='rightdata'>-</td>";

                //  Monthly Allocation
                for ($i = 0; $i < $months; $i++)
                {
                    if ($array[$project[0]][getNextMonthStart($date_from, $i)] > 0) {
                        if ($i == ($months - 1))
                            $display                                    .= "<td class='rightdata'>".
                                                                            $array[$project[0]][getNextMonthStart($date_from, $i)]."</td>";
                        else
                            $display                                    .= "<td class='rightdata'>".
                                                                            $array[$project[0]][getNextMonthStart($date_from, $i)]."</td>";
                    }
                    else
                    {
                        if ($i == ($months - 1))
                            $display                                    .= "<td class='rightdata'>-</td>";
                        else
                            $display                                    .= "<td class='rightdata'>-</td>";
                    }
                }

                $display                                                .= "</tr>";
            }
        }

        $display                                                        .= "<tr>
                                                                                        <td class='on-table-total' colspan='5'>Grand Total <i>(".$_SESSION["currency"].")</i>:</td>
                                                                                        <td class='on-table-total'>".number_format($payment, 2, ".", "")."</td>";

        for ($i = 0; $i < $months; $i++)
        {
            if ($i == ($months - 1))
                $display                                                .= "<td class='on-table-total'>".
                                                                            number_format($total_array[$i], 2, ".", "")."</td>";
            else
                $display                                                .= "<td class='on-table-total'>".
                                                                            number_format($total_array[$i], 2, ".", "")."</td>";
        }

        $display                                                        .= "</tr>";

        $tableString                                                    = $headers.$display;

        return $tableString;
    }

    //  Create Display Information Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")
    {
        $errorMessage                                                   = "";

        $date_from                                                      = addslashes(strip_tags($_POST["date_from"]));
        $date_to                                                        = addslashes(strip_tags($_POST["date_to"]));
        $displayString                                                  = "";

        $date_from                                                      = getMonthStart($date_from);
        $date_to                                                        = getMonthEnd($date_to);
        $months                                                         = getMonthPeriod($date_from, $date_to);

        $projects                                                       = q("SELECT p.id, p.name, cot.total_budget, cot.buffer, p.reg_date, p.due_date, cot.expenses, cot.diverse_income, cot.vat ".
                                                                            "FROM (Project AS p INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) ".
                                                                            "WHERE p.completed = '0' AND cot.total_budget > 0 AND p.due_date > '".$date_from."' AND cot.company_id = '".$_SESSION["company_id"]."' ".
                                                                            "ORDER BY UPPER(p.name)");

        $graph                                                          = createXMLString($date_from, $months, $projects);
        $displayString                                                  = renderChart("fusion/MSColumn3D.swf", "", $graph, "multigraph", 800, 400, false, false);

        //  Create Tables
        $table                                                          = "";

        //  Create Table - Time Based
        $table                                                          .= "".createTable($date_from, $months, $projects, "Time Based");
        //  Create Table - Invoice Based
        $table                                                          .= "<tr><td colspan='100%'><br/></td></tr>".
                                                                            createTable($date_from, $months, $projects, "Invoice Based");
        //  Create Table - Average
        $table                                                          .= "<tr><td colspan='100%'><br/></td></tr>".
                                                                            createTable($date_from, $months, $projects, "Average");

        $displayString                                                  = "<tr><td class='on-table-clear' colspan='".(6 + $months)."'><a>Income Forecast Report</a>".
                                                                            "</td></tr><tr><td colspan='".(6 + $months)."'>".$displayString.$table;

        if ($displayString != "")
            $generated                                                  = "1";
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("5", "reports");

    if ($errorMessage != "")
    {
        echo "<p align='center' style='padding:0px;'><strong><font class='on-validate-error'>$errorMessage</font></strong></p>";
        echo "<br/>";
    }
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    jQuery(function(){
	jQuery('#date_to').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_to').val(),
		current: jQuery('#date_to').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_to').val()))
			_date = new Date();
			else _date = jQuery('#date_to').val();
			jQuery('#date_to').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_to').val(formated);
			jQuery('#date_to').DatePickerHide();
		}
	});
    })

    function check()
    {
        var valid                                                       = 1;

        //  Check That Date To Is Entered
        if (document.forms["report_income_forecast"].date_to.value == "")
        {
            ShowLayer("dateTo", "block");
            valid                                                       = 0;
        }
        //  Check That Entered Date To Is Valid
        else
        {
            ShowLayer("dateTo", "none");

            if (!validation("date", document.forms["report_income_forecast"].date_to.value))
            {
                ShowLayer("dateToDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("dateToDiv", "none");
        }

        //  Check That Date To Is Later Than Date From
        if (valid == 1)
        {
            if (document.forms["report_income_forecast"].date_to.value < document.forms["report_income_forecast"].date_from.value)
            {
                ShowLayer("dateDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("dateDiv", "none");
        }

        if (valid == 1)
        {
            document.forms["report_income_forecast"].save.value         = 1;
            document.forms["report_income_forecast"].submit();
        }
    }
</script>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="report_income_forecast">
                    <div id="" style="<?php if ($generated == "1") echo "display: none;"; else echo "display: block"; ?>">
                        <table width="100%">
                            <tr>
                                <td class="centerdata">
                                    <h6>Income Forecast Report</h6>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Date From:
                                </td>
                                <td width="50%">
                                    <input class="on-field-date" name="date_from" type="text" readonly style="text-align:right;" value="<?php echo "".getNextMonthStart($today, 1); ?>">
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Date To:
                                </td>
                                <td width="50%">
                                    <input class="on-field-date" id="date_to" name="date_to" type="text" style="text-align:right;" value="<?php
                                        if ($_POST["date_to"] != "") echo "".$_POST["date_to"]; else echo "".getMonthEnd(getNextMonthStart($today, 1)); ?>">
                                    <div id="dateTo" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                    <div id="dateToDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                    <div id="dateDiv" style="display: none;"><font class="on-validate-error">* Date to must be later than date from</font></div>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <input name="btnGenerate" onClick="check();" type="button" value="Generate Report">
                        <input method="post" name="save" type="hidden" value="0" />
                    </div>
                </form>
                <div id="report" style="<?php if ($generated == "1") echo "display: block;"; else echo "display: none"; ?>">
                    <?php
                        echo "<table class='on-table-center on-table'>";
                            echo "".$displayString;
                            echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        echo "</table>";
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
