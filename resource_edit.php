<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!hasAccess("RESOURCE_MAN"))
        header("Location: noaccess.php");

    //  Update Resource Function
    if (isset($_POST["save"]) && $_POST["save"] === "1") {
        $errorMessage                                                   = "";

        //  Get Information
        $id                                                             = $_GET["id"];
        $resource                                                       = addslashes(strip_tags($_POST["resource_new"]));

        //  Check If Resource Exists In Database
        $exist                                                          = q("SELECT id,resource FROM Resources WHERE resource = '$resource' AND id != '$id'");

        if (!is_array($exist)) {
            $update                                                     = q("UPDATE Resources SET resource = '$resource' WHERE id = '$id'");

            if ($update) {
                $time                                                   = date("H:i:s");

                $logs                                                   = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time) ".
                                                                            "VALUES ('$resource updated', 'Update', 'Resources', '".$_SESSION["email"]."', ".
                                                                            "'$today', '$time')");

                header("Location: resources.php");
            }
        }
        else
            $errorMessage                                               = "Resource Already Exists";
    }

    if ($errorMessage != "")
        echo "<p align='center' style='padding:0px;'><strong><font class='on-validate-error'>$errorMessage</font></strong></p>";

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("1", "resources");
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    function check()
    {
        var valid                                                       = 1;

        //  Check That Project Type Is Entered
        if (document.forms["resource_edit"].resource_new.value == "")
        {
            ShowLayer("resourceName", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("resourceName", "none");

        if (valid == 1)
        {
            document.forms["resource_edit"].save.value                  = 1;
            document.forms["resource_edit"].submit();
        }
    }
</script>
<?php
    $id                                                                 = $_GET["id"];
    $resource_info                                                      = q("SELECT resource FROM Resources WHERE id = '$id'");
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata">
                <form action="" method="post" name="resource_edit">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>
                                    Edit Resource
                                </h6>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <br/>
                            </td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                    Old Resource:
                            </td>
                            <td width="50%">
                                <input class="on-field" name="resource_old" readonly type="text" value="<?php echo $resource_info; ?>">
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                    New Resource:
                            </td>
                            <td width="50%">
                                <input class="on-field" name="resource_new" tabindex="1" type="text">
                                <div id="resourceName" style="display: none;"><font class="on-validate-error">* Resource must be entered</font></div>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <input name="btnUpdate" onClick="check();" tabindex="2" type="button" value="Update Resource">
                    <input method="post" name="save" type="hidden" value="0" />
                </form>
            </td>
        </tr>
        <tr>
            <td>
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
