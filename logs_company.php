<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

	if (!hasAccess("LEAVE_MANAGE_ALL"))
		header("Location: noaccess.php");

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("8", "logs");

    if ($errorMessage != "")
    {
        echo "<p align='center' style='padding:0px;'><strong><font color='#999999'>$errorMessage</font></strong></p>";
        echo "<br/>";
    }
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    jQuery(function(){
	jQuery('#display_date').DatePicker({
		format:'Y-m-d',
		date: jQuery('#display_date').val(),
		current: jQuery('#display_date').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#display_date').val()))
			_date = new Date();
			else _date = jQuery('#display_date').val();
			jQuery('#display_date').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#display_date').val(formated);
			jQuery('#display_date').DatePickerHide();
		}
	});
    })

    function display_check()
    {
        var valid                                                       = 1;

        //  Check That Company Is Selected
        if (document.forms["company_logs"].company.value == "null")
        {
            ShowLayer("companyDiv", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("companyDiv", "none");

        // Check That Registration Date Is Entered
        if (document.forms["company_logs"].display_date.value == "")
        {
            ShowLayer("displayDate", "block");
            valid                                                       = 0;
        }
        //  Check That Entered Registration Date Is Valid
        else
        {
            ShowLayer("displayDate", "none");

            if (!validation("date", document.forms["company_logs"].display_date.value))
            {
                ShowLayer("displayDateDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("displayDateDiv", "none");
        }

        if (valid == 1)
        {
            document.forms["company_logs"].save.value                   = 1;
            document.forms["company_logs"].submit();
        }
    }
</script>
<?php
    //  nCompanies                                                      = Number of Companies
    $nCompanies                                                         = q("SELECT COUNT(id) FROM Company");
    $companies                                                          = q("SELECT id, name FROM Company ORDER BY name");
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="company_logs">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>
                                    Company Logs
                                </h6>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                Select Company:
                            </td>
                            <td width="50%">
                                <select class="on-field" method="post" name="company" tabindex="1">
                                    <option value="null">--  Select Company  --</option>
                                    <?php
                                        if ($nCompanies > 1)
                                            foreach ($companies as $company)
                                                if ($_POST["company"] == $company[0])
                                                    echo "<option value='".$company[0]."' selected>".$company[1]."</option>";
                                                else
                                                    echo "<option value='".$company[0]."'>".$company[1]."</option>";
                                        else if ($nCompanies == 1)
                                            if ($_POST["company"] == $companies[0][0])
                                                echo "<option value='".$companies[0][0]."' selected>".$companies[0][1]."</option>";
                                            else
                                                echo "<option value='".$companies[0][0]."'>".$companies[0][1]."</option>";
                                    ?>
                                </select>
                                <div id="companyDiv" style="display: none;"><font class="on-validate-error">* Company must be selected</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                Display Date:
                            </td>
                            <td width="50%">
                                <input class="on-field-date" id="display_date" name="display_date" tabindex="2" type="text" style="text-align:right;" value="<?php
                                    if ($_POST["display_date"] != "") echo "".$_POST["display_date"]; else echo "".$today; ?>">
                                <div id="displayDate" style="display: none;"><font class="on-validate-error">* Display date must be entered</font></div>
                                <div id="displayDateDiv" style="display: none;"><font class="on-validate-error">* Display date not valid, eg.<?php echo "".date("Y-m-d"); ?></font>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <input name="btnDisplay" onClick="display_check();" tabindex="3" type="button" value="Display Information">
                    <input method="post" name="save" type="hidden" value="0" />
                </form>
                <?php
                    //  Display Information Function
                    if (isset($_POST["save"]) && $_POST["save"] === "1")
                    {
                        echo "<form action='' method='post'>";
                            $company_id                                 = $_POST["company"];
                            $display_date                               = addslashes(strip_tags($_POST["display_date"]));

                            ///////////////////////////
                            //  Get Information
                            $nLogs                                      = q("SELECT COUNT(id) FROM Logs WHERE on_date = '$display_date' AND company_id = '$company_id' ".
                                                                            "AND by_user != 'admin'");
                            $logs                                       = q("SELECT what, by_user, on_table, on_date, on_time FROM Logs WHERE on_date = '$display_date' ".
                                                                            "AND company_id = '$company_id' AND by_user != 'admin' ORDER BY on_date, on_time DESC");
                            ///////////////////////////
                            //  Display Information
                            echo "<br/><br/>";
                            echo "<div class='on-20px'><table class='on-table-center on-table'>";
                                //  Table Headers
                                echo "<tr>";
                                    echo "<th>What</th>";
                                    echo "<th>Employee</th>";
                                    echo "<th>On Table</th>";
                                    echo "<th>On Date</th>";
                                    echo "<th>@ Time</th>";
                                echo "</tr>";
                                //  Table Information
                                if ($nLogs > 1)
                                {
                                    foreach ($logs as $log)
                                    {
                                        echo "<tr>";
                                            echo "<td>".$log[0]."</td>";
                                            echo "<td>".$log[1]."</td>";
                                            echo "<td>".$log[2]."</td>";
                                            echo "<td style='white-space:nowrap; width:80px' class='rightdata'>".$log[3]."</td>";
                                            echo "<td class='rightdata'>".$log[4]."</td>";
                                        echo "</tr>";
                                    }
                                }
                                else if ($nLogs == 1)
                                {
                                    echo "<tr>";
                                        echo "<td>".$logs[0][0]."</td>";
                                        echo "<td>".$logs[0][1]."</td>";
                                        echo "<td>".$logs[0][2]."</td>";
                                        echo "<td style='white-space:nowrap; width:80px' class='rightdata'>".$logs[0][3]."</td>";
                                        echo "<td class='rightdata'>".$logs[0][4]."</td>";
                                    echo "</tr>";
                                }
                                else
                                {
                                    echo "<tr>";
                                        echo "<td class='centerdata' colspan='100%'>No information to display</td>";
                                    echo "</tr>";
                                }
                            echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                            echo "</table></div>";
                            ///////////////////////////
                        echo "</form>";
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
