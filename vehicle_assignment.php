<?php
	session_start();

	include("_db.php");
	include("graphics.php");
	include("include/sajax.php");
	include("_functions.php");

	if (!$_SESSION["logged_in"] === true)
		header("Location: login.php");

	if (!isset($_SESSION["company_id"]))
		header("Location: home.php");

	if (!hasAccess("VEHICLE_ASSIGN"))
		header("Location: noaccess.php");

	if ($_POST['saveInfo']){  	//save clicked
		$user = $_POST["user"];
		$veh = $_POST["veh"];
		$delete = q("DELETE FROM vehicle_user WHERE user_id = '$user' ".
			"AND vehicle_id IN (SELECT id FROM Vehicle WHERE company_id = '".$_SESSION["company_id"]."')");
		foreach ($veh as $h)	{
			$insert = q("INSERT INTO vehicle_user (vehicle_id,user_id) VALUES ('$h','$user')");
		}
	}

	//  Print Header
	print_header();
	//  Print Menu
	print_menus("0", "vehicles");
?>
<link rel="stylesheet" type="text/css" href="include/jquery.autocomplete.css" />
<script type='text/javascript' src='include/jquery.autocomplete.js'></script>
<script language="JavaScript">
	jQuery(function()    {
		jQuery("#user").change(function() {
			var userid = jQuery("#user").val();
			if(userid != "0")   {
				jQuery.post("_ajax.php", {func: "get_vehicle_assign", userid: userid}, function(data)	{
					data = json_decode(data);
					if (is_array(data)){
						jQuery("#data tbody>tr").not(':first').remove();    //remove current entries
						jQuery("#vehicleDiv").show();	//make the table shown after the entries have been wiped
						var last = jQuery("#hidden_vehicleTbl tbody>tr:last");
						var first = jQuery("#data tbody>tr:first");
						var hiddenValue = "";

						jQuery.each(data,function(i, v)  {
							var n = last.clone(true);
							n.children().eq(0).children().eq(0).text(v[1]);
							n.children().eq(1).children().eq(0).val(v[0]);
							n.insertAfter(jQuery("#data tbody>tr:last"));
							hiddenValue = hiddenValue + v[0] + "_";
						});

						jQuery("#hiddenVehicles").val(hiddenValue);
						//jQuery("#data tbody>tr:last").remove();    //remove current entries
					}
				});
			}else{
				jQuery("#vehicleDiv").hide();
			}
		});

		///////////////////////////////////
		//	Autocomplete
		//////////////////////////////////
		function findValue(li)  {
			if (li == null) return alert("No match!");
			var sValue = li.extra[0];
			var tValue = li.selectValue;
		}

		function selectItem(li) {
			findValue(li);
		}

		function setVariables(field, fieldValue, fieldClass)    {
			var vehicles = jQuery("#hiddenVehicles").val();
			jQuery.post("_ajax.php", {func: "setSessionVariable", sessionName: "vehicles", sessionValue: vehicles}, function(data){});
		}

		jQuery(".autocomplete").focus(function() {
			setVariables("vehicleName", jQuery("#hiddenVehicles").val(), "");
		});

	   <?php
				jQAutoComplete("#", "vehicleName", "get_vehicles");
			?>

		jQuery(".addVehicleBtn").click(function()    {
			var inputField = "#"+jQuery(this).attr("name");
			var inputValue = jQuery(inputField).val();

			var alreadyInList = false;

			jQuery("#data tbody>tr").each(function(i,v){
				if (jQuery.trim(jQuery(this).children().eq(0).children().eq(0).text()) == inputValue)	alreadyInList = true;
			});

			if (inputValue != "" && !alreadyInList)   {
			//if (inputValue != "")   {
				var query = "SELECT id,type FROM Vehicle WHERE type = '" + addslashes(inputValue) + "' AND company_id = '<?php  echo $_SESSION["company_id"] ?>'";
				jQuery.post("_ajax.php", {func: "query_direct", query: query}, function(data)    {
					data = json_decode(data);
					var veh_id = data[0][0];
					var veh_name = data[0][1];
					var last = jQuery("#hidden_vehicleTbl tbody>tr:last");
					var first = jQuery("#data tbody>tr:first");
					var n = last.clone(true);
					n.children().eq(0).children().eq(0).text(veh_name);
					n.children().eq(1).children().eq(0).val(veh_id);
					n.insertAfter(jQuery("#data tbody>tr:last"));
					jQuery(inputField)[0].autocompleter.flushCache();
					jQuery("#vehicleDiv").show();

					var hiddenValue = jQuery("#hiddenVehicles").val();

					hiddenValue = hiddenValue + veh_id + "_";

					jQuery("#hiddenVehicles").val(hiddenValue);
				});
			}

			jQuery(inputField).val("");
		});
		//////////////////////////////////////////////////////////////////////

		jQuery(".delRow").click(function()   {
			if (jQuery(this).parent().parent().parent().children().length > 1)
				jQuery(this).parent().parent().remove();
		});
	});
</script>

	<table width="100%">
		<tr height="380px">
			<td class="centerdata" valign="top">
				<form action="" method="post" name="assign_vehicle">
					<table width="100%">
						<tr>
							<td class="centerdata">
								<h6>Vehicle Assignment</h6>
							</td>
						</tr>
						<?php if($_POST['saveInfo']) echo "<tr><td class='centerdata'><br/><a style='color:#028200'> - Vehicle(s) has been assigned - </a></td></tr>"; ?>
						<tr>
							<td>
								<br/>
							</td>
						</tr>
					</table>

					<table width="100%">
						<tr>
							<td class="on-description" width="50%">
								User Name:
							</td>
							<td width="50%">
								<?php
									$result = q("SELECT e.lstname,e.frstname,e.id FROM (Employee AS e INNER JOIN Company_Users as cu ON e.id = cu.user_id) ".
												"WHERE cu.company_id = ".$_SESSION["company_id"]." AND e.locked = '0' AND e.deleted = '0' AND e.email != 'admin' ".
												"ORDER BY e.lstname,e.frstname");
									echo "<select class='on-field' id='user' name='user'></option>";
									echo "<option value='0'>-- Select a User --</option>";
									if (is_array($result))
										foreach($result as $r){
											echo "<option value=".$r[2].">".$r[0].","." "."".$r[1]."</option>";
										}
									echo "</select>";   // Closing of list box
								?>
							</td>
						</tr>
						<tr>
						<td class="on-description">
							Please start typing vehicle name:
						</td>
						<td width="180px">
							<table>
								<tr>
									<td>
									<input id="vehicleName" name="vehicleName" type="text" class="on-field autocomplete" value="" />
									<td>
									<td>
									<img style="margin-top:6px" id="teamTbl" name="vehicleName" class="addVehicleBtn" src="images/icons/add.ico" title="Add" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
					</table>

					<div id="vehicleDiv" style="display:none">
						<table width="100%">
							<tr>
								<td class="on-description" width="50%">
									Vehicles:
								</td>
								<td width="50%">
									<table id="data" class="on-lefttable on-table">
										<tr class="hidden">
											<td></td>
											<td></td>
											<td></td>
										</tr>
										<?php
										echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
										?>
									</table>
								</td>
							</tr>
						</table>
					</div>

					<br/>
					<input name="btnSave" type="submit" value="Save">
					<input id="hiddenVehicles" name="hiddenVehicles" type="hidden" value="" />
					<input name="saveInfo" type="hidden" value="saveInfo" />
				</form>
			</td>
		</tr>
		<tr>
			<td align="center">
				<br/>
			</td>
		</tr>
	</table>
	<div class="hidden">
		<table id="hidden_vehicleTbl" name="hidden_vehicleTbl">
			<tr>
				<td><div></div></td>
				<td><input id="veh[]" name="veh[]" type="hidden" value="" /></td>
				<td><input class="delRow" type="button" value="Remove" /></td>
			</tr>
		</table>
	</div>
<?php
    //  Print Footer
    print_footer();
?>
