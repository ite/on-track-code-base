<?php
    include("_db.php");
?>
<html>
    <head>
        <title>
            On-Track - Script...
        </title>
    </head>
    <body>
        <h1 align="center">
            On-Track - Script...
        </h1>
        <?php
            $date = date("Y-m-d H:i:s");
            $user = "admin";

			$index = q("CREATE INDEX cot_project_id ON companiesOnTeam (project_id)");
			$index = q("CREATE INDEX cot_company_id ON companiesOnTeam (company_id)");
/*
			$index = q("CREATE INDEX ts_projectType ON TimeSheet (projectType);");

			$index = q("CREATE INDEX aptime_date ON ApprovedTime (date);");
			$index = q("CREATE INDEX aptime_company_id ON ApprovedTime (company_id);");
			$index = q("CREATE INDEX aptime_user_id ON ApprovedTime (user_id);");
			$index = q("CREATE INDEX aptime_project_id ON ApprovedTime (project_id);");
			$index = q("CREATE INDEX aptime_area_id ON ApprovedTime (area_id);");
			$index = q("CREATE INDEX aptime_activity_id ON ApprovedTime (activity_id);");
			$index = q("CREATE INDEX aptime_time_spent ON ApprovedTime (time_spent);");
			$index = q("CREATE INDEX aptime_on_date ON ApprovedTime (on_date);");
			$index = q("CREATE INDEX aptime_on_time ON ApprovedTime (on_time);");
			$index = q("CREATE INDEX aptime_projectType ON ApprovedTime (projectType);");
			$index = q("CREATE INDEX aptime_status ON ApprovedTime (status);");
			$index = q("CREATE INDEX aptime_status2 ON ApprovedTime (status2);");

			$index = q("CREATE INDEX ex_company_id ON ExpenseSheet (company_id);");
			$index = q("CREATE INDEX ex_projectType ON ExpenseSheet (projectType);");
			$index = q("CREATE INDEX ex_date ON ExpenseSheet (date);");
			$index = q("CREATE INDEX ex_user_id ON ExpenseSheet (user_id);");
			$index = q("CREATE INDEX ex_project_id ON ExpenseSheet (project_id);");
			$index = q("CREATE INDEX ex_area_id ON ExpenseSheet (area_id);");
			$index = q("CREATE INDEX ex_activity_id ON ExpenseSheet (activity_id);");
			$index = q("CREATE INDEX ex_expense_type_id ON ExpenseSheet (expense_type_id);");
			$index = q("CREATE INDEX ex_vehicle_id ON ExpenseSheet (vehicle_id);");
			$index = q("CREATE INDEX ex_disbursement_id ON ExpenseSheet (disbursement_id);");
			$index = q("CREATE INDEX ex_on_date ON ExpenseSheet (on_date);");
			$index = q("CREATE INDEX ex_on_time ON ExpenseSheet (on_time);");

			$index = q("CREATE INDEX apexpense_company_id ON ApprovedExpense (company_id);");
			$index = q("CREATE INDEX apexpense_projectType ON ApprovedExpense (projectType);");
			$index = q("CREATE INDEX apexpense_date ON ApprovedExpense (date);");
			$index = q("CREATE INDEX apexpense_user_id ON ApprovedExpense (user_id);");
			$index = q("CREATE INDEX apexpense_project_id ON ApprovedExpense (project_id);");
			$index = q("CREATE INDEX apexpense_area_id ON ApprovedExpense (area_id);");
			$index = q("CREATE INDEX apexpense_activity_id ON ApprovedExpense (activity_id);");
			$index = q("CREATE INDEX apexpense_expense_type_id ON ApprovedExpense (expense_type_id);");
			$index = q("CREATE INDEX apexpense_vehicle_id ON ApprovedExpense (vehicle_id);");
			$index = q("CREATE INDEX apexpense_disbursement_id ON ApprovedExpense (disbursement_id);");
			$index = q("CREATE INDEX apexpense_on_date ON ApprovedExpense (on_date);");
			$index = q("CREATE INDEX apexpense_on_time ON ApprovedExpense (on_time);");
			$index = q("CREATE INDEX apexpense_status ON ApprovedExpense (status);");
			$index = q("CREATE INDEX apexpense_status2 ON ApprovedExpense (status2);");
*/
            echo "<p align='center'>Script completed successfully...</p>";
        ?>
        <form action="index.php" method="post">
            <center><input type="submit" value="Return"/></center>
        </form>
    </body>
</html>
