<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("include/sajax.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");
        
    if (!hasAccess("REP_PROJECT"))
        header("Location: noaccess.php");

    // Class for table background color
    $colorClass = "OnTrack_#_";

    ///////////////////////////
    //  Sajax
    function getProjects($id) {
		if ($id == "null")
			$projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p 
                                    INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) 
                                    WHERE cot.company_id = '".$_SESSION["company_id"]."' 
                                    AND p.completed = '0' 
                                    AND p.deleted = '0' ORDER BY UPPER(p.name)");
		else
			$projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p 
                                    INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) 
                                    WHERE cot.company_id = '".$_SESSION["company_id"]."' 
                                    AND p.completed = '0' 
                                    AND p.deleted = '0' 
                                    AND p.type_id='$id' ORDER BY UPPER(p.name)");
		return $projects;
	}



    $sajax_request_type                                                 = "GET";
    sajax_init();
    sajax_export("getProjects");
    sajax_handle_client_request();
    ///////////////////////////

    //  Get Expenses Info
    function getExpenses($project_id, $date_from, $date_to, $total_time, $total_cost)
    {
        GLOBAL $myData;
        GLOBAL $row;
    
        $expense_based                                                  = "";
                                                                            
        $activityTypes                                                  =q("SELECT DISTINCT(at.id), at.type, es.expense, es.area_id FROM (ActivityTypes AS at 
                                                                                        LEFT JOIN Activities AS a ON at.id = a.parent_id 
                                                                                        LEFT JOIN ExpenseSheet AS es ON es.activity_id = a.id
                                                                                        INNER JOIN companiesOnTeam AS cot ON cot.project_id = es.project_id) 
                                                                                        WHERE cot.project_id = '$project_id' 
                                                                                        AND es.date >= '$date_from' 
                                                                                        AND es.date <= '$date_to' 
                                                                                        AND cot.company_id = '".$_SESSION["company_id"]."' 
																						AND es.company_id = '".$_SESSION["company_id"]."' 
                                                                                        ORDER BY at.type");
                                                                            
         ///////////////////////////////
            $hasAreas = 0;                                               // Does Areas exist for the user in expense or time sheet table
            
            $areasExpense = q("SELECT project_id, area_id FROM ExpenseSheet WHERE project_id = '".$project_id."' AND user_id = ".$_SESSION["user_id"]." AND area_id > 0 AND date BETWEEN '".$date_from."' AND '".$date_to."'");
             
            if($areasExpense != "" && $areasExpense != 0 ) {                              // Time
                $hasAreas = 1;
            }
            ////////////////////////////////
            $extraCounter = 0;
            if($hasAreas){
                $areaData = "<th>Area</th>";
                $extraCounter  = 1;
            }else{
                $areaData = "";
                $extraCounter  = 0;
            }

        if (is_array($activityTypes)) {
            $expense_based                                              .= "<tr>
                                                                                            <th class='on-table-light' colspan='100%'><a>Expenses:</a></th>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            ".$areaData."
                                                                                            <th>Activity Type</th>
                                                                                            <th>Activity</th>
                                                                                            <th>Time Spent</th>
                                                                                            <th>Cost <i>(".$_SESSION["currency"].")</i></th>
                                                                                    </tr>";
                                                                                        
            foreach ($activityTypes as $activityType){
                if($hasAreas){
                   $activities  = q("SELECT a.id, a.name FROM (ExpenseSheet AS es ".
                            "INNER JOIN Activities AS a ON a.id = es.activity_id) ".
                            "WHERE a.parent_id = '".$activityType[0]."' AND es.area_id = '".$activityType[3]."' AND es.project_id = '$project_id' ".
							"AND es.company_id = '".$_SESSION["company_id"]."' ".
                            "AND es.date >= '$date_from' AND es.date <= '$date_to' GROUP BY a.id ORDER BY a.name");
                }else{
                    $activities  = q("SELECT a.id, a.name FROM (ExpenseSheet AS es ".
                            "INNER JOIN Activities AS a ON a.id = es.activity_id) ".
                            "WHERE a.parent_id = '".$activityType[0]."' AND es.project_id = '$project_id' ".
							"AND es.company_id = '".$_SESSION["company_id"]."' ".
                            "AND es.date >= '$date_from' AND es.date <= '$date_to' GROUP BY a.id ORDER BY a.name");
                }
        
                $expense_based                                          .= "<tr>";
                
                if($hasAreas){
                $area = q("SELECT name FROM areas WHERE id =  '".$activityType[3]."' ");
                if($area == "")
                    $area = "-";
                $expense_based .= "<td>".$area."</td>
                                            <td>".$activityType[1]."</td>
                                            <td>";
            }else{
                $expense_based .= "<td>".$activityType[1]."</td>
                                            <td>";
            }
                                                                                            
            foreach ($activities as $activity)
                $expense_based                                           .= $activity[1]."<br><br>";
                                                                                            
                $expense_based                                          .= "</td>
                                                                                            <td class='rightdata'>-</td>
                                                                                            <td class='rightdata'>".number_format($activityType[2], 2, ".", "")."</td>
                                                                                        </tr>";   
            }

            $expCount = 0;
            foreach ($activityTypes as $activityType)
            {
                $total_cost                                             += $activityType[2];

                /////////////
                if($expCount == 0){
                    $myData[$row][0] = "Expenses:";                                                   // Heading
                    $myData[$row][1] = "";                                               
                    $myData[$row][2] = "";                                                               
                    $myData[$row][3] = "";  
                    $myData[$row][4] = "";
                        $row++;
                        $expCount ++;
                }else{
                    $myData[$row][0] = "";                                                                           // Heading
                    $myData[$row][1] = "";                                                 
                    $myData[$row][2] = "";                                                               
                    $myData[$row][3] = "";                                                                
                    $myData[$row][4] = "";
                }
                
                if($hasAreas){
                   $activities  = q("SELECT a.id, a.name FROM (ExpenseSheet AS es ".
                            "INNER JOIN Activities AS a ON a.id = es.activity_id) ".
                            "WHERE a.parent_id = '".$activityType[0]."' AND es.area_id = '".$activityType[3]."' AND es.project_id = '$project_id' ".
                            "AND es.date >= '$date_from' AND es.date <= '$date_to' GROUP BY a.id ORDER BY a.name");
                }else{
                    $activities  = q("SELECT a.id, a.name FROM (ExpenseSheet AS es ".
                            "INNER JOIN Activities AS a ON a.id = es.activity_id) ".
                            "WHERE a.parent_id = '".$activityType[0]."' AND es.project_id = '$project_id' ".
                            "AND es.date >= '$date_from' AND es.date <= '$date_to' GROUP BY a.id ORDER BY a.name");
                }
                                                
                    
                                                
                foreach ($activities as $activity)
                {
                    if($hasAreas){
                        $area = q("SELECT name FROM areas WHERE id =  '".$activityType[3]."' ");
                    }
                    if($area == "")
                        $area = "-";
                    
                    $myData[$row][0] = $area;                                                                      // Area
                    $myData[$row][1] = $activityType[1];                                                     // Activity Type
                    $myData[$row][2] = $activity[1];                                                             // Activity
                    $myData[$row][3] = "-";                                                                          // Blank Times
                    $myData[$row][4] = number_format($activityType[2], 2, ".", "");             // Amount
                        $row++;
                } 
                /////////////
            }
        }

        $expense_based                                                  .= "<tr>
                                                                                            <td class='on-table-total' colspan='".(2+$extraCounter)."'>Grand Total <i>(".$_SESSION["currency"].")</i>:</td>
                                                                                            <td class='on-table-total'>".number_format($total_time, 2, ".", "")."</td>
                                                                                            <td class='on-table-total'>".number_format($total_cost, 2, ".", "")."</td>
                                                                                        </tr>";
        
        $myData[$row][0] = "";        
        $myData[$row][1] = "";        
        $myData[$row][2] = "Grand Total (".$_SESSION["currency"].")";                                                
        $myData[$row][3] = number_format($total_time, 2, ".", "");                                                               
        $myData[$row][4] = number_format($total_cost, 2, ".", "");         
            $row++;
            
        return "".$expense_based;
    }
    
    //  Create Display Information Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")
    {
        $errorMessage                                                   = "";

        $project_id                                                     = $_POST["project"];
        $date_from                                                      = addslashes(strip_tags($_POST["date_from"]));
        $date_to                                                        = addslashes(strip_tags($_POST["date_to"]));
        $displayString                                                  = "";
        
        ///////////////////////////////
            $hasAreas = 0;                                               // Does Areas exist for the user in expense or time sheet table
            
            $areasTime = q("SELECT project_id, area_id FROM TimeSheet WHERE project_id = '".$project_id."' AND user_id = ".$_SESSION["user_id"]." AND area_id > 0 AND date BETWEEN '".$date_from."' AND '".$date_to."'");
            
            if($areasTime != "" && $areasTime != 0 ) {                              // Time
                $hasAreas = 1;
            }
            
        ////////////////////////////////
        $extraCounter = 0;
        if($hasAreas){
            $areaData = "<th>Area</a></th>";
            $extraCounter  = 1;
        }else{
            $areaData = "";
            $extraCounter  = 0;
        }

        ///////////////////////////
        //  Get Info From Tables
        ///////////////////////////
        $project_name                                                   = q("SELECT name From Project WHERE id = '$project_id'");

        $total_cost                                                     = 0;
        $total_time                                                     = 0;
        
        $table_headers                                                  = "<tr>
                                                                                            <th class='on-table-light' colspan='100%'><a>Consultation:</a></th>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            ".$areaData ."
                                                                                            <th>Activity Type</th>
                                                                                            <th>Activity</th>
                                                                                            <th>Time Spent</th>
                                                                                            <th>Cost <i>(".$_SESSION["currency"].")</i></th>
                                                                                    </tr>";
                                                                                    
         //////  Set Report Generated Status
                $generated                                                          = "0";
                
                $row                                                                    = 0;
                $myData;
                
                $excelheadings[$row][] = "";
                $excelheadings[$row][] = "";
                $excelheadings[$row][] = "";
                $excelheadings[$row][] = "";
                $excelheadings[$row][] = "";
                    $row++;
                    
                $exceldata[$row][] = "Report: Project";
                $exceldata[$row][] = "";
                $exceldata[$row][] = "";
                $exceldata[$row][] = "";
                $exceldata[$row][] = "";
                    $row++;
                    
                $exceldata[$row][] = "Project: ".$project_name;
                $exceldata[$row][] = "";
                $exceldata[$row][] = "";
                $exceldata[$row][] = "";
                $exceldata[$row][] = "";
                    $row++;
                    
                $exceldata[$row][] = "Consultation:";
                $exceldata[$row][] = "";
                $exceldata[$row][] = "";
                $exceldata[$row][] = "";
                $exceldata[$row][] = "";
                    $row++;
                    
                $exceldata[$row][] = $colorClass."Area:";
                $exceldata[$row][] = $colorClass."Activity Type";
                $exceldata[$row][] = $colorClass."Activity";
                $exceldata[$row][] = $colorClass."Time Spent ";
                $exceldata[$row][] = $colorClass."Cost";
                    $row++;

        ///////////////////////////
        //  Create Display
        ///////////////////////////
        //  Time Booked Section
        $activityTypes                                                  = q("SELECT DISTINCT(at.id), at.type, ts.area_id FROM (ActivityTypes AS at 
                                                                                        LEFT JOIN Activities AS a ON at.id = a.parent_id 
                                                                                        LEFT JOIN TimeSheet AS ts ON ts.activity_id = a.id
                                                                                        INNER JOIN companiesOnTeam AS cot ON cot.project_id = ts.project_id) 
                                                                                        WHERE cot.project_id = '$project_id' 
                                                                                        AND ts.date >= '$date_from' 
                                                                                        AND ts.date <= '$date_to' 
																						AND ts.company_id = '".$_SESSION["company_id"]."' 
                                                                                        ORDER BY at.type");

        if (is_array($activityTypes)) {
            foreach ($activityTypes as $activityType) {
             if($hasAreas){
               $activities                                             = q("SELECT a.id, a.name, SUM(ts.time_spent), SUM(ts.rate * ts.time_spent) FROM (TimeSheet AS ts 
                                                                                    INNER JOIN Activities AS a ON a.id = ts.activity_id) 
                                                                                    WHERE a.parent_id = '".$activityType[0]."' 
                                                                                    AND ts.area_id = '".$activityType[2]."' 
                                                                                    AND ts.project_id = '$project_id' 
                                                                                    AND ts.date >= '$date_from' 
																					AND ts.company_id = '".$_SESSION["company_id"]."' 
                                                                                    AND ts.date <= '$date_to' GROUP BY a.id ORDER BY a.name");
            }else{
                $activities                                             = q("SELECT a.id, a.name, SUM(ts.time_spent), SUM(ts.rate * ts.time_spent) FROM (TimeSheet AS ts 
                                                                                    INNER JOIN Activities AS a ON a.id = ts.activity_id) 
                                                                                    WHERE a.parent_id = '".$activityType[0]."' 
                                                                                    AND ts.project_id = '$project_id' 
                                                                                    AND ts.date >= '$date_from' 
																					AND ts.company_id = '".$_SESSION["company_id"]."' 
                                                                                    AND ts.date <= '$date_to' GROUP BY a.id ORDER BY a.name");
            }
                                                                            
            $displayString           .= "<tr>";

            if($hasAreas){
                $area = q("SELECT name FROM areas WHERE id =  '".$activityType[2]."' ");
                if($area == "")
                    $area = "-";
                $displayString .= "<td>".$area."</td>
                                            <td>".$activityType[1]."</td>";
                                            
            }else{
                $displayString .= "<td>".$activityType[1]."</td>";
            }

                if (is_array($activities )) {
                        
                    $displayString                                      .= "<td>"; // Activities

                    foreach ($activities as $activity)
                        $displayString                                  .= $activity[1]."<br><br>";

                    $displayString                                      .= "</td><td class='rightdata'>";

                    foreach ($activities as $activity)
                    {
                        $total_time                                     += $activity[2];

                        $displayString                                  .= number_format($activity[2], 2, ".", "")."<br><br>";
                    }

                    $displayString                                      .= "</td><td class='rightdata'>";

                    foreach ($activities as $activity)
                    {
                        $total_cost                                     += $activity[3];

                        $displayString                                  .= number_format($activity[3], 2, ".", "")."<br><br>";
                        
                        if($hasAreas){
                            $area = q("SELECT name FROM areas WHERE id =  '".$activityType[2]."' ");
                        }
                        if($area == "")
                            $area = "-";
                        $myData[$row][0] = $area;                                                     // Area
                        $myData[$row][1] = $activityType[1];                                    // Activity Type
                        $myData[$row][2] = $activity[1];                                           // Activity
                        $myData[$row][3] = $activity[2];                                           // Time Spent
                        $myData[$row][4] = $activity[3];                                           // Cost
                            $row++;
                    }

                    $displayString                                      .= "</td>";
                }

                $displayString                                          .= "</tr>";
            }
        }

        $displayString                                                  .= getExpenses($project_id, $date_from, $date_to, $total_time, $total_cost);

        if ($displayString != "") {
            $displayString                                              = "<tr>
                                                                                        <td class='on-table-clear' colspan='100%'><a>Project Report<a></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class='on-table-clear' colspan='100%'>Project: ".$project_name."</td>
                                                                                    </tr>".$table_headers.$displayString;

            $generated                                                  = "1";
        }
    }
    
    for($i=5; $i<$row; $i++){
        $exceldata[$i][0] = $myData[$i][0]; 
        $exceldata[$i][1] = $myData[$i][1]; 
        $exceldata[$i][2] = $myData[$i][2]; 
        $exceldata[$i][3] = $myData[$i][3];
        $exceldata[$i][4] = $myData[$i][4];
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("3", "reports");
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    jQuery(function(){
	jQuery('#date_from').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_from').val(),
		current: jQuery('#date_from').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_from').val()))
			_date = new Date();
			else _date = jQuery('#date_from').val();
			jQuery('#date_from').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_from').val(formated);
			jQuery('#date_from').DatePickerHide();
		}
	});
    })

    jQuery(function(){
	jQuery('#date_to').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_to').val(),
		current: jQuery('#date_to').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_to').val()))
			_date = new Date();
			else _date = jQuery('#date_to').val();
			jQuery('#date_to').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_to').val(formated);
			jQuery('#date_to').DatePickerHide();
		}
	});
    })

    function check()
    {
        var valid                                                       = 1;

        //  Check That Employee Is Selected
        if (document.forms["report_project"].project.value == "null")
        {
            ShowLayer("projectDiv", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("projectDiv", "none");

        //  Check That Date From Is Entered
        if (document.forms["report_project"].date_from.value == "")
        {
            ShowLayer("dateFrom", "block");
            valid                                                       = 0;
        }
        //  Check That Entered Date From Is Valid
        else
        {
            ShowLayer("dateFrom", "none");

            if (!validation("date", document.forms["report_project"].date_from.value))
            {
                ShowLayer("dateFromDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("dateFromDiv", "none");
        }

        //  Check That Date To Is Entered
        if (document.forms["report_project"].date_to.value == "")
        {
            ShowLayer("dateTo", "block");
            valid                                                       = 0;
        }
        //  Check That Entered Date To Is Valid
        else
        {
            ShowLayer("dateTo", "none");

            if (!validation("date", document.forms["report_project"].date_to.value))
            {
                ShowLayer("dateToDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("dateToDiv", "none");
        }

        if (valid == 1)
        {
            document.forms["report_project"].save.value                 = 1;
            document.forms["report_project"].submit();
        }
    }

    ///////////////////////////
    //  Sajax
    <?php sajax_show_javascript(); ?>

    function ClearOptions(OptionList) {
        for (x = OptionList.length; x >= 0; x--)
        {
            OptionList[x]                                               = null;
        }
    }

    function setProjects(data) {
        var c                                                           = 0;

        document.forms["report_project"].project.options[(c++)] = new Option("--  Select A Project  --", "null");

        for (var i in data)
            eval("document.forms['report_project'].project.options["+(c++)+"] = new Option('" + data[i][1] + "', '" + data[i][0] + " ');");
    }

    function getProject(select) {
        ClearOptions(document.forms["report_project"].project);

        x_getProjects(select.value, setProjects);
    }
    ///////////////////////////
</script>
<?php
    $projectTypes = q("SELECT id, type FROM ProjectTypes WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY type");
    $projectTypes[] = array("0","Shared Projects");
    $projectTypes = sortArrayByColumn($projectTypes, 1);
    
	$projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) ".
		"WHERE cot.company_id = '".$_SESSION["company_id"]."' AND p.completed = '0' AND p.deleted = '0' ORDER BY UPPER(p.name)");
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="report_project">
                    <div id="" style="<?php if ($generated == "1") echo "display: none;"; else echo "display: block"; ?>">
                        <table width="100%">
                            <tr>
                                <td class="centerdata">
                                    <h6>Project Report</h6>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Project Type:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="projectType" onChange="getProject(projectType);">
                                        <option value="null">All Project Types</option>
                                        <?php
                                            if (is_array($projectTypes))
                                                foreach ($projectTypes as $projectType)
                                                    if ($_POST["projectType"] == $projectType[0])
                                                        echo "<option value='".$projectType[0]."' selected>".$projectType[1]."</option>";
                                                    else
                                                        echo "<option value='".$projectType[0]."'>".$projectType[1]."</option>";
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Project:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" id="project" name="project">
                                        <option value="null">--  Select A Project  --</option>
                                        <?php
                                            if (is_array($projects))
                                                foreach ($projects as $project)
                                                    if ($_POST["project"] == $project[0])
                                                        echo "<option value='".$project[0]."' selected>".$project[1]."</option>";
                                                    else
                                                        echo "<option value='".$project[0]."'>".$project[1]."</option>";
                                        ?>
                                    </select>
                                    <div id="projectDiv" style="display: none;"><font class="on-validate-error">* Project must be selected</font></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Date From:
                                </td>
                                <td width="50%">
                                    <input class="on-field-date" id="date_from" name="date_from" type="text" style="text-align:right;" value="<?php
                                        if ($_POST["date_from"] != "") echo "".$_POST["date_from"]; else echo "".getMonthStart($today); ?>">
                                    <div id="dateFrom" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                    <div id="dateFromDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Date To:
                                </td>
                                <td width="50%">
                                    <input class="on-field-date" id="date_to" name="date_to" type="text" style="text-align:right;" value="<?php
                                        if ($_POST["date_to"] != "") echo "".$_POST["date_to"]; else echo "".getMonthEnd($today); ?>">
                                    <div id="dateTo" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                    <div id="dateToDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <input name="btnGenerate" onClick="check();" type="button" value="Generate Report">
                        <input method="post" name="save" type="hidden" value="0" />
                    </div>
                </form>
                <div id="report" style="<?php if ($generated == "1") echo "display: block;"; else echo "display: none"; ?>">
                    <?php
                        echo "<table class='on-table-center on-table'>";
                            echo "".$displayString;
                            echo  "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        echo "</table>";
                        echo "<br/>";

                        ///////////////////////////
                        //  Set Export Information
                        $_SESSION["fileName"] = "Project Report";
                        $_SESSION["fileData"] = array_merge($excelheadings, $exceldata);
                        ///////////////////////////

                        echo "<input name='btnExport' onClick=\"location.href='_export.php'\" type='button' value='Export to Excel' />";
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
