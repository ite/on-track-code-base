<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

	//if (!hasAccess("VEH_MANAGE"))
	//	header("Location: noaccess.php");
    
    //  Get Contact Information
    //  Contacts   
    $contact_id = $_GET["id"];
    $clientID = $_GET["cid"];
    $contact                                                           = q("SELECT id, client_id, firstName, lastName, email, officeNum, phoneNum FROM Contact 
                                                                                    WHERE company_id = '".$_SESSION["company_id"]."' 
                                                                                    AND deleted = '0' AND id='".$contact_id."' ");
    //  Insert Contact Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")	{
		include("InvoiceEngine.php");
        $ie_info = q("SELECT invoice_ID, invoice_UN, invoice_PW FROM Company WHERE id = '".$_SESSION["company_id"]."'");
		$ie = new InvoiceEngine($ie_info[0][0],$ie_info[0][1],$ie_info[0][2]); //TODO: get from db


        $errorMessage                                                   = "";

        //  Get Information
        $client_id                                                     = addslashes(strip_tags($_POST["client"]));
        $first_name                                                     = addslashes(strip_tags($_POST["first_name"]));
        $last_name                                                      = addslashes(strip_tags($_POST["last_name"]));
        $email_                                                            = addslashes(strip_tags($_POST["email_"]));
        $officeNum_                                                    = addslashes(strip_tags($_POST["officeNum_"]));
        $phoneNum_                                                   = addslashes(strip_tags($_POST["phoneNum_"]));
        
		$external_id = q("SELECT external_id FROM Contact WHERE company_id = '".$_SESSION["company_id"]."' AND id='".$contact_id."' ");
		$client_external_id = q("SELECT external_id FROM Client WHERE id = '$client_id'");

		//API call
		$ie->people_update($client_external_id, $external_id, $first_name, $last_name, $email_, $officeNum_, $phoneNum_);

        $update = q("UPDATE Contact SET client_id = '".$client_id."', firstName='".$first_name."', lastName='".$last_name."', 
               email='".$email_."', officeNum='".$officeNum_."', phoneNum='".$phoneNum_."' WHERE id='".$contact_id."' ");

        if($update)
            $errorMessage                                               = "";
        else
            $errorMessage                                               = "Update Error";

        if ($errorMessage == "")    {    
            $time                                                       = date("H:i:s");
            $logs                                                       = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                                    "VALUES ('Contact edited', 'Update', 'Contact', '".$_SESSION["email"]."', '$today', ".
                                                                                    "'$time', '".$_SESSION["company_id"]."')");

            $errorMessage                                               = "Contact Updated Successfully";

            header("Location: contacts.php?id=".$client_id."");
        }
    }

    if ($errorMessage != "")    {
        echo "<p align='center' style='padding:0px;'><strong><font class='on-validate-error'>$errorMessage</font></strong></p>";
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0", "vehicles");
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    jQuery(function()    {
        jQuery(function()    {
        //  Client Change - Show/Hide
        jQuery("#client").change(function()    {
            if(jQuery("#client").val() == "null") {
                jQuery("#divClient").hide();
            }else   {
                jQuery("#divClient").show();
            }
        });
    });
    });
    function check()
    {
        var valid                                                       = 1;

        //  Check That First Name Name Is Entered
        if (document.forms["contact_add"].first_name.value == "")   {
            ShowLayer("firstName", "block");
            valid                                                       = 0;
        }else   {
            ShowLayer("firstName", "none");
        }
        //  Check That Last Name Is Entered
        if (document.forms["contact_add"].last_name.value == "") {
            ShowLayer("lastName", "block");
            valid                                                       = 0;
        }else   {
            ShowLayer("lastName", "none");
        }
        //  Check That Email Is Entered
        if (document.forms["contact_add"].email_.value == "") {
            ShowLayer("email", "block");
            valid                                                       = 0;
        }else   {
            ShowLayer("email", "none");
        }
        //  Check That Office Number Is Entered
        if (document.forms["contact_add"].officeNum_.value == "") {
            ShowLayer("officeNum", "block");
            valid                                                       = 0;
        }else   {
            ShowLayer("officeNum", "none");
        }
        //  Check That Phone Number Is Entered
        if (document.forms["contact_add"].phoneNum_.value == "") {
            ShowLayer("phoneNum", "block");
            valid                                                       = 0;
        }else   {
            ShowLayer("phoneNum", "none");
        }

        if (valid == 1)
        {
            document.forms["contact_add"].save.value                    = 1;
            document.forms["contact_add"].submit();
        }
    }
</script>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata">
                <form action="" method="post" name="contact_add">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>
                                    Edit Contact
                                </h6>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <br/>
                            </td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                           <td class="on-description" width="50%">Client</td>
                            <td width="50%">
                                <select id="client" name="client" method="post" class="on-field required">
                                    <option value="null">--  Select A Client  --</option>
                                    <?php
                                        $contacts = q("SELECT id, companyName FROM Client WHERE company_id = '".$_SESSION["company_id"]."' AND deleted='0' ORDER BY companyName");
                                        if (is_array($contacts))    {
                                            foreach ($contacts as $c) {
                                                if($c[0] == $clientID )   {
                                                    echo "<option value='".$c[0]."' selected>".$c[1]."</option>";
                                                }else   {
                                                    echo "<option value='".$c[0]."'>".$c[1]."</option>";
                                                }
                                            }
                                        }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <?php 
                        if (!is_array($contacts))    {
                            echo "<tr>";
                                echo "<td class='on-description' width='50%'></td>";
                                echo "<td style='width:50%; padding-left:30px;'>";
                                    echo "<a style='color:orange;'> - No Clients added yet - </a>";
                                echo "</td>";
                            echo "</tr>";
                        }
                        ?>
                    </table>
                    
                    <div id="divClient" style="display: block;">
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                        First Name
                                </td>
                                <td width="50%">
                                    <?php echo "<input class='on-field' name='first_name' tabindex='1' size='60' type='text' value='".$contact[0][2]."'>"; ?>
                                    <div id="firstName" style="display: none;"><font class="on-validate-error">* Name must be entered</font></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Last Name
                                </td>
                                <td width="50%">
                                    <?php echo "<input class='on-field' name='last_name' tabindex='2' size='60' type='text' value='".$contact[0][3]."'>"; ?>
                                    <div id="lastName" style="display: none;"><font class="on-validate-error">* Last Name must be entered</font></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Email
                                </td>
                                <td width="50%">
                                    <?php echo "<input class='on-field' name='email_' tabindex='3' size='60' type='text' value='".$contact[0][4]."'>"; ?>
                                    <div id="email" style="display: none;"><font class="on-validate-error">* Email address must be entered</font></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Office Number
                                </td>
                                <td width="50%">
                                    <?php echo "<input class='on-field' name='officeNum_' tabindex='4' size='60' type='text' value='".$contact[0][5]."'>"; ?>
                                    <div id="officeNum" style="display: none;"><font class="on-validate-error">* Office Number must be entered</font></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Phone Number
                                </td>
                                <td width="50%">
                                    <?php echo "<input class='on-field' name='phoneNum_' tabindex='5' size='60' type='text' value='".$contact[0][6]."'>"; ?>
                                    <div id="phoneNum" style="display: none;"><font class="on-validate-error">* Phone Number Code must be entered</font></div>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <input name="btnAdd" onClick="check();" type="button" value="Update Contact">
                        <input method="post" name="save" type="hidden" value="0" />
                    </div>
                    
                </form>
            </td>
        </tr>
        <tr>
            <td>
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
