<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

	if (!hasAccess("ATYPE_MANAGE"))
		header("Location: noaccess.php");

    $display_link                                                       = "activity_type_management.php?alphabet";

    //  Set Alphabet Letter
    $alphabet                                                           = "";
    $alphabet                                                           = $_GET["alphabet"];

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("1", "activity_types");
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="activity_type_management">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Activity Type List</h6>
                            </td>
                        </tr>
                    </table>
                    <?php
                        echo "<br/>";
                        echo "| <a href='$display_link='>View All Activity Types</a> |";
                        echo "<br/><br/>";
                        echo "<a href='$display_link=A'>A</a> | <a href='$display_link=B'>B</a> | ".
                            "<a href='$display_link=C'>C</a> | <a href='$display_link=D'>D</a> | ".
                            "<a href='$display_link=E'>E</a> | <a href='$display_link=F'>F</a> | ".
                            "<a href='$display_link=G'>G</a> | <a href='$display_link=H'>H</a> | ".
                            "<a href='$display_link=I'>I</a> | <a href='$display_link=J'>J</a> | ".
                            "<a href='$display_link=K'>K</a> | <a href='$display_link=L'>L</a> | ".
                            "<a href='$display_link=M'>M</a> | <a href='$display_link=N'>N</a> | ".
                            "<a href='$display_link=O'>O</a> | <a href='$display_link=P'>P</a> | ".
                            "<a href='$display_link=Q'>Q</a> | <a href='$display_link=R'>R</a> | ".
                            "<a href='$display_link=S'>S</a> | <a href='$display_link=T'>T</a> | ".
                            "<a href='$display_link=U'>U</a> | <a href='$display_link=V'>V</a> | ".
                            "<a href='$display_link=W'>W</a> | <a href='$display_link=X'>X</a> | ".
                            "<a href='$display_link=Y'>Y</a> | <a href='$display_link=Z'>Z</a>";
                    ?>
                    <br/><br/>
                    <input name="btnAdd" onClick="location.href='activity_add.php';" tabindex="1" type="button" value="Add New Activity Type">
                    <br/><br/>
                    <!--  Headings   -->
                    <table width="50%" class="on-table-center on-table">
                        <tr>
                            <th>Activity Type</th>
                            <th>Activity</th>
                        </tr>
                        <!--  Table Information   -->
                        <?php
                            //  nActivityTypes                          = Number of Activity Types
                            $nActivityTypes                             = q("SELECT COUNT(id) FROM ActivityTypes WHERE company_id = '".$_SESSION["company_id"]."' ".
                                                                            "AND type LIKE '$alphabet%' AND active = '1'");
                            $activityTypes                              = q("SELECT id, type FROM ActivityTypes WHERE company_id = '".$_SESSION["company_id"]."' ".
                                                                            "AND type LIKE '$alphabet%' AND active = '1' ORDER BY type");

                            if ($nActivityTypes > 1)
                            {
                                foreach ($activityTypes as $activityType)
                                {
                                    //  nActivities                     = Number of Activities per Type
                                    $nActivities                        = q("SELECT COUNT(id) FROM Activities WHERE parent_id = '".$activityType[0]."'");
                                    $activities                         = q("SELECT id, name FROM Activities WHERE parent_id = '".$activityType[0]."' ".
                                                                            "ORDER BY name");

                                    echo "<tr>";
                                        echo "<td>";
                                            echo "<a href='activity_edit.php?id=".$activityType[0]."'>".$activityType[1]."</a>";
                                        echo "</td>";
                                        if ($nActivities > 1)
                                        {
                                            echo "<td>";
                                                echo "<table width='100%'>";
                                                    foreach ($activities as $activity)
                                                    {
                                                        echo "<tr>";
                                                            echo "<td class='noborder'>";
                                                                echo $activity[1];
                                                            echo "</td>";
                                                        echo "</tr>";
                                                    }
                                                echo "</table>";
                                            echo "</td>";
                                        }
                                        else if ($nActivities == 1)
                                        {
                                            echo "<td>";
                                                echo "<table width='100%'>";
                                                     echo "<tr>";
                                                            echo "<td class='noborder'>";
                                                                echo $activities[0][1];
                                                              echo "</td>";
                                                        echo "</tr>";
                                                     echo "</table>";
                                            echo "</td>";
                                        }
                                        else
                                        {
                                            echo "<td>";
                                                echo "-";
                                            echo "</td>";
                                        }
                                    echo "</tr>";
                                }
                            }
                            else if ($nActivityTypes == 1)
                            {
                                echo "<tr>";
                                    //  nActivities                     = Number of Activities per Type
                                    $nActivities                        = q("SELECT COUNT(id) FROM Activities WHERE parent_id = '".$activityTypes[0][0]."'");
                                    $activities                         = q("SELECT id, name FROM Activities WHERE parent_id = '".$activityTypes[0][0]."' ".
                                                                            "ORDER BY name");

                                    echo "<td>";
                                        echo "<a href='activity_edit.php?id=".$activityTypes[0][0]."'>".$activityTypes[0][1]."</a>";
                                    echo "</td>";
                                    if ($nActivities > 1)
                                    {
                                        echo "<td>";
                                            echo "<table width='100%'>";
                                                foreach ($activities as $activity)
                                                {
                                                    echo "<tr>";
                                                        echo "<td>";
                                                            echo $activity[1];
                                                        echo "</td>";
                                                    echo "</tr>";
                                                }
                                            echo "</table>";
                                        echo "</td>";
                                    }
                                    else if ($nActivities == 1)
                                    {
                                        echo "<td>";
                                            echo $activities[0][1];
                                        echo "</td>";
                                    }
                                    else
                                    {
                                        echo "<td>";
                                            echo "-";
                                        echo "</td>";
                                    }
                                echo "</tr>";
                            }
                            else
                            {
                                echo "<tr>";
                                    echo "<td align='center' colspan='2'>";
                                        if ($alphabet == "")
                                            echo "No activity types available";
                                        else
                                            echo "No activity types available under $alphabet";
                                    echo "</td>";
                                echo "</tr>";
                            }
                            echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        ?>
                    </table>
                    <br/>
                    <input name="btnAdd" onClick="location.href='activity_add.php';" tabindex="1" type="button" value="Add New Activity Type">
                </form>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
