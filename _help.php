<?php
    include("_db.php");
    include("_dates.php");
    include("_functions.php");
?>
<html>
    <head>
        <title>
            On-Track - Help Script...
        </title>
    </head>
    <body>
        <h1 align="center">
            On-Track - Help Script...
        </h1>
        <?php
            //$alter = q("ALTER TABLE seen ADD COLUMN blog TINYINT UNSIGNED NOT NULL DEFAULT 0");

            $drop = q("DROP TABLE IF EXISTS help");
            $create = q("CREATE TABLE help (
                                  id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
                                  page VARCHAR(255),INDEX(page),
                                  heading VARCHAR(255),
                                  content TEXT)");

            $insert = q("INSERT INTO help (page,heading,content) VALUES ".
                            "('bookings.php','Frequently Asked Questions','Q: Why does some of my booked time/expenses appear to be grayed out?<br/>
A: Time/Expenses that has been approved for invoicing are no longer editable. Contact your Manager if you need to make changes to this.<br/>
<br/>
Q: Why don\'t I see the time/expenses I booked last week?<br/>
A: Only time/expenses booked in the current week is displayed on this page. Click on \"Bookings\" and \"Manage Bookings\" for more comprehensive reporting on booked time/expenses.<br/>'),".
                            "('book_time.php','Frequently Asked Questions','Q: Why does some of my booked time appear to be grayed out?<br/>
A: Time that has been approved for invoicing are no longer editable. Contact your Manager if you need to make changes to this.<br/>
<br/>
Q: Why don\'t I see the time I booked last week?<br/>
A: Only time booked in the current week is displayed on this page. Click on \"Bookings\" and \"Manage Bookings\" for more comprehensive reporting on booked time.<br/>'),".
                            "('book_expense.php','Frequently Asked Questions','Q: Why does some of my booked expenses appear to be grayed out?<br/>
A: Expenses that have been approved for invoicing are no longer editable. Contact your Manager if you need to make changes to this.<br/>
<br/>
Q: Why don\'t I see the time I booked last week?<br/>
A: Only expenses booked in the current week is displayed on this page. Click on \"Bookings\" and \"Manage Bookings\" for more comprehensive reporting on booked expenses.<br/>'),".
                            "('projects.php','Frequently Asked Questions','Q: When is a project seen as invoicable?<br/>
A: A project is seen as invoicable when the project has a total budget.<br/>
<br/>
Q: When is a project seen as non-invoicable?<br/>
A: A project is seen as non-invoicable when the project has no total budget.<br/>'),".
                            "('report_progress.php','Frequently Asked Questions','Q: When is a project seen as active?<br/>
A: A project is seen as active when the project has a total budget.<br/>
<br/>
Q: When is a project seen as non-invoicable?<br/>
A: A project is seen as non-invoicable when the project has no total budget.<br/>'),".
                            "('report_invoiced_vs_cost_to_company.php','Invoiced vs. Cost to Company Report','This report shows the productivity and efficiency of the employees.<br/>
The amount of time that is spent on invoiceable projects are compared with the cost of an employee to the company.<br/>
This report can be drawn per employee or for all employees for a specific timeframe.<br/><br/>
<i>x = total number of invoiceable projects</i><br/>
<i>n = project id</i><br/><br/>
<b>For defined period per employee:</b>
<table cellspacing=\'0\' class=\'math\'>
<tr align=\'center\'>
	<td style=\'vertical-align:middle;\'>Invoiced<big>&nbsp;</big>=&nbsp;<br></td>
	<td style=\'vertical-align:middle\'>
            <small>&nbsp;x&nbsp;</small><br>
            <big><big>&#8721;</big></big><br>
            <small>n<small>&nbsp;</small>=<small>&nbsp;</small>1</small>
        </td>
	<td style=\'vertical-align:middle\'>[time worked(n)][rate(n)]<br></td>
</tr>
</table>
<table cellspacing=\'0\' class=\'math\'>
<tr align=\'center\'>
	<td style=\'vertical-align:middle;\'>Cost to Company<big>&nbsp;</big>=&nbsp;<br></td>
	<td style=\'vertical-align:middle\'>
            <small>&nbsp;x&nbsp;</small><br>
            <big><big>&#8721;</big></big><br>
            <small>n<small>&nbsp;</small>=<small>&nbsp;</small>1</small>
        </td>
	<td style=\'vertical-align:middle\'>[time worked(n)][Cost to company rate]<br></td>
</tr>
</table>')
                        ");
        ?>
        <form action="index.php" method="post">
            <center><input type="submit" value="Return"/></center>
        </form>
    </body>
</html>
