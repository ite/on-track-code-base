<?php
    include("_db.php");
    include("_dates.php");
?>
<html>
    <head>
        <title>
            On-Track - Recovery Script...
        </title>
    </head>
    <body>
        <h1 align="center">
            Install
        </h1>
        <?php
            $date = date("Y-m-d H:i:s");
            $user = "admin";

            $alter = q("ALTER TABLE Budget CHANGE id id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT");

            $companies = q("SELECT id, name FROM Company ORDER BY name");

            if (is_array($companies))   {
                foreach ($companies as $c)      {
                    $usesBudget = (is_array(q("SELECT * FROM Budget WHERE company_id = '".$c[0]."'"))) ? 1 : 0;

                    if ($usesBudget)    {
                        $yearMonth = q("SELECT DISTINCT(CONCAT(year, '-', month)), year, month FROM Budget WHERE company_id = '".$c[0]."' ORDER BY CONCAT(year, '-', month) ASC");

                        if (is_array($yearMonth))       {
                            foreach ($yearMonth as $yM) {
                                $categories = q("SELECT DISTINCT(c.id), c.name FROM (Category AS c INNER JOIN Elements AS e ON c.id = e.parent_id INNER JOIN Budget AS b ON e.id = b.element_id) ".
                                                "WHERE (c.company_id = '".$c[0]."' AND e.status = '1') OR (b.year = '".$yM[1]."' AND b.month = '".$yM[2]."' AND b.company_id = '".$c[0]."') ORDER BY c.name");

                                if (is_array($categories)) {
                                    foreach ($categories as $cat) {
                                        $elements = q("SELECT id, name FROM Elements WHERE parent_id = '".$cat[0]."' AND status = '1' ORDER BY name");

                                        if (is_array($elements))        {
                                            foreach ($elements as $el)  {
                                                if (!exist("Budget", "year = '".$yM[1]."' AND month = '".$yM[2]."' AND company_id = '".$c[0]."' AND element_id = '".$el[0]."'"))
                                                    $insert = q("INSERT INTO Budget (year, month, element_id, planned, amount, actual, due_date, company_id) ".
                                                                "VALUES ('".$yM[1]."', '".$yM[2]."', '".$el[0]."', '', '', '0', '', '".$c[0]."')");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            echo "<p align='center'>Script completed successfully</p>";
        ?>
        <form action="index.php" method="post">
            <center><input type="submit" value="Return"/></center>
        </form>
    </body>
</html>
