<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

	if (!hasAccess("PROJ_MANAGE"))
		header("Location: noaccess.php");

    //  Get Project Info From Array
    $projectInfo                                                        = array();
    $projectInfo                                                        = $_SESSION["projectInfo"];

    if (is_array($projectInfo))
        $projectManager                                                 = $projectInfo[3];
    else
        $projectManager                                                 = $projectInfo;

    //  Insert/Update Project Team Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")
    {
        $array                                                          = split("_", $_POST["checkbox"]);

        if ($arr[0][0])
            $array                                                      = array_slice($array, 0, count($array) - 1);

        if ($array[0][0]) {
            foreach ($array as $a) {
                if ($_POST["box".$a]) {
                    $employee_info                                      = q("SELECT frstname, lstname FROM Employee WHERE id = '$a'");
                    $tariff_id                                          = $_POST["user_tariff".$a];

		    if ($tariff_id == 0)
			$tariff_id					= 1;

                    $exist                                              = q("SELECT id FROM Project_User WHERE user_id = '$a' ".
                                                                            "AND project_id = '".$_SESSION["project_id"]."'");

                    if (!$exist)
                        $insert                                         = q("INSERT INTO Project_User (user_id, user_tariff, project_id, manager) ".
                                                                            "VALUES ('$a', '$tariff_id', '".$_SESSION["project_id"]."', '0')");
                    else
                        $update                                         = q("UPDATE Project_User SET user_tariff = '$tariff_id', manager = '0' ".
                                                                            "WHERE user_id = '$a' AND project_id = '".$_SESSION["project_id"]."'");

                    if ($insert) {
                        $time                                           = date("H:i:s");

                        $logs                                           = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('".$employee_info[0][0]." ".$employee_info[0][1]." added to project team', ".
                                                                            "'Insert', 'Project_User', '".$_SESSION["email"]."', '$today', '$time', ".
                                                                            "'".$_SESSION["company_id"]."')");
                    }
                    else if ($update) {
                        $time                                           = date("H:i:s");

                        $logs                                           = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('".$employee_info[0][0]." ".$employee_info[0][1]." information changed at team', ".
                                                                            "'Update', 'Project_User', '".$_SESSION["email"]."', '$today', '$time', ".
                                                                            "'".$_SESSION["company_id"]."')");
                    }
                }
                else {
                    $employee_info                                      = q("SELECT frstname, lstname FROM Employee WHERE id = '$a'");

                    $exist                                              = q("SELECT id FROM Project_User WHERE user_id = '$a' ".
                                                                            "AND project_id = '".$_SESSION["project_id"]."'");

                    if ($exist)
                        $delete                                         = q("DELETE FROM Project_User WHERE user_id = '$a' AND ".
                                                                            "project_id = '".$_SESSION["project_id"]."'");

                    if ($delete) {
                        $time                                           = date("H:i:s");

                        $logs                                           = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('".$employee_info[0][0]." ".$employee_info[0][1]." removed from project team', ".
                                                                            "'Delete', 'Project_User', '".$_SESSION["email"]."', '$today', '$time', ".
                                                                            "'".$_SESSION["company_id"]."')");
                    }
                }
            }
        }

        header($_SESSION["location"]);
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0", "projects");

    //  nEmployees                                                      = Number of Employees
    $nEmployees                                                         = q("SELECT COUNT(e.id) FROM (Employee AS e INNER JOIN Company_Users ".
                                                                            "AS cu ON e.id = cu.user_id) WHERE cu.company_id = '".$_SESSION["company_id"]."' ".
                                                                            "AND e.email != 'admin' AND e.id != '$projectManager' AND e.deleted = '0'");
	//TODO: what does usertype do here?
    $employees                                                          = q("SELECT e.id, e.lstname, e.frstname FROM (Employee AS e INNER JOIN Company_Users ".
                                                                            "AS cu ON e.id = cu.user_id) WHERE cu.company_id = '".$_SESSION["company_id"]."' ".
                                                                            "AND e.email != 'admin' AND e.id != '$projectManager' AND e.deleted = '0' ".
                                                                            "ORDER BY e.lstname, frstname");
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    function create()
    {
        document.forms["project_team"].save.value                       = 1;
        document.forms["project_team"].submit();
    }
</script>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="project_team">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Project Team</h6>
                            </td>
                        </tr>
                    </table>
                    <br/><br/>
                    <input name="btnCreate" onClick="create();" type="button" value="Create/Update Team">
                    <br/>
                    <br/>
                    <!--  Headings   -->
                    <table class="on-table-center on-table">
                        <tr>
                            <th>Employee Name</th>
                            <th>Tariff Rate</th>
                            <th>Team Member</th>
                        </tr>
                        <!--  Table Information   -->
                        <?php
                            $checkbox                                   = "";

                            if (is_array($employees)) {
                                foreach ($employees as $employee) {
                                    echo "<tr>";
                                        echo "<td>";
                                            echo "".$employee[1].", ".$employee[2];
                                        echo "</td>";
                                        echo "<td class='centerdata'>";
                                            echo "<select name='user_tariff".$employee[0]."' style='width:100%;' tabindex='1'>";
												echo "<option value='0'></option>";

                                                $tariffs                = q("SELECT tariff1, tariff2, tariff3, tariff4, tariff5 FROM Employee ".
                                                                            "WHERE id = '".$employee[0]."'");
                                                $tariff_id              = q("SELECT user_tariff FROM Project_User WHERE user_id = '".$employee[0]."' ".
                                                                            "AND project_id = '".$_SESSION["project_id"]."'");

                                                if ($tariffs[0][0] > 0)
                                                    if ($tariff_id == 1)
                                                        echo "<option value='1' selected>".$tariffs[0][0]."</option>";
                                                    else
                                                        echo "<option value='1'>".$tariffs[0][0]."</option>";

                                                if ($tariffs[0][1] > 0)
                                                    if ($tariff_id == 2)
                                                        echo "<option value='2' selected>".$tariffs[0][1]."</option>";
                                                    else
                                                        echo "<option value='2'>".$tariffs[0][1]."</option>";

                                                if ($tariffs[0][2] > 0)
                                                    if ($tariff_id == 3)
                                                        echo "<option value='3' selected>".$tariffs[0][2]."</option>";
                                                    else
                                                        echo "<option value='3'>".$tariffs[0][2]."</option>";

                                                if ($tariffs[0][3] > 0)
                                                    if ($tariff_id == 4)
                                                        echo "<option value='4' selected>".$tariffs[0][3]."</option>";
                                                    else
                                                        echo "<option value='4'>".$tariffs[0][3]."</option>";

                                                if ($tariffs[0][4] > 0)
                                                    if ($tariff_id == 5)
                                                        echo "<option value='5' selected>".$tariffs[0][4]."</option>";
                                                    else
                                                        echo "<option value='5'>".$tariffs[0][4]."</option>";
                                            echo "</select>";
                                        echo "</td>";
                                        echo "<td class='centerdata'>";
                                            $checkbox                   .= "".$employee[0]."_";

                                            $is_member                  = q("SELECT id FROM Project_User WHERE user_id = '".$employee[0]."' ".
                                                                            "AND project_id = '".$_SESSION["project_id"]."'");

                                            if ($is_member)
                                                echo "<input name='box".$employee[0]."' tabindex='1' type='checkbox' value='1' checked>";
                                            else
                                                echo "<input name='box".$employee[0]."' tabindex='1' type='checkbox' value='1'>";
                                        echo "</td>";
                                    echo "</tr>";
                                }
                            }
                            else
                            {
                                echo "<tr>";
                                    echo "<td class='centerdata' colspan='3' style='".$left.$bottom.$right."'>";
                                        echo "No Employees available";
                                    echo "</td>";
                                echo "</tr>";
                            }
                            echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        ?>
                    </table>
                    <?php
                        echo "<input name='checkbox' type='hidden' value='".$checkbox."'>";
                    ?>
                    <br/>
                    <input name="btnCreate" onClick="create();" type="button" value="Create/Update Team">
                    <input method="post" name="save" type="hidden" value="0" />
                </form>
            </td>
        </tr>
        <tr>
            <td class="centerdata">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
