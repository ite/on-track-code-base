<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("include/sajax.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");
        
    if (!hasAccess("REP_LOADED_INV"))
        header("Location: noaccess.php");

    ///////////////////////////
    //  Sajax
    function getCompanies($id)  {
       return q("SELECT DISTINCT(c.id), c.name FROM (Company AS c INNER JOIN companiesOnTeam AS cot ON c.id = cot.company_id 
                        INNER JOIN Project AS p ON p.id =  cot.project_id) WHERE cot.project_id = '$id' ORDER BY c.name"); 
    }
    
    function getAreas($id){
        $areas = q("SELECT id, name FROM areas WHERE pID='$id' ORDER BY name");
        return $areas ;
    }

    $sajax_request_type = "GET";
    sajax_init();
    sajax_export("getCompanies");    
    sajax_export("getAreas");
    sajax_handle_client_request();
    ///////////////////////////

    //  Set Report Generated Status
    $generated = "0";

    //  Create Display Information Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")        {
        unset($exceldata);

        $errorMessage = "";

        $project_id = $_POST["project"];
        $company_id = $_POST["company"];
        $area_id = $_POST["area"];
        $invoiceType = $_POST["invoiceType"];
        $viewType = $_POST["viewType"];

        if ($viewType == "between")     {
            $date_from = addslashes(strip_tags($_POST["date_from"]));
            $date_to = addslashes(strip_tags($_POST["date_to"]));
        }

        $where = "";

        if ($viewType == "between")     $where .= "AND li.create_date >= '$date_from' AND li.create_date <= '$date_to' ";
        if ($invoiceType != "all")      $where .= "AND li.paid = '$invoiceType' ";

        if ($project_id != "null" && is_numeric($project_id))   $where .= "AND li.project_id = '$project_id' ";
        if ($company_id != "null" && is_numeric($company_id))   $where .= "AND li.company_id = '$company_id' ";
        if ($area_id != "null" && is_numeric($area_id))         $where .= "AND li.area_id = '$area_id' ";

        $where = ($where != "") ? "WHERE ".substr($where, 3) : "";

        $hasAreas = 0;

        $areas  = q("SELECT li.id,li.id 
                        FROM (LoadInvoices AS li 
                            INNER JOIN Project AS p ON p.id = li.project_id 
                            INNER JOIN companiesOnTeam AS cot ON p.id = cot.project_id 
                            INNER JOIN Company AS c ON c.id = li.company_id 
                            LEFT JOIN areas AS ar ON ar.id = li.area_id)
                        $where AND li.area_id > 0");

        if (is_array($areas))    $hasAreas = 1;

        $select = "";

        if ($hasAreas)  {
            $select .= "ar.name,";
            $headings = array("Project Name","Area","Company Name","Invoice Name","Date<br/>Created","Total <i>(".$_SESSION["currency"].")</i>","Consulting<br/><i>(".$_SESSION["currency"].")</i>",
                                "Expenses<br/><i>(".$_SESSION["currency"].")</i>","Diverse<br/>Income<br/><i>(".$_SESSION["currency"].")</i>","Due Date","Paid");
        } else  $headings = array("Project Name","Company Name","Invoice Name","Date<br/>Created","Total <i>(".$_SESSION["currency"].")</i>","Consulting<br/><i>(".$_SESSION["currency"].")</i>",
                                    "Expenses<br/><i>(".$_SESSION["currency"].")</i>","Diverse<br/>Income<br/><i>(".$_SESSION["currency"].")</i>","Due Date","Paid");

        $invoices = q("SELECT p.name,".$select."c.name,li.name,li.create_date,SUM(li.consulting + li.expenses + ((li.consulting + li.expenses) * (li.vat / 100))),li.consulting,li.expenses,li.diverse_income,li.due_date,li.paid 
                        FROM (LoadInvoices AS li 
                            INNER JOIN Project AS p ON p.id = li.project_id 
                            INNER JOIN companiesOnTeam AS cot ON p.id = cot.project_id 
                            INNER JOIN Company AS c ON c.id = li.company_id 
                            LEFT JOIN areas AS ar ON ar.id = li.area_id)
                        $where ORDER BY li.due_date, c.name, p.name");

        if (is_array($invoices))     {
            $columns = count($headings) - 1;

            $displayString = "";
            $row = 0;

            $total = 0;
            $consulting = 0;
            $expenses = 0;
            $diverse = 0;

            //  Headings
            //  HTML Display
            $displayString .= "<tr>";

            foreach ($headings as $h)   $displayString .= "<th>".$h."</th>";

            $displayString .= "</tr>";

            //  EXCEL DISPLAY
            foreach ($headings as $h)   $exceldata[$row][] = strip_tags($h);

            $row++;

            //  Data
            foreach ($invoices as $r)        {
                $col = 0;
                $displayString .= "<tr>";

                foreach ($r as $d)      {
                    if ($headings[$col] == "Total <i>(".$_SESSION["currency"].")</i>")                  $total += $d;
                    if ($headings[$col] == "Consulting<br/><i>(".$_SESSION["currency"].")</i>")         $consulting += $d;
                    if ($headings[$col] == "Expenses<br/><i>(".$_SESSION["currency"].")</i>")           $expenses += $d;
                    if ($headings[$col] == "Diverse<br/>Income<br/><i>(".$_SESSION["currency"].")</i>") $diverse += $d;

                    if ($headings[$col] == "Paid")      {
                        if ($d == "1")  $displayString .= "<td style='white-space:nowrap' class='centerdata'><input disabled type='checkbox' value='1' checked></td>";
                        else            $displayString .= "<td style='white-space:nowrap' class='centerdata'><input disabled type='checkbox' value='1'</td>";

                        if ($d == "1")  $d = "Yes";
                        else            $d = "No";
                    }
                    else if (preg_match("/^((\d{2}(([02468][048])|([13579][26]))[\-\/\s]?((((0?[13578])|(1[02]))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-\/\s]?((0?[1-9])|([1-2][0-9])))))|(\d{2}(([02468][1235679])|([13579][01345789]))[\-\/\s]?((((0?[13578])|(1[02]))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-\/\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))(\s(((0?[1-9])|(1[0-2]))\:([0-5][0-9])((\s)|(\:([0-5][0-9])\s))([AM|PM|am|pm]{2,2})))?$/", $d))
                        $displayString .= "<td style='white-space:nowrap'>".$d."</td>";
                    else if (is_numeric($d))
                        $displayString .= "<td style='white-space:nowrap' class='rightdata'>".number_format($d, 2, ".", "")."</td>";
                    else if ($d == "")
                        $displayString .= "<td style='white-space:nowrap'>-</td>";
                    else
                        $displayString .= "<td style='white-space:nowrap'>".$d."</td>";

                    $exceldata[$row][] .= $d;

                    $col++;
                }

                $displayString .= "</tr>";
                $row++;
            }

            $colspan = ($hasAreas) ? 5 : 4;

            $displayString .= "<tr>
                                    <td class='on-table-total' colspan='".$colspan."'><a style='color:#FFFFFF;'>Grand Total <i>(".$_SESSION["currency"].")</a></td>
                                    <td class='on-table-total' class='rightdata'>".number_format($total, 2, ".", "")."</td>
                                    <td class='on-table-total' class='rightdata'>".number_format($consulting, 2, ".", "")."</td>
                                    <td class='on-table-total' class='rightdata'>".number_format($expenses, 2, ".", "")."</td>
                                    <td class='on-table-total' class='rightdata'>".number_format($diverse, 2, ".", "")."</td>
                                    <td class='on-table-total' colspan='2'></td>
                                </tr>";

            for ($a = 0; $a < $colspan - 1; $a++)       $exceldata[$row][] = "";

            $exceldata[$row][] .= "Total (".$_SESSION["currency"]."):";
            $exceldata[$row][] .= $total;
            $exceldata[$row][] .= $consulting;
            $exceldata[$row][] .= $expenses;
            $exceldata[$row][] .= $diverse;
            $exceldata[$row][] .= "";
            $exceldata[$row][] .= "";

            $row++;
        }

        if ($displayString != "")
            $generated = "1";
    } 

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("5", "reports");

    if ($errorMessage != "") {
        echo "<p align='center' style='padding:0px;'><strong><font class='on-validate-error'>$errorMessage</font></strong></p>";
        echo "<br/>";
    }
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    jQuery(function(){
	jQuery('#date_from').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_from').val(),
		current: jQuery('#date_from').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_from').val()))
			_date = new Date();
			else _date = jQuery('#date_from').val();
			jQuery('#date_from').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_from').val(formated);
			jQuery('#date_from').DatePickerHide();
		}
	});
    })

    jQuery(function(){
	jQuery('#date_to').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_to').val(),
		current: jQuery('#date_to').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_to').val()))
			_date = new Date();
			else _date = jQuery('#date_to').val();
			jQuery('#date_to').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_to').val(formated);
			jQuery('#date_to').DatePickerHide();
		}
	});
    })

    function check()
    {
        var valid                                                       = 1;

        //  Check That Employee Is Selected
        if (document.forms["report_loaded_invoices"].project.value == "null")
        {
            ShowLayer("projectDiv", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("projectDiv", "none");

        //  Check That Date From Is Entered
        if (document.forms["report_loaded_invoices"].date_from.value == "")
        {
            ShowLayer("dateFrom", "block");
            valid                                                       = 0;
        }
        //  Check That Entered Date From Is Valid
        else
        {
            ShowLayer("dateFrom", "none");

            if (!validation("date", document.forms["report_loaded_invoices"].date_from.value))
            {
                ShowLayer("dateFromDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("dateFromDiv", "none");
        }

        //  Check That Date To Is Entered
        if (document.forms["report_loaded_invoices"].date_to.value == "")
        {
            ShowLayer("dateTo", "block");
            valid                                                       = 0;
        }
        //  Check That Entered Date To Is Valid
        else
        {
            ShowLayer("dateTo", "none");

            if (!validation("date", document.forms["report_loaded_invoices"].date_to.value))
            {
                ShowLayer("dateToDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("dateToDiv", "none");
        }

        if (valid == 1)
        {
            document.forms["report_loaded_invoices"].save.value         = 1;
            document.forms["report_loaded_invoices"].submit();
        }
    }

    function viewSelection()
    {
        if (document.forms["report_loaded_invoices"].viewType.value == "between")
            ShowLayer("dateDisplay", "block");
        else
            ShowLayer("dateDisplay", "none");
    }

    ///////////////////////////
    //  Sajax
    <?php sajax_show_javascript(); ?>

    function ClearOptions(OptionList) {
        for (x = OptionList.length; x >= 0; x--) {
            OptionList[x] = null;
        }
    }
    
    // Set Company
    function setCompanies(data) {
        var c                                                           = 0;
        
        document.forms["report_loaded_invoices"].company.options[(c++)]                         = new Option("All Companies", "all");
        
        for (var i in data)
          eval("document.forms['report_loaded_invoices'].company.options["+(c++)+"] = new Option('" + data[i][1] + "', '" + data[i][0] + "');");
    }

    //Set Areas
    function setAreas(data) {
        if(!data)
            ShowLayer("areaDiv", "none");
        var c                                                           = 0;
        document.report_loaded_invoices.area.options[(c++)]                         = new Option("All Areas", "all");
        for (var i in data)
            eval("document.report_loaded_invoices.area.options["+(c++)+"] = new Option('" + data[i][1] + "', '" + data[i][0] + "');");
    }

    // Get Companies
    function getCompanies(select) {
        ClearOptions(document.report_loaded_invoices.company);
        if (select.value != "null")
        {
            ShowLayer("companyDiv", "block");
        }else{
            ShowLayer("companyDiv", "none");
        }
        x_getCompanies(select.value, setCompanies);
        
    }

    //Get Areas
    function getAreas(select) {
        ClearOptions(document.report_loaded_invoices.area);
        if (select.value != "null")
        {
            ShowLayer("areaDiv", "block");
            x_getAreas(select.value, setAreas);
        }
        else{
            ShowLayer("areaDiv", "none");
        }
    }
    ///////////////////////////
</script>
<?php
	$projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p 
                        INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) 
                        WHERE cot.company_id = '".$_SESSION["company_id"]."' 
                        AND p.completed = '0' 
                        AND p.deleted = '0' 
                        AND p.p_type='SP' ORDER BY UPPER(p.name)");
    ?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="report_loaded_invoices">
                    <div id="" style="<?php if ($generated == "1") echo "display: none;"; else echo "display: block"; ?>">
                        <table width="100%">
                            <tr>
                                <td class="centerdata">
                                    <h6>Loaded Invoice Report</h6>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Project:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" id="project" name="project" onChange="getCompanies(project);getAreas(project);">
                                        <option value="null">--  Select A Project  --</option>
                                        <?php
                                            if (is_array($projects))
                                                foreach ($projects as $project)
                                                    if (isset($_POST["project"]) && $_POST["project"] == $project[0])
                                                        echo "<option value='".$project[0]."' selected>".$project[1]."</option>";
                                                    else
                                                        echo "<option value='".$project[0]."'>".$project[1]."</option>";
                                        ?>
                                    </select>
                                    <div id="projectDiv" style="display: none;"><font class="on-validate-error">* Project must be selected</font></div>
                                </td>
                            </tr>
                        </table>
                        <div id="companyDiv" style="display: none;"> 
                            <table width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Select Company:
                                    </td>
                                    <td width="50%">
                                        <select class="on-field" method="post" name="company">
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="areaDiv" style="display: none;">
                            <table width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Area:
                                    </td>
                                    <td width='50%'>
                                        <select class='on-field' method='post' name='area'>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Invoice Type:
                                </td>
                                <td class="on-description-left" width="50%">
                                    <input method="post" name="invoiceType" type="radio" value="all" checked><a>All Invoices</a><br/>
                                    <input method="post" name="invoiceType" type="radio" value="0"><a>Unpaid Invoices</a><br/>
                                    <input method="post" name="invoiceType" type="radio" value="1"><a>Paid Invoices</a><br/>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Viewing Type:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="viewType" onChange="viewSelection();">
                                        <option value="all">View All Invoices</option>
                                        <option value="between">View Invoices Between Dates</option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                        <div id="dateDisplay" style="display: none;">
                            <table width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Date From:
                                    </td>
                                    <td width="50%">
                                        <input class="on-field-date" id="date_from" name="date_from" type="text" style="text-align:right;" value="<?php
                                            if ($_POST["date_from"] != "") echo "".$_POST["date_from"]; else echo "".getMonthStart($today); ?>">
                                        <div id="dateFrom" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                        <div id="dateFromDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="on-description" width="50%">
                                        Date To:
                                    </td>
                                    <td width="50%">
                                        <input class="on-field-date" id="date_to" name="date_to" type="text" style="text-align:right;" value="<?php
                                            if ($_POST["date_to"] != "") echo "".$_POST["date_to"]; else echo "".getMonthEnd($today); ?>">
                                        <div id="dateTo" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                        <div id="dateToDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <br/>
                        <input name="btnGenerate" onClick="check();" type="button" value="Generate Report">
                        <input method="post" name="save" type="hidden" value="0" />
                    </div>
                </form>
                <div id="report" style="<?php if ($generated == "1") echo "display: block;"; else echo "display: none"; ?>">
                    <?php
                        $report_heading                                 = "<tr>
                                                                                <td class='on-table-clear' colspan='100%'><a>Loaded Invoice Report<a></td>
                                                                            </tr>";

                        echo "<div class='on-20px'><table class='on-table-center on-table'>";
                            echo "".$report_heading.$displayString;
                            echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        echo "</table></div>";
                        echo "<br/>";

                        ///////////////////////////
                        if ($exceldata) {
                            //  Set Export Information
                            $_SESSION["fileName"] = "Loaded Invoices Report";
                            $_SESSION["fileData"] = $exceldata;
                        }
                        ///////////////////////////

                        echo "<input name='btnExport' onClick=\"location.href='_export.php'\" type='button' value='Export to Excel' />";
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
