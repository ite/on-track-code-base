<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!hasAccess("SHAREDRES_MANAGE"))
        header("Location: noaccess.php");

    if ($_SESSION["email"] != "admin")
        header("Location: noaccess.php");

    if (!hasAccess("RESOURCE_ASSIGN"))
        header("Location: noaccess.php");

    if (isset($_POST["save"]) && $_POST["save"] === "1") {
        $errorMessage                                                   = "";

        assignResources();

        header("Location: resource_assignment.php");
    }

    //  Company Assignment Function
    function assignResources() {
        $array                                                          = split("_", $_POST["checkbox"]);

        if ($arr[0][0])
            $array                                                      = array_slice($array, 0, count($array) - 1);

        if ($array[0][0]) {
            foreach ($array as $a) {
                $resource_id                                            = substr($a, 0, strpos($a, ":"));
                $company_id                                             = substr($a, (strpos($a, ":") + 1), strlen($a));

                $resource                                               = q("SELECT resource FROM Resources WHERE id = '$resource_id'");
                $company                                                = q("SELECT name FROM Company WHERE id = '$company_id'");

                //  Assign Resource To Company
                if ($_POST[$a]) {
                    $exist                                              = q("SELECT * FROM ResourceAssign WHERE resource_id = '$resource_id' ".
                                                                            "AND company_id = '$company_id'");

                    if (!$exist[0][0])
                        $insert                                         = q("INSERT INTO ResourceAssign (resource_id, company_id) ".
                                                                            "VALUES ('$resource_id', '$company_id')");

                    if ($insert) {
                        $time                                           = date("H:i:s");

                        $logs                                           = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time) ".
                                                                            "VALUES ('$resource assigned to $company', 'Insert', 'ResourceAssign', ".
                                                                            "'".$_SESSION["email"]."', '$today', '$time')");
                    }
                }
                //  Remove Resource From Company
                else {
                    $exist                                              = q("SELECT * FROM ResourceAssign WHERE resource_id = '$resource_id' ".
                                                                            "AND company_id = '$company_id'");

                    if ($exist[0][0])
                        $delete                                         = q("DELETE FROM ResourceAssign WHERE resource_id = '$resource_id' ".
                                                                            "AND company_id = '$company_id'");

                    if ($delete) {
                        $time                                           = date("H:i:s");

                        $logs                                           = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time) ".
                                                                            "VALUES ('$resource removed from $company', 'Delete', 'ResourceAssign', ".
                                                                            "'".$_SESSION["email"]."', '$today', '$time')");
                    }
                }
            }

            if ($insert && $delete)
                $errorMessage                                           = "Resource Assignment(s) Successful";
        }
    }

    if ($errorMessage != "")
        echo "<p align='center' style='padding:0px;'><strong><font color='#999999'>$errorMessage</font></strong></p>";

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0", "assignment");
?>
<script language="JavaScript" src="include/menuvisibility.js"></script>
<script language="JavaScript">
    //  Internet Browser
    var type                                                            = "MO";

    if (window.opera)
        type                                                            = "OP";
    else if (document.all)
        type                                                            = "IE";	//IE4+
    else if (!document.all && document.getElementById)
        type                                                            = "MO";
    else
        type                                                            = "IE";

    function ShowLayer(id, action)
    {
        if (type == "IE")
            eval("document.all['" + id + "'].style.display='" + action + "'");
        else if(type == "MO" || type == "OP")
            eval("document.getElementById('" + id + "').style.display='" + action + "'");
    }

    function assign()
    {
        document.forms["resource_assignment"].save.value                = 1;
        document.forms["resource_assignment"].submit();
    }
</script>
    <table width="100%">
        <tr>
            <td class="centerdata">
                <form action="" method="post" name="resource_assignment">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>
                                    Shared Resource Assignment
                                </h6>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <div style="width:100%">
                        <input name="btnAssign" onClick="assign();" type="button" value="Assign Users">
                    </div>
                    <br/>
                    <?php
                        //  nResources                                  = Number of Resources
                        $nResources                                     = q("SELECT COUNT(id) FROM Resources");
                        $resources                                      = q("SELECT id, resource FROM Resources ORDER BY resource");
                    ?>
                    <br/>
                    <table class="on-table">
                        <!--  Table Headings   -->
                        <tr>
                            <th>Resource Name</td>
                            <?php
                                //  nCompanies                          = Number of Companies
                                $nCompanies                             = q("SELECT COUNT(id) FROM Company");
                                $companies                              = q("SELECT id, name FROM Company ORDER BY name");

                                if ($nCompanies > 1)
                                    foreach ($companies as $company)
                                    {
                                        echo "<th style='padding:5px; vertical-align:text-top;' class='on-table-light'>";
                                                $name_length            = strlen($company[1]);

                                                for ($i = 0; $i < $name_length; $i++)
                                                    echo "".substr($company[1], $i, 1)."<br/>";
                                        echo "</th>";
                                    }
                                else if ($nCompanies == 1)
                                {
                                    echo "<td>".$companies[0][1]."</td>";
                                }
                            ?>
                        </tr>
                        <!--  Table Information   -->
                        <?php
                            $checkbox  = "";

                            foreach ($resources as $resource)
                            {
                                echo "<tr>
                                            <td>".$resource[1]."</td>";

                                    if ($nCompanies > 1)
                                        foreach ($companies as $company)
                                        {
                                            $checkbox                   .= "".$resource[0].":".$company[0]."_";
                                            $checked                    = q("SELECT id FROM ResourceAssign WHERE resource_id = '".$resource[0]."' ".
                                                                            "AND company_id = '".$company[0]."'");

                                            echo "<td style='padding:5px' class='centerdata'>";
                                                if ($checked)
                                                    echo "<input name='".$resource[0].":".$company[0]."' tabindex=1 type='checkbox' value='1' checked>";
                                                else
                                                    echo "<input name='".$resource[0].":".$company[0]."' tabindex=1 type='checkbox' value='1'>";
                                            echo "</td>";
                                        }
                                    else if ($nCompanies == 1)
                                    {
                                        $checkbox                       .= "".$resource[0].":".$companies[0][0]."_";
                                        $checked                        = q("SELECT id FROM ResourceAssign WHERE resource_id = '".$resource[0]."' ".
                                                                            "AND company_id = '".$companies[0][0]."'");

                                        echo "<td style='padding:5px' class='centerdata'>";
                                            if ($checked)
                                                echo "<input name='".$resource[0].":".$companies[0][0]."' tabindex=1 type='checkbox' value='1' checked>";
                                            else
                                                echo "<input name='".$resource[0].":".$companies[0][0]."' tabindex=1 type='checkbox' value='1'>";
                                        echo "</td>";
                                    }
                                echo "</tr>";
                            }
                            echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        ?>
                    </table>
                    <?php
                        echo "<input name='checkbox' type='hidden' value='".$checkbox."'>";
                    ?>
                    <br/>
                    <div style="width:100%">
                        <input name="btnAssign" onClick="assign();" type="button" value="Assign Users">
                        <input method="post" name="save" type="hidden" value="0" />
                    </div>
                </form>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
