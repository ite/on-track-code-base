<strong style="color:orange;font-weight:bold;font-size:14px;font-variant:small-caps;">
	- Company Information -
</strong>
<br/>
<table>
	<tr>
		<td class="on-description-mail">Company Name:</td>
		<td>
			<input id="companyName" name="companyName" class="on-field" type="text" tabindex="<?php echo $index++; ?>" />
			<div id="companyNameDiv" name="companyNameDiv" class="error"><font class="on-description-left"></font></div>
		</td>
	</tr>
	<tr>
		<td class="on-description-mail">Currency:</td>
		<td>
			<select id="currency" name="currency" class="on-field" tabindex="<?php echo $index++; ?>">
				<?php
					$currency = q("SELECT id, name FROM dropdowns WHERE categoryID = (SELECT id FROM categories WHERE code = 'currency') ORDER BY ranking");

					if (is_array($currency))        {
						foreach ($currency as $c)   {
				?>
				<option value="<?php echo $c[1]; ?>"><?php echo $c[1]; ?></option>
				<?php
						}
					}
				?>
			</select>
		</td>
	</tr>
	<tr>
		<td class="on-description-mail">Industry:</td>
		<td>
			<select id="industry" name="industry" class="on-field" tabindex="<?php echo $index++; ?>">
				<?php
					$industry = array("Architecture","ICT","Engineering","Consulting","Geotechnical","Other");

					if (is_array($industry))        {
						foreach ($industry as $i)   {
				?>
				<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
				<?php
						}
					}
				?>
			</select>
		</td>
	</tr>
</table>
