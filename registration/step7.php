<strong style="color:orange;font-weight:bold;font-size:14px;font-variant:small-caps;">
	- What Now? -
</strong>
<br/>
<p class="on-description-center" style="width:90%">
	Welcome to On-Track. You can now start booking time & expenses, add more projects & employees, load invoices on projects and get reports.<br/>
	<br/>
	Book Time & Expenses -> Load Invoices -> Get Reports<br/>
	<br/>
	Please click on the "?" on the top-right of your screen if you need help on a specific page or click on the "Contact Us" in the menu to send us a request!!
</p>
