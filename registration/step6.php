<strong style="color:orange;font-weight:bold;font-size:14px;font-variant:small-caps;">
	- Project Information -
</strong>
<br/><br/>
<i style="margin-left:7px;white-space:nowrap;">
	Please register your first project.
</i>
<br/><br/>
<table width="90%">
	<tr>
		<td class="on-description-mail" width="50%">Project Status:</td>
		<td width="50%">
			<select id="projectStatus" name="projectStatus" class="on-field" tabindex="<?php echo $index++; ?>">
				<option value="proposal">Proposal</option>
				<option value="active">Active</option>
			</select>
		</td>
	</tr>
	<tr>
		<td class="on-description-mail" width="50%">Project Name:</td>
		<td width="50%">
			<input id="projectName" name="projectName" class="on-field" type="text" tabindex="<?php echo $index++; ?>" />
			<div id="projectNameDiv" name="projectNameDiv" class="error"><font class="on-description-left"></font></div>
		</td>
	</tr>
	<tr>
		<td class="on-description-mail" width="50%">Registration Date:</td>
		<td width="50%"><input id="regDate" name="regDate" class="on-field date" type="text" tabindex="<?php echo $index++; ?>" /></td>
	</tr>
	<tr>
		<td class="on-description-mail" width="50%">Project Type:</td>
		<td width="50%">
			<select id="projectType" name="projectType" class="on-field" tabindex="<?php echo $index++; ?>">
			</select>
		</td>
	</tr>
	<tr>
		<td class="on-description-mail" width="50%">Total Project Budget <i>(<span class="companyCurrency"></span>)</i>:</td>
		<td width="50%"><input id="totalBudget" name="totalBudget" class="on-field" type="text" tabindex="<?php echo $index++; ?>" /></td>
	</tr>
	<tr>
		<td class="on-description-mail" width="50%">Consulting Budget <i>(<span class="companyCurrency"></span>)</i>:</td>
		<td width="50%"><input id="consulting" name="consulting" class="on-field" type="text" tabindex="<?php echo $index++; ?>" /></td>
	</tr>
	<tr>
		<td class="on-description-mail" width="50%">Expense Budget <i>(<span class="companyCurrency"></span>)</i>:</td>
		<td width="50%"><input id="expenses" name="expenses" class="on-field" type="text" tabindex="<?php echo $index++; ?>" /></td>
	</tr>
</table>
<div id="activeDiv" name="activeDiv" class="hidden">
	<table width="90%">
		<tr>
			<td class="on-description-mail" width="50%">Diverse Income Budget <i>(<span class="companyCurrency"></span>)</i>:</td>
			<td width="50%"><input id="diverse_income" name="diverse_income" class="on-field" type="text" tabindex="<?php echo $index++; ?>" /></td>
		</tr>
		<tr>
			<td class="on-description-mail" width="50%">VAT <i>(%)</i>:</td>
			<td width="50%"><input id="vat" name="vat" class="on-field" type="text" tabindex="<?php echo $index++; ?>" /></td>
		</tr>
		<tr>
			<td class="on-description-mail" width="50%">Buffer <i>(%)</i>:</td>
			<td width="50%"><input id="buffer" name="buffer" class="on-field" type="text" tabindex="<?php echo $index++; ?>" /></td>
		</tr>
		<tr>
			<td class="on-description-mail" width="50%">Due Date:</td>
			<td width="50%"><input id="dueDate" name="dueDate" class="on-field" type="text" tabindex="<?php echo $index++; ?>" /></td>
		</tr>
		<tr>
			<td class="on-description-mail" width="50%">Travelling Rate:</td>
			<td width="50%">
			<select id="travelRate" name="travelRate" class="on-field" tabindex="<?php echo $index++; ?>">
			<?php
				$travelRates = q("SELECT id, name, deflt FROM dropdowns WHERE categoryID = (SELECT id FROM categories WHERE code = 'travelRate') AND active = 1 ORDER BY ranking");

				if (is_array($travelRates))  {
					foreach ($travelRates as $v)   {
						echo "<option value='".$v[0]."'>".$v[1]."</option>";
					}
				}
			?>
			</select>
			</td>
		</tr>
	</table>
</div>
<table width="90%">
	<tr>
		<td class="on-description-center" colspan="100%">Team Definition</td>
	</tr>
</table>
<table cellspacing="0" cellpadding="0">
	<tr>
		<td class="on-description-center">
			User
		</td>
		<td class="on-description-center">
			PM
		</td>
		<td class="on-description-center">
			Budget
		</td>
		<td class="on-description-center">
			Rate
		</td>
	</tr>
	<tr>
		<td class="on-description-mail">
			<span id="userInfo" name="userInfo"></span>
		</td>
		<td class="centerdata">
			<input id="pm" name="pm" class="" type="checkbox" checked disabled tabindex="<?php echo $index++; ?>" />
		</td>
		<td>
			<input id="userBudget" name="userBudget" class="on-field-numeric" type="text" tabindex="<?php echo $index++; ?>" />
		</td>
		<td>
			<select id="userRates" name="userRates" class="on-field-numeric" tabindex="<?php echo $index++; ?>">
			</select>
		</td>
	</tr>
</table>