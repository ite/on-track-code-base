<strong style="color:orange;font-weight:bold;font-size:14px;font-variant:small-caps;">
	- Vehicle(s) -
</strong>
<br/>
<table>
	<tr>
		<td>
			<table id="step5Tbl" name="step5Tbl">
				<?php
				for ($i = 0; $i < 1; $i++)  {
				?>
				<tr>
					<td class="on-description-center">
						Vehicle Description
					</td>
					<td class="on-description-center">
						Rate/Km
					</td>
				</tr>
				<tr>
					<td>
						<input id="step5Tbl_0_<?php echo $i; ?>" name="step5Tbl_0_<?php echo $i; ?>" class="on-field" type="text" tabindex="<?php echo $index++; ?>" />
					</td>
					<td>
						<input id="step5Tbl_1_<?php echo $i; ?>" name="step5Tbl_1_<?php echo $i; ?>" class="on-field" type="text" tabindex="<?php echo $index++; ?>" />
					</td>
				</tr>
				<?php
				}
				?>
				<tr id="step5TblLstRow" name="step5TblLstRow"></tr>
			</table>
			<input class="addRow" href="step5Tbl" type="button" value="+" tabindex="<?php echo $index++; ?>" style="margin-left:7px;" />
			<input id="step5TblCntr" name="step5TblCntr" type="hidden" value="<?php echo $i; ?>" />
			<input id="step5TblColumns" name="step5TblColumns" type="hidden" value="2" />
		</td>
	</tr>
</table>
