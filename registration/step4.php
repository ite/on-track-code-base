<strong style="color:orange;font-weight:bold;font-size:14px;font-variant:small-caps;">
	- Activity Type(s) & Activities -
</strong>
<br/>
<table>
	<tr>
		<td>
			<table id="step4Tbl" name="step4Tbl">
				<?php
				for ($i = 0; $i < 1; $i++)  {
				?>
				<tr>
					<td class="on-description-center">
						Activity Type
					</td>
					<td class="on-description-center">
						Activities
					</td>
				</tr>
				<tr>
					<td>
						<input id="step4Tbl_0_<?php echo $i; ?>" name="step4Tbl_0_<?php echo $i; ?>" class="on-field" type="text" tabindex="<?php echo $index++; ?>" />
					</td>
					<td>
						<textarea id="step4Tbl_1_<?php echo $i; ?>" name="step4Tbl_1_<?php echo $i; ?>" class="on-field" type="text" tabindex="<?php echo $index++; ?>"></textarea>
						<br/>
						<i style="margin-left:7px;white-space:nowrap;">
							Activities delimited/seperated by ","
						</i>
					</td>
				</tr>
				<?php
				}
				?>
				<tr id="step4TblLstRow" name="step4TblLstRow"></tr>
			</table>
			<input class="addRow" href="step4Tbl" type="button" value="+" tabindex="<?php echo $index++; ?>" style="margin-left:7px;" />
			<input id="step4TblCntr" name="step4TblCntr" type="hidden" value="<?php echo $i; ?>" />
			<input id="step4TblColumns" name="step4TblColumns" type="hidden" value="2" />
			<div id="step4Div" name="step4Div" class="error"><font class="on-description-left"></font></div>
		</td>
	</tr>
</table>
