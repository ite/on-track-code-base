<strong style="color:orange;font-weight:bold;font-size:14px;font-variant:small-caps;">
	- Project Types -
</strong>
<br/><br/>
<i style="margin-left:7px;white-space:nowrap;">
	Please specify the typical project types that your company are involved in.
</i>
<br/><br/>
<table>
	<tr>
		<td>
			<table id="step3Tbl" name="step3Tbl">
				<tr>
					<td class="on-description-center">
						Project Type
					</td>
				</tr>
				<?php
				for ($i = 0; $i < 1; $i++)  {
				?>
				<tr>
					<td>
						<input id="step3Tbl_0_<?php echo $i; ?>" name="step3Tbl_0_<?php echo $i; ?>" class="on-field projectType" type="text" tabindex="<?php echo $index++; ?>" />
					</td>
				</tr>
				<?php
				}
				?>
				<tr id="step3TblLstRow" name="step3TblLstRow"></tr>
			</table>
			<input class="addRow" href="step3Tbl" type="button" value="+" tabindex="<?php echo $index++; ?>" style="margin-left:7px;" />
			<input id="step3TblCntr" name="step3TblCntr" type="hidden" value="<?php echo $i; ?>" />
			<input id="step3TblColumns" name="step3TblColumns" type="hidden" value="1" />
			<div id="step3Div" name="step3Div" class="error"><font class="on-description-left"></font></div>
		</td>
	</tr>
</table>
