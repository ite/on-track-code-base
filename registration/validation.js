var test;

var validationDivs = function(e)       {
	jQuery("#step1,#step2,#step3,#step4,#step5,#step6,#step7").hide();

	jQuery("li.step1").children().eq(0).removeClass("activeStep");
	jQuery("li.step2").children().eq(0).removeClass("activeStep");
	jQuery("li.step3").children().eq(0).removeClass("activeStep");
	jQuery("li.step4").children().eq(0).removeClass("activeStep");
	jQuery("li.step5").children().eq(0).removeClass("activeStep");
	jQuery("li.step6").children().eq(0).removeClass("activeStep");
	jQuery("li.step7").children().eq(0).removeClass("activeStep");

	currentStep = e;

	jQuery("#"+currentStep).show();
	jQuery("li."+currentStep).children().eq(0).addClass("activeStep");

	buttonsHideShow();
	windowPosition();
}

function showErrorDiv(field, msg)	{
	jQuery(field).children().eq(0).text(msg);
	jQuery(field).show();
}

function hideErrorDiv(field)	{
	jQuery(field).children().eq(0).text("");
	jQuery(field).hide();
}

function step1_validation()	{
	var stepValid = 1;

	var companyName = jQuery("#companyName").val();
	var currency = jQuery("#currency").val();

	hideErrorDiv("#companyNameDiv");

	if (companyName != "")      {
		jQuery.post("_ajax.php", {func: "recordExists", table: "Company", where: "name = '"+companyName+"'"}, function(data)	{
			data = json_decode(data);

			if (data == "0")       {
				companyInfo[0] = new Array();

				companyInfo[0][0] = companyName;
				companyInfo[0][1] = currency;
			} else	{
				showErrorDiv("#companyNameDiv", "Company Already Exists");

				validationDivs("step1");
				stepValid &= 0;
			}
		});
	} else	{
		showErrorDiv("#companyNameDiv", "Company Name Should Be Entered");

		validationDivs("step1");
		stepValid &= 0;
	}

	return stepValid;
}

function step2_validation()	{
	var stepValid = 1;

	var frstname = jQuery("#frstname").val();
	var lstname = jQuery("#lstname").val();
	var email = jQuery("#email").val();
	var phone = jQuery("#phone").val();
	var password = jQuery("#password").val();
	var costToCompany = jQuery("#costToCompany").val();
	var leave = new Array();

	//  Leave
	var leaveTypes = new Array("annual", "sick", "special", "study", "unpaid", "maternity", "family");

	hideErrorDiv("#frstnameDiv");
	hideErrorDiv("#lstnameDiv");
	hideErrorDiv("#emailDiv");
	hideErrorDiv("#phoneDiv");
	hideErrorDiv("#passwordDiv");
	hideErrorDiv("#pwConfirmDiv");
	hideErrorDiv("#costToCompanyDiv");

	test = validation("entered", frstname);

	if (!test)	showErrorDiv("#frstnameDiv", validation_message("entered"));

	stepValid &= test;

	test = validation("entered", lstname);

	if (!test)	showErrorDiv("#lstnameDiv", validation_message("entered"));

	stepValid &= test;

	test = validation("email", email);

	if (!test)	showErrorDiv("#emailDiv", validation_message("email"));

	stepValid &= test;

	test = validation("phone", phone);

	if (!test)	showErrorDiv("#phoneDiv", validation_message("phone"));

	stepValid &= test;

	test = validation("currency", costToCompany);

	if (!test)	showErrorDiv("#costToCompanyDiv", validation_message("currency"));

	stepValid &= test;

	if (stepValid == 1)      {
		jQuery.post("_ajax.php", {func: "recordExists", table: "Employee", where: "email = '"+email+"'"}, function(data)	{
			data = json_decode(data);

			if (data == "0")       {
				userInfo[0] = new Array();

				userInfo[0][0] = frstname;
				userInfo[0][1] = lstname;
				userInfo[0][2] = email;
				userInfo[0][3] = phone;
				userInfo[0][4] = password;
				userInfo[0][5] = costToCompany;

				jQuery.each(leaveTypes, function(index, value) {
					var c = leave.length;

					leave[c] = new Array();

					leave[c][0] = value;
					leave[c][1] = jQuery("#"+value).val();
				});

				userInfo[0][6] = leave;

				var counter = jQuery("#step2TblCntr").val();

				for (var i = 0; i < counter; i++)   {
					var value = jQuery("#step2Tbl_0_"+i).val();

					if (value != "")        {
						jQuery("#step2Tbl_0_"+i).val(parseFloat(value).toFixed(2));

						if (jQuery.inArray(parseFloat(value).toFixed(2), rateInfo) == -1)
							rateInfo.push(parseFloat(value).toFixed(2));
					}
				}

				rateInfo.sort();
			} else  {
				validationDivs("step2");
				valid &= 0;
			}
		});
	} else  {
		validationDivs("step2");
	}

	return stepValid;
}

function step3_validation()	{
	var stepValid = 1;

	hideErrorDiv("#step3Div");

	if (projectTypes.length >= 1)       {
		projectTypeInfo = projectTypes;
	} else	 {
		showErrorDiv("#step3Div", "Please Ensure That At Least One Project Type Is Added");
		validationDivs("step3");
		stepValid &= 0;
	}

	return stepValid;
}

function step4_validation()	{
	var stepValid = 1;

	var counter = jQuery("#step4TblCntr").val();
	var activityTypes = new Array();

	for (var i = 0; i < counter; i++)   {
		var value1 = jQuery("#step4Tbl_0_"+i).val();
		var value2 = jQuery("#step4Tbl_1_"+i).val();

		if (value1 != "" && value2 != "")        {
			var c = activityTypes.length;

			activityTypes[c] = new Array();

			activityTypes[c][0] = value1;
			activityTypes[c][1] = value2;
		}
	}

	hideErrorDiv("#step4Div");

	if (activityTypes.length >= 1)       {
		activityTypeInfo = activityTypes;
	} else      {
		showErrorDiv("#step4Div", "Please Ensure That At Least One Activity Type And Activity Is Added");
		validationDivs("step4");
		stepValid &= 0;
	}

	return stepValid;
}

function step5_validation()	{
	var stepValid = 1;

	var counter = jQuery("#step5TblCntr").val();
	var vehicles = new Array();

	for (var i = 0; i < counter; i++)   {
		var value1 = jQuery("#step5Tbl_0_"+i).val();
		var value2 = jQuery("#step5Tbl_1_"+i).val();

		if (value1 != "" && value2 != "")        {
			var c = vehicles.length;

			vehicles[c] = new Array();

			vehicles[c][0] = value1;
			vehicles[c][1] = value2;
		}
	}

	vehicleInfo = vehicles;

	return stepValid;
}

function step6_validation()	{
	var stepValid = 1;

	var projectStatus = jQuery("#projectStatus").val();
	var projectName = jQuery("#projectName").val();
	var regDate = jQuery("#regDate").val();
	var projectType = jQuery("#projectType").val();
	var totalBudget = jQuery("#totalBudget").val();
	var consulting = jQuery("#consulting").val();
	var expenses = jQuery("#expenses").val();
	var diverse_income = jQuery("#diverse_income").val();
	var vat = jQuery("#vat").val();
	var buffer = jQuery("#buffer").val();
	var dueDate = jQuery("#dueDate").val();
	var travelRate = jQuery("#travelRate").val();

	var userBudget = jQuery("#userBudget").val();
	var userRates = jQuery("#userRates").val();

	test = validation("entered", projectName);

	if (!test)	showErrorDiv("#projectNameDiv", validation_message("entered"));

	stepValid &= test;

	if (projectName != "")      {
		projectInfo[0] = new Array();

		projectInfo[0][0] = projectStatus;
		projectInfo[0][1] = projectName;
		projectInfo[0][2] = regDate;
		projectInfo[0][3] = projectType;
		projectInfo[0][4] = totalBudget;
		projectInfo[0][5] = consulting;
		projectInfo[0][6] = expenses;
		projectInfo[0][7] = diverse_income;
		projectInfo[0][8] = vat;
		projectInfo[0][9] = buffer;
		projectInfo[0][10] = dueDate;
		projectInfo[0][11] = travelRate;

		projectInfo[0][12] = userBudget;
		projectInfo[0][13] = userRates;

	} else  {
		showErrorDiv("#projectNameDiv", "Project Should Be Entered");

		validationDivs("step6");
		stepValid &= 0;
	}

	return stepValid;
}
