<strong style="color:orange;font-weight:bold;font-size:14px;font-variant:small-caps;">
	- Personal Information -
</strong>
<br/>
<table width="90%">
	<tr>
		<td class="on-description-mail" width="50%">Name:</td>
		<td width="50%">
			<input id="frstname" name="frstname" class="on-field" type="text" tabindex="<?php echo $index++; ?>" />
			<div id="frstnameDiv" name="frstnameDiv" class="error"><font class="on-description-left"></font></div>
		</td>
	</tr>
	<tr>
		<td class="on-description-mail" width="50%">Surname:</td>
		<td width="50%">
			<input id="lstname" name="lstname" class="on-field" type="text" tabindex="<?php echo $index++; ?>" />
			<div id="lstnameDiv" name="lstnameDiv" class="error"><font class="on-description-left"></font></div>
		</td>
	</tr>
	<tr>
		<td class="on-description-mail" width="50%">Phone Number:</td>
		<td width="50%">
			<input id="phone" name="phone" class="on-field" type="text" tabindex="<?php echo $index++; ?>" />
			<div id="phoneDiv" name="phoneDiv" class="error"><font class="on-description-left"></font></div>
		</td>
	</tr>
	<tr>
		<td class="on-description-mail" width="50%">E-Mail Address:</td>
		<td width="50%">
			<input id="email" name="email" class="on-field" type="text" tabindex="<?php echo $index++; ?>" />
			<div id="emailDiv" name="emailDiv" class="error"><font class="on-description-left"></font></div>
		</td>
	</tr>
	<tr>
		<td class="on-description-mail" width="50%">Password:</td>
		<td width="50%">
			<input id="password" name="password" class="on-field" type="password" tabindex="<?php echo $index++; ?>" />
			<div id="passwordDiv" name="passwordDiv" class="error"><font class="on-description-left"></font></div>
		</td>
	</tr>
	<tr>
		<td class="on-description-mail" width="50%">Confirm Password:</td>
		<td width="50%">
			<input id="pwConfirm" name="pwConfirm" class="on-field" type="password" tabindex="<?php echo $index++; ?>" />
			<div id="pwConfirmDiv" name="pwConfirmDiv" class="error"><font class="on-description-left"></font></div>
		</td>
	</tr>
	<tr>
		<td class="on-description-mail" width="50%">Cost to Company [<span class="companyCurrency"></span>/h]:</td>
		<td width="50%">
			<input id="costToCompany" name="costToCompany" class="on-field" type="text" tabindex="<?php echo $index++; ?>" />
			<div id="costToCompanyDiv" name="costToCompanyDiv" class="error"><font class="on-description-left"></font></div>
		</td>
	</tr>
</table>
<table>
	<tr>
		<td class="on-description-center">Rates</td>
	</tr>
	<tr>
		<td>
			<table id="step2Tbl" name="step2Tbl">
				<?php
				for ($i = 0; $i < 1; $i++)  {
				?>
				<tr>
					<td>
						<input id="step2Tbl_0_<?php echo $i; ?>" name="step2Tbl_0_<?php echo $i; ?>" class="on-field userRates" type="text" tabindex="<?php echo $index++; ?>" />
						<div id="step2Tbl_0_<?php echo $i; ?>" name="step2Tbl_0_<?php echo $i; ?>" class="error"><font class="on-description-left"></font></div>
					</td>
				</tr>
				<?php
				}
				?>
				<tr id="step2TblLstRow" name="step2TblLstRow"></tr>
			</table>
			<input class="addRow" href="step2Tbl" type="button" value="+" tabindex="<?php echo $index++; ?>" style="margin-left:7px;" />
			<input id="step2TblCntr" name="step2TblCntr" type="hidden" value="<?php echo $i; ?>" />
			<input id="step2TblColumns" name="step2TblColumns" type="hidden" value="1" />
		</td>
	</tr>
</table>
<?php
$leave_types = array("annual", "sick", "special", "study", "unpaid", "maternity", "family");

if (is_array($leave_types)) {
?>
<table width="90%">
	<tr>
		<td class="on-description-center" colspan="100%">Leave</td>
	</tr>
</table>
<table width="90%">
	<?php
	foreach ($leave_types as $lt)       {
		$days = ($lt == "annual") ? "21" : (($lt == "sick") ? "12" : (($lt == "special") ? "5" : ""));

		$leaveType = "";

		if ($lt == "annual")				$leaveType = "Annual";
		else if ($lt == "sick")			$leaveType = "Sick";
		else if ($lt == "special")		$leaveType = "Special";
		else if ($lt == "study")		$leaveType = "Study";
		else if ($lt == "unpaid")		$leaveType = "Unpaid";
		else if ($lt == "maternity")	$leaveType = "Maternity/Paternity";
		else if ($lt == "family")		$leaveType = "Family Responsibility";
	?>
	<tr>
		<td class="on-description-mail" width="50%"><?php echo $leaveType; ?></td>
		<td align="left" width="50%">
			<input id="<?php echo $lt; ?>" name="<?php echo $lt; ?>" class="on-field" type="text" tabindex="<?php echo $index++; ?>" value="<?php echo $days; ?>" />
			<div id="<?php echo $lt; ?>Div" name="<?php echo $lt; ?>Div" class="error"><font class="on-description-left"></font></div>
		</td>
	</tr>
	<?php
	}
	?>
</table>
<i style="margin-left:7px;white-space:nowrap;">
	* Please leave days available blank if no amount of days are specified.
</i>
<?php
}
?>
