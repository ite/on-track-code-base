<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("_functions.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    //if ($_SESSION["logged_in"] === true && !($_SESSION["usertype"] < "3"))
    //    header("Location: home.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    if (!hasAccess("ACTION_ASSIGN"))
        header("Location: noaccess.php");

        if ($_POST['saveactions']){
            //Get POST Values
            $companyid = $_SESSION['company_id'];
                //echo "Company id: ".$companyid."<br/>";
            $roleid = $_POST['roleid'];
                //echo "Role id: ".$roleid."<br/>";

            $actionCount = 0;
            $query = q("SELECT id FROM actions WHERE active='1' ");
            foreach($query AS $q){
                if($_POST['action_'.$q[0]]){
                    $actionid[$actionCount] = $_POST['action_'.$q[0].''];
                    $actionCount++;
                }
            }

            // Remove old entries from database
            $remove = q("DELETE FROM role_action WHERE roleid = $roleid and companyid = $companyid");
            // Save values to Database
            $theNewActions;       // Stores the action values
            $byUser = $_SESSION["email"];
            $date__ = date("Y-m-d h:i:s");
            for ($i=0; $i<$actionCount; $i++){
                $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES ('$companyid', '$roleid ', '".$actionid[$i]."') ");	// Correct Query
                $theNewActions.= q("SELECT action FROM actions WHERE id='".$actionid[$i]."' ").", ";
            }

            $menus = getMenus_();
            $users = q("SELECT cu.company_id, cu.user_id, c.name, e.email FROM (Company_Users AS cu INNER JOIN Company AS c ON c.id = cu.company_id ".
                            "INNER JOIN Employee AS e ON e.id = cu.user_id) WHERE cu.company_id = '$companyid' ORDER BY c.name, e.email");
            $drop = q("DELETE FROM MenuIndex WHERE company_id = '$companyid'");

            foreach($users as $u)	{
                $lookup = addMenuIndex($menus, $u[0], $u[1], $u[3]);

                // Re-Issue User Actions
                if (exist("reissue_actions", "companyID = '".$u[0]."' AND userID = '".$u[1]."'"))
                    $update = q("UPDATE reissue_actions SET processed = 0 WHERE companyID = '".$u[0]."' AND userID = '".$u[1]."'");
                else
                    $insert = q("INSERT INTO reissue_actions (companyID,userID,processed) VALUES ('".$u[0]."','".$u[1]."','0')");
            }

            // Add a LOG of the addition
            $forRole= q("SELECT role FROM roles WHERE id='".$_POST['roleid']."' ");
            $what = $byUser ." changed ".$forRole."(".$_POST['roleid'].") actions to ".$theNewActions;
            $onDate = date("Y-m-d");
            $onTime = date("h:i:s");
            $compId = $_SESSION["company_id"];

            $insert = q("INSERT INTO Logs (what,access,on_table,by_user,on_date,on_time,company_id) VALUES ( '".$what."','Update','Actions','".$byUser."','".$onDate."','".$onTime."','".$compId."' )");
            // END Save

            header("Location: role_assignment.php");
        }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0", "vehicles");
?>
    <script language="JavaScript">
        jQuery(function()    {
            jQuery("#role").change(function() {
                jQuery(".chkbox").each(function(i, v)	{
                    jQuery(this).attr("checked", "");
                });

                var roleid = jQuery("#role").val();

                jQuery.post("_ajax.php", {func: "get_action_assign", roleid: roleid}, function(data)	{
                    data = json_decode(data);
                    jQuery.each(data,function(i, v)	{
                        var field = "#action_"+v[3];
                        jQuery(field).attr("checked", "checked");
                    });
                });
            });

            // For firs load/view
            jQuery(".chkbox").each(function(i, v)	{
                    jQuery(this).attr("checked", "");
                });

                var roleid = jQuery("#role").val();

                jQuery.post("_ajax.php", {func: "get_action_assign", roleid: roleid}, function(data)	{
                    data = json_decode(data);
                    jQuery.each(data,function(i, v)	{
                        var field = "#action_"+v[3];
                        jQuery(field).attr("checked", "checked");
                    });
                });
        });
    </script>

    <table width="90%">
        <tr height="380px">
            <td class='centerdata' valign="top">
                <form action="" method="post" name="role_actions">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Action Assignment</h6>
                            </td>
                        </tr>
                        <?php if($_POST['saveactions']) echo "<tr><td class='centerdata'><br/><font style='color:#028200'> - Actions have been saved - </font></td></tr>"; ?>
                        <tr>
                            <td><br/></td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                    Roles:
                            </td>
                            <td class="on-leftinput">
                                <!-- -->
                                <?php
                                    $query = "SELECT role,descr,id FROM roles WHERE active = '1' AND companyid = '".$_SESSION['company_id']."' ORDER BY id";

                                    $result = q($query);
                                    echo "<select class='on-field' id='role' name='roleid' value=''>Role</option>";

                                    foreach($result as $r){ // Array or records
                                        if(($r[1] == "Admin") || ($r[1] == "admin")){
                                           // Do not dispaly Admin
                                        }else{
                                            echo "<option value=".$r[2].">".$r[1]."</option>";
                                        }
                                    }
                                    echo "</select>";// Closing of list box
                                ?>
                                <!-- -->
                            </td>
                        </tr>

                        <tr>
                            <td class="on-description" width="50%">
                                Actions:
                            </td>
                            <td width="50%">
                               <!-- -->
                               <?php
                                        $query = "SELECT id,action,descr FROM actions WHERE active='1' ";
                                        $result = q($query);
                                    ?>
                                    <table class="on-table on-lefttable" >
                                        <th colspan="100%"></th>
                                    <?php
                                        $counter = 0;
                                        $colorCounter = 0;
                                    foreach($result as $r){     // Array or records
                                        if($colorCounter == 0){
                                            echo "<tr>
                                                        <td>".$r[2]."</td><td><input id='action_".$r[0]."' name='action_".$r[0]."' type='checkbox' class='chkbox' value='".$r[0]."' /></td>
                                                    </tr>";

                                        }else if($colorCounter == 1){
                                            echo "<tr>
                                                        <td>".$r[2]."</td><td><input id='action_".$r[0]."' name='action_".$r[0]."' type='checkbox' class='chkbox' value='".$r[0]."' /></td>
                                                    </tr>";
                                        }
                                        $counter++;
                                        $colorCounter ++;
                                        if($colorCounter ==2){
                                            $colorCounter = 0;
                                        }
                                    }
                                    echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                                    ?>
                                    </table>
                               <!-- -->
                            </td>
                        </tr>
                    </table>

                    <br/>
                    <input name="btnactions" type="submit" value="Add/Save Actions">
                    <input name="saveactions" type="hidden" value="saveactions" />
                </form>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>

<?php
    //  Print Footer
    print_footer();
?>
