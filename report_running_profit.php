<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");
        
    if (!hasAccess("REP_RUNNING_PROF"))
        header("Location: noaccess.php");
        
    // Class for table background color
    $colorClass = "OnTrack_#_";

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("5", "reports");
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="report_active_projects">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Running Profit Report</h6>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <?php
                        ///////////////////////////
                        //  Get Information
                        ///////////////////////////
                        //  nProjects                                   = Number of Contacts
                        $nProjects = q("SELECT COUNT(p.id) FROM Project as p INNER JOIN companiesOnTeam as cot ON p.id = cot.project_id WHERE p.completed = 0 ".
											"AND cot.company_id = '".$_SESSION["company_id"]."'");
                        $projects = q("SELECT p.id, p.name FROM Project as p INNER JOIN companiesOnTeam as cot ON p.id = cot.project_id WHERE p.completed = 0 ".
											"AND cot.company_id = '".$_SESSION["company_id"]."' ORDER BY p.name");
                        ///////////////////////////
                        //  Create Information String
                        ///////////////////////////
                        //  Table Headers
                        $string                                         = "";
                        $grand_total                                    = 0;
                        
                        $row                                                              = 0;
                        
                        $excelheadings[$row][]                                     = "Report: Running Profit"; 
                        $excelheadings[$row][]                                     = ""; 
                        $excelheadings[$row][]                                     = ""; 
                        $excelheadings[$row][]                                     = "";
                            $row ++;
                        $excelheadings[$row][]                                     = $colorClass."Project Name"; 
                        $excelheadings[$row][]                                     = $colorClass."Amount Invoiced"; 
                        $excelheadings[$row][]                                     = $colorClass."Expected Income"; 
                        $excelheadings[$row][]                                     = $colorClass."Profit (".$_SESSION["currency"].")";
                            $row ++;
                        
                        $headers                                        = "<tr>
                                                                                    <th>Project Name</th>
                                                                                    <th>Amount Invoiced</th>
                                                                                    <th>Expected Income</th>
                                                                                    <th>Profit <i>(".$_SESSION["currency"].")</i></th>
                                                                                </tr>";

                        //  Table Information
                        if ($nProjects > 1)
                        {
                            foreach ($projects as $project)
                            {
                                $invoiced = q("SELECT SUM(li.consulting) FROM (LoadInvoices AS li) ".
											"WHERE li.project_id = '".$project[0]."' AND li.company_id = '".$_SESSION["company_id"]."'");
                                $expected = q("SELECT SUM(ts.rate * ts.time_spent) FROM (TimeSheet AS ts) ".
											"WHERE ts.project_id = '".$project[0]."' AND ts.company_id = '".$_SESSION["company_id"]."'");
                                $invoiced                               = number_format($invoiced, 2, ".", "");
                                $expected                               = number_format($expected, 2, ".", "");
                                $deficit                                = number_format($invoiced - $expected, 2, ".", "");
                                $grand_total                            += $deficit;
                                $text_color                             = "color:#FFFFFF;";

                                if ($deficit < 0)
                                    $text_color                         = "color:#FF0000;";

                                $string                                 .= "<tr>
                                                                                    <td>".$project[1]."</td>
                                                                                    <td class='rightdata'>".$invoiced."</td>
                                                                                    <td class='rightdata'>".$expected."</td>
                                                                                    <td class='rightdata' style='".$text_color."'>".$deficit."</td>
                                                                                </tr>";
                                                                            
                                $exceldata[$row][]                                     = $project[1]; 
                                $exceldata[$row][]                                     = $invoiced; 
                                $exceldata[$row][]                                     = $expected; 
                                $exceldata[$row][]                                     = $deficit;
                                    $row ++;
                            }
                        }
                        else if ($nProjects == 1)
                        {
                            $invoiced = q("SELECT SUM(li.consulting) FROM (LoadInvoices AS li)".
											"WHERE li.project_id = '".$projects[0][0]."' AND li.company_id = '".$_SESSION["company_id"]."'");
                            $expected = q("SELECT SUM(ts.rate * ts.time_spent) FROM (TimeSheet AS ts INNER JOIN Project AS p ".
											"WHERE ts.project_id = '".$projects[0][0]."' AND ts.company_id = '".$_SESSION["company_id"]."'");
                            $invoiced                                   = number_format($invoiced, 2, ".", "");
                            $expected                                   = number_format($expected, 2, ".", "");
                            $deficit                                    = number_format($invoiced - $expected, 2, ".", "");
                            $grand_total                                += $deficit;
                            $text_color                                 = "color:#000000;";

                            if ($deficit < 0)
                                $text_color                             = "color:#FF0000;";

                            $string                                     .= "<tr>
                                                                                <td>".$projects[0][1]."</td>
                                                                                <td class='rightdata'>".$invoiced."</td>
                                                                                <td class='rightdata'>".$expected."</td>
                                                                                <td class='rightdata'>".$deficit."</td>
                                                                            </tr>";
                                                                            
                            $exceldata[$row][]                                     = $projects[0][1]; 
                            $exceldata[$row][]                                     = $invoiced; 
                            $exceldata[$row][]                                     = $expected; 
                            $exceldata[$row][]                                     = $deficit;
                        }
                        
                        $exceldata[$row][]                                     = ""; 
                        $exceldata[$row][]                                     = "";
                        $exceldata[$row][]                                     = "Grand Total (".$_SESSION["currency"].")"; 
                        $exceldata[$row][]                                     = $grand_total;
                        
                        ///////////////////////////
                        //  Display Information
                        if ($string != "")
                        {
                            $text_color                                 = "color:#000000;";

                            if ($grand_total < 0)
                                $text_color                             = "color:#FF0000;";

                            $grand_total                                = number_format($grand_total, 2, ".", "");

                            $string                                     = $headers.$string."<tr>
                                                                                    <td class='on-table-total' colspan='3'>Grand Total <i>(".$_SESSION["currency"].")</i>:</td>
                                                                                    <td class='on-table-total'>".$grand_total."</td>
                                                                                </tr>";
                        }

                        if ($string != "")
                        {
                            echo "<div class='on-20px'><table class='on-table-center on-table'>";
                                echo "".$string;
                                echo  "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                            echo "</table></div>";
                            echo "<br/>";

                            ///////////////////////////
                            //  Set Export Information
                            $_SESSION["fileName"] = "Running Profit Report";
                            $_SESSION["fileData"] = array_merge($excelheadings, $exceldata);
                            ///////////////////////////

                            echo "<input name='btnExport' onClick=\"location.href='_export.php'\" type='button' value='Export to Excel' />";
                        }
                        ///////////////////////////
                    ?>
                </form>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
