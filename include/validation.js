//  Internet Browser
var type                                                                = "MO";

if (window.opera)
    type                                                                = "OP";
else if (document.all)
    type                                                                = "IE";	//IE4+
else if (!document.all && document.getElementById)
    type                                                                = "MO";
else
    type                                                                = "IE";

function ShowLayer(id, action) {
    if (type == "IE")
        eval("document.all['" + id + "'].style.display='" + action + "'");
    else if(type == "MO" || type == "OP")
        eval("document.getElementById('" + id + "').style.display='" + action + "'");
}

function validation(type, value) {
    if (type == "maxLength")    {
        return (value.length > 250) ? false : true;
    }
    else if (type == "required")  {
        return (value == "" || value == "0" || value == "null") ? false : true;
    }
    else if (type == "entered")  {
        return (value == "") ? false : true;
    }
    else if (type == "non-numeric")  {
        var exp = /^[\d]{1,9}$/;

        if (value != "" && exp.test(value))
            return false;

        return true;
    }
    else if (type == "required-numeric")  {
        var exp = /^[\d]{1,9}$/;

        if (value != "" && exp.test(value))	{
            return true;
		}

        return false;
    }
    else if (type == "required-currency")  {
        var exp = /^[-+]?[\d]+(\.\d{1,2})?$/;

        if (value != "" && exp.test(value))	{
            return true;
		}

        return false;
    }
    else if (type == "required-latitude")  {
		var exp = /^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,10}$/;

        if (value != "" && exp.test(value))	{
            return true;
		}

        return false;
    }
    else if (type == "required-longitude")  {
        var exp = /^-?([1]?[1-7][1-9]|[1]?[1-8][0]|[1-9]?[0-9])\.{1}\d{1,10}$/;

        if (value != "" && exp.test(value))	{
            return true;
		}

        return false;
    }
    else if (type == "phone" || type == "number" || type == "currency" || type == "percentage" || type == "date" || type == "email" || type == "file" || type == "lat" || type == "long")    {
        if (type == "phone")
            var exp =  /^[\d]{10,11}$/;
        else if (type == "number")
            var exp = /^[\d]{1,9}$/;
        else if (type == "currency")
            var exp = /^[-+]?[\d]+(\.\d{1,2})?$/;
        else if (type == "percentage")
            var exp = /^((100(\.0{1,2})?)|([0-9]{0,2}(\.[0-9]{1,2})?))%?$/;
        else if (type == "date")
            var exp = /^[0-9]{4}-(((0[13578]|(10|12))-(0[1-9]|[1-2][0-9]|3[0-1]))|(02-(0[1-9]|[1-2][0-9]))|((0[469]|11)-(0[1-9]|[1-2][0-9]|30)))$/;
        else if (type == "email")
            var exp = /^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/;
        else if (type == "file")	{
            var exp = /^(.)+\.(jpg|jpeg|png|pdf)$/;

			value = value.toLowerCase();
		}
		else if (type == "lat")
			var exp = /^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,10}$/;
		else if (type == "long")
			var exp = /^-?([1]?[1-7][1-9]|[1]?[1-8][0]|[1-9]?[0-9])\.{1}\d{1,10}$/;

        return (exp.test(value)) ? true : ((value == "" && type != "date") ? true : false);
    }
    else if (type == "date2")    {
        var exp = /^[0-9]{4}-(((0[13578]|(10|12))-(0[1-9]|[1-2][0-9]|3[0-1]))|(02-(0[1-9]|[1-2][0-9]))|((0[469]|11)-(0[1-9]|[1-2][0-9]|30)))$/;

        return (value == "") ? true : ((exp.test(value)) ? true : false);
    }
    else
        return true;  //  No Type Provided
}

function validation_message(type)	{
	var msg = "";

	switch(type)	{
		case "maxLength":
			msg += "* Field may not be longer that 250 characters";
			break;
		case "required":
			msg += "* Field must be selected";
			break;
		case "entered":
			msg += "* Field must be entered";
			break;
		case "non-numeric":
			msg += "* Field must be alpha-numeric, eg. AT 145";
			break;
		case "required-numeric":
			msg += "* Field must be entered and be numeric, eg. 123";
			break;
		case "required-currency":
			msg += "* Field must be entered and be numeric, eg. 12.34";
			break;
		case "required-latitude":
			msg += "* Field must be entered and fit xx.xxxxxx, eg. -23.456324";
			break;
		case "required-longitude":
			msg += "* Field must be entered and fit xx.xxxxxx, eg. 23.456324";
			break;
		case "phone":
			msg += "* Field must be a phone number, eg. 0825552222";
			break;
		case "number":
			msg += "* Field must be numeric, eg. 123";
			break;
		case "currency":
			msg += "* Field must be numeric, eg. 123.45";
			break;
		case "percentage":
			msg += "* Field must be a percentage, eg. 100%";
			break;
		case "date":
		case "date2":
			msg += "* Field must be a valid date, eg. 1990-01-23";
			break;
		case "email":
			msg += "* Field must be a valid email address, eg. you@somehost.com";
			break;
		case "file":
			msg += "* File must be of type (jpg/jpeg/png/pdf) ";
			break;
		case "lat":
			msg += "* Field must fit xx.xxxxxx, eg. -23.456324";
			break;
		case "long":
			msg += "* Field must fit xx.xxxxxx, eg. 23.456324";
			break;
		default:
			break;
	}

	return msg;
}

//  Live Search
var xmlHttp;

function GetXmlHttpObject() {
    var xmlHttp = null;

    // Firefox, Opera 8.0+, Safari
    try {
        xmlHttp = new XMLHttpRequest();
    }
    //Internet Explorer
    catch (e) {
        try {
            xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
    }

    return xmlHttp;
}

function stateChanged() {
    if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete") {
        document.getElementById("listHint").innerHTML = xmlHttp.responseText;
    }
}

function getList(str, page, status, type, completed) {
    xmlHttp = GetXmlHttpObject();

    if (xmlHttp == null) {
        alert ("Browser does not support HTTP Request");

        return;
    }

    var url = "_getList.php";
    url = url + "?q=" + str;
    url = url + "&page=" + page;
    url = url + "&status=" + status;
    url = url + "&type=" + type;
    url = url + "&completed=" + completed;
    url = url + "&id=" + Math.random();

    xmlHttp.onreadystatechange=stateChanged;
    xmlHttp.open("GET", url, true);
    xmlHttp.send(null);
}