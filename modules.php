<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

//    if ($_SESSION["logged_in"] === true && !($_SESSION["usertype"] === "0"))
//        header("Location: home.php");

    if ($_SESSION["email"] != "admin")
        header("Location: noaccess.php");

    if (!hasAccess("MODULE_MAN"))
        header("Location: noaccess.php");

    $display_link                                                       = "modules.php?alphabet";

    //  Set Alphabet Letter
    $alphabet                                                           = "";
    $alphabet                                                           = $_GET["alphabet"];

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0", "modules");
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="modules">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Module List</h6>
                            </td>
                        </tr>
                        <tr>
                            <td class="centerdata">
                                <br/>
                            </td>
                        </tr>
                    </table>
                    <?php
                        echo "<br/>";
                        echo "| <a href='$display_link='>View All Modules</a> |";
                        echo "<br/><br/>";
                        echo "<a href='$display_link=A'>A</a> | <a href='$display_link=B'>B</a> | ".
                            "<a href='$display_link=C'>C</a> | <a href='$display_link=D'>D</a> | ".
                            "<a href='$display_link=E'>E</a> | <a href='$display_link=F'>F</a> | ".
                            "<a href='$display_link=G'>G</a> | <a href='$display_link=H'>H</a> | ".
                            "<a href='$display_link=I'>I</a> | <a href='$display_link=J'>J</a> | ".
                            "<a href='$display_link=K'>K</a> | <a href='$display_link=L'>L</a> | ".
                            "<a href='$display_link=M'>M</a> | <a href='$display_link=N'>N</a> | ".
                            "<a href='$display_link=O'>O</a> | <a href='$display_link=P'>P</a> | ".
                            "<a href='$display_link=Q'>Q</a> | <a href='$display_link=R'>R</a> | ".
                            "<a href='$display_link=S'>S</a> | <a href='$display_link=T'>T</a> | ".
                            "<a href='$display_link=U'>U</a> | <a href='$display_link=V'>V</a> | ".
                            "<a href='$display_link=W'>W</a> | <a href='$display_link=X'>X</a> | ".
                            "<a href='$display_link=Y'>Y</a> | <a href='$display_link=Z'>Z</a>";
                    ?>
                    <br/><br/>
                    <input name="btnAdd" onClick="location.href='module_add.php';" tabindex="1" type="button" value="Add New Module">
                    <br/><br/>
                    <table class="on-table-center on-table">
                        <!--  Table Headings   -->
                        <tr>
                            <th>Module Name</th>
                            <th>Module Code</th>
                        </tr>
                        <!--  Table Information   -->
                        <?php
                            //  nModules                                = Number of Modules
                            $nModules                                   = q("SELECT COUNT(id) FROM Modules WHERE name LIKE '$alphabet%'");
                            $modules                                    = q("SELECT id, name, code FROM Modules WHERE name LIKE '$alphabet%' ORDER BY name");

                            if ($nModules > 1)
                            {
                                foreach ($modules as $module)
                                {
                                    echo "<tr>";
                                        echo "<td>";
                                            echo "<a href='module_edit.php?id=".$module[0]."'>".$module[1]."</a>";
                                        echo "</td>";
                                        echo "<td>";
                                            echo "".$module[2]."";
                                        echo "</td>";
                                    echo "</tr>";
                                }
                            }
                            else if ($nModules == 1)
                            {
                                echo "<tr>";
                                    echo "<td>";
                                        echo "<a href='module_edit.php?id=".$modules[0][0]."'>".$modules[0][1]."</a>";
                                    echo "</td>";
                                    echo "<td>";
                                        echo "".$modules[0][2]."";
                                    echo "</td>";
                                echo "</tr>";
                            }
                            else
                            {
                                echo "<tr>";
                                    echo "<td align='center' colspan='2'>";
                                        if ($alphabet == "")
                                            echo "No modules available";
                                        else
                                            echo "No modules available under $alphabet";
                                    echo "</td>";
                                echo "</tr>";
                            }
                            echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        ?>
                    </table>
                    <br/>
                    <input name="btnAdd" onClick="location.href='vehicle_add.php';" tabindex="4" type="button" value="Add New Vehicle">
                </form>
            </td>
        </tr>
        <tr>
            <td class="centerdata">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
