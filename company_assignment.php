<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("include/sajax.php");
    include("_functions.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");
        
    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    if (!hasAccess("COMP_ASSIGN"))
        header("Location: noaccess.php");

    ///////////////////////////
    //  Sajax
    function getCompanies($id) {
       return q("SELECT c.id, c.name FROM Company AS c WHERE c.id = '$id' ORDER BY c.name"); 
    }

	if ($_POST['saveInfo']){  	//save clicked
                $compList = "";
		$user = $_POST["user"];
		$comp = $_POST["comp"];
		$delete = q("DELETE FROM Company_Users WHERE user_id = '$user'");
		foreach($comp as $c)	{
                        $compList .= $c.",";
			$insert = q("INSERT INTO Company_Users (user_id,company_id) VALUES ('$user','$c')");
			$exist = q("SELECT id,roleid FROM user_role WHERE companyid = '$c' AND userid = '$user' AND roleid = (SELECT id FROM roles WHERE role = 'CL' AND companyid = '".$_SESSION["company_id"]."')");
			if (!is_array($exist))
                            $insert = q("INSERT INTO user_role(companyid,userid,roleid) VALUES ('$c','$user',(SELECT id FROM roles WHERE role = 'CL' AND companyid = '".$_SESSION["company_id"]."'))");//CLIENT

                        //  INSERT DEFAULT LEAVE FOR USER
			$exist = q("SELECT * FROM user_leave WHERE companyid = '$c' AND userid = '$user'");
			if (!is_array($exist))
                            $insert = q("INSERT INTO user_leave(companyid,userid) VALUES ('$c','$user')");

                        updateMenuIndex($c,$user);
		}

                if ($compList != "")    {
                    $compList = substr($compList, 0, -1);
                    $drop = q("DELETE FROM MenuIndex WHERE company_id NOT IN ($compList) AND user_id = $user");
                }
	}
    
    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0", "vehicles");
    
    $sajax_request_type = "GET";
    sajax_init();    
    sajax_export("getCompanies");    
    sajax_handle_client_request();
    ///////////////////////////
?>
<link rel="stylesheet" type="text/css" href="include/jquery.autocomplete.css" />
<script type='text/javascript' src='include/jquery.autocomplete.js'></script>
<script language="JavaScript">
    jQuery(function()    {
        jQuery("#user").change(function() {
            var userid = jQuery("#user").val();
            if(userid != "0")   {
                jQuery.post("_ajax.php", {func: "get_company_assign", userid: userid}, function(data)	{
                    data = json_decode(data);
                    jQuery("#data tbody>tr").not(':first').remove();    //remove current entries
                    jQuery("#companyDiv").show();	//make the table shown after the entries have been wiped
                    var last = jQuery("#data tbody>tr:last");
                    jQuery.each(data,function(i, v)  {
                        var n = last.clone(true);
                        n.children().eq(0).children().eq(0).text(v[1]);
                        n.children().eq(1).children().eq(0).val(v[0]);
                        n.insertBefore(last);
                    });
                    jQuery("#data tbody>tr").eq(-1).remove();    //remove current entries
                });
            }else{
                jQuery("#companyDiv").hide();
            }
        });

        ///////////////////////////////////
        //	Autocomplete
        //////////////////////////////////
        function findValue(li)  {
            if (li == null) return alert("No match!");
            var sValue = li.extra[0];
            var tValue = li.selectValue;
        }

        function selectItem(li) {
            findValue(li);
        }

        function setVariables(field, fieldValue, fieldClass)    {
            var companies = jQuery("#hiddenCompanies").val();
            jQuery.post("_ajax.php", {func: "setSessionVariable", sessionName: "companies", sessionValue: companies}, function(data){});
        }

        jQuery(".autocomplete").focus(function() {
            setVariables("companyName", jQuery("#hiddenCompanies").val(), "");
        });
    
       <?php
                jQAutoComplete("#", "companyName", "get_companies");
            ?>

        jQuery(".addCompanyBtn").click(function()    {
            var inputField = "#"+jQuery(this).attr("name");
            var inputValue = jQuery(inputField).val();
            if (inputValue != "")   {
                var query = "SELECT id,name FROM Company WHERE name = '" + addslashes(inputValue) + "'";
                jQuery.post("_ajax.php", {func: "query_direct", query: query}, function(data)    {
                    data = json_decode(data);
                    var comp_id = data[0][0];
                    var comp_name = data[0][1];
                    var last = jQuery("#data tbody>tr:last");
                    var first = jQuery("#data tbody>tr:first");
                    var n = last.clone(true);
                    n.children().eq(0).children().eq(0).text(comp_name);
                    n.children().eq(1).children().eq(0).val(comp_id);
                    n.insertBefore(first);
                    jQuery(inputField)[0].autocompleter.flushCache(); 
                });
            }
        });
        //////////////////////////////////////////////////////////////////////

        jQuery(".delRow").click(function()   {
            if (jQuery(this).parent().parent().parent().children().length > 1)
                jQuery(this).parent().parent().remove();
        }); 

    });    

</script>
    
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="assign_company">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Company Assignment</h6>
                            </td>
                        </tr>
                        <?php if($_POST['saveInfo']) echo "<tr><td class='centerdata'><br/><a style='color:#028200'> - User has been assigned - </a></td></tr>"; ?>
                        <tr>
                            <td>
                                <br/>
                            </td>
                        </tr>
                    </table>
                    
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                User Name:
                            </td>
                            <td width="50%">
                                <?php 
                                    $result = q("SELECT e.lstname, e.frstname, e.id FROM Employee AS e 
                                                        WHERE e.locked = '0' 
                                                        AND e.deleted = '0' 
                                                        AND e.email != 'admin' ORDER BY e.lstname,e.frstname");
                                    echo "<select class='on-field' id='user' name='user'></option>";
									echo "<option value='0'>-- Select a User --</option>";
                                    if (is_array($result))
                                        foreach($result as $r){
                                            echo "<option value=".$r[2].">".$r[0].","." "."".$r[1]."</option>";
                                        }
                                    echo "</select>";   // Closing of list box 
                                ?> 
                            </td>
                        </tr>
						<tr>
                        <td class="on-description">
                            Please start typing company name:
                        </td>
                        <td width="180px">
                            <table>
                                <tr>
                                    <td>
									<input id="companyName" name="companyName" type="text" class="on-field autocomplete" value="" />
                                    <td>
                                    <td>
									<img style="margin-top:6px" id="teamTbl" name="companyName" class="addCompanyBtn" src="images/icons/add.ico" title="Add" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    </table>
                    
                    <div id="companyDiv" style="display:none">
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Companies:
                                </td>
                                <td width="50%">
                                   <?php
                                        $result = q("SELECT id, name FROM Company ORDER BY name");
                                    ?>
                                    <table id="data" class="on-lefttable on-table">                                      
                                        <?php
                                            $counter = 0;
                                            foreach($result as $r){
                                                if( ($r[1] == "Admin") || ($r[1] == "admin") ){
                                                    // Do Nothing
                                                }else{
													echo "<tr>
														<td><div>".$r[1]."</div></td>
														<td><input id='comp[]' name='comp[]' type='hidden' value='".$r[0]."' /></td>
														<td><input class='delRow' type='button' value='Remove' /></td>
													</tr>
                                                    ";
                                                    $counter++;
                                                }
                                            }
                                           if(is_array($result))
                                               echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                                        ?>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                    
                    <br/>
                    <input name="btnSave" type="submit" value="Save">
                    <input name="saveInfo" type="hidden" value="saveInfo" />
                </form>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>

<?php
    //  Print Footer
    print_footer();
?>
