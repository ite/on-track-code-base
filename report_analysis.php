<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("_functions.php");
    include("fusion/FusionCharts.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    if (!hasAccess("REP_ANALYSIS"))
        header("Location: noaccess.php");

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("5", "reports");
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    jQuery(function(){
	jQuery('#since').DatePicker({
		format:'Y-m-d',
		date: jQuery('#since').val(),
		current: jQuery('#since').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#since').val()))
			_date = new Date();
			else _date = jQuery('#since').val();
			jQuery('#since').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#since').val(formated);
			jQuery('#since').DatePickerHide();
		}
	});
    });
</script>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="report_analysis">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Business Analysis Report</h6>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <?php
                        $generated = 0;

                        if ($_POST["since"] != "")      {
                            $since = addslashes(strip_tags($_POST["since"]));
                            $generated = 1;
                        }

                        if ($generated != "1") {
                    ?>
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                Proposal Projects Since:
                            </td>
                            <td width="50%">
                                <input class="on-field-date" id="since" name="since" type="text" style="text-align:right;" value="<?php
                                    if ($_POST["since"] != "") echo "".$_POST["since"]; else echo "".getPreviousMonthStart(6); ?>">
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <input name="btnGenerate" type="submit" value="Generate Report">
                    <?php
                        } else  {
                            ///////////////////////////
                            //  Get Information
                            ///////////////////////////
                            $proposedProjects = q("SELECT p.id, p.name, cot.consulting FROM Project as p INNER JOIN companiesOnTeam as cot ON p.id = cot.project_id ".
                                                    "WHERE p.completed = 0 AND cot.total_budget > 0 AND p.status = 'proposal' AND p.reg_date >= '".$since."' ".
                                                    "AND cot.company_id = '".$_SESSION["company_id"]."' ORDER BY p.name");
                            $projects = q("SELECT p.id, p.name, cot.consulting FROM Project as p INNER JOIN companiesOnTeam as cot ON p.id = cot.project_id ".
                                            "WHERE p.completed = 0 AND cot.consulting > 0 AND p.status = 'active' AND p.due_date != '' ".
                                            "AND cot.company_id = '".$_SESSION["company_id"]."' ORDER BY p.name");

                            $tableRow = "";
                            $xmlString = "<chart palette='1' caption='Business Analysis' shownames='1' showvalues='0' numberPrefix='".$_SESSION["currency"]."' ".
                                            "showSum='1' decimals='0' overlapColumns='0' formatNumberScale='0' formatNumber='0' ".fusionChart().">";

                            //  Categories
                            $xmlString .= "<categories>";
                                $xmlString .= "<category label='Proposed Projects'/>";
                                $xmlString .= "<category label='Work In Hand'/>";
                                $xmlString .= "<category label='Work In Process'/>";
                                $xmlString .= "<category label='Debtors'/>";
                                $xmlString .= "<category label='Cash In Hand'/>";
                            $xmlString .= "</categories>";

                            //  DataSet = Project With Values
                            if (is_array($proposedProjects)) {
                                foreach ($proposedProjects as $project) {
                                    $proj = removeSC($project[1]);
                                    $xmlString .= "<dataset seriesName='".$proj."' showValues='0'>";
                                        $proposed = $project[2];

                                        $proposed = number_format((double)$proposed, 2, ".", "");

                                        $xmlString .= "<set value='".$proposed."'/>";
                                        $xmlString .= "<set value='0'/>";
                                        $xmlString .= "<set value='0'/>";
                                        $xmlString .= "<set value='0'/>";
                                        $xmlString .= "<set value='0'/>";

                                        $tableRow .= "<tr><td>".$project[1]."</td>".
                                                    "<td class='rightdata'>".$proposed."</td>".
                                                    "<td class='rightdata'>-</td>".
                                                    "<td class='rightdata'>-</td>".
                                                    "<td class='rightdata'>-</td>".
                                                    "<td class='rightdata'>-</td></tr>";
                                    $xmlString .= "</dataset>";
                                }
                            }

                            //  DataSet = Project With Values
                            if (is_array($projects)) {
                                foreach ($projects as $project) {
                                    $proj = removeSC($project[1]);
                                    $xmlString .= "<dataset seriesName='".$proj."' showValues='0'>";
                                        $wih = $project[2];
                                        $wip = q("SELECT SUM(time_spent * rate) FROM TimeSheet WHERE project_id = '".$project[0]."' AND company_id = '".$_SESSION["company_id"]."'");
                                        $debtors = q("SELECT SUM(consulting) FROM LoadInvoices WHERE project_id = '".$project[0]."' AND paid = '0' AND company_id = '".$_SESSION["company_id"]."'");
                                        $cih = q("SELECT SUM(consulting) FROM LoadInvoices WHERE project_id = '".$project[0]."' AND paid = '1' AND company_id = '".$_SESSION["company_id"]."'");

                                        $wih = number_format(($wih - $wip), 2, ".", "");
                                        $wip = number_format(($wip - ($debtors + $cih)), 2, ".", "");
                                        $debtors = number_format($debtors, 2, ".", "");
                                        $cih = number_format($cih, 2, ".", "");

                                        $text_color = "color:#E6E6E6;";

                                        if ($wip < 0)       {
                                            $text_color = "color:#FF0000;";
                                        }

                                        if ($wih < 0)
                                            $wih = number_format(0, 2, ".", "");

                                        $xmlString .= "<set value='0'/>";
                                        $xmlString .= "<set value='".$wih."'/>";
                                        if ($wip >= 0)      $xmlString .= "<set value='".$wip."'/>";
                                        else                $xmlString .= "<set value='0'/>";
                                        $xmlString .= "<set value='".$debtors."'/>";
                                        $xmlString .= "<set value='".$cih."'/>";

                                        $tableRow .= "<tr><td>".$project[1]."</td>".
                                                    "<td class='rightdata'>-</td>".
                                                    "<td class='rightdata'>".$wih."</td>".
                                                    "<td class='rightdata' style='".$text_color."'>".$wip."</td>".
                                                    "<td class='rightdata'>".$debtors."</td>".
                                                    "<td class='rightdata'>".$cih."</td></tr>";
                                    $xmlString .= "</dataset>";
                                }
                            }

                            $xmlString .= "</chart>";

                            ///////////////////////////
                            //  Create Information String
                            ///////////////////////////
                            //  Table Headers
                            $headers = "<tr><th>Project Name</th>".
                                                "<th>Proposed <i>(".
                                                $_SESSION["currency"].")</i></th>".
                                                "<th>Work In Hand <i>(".
                                                $_SESSION["currency"].")</i></th>".
                                                "<th>Work In Process <i>(".
                                                $_SESSION["currency"].")</i></th>".
                                                "<th>Debtors <i>(".
                                                $_SESSION["currency"].")</i></th>".
                                                "<th>Cash In Hand <i>(".
                                                $_SESSION["currency"].")</i></th></tr>";

                            if ($tableRow != "") {
                                echo "<div class='on-20px'><table class='on-table-center on-table'>";
                                    echo "<tr><td colspan='6'>".renderChart("fusion/StackedColumn3D.swf", "", $xmlString, "multigraph", 800, 1200, false, false)."</td></tr>";
                                    echo "".$headers.$tableRow;
                                echo "</table></div>";
                            }
                            //////////////////////////
                        }
                    ?>
                </form>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
