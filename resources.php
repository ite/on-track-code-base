<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!hasAccess("RESOURCE_MAN"))
        header("Location: noaccess.php");

    $display_link = "resources.php?alphabet";

    //  Set Alphabet Letter
    $alphabet                                                           = "";
    $alphabet                                                           = $_GET["alphabet"];

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("1", "resources");
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata">
                <form action="" method="post" name="resources">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>
                                    Shared Resources List
                                </h6>
                            </td>
                        </tr>
                    </table>
                    <?php
                        echo "<br/>";
                        echo "| <a href='$display_link='>View All Resources</a> |";
                        echo "<br/><br/>";
                        echo "<a href='$display_link=A'>A</a> | <a href='$display_link=B'>B</a> | ".
                            "<a href='$display_link=C'>C</a> | <a href='$display_link=D'>D</a> | ".
                            "<a href='$display_link=E'>E</a> | <a href='$display_link=F'>F</a> | ".
                            "<a href='$display_link=G'>G</a> | <a href='$display_link=H'>H</a> | ".
                            "<a href='$display_link=I'>I</a> | <a href='$display_link=J'>J</a> | ".
                            "<a href='$display_link=K'>K</a> | <a href='$display_link=L'>L</a> | ".
                            "<a href='$display_link=M'>M</a> | <a href='$display_link=N'>N</a> | ".
                            "<a href='$display_link=O'>O</a> | <a href='$display_link=P'>P</a> | ".
                            "<a href='$display_link=Q'>Q</a> | <a href='$display_link=R'>R</a> | ".
                            "<a href='$display_link=S'>S</a> | <a href='$display_link=T'>T</a> | ".
                            "<a href='$display_link=U'>U</a> | <a href='$display_link=V'>V</a> | ".
                            "<a href='$display_link=W'>W</a> | <a href='$display_link=X'>X</a> | ".
                            "<a href='$display_link=Y'>Y</a> | <a href='$display_link=Z'>Z</a>";
                    ?>
                    <br/><br/>
                    <input name="btnAdd" onClick="location.href='resource_add.php';" tabindex="1" type="button" value="Add New Resource">
                    <br/><br/>
                    <!--  Headings   -->
                    <table style="width:25%" class="on-table-center on-table">
                        <tr>
                            <th>
                                Resources
                            </th>
                        </tr>
                        <!--  Table Information   -->
                        <?php
                            if ($_SESSION["email"] == "admin")
                                $resources = q("SELECT id, resource FROM Resources WHERE resource LIKE '$alphabet%' ORDER BY resource");
                            else
                                $resources = q("SELECT rs.id, rs.resource FROM (Resources AS rs INNER JOIN ResourceAssign AS ra  ON rs.id = ra.resource_id) ".
                                                "WHERE rs.resource LIKE '$alphabet%' AND ra.company_id = '".$_SESSION["company_id"]."' ORDER BY rs.resource");

                            if (is_array($resources))   {
                                foreach ($resources as $resource)       {
                                    echo "<tr>
                                                <td>
                                                    <a href='resource_edit.php?id=".$resource[0]."'>".$resource[1]."</a>
                                                </td>
                                            </tr>";
                                }
                            } else      {
                                echo "<tr>";
                                    echo "<td class='centerdata'>";
                                        if ($alphabet == "")
                                            echo "No resources available";
                                        else
                                            echo "No resources available under $alphabet";
                                    echo "</td>";
                                echo "</tr>";
                            }
                            echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        ?>
                    </table>
                    </div>
                    <br/>
                    <input name="btnAdd" onClick="location.href='resource_add.php';" tabindex="1" type="button" value="Add New Resource">
                </form>
            </td>
        </tr>
        <tr>
            <td>
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
