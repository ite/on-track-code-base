﻿<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!$_SESSION["eula_agree"] === true)
        header("Location: logout.php");
        
    if (seen($_SESSION["user_id"], "tutorial") == 1){
        //header("Location: home.php");
        echo "<script language='JavaScript'>location.href='home.php';</script>";
    }

    //  Print Header
    print_header();
    //  Print Menu
	if ($_GET["menu"] == "")
		print_menus("0", "home");
	else
		print_menus($_GET["menu"], "home");

	if ($_GET["menu"] == "") {
?>
<script language="JavaScript">
    jQuery(function(){
        // Timer - 40 Sec
        jQuery("#myButton").oneTime(40000,function(){
            jQuery('#myButton').val("Continue");
        });
        
        // Button Value Check
        jQuery("#myButton").click(function() {
            if(jQuery(this).val() == "Continue"){
                 jQuery.post("_ajax.php", {func: 'setSeen', video: 'tutorial'}, function(data){
                    data = json_decode(data);
                    location.href='home.php';  
                });
            }else{
                location.href='home.php';
            }
        });
    });
</script>
<table width="100%">
    <tr>
        <td class='centerdata'>
            <div style="z-index:1; position:relative; top:1; left:1;" id="flashContent">
                <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="900" height="506" id="intro_tutorial" align="middle">
                    <param name="movie" value="intro_tutorial.swf" />
                    <param name="quality" value="high" />
                    <param name="bgcolor" value="#ffffff" />
                    <param name="play" value="true" />
                    <param name="loop" value="false" />
                    <param name="wmode" value="transparent" />
                    <param name="scale" value="noborder" />
                    <param name="menu" value="false" />
                    <param name="devicefont" value="false" />
                    <param name="salign" value="" />
                    <param name="allowScriptAccess" value="sameDomain" />
                    <!--[if !IE]>-->
                    <object type="application/x-shockwave-flash" data="intro_tutorial.swf" width="900" height="506">
                        <param name="movie" value="intro_tutorial.swf" />
                        <param name="quality" value="high" />
                        <param name="bgcolor" value="#ffffff" />
                        <param name="play" value="true" />
                        <param name="loop" value="false" />
                        <param name="wmode" value="transparent" />
                        <param name="scale" value="noborder" />
                        <param name="menu" value="false" />
                        <param name="devicefont" value="false" />
                        <param name="salign" value="" />
                        <param name="allowScriptAccess" value="sameDomain" />
                        <!--<![endif]-->
                        <a href="http://www.adobe.com/go/getflash">
                            <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
                        </a>
                        <!--[if !IE]>-->
                        </object>
                    <!--<![endif]-->
                </object>
                <div style="z-index:-1; position:absolute; top:35%; width:100%;">
                    <table width="100%">
                        <tr>
                            <td class='centerdata'>
                                <img src="flashvideo/loading.gif"/><br/><br/>
                                <h2 style="font-family:Tahoma; ">Please be patient while the video loads...</h2>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td class="centerdata"><br>
            <input type='button' value='Skip Video' id="myButton">
        </td>
    </tr>
</table>
<br/>
<br/>
<?php
    }
    //  Print Footer
    print_footer();
?>
