<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    if (!hasAccess("AREA_MANAGE"))
        header("Location: noaccess.php");

    $index = 1;

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("1", "areas");
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    var areas_LOAD;
    var project = "<?php echo $_GET["project"]; ?>";

    jQuery(function()    {
        areas_LOAD = function(e)  {
            jQuery.post("_ajax.php", {func: "get_areas", pID: e}, function(data)	{
                data = json_decode(data);
                var displayBool = false;

                jQuery("#tableContent tbody>tr").not(":first").not(":last").remove();
                var lstRow = jQuery("#tableContent tbody>tr:last");

                jQuery.each(data,function(i, v)	{
                    var n = lstRow.clone(true);

                    n.children().eq(0).children().eq(0).text((v[1] != null) ? v[1] : "");
                    n.children().eq(0).children().eq(0).attr("id", (v[0] != null) ? v[0] : "");
                    n.children().eq(0).children().eq(0).attr("name", (v[0] != null) ? v[0] : "");

                    var subs = "";

                    jQuery.each(v[2],function(j, w)	{
                        var cls = (w[2] == 1) ? "active" : "inactive";
                        var val = (w[1] != null) ? w[1] : "";

                        if (w[1] == "-")        displayBool = false;
                        else                    displayBool = true;

                        subs += "&nbsp;<a class='"+cls+"'>"+val+"</a><br/>";
                    });

                    n.children().eq(1).html(subs);

                    for (var a = 0; a < 4; a++) {
                        var subs = "";

                        jQuery.each(v[2],function(j, w)	{
                            var val = (w[3 + a] != null) ? w[3 + a] : "";

                            subs += "&nbsp;"+val+"<br/>";
                        });

                        n.children().eq(2 + a).html(subs);
                    }

                    n.insertBefore(lstRow);
                });

                if (data != "") lstRow.remove();

                if (displayBool)        jQuery("#informationDiv").show();
                else                    jQuery("#informationDiv").hide();

                jQuery("#addBtnDiv").show();
            });
        };

        if (project != "" && validation("number", project))     areas_LOAD(project);

        jQuery("#project").change(function() {
            if (jQuery(this).val() != "null")        areas_LOAD(jQuery(this).val());
            else                                jQuery("#informationDiv").hide();

            if (jQuery(this).val() != "null")        jQuery("#addBtnDiv").show();
            else                                jQuery("#addBtnDiv").hide();
        });

        jQuery(".btnAdd").click(function() {
            jQuery("#id").val(jQuery("#project").val());
            jQuery("#form").attr("action", "areasAddEdit.php");
            jQuery("#form").submit();
        });

        jQuery(".btnEdit").click(function() {
            jQuery("#id").val(jQuery(this).attr("id"));
            jQuery("#form").attr("action", "areasAddEdit.php");
            jQuery("#form").submit();
        });
    });
</script>
<table width="100%">
    <tr height="380px">
        <td class="centerdata" valign="top">
            <form id="form" name="form" action="" method="post">
                <table width="100%">
                    <tr>
                        <td class="centerdata">
                            <h6> Project Areas</h6>
                        </td>
                    </tr>
                </table>
                <br/>
                <table width="100%">
                    <tr>
                        <td class="on-description" width="50%">
                            Project:
                        </td>
                        <td style="padding-top:2px;" width="50%">
                            <select class="on-field" id="project" name="project" method="post" tabindex="<?php echo $index++; ?>">
                                <option value="null">--  Select A Project  --&nbsp;&nbsp;</option>
                                <?php
                                    $projects = q("SELECT id, name FROM Project WHERE p_type = 'CP' AND completed = '0' AND company_id = '".$_SESSION["company_id"]."' ".
                                                    "ORDER BY name");

                                    if (is_array($projects))    {
                                        $isManager = q("SELECT ur.id,r.id FROM user_role AS ur INNER JOIN roles AS r ON ur.roleid = r.id WHERE r.role = 'MAN' AND ur.userid='".$_SESSION["user_id"]."' ".
                                                        "AND ur.companyid = '".$_SESSION["company_id"]."' AND r.companyid = '".$_SESSION["company_id"]."'");
                                        $isManager = ($isManager[0][0] != 0) ? 1 : 0;

                                        foreach ($projects as $project)      {
                                            if ($_SESSION["email"] != "admin" && !$isManager)	{
                                                $isPM = q("SELECT manager FROM Project_User WHERE user_id = '".$_SESSION["user_id"]."' AND project_id = '".$project[0]."' AND company_id = '".$_SESSION["company_id"]."'");

                                                if ($isPM == 1) $data[] = $project;
                                            }
                                            else
                                                $data[] = $project;
                                        }

                                        $projects = $data;
                                    }

                                    if (is_array($projects))
                                        foreach ($projects as $project)
                                            if ($_GET["project"] == $project[0])
                                                echo "<option value='".$project[0]."' selected>".$project[1]."&nbsp;&nbsp;</option>";
                                            else
                                                echo "<option value='".$project[0]."'>".$project[1]."&nbsp;&nbsp;</option>";
                                ?>
                            </select>
                        </td>
                    </tr>
                </table>
                <br/><br/>
                <div id="informationDiv" name="informationDiv" class="hidden" style='display:none'>
                    <div class="on-20px">
                    <table class="on-table-center on-table" id="tableContent" name="tableContent">
                        <tr>
                            <th>Project</th>
                            <th>Area(s)</th>
                            <th>Date Created</th>
                            <th>Created By</th>
                            <th>Date Updated</th>
                            <th>Updated By</th>
                        </tr>
                        <!--  Table Information   -->
                        <?php                    
                            echo "<tr>";
                                echo "<td style='white-space:nowrap;'>";
                                    echo "<a name='projLink' id='projLink' class='btnEdit' style='cursor: hand' tabindex='".$index++."'></a>";
                                    //echo "&nbsp;<a onMouseOver='showHand()' name='projLink' id='projLink' class='btnEdit' style='".$link."' tabindex='".$index++."'></a>&nbsp;";
                                echo "</td>";
                                echo "<td></td>";
                                echo "<td style='white-space:nowrap;'></td>";
                                echo "<td></td>";
                                echo "<td style='white-space:nowrap;'></td>";
                                echo "<td></td>";
                            echo "</tr><tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        ?>
                    </table>
                    </div>
                    <br/>
                </div>
                <div id="addBtnDiv" name="addBtnDiv" class="hidden" style='display:none'>
                    <input id="btnAdd" name="btnAdd" class="btnAdd" tabindex="<?php echo $index++; ?>" type="button" value="Add New">
                </div>
                <input id="id" name="id" type="hidden" value="">
            </form>
        </td>
    </tr>
    <tr>
        <td align="center">
            <br/>
        </td>
    </tr>
</table>
<?php
    //  Print Footer
    print_footer();
?>
