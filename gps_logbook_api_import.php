<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0", "bookings", "bookings");

    if ($errorMessage != "") {
        if ($errorMessage == "Please note that we are currently busy with data management, no entries are allowed for 2010-08-26. Please check back in 24 Hours.")
            echo "<p style='text-align:center'><font color='red'> - $errorMessage - </font></p>";
        else
            echo "<p style='text-align:center'><font class='on-validate-error'> - $errorMessage - </font></p>";
        echo "<br/>";
    }

?>

<script type="text/javascript">
	jQuery(function() {

		jQuery("#maskNotification").css({"width":jQuery(document).width(),"height":jQuery(document).height()});
		jQuery('#maskNotification').fadeIn("fast").fadeTo("fast",0.8);   //transition effect
		jQuery("#modal_gps_logbook").fadeIn();

		jQuery.post("_ajax.php", {func: "getGPSLogbookData", user_email: "<?php echo $_SESSION['email']; ?>", user_id: "<?php echo $_SESSION['user_id']; ?>"}, function(data) {
			data = json_decode(data);
			// Redirect to home.php
			window.location = "home.php";
		});
	});
</script>

<!--  BEGIN: GPS IMPORT NOTIFICATION MODAL  -->
	<div id="modal_gps_logbook" name="modal_gps_logbook" class="on-gps-logbook-notification-modal">
		<strong style="color:orange;font-weight:bold;font-size:20px;font-variant:small-caps;">Importing new GPS Logbook Data</strong>
		<br/>
		<br/>
		<br/>
		<img src="images/loading.gif" width="80px" height="80px" />
		<br/>
		<br/>
		<br/>
		<strong style="color:orange;font-weight:bold;font-size:14px;font-variant:small-caps;">This process cannot be skipped - Please be patient</strong>
	</div>
<!--  END: GPS IMPORT NOTIFICATION MODAL  -->

<?php
    //  Print Footer
    print_footer();
?>
