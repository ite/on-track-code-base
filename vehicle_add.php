<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

	if (!hasAccess("VEH_MANAGE"))
		header("Location: noaccess.php");

    //  Insert Vehicle Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")
    {
        $errorMessage                                                   = "";

        //  Get Information
        $vehicle_name                                                   = addslashes(strip_tags($_POST["vehicle_name"]));
        $vehicle_rate                                                   = addslashes(strip_tags($_POST["vehicle_rate"]));

        $vehicle_rate                                                   = number_format($vehicle_rate, 2, ".", "");

        //  Check If Vehicle Exists In Database
        $exist                                                          = q("SELECT id FROM Vehicle WHERE type = '$vehicle_name' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."'");

        if (!$exist)
        {
            $insert                                                     = q("INSERT INTO Vehicle (type, rate, company_id) VALUES ('$vehicle_name', '$vehicle_rate' , ".
                                                                            "'".$_SESSION["company_id"]."')");

            if ($insert)
            {
                $time                                                   = date("H:i:s");

                $logs                                                   = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('$vehicle_name created', 'Insert', 'Vehicle', '".$_SESSION["email"]."', ".
                                                                            "'$today', '$time', '".$_SESSION["company_id"]."')");

                $errorMessage                                           = "Vehicle Created Successfully";

                header("Location: vehicles.php");
            }
        }
        else
            $errorMessage                                               = "Vehicle Already Exists";
    }

    if ($errorMessage != "")
        echo "<p align='center' style='padding:0px;'><strong><font class='on-validate-error'>$errorMessage</font></strong></p>";

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0", "vehicles");
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    function check()
    {
        var valid                                                       = 1;

        //  Check That Company Name Is Entered
        if (document.forms["vehicle_add"].vehicle_name.value == "")
        {
            ShowLayer("vehicleName", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("vehicleName", "none");

        //  Check That Vehicle Rate Is Entered
        if (document.forms["vehicle_add"].vehicle_rate.value == "")
        {
            ShowLayer("vehicleRate", "none");
            ShowLayer("vehicleRateEmpty", "block");
            valid                                                       = 0;
        }
        //  Check That Vehicle Rate Is Valid
        else
        {
            ShowLayer("vehicleRateEmpty", "none");

            if (!validation("currency", document.forms["vehicle_add"].vehicle_rate.value))
            {
                ShowLayer("vehicleRate", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("vehicleRate", "none");
        }

        if (valid == 1)
        {
            document.forms["vehicle_add"].save.value                    = 1;
            document.forms["vehicle_add"].submit();
        }
    }
</script>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata">
                <form action="" method="post" name="vehicle_add">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>
                                    Add New Vehicle
                                </h6>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <br/>
                            </td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                    Vehicle Name:
                            </td>
                            <td width="50%">
                                <input class="on-field" name="vehicle_name" tabindex="1" size="60" type="text">
                                <div id="vehicleName" style="display: none;"><font class="on-validate-error">* Name must be entered</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                Rate/Km
                            </td>
                            <td width="50%">
                                <input class="on-field-date" name="vehicle_rate" style="text-align:right" tabindex="2" type="text">
                                <div id="vehicleRateEmpty" style="display: none;"><font class="on-validate-error">* Rate per Km must be entered</font></div>
                                <div id="vehicleRate" style="display: none;"><font class="on-validate-error">* Entered amount must be numeric, eg. 123.45</font></div>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <input name="btnAdd" onClick="check();" tabindex="3" type="button" value="Add Vehicle">
                    <input method="post" name="save" type="hidden" value="0" />
                </form>
            </td>
        </tr>
        <tr>
            <td>
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
