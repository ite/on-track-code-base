<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("_functions.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    if (!hasAccess("INVOICE_MANAGE") && !hasAccess("INVOICE_PROCESS"))
        header("Location: noaccess.php");

    if (hasAccess("INVOICE_MANAGE"))
        $RA_VIEW = 1;

    $project_id = "null";

    if (isset($_POST["project"]))
        $project_id = $_POST["project"];

    //  Update Invoice Information Function
    if (isset($_POST["update"]) && $_POST["update"] === "1")    {
        $invoices = q("SELECT li.id, li.create_date, p.name, li.name, ((li.consulting + li.expenses) + ((li.consulting + li.expenses) * (li.vat / 100))), li.amountPaid, li.due_date, li.paid, li.processed ".
                        "FROM (LoadInvoices AS li INNER JOIN Project AS p ON li.project_id = p.id) ".
                        "WHERE p.company_id = '".$_SESSION["company_id"]."' AND li.paid = '0' ORDER BY li.due_date");

        if (is_array($invoices)){
            foreach($invoices as $i)      {
                $invTotal = addslashes(strip_tags($_POST["invTotal".$i[0]]));
                $invPaid = addslashes(strip_tags($_POST["invPaid".$i[0]]));
                $dueDate = addslashes(strip_tags($_POST["date".$i[0]]));
                $paid = addslashes(strip_tags($_POST["paid".$i[0]]));
                $amountPaid = addslashes(strip_tags($_POST["amountPaid".$i[0]]));
                $processed = addslashes(strip_tags($_POST["processed".$i[0]]));

                $paid = ($paid == "") ? 0 : 1;
                $processed = ($processed == "") ? 0 : 1;

                $amountPaid = $invPaid + $amountPaid;

                //  TODO: Invoice payment ported to invoice engine

                $update = q("UPDATE LoadInvoices SET due_date = '".$dueDate."', paid = '".$paid."', processed = '".$processed."', amountPaid = '".$amountPaid."' WHERE id = '".$i[0]."'");
            }
        }

        header("Location: invoice_processing.php");
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("2", "budget");

    if ($errorMessage != "")    echo "<p style='width:100%; text-align:center'><strong><font class='on-validate-error'>$errorMessage</font></strong></p>";
?>
<script language="JavaScript">
    jQuery(function()   {
	jQuery(".paid").change(function(){
            var invPaidChecked = jQuery(this).attr("checked");
            var invID = jQuery(this).attr("id").replace("paid", "");

            var invTotal = jQuery("#invTotal"+invID).val();
            var invPaid = jQuery("#invPaid"+invID).val();

            if (invPaidChecked) {
                var invDiff = invTotal - invPaid;
                jQuery("#amountPaid"+invID).val(invDiff.toFixed(2));
            }
            else
                jQuery("#amountPaid"+invID).val("");
        });
    });

    function updateInfo()       {
        document.forms["invoice_load"].update.value = 1;
        document.forms["invoice_load"].submit();
    }
</script>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="invoice_load">
                    <table class="on-table-center" style="width:100%;">
                        <tr>
                            <td width="100%"class="centerdata">
                                <h6>Unpaid Invoice Processing</h6>
                            </td>
                        </tr>
                        <tr>
                            <td >
                                <br/>
                            </td>
                        </tr>
                        <tr>
                    </table>
                    <br/>
                    <?php
                        $invoices = q("SELECT li.id, li.create_date, p.name, li.name, ((li.consulting + li.expenses) + ((li.consulting + li.expenses) * (li.vat / 100))), li.amountPaid, li.due_date, li.paid, li.processed ".
                                        "FROM (LoadInvoices AS li INNER JOIN Project AS p ON li.project_id = p.id) ".
                                        "WHERE p.company_id = '".$_SESSION["company_id"]."' AND li.paid = '0' ORDER BY li.due_date");

                        if (is_array($invoices))    {
                            echo "<table class='on-table'>";
                                //  Table Headings
                                echo "<tr>";
                                    echo "<th>Date<br/>Created</th>";
                                    echo "<th>Project<br/>Name</th>";
                                    echo "<th>Invoice<br/>Name</th>";
                                    echo "<th>Total<br/>Amount <i>(".$_SESSION["currency"].")</i></th>";
                                    echo "<th>Total Already<br/>Paid <i>(".$_SESSION["currency"].")</i></th>";
                                    echo "<th>Due<br/>Date</th>";
                                    echo "<th>Paid</th>";
                                    echo "<th>Amount<br/>Paid <i>(".$_SESSION["currency"].")</i></th>";
                                    echo "<th>Processed</th>";
                                echo "</tr>";
                                //  Table Information
                                foreach ($invoices as $invoice)     {
                                    echo "<tr>";
                                        echo "<td style='white-space:nowrap; width:80px'>".$invoice[1]."</td>";
                                        echo "<td style='white-space:nowrap;'>".$invoice[2]."</td>";

                                        echo "<td style='white-space:nowrap;'>";
                                        if (!$RA_VIEW)      echo "".$invoice[3]."";
                                        else                echo "<a href='invoice_edit.php?id=".$invoice[0]."'>".$invoice[3]."</a>";
                                        echo "</td>";

                                        echo "<td style='white-space:nowrap;' class='rightdata'><input id='invTotal".$invoice[0]."' name='invTotal".$invoice[0]."' type='text' style='text-align:right;' readonly value='".
                                                number_format((double)$invoice[4], 2, ".", "")."'></td>";
                                        echo "<td style='white-space:nowrap;' class='rightdata'><input id='invPaid".$invoice[0]."' name='invPaid".$invoice[0]."' type='text' style='text-align:right;' readonly value='".
                                                number_format((double)$invoice[5], 2, ".", "")."'></td>";
                                        echo "<td style='white-space:nowrap;' class='rightdata'><input id='date".$invoice[0]."' name='date".$invoice[0]."' type='text' style='text-align:right;' value='".$invoice[6]."'></td>";

                                        $checked = ($invoice[7] == "1") ? "checked" : "";
                                        echo "<td style='white-space:nowrap;' class='centerdata'><input id='paid".$invoice[0]."' name='paid".$invoice[0]."' class='paid' type='checkbox' value='1' $checked></td>";

                                        echo "<td style='white-space:nowrap;' class='rightdata'><input id='amountPaid".$invoice[0]."' name='amountPaid".$invoice[0]."' type='text' style='text-align:right;' value=''></td>";

                                        $checked = ($invoice[8] == "1") ? "checked" : "";
                                        echo "<td style='white-space:nowrap;' class='centerdata'><input id='processed".$invoice[0]."' name='processed".$invoice[0]."' type='checkbox' value='1' $checked></td>";
                                    echo "</tr>";
                                }
                                echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";

                            echo "</table>";
                            echo "<center><br/><input name='btnUpdate' onClick='updateInfo();' type='button' value='Update Information'></center>";
                        }
                    ?>
                    <br/>
                    <input method="post" name="save" type="hidden" value="0" />
                    <input method="post" name="update" type="hidden" value="0" />
                </form>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
