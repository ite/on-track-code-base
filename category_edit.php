<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

	if (!hasAccess("BUDGET_CAT_MANAGE"))
		header("Location: noaccess.php");

    //  Update Category And Elements Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")        {
        $errorMessage = "";

        //  Get Information
        $id = $_GET["id"];
        $category_info_old = q("SELECT name FROM Category WHERE id = '$id'");
        $category_info_new = addslashes(strip_tags($_POST["category_name"]));

        //  nElements                                                   = Number of Elements per Category
        $nElements = q("SELECT COUNT(id) FROM Elements WHERE parent_id = '$id'");
        $elements = q("SELECT id, name, status FROM Elements WHERE parent_id = '$id' ORDER BY name");

        //  Check If Activity Type Exists In Database
        if ($category_info_old != $category_info_new)   {
            $exist = q("SELECT id FROM Category WHERE name = '$category_info_new' AND company_id = '".$_SESSION["company_id"]."'");

            $do_update = true;
        }
        else    {
            $exist = false;

            $do_update = false;
        }

        if (!$exist)    {
            //  Update Activity Type If Necessary
            if ($do_update)
                $update = q("UPDATE Category SET name = '$category_info_new' WHERE id = '$id'");

            //  Elements
            if ($nElements > 1) {
                foreach ($elements as $element) {
                    $element_name_old = $element[1];
                    $element_name_new = addslashes(strip_tags($_POST["element_name_$element[0]"]));
                    $element_status = $_POST["box_$element[0]"];

                    if ($element_status == "")
                        $element_status = 0;

                    if ($element_name_new != "")
                        $update = q("UPDATE Elements SET name = '$element_name_new', status = '$element_status' WHERE id = '$element[0]'");
                }

                //  New Element
                $element_name = addslashes(strip_tags($_POST["element_name_".($nElements + 1).""]));

                if ($element_name != "")        {
                    $insert = q("INSERT INTO Elements (parent_id, name, status) VALUES ('$id', '$element_name', '1')");
                    $getID = q("SELECT id FROM Elements WHERE parent_id = '$id' AND name = '$element_name' AND status = '1'");

                    $yearMonth = q("SELECT DISTINCT(CONCAT(year, '-', month)), year, month FROM Budget WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY CONCAT(year, '-', month) ASC");

                    if (is_array($yearMonth))       {
                        foreach ($yearMonth as $yM) {
                            $insert = q("INSERT INTO Budget (year, month, element_id, planned, amount, actual, due_date, company_id) ".
                                        "VALUES ('".$yM[1]."', '".$yM[2]."', '".$getID."', '', '', '0', '', '".$_SESSION["company_id"]."')");
                        }
                    }
                }
            }
            else if ($nElements == 1)   {
                $element_name_old = $elements[0][1];
                $element_name_new = addslashes(strip_tags($_POST["element_name_$elements[0][0]"]));
                $element_status = $_POST["box_$elements[0][0]"];

                if ($element_status == "")
                    $element_status = 0;

                if ($element_name_new != "")
                    $update = q("UPDATE Elements SET name = '$element_name_new', status = '$element_status' WHERE id = '$elements[0][0]'");

                //  New SElement
                $element_name = addslashes(strip_tags($_POST["element_name_".($nElements + 1).""]));

                if ($element_name != "")        {
                    $insert = q("INSERT INTO Elements (parent_id, name, status) VALUES ('$id', '$element_name', '1')");
                    $getID = q("SELECT id FROM Elements WHERE parent_id = '$id' AND name = '$element_name' AND status = '1'");

                    $yearMonth = q("SELECT DISTINCT(CONCAT(year, '-', month)), year, month FROM Budget WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY CONCAT(year, '-', month) ASC");

                    if (is_array($yearMonth))       {
                        foreach ($yearMonth as $yM) {
                            $insert = q("INSERT INTO Budget (year, month, element_id, planned, amount, actual, due_date, company_id) ".
                                        "VALUES ('".$yM[1]."', '".$yM[2]."', '".$getID."', '', '', '0', '', '".$_SESSION["company_id"]."')");
                        }
                    }
                }
            }
            else        {
                for ($i = 1; $i <= 5; $i++)     {
                    $element_name = addslashes(strip_tags($_POST["element_name_$i"]));

                    if ($element_name != "")    {
                        $insert = q("INSERT INTO Elements (parent_id, name, status) VALUES ('$id', '$element_name', '1')");
                        $getID = q("SELECT id FROM Elements WHERE parent_id = '$id' AND name = '$element_name' AND status = '1'");

                        $yearMonth = q("SELECT DISTINCT(CONCAT(year, '-', month)), year, month FROM Budget WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY CONCAT(year, '-', month) ASC");

                        if (is_array($yearMonth))       {
                            foreach ($yearMonth as $yM) {
                                $insert = q("INSERT INTO Budget (year, month, element_id, planned, amount, actual, due_date, company_id) ".
                                            "VALUES ('".$yM[1]."', '".$yM[2]."', '".$getID."', '', '', '0', '', '".$_SESSION["company_id"]."')");
                            }
                        }
                    }
                }
            }

            if ($errorMessage == "")    {
                $time = date("H:i:s");

                $logs = q("INSERT INTO Logs (what,access,on_table,by_user,on_date,on_time,company_id) ".
                            "VALUES ('$category_info_new and elements updated','Update','Category','".$_SESSION["email"]."','$today','$time','".$_SESSION["company_id"]."')");

                header("Location: categories.php");
            }
        }
        else
            $errorMessage = "Category Already Exists";
    }

    if ($errorMessage != "")
        echo "<p align='center' style='padding:0px;'><strong><font color='#999999'>$errorMessage</font></strong></p>";

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("2", "budget");
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    var cal                                                             = new CodeThatCalendar(caldef1);

    function check(box)
    {
        var valid                                                       = 1;

        //  Check That Project Type Is Entered
        if (document.forms["category_edit"].category_name.value == "")
        {
            ShowLayer("categoryName", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("categoryName", "none");

        if (valid == 1)
        {
            document.forms["category_edit"].save.value                  = 1;
            document.forms["category_edit"].submit();
        }
    }
</script>
<?php
    $id = $_GET["id"];
    $category_info = q("SELECT name FROM Category WHERE id = '$id'");

    //  nElements = Number of Elements per Category
    $nElements = q("SELECT COUNT(id) FROM Elements WHERE parent_id = '$id'");
    $elements = q("SELECT id, name, status FROM Elements WHERE parent_id = '$id' ORDER BY name");

    $index = 2;
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="category_edit">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Edit Category</h6>
                            </td>
                        </tr>
                        <tr>
                            <td class="centerdata">
                                <br/>
                            </td>
                        </tr>
                    </table>
                    <table class="on-table-center">
                        <tr>
                            <td class="on-description-left">Category</td>
                            <td class="on-description-left">Element</td>
                            <td class="on-description-center">Active</td>
                        </tr>
                        <tr>
                            <td class="rightdata" valign="top" rowspan="100%">
                                <input class="on-field" name="category_name" tabindex="1" type="text" value="<?php echo $category_info; ?>">
                                <div id="categoryName" style="display: none;"><font style="margin-right:45px;" class='on-validate-error'>* Category must be entered</font></div>
                            </td>
                            <?php
                                if (is_array($elements)) {
                                    $count = 0;

                                    foreach ($elements as $element)     {
                                        ?>
                                        <td class="rightdata">
                                        <?php
                                            echo "<input class='on-field' id='element_name_".$element[0]."' name='element_name_".$element[0]."' tabindex='".($index++)."' type='text' value='".$element[1]."'>";
                                        ?>
                                        </td>
                                        <td class="centerdata" style="padding-top:8px;">
                                        <?php
                                            if ($element[2] == "1")
                                                echo "<input id='box_".$element[0]."' name='box_".$element[0]."' type='checkbox' value='1' checked>";
                                            else
                                                echo "<input id='box_".$element[0]."' name='box_".$element[0]."' type='checkbox' value='1'>";
                                        ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <?php
                                    }
                                    ?>
                                        <td class="rightdata">
                                        <?php
                                            echo "<input class='on-field' id='element_name_".($nElements + 1)."' name='element_name_".($nElements + 1)."' tabindex='".($index++)."' type='text'>";
                                        ?>
                                        </td>
                                        <td class="centerdata">
                                        </td>
                                    </tr>
                                    <?php
                                } else  {
                                    for ($i = 1; $i <= 5; $i++) {
                                    ?>
                                        <td class="rightdata">
                                        <?php
                                            echo "<input class='on-field' id='element_name_".$i."' name='element_name_".$i."' tabindex='".($index++)."' type='text'>";
                                        ?>
                                        </td>
                                        <td class="centerdata">
                                        </td>
                                    </tr>
                                    <?php
                                        if ($i < 5)
                                            echo "<tr>";
                                    }
                                }
                            ?>
                    </table>
                    <br/>
                    <input name="btnAdd" onClick="check();" tabindex="<?php echo "".($index++); ?>" type="button" value="Update Category">
                    <input method="post" name="save" type="hidden" value="0" />
                </form>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
