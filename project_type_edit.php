<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

	if (!hasAccess("PROJTYPE_MANAGE"))
		header("Location: noaccess.php");

    //  Update Project Type Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")
    {
        $errorMessage                                                   = "";

        //  Get Information
        $id                                                             = $_GET["id"];
        $project_type_name                                              = addslashes(strip_tags($_POST["project_type_name_new"]));

        //  Check If Project Type Exists In Database
        $exist                                                          = q("SELECT id FROM ProjectTypes WHERE name = '$project_type_name' AND id != '$id' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."'");

        if (!$exist)
        {
            $update                                                     = q("UPDATE ProjectTypes SET type = '$project_type_name' WHERE id = '$id'");

            if ($update)
            {
                $time                                                   = date("H:i:s");

                $logs                                                   = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('$project_type_name updated', 'Update', 'ProjectTypes', '".$_SESSION["email"]."', ".
                                                                            "'$today', '$time', '".$_SESSION["company_id"]."')");

                header("Location: project_type_management.php");
            }
        }
        else
            $errorMessage                                               = "Project Type Already Exists";
    }

    if ($errorMessage != "")
        echo "<p align='center' style='padding:0px;'><strong><font class='on-validate-error'>$errorMessage</font></strong></p>";

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("1", "project_types");
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    function check()
    {
        var valid                                                       = 1;

        //  Check That Project Type Is Entered
        if (document.forms["project_type_edit"].project_type_name_new.value == "")
        {
            ShowLayer("projectTypeName", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("projectTypeName", "none");

        if (valid == 1)
        {
            document.forms["project_type_edit"].save.value              = 1;
            document.forms["project_type_edit"].submit();
        }
    }
</script>
<?php
    $id                                                                 = $_GET["id"];
    $project_type_info                                                  = q("SELECT type FROM ProjectTypes WHERE id = '$id'");
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="project_type_edit">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Edit Project Type</h6>
                            </td>
                        </tr>
                        <tr>
                            <td class="centerdata">
                                <br/>
                            </td>
                        </tr>
                    </table>
                    <table cellpadding="0" cellspacing="2" width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                    Old Project Type:
                            </td>
                            <td width="50%">
                                <input class="on-field" name="project_type_name_old" readonly type="text" value="<?php echo $project_type_info; ?>">
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                    New Project Type:
                            </td>
                            <td align="left" width="50%">
                                <input class="on-field" name="project_type_name_new" tabindex="1" type="text">
                                <div id="projectTypeName" style="display: none;"><font class="on-validate-error">* Type must be entered</font></div>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <input name="btnUpdate" onClick="check();" tabindex="2" type="button" value="Update Project Type">
                    <input method="post" name="save" type="hidden" value="0" />
                </form>
            </td>
        </tr>
        <tr>
            <td class="centerdata">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
