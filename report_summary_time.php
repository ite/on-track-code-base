<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("_functions.php");
    include("fusion/FusionCharts.php");
    include("include/sajax.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    if (!hasAccess("REP_SUMMARY_TIME"))
        header("Location: noaccess.php");

    // Class for table background color
    $colorClass = "OnTrack_#_";

    ///////////////////////////
    //  Sajax
    function getProjects($status, $type) {
        if ($status == "all")   $status = " AND p.completed IN (0,1)";
        else                    $status = " AND p.completed = '".$status."'";

        if ($type == "null")
            $projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p
                            INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id)
                            WHERE cot.company_id = '".$_SESSION["company_id"]."'
                            AND p.deleted = '0' $status ORDER BY UPPER(p.name)");
        else
            $projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p
                            INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id)
                            WHERE cot.company_id = '".$_SESSION["company_id"]."'
                            AND p.deleted = '0'
                            AND p.type_id='$type' $status ORDER BY UPPER(p.name)");

        return $projects;
    }

    $sajax_request_type                                                 = "GET";
    sajax_init();
    sajax_export("getProjects");
    sajax_handle_client_request();
    ///////////////////////////

    //  Set Report Generated Status
    $generated                                                          = "0";

    $myData;
    $row = 0;
    $employeeCounter = 0;
    $columnCounter = 0;

    function createPieChartXML($activityTypes,$chartTotals)     {
        //  Create XML String
        $xmlString = "";

        //  Calculate Values
        if (is_array($activityTypes)) {
            $xmlString = "<chart caption='Summary Report (Time Based)' palette='4' decimals='2' enableSmartLabels='1' showBorder='1' ".fusionChart().">";

            foreach ($activityTypes as $activityType)   {
                $ATName = removeSC($activityType[1]);

                $xmlString .= "<set label='".$ATName."' value='".$chartTotals[$activityType[0]]."'/>";
            }

            $xmlString .= "</chart>";
        }

        return $xmlString;
    }

    function createRow($employee_id, $project_id, $project_Type, $report_type, $date_from, $date_to, $projectStatus, $table, $where) {
        GLOBAL $myData;
        GLOBAL $row;

        $rowString = "";
        $total = 0;

        if ($projectStatus == "all")    $projectStatus = " AND p.completed IN (0,1) ";
        else                            $projectStatus = $projectStatus;

        if ($project_id == "all") {     // If all Projects
            if ($project_Type == "both") {
                $activityTypes = q("SELECT DISTINCT(at.id), at.type FROM (ActivityTypes AS at INNER JOIN Activities AS a ".
                                    "ON at.id = a.parent_id INNER JOIN $table AS ts ON a.id = ts.activity_id ".
                                    "INNER JOIN Project AS p ON p.id = ts.project_id) ".
                                    "WHERE ts.projectType = 'CP' ".
                                    //at.company_id = '".$_SESSION["company_id"]."' AND
                                    "AND ts.date >= '$date_from' ".
                                    "AND ts.date <= '$date_to' AND ts.company_id = '".$_SESSION["company_id"]."' $projectStatus $where ".
                                    "ORDER BY type");
            }
            else if ($project_Type == "invoicable") {
                $activityTypes = q("SELECT DISTINCT(at.id), at.type FROM (ActivityTypes AS at INNER JOIN Activities AS a
                                        ON at.id = a.parent_id INNER JOIN $table AS ts ON a.id = ts.activity_id
                                        INNER JOIN companiesOnTeam AS cot ON ts.project_id = cot.project_id
										INNER JOIN Project AS p ON p.id = ts.project_id)
                                        WHERE ".
                                        //at.company_id = '".$_SESSION["company_id"]."' AND
                                        "ts.date >= '$date_from'
                                        AND ts.date <= '$date_to'
                                        AND ts.company_id = '".$_SESSION["company_id"]."'
                                        AND ts.projectType ='CP' $projectStatus $where AND cot.total_budget > 0 ORDER BY type");
            }
            else {
                $activityTypes = q("SELECT DISTINCT(at.id), at.type FROM (ActivityTypes AS at INNER JOIN Activities AS a ".
                                    "ON at.id = a.parent_id INNER JOIN $table AS ts ON a.id = ts.activity_id ".
                                    "INNER JOIN companiesOnTeam AS cot ON ts.project_id = cot.project_id INNER JOIN Project AS p ON p.id = ts.project_id) ".
                                    "WHERE ts.projectType = 'CP' ".
                                    //at.company_id = '".$_SESSION["company_id"]."' AND
                                    "AND ts.date >= '$date_from' ".
                                    "AND ts.date <= '$date_to' AND ts.company_id = '".$_SESSION["company_id"]."' $projectStatus $where ".
                                    "AND cot.total_budget = '' ORDER BY type");
            }
        }
        else {
            $activityTypes = q("SELECT DISTINCT(at.id), at.type FROM (ActivityTypes AS at INNER JOIN Activities AS a ".
                                "ON at.id = a.parent_id INNER JOIN $table AS ts ON a.id = ts.activity_id) ".
                                "WHERE ".
                                //at.company_id = '".$_SESSION["company_id"]."' AND
                                "ts.date >= '$date_from' ".
                                "AND ts.date <= '$date_to' AND ts.project_id = '$project_id' $where ORDER BY type");
        }

        if (is_array($activityTypes))
        {
            foreach ($activityTypes as $activityType) {
                if ($project_id == "all") {
                    if ($project_Type == "both") {
                        if ($report_type == "time")
                            $spent = q("SELECT SUM(ts.time_spent) FROM ($table AS ts INNER JOIN Activities AS a ".
                                        "ON ts.activity_id = a.id INNER JOIN Project AS p ON ts.project_id = p.id) ".
                                        "WHERE a.parent_id = '".$activityType[0]."' AND ts.user_id = '$employee_id' ".
                                        "AND ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                        "AND ts.projectType = 'CP' AND ts.company_id = '".$_SESSION["company_id"]."' $projectStatus $where");
                        else
                            $spent = q("SELECT SUM(ts.time_spent * ts.rate) FROM ($table AS ts INNER JOIN Activities AS a ".
                                        "ON ts.activity_id = a.id INNER JOIN Project AS p ON ts.project_id = p.id) ".
                                        "WHERE a.parent_id = '".$activityType[0]."' AND ts.user_id = '$employee_id' ".
                                        "AND ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                        "AND ts.projectType = 'CP' AND ts.company_id = '".$_SESSION["company_id"]."' $projectStatus $where");
                    }
                    else if ($project_Type == "invoicable") {
                        if ($report_type == "time")
                            $spent = q("SELECT SUM(ts.time_spent)
										FROM ($table AS ts
											INNER JOIN Activities AS a ON ts.activity_id = a.id
											INNER JOIN companiesOnTeam AS cot ON ts.project_id = cot.project_id
											INNER JOIN Project AS p ON ts.project_id = p.id)
										WHERE a.parent_id = '".$activityType[0]."'
											AND ts.user_id = '$employee_id'
											AND ts.date >= '$date_from'
											AND ts.date <= '$date_to'
											AND cot.total_budget > 0
											AND ts.projectType = 'CP'
											AND ts.company_id = '".$_SESSION["company_id"]."' $projectStatus $where");
						else
                            $spent = q("SELECT SUM(ts.time_spent * ts.rate)
										FROM ($table AS ts
											INNER JOIN Activities AS a ON ts.activity_id = a.id
											INNER JOIN companiesOnTeam AS cot ON ts.project_id = cot.project_id
											INNER JOIN Project AS p ON ts.project_id = p.id)
										WHERE a.parent_id = '".$activityType[0]."'
											AND ts.user_id = '$employee_id'
											AND ts.date >= '$date_from'
											AND ts.date <= '$date_to'
											AND cot.total_budget > 0
											AND ts.projectType = 'CP'
											AND ts.company_id = '".$_SESSION["company_id"]."' $projectStatus $where");
                    }
                    else {
                        if ($report_type == "time")
                            $spent = q("SELECT SUM(ts.time_spent) FROM ($table AS ts INNER JOIN Activities AS a ".
                                        "ON ts.activity_id = a.id INNER JOIN companiesOnTeam AS cot ON ts.project_id = cot.project_id INNER JOIN Project AS p ON ts.project_id = p.id) ".
                                        "WHERE a.parent_id = '".$activityType[0]."' AND ts.user_id = '$employee_id' ".
                                        "AND ts.date >= '$date_from' AND ts.date <= '$date_to' AND cot.total_budget = '' ".
                                        "AND ts.projectType = 'CP' AND ts.company_id = '".$_SESSION["company_id"]."' $projectStatus $where");
                        else
                            $spent = q("SELECT SUM(ts.time_spent * ts.rate) FROM ($table AS ts INNER JOIN Activities AS a ".
                                        "ON ts.activity_id = a.id INNER JOIN companiesOnTeam AS cot ON ts.project_id = cot.project_id INNER JOIN Project AS p ON ts.project_id = p.id) ".
                                        "WHERE a.parent_id = '".$activityType[0]."' AND ts.user_id = '$employee_id' ".
                                        "AND ts.date >= '$date_from' AND ts.date <= '$date_to' AND cot.total_budget = '' ".
                                        "AND ts.projectType = 'CP' AND ts.company_id = '".$_SESSION["company_id"]."' $projectStatus $where");
                    }
                }
                else {
                    if ($report_type == "time"){
                        $spent = q("SELECT SUM(ts.time_spent) FROM ($table AS ts
                                            INNER JOIN Activities AS a ON ts.activity_id = a.id)
                                            WHERE a.parent_id = '".$activityType[0]."'
                                            AND ts.user_id = '".$employee_id."' AND ts.date >= '".$date_from."'
                                            AND ts.date <= '".$date_to."' AND ts.project_id = '$project_id' AND ts.company_id = '".$_SESSION["company_id"]."' $where");
                    }else{
                        $spent = q("SELECT SUM(ts.time_spent * ts.rate) FROM ($table AS ts INNER JOIN Activities AS a ".
                                    "ON ts.activity_id = a.id) WHERE a.parent_id = '".$activityType[0]."' ".
                                    "AND ts.user_id = '".$employee_id."' AND ts.date >= '".$date_from."' ".
                                    "AND ts.date <= '".$date_to."' AND ts.project_id = '$project_id' AND ts.company_id = '".$_SESSION["company_id"]."' $where");
                    }
                }

                $total += $spent;

                if ($spent){                // 1 -> ...
                    $rowString .= "<td class='rightdata'>".number_format($spent, 2, ".", "")."</td>";
                    $myData[$row][] = $spent;
                }else{                      // 1 -> ...
                    $rowString .= "<td class='rightdata'>-</td>";
                    $myData[$row][] = "-";
                }
            }
        }

        $rowString .= "<td class='rightdata'>".number_format($total, 2, ".", "")."</td>";
        $myData[$row][] = $total;           // LAST
        $row++;

        return $rowString;
    }
    //  Create Display Information Function
    if (isset($_POST["save"]) && $_POST["save"] === "1") {
        unset($chartTotals);

        $errorMessage = "";

        $reportOn = $_POST["reportOn"];
        $projectStatus = $_POST["projectStatus"];

        $project_type_id = $_POST["projectType"];
        $project_id = $_POST["project"];
        $report_type = $_POST["reportType"];
        $date_from = addslashes(strip_tags($_POST["date_from"]));
        $date_to = addslashes(strip_tags($_POST["date_to"]));
        $displayString = "";
        $where = "";

        if ($project_type_id != "null" && is_numeric($project_type_id))
            $where .= "AND p.type_id = '$project_type_id' ";

        $table = ($reportOn == "approved") ? "ApprovedTime" : "TimeSheet";
        $where .= ($reportOn == "approved") ? "AND ts.status = '2' " : "";

        ///////////////////////////
        //  Get Info From Tables
        ///////////////////////////
        if ($project_id == "all") {
            $project_name = "All Projects <a class='on-validate-error'>(Shared Projects not included)</a>";
            $project_Type = $_POST["projectType2"];
        }else{
            $project_name = q("SELECT name From Project WHERE id = '$project_id'");
            $row++;
            $myData[$row][] = "Project: ".$project_name;        // col 1
        }

        if ($projectStatus == "all")    $projectStatus = " AND p.completed IN (0,1) ";
        else                            $projectStatus = " AND p.completed = '".$projectStatus."' ";

        if ($project_id == "all") {
            if ($project_Type == "both") {
                $employees = q("SELECT DISTINCT(e.id), e.frstname, e.lstname, e.email FROM (Employee AS e
                                        INNER JOIN Company_Users AS cu ON e.id = cu.user_id
                                        INNER JOIN $table AS ts ON e.id = ts.user_id
                                        INNER JOIN Project AS p ON p.id = ts.project_id)
                                        WHERE cu.company_id = '".$_SESSION["company_id"]."'
                                        AND e.email != 'admin'
                                        AND ts.company_id = '".$_SESSION["company_id"]."'
                                        AND ts.date >= '$date_from'
                                        AND ts.date <= '$date_to'
                                        AND ts.projectType ='CP' $projectStatus ORDER BY e.lstname, frstname");

                $activityTypes = q("SELECT DISTINCT(at.id), at.type FROM (ActivityTypes AS at INNER JOIN Activities AS a
                                            ON at.id = a.parent_id INNER JOIN $table AS ts ON a.id = ts.activity_id
                                            INNER JOIN Project AS p ON p.id = ts.project_id)
                                            WHERE ".
                                            //at.company_id = '".$_SESSION["company_id"]."' AND
                                            "ts.date >= '$date_from'
                                            AND ts.date <= '$date_to'
                                            AND ts.company_id = '".$_SESSION["company_id"]."'
                                            AND ts.projectType ='CP' $projectStatus $where ORDER BY type");
            }
            else if ($project_Type == "invoicable") {
                $employees = q("SELECT DISTINCT(e.id), e.frstname, e.lstname, e.email FROM (Employee AS e
                                        INNER JOIN Company_Users AS cu ON e.id = cu.user_id
                                        INNER JOIN $table AS ts ON e.id = ts.user_id
                                        INNER JOIN companiesOnTeam AS cot ON ts.project_id = cot.project_id
										INNER JOIN Project AS p ON p.id = ts.project_id)
                                        WHERE cu.company_id = '".$_SESSION["company_id"]."'
                                        AND e.email != 'admin'
                                        AND ts.company_id = '".$_SESSION["company_id"]."'
                                        AND ts.date >= '$date_from'
                                        AND ts.date <= '$date_to'
                                        AND cot.total_budget > 0
                                        AND ts.projectType ='CP' $projectStatus ORDER BY e.lstname, e.frstname");

                $activityTypes = q("SELECT DISTINCT(at.id), at.type FROM (ActivityTypes AS at INNER JOIN Activities AS a
                                        ON at.id = a.parent_id INNER JOIN $table AS ts ON a.id = ts.activity_id
                                        INNER JOIN companiesOnTeam AS cot ON ts.project_id = cot.project_id
										INNER JOIN Project AS p ON p.id = ts.project_id)
                                        WHERE ".
                                        //at.company_id = '".$_SESSION["company_id"]."' AND
                                        "ts.date >= '$date_from'
                                        AND ts.date <= '$date_to'
                                        AND ts.company_id = '".$_SESSION["company_id"]."'
                                        AND ts.projectType ='CP' $projectStatus $where AND cot.total_budget > 0 ORDER BY type");
            }
            else {
                $employees = q("SELECT DISTINCT(e.id), e.frstname, e.lstname, e.email FROM (Employee AS e
                                        INNER JOIN Company_Users AS cu ON e.id = cu.user_id
                                        INNER JOIN $table AS ts ON e.id = ts.user_id
										INNER JOIN companiesOnTeam AS cot ON ts.project_id = cot.project_id
                                        INNER JOIN Project AS p ON p.id = ts.project_id)
                                        WHERE cu.company_id = '".$_SESSION["company_id"]."'
                                        AND e.email != 'admin'
                                        AND ts.company_id = '".$_SESSION["company_id"]."'
                                        AND ts.date >= '$date_from'
                                        AND ts.date <= '$date_to'
                                        AND cot.total_budget = ''
                                        AND ts.projectType ='CP' $projectStatus ORDER BY e.lstname, frstname");

                $activityTypes = q("SELECT DISTINCT(at.id), at.type FROM (ActivityTypes AS at
                                            INNER JOIN Activities AS a ON at.id = a.parent_id
                                            INNER JOIN $table AS ts ON a.id = ts.activity_id
											INNER JOIN companiesOnTeam AS cot ON ts.project_id = cot.project_id
                                            INNER JOIN Project AS p ON p.id = ts.project_id)
                                            WHERE ".
                                            //at.company_id = '".$_SESSION["company_id"]."' AND
                                            "ts.date >= '$date_from'
                                            AND ts.date <= '$date_to'
                                            AND ts.company_id = '".$_SESSION["company_id"]."' $where
                                            AND cot.total_budget = ''
                                            AND ts.projectType ='CP' $projectStatus ORDER BY type");
            }
        } // for specefic project:
        else {
            $employees = q("SELECT DISTINCT(e.id), e.frstname, e.lstname, e.email FROM (Employee AS e
                                    INNER JOIN Company_Users AS cu ON e.id = cu.user_id
                                    INNER JOIN $table AS ts ON e.id = ts.user_id
                                    INNER JOIN Project AS p ON p.id = ts.project_id)
                                    WHERE cu.company_id = '".$_SESSION["company_id"]."'
                                    AND e.email != 'admin'
                                    AND ts.project_id = '$project_id'
                                    AND ts.date >= '$date_from'
                                    AND ts.date <= '$date_to' $projectStatus ORDER BY e.lstname, frstname;");

            $activityTypes = q("SELECT DISTINCT(at.id), at.type FROM (ActivityTypes AS at
                                            INNER JOIN Activities AS a ON at.id = a.parent_id
                                            INNER JOIN $table AS ts ON a.id = ts.activity_id)
                                            WHERE ".
                                            //at.company_id = '".$_SESSION["company_id"]."' AND
                                            "ts.date >= '$date_from'
                                            AND ts.date <= '$date_to'
                                            AND ts.project_id = '$project_id' $where ORDER BY type");
        }

        ///////////////////////////
        //  Create Table Headers
        $headers = "<tr><th>Employee Name</th>";
        $columnCounter ++;

        if (is_array($activityTypes)) {
            if (count($activityTypes) > 1) {

                //$row++;         // ROW = 1

                if ($project_id == "all") {
                    $myData[$row][] = "Project: All Projects";
                    $row++;     // ROW = 2
                }else{
                    //$myData[$row][] = " ".q("SELECT name From Project WHERE id = '$project_id'");
                    $row++;     // ROW = 2
                }


                $row++;
                $myData[$row][] = $colorClass."Employee Name";      // Col 1

                foreach ($activityTypes as $activityType) {
                    $name = "";
                    $nameExcel = "";
                    $name_length = strlen($activityType[1]);

                    for ($i = 0; $i < $name_length; $i++){
                        $name .= "<br/>".substr($activityType[1], $i, 1)."";
                        $nameExcel .= substr($activityType[1], $i, 1);
                    }

                    $headers .= "<th class='on-table-light' style='vertical-align:text-top;'>".$name."</th>";

                    $myData[$row][] = $colorClass.$nameExcel;
                    $columnCounter ++;
                }
            }
            else{
                $headers .= "<td>".$activityTypes[0][1]."</td>";
                $myData[$row][] = $activityTypes[0][1];
            }
            $myData[$row][] = $colorClass."Total";      // Col Last
            $row++;
        }

        if ($report_type == "time"){
            $headers .= "<th>Total</th></tr>";
            $columnCounter ++;
        }else{
            $headers .= "<th>Total <i>(".$_SESSION["currency"].")</i></th></tr>";
            $columnCounter ++;
        }

        for($i=0; $i<$columnCounter ; $i++){          // Print Blank Headers
            $excelheadings[0][] = "";
        }

        if($project_id == "all"){
            $myData[$row][] = "All Projects";           // col 1
        }else{
            $myData[$row][] = "Project: ".$project_name;        // col 1
        }

        if (is_array($employees))
            foreach ($employees as $employee){
                $myData[$row][0] = $employee[1]." ".$employee[2];
                $displayString .= "<tr><td>".$employee[1]." ".$employee[2]. "</td>".
                                    createRow($employee[0], $project_id, $project_Type, $report_type, $date_from, $date_to, $projectStatus, $table, $where)."</tr>"; /////////////////////////  HIER  /////////////////////////
            $employeeCounter ++;
        }

        if ($project_id == "all") {
            if ($project_Type == "both") {
                if ($report_type == "time")
                    $grand_total = q("SELECT SUM(ts.time_spent) FROM ($table AS ts
                                                INNER JOIN Activities AS a ON ts.activity_id = a.id
                                                INNER JOIN ActivityTypes AS at ON at.id = a.parent_id
                                                INNER JOIN Project AS p ON ts.project_id = p.id)
                                                WHERE ".
                                                //at.company_id = '".$_SESSION["company_id"]."' AND
                                                "ts.date >= '".$date_from."'
                                                AND ts.date <= '".$date_to."'
                                                AND ts.company_id = '".$_SESSION["company_id"]."'
                                                AND ts.projectType ='CP' $projectStatus $where");
                else
                    $grand_total = q("SELECT SUM(ts.time_spent * ts.rate) FROM ($table AS ts
                                                INNER JOIN Activities AS a ON ts.activity_id = a.id
                                                INNER JOIN ActivityTypes AS at ON at.id = a.parent_id
                                                INNER JOIN Project AS p ON ts.project_id = p.id)
                                                WHERE ".
                                                //at.company_id = '".$_SESSION["company_id"]."' AND
                                                "ts.date >= '".$date_from."'
                                                AND ts.date <= '".$date_to."'
                                                AND ts.company_id = '".$_SESSION["company_id"]."'
                                                AND ts.projectType ='CP' $projectStatus $where");
            }
            else if ($project_Type == "invoicable") {
                if ($report_type == "time")
                    $grand_total = q("SELECT SUM(ts.time_spent) FROM ($table AS ts INNER JOIN Activities AS a ".
                                        "ON ts.activity_id = a.id INNER JOIN ActivityTypes AS at ON at.id = a.parent_id ".
										"INNER JOIN companiesOnTeam AS cot ON ts.project_id = cot.project_id ".
                                        "INNER JOIN Project AS p ON ts.project_id = p.id) ".
                                        "WHERE ".
                                        //at.company_id = '".$_SESSION["company_id"]."' AND
                                        "ts.date >= '".$date_from."' ".
                                        "AND ts.date <= '".$date_to."' AND ts.company_id = '".$_SESSION["company_id"]."' ".
                                        "AND cot.total_budget > 0
                                        AND ts.projectType ='CP' $projectStatus $where");
                else
                    $grand_total = q("SELECT SUM(ts.time_spent * ts.rate) FROM ($table AS ts INNER JOIN Activities AS a ".
                                        "ON ts.activity_id = a.id INNER JOIN ActivityTypes AS at ON at.id = a.parent_id ".
                                        "INNER JOIN companiesOnTeam AS cot ON ts.project_id = cot.project_id INNER JOIN Project AS p ON ts.project_id = p.id) ".
                                        "WHERE ".
                                        //at.company_id = '".$_SESSION["company_id"]."' AND
                                        "ts.date >= '".$date_from."' ".
                                        "AND ts.date <= '".$date_to."' AND ts.company_id = '".$_SESSION["company_id"]."' ".
                                        "AND cot.total_budget > 0
                                        AND ts.projectType ='CP' $projectStatus $where");
            }
            else {
                if ($report_type == "time")
                    $grand_total = q("SELECT SUM(ts.time_spent) FROM ($table AS ts INNER JOIN Activities AS a ".
                                        "ON ts.activity_id = a.id INNER JOIN ActivityTypes AS at ON at.id = a.parent_id ".
                                        "INNER JOIN companiesOnTeam AS cot ON ts.project_id = cot.project_id INNER JOIN Project AS p ON ts.project_id = p.id) ".
                                        "WHERE ".
                                        //at.company_id = '".$_SESSION["company_id"]."' AND
                                        "ts.date >= '".$date_from."' ".
                                        "AND ts.date <= '".$date_to."' AND ts.company_id = '".$_SESSION["company_id"]."' ".
                                        "AND cot.total_budget = '' AND ts.projectType ='CP' $projectStatus $where");
                else
                    $grand_total = q("SELECT SUM(ts.time_spent * ts.rate) FROM ($table AS ts INNER JOIN Activities AS a ".
                                        "ON ts.activity_id = a.id INNER JOIN ActivityTypes AS at ON at.id = a.parent_id ".
                                        "INNER JOIN companiesOnTeam AS cot ON ts.project_id = cot.project_id INNER JOIN Project AS p ON ts.project_id = p.id) ".
                                        "WHERE ".
                                        //at.company_id = '".$_SESSION["company_id"]."' AND
                                        "ts.date >= '".$date_from."' ".
                                        "AND ts.date <= '".$date_to."' AND ts.company_id = '".$_SESSION["company_id"]."' ".
                                        "AND cot.total_budget = '' AND ts.projectType ='CP' $projectStatus $where");
            }
        }
        else {
            if ($report_type == "time")
                $grand_total = q("SELECT SUM(ts.time_spent) FROM ($table AS ts INNER JOIN Activities AS a ".
                                    "ON ts.activity_id = a.id INNER JOIN ActivityTypes AS at ON at.id = a.parent_id) ".
                                    "WHERE ".
                                    //at.company_id = '".$_SESSION["company_id"]."' AND
                                    "ts.date >= '".$date_from."' ".
                                    "AND ts.date <= '".$date_to."' AND ts.project_id = '$project_id' AND ts.company_id = '".$_SESSION["company_id"]."' $where");
            else
                $grand_total = q("SELECT SUM(ts.time_spent * rate) FROM ($table AS ts INNER JOIN Activities AS a ".
                                    "ON ts.activity_id = a.id INNER JOIN ActivityTypes AS at ON at.id = a.parent_id) ".
                                    "WHERE ".
                                    //at.company_id = '".$_SESSION["company_id"]."' AND
                                    "ts.date >= '".$date_from."' ".
                                    "AND ts.date <= '".$date_to."' AND ts.project_id = '$project_id' AND ts.company_id = '".$_SESSION["company_id"]."' $where");
        }

        $displayString                                                  = "<tr>
                                                                                        <td class='on-table-clear' colspan='100%'><a>Summary Report (Consulting): ".$date_from." - ".$date_to."</a></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class='on-table-clear' colspan='100%'>Project: ".$project_name."</td>
                                                                                    </tr>".
                                                                                    $headers.$displayString;

        $myData[0][] = "Summary Report (Consulting): ".$date_from."  -  ".$date_to;
        for($i=0; $i<$columnCounter-1 ; $i++){          // Print Summary Report (Consulting) Header
            $myData[0][] = "";
        }
        //$row++;

        if ($report_type == "time"){
            $displayString .= "<tr><td class='on-table-total'>Grand Total:</td>";
            $myData[$row][0] = "Grand Total";                                                   // 0
        }else{
            $displayString .= "<tr><td class='on-table-total'>Grand Total <i>(".$_SESSION["currency"].")</i>:</td>";
            $myData[$row][0] = "Grand Total (".$_SESSION["currency"].")";        // 0
        }

        if (is_array($activityTypes)) {
            foreach ($activityTypes as $activityType) {
                if ($project_id == "all") {
                    if ($project_Type == "both") {
                        if ($report_type == "time")
                            $spent                                      = q("SELECT SUM(ts.time_spent) FROM ($table AS ts
                                                                            INNER JOIN Activities AS a ON ts.activity_id = a.id
                                                                            INNER JOIN Project AS p ON ts.project_id = p.id)
                                                                            WHERE a.parent_id = '".$activityType[0]."'
                                                                            AND ts.date >= '$date_from'
                                                                            AND ts.date <= '$date_to'
                                                                            AND ts.company_id = '".$_SESSION["company_id"]."' $projectStatus $where");
                        else
                            $spent                                      = q("SELECT SUM(ts.time_spent * ts.rate) FROM ($table AS ts
                                                                            INNER JOIN Activities AS a ON ts.activity_id = a.id
                                                                            INNER JOIN Project AS p ON ts.project_id = p.id)
                                                                            WHERE a.parent_id = '".$activityType[0]."'
                                                                            AND ts.date >= '$date_from'
                                                                            AND ts.date <= '$date_to'
                                                                            AND ts.company_id = '".$_SESSION["company_id"]."' $projectStatus $where");
                    }
                    else if ($project_Type == "invoicable") {
                        if ($report_type == "time")
                            $spent                                      = q("SELECT SUM(ts.time_spent) FROM ($table AS ts INNER JOIN Activities AS a ".
                                                                            "ON ts.activity_id = a.id INNER JOIN companiesOnTeam AS cot ON ts.project_id = cot.project_id ".
																			"INNER JOIN Project AS p ON ts.project_id = p.id) ".
                                                                            "WHERE a.parent_id = '".$activityType[0]."' AND ts.date >= '$date_from' ".
                                                                            "AND ts.date <= '$date_to' AND cot.total_budget > 0 ".
                                                                            "AND ts.company_id = '".$_SESSION["company_id"]."' $projectStatus $where");
                        else
                            $spent                                      = q("SELECT SUM(ts.time_spent * ts.rate) FROM ($table AS ts INNER JOIN Activities AS a ".
                                                                            "ON ts.activity_id = a.id INNER JOIN companiesOnTeam AS cot ON ts.project_id = cot.project_id ".
																			"INNER JOIN Project AS p ON ts.project_id = p.id) ".
                                                                            "WHERE a.parent_id = '".$activityType[0]."' AND ts.date >= '$date_from' ".
                                                                            "AND ts.date <= '$date_to' AND cot.total_budget > 0 ".
                                                                            "AND ts.company_id = '".$_SESSION["company_id"]."' $projectStatus $where");
                    }
                    else {
                        if ($report_type == "time")
                            $spent                                      = q("SELECT SUM(ts.time_spent) FROM ($table AS ts INNER JOIN Activities AS a ".
                                                                            "ON ts.activity_id = a.id INNER JOIN companiesOnTeam AS cot ON ts.project_id = cot.project_id INNER JOIN Project AS p ON ts.project_id = p.id) ".
                                                                            "WHERE a.parent_id = '".$activityType[0]."' AND ts.date >= '$date_from' ".
                                                                            "AND ts.date <= '$date_to' AND cot.total_budget = '' ".
                                                                            "AND ts.company_id = '".$_SESSION["company_id"]."' $projectStatus $where");
                        else
                            $spent                                      = q("SELECT SUM(ts.time_spent * ts.rate) FROM ($table AS ts INNER JOIN Activities AS a ".
                                                                            "ON ts.activity_id = a.id INNER JOIN companiesOnTeam AS cot ON ts.project_id = cot.project_id INNER JOIN Project AS p ON ts.project_id = p.id) ".
                                                                            "WHERE a.parent_id = '".$activityType[0]."' AND ts.date >= '$date_from' ".
                                                                            "AND ts.date <= '$date_to' AND cot.total_budget = '' ".
                                                                            "AND ts.company_id = '".$_SESSION["company_id"]."' $projectStatus $where");
                    }
                }
                else {
                    if ($report_type == "time"){
                        $spent                                          = q("SELECT SUM(ts.time_spent) FROM ($table AS ts INNER JOIN Activities AS a ".
                                                                            "ON ts.activity_id = a.id) WHERE a.parent_id = '".$activityType[0]."' ".
                                                                            "AND ts.date >= '".$date_from."' AND ts.date <= '".$date_to."' ".
                                                                            "AND ts.project_id = '$project_id' AND ts.company_id = '".$_SESSION["company_id"]."' $where");
                    }else{
                        $spent                                          = q("SELECT SUM(ts.time_spent * ts.rate) FROM ($table AS ts INNER JOIN Activities AS a ".
                                                                            "ON ts.activity_id = a.id) WHERE a.parent_id = '".$activityType[0]."' ".
                                                                            "AND ts.date >= '".$date_from."' AND ts.date <= '".$date_to."' ".
                                                                            "AND ts.project_id = '$project_id' AND ts.company_id = '".$_SESSION["company_id"]."' $where");
                    }
                }

                $chartTotals[$activityType[0]] = $spent;
                $displayString                                          .= "<td class='on-table-total'>".number_format($spent, 2, ".", "")."</td>";
                $myData[$row][] = $spent;                // 1 - 5   TOTALS
            }
        }

        $displayString                                                  .= "<td class='on-table-total'>".number_format($grand_total, 2, ".", "")."</td>
                                                                                    </tr>";

        $myData[$row][] = $grand_total;               // 6  GRAND TOTAL

        $graph = createPieChartXML($activityTypes,$chartTotals);

        if ($graph != "")   $displayString = renderChart("fusion/Pie3D.swf", "", $graph, "multigraph2", 800, 400, false, false).$displayString;

        if ($displayString != "")
            $generated                                                  = "1";
    }

    for($i=0; $i<$employeeCounter+4; $i++){                                         // +4 for Headings and total.
        for($cols=0; $cols < $columnCounter; $cols++){                             // & moet variable wees wat cols count
            $exceldata[$i][$cols] = $myData[$i][$cols];
        }
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("3", "reports");
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    jQuery(function(){
	jQuery('#date_from').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_from').val(),
		current: jQuery('#date_from').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_from').val()))
			_date = new Date();
			else _date = jQuery('#date_from').val();
			jQuery('#date_from').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_from').val(formated);
			jQuery('#date_from').DatePickerHide();
		}
	});
    })

    jQuery(function(){
	jQuery('#date_to').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_to').val(),
		current: jQuery('#date_to').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_to').val()))
			_date = new Date();
			else _date = jQuery('#date_to').val();
			jQuery('#date_to').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_to').val(formated);
			jQuery('#date_to').DatePickerHide();
		}
	});
    })

    function check()
    {
        var valid                                                       = 1;

        //  Check That Employee Is Selected
        if (document.forms["report_summary"].project.value == "null")
        {
            ShowLayer("projectDiv", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("projectDiv", "none");

        //  Check That Date From Is Entered
        if (document.forms["report_summary"].date_from.value == "")
        {
            ShowLayer("dateFrom", "block");
            valid                                                       = 0;
        }
        //  Check That Entered Date From Is Valid
        else
        {
            ShowLayer("dateFrom", "none");

            if (!validation("date", document.forms["report_summary"].date_from.value))
            {
                ShowLayer("dateFromDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("dateFromDiv", "none");
        }

        //  Check That Date To Is Entered
        if (document.forms["report_summary"].date_to.value == "")
        {
            ShowLayer("dateTo", "block");
            valid                                                       = 0;
        }
        //  Check That Entered Date To Is Valid
        else
        {
            ShowLayer("dateTo", "none");

            if (!validation("date", document.forms["report_summary"].date_to.value))
            {
                ShowLayer("dateToDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("dateToDiv", "none");
        }

        if (valid == 1)
        {
            document.forms["report_summary"].save.value                 = 1;
            document.forms["report_summary"].submit();
        }
    }

    function displayCheck(select)
    {
        if (select.value == "all")
            ShowLayer("projectTypeDiv", "block");
        else
            ShowLayer("projectTypeDiv", "none");
    }

    ///////////////////////////
    //  Sajax
    <?php sajax_show_javascript(); ?>

    function ClearOptions(OptionList) {
        for (x = OptionList.length; x >= 0; x--)
        {
            OptionList[x]                                               = null;
        }
    }

    function setProjects(data) {
        var c                                                           = 0;

        document.forms["report_summary"].project.options[(c++)] = new Option("--  Select A Project  --", "null");
        document.forms["report_summary"].project.options[(c++)] = new Option("All Projects", "all");

        for (var i in data)
            eval("document.forms['report_summary'].project.options["+(c++)+"] = new Option('" + data[i][1] + "', '" + data[i][0] + " ');");
    }

    function getProjects(select1,select2) {
        var status = eval("document.report_summary."+select1+".value;");
        var type = eval("document.report_summary."+select2+".value;");;

        ClearOptions(document.report_summary.project);
        x_getProjects(status, type, setProjects);
    }
    ///////////////////////////
</script>
<?php

    $n[0][0] = "0"; $n[0][1] = "Shared Projects";
    $projectTypes = q("SELECT id, type FROM ProjectTypes WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY type");
    if (!is_array($projectTypes))   $projectTypes = $n;
    else                            $projectTypes = array_merge($n,$projectTypes);
    $projectTypes = sortArrayByColumn($projectTypes, 1);

    $projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id)
                            WHERE cot.company_id = '".$_SESSION["company_id"]."' AND p.completed = '0' AND p.deleted = '0' ORDER BY UPPER(p.name)");
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="report_summary">
                    <div id="" style="<?php if ($generated == "1") echo "display: none;"; else echo "display: block"; ?>">
                        <table width="100%">
                            <tr>
                                <td class="centerdata">
                                    <h6>Summary Report (Time Based)</h6>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Report On:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="reportOn">
                                        <option value="normal">Actual Time</option>
                                        <option value="approved">Approved Time</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Project Status:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="projectStatus" onChange="getProjects('projectStatus','projectType');">
                                        <option value="all">Completed & Uncompleted</option>
                                        <option value="0" selected>Uncompleted</option>
                                        <option value="1">Completed</option>
                                    </select>
                                </td>
                            </tr>
                            <tr><td colspan="100%"><br/></td></tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Project Type:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="projectType" onChange="getProjects('projectStatus','projectType');">
                                        <option value="null">All Project Types</option>
                                        <?php
                                            if (is_array($projectTypes))
                                                foreach ($projectTypes as $projectType)
                                                    if ($_POST["projectType"] == $projectType[0])
                                                        echo "<option value='".$projectType[0]."' selected>".$projectType[1]."</option>";
                                                    else
                                                        echo "<option value='".$projectType[0]."'>".$projectType[1]."</option>";
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Project:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" id="project" name="project" onChange="displayCheck(project);">
                                        <option value="null">--  Select A Project  --</option>
                                        <option value="all">All Projects</option>
                                        <?php
                                            if (is_array($projects))
                                                foreach ($projects as $project)
                                                    if (isset($_POST["project"]) && $_POST["project"] == $project[0])
                                                        echo "<option value='".$project[0]."' selected>".$project[1]."</option>";
                                                    else
                                                        echo "<option value='".$project[0]."'>".$project[1]."</option>";
                                        ?>
                                    </select>
                                    <div id="projectDiv" style="display: none;"><font class="on-validate-error">* Project must be selected</font></div>
                                </td>
                            </tr>
                        </table>
                        <div id="projectTypeDiv" style="display: none;">
                            <table cellpadding="0" cellspacing="2" width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Project Type:
                                    </td>
                                    <td class="on-description-left" width="50%">
                                        <input method="post" name="projectType2" type="radio" value="both" checked><a>Invoicable & Non-Invoicable</a><br/>
                                        <input method="post" name="projectType2" type="radio" value="invoicable"><a>Invoicable</a><br/>
                                        <input method="post" name="projectType2" type="radio" value="non_invoicable"><a>Non-Invoicable</a><br/>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Report Type:
                                </td>
                                <td class="on-description-left" width="50%">
                                    <input method="post" name="reportType" type="radio" value="time" checked><a>Time Based</a><br/>
                                    <input method="post" name="reportType" type="radio" value="cost"><a>Cost Based</a><br/>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Date From:
                                </td>
                                <td width="50%">
                                    <input class="on-field-date" id="date_from" name="date_from" type="text" style="text-align:right;" value="<?php
                                        if ($_POST["date_from"] != "") echo "".$_POST["date_from"]; else echo "".getMonthStart($today); ?>">
                                    <div id="dateFrom" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                    <div id="dateFromDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Date To:
                                </td>
                                <td width="50%">
                                    <input class="on-field-date" id="date_to" name="date_to" type="text" style="text-align:right;" value="<?php
                                        if ($_POST["date_to"] != "") echo "".$_POST["date_to"]; else echo "".getMonthEnd($today); ?>">
                                    <div id="dateTo" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                    <div id="dateToDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <input name="btnGenerate" onClick="check();" type="button" value="Generate Report">
                        <input method="post" name="save" type="hidden" value="0" />
                    </div>
                </form>
                <div id="report" style="<?php if ($generated == "1") echo "display: block;"; else echo "display: none"; ?>">
                    <?php
                        echo "<div class='on-20px'><table class='on-table-center on-table'>";
                            echo "".$displayString;
                             echo  "<tfoot>
                                            <tr>
                                                <td colspan='100%'></td>
                                            </tr>
                                        </tfoot>";
                        echo "</table></div>";
                        echo "<br/>";

                        ///////////////////////////
                        //  Set Export Information
                        $_SESSION["fileName"] = "Report Summary (Time)";
                        $_SESSION["fileData"] = array_merge($excelheadings, $exceldata);
                        ///////////////////////////

                        echo "<input name='btnExport' onClick=\"location.href='_export.php'\" type='button' value='Export to Excel' />";
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
