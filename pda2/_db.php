<?php
    //  Local & Live Site Connection Settings
    //include("../../conn_settings/_dbconn.php");
    //  Internet Connection Settings - Test Site
    include("../../conn_test/_dbconn.php");

    //  Set Table Border & Background Details
    $top                                                                = "border-top: 1px solid #336699;";
    $left                                                               = "border-left: 1px solid #336699;";
    $right                                                              = "border-right: 1px solid #336699;";
    $bottom                                                             = "border-bottom: 1px solid #336699;";
    $background                                                         = "background-color:#6699CC;";

    //  Set Time Zone
    date_default_timezone_set("Africa/Johannesburg");

    //  Set Link Details
    $link                                                               = "color:#336699;font-weight:normal;";

    //  Set Current Days Date
    $today                                                              = date("Y-m-d");

    //  Password Hash Generation
    define("SALT_LENGTH", 9);
    $origsalt                                                           = "";

    function generateHash($plainText, $salt = null)
    {
	if ($salt === null)
	{
            $salt                                                       = substr(md5(uniqid(rand(), true)), 0, SALT_LENGTH);
	}
	else
	{
            $salt                                                       = substr($salt, 0, SALT_LENGTH);
	}

	global $origsalt;

	$origsalt                                                       = $salt;

	return $salt.hash("sha512", ($salt.$plainText));
    }

    //  SQL Query Run Procedure
    function q($query, $assoc = 0)
    {
        global $dblink;
        $r                                                              = @mysqli_query($dblink, $query);

        if (mysqli_errno($dblink))
        {
            $error                                                      = "MYSQL ERROR #".mysqli_errno($dblink)." : <small>".mysqli_error($dblink)."</small><br><VAR>$query</VAR>";

            echo($error);

            return FALSE;
        }

        if (strtolower(substr($query, 0, 6)) != "select")
            return array(mysqli_affected_rows(), mysqli_insert_id());

        $count                                                          = @mysqli_num_rows($r);

        if (!$count)
            return 0;

        if ($count == 1)
        {
            if ($assoc)
                $f                                                      = mysqli_fetch_assoc($r);
            else
                $f                                                      = mysqli_fetch_row($r);

            mysqli_free_result($r);

            if (count($f) == 1)
            {
                list($key)                                              = array_keys($f);

                return $f[$key];
            }
            else
            {
                $all                                                    = array();
                $all[]                                                  = $f;

                return $all;
            }
        }
        else
        {
            $all                                                        = array();

            for ($i = 0; $i < $count; $i++)
            {
                if ($assoc)
                    $f                                                  = mysqli_fetch_assoc($r);
                else
                    $f                                                  = mysqli_fetch_row($r);

                $all[]                                                  = $f;
            }

            mysqli_free_result($r);

            return $all;
        }
    }
?>