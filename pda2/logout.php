<?php
    session_start();
    global $dblink;

    session_destroy();

    mysqli_close($dblink);

    header("Location: login.php");
?>