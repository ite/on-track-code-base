<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("include/sajax.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!$_SESSION["eula_agree"] === true)
        header("Location: logout.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

	if (!hasAccess("TIME_MANAGE") || !hasAccess("EXPENSE_MANAGE_OWN"))
		header("Location: noaccess.php");

    ///////////////////////////
    //  Sajax
    function getPrinting($id) {
        return q("SELECT id, name FROM Disbursements WHERE parent_id = '$id' ORDER BY name");
    }

    $sajax_request_type = "GET";
    sajax_init();
    sajax_export("getPrinting");
    sajax_handle_client_request();

    if (isset($_POST["btnUpdate"])) {
        unset($_POST["btnUpdate"]);

        $array = split("_", addslashes(strip_tags($_POST["idBox"])));

        if (is_array($array))   $array = array_slice($array, 0, count($array) - 1);

        if (isset($timeArr))    unset($timeArr);

        $timeArr[] = number_format("0.25", 2, ".", "");
        $timeArr[] = number_format("0.50", 2, ".", "");
        $timeArr[] = number_format("0.75", 2, ".", "");

        for ($i = 1; $i <= 12; $i += 0.5)
            $timeArr[] = number_format($i, 2, ".", "");

        foreach ($array as $a)  {
            $id = substr($a, 0, strpos($a, ":"));
            $locked = substr($a, (strpos($a, ":") + 1), strlen($a));

            if (is_numeric($id))    {
                $date = addslashes(strip_tags($_POST["A".$id]));
                $descr = addslashes(strip_tags($_POST["B".$id]));
                $time = number_format(addslashes(strip_tags($_POST["C".$id])), 2, ".", "");

                if (submitDateCheck($date)) {
                    if (in_array($time, $timeArr, true) && $locked == "0")  {
                        $update = q("UPDATE TimeSheet SET date = '$date', descr = '$descr', time_spent = '$time' WHERE id = '$id'");
                        $update = q("UPDATE ApprovedTime SET date = '$date', descr = '$descr', time_spent = '$time' WHERE timesheet_id = '$id'");
                    }
                }
            }
        }

        if ($errorMessage == "")    header("Location: bookings.php");
        
    }
    
    if (isset($_POST["save"]) && $_POST["save"] === "1")    {
        $errorMessage = "";

        ///////////////////////////
        //  Get Information
        $project = $_POST["project"];
        $projectType = (substr($project,0,3) == "CP_") ? "CP" : "SP";
        $projectID = substr($project,3);
        ///////////////////////////
        //  Time Fields
        $activityType = $_POST["activityType"];
        $activity = $_POST["activity"];
        $timeDescr = addslashes(strip_tags($_POST["timeDescr"]));
        $timeDate = addslashes(strip_tags($_POST["timeDate"]));
        $timeSpent = $_POST["timeSpent"];

        if ($projectType == "CP")
            $tariff = "tariff".q("SELECT user_tariff FROM Project_User WHERE user_id = '".$_SESSION["user_id"]."' AND project_id = '".$projectID."'");
        else
            $tariff = "tariff".q("SELECT rateID FROM sp_usersOnTeam WHERE userID = '".$_SESSION["user_id"]."' AND spID = '".$projectID."'");

        $timeRate = q("SELECT $tariff FROM Employee WHERE id = '".$_SESSION["user_id"]."'");
        ///////////////////////////
        
        $timeTime = date("H:i:s");
        ///////////////////////////
        
        $area = ($_POST["area"] != "") && is_numeric($_POST["area"]) ? addslashes(strip_tags($_POST["area"])) : 0;      // Check if any AREAs were posted$hasAreas  = false;

        if (submitDateCheck($timeDate))   {
            if ($timeDate <= $today)    {
                if ($projectID != "0" && $projectID != "null" && $_SESSION["user_id"] != "0" && $_SESSION["user_id"] != "" && $activity != "0" && $activity != "null"
                    && $timeDate != "" && $timeSpent != "0" && $timeSpent != "null")   {
                    $insert = q("INSERT INTO TimeSheet (projectType, date, user_id, project_id, area_id, activity_id, descr, rate, time_spent, on_date, on_time) VALUES ".
                                "('".$projectType."', '$timeDate', '".$_SESSION["user_id"]."', '$projectID', '$area', '$activity', '$timeDescr', '$timeRate', '$timeSpent', '$today', '$timeTime')");

                    if ($insert)    {
                        //  Check If Company Has Access To Current Module
                        $access = q("SELECT mu.id FROM (Module_Users AS mu INNER JOIN Modules AS m ON mu.module_id = m.id) ".
                                    "WHERE mu.company_id = '".$_SESSION["company_id"]."' AND m.code = 'Approval'");

                        if ($access)    {
                            $timeSheetID = q("SELECT id FROM TimeSheet WHERE user_id = '".$_SESSION["user_id"]."' AND on_date = '$today' AND on_time = '$timeTime'");
                            $insert = q("INSERT INTO ApprovedTime (projectType, date, user_id, project_id, area_id, activity_id, descr, rate, time_spent, on_date, on_time, timesheet_id, ".
                                        "status) VALUES ('".$projectType."', '$timeDate', '".$_SESSION["user_id"]."', '$projectID','$area', '$activity', '$timeDescr', '$timeRate', '$timeSpent', ".
                                        "'$today', '$timeTime',  '$timeSheetID', '0')");
                        }

                        $projectIDName = q("SELECT name FROM Project WHERE id = '$projectID'");

                        $logs = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) VALUES ".
                                    "('".$_SESSION["email"]." booked time on ".$projectIDName."', 'Insert', 'TimeSheet', '".$_SESSION["email"]."', '$today', '$timeTime', ".
                                    "'".$_SESSION["company_id"]."')");
                    }
                }

                unset($insert);
                
                header("Location: bookings.php");
            }
            else
                $errorMessage = "Time cannot be booked in advance";
        }
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0", "bookings", "bookings");

    if ($errorMessage != "")    {
        if ($errorMessage == "Please note that we are currently busy with data management, no entries are allowed for 2010-08-26. Please check back in 24 Hours.")
            echo "<p align='center' style='padding:0px;'><strong style='font-size:12px;'><font color='#FF5900'>$errorMessage</font></strong></p>";
        else
            echo "<p align='center' style='padding:0px;'><strong><font color='#999999'>$errorMessage</font></strong></p>";
        echo "<br/>";
    }

    $index = 1;
    $currency = "<i>(".$_SESSION["currency"].")</i>";
    $projects = q("SELECT p.id, p.name, 'CP' FROM (Project AS p INNER JOIN Project_User AS pu ON p.id = pu.project_id) WHERE pu.user_id = '".$_SESSION["user_id"]."' ".
                    "AND company_id = '".$_SESSION["company_id"]."' AND p.completed = '0' ORDER BY p.name");
	$sp_projects = q("SELECT sp.id, sp.name, 'SP' FROM ((sp_projects AS sp INNER JOIN sp_usersOnTeam AS spu ON sp.id = spu.spID) INNER JOIN sp_companiesOnTeam AS cot ".
					"ON cot.spid = sp.id) ".
					"WHERE spu.userID = '".$_SESSION["user_id"]."' AND spu.companyID = '".$_SESSION["company_id"]."' AND cot.companyID = '".$_SESSION["company_id"]."' ".
					"AND spu.deleted = '0' AND sp.completed = '0' AND cot.status = '1' ORDER BY sp.name");

    if (is_array($projects) && is_array($sp_projects))
        $projects = array_merge($projects, $sp_projects);
    //	$projects = sortArrayByColumn($projects,1);

?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    var loadActivityTypes;
    var loadActivities;
    
    function is_array(input){
        return typeof(input)=='object'&&(input instanceof Array);
    }

    jQuery(function()    {
        //////////////////////////////
        //  Load Activity Types
        loadActivityTypes = function()  {
            var projectType = jQuery("#project").val().substring(0, 2);
            var projectID = jQuery("#project").val().substring(3);

            jQuery.post("_ajax.php", {func: "get_activity_types", projectType: projectType, projectID: projectID}, function(data)	{
                data = json_decode(data);

                jQuery("#activityType option").remove();
                jQuery("#activity option").remove();

                jQuery("#activityType").append(jQuery("<option></option>").attr("value","null").text("--  Select Activity Type  --"));

                jQuery.each(data,function(i, v)	{
                    jQuery("#activityType").append(jQuery("<option></option>").attr("value",v[0]).text(v[1]));
                });
            });
        };
        
        //  Load Areas
        loadAreas = function()  {
            var projectType = jQuery("#project").val().substring(0, 2);
            var projectID = jQuery("#project").val().substring(3);
            
            jQuery.post("_ajax.php", {func: "get_area_info", projectType: projectType, projectID: projectID}, function(data)	{
                data = json_decode(data);
        
                jQuery("#area option").remove();
                
                jQuery("#area").append(jQuery("<option></option>").attr("value","null").text("--  Select Area --"));
                
                if(is_array(data)){     // Do not display area options
                  jQuery("#areaInfo1").show();     
                  jQuery("#areaInfo2").show();                    
                }else{
                    jQuery("#areaInfo1").hide();   
                    jQuery("#areaInfo2").hide(); 
                }
                jQuery.each(data,function(i, v)	{
                    jQuery("#area").append(jQuery("<option></option>").attr("value",v[0]).text(v[1]));
                });
                
                
            });
        };

        //  Load Activities
        loadActivities = function()  {
            var projectType = jQuery("#project").val().substring(0, 2);
            var activityType = jQuery("#activityType").val();

            jQuery.post("_ajax.php", {func: "get_activities", projectType: projectType, activityType: activityType}, function(data)	{
                data = json_decode(data);

                jQuery("#activity option").remove();

                jQuery.each(data,function(i, v)	{
                    jQuery("#activity").append(jQuery("<option></option>").attr("value",v[0]).text(v[1]));
                });
            });
        };
        //////////////////////////////

        //////////////////////////////
        //  Date Picker(s)
        jQuery("#timeDate").DatePicker( {
            format:"Y-m-d",
            date: jQuery("#timeDate").val(),
            current: jQuery("#timeDate").val(),
            starts: 1,
            position: "right",
            onBeforeShow: function()    {
                var _date = (!validation("date", jQuery("#timeDate").val())) ? new Date(): jQuery("#timeDate").val();
                jQuery("#timeDate").DatePickerSetDate(_date, true);
            },
            onChange: function(formated, dates) {
                jQuery("#timeDate").val(formated);
                jQuery("#timeDate").DatePickerHide();
            }
        });
    
        //  Project Change - Show/Hide
        jQuery("#project").change(function()    {
            if (jQuery(this).val() != "null")    {
                jQuery("#info").show();
                jQuery("#project").focus();
            }
            else    {
                jQuery("#info").hide();
                jQuery("#project").focus();
            }
            loadAreas();
            loadActivityTypes();
        });
        //////////////////////////////

        //////////////////////////////
        //  Activity Type Change
        jQuery("#activityType").change(function()    {
            loadActivities();
        });
        //////////////////////////////

        //////////////////////////////
        //  Book Button Pressed
        jQuery("#btnAdd").click(function()    {
            var i = 0;
            var j;
            var valid = true;
            var test;

            var fields = [];

            fields[i++] = "project";

            //Time Fields
            var timeFields = new Array("activityType","activity","timeDescr","timeDate","timeSpent");
            
            if (jQuery("#activityType").val() != "null")
                for (j = 0; j < timeFields.length; j++)  {fields[i++] = timeFields[j];  }

            for (i = 0; i < fields.length; i++)  {
                var field = fields[i];
                var cls = jQuery("#"+field).attr("class");

                cls = cls.split(", ");

                for (j = 0; j < cls.length; j++)  {
                    var divName = (cls[j] == "entered") ? "#"+field+"Empty" : "#"+field+"Div";
                    test = validation(cls[j], jQuery("#"+field).val());

                    if (!test)  jQuery(divName).show();
                    else        jQuery(divName).hide();

                    valid &= test;
                }
            }

            if (valid) {
                jQuery("#save").val("1");
                jQuery("bookings").submit();
            }
        });
        //////////////////////////////
    });

    //  Sajax
    <?php sajax_show_javascript(); ?>

    ///////////////////////////
    //  Sajax
    function ClearOptions(OptionList) {
        for (x = OptionList.length; x >= 0; x--) {
            OptionList[x] = null;
        }
    }

    function setPrinting(data) {
        var c = 0;

        for (var i in data) {
            var display = data[i][1];
            display = display.replace(new RegExp(/\'/g), "");
            eval("document.bookings.printing.options[" + (c++) + "] = new Option('" + display + "', '" + data[i][0] + "');");
        }
    }

    function getPrinting(select) {
        ClearOptions(document.bookings.printing);

        if (select.value != "null")
            x_getPrinting(select.value, setPrinting);
    }
    ///////////////////////////

    function setInfo(select)    {
    }
</script>
<table cellpadding="0" cellspacing="0" width="100%">
    <tr height="380px">
        <td align="center" valign="top">
            <form action="" id="bookings" name="bookings" method="post">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="center">
                            <a style="font-size:12px;font-weight:bold;">
                                Time Bookings
                            </a>
                        </td>
                    </tr>
                </table>
                <br/>
                <div id="budgetExceeded" name="budgetExceeded" style="display: none;">
                    <p align="center" style="padding:0px;">
                        <strong>
                            <font color="#FF5900">
                                Budget For Project Has Been Reached/Exceeded, Please Notify Your Project Manager
                            </font>
                        </strong>
                    </p>
                    <br/>
                </div>
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="right" valign="top" style="padding-top:4px;" width="50%">
                            <strong>
                                Project Name:&nbsp;
                            </strong>
                        </td>
                        <td align="left" style="padding-top:2px;" width="50%">
                            &nbsp;<select id="project" name="project" method="post" class="required" tabindex="<?php echo $index++; ?>">
                                <option value="null">--  Select A Project  --&nbsp;&nbsp;</option>
                                <?php
                                    if (is_array($projects))    {
                                        foreach ($projects as $project) {
                                            $value = ($project[2] == "CP") ? "CP_".$project[0]: "SP_".$project[0];

                                            if ($_POST["project"] == $value)
                                                echo "<option value='".$value."' selected>".$project[1]."&nbsp;&nbsp;</option>";
                                            else
                                                echo "<option value='".$value."'>".$project[1]."&nbsp;&nbsp;</option>";
                                        }
                                    }
                                ?>
                            </select>
                            <div id="projectDiv" name="projectDiv" class="error">* Project must be selected</div>
                        </td>
                    </tr>
                </table>
                <br/>
                <div id="info" name="info" style="display: none;">
                    <table cellpadding="0" cellspacing="0" width="90%">
                        <tr>
                            <td align="center" valign="top" style="padding-top:4px;" width="45%">
                                <a style="font-weight:bold;text-decoration:underline;">
                                    Time Information
                                </a>
                                <table cellpadding="0" cellspacing="0" width="100%">
                                
                                <tr>
                                    <td colspan="1" align="right" valign="top" style="padding-top:4px;" width="50%">
                                        <div id="areaInfo1" name="areaInfo1" style="display:none;">
                                            <strong>
                                                Area:&nbsp;
                                            <strong>
                                        </div>
                                    </td>
                                    <td>
                                        <div id="areaInfo2" name="areaInfo2" style="display:none;">
                                            &nbsp;<select id="area" name="area" method="post" class="required" tabindex="<?php echo $index++; ?>">
                                                <option value="null">--  Select A Area  --&nbsp;&nbsp;</option>
                                                    <?php
                                                        if (is_array($areas))
                                                            foreach ($areas as $area)
                                                                if ($_POST["area"] == $area[0])
                                                                    echo "<option value='".$area[0]."' selected>".$area[1]."&nbsp;&nbsp;</option>";
                                                                else
                                                                    echo "<option value='".$area[0]."'>".$area[1]."&nbsp;&nbsp;</option>";
                                                    ?>
                                                        </select>
                                                        <div id="areaDiv" name="areaDiv" class="error">* Area must be selected</div>
                                        </div>
                                    </td>
                                </tr>
                                
                                <tr>
                                        <td align="right" valign="top" style="padding-top:4px;" width="50%">
                                            <strong>
                                                Activity Type:&nbsp;
                                            </strong>
                                        </td>
                                        <td align="left" style="padding-top:2px;" width="50%">
                                            &nbsp;<select id="activityType" name="activityType" method="post" class="required" tabindex="<?php echo $index++; ?>">
                                            </select>
                                            <div id="activityTypeDiv" name="activityTypeDiv" class="error">* Activity type must be selected</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" valign="top" style="padding-top:4px;" width="50%">
                                            <strong>
                                                Activity:&nbsp;
                                            </strong>
                                        </td>
                                        <td align="left" style="padding-top:2px;" width="50%">
                                            &nbsp;<select id="activity" name="activity" method="post" class="required" tabindex="<?php echo $index++; ?>"></select>
                                            <div id="activityDiv" name="activityDiv" class="error">* Activity must be selected</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" valign="top" style="padding-top:4px;" width="50%">
                                            <strong>
                                                Description:&nbsp;
                                            </strong>
                                        </td>
                                        <td align="left" style="padding-top:2px;" width="50%">
                                            &nbsp;<textarea id="timeDescr" name="timeDescr" cols="50" rows="6" class="maxLength" tabindex="<?php echo $index++; ?>"><?php echo "".$_POST["timeDescr"]; ?></textarea>
                                            <div id="timeDescrDiv" name="timeDescrDiv" class="error">* Description may not be longer that 250 characters</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" valign="top" style="padding-top:4px;" width="50%">
                                            <strong>
                                                Date:&nbsp;
                                            </strong>
                                        </td>
                                        <td align="left" style="padding-top:2px;" width="50%">
                                            &nbsp;<input id="timeDate" name="timeDate" tabindex="<?php echo $index++; ?>" class="entered, date" type="text" style="text-align:right;" value="<?php
                                                if ($_POST["timeDate"] != "") echo "".$_POST["timeDate"]; else echo "".$today; ?>" />
                                            <div id="timeDateEmpty" name="timeDateEmpty" style="display: none;"><font color="#FF5900">* Date must be entered</font></div>
                                            <div id="timeDateDiv" name="timeDateDiv" class="error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" valign="top" style="padding-top:4px;" width="50%">
                                            <strong>
                                                Time Spent:&nbsp;
                                            </strong>
                                        </td>
                                        <td align="left" style="padding-top:2px;" width="50%">
                                            &nbsp;<select id="timeSpent" name="timeSpent" method="post" class="required" tabindex="<?php echo $index++; ?>">
                                                <option value="null">&nbsp;</option>
                                                <option value="0.25">0.25</option>
                                                <option value="0.50">0.50</option>
                                                <option value="0.75">0.75</option>
                                                <?php
                                                    for ($i = 1; $i <= 12; $i += 0.5)   {
                                                        echo "<option value='".number_format($i, 2)."'>".number_format($i, 2, ".", "")."</option>";
                                                    }
                                                ?>
                                            </select>
                                            <div id="timeSpentDiv" name="timeSpentDiv" class="error">* Time spent must be selected</div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <input id="btnAdd" name="btnAdd" type="button" tabindex="<?php echo $index++; ?>" value="Book Time" />
                </div>
                <br/><br/>
                <input id="save" name="save" method="post" type="hidden" value="0" />
                <br/>
                <?php
                    $headingsCSS = $left.$top.$bottom.$background;
                    $displayCSS = $left.$bottom;

                    $finalDisplay = "";
                    $display = "";

                    //  Daily Summary - Time Booked
                    
                     ///////////////////////////////
                    $hasAreas = false;
                    $areas                                                     = q("SELECT project_id, area_id FROM TimeSheet WHERE area_id > 0");
                    if(is_array($areas)) {
                        $hasAreas = true;
                    }else{
                        $hasAreas = false;
                    }
                    ////////////////////////////////
                    $extraCounter = 0;
                    if($hasAreas) {                                                 // Check if there is areas linked to these entries
                        $areaData                                           = "<td align='center' style='".$headingsCSS."'><a style='color:#FFFFFF;'>&nbsp;Area&nbsp;</a></td>";
                        $extraCounter  = 1;
                    }else{
                        $areaData                                           = "";
                        $extraCounter  = 0;
                    }
               
                      //  Daily Summary - Time Booked
                    $headings = "<tr>".
                                    "<td align='center' style='".$headingsCSS."'><a style='color:#FFFFFF;'>&nbsp;Date&nbsp;</a></td>".
                                    "<td align='center' style='".$headingsCSS."'><a style='color:#FFFFFF;'>&nbsp;Project Name&nbsp;</a></td>".
                                     $areaData.
                                    "<td align='center' style='".$headingsCSS."'><a style='color:#FFFFFF;'>&nbsp;Activity Type&nbsp;</a></td>".
                                    "<td align='center' style='".$headingsCSS."'><a style='color:#FFFFFF;'>&nbsp;Activity&nbsp;</a></td>".
                                    "<td align='center' style='".$headingsCSS."'><a style='color:#FFFFFF;'>&nbsp;Description&nbsp;</a></td>".
                                    "<td align='center' style='".$headingsCSS."'><a style='color:#FFFFFF;'>&nbsp;Time&nbsp;</a></td>".
                                "</tr>";

                    $weekStart = currentWeekStart();
                    $date = $today;
                    $dayHours = 0;
                    $totalHours = 0;

                    while ($date >= $weekStart)    {
                        $dayHours = 0;
                        $timeDisplay = "";

                        $dayHeading = ($date == $today) ? "Time booked for today by " : "Time booked for ".date("l", strtotime($date))." by ";

                        $timeSummary = q("SELECT id,projectType, project_id,area_id, activity_id,descr,time_spent,locked ".
                                                        "FROM TimeSheet WHERE user_id = '".$_SESSION["user_id"]."' AND date = '$date' ORDER BY on_date, on_time");

                        if (is_array($timeSummary))   {
                            foreach ($timeSummary as $s) {
                       
                                $companyID = ($s[1] == "CP") ? q("SELECT company_id FROM Project WHERE id = '".$s[2]."'") 
                                                            : q("SELECT companyID FROM sp_projects WHERE id = '".$s[2]."'");

                                if ($companyID == $_SESSION["company_id"])   {
                                    $project = ($s[1] == "CP") ? q("SELECT name FROM Project WHERE id = '".$s[2]."'") 
                                                                : q("SELECT name FROM sp_projects WHERE id = '".$s[2]."'");

                                    $idBox .= "".$s[0].":".$s[6]."_";
                                    $dayHours += $s[5];
                                    $totalHours += $s[5];

                                    $readonly = ($s[6] != "0") ? "readonly" : "";

                                    if ($s[1] == "CP")  {
                                        $activityType = q("SELECT type FROM ActivityTypes WHERE id = (SELECT parent_id FROM Activities WHERE id = '".$s[4]."')");
                                        $activity = q("SELECT name FROM Activities WHERE id = '".$s[4]."'");
                                    }
                                    else    {
                                        $activityType = q("SELECT name FROM sp_activityTypes WHERE id = (SELECT activityTypeID FROM sp_activities WHERE id = '".$s[4]."')");
                                        $activity = q("SELECT name FROM sp_activities WHERE id = '".$s[4]."'");
                                    }
                                    
                                    if($hasAreas){
                                        $area = q("SELECT name FROM areas WHERE id = ".$s[3]." ");
                                        if($s[3] != 0){
                                            $aData = "<td align='left' style='".$displayCSS."'>&nbsp;".$area."&nbsp;</td>";
                                        }else{
                                            $aData = "<td align='left' style=' background-color:#e1e2ee; ".$displayCSS."'>&nbsp;&nbsp;</td>";
                                        }
                                    }else{
                                        $aData = "";
                                    }
                                    
                                /////////

                                $timeDisplay .= "<tr>".
                                                    "<td align='left' style='".$displayCSS."'>".
                                                        "<input id='A".$s[0]."' name='A".$s[0]."' type='text' size='12' $readonly value='".$date."' tabindex='".$index."'>".
                                                    "</td>".
                                                    "<td align='left' style='".$displayCSS."'>&nbsp;".$project."&nbsp;</td>".
                                                    $aData.
                                                    "<td align='left' style='".$displayCSS."'>&nbsp;".$activityType."&nbsp;</td>".
                                                    "<td align='left' style='".$displayCSS."'>&nbsp;".$activity."&nbsp;</td>".
                                                    "<td align='left' style='".$displayCSS."'>".
                                                        "<input id='B".$s[0]."' name='B".$s[0]."' type='text' size='50' $readonly value='".str_replace("'", "&#39;" , $s[5])."' tabindex='".$index."'>".    // Description
                                                    "</td>".
                                                    "<td align='left' style='".$displayCSS.$right."'>".     // Time
                                                        "<input id='C".$s[0]."' class='right' name='C".$s[0]."' type='text' size='12' $readonly value='".number_format($s[6], 2, ".", "")."' tabindex='".$index."'>".
                                                    "</td>".
                                                "</tr>";
                                }
                            }
                
                            $display .= "<tr><td align='center' colspan='".(7)."'><a>$dayHeading".$_SESSION["email"].":</a></td></tr>".$headings.$timeDisplay.
                                        "<tr>".
                                            "<td colspan='".(5+$extraCounter)."' align='right' style='".$left.$bottom.$background."'><a style='color:#FFFFFF;'>Total:</a></td>".
                                            "<td align='right' style='".$left.$bottom.$right."'>".number_format($dayHours, 2, ".", "")."</td>".
                                        "</tr><tr><td colspan='".(6 )."'>&nbsp;</td></tr>";
                        }

                        $date = getDates($date, -1);
                    }

                    if ($display != "")   {
                        $display .= "<tr>".
                                        "<td colspan='".(5+$extraCounter)."' align='right' style='".$left.$top.$bottom.$background."'><a style='color:#FFFFFF;'>Grand Total:</a></td>".
                                        "<td align='right' style='".$left.$top.$bottom.$right."'>".number_format($totalHours, 2, ".", "")."</td>".
                                    "</tr><tr><td colspan='".(5)."'>&nbsp;</td></tr>".
                                    "</tr><tr><td align='center' colspan='".(5+$extraCounter)."'>".
                                        "<input id='btnUpdate' name='btnUpdate' type='submit' value='Apply Changes' tabindex='".++$index."'>".
                                    "</td></tr>".
                                    "</tr><tr><td colspan='".(5)."'>&nbsp;</td></tr>";
                    }

                    if ($display != "")
                        $finalDisplay .=  "<tr><td align='center' colspan='6'><a style='text-decoration:underline;'>Booked Time Information</a></td></tr>".$display;

                    if ($finalDisplay != "")    {
                        echo "<table class='report' cellpadding='0' cellspacing='0'>$finalDisplay</table>";
                        echo "<input method='post' name='idBox' type='hidden' value='".$idBox."' />";
                    }
                ?>
            </form>
        </td>
    </tr>
    <tr>
        <td align="center">
            <br/>
        </td>
    </tr>
</table>
<?php
    //  Print Footer
    print_footer();
?>
