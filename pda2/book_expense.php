<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if ($_SESSION["logged_in"] === true && !($_SESSION["usertype"] > "0" && $_SESSION["usertype"] < "5"))
        header("Location: home.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    //  Insert Booked Expense Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")
    {
        $errorMessage                                                   = "";

        //  Get Information
        $project_id                                                     = $_POST["project"];
        $expense_type                                                   = $_POST["expense_type"];

        if ($expense_type == "2")
        {
            $vehicle_id                                                 = $_POST["vehicle"];
            $rate                                                       = q("SELECT rate FROM Vehicle WHERE id = '$vehicle_id'");
        }

        $description                                                    = addslashes(strip_tags($_POST["description"]));
        $date                                                           = addslashes(strip_tags($_POST["book_date"]));

        if ($expense_type == "1")
            $total                                                      = addslashes(strip_tags($_POST["total_expense"]));
        else
            $total                                                      = addslashes(strip_tags($_POST["total_travel"]));

        $time                                                           = date("H:i:s");

        if ($date <= $today)
        {
            if ($expense_type == "1")
                $insert                                                 = q("INSERT INTO ExpenseSheet (date, user_id, project_id, expense_type_id, descr, expense, ".
                                                                            "on_date, on_time) VALUES ('$date', '".$_SESSION["user_id"]."', '$project_id', ".
                                                                            "'$expense_type', '$description', '$total', '$today', '$time')");
            else
                $insert                                                 = q("INSERT INTO ExpenseSheet (date, user_id, project_id, expense_type_id, vehicle_id, descr, ".
                                                                            "kilometers, rate, expense, on_date, on_time) VALUES ('$date', '".$_SESSION["user_id"]."', ".
                                                                            "'$project_id', '$expense_type', '$vehicle_id', '$description', '$total', '$rate', ".
                                                                            "'".($total * $rate)."', '$today', '$time')");

            if ($insert)
            {
                $project_name                                           = q("SELECT name FROM Project WHERE id = '$project_id'");

                $logs                                                   = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('".$_SESSION["email"]." booked expenses on ".$project_name."', 'Insert', ".
                                                                            "'ExpenseSheet', '".$_SESSION["email"]."', '$today', '$time', '".$_SESSION["company_id"]."')");

                header("Location: book_expense.php");
            }
        }
        else
            $errorMessage                                               = "Time Cannot Be Booked In Advance";
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus();

    if ($errorMessage != "")
    {
        echo "<p align='center' style='padding:0px;'><strong><font color='#999999'>$errorMessage</font></strong></p>";
        echo "<br/>";
    }

    //  nProjects                                                       = Number of Contacts
    $nProjects                                                          = q("SELECT COUNT(p.id) FROM (Project AS p INNER JOIN Project_User AS pu ".
                                                                            "ON p.id = pu.project_id) WHERE pu.user_id = '".$_SESSION["user_id"]."' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."' AND p.completed = '0'");
    $projects                                                           = q("SELECT p.id, p.name FROM (Project AS p INNER JOIN Project_User AS pu ".
                                                                            "ON p.id = pu.project_id) WHERE pu.user_id = '".$_SESSION["user_id"]."' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."' AND p.completed = '0' ORDER BY p.name");

    //  nVehicles                                                       = Number of Vehicles
    $nVehicles                                                          = q("SELECT COUNT(id) FROM Vehicle WHERE company_id = '".$_SESSION["company_id"]."'");
    $vehicles                                                           = q("SELECT id, type, rate FROM Vehicle WHERE company_id = '".$_SESSION["company_id"]."' ".
                                                                            "ORDER BY type");
?>
<script language="JavaScript">
    //  Internet Browser
    var type                                                            = "MO";

    if (window.opera)
        type                                                            = "OP";
    else if (document.all)
        type                                                            = "IE";	//IE4+
    else if (!document.all && document.getElementById)
        type                                                            = "MO";
    else
        type                                                            = "IE";

    function ShowLayer(id, action)
    {
        if (type == "IE")
            eval("document.all['" + id + "'].style.display='" + action + "'");
        else if(type == "MO" || type == "OP")
            eval("document.getElementById('" + id + "').style.display='" + action + "'");
    }

    function validation(type, value)
    {
        var currency                                                    = /^[-+]?[\d]+(\.\d{1,2})?$/;
        var date                                                        = /^[0-9]{4}-(((0[13578]|(10|12))-(0[1-9]|[1-2][0-9]|3[0-1]))|(02-(0[1-9]|[1-2][0-9]))|((0[469]|11)-(0[1-9]|[1-2][0-9]|30)))$/;
        var email                                                       = /^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/;
        var number                                                      = /^[\d]{1,9}$/;
        var percentage                                                  = /^((100(\.0{1,2})?)|([0-9]{0,2}(\.[0-9]{1,2})?))%?$/;
        var phone                                                       = /^[\d]{10,11}$/;

        if (type == "currency")
            return currency.test(value);
        else if (type == "date")
            return date.test(value);
        else if (type == "email")
            return email.test(value);
        else if (type == "number")
            return number.test(value);
        else if (type == "percentage")
            return percentage.test(value);
        else if (type == "phone")
            return phone.test(value);
    }

    function setInfo1(select)
    {
        if (select.value != "null")
            ShowLayer("getInfo1", "block");
        else
            ShowLayer("getInfo1", "none");
    }

    function setInfo2(select)
    {
        if (select.value != "null")
            if (select.value == "2")
            {
                ShowLayer("getInfo2", "block");
                ShowLayer("Travel", "block");
                ShowLayer("Expense", "none");
            }
            else
            {
                ShowLayer("getInfo2", "none");
                ShowLayer("Travel", "none");
                ShowLayer("Expense", "block");
            }
        else
        {
            ShowLayer("getInfo2", "none");
            ShowLayer("Travel", "none");
            ShowLayer("Expense", "none");
        }
    }

    function check()
    {
        var valid                                                       = 1;

        //  Check That Expense Type Is Selected
        if (document.forms["book_expense"].expense_type.value == "null")
        {
            ShowLayer("expenseType", "block");
            valid                                                       = 0;
        }
        else
        {
            ShowLayer("expenseType", "none");

            //  Expense Type - Travel
            if (document.forms["book_expense"].expense_type.value == "2")
                if (document.forms["book_expense"].vehicle.value == "null")
                {
                    ShowLayer("vehicleDiv", "block");
                    valid                                               = 0;
                }
                else
                    ShowLayer("vehicleDiv", "none");
            else
                ShowLayer("vehicleDiv", "none");

            //  Check That Description Is Not Longer Than 150 Characters
            if (document.forms["book_expense"].description.value.length > 150)
            {
                ShowLayer("descriptionDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("descriptionDiv", "none");

            //  Check That Date Is Entered
            if (document.forms["book_expense"].book_date.value == "")
            {
                ShowLayer("bookDate", "block");
                valid                                                   = 0;
            }
            //  Check That Entered Date Is Valid
            else
            {
                ShowLayer("bookDate", "none");

                if (!validation("date", document.forms["book_expense"].book_date.value))
                {
                    ShowLayer("bookDateDiv", "block");
                    valid                                               = 0;
                }
                else
                    ShowLayer("bookDateDiv", "none");
            }

            //  Expense Type - Travel
            if (document.forms["book_expense"].expense_type.value == "2")
            {
                if (!validation("number", document.forms["book_expense"].total_travel.value))
                {
                    ShowLayer("totalTravelDiv", "block");
                    valid                                               = 0;
                }
                else
                    ShowLayer("totalTravelDiv", "none");
            }
            else
            {
                if (!validation("currency", document.forms["book_expense"].total_expense.value))
                {
                    ShowLayer("totalExpenseDiv", "block");
                    valid                                               = 0;
                }
                else
                    ShowLayer("totalExpenseDiv", "none");
            }
        }

        if (valid == 1)
        {
            document.forms["book_expense"].save.value                   = 1;
            document.forms["book_expense"].submit();
        }
    }
</script>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="left" valign="top">
                <form action="" method="post" name="book_expense">
                    <table cellpadding="0" cellspacing="2">
                        <tr>
                            <td align="left">
                                <strong>
                                    Project Name:&nbsp;
                                </strong>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <select method="post" name="project" onChange="setInfo1(project);" tabindex="1">
                                    <option value="null">--  Select A Project  --&nbsp;&nbsp;</option>
                                    <?php
                                        if ($nProjects > 1)
                                            foreach ($projects as $project)
                                                if ($_POST["project"] == $project[0])
                                                    echo "<option value='".$project[0]."' selected>".$project[1]."&nbsp;&nbsp;</option>";
                                                else
                                                    echo "<option value='".$project[0]."'>".$project[1]."&nbsp;&nbsp;</option>";
                                        else if ($nProjects == 1)
                                            if ($_POST["project"] == $projects[0][0])
                                                echo "<option value='".$projects[0][0]."' selected>".$projects[0][1]."&nbsp;&nbsp;</option>";
                                            else
                                                echo "<option value='".$projects[0][0]."'>".$projects[0][1]."&nbsp;&nbsp;</option>";
                                    ?>
                                </select>
                            </td>
                        </tr>
                    </table>
                    <div id="getInfo1" style="display: none;">
                        <table cellpadding="0" cellspacing="2">
                            <tr>
                                <td align="left">
                                    <strong>
                                        Expense Type:&nbsp;
                                    </strong>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <select method="post" name="expense_type" onChange="setInfo2(expense_type);" tabindex="2">
                                        <option value="null">--  Select Expense Type  --&nbsp;&nbsp;</option>
                                        <option value="1">Expense&nbsp;&nbsp;</option>
                                        <option value="2">Travel&nbsp;&nbsp;</option>
                                    </select>
                                    <div id="expenseType" style="display: none;"><font color="#FF5900">* Expense type must be selected</font></div>
                                </td>
                            </tr>
                        </table>
                        <div id="getInfo2" style="display: none;">
                            <table cellpadding="0" cellspacing="2" width="100%">
                                <tr>
                                    <td align="left">
                                        <strong>
                                            Vehicle:&nbsp;
                                        </strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <select method="post" name="vehicle" tabindex="3">
                                            <option value="null">--  Select A Vehicle  --&nbsp;&nbsp;</option>
                                            <?php
                                                if ($nVehicles > 1)
                                                    foreach ($vehicles as $vehicle)
                                                        if ($_POST["vehicle"] == $vehicle[0])
                                                            echo "<option value='".$vehicle[0]."' selected>".$vehicle[1]."&nbsp;&nbsp;</option>";
                                                        else
                                                            echo "<option value='".$vehicle[0]."'>".$vehicle[1]."&nbsp;&nbsp;</option>";
                                                else if ($nVehicles == 1)
                                                    if ($_POST["vehicle"] == $vehicles[0][0])
                                                        echo "<option value='".$vehicles[0][0]."' selected>".$vehicles[0][1]."&nbsp;&nbsp;</option>";
                                                    else
                                                        echo "<option value='".$vehicles[0][0]."'>".$vehicles[0][1]."&nbsp;&nbsp;</option>";
                                            ?>
                                        </select>
                                        <div id="vehicleDiv" style="display: none;"><font color="#FF5900">* Vehicle must be selected</font></div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <table cellpadding="0" cellspacing="2">
                            <tr>
                                <td align="left">
                                    <strong>
                                        Desciption:&nbsp;
                                    </strong>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <input name="description" type="text" value="<?php echo "".$_POST["description"]; ?>" tabindex="4">
                                    <div id="descriptionDiv" style="display: none;"><font color="#FF5900">* Description may not be longer that 150 characters</font></div>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <strong>
                                        Date:&nbsp;
                                    </strong>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="50%">
                                    <input name="book_date" tabindex="5" type="text" style="text-align:right;" value="<?php 
                                        if ($_POST["book_date"] != "") echo "".$_POST["book_date"]; else echo "".$today; ?>">
                                    <div id="bookDate" style="display: none;"><font color="#FF5900">* Date must be entered</font></div>
                                    <div id="bookDateDiv" style="display: none;"><font color="#FF5900">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                </td>
                            </tr>
                        </table>
                        <div id="Expense" style="display: none;">
                            <table cellpadding="0" cellspacing="2">
                                <tr>
                                    <td align="left">
                                        <strong>
                                            Total <i>(<?php echo "".$_SESSION["currency"]; ?>)</i>:&nbsp;
                                        </strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        &nbsp;<input name="total_expense" style="text-align:right;" tabindex="6" type="text" value="<?php echo "".$_POST["total"]; ?>">
                                        <div id="totalExpenseDiv" style="display: none;"><font color="#FF5900">* Entered total must be numeric, eg. 123.45</font></div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="Travel" style="display: none;">
                            <table cellpadding="0" cellspacing="2">
                                <tr>
                                    <td align="left">
                                        <strong>
                                            Total <i>(Km)</i>:&nbsp;
                                        </strong>
                                    </td>
                                    <td align="left">
                                        <input name="total_travel" style="text-align:right;" tabindex="7" type="text" value="<?php echo "".$_POST["total"]; ?>">
                                        <div id="totalTravelDiv" style="display: none;"><font color="#FF5900">* Entered total must be numeric, eg. 123.45</font></div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <input name="btnAdd" onClick="check();"  tabindex="8" type="button" value="Book Expense">
                    </div>
                    <br/>
                    <input method="post" name="save" type="hidden" value="0" />
                </form>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>