<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("_functions.php");
    include("fusion/FusionCharts.php");
    include("include/sajax.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    if (!hasAccess("REP_WORK_IN_PR"))
        header("Location: noaccess.php");

    ///////////////////////////
    //  Sajax
    function getProjects($status, $type) {
        if ($status == "all")   $status = " AND p.completed IN (0,1)";
        else                    $status = " AND p.completed = '".$status."'";

        if ($type == "null")
            $projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p
                            INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id)
                            WHERE cot.company_id = '".$_SESSION["company_id"]."'
							AND p.deleted = '0' $status ORDER BY UPPER(p.name)");
        else
            $projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p
                            INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id)
                            WHERE cot.company_id = '".$_SESSION["company_id"]."'
							AND p.deleted = '0'
                            AND p.type_id='$type' $status ORDER BY UPPER(p.name)");

// if ($type == "null")
// $projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p
//                 INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id)
//                 WHERE cot.company_id = '".$_SESSION["company_id"]."' AND cot.total_budget > 0
//                 AND p.due_date != ''
//                 AND p.deleted = '0' $status ORDER BY UPPER(p.name)");
// else
// $projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p
//                 INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id)
//                 WHERE cot.company_id = '".$_SESSION["company_id"]."' AND cot.total_budget > 0
//                 AND p.due_date != ''
//                 AND p.deleted = '0'
//                 AND p.type_id='$type' $status ORDER BY UPPER(p.name)");

        return $projects;
    }

    $sajax_request_type = "GET";
    sajax_init();
    sajax_export("getProjects");
    sajax_handle_client_request();
    ///////////////////////////

    //  Set Report Generated Status
    $generated = "0";

    function createNegativeXMLString($projects,$table,$where) {
        //  Create XML String
        $xmlString = "";

        //  Calculate Values
        if (is_array($projects)) {
            $xmlString = "<chart caption='Negative WIP' palette='4' decimals='2' enableSmartLabels='1' showBorder='1' ".fusionChart().">";

            foreach ($projects as $project)
            {
                $project_name = removeSC($project[1]);
                $projectVat = q("SELECT c.vat FROM (Project AS p INNER JOIN companiesOnTeam AS c ON p.id = c.project_id) WHERE p.id = '".$project[0]."' AND c.company_id = '".$_SESSION["company_id"]."'");
                $booked = q("SELECT SUM(time_spent * rate) FROM $table WHERE project_id = '".$project[0]."' AND company_id = '".$_SESSION["company_id"]."' $where");
                $invoiced = q("SELECT SUM(consulting + (consulting * (vat / 100))) FROM LoadInvoices WHERE project_id = '".$project[0]."' AND company_id = '".$_SESSION["company_id"]."'");
                $booked = $booked + ($booked * ($projectVat / 100));
                $wip = $booked - $invoiced;

                if ($wip < 0)
                    $xmlString .= "<set label='".$project_name."' value='$wip'/>";
            }

            $xmlString .= "</chart>";
        }

        return $xmlString;
    }

    function createPositiveXMLString($projects,$table,$where) {
        //  Create XML String
        $xmlString = "";

        //  Calculate Values
        if (is_array($projects)) {
            $xmlString = "<chart caption='Positive WIP' palette='4' decimals='2' enableSmartLabels='1' showBorder='1' ".fusionChart().">";

            foreach ($projects as $project) {
                $project_name = removeSC($project[1]);
                $projectVat = q("SELECT c.vat FROM (Project AS p INNER JOIN companiesOnTeam AS c ON p.id = c.project_id) WHERE p.id = '".$project[0]."' AND c.company_id = '".$_SESSION["company_id"]."'");
                $booked = q("SELECT SUM(time_spent * rate) FROM $table WHERE project_id = '".$project[0]."' AND company_id = '".$_SESSION["company_id"]."' $where");
                $invoiced = q("SELECT SUM(consulting + (consulting * (vat / 100))) FROM LoadInvoices WHERE project_id = '".$project[0]."' AND company_id = '".$_SESSION["company_id"]."'");
                $booked = $booked + ($booked * ($projectVat / 100));
                $wip = $booked - $invoiced;

                if ($wip >= 0)
                    $xmlString .= "<set label='".$project_name."' value='$wip'/>";
            }

            $xmlString .= "</chart>";
        }

        return $xmlString;
    }

    //  Create Display Information Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")
    {
        $errorMessage = "";

        $reportOn = $_POST["reportOn"];
        $projectStatus = $_POST["projectStatus"];

        $project_type_id = $_POST["projectType"];
        $project_id = $_POST["project"];
        $displayString = "";
        $displayStringNegative = "";
        $displayStringPositive = "";
        $where = "";

        if ($project_type_id != "null" && is_numeric($project_type_id))
            $where = "AND type_id = '$project_type_id' ";

        //  Column Headings
        $headers = "<tr>
                                <th>Project Name</th>
                                <th>Project<br/>Budget <i>(".$_SESSION["currency"].")</i></th>
                                <th>Time As<br/>Cost <i>(".$_SESSION["currency"].")</i></th>
                                <th>Invoiced <i>(".$_SESSION["currency"].")</i></th>
                                <th>WIP <i>(".$_SESSION["currency"].")</i></th>
                            </tr>";

        //  Create Total Variable
        $total_budget_negative = 0;
        $total_booked_negative = 0;
        $total_invoiced_negative = 0;
        $total_wip_negative = 0;
        $total_budget_positive = 0;
        $total_booked_positive = 0;
        $total_invoiced_positive = 0;
        $total_wip_positive = 0;

        if ($projectStatus == "all")    $projectStatus = " AND p.completed IN (0,1)";
        else                            $projectStatus = " AND p.completed = '".$projectStatus."'";

        if ($project_id == "all") {
            $projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p
                        INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id)
                        WHERE cot.company_id = '".$_SESSION["company_id"]."'
                        AND p.deleted = '0' $where $projectStatus ORDER BY UPPER(p.name)");
            // $projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p
            // INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id)
            // WHERE cot.company_id = '".$_SESSION["company_id"]."'
            // AND cot.total_budget > 0
            // AND p.due_date != ''
            // AND p.deleted = '0' $where $projectStatus ORDER BY UPPER(p.name)");

            $table = ($reportOn == "approved") ? "ApprovedTime" : "TimeSheet";
            $where = ($reportOn == "approved") ? " AND status = '2'" : "";

            if (is_array($projects)) {
                foreach ($projects as $project) {
                    $projectVat = q("SELECT c.vat FROM (Project AS p INNER JOIN companiesOnTeam AS c ON p.id = c.project_id) WHERE p.id = '".$project[0]."' AND c.company_id = '".$_SESSION["company_id"]."'");
                    $projectConsultingBudget = q("SELECT c.consulting FROM (Project AS p INNER JOIN companiesOnTeam AS c ON p.id = c.project_id) WHERE p.id = '".$project[0]."' AND c.company_id = '".$_SESSION["company_id"]."'");
                    $booked = q("SELECT SUM(time_spent * rate) FROM $table WHERE project_id = '".$project[0]."' AND company_id = '".$_SESSION["company_id"]."' $where");
                    $invoiced = q("SELECT SUM(consulting + (consulting * (vat / 100))) FROM LoadInvoices WHERE project_id = '".$project[0]."' AND company_id = '".$_SESSION["company_id"]."'");
                    $booked = $booked + ($booked * ($projectVat / 100));
                    $wip = $booked - $invoiced;

                    if ($wip < 0) {
                        $total_budget_negative += $projectConsultingBudget;
                        $total_booked_negative += $booked;
                        $total_invoiced_negative += $invoiced;
                        $total_wip_negative += $wip;

                        $displayStringNegative .= "<tr>
                                                                    <td>".$project[1]."</td>
                                                                    <td class='rightdata'>".number_format($projectConsultingBudget, 2, ".", "")."</td>
                                                                    <td class='rightdata'>".number_format($booked, 2, ".", "")."</td>
                                                                    <td class='rightdata'>".number_format($invoiced, 2, ".", "")."</td>";
                                                                    if($wip < 0)
                                                                        $fontColor = "style='color:#FF0000'";
                                                                    else
                                                                        $fontColor = "";
                        $displayStringNegative .= "<td ".$fontColor." class='rightdata'>".number_format($wip, 2, ".", "")."</td>
                                                                </tr>";
                    }
                    else {
                        $total_budget_positive += $projectConsultingBudget;
                        $total_booked_positive += $booked;
                        $total_invoiced_positive += $invoiced;
                        $total_wip_positive += $wip;

                        $displayStringPositive .= "<tr>
                                                                    <td>".$project[1]."</td>
                                                                    <td class='rightdata'>".number_format($projectConsultingBudget, 2, ".", "")."</td>
                                                                    <td class='rightdata'>".number_format($booked, 2, ".", "")."</td>
                                                                    <td class='rightdata'>".number_format($invoiced, 2, ".", "")."</td>";
                                                                    if($wip < 0)
                                                                        $fontColor = "style='color:#FF0000'";
                                                                    else
                                                                        $fontColor = "";
                        $displayStringPositive .= "<td ".$fontColor." class='rightdata'>".number_format($wip, 2, ".", "")."</td>
                                                                </tr>";
                    }
                }
            }
        }
        else {
            $projects = q("SELECT id, name FROM Project WHERE id = '$project_id'");
            $project_name = q("SELECT name FROM Project WHERE id = '$project_id'");
            $query = "SELECT name FROM Project WHERE id = '".$project_id."'";

            $table = ($reportOn == "approved") ? "ApprovedTime" : "TimeSheet";
            $where = ($reportOn == "approved") ? " AND status = '2'" : "";

            $projectVat = q("SELECT c.vat FROM (Project AS p INNER JOIN companiesOnTeam AS c ON p.id = c.project_id) WHERE p.id = '".$project_id."' AND c.company_id = '".$_SESSION["company_id"]."'");
            $projectConsultingBudget = q("SELECT c.consulting FROM (Project AS p INNER JOIN companiesOnTeam AS c ON p.id = c.project_id) WHERE p.id = '".$project_id."' AND c.company_id = '".$_SESSION["company_id"]."'");
            $booked = q("SELECT SUM(time_spent * rate) FROM $table WHERE project_id = '".$project_id."' AND company_id = '".$_SESSION["company_id"]."' $where");
            $invoiced = q("SELECT SUM(consulting + (consulting * (vat / 100))) FROM LoadInvoices WHERE project_id = '".$project_id."' AND company_id = '".$_SESSION["company_id"]."'");
            $booked = $booked + ($booked * ($projectVat / 100));
            $wip = $booked - $invoiced;

            if ($wip < 0) {
                $total_budget_negative += $projectConsultingBudget;
                $total_booked_negative += $booked;
                $total_invoiced_negative += $invoiced;
                $total_wip_negative += $wip;

                $displayStringNegative .= "<tr>
                                                                <td>".$project_name."</td>
                                                                <td class='rightdata'>".number_format($projectConsultingBudget, 2, ".", "")."</td>
                                                                <td class='rightdata'>".number_format($booked, 2, ".", "")."</td>
                                                                <td class='rightdata'>".number_format($invoiced, 2, ".", "")."</td>";
                                                                 if($wip < 0)
                                                                        $fontColor = "style='color:#FF0000'";
                                                                    else
                                                                        $fontColor = "";
                 $displayStringNegative .= "<td ".$fontColor." class='rightdata'>".number_format($wip, 2, ".", "")."</td>
                                                            </tr>";
            }
            else {
                $total_budget_positive += $projectConsultingBudget;
                $total_booked_positive += $booked;
                $total_invoiced_positive += $invoiced;
                $total_wip_positive += $wip;

                $displayStringPositive .= "<tr>
                                                                <td>".$project_name."</td>
                                                                <td class='rightdata'>".number_format($projectConsultingBudget, 2, ".", "")."</td>
                                                                <td class='rightdata'>".number_format($booked, 2, ".", "")."</td>
                                                                <td class='rightdata'>".number_format($invoiced, 2, ".", "")."</td>
                                                                <td class='rightdata'>".number_format($wip, 2, ".", "")."</td>
                                                            </tr>";
            }
        }

        if ($displayStringPositive != "")       {
            $graph = createPositiveXMLString($projects,$table,$where);

            if ($graph != "")   $displayString .= renderChart("fusion/Pie3D.swf", "", $graph, "multigraph1", 800, 400, false, false);

            $displayString .= $headers.$displayStringPositive.
                                "<tr>
                                    <td class='on-table-total'>Grand Total <i>(".$_SESSION["currency"].")</i>:</td>
                                    <td class='on-table-total'>".number_format($total_budget_positive, 2, ".", "")."</td>
                                    <td class='on-table-total'>".number_format($total_booked_positive, 2, ".", "")."</td>
                                    <td class='on-table-total'>".number_format($total_invoiced_positive, 2, ".", "")."</td>
                                    <td class='on-table-total'>".number_format($total_wip_positive, 2, ".", "")."</td>
                                </tr>
                                <tfoot>
                                    <tr>
                                        <td colspan='100%'></td>
                                    </tr>
                                </tfoot>
                            </table>
                            <br/>
                            <table class='on-table-center on-table'>";
        }

        if ($displayStringNegative != "")       {
            $graph = createNegativeXMLString($projects,$table,$where);

            if ($graph != "")   $displayString .= renderChart("fusion/Pie3D.swf", "", $graph, "multigraph2", 800, 400, false, false);

            $displayString .= $headers.$displayStringNegative.
                                "<tr>
                                    <td class='on-table-total'>Grand Total <i>(".$_SESSION["currency"].")</i>:</td>
                                    <td class='on-table-total'>".number_format($total_budget_negative, 2, ".", "")."</td>
                                    <td class='on-table-total'>".number_format($total_booked_negative, 2, ".", "")."</td>
                                    <td class='on-table-total'>".number_format($total_invoiced_negative, 2, ".", "")."</td>
                                    <td class='on-table-total'>".number_format($total_wip_negative, 2, ".", "")."</td>
                                </tr>";
        }

        if ($displayString != "")
            $generated = "1";
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("3", "reports");

    if ($errorMessage != "") {
        echo "<p style='padding:0px;'><strong><font class='on-validate-error'>$errorMessage</font></strong></p>";
        echo "<br/>";
    }
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    function check()
    {
        var valid                                                       = 1;

        //  Check That Project Is Selected
        if (document.forms["report_work_in_process"].project.value == "null")
        {
            ShowLayer("projectDiv", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("projectDiv", "none");

        if (valid == 1)
        {
            document.forms["report_work_in_process"].save.value         = 1;
            document.forms["report_work_in_process"].submit();
        }
    }

    ///////////////////////////
    //  Sajax
    <?php sajax_show_javascript(); ?>

    function ClearOptions(OptionList) {
        for (x = OptionList.length; x >= 0; x--)
        {
            OptionList[x]                                               = null;
        }
    }

    function setProjects(data) {
        var c                                                           = 0;

        document.forms["report_work_in_process"].project.options[(c++)] = new Option("--  Select A Project  --", "null");
        document.forms["report_work_in_process"].project.options[(c++)] = new Option("All Projects", "all");

        for (var i in data)
            eval("document.forms['report_work_in_process'].project.options["+(c++)+"] = new Option('" + data[i][1] + "', '" + data[i][0] + " ');");
    }

    function getProjects(select1,select2) {
        var status = eval("document.report_work_in_process."+select1+".value;");
        var type = eval("document.report_work_in_process."+select2+".value;");;

        ClearOptions(document.report_work_in_process.project);
        x_getProjects(status, type, setProjects);
    }
    ///////////////////////////
</script>
<?php
    $projectTypes = q("SELECT id, type FROM ProjectTypes WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY type");
    $projectTypes[] = array("0","Shared Projects");
    $projectTypes = sortArrayByColumn($projectTypes, 1);

    $projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p
                INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id)
                WHERE cot.company_id = '".$_SESSION["company_id"]."'
                AND p.completed = '0'
                AND p.deleted = '0' ORDER BY UPPER(p.name)");
    // $projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p
    // INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id)
    // WHERE cot.company_id = '".$_SESSION["company_id"]."'
    // AND cot.total_budget > 0
    // AND p.due_date != ''
    // AND p.completed = '0'
    // AND p.deleted = '0' ORDER BY UPPER(p.name)");
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata">
                <form action="" method="post" name="report_work_in_process">
                    <div id="" style="<?php if ($generated == "1") echo "display: none;"; else echo "display: block"; ?>">
                        <table width="100%">
                            <tr>
                                <td class="centerdata">
                                    <h6>Work In Process (Consulting)</h6>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Report On:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="reportOn">
                                        <option value="normal">Actual Time</option>
                                        <option value="approved">Approved Time</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Project Status:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="projectStatus" onChange="getProjects('projectStatus','projectType');">
                                        <option value="all">Completed & Uncompleted</option>
                                        <option value="0" selected>Uncompleted</option>
                                        <option value="1">Completed</option>
                                    </select>
                                </td>
                            </tr>
                            <tr><td colspan="100%"><br/></td></tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Project Type:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="projectType" onChange="getProjects('projectStatus','projectType');">
                                        <option value="null">All Project Types</option>
                                        <?php
                                            if (is_array($projectTypes))
                                                foreach ($projectTypes as $projectType)
                                                    if ($_POST["projectType"] == $projectType[0])
                                                        echo "<option value='".$projectType[0]."' selected>".$projectType[1]."</option>";
                                                    else
                                                        echo "<option value='".$projectType[0]."'>".$projectType[1]."</option>";
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Project:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" id="project" name="project">
                                        <option value="null">--  Select A Project  --</option>
                                        <option value="all">All Projects</option>
                                        <?php
                                            if (is_array($projects))
                                                foreach ($projects as $project)
                                                    if (isset($_POST["project"]) && $_POST["project"] == $project[0])
                                                        echo "<option value='".$project[0]."' selected>".$project[1]."</option>";
                                                    else
                                                        echo "<option value='".$project[0]."'>".$project[1]."</option>";
                                        ?>
                                    </select>
                                    <div id="projectDiv" style="display: none;"><font class="on-validate-error">* Project must be selected</font></div>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <input name="btnGenerate" onClick="check();" type="button" value="Generate Report">
                        <input method="post" name="save" type="hidden" value="0" />
                    </div>
                </form>
                <div id="report" style="<?php if ($generated == "1") echo "display: block;"; else echo "display: none"; ?>">
                    <?php
                        echo "<table width='100%'>";
                            echo "<tr>";
                                echo "<td class='centerdata'>";
                                        echo "<h6>Work In Process Report</h6><br>";
                                echo "</td>";
                            echo "</tr>";
                            echo "<tr>";
                                echo "<td class='centerdata'>";
                                        echo "<h6>Company Name: ".$_SESSION["company_name"]."</h6>";
                                echo "</td>";
                            echo "</tr>";
                        echo "</table>";
                        echo "<br/>";

                        echo "<table class='on-table-center on-table'>";
                            echo "".$displayString;
                            echo  "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        echo "</table>";
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
