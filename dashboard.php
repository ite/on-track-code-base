<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("_functions.php");
    include("fusion/FusionCharts.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    //  Print Header
    print_header();
    //  Print Menu
	if ($_GET["menu"] == "")
		print_menus("0", "home");
	else
		print_menus($_GET["menu"], "home");

	if ($_GET["menu"] == "") {
?>
<script language="JavaScript">
    
    var viewNum = 0;
    var viewBehind = 0;
    var viewProj = 0;
    var viewBehindProj = 0;
        
     jQuery(function()    {      
        jQuery(".viewNumEmployees").click(function()     {
            if(!viewNum)
                jQuery(".numEmployeeDiv").show();
            else
                jQuery(".numEmployeeDiv").hide();
            viewNum = !viewNum;
        });
        jQuery(".viewEmployees").click(function()     {
           if(!viewBehind)
                jQuery(".employeeDiv").show();
            else
                jQuery(".employeeDiv").hide();
            viewBehind = !viewBehind;
        });
        jQuery(".viewProj").click(function()     {
           if(!viewProj)
                jQuery(".numProjDiv").show();
            else
                jQuery(".numProjDiv").hide();
            viewProj = !viewProj;
        });
        
        jQuery(".viewActiveProj").click(function()     {
            alert("ABC");
        });
        jQuery(".viewCompletedProj").click(function()     {
           alert("123");
        });
        
        jQuery(".viewProjOverDue").click(function()     {
           if(!viewBehindProj)
                jQuery(".projOverDueDiv").show();
            else
                jQuery(".projOverDueDiv").hide();
            viewBehindProj = !viewBehindProj;
        });
     });
</script>
    <table width="100%">
        <tr>
            <td align="center" valign="top">
                <form action="" method="post" name="home">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Dashboard</h6>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <?php
                        $date = $today;
                        $date = getDates($date, -4);    // 4 Days back        
                        
                        $employeeCount = q("SELECT COUNT(id) FROM Employee WHERE deleted='0' AND demo_user='0' AND company_id = '".$_SESSION["company_id"]."'");
                        $employees = q("SELECT id, frstname, lstname FROM Employee WHERE deleted='0' AND demo_user='0' AND company_id = '".$_SESSION["company_id"]."' ORDER BY lstname");
                        $employeeBehindCount = $employeeCount - q("SELECT COUNT(DISTINCT(user_id)) FROM TimeSheet WHERE date >= '".$date."'AND company_id = '".$_SESSION["company_id"]."'");
                        
                        $companyTime = q("SELECT SUM(time_spent) FROM TimeSheet WHERE company_id = '".$_SESSION["company_id"]."'");
                        
                        $projectCount = q("SELECT COUNT(id) FROM Project WHERE company_id = '".$_SESSION["company_id"]."'");
                        $projects= q("SELECT name FROM Project WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY name");
                        $activeProjects = q("SELECT COUNT(id) FROM Project WHERE completed = '0' AND company_id = '".$_SESSION["company_id"]."'");
                        $completedProjects = q("SELECT COUNT(id) FROM Project WHERE completed = '1' AND company_id = '".$_SESSION["company_id"]."'");
                        
                        $projectOverDueCount = q("SELECT COUNT(id) FROM Project WHERE completed = '0' AND due_date < '".$today."' AND company_id = '".$_SESSION["company_id"]."'");
                        $projectOverDue= q("SELECT name, due_date FROM Project WHERE completed = '0' AND due_date < '".$today."' AND company_id = '".$_SESSION["company_id"]."' ORDER BY name");
                        
                        if($companyTime =="")
                            $companyTime = "Not Available"
                    ?>
                    
                    <table width="100%">
                        <tr>
                            <td colspan="100%" class="centerdata">
                                <br/>
                                <h6>Employees</h6>
                            </td>
                        </tr>
                    </table>
                    
                    <table width="100%"> 
                        <tr>
                            <td class="on-description" width="50%">
                               Employees in company: 
                            </td>
                            <td class="on-description-left-orange" width="50%">
                                <?php  echo $employeeCount; ?>&nbsp;&nbsp;<a class="viewNumEmployees" style="cursor:pointer; color:#acabab">&raquo;</a>
                            </td>
                        </tr>
                        <tr>
                            <td class="centerdata" colspan="100%" width="50%">
                                <div class="numEmployeeDiv" style="display:none">
                                    <table class="on-table on-table-center">
                                        <?php 
                                            if(is_array($employees))    {
                                                foreach($employees as $e) {
                                                    echo "<tr><td>".$e[2].", ".$e[1]."</td></tr>";
                                                }
                                            }
                                        ?>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                               Employees behind schedule:
                            </td>
                            <td class="on-description-left-orange" width="50%">
                                <?php echo $employeeBehindCount; ?>&nbsp;&nbsp;<a class="viewEmployees" style="cursor:pointer; color:#acabab">&raquo;</a>
                            </td>
                        </tr>
                        <tr>
                            <td class="centerdata" colspan="100%" width="50%">
                                <div class="employeeDiv" style="display:none">
                                    <table class="on-table on-table-center">
                                        <?php 
                                            if(is_array($employees))    {
                                                foreach($employees as $e){
                                                    $date_last_booked = q("SELECT MAX(date) FROM TimeSheet WHERE user_id = '".$e[0]."'");
                                                    if($date_last_booked < $date)   {
                                                        if(checkLeave($date, $today, $_SESSION["company_id"], $e[0])){
                                                            echo "<tr><td>".$e[2].", ".$e[1]."</td><td style='color:#ff8e0b'>Leave</td></tr>";
                                                        }else{
                                                             echo "<tr><td>".$e[2].", ".$e[1]."</td><td style='color:#ff0b0b'>Behind</td></tr>";
                                                        }
                                                    }else{
                                                        echo "<tr><td>".$e[2].", ".$e[1]."</td><td style='color:#09c900'>Active</td></tr>";
                                                    }
                                                }
                                            }
                                        ?>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                               Hours booked by company: 
                            </td>
                            <td class="on-description-left-orange" width="50%">
                                <?php  echo $companyTime; ?>
                            </td>
                        </tr>
                    </table>
                    <br/><br/>
                    <table width="100%">
                        <tr>
                            <td colspan="100%" class="centerdata">
                                <h6>Projects</h6>
                            </td>
                        </tr>
                    </table>
                    <table width="100%"> 
                        <tr>
                            <td class="on-description" width="50%">
                               Projects: 
                            </td>
                            <td class="on-description-left-orange" width="50%">
                                <?php  echo $projectCount; ?>&nbsp;&nbsp;<a class="viewProj" style="cursor:pointer; color:#acabab">&raquo;</a>
                            </td>
                        </tr>
                        
                        <tr>
                            <td class="centerdata" colspan="100%" width="50%">
                                <div class="numProjDiv" style="display:none">
                                    <table class="on-table on-table-center">
                                        <?php 
                                            if(is_array($projects))    {
                                                foreach($projects as $p) {
                                                    echo "<tr><td>".$p[0]."</td></tr>";
                                                }
                                            }
                                        ?>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        
                        <tr>
                            <td class="on-description" width="50%">
                               Active projects:
                            </td>
                            <td class="on-description-left-orange" width="50%">
                                <?php  echo $activeProjects; ?>&nbsp;&nbsp;<a class="viewActiveProj" style="cursor:pointer; color:#acabab">&raquo;</a>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                               Completed projects: 
                            </td>
                            <td class="on-description-left-orange" width="50%">
                                <?php  echo $completedProjects; ?>&nbsp;&nbsp;<a class="viewCompletedProj" style="cursor:pointer; color:#acabab">&raquo;</a>
                            </td>
                        </tr>
                    </table>
                    <table width="100%"> 
                        <tr>
                            <td class="on-description" width="50%">
                               Projects over due date:
                            </td>
                            <td class="on-description-left-orange" width="50%">
                                <?php  echo $projectOverDueCount; ?>&nbsp;&nbsp;<a class="viewProjOverDue" style="cursor:pointer; color:#acabab">&raquo;</a>
                            </td>
                        </tr>
                        <tr>
                            <td class="centerdata" colspan="100%" width="50%">
                                <div class="projOverDueDiv" style="display:none">
                                    <table class="on-table on-table-center">
                                        <?php 
                                            if(is_array($projectOverDue))    {
                                                foreach($projectOverDue as $pOver) {
                                                    if($pOver[1] == "")
                                                        $pOver[1] = "no date";
                                                    echo "<tr><td>".$pOver[0]." (".$pOver[1].")</td></tr>";
                                                }
                                            }
                                        ?>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <br/><br/>

                    <table width="100%">
                        <tr>
                            <td colspan="100%" class="centerdata">
                                <h6>Graphs</h6>
                            </td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td colspan="100%" class="centerdata">
                                <br/>
                                <!--<h6>-- Cashflow forecast --</h6>-->
                                <?php
                                    // - DATES - //
                                    $date_from = getDates($today, -15); 
                                    $date_to = getDates($today, 35);
                                    
                                    function getAnchorAlpha($arr)   {
                                        $cc = 0;
                                        if (!is_array($arr)) return 100;
                                        foreach ($arr as $d)    {
                                            if ($d[0] !== "NULL")   $cc++;
                                            if ($cc > 52)   return 0;
                                        }
                                        return 100;
                                    }
                                    function getLabelCount($arr)	{
                                        return (count($arr) > 30) ? ceil(count($arr) / 30.0) : 1;
                                    }
                                    function createXMLString($date_from, $date_to, $consulting_array, $expenses_array, $combination_array)  {
                                        $display_number_of_labels = getLabelCount($consulting_array);
                                        $al = getAnchorAlpha($consulting_array);
                                        $xmlString                                                      = "";
                                        $xmlString                                                      = "<chart caption='Cash Flow Forecast Report' subcaption='' xAxisName='Date' ".
                                                                                                            "numberPrefix='". $_SESSION["currency"]."' showValues='0' labelDisplay='rotate' ".
                                                                                                            "labelStep = '$display_number_of_labels' chartTopMargin='4' chartBottomMargin='10' chartLeftMargin='4' chartRightMargin='30' captionPadding='4' ".fusionChart().">";
                                        //  Create Categories
                                        $plus                                                           = 0;
                                        $date                                                           = $date_from;
                                        $xmlString                                                      .= "<categories>";
                                        while ($date <= $date_to)    {
                                            $xmlString                                                  .= "<category label='$date' />";
                                            $bank                                                       = q("SELECT amount FROM StartBalance WHERE date = '$date' ".
                                                                                                            "AND company_id = '".$_SESSION["company_id"]."'");
                                            if ($bank != 0)
                                                $xmlString                                              .= "<vLine color='000000' thickness='2' alpha='50' dashed='1' />";
                                            $plus++;
                                            $date                                                       = getDates($date_from, $plus);
                                        }
                                        $xmlString                                                      .= "</categories>";
                                        //  Create Dataset - Consulting
                                        $plus                                                           = 0;
                                        $date                                                           = $date_from;
                                        $xmlString                                                      .= "<dataset color='0033FF' seriesName='Consulting'>";
                                        while ($date <= $date_to)    {
                                            if ($consulting_array[$date] < 0)
                                                $xmlString                                              .= "<set color='FF0000' value='".$consulting_array[$date]."' anchorAlpha = '$al'/>";
                                            else
                                                $xmlString                                              .= "<set color='0033FF' value='".$consulting_array[$date]."' anchorAlpha = '$al'/>";
                                            $plus++;
                                            $date                                                       = getDates($date_from, $plus);
                                        }
                                        $xmlString                                                      .= "</dataset>";
                                        //  Create Dataset - Expenses
                                        $plus                                                           = 0;
                                        $date                                                           = $date_from;
                                        $xmlString                                                      .= "<dataset color='FF9933' seriesName='Expenses'>";
                                        while ($date <= $date_to)   {
                                            if ($expenses_array[$date] < 0)
                                                $xmlString                                              .= "<set color='FF0000' value='".$expenses_array[$date]."' anchorAlpha = '$al'/>";
                                            else
                                                $xmlString                                              .= "<set color='FF9933' value='".$expenses_array[$date]."' anchorAlpha = '$al'/>";
                                            $plus++;
                                            $date                                                       = getDates($date_from, $plus);
                                        }
                                        $xmlString                                                      .= "</dataset>";
                                        //  Create Dataset - Combination
                                        $plus                                                           = 0;
                                        $date                                                           = $date_from;

                                        $xmlString                                                      .= "<dataset color='33FF00' seriesName='Combination'>";
                                        while ($date <= $date_to)   {
                                            if ($combination_array[$date] < 0)
                                                $xmlString                                              .= "<set color='FF0000' value='".$combination_array[$date]."' anchorAlpha = '$al'/>";
                                            else
                                                $xmlString                                              .= "<set color='33FF00' value='".$combination_array[$date]."' anchorAlpha = '$al'/>";
                                            $plus++;
                                            $date                                                       = getDates($date_from, $plus);
                                        }
                                        $xmlString                                                      .= "</dataset>";
                                        $xmlString                                                      .= "<trendLines>";
                                        $xmlString                                                      .= "<line startValue='0' color='000000' thickness='2' />";
                                        $xmlString                                                      .= "</trendLines>";
                                        $xmlString                                                      .= "</chart>";
                                        return $xmlString;
                                    }
                                    
                                    $displayString                                                  = "";
                                    //  Create Arrays
                                    $start_balance_array                                            = array();
                                    $invoices_consulting_array                                 = array();
                                    $invoices_expense_array                                     = array();
                                    $budget_assigned_array                                      = array();
                                    $budget_expense_array                                       = array();
                                    $consulting_array                                                = array();
                                    $expenses_array                                                  = array();
                                    $combination_array                                             = array();

                                    $plus                                                              = 0;
                                    $date                                                              = $date_from;
                                    $balance1                                                       = 0;
                                    $balance2                                                       = 0;
                                    $balance3                                                       = 0;

                                    //  Calculate Balances @ Start Date
                                    $tempDate                                                       = q("SELECT id, date FROM ExpenseStatus WHERE date <= '$date' AND ".
                                                                                                        "company_id = '".$_SESSION["company_id"]."' ORDER BY date DESC");
                                    $tempDate                                                       = $tempDate[0][1];

                                    //  Expense Balance @ Start Date
                                    if ($tempDate != 0)  {
                                        $balance2                                                   = q("SELECT amount FROM ExpenseStatus WHERE date = '$tempDate' ".
                                                                                                        "AND company_id = '".$_SESSION["company_id"]."'");
                                        $invoiced_expenses                                          = q("SELECT SUM(li.expenses + (li.expenses * (li.vat / 100))) FROM (LoadInvoices AS li ".
                                                                                                        "INNER JOIN Project AS p ON p.id = li.project_id) WHERE li.due_date >= '$tempDate' ".
                                                                                                        "AND li.due_date < '$date' AND p.company_id = '".$_SESSION["company_id"]."'");
                                        $budget_expenses                                            = q("SELECT SUM(amount + (amount * (vat / 100))) FROM ExpenseBudget ".
                                                                                                        "WHERE due_date >= '$tempDate' AND due_date < '$date' ".
                                                                                                        "AND company_id = '".$_SESSION["company_id"]."'");
                                        $invoiced_diverse_income                                    = q("SELECT SUM(li.diverse_income + (li.diverse_income * (li.vat / 100))) ".
                                                                                                        "FROM (LoadInvoices AS li INNER JOIN Project AS p ON p.id = li.project_id) ".
                                                                                                        "WHERE li.due_date >= '$tempDate' AND li.due_date < '$date' ".
                                                                                                        "AND p.company_id = '".$_SESSION["company_id"]."'");
                                        $balance2                                                   += ($invoiced_expenses - ($budget_expenses + $invoiced_diverse_income));
                                    }else       {
                                        $invoiced_expenses                                          = q("SELECT SUM(li.expenses + (li.expenses * (li.vat / 100))) FROM (LoadInvoices AS li ".
                                                                                                        "INNER JOIN Project AS p ON p.id = li.project_id) WHERE li.due_date < '$date' ".
                                                                                                        "AND p.company_id = '".$_SESSION["company_id"]."'");
                                        $budget_expenses                                            = q("SELECT SUM(amount + (amount * (vat / 100))) FROM ExpenseBudget WHERE due_date < '$date' ".
                                                                                                        "AND company_id = '".$_SESSION["company_id"]."'");
                                        $invoiced_diverse_income                                    = q("SELECT SUM(li.diverse_income + (li.diverse_income * (li.vat / 100))) ".
                                                                                                        "FROM (LoadInvoices AS li INNER JOIN Project AS p ON p.id = li.project_id) ".
                                                                                                        "WHERE li.due_date < '$date' AND p.company_id = '".$_SESSION["company_id"]."'");
                                        $balance2                                                   += ($invoiced_expenses - ($budget_expenses + $invoiced_diverse_income));
                                    }

                                    $tempDate                                                       = q("SELECT id, date FROM StartBalance WHERE date < '$date' ".
                                                                                                        "AND company_id = '".$_SESSION["company_id"]."' ORDER BY date DESC");
                                    $tempDate                                                       = $tempDate[0][1];

                                    //  Consulting Balance @ Start Date
                                    if ($tempDate != 0) {
                                        $balance1                                                   = q("SELECT amount FROM StartBalance WHERE date = '$tempDate' ".
                                                                                                        "AND company_id = '".$_SESSION["company_id"]."'");
                                        $invoiced_consulting                                        = q("SELECT SUM(li.consulting + (li.consulting * (li.vat / 100))) ".
                                                                                                        "FROM (LoadInvoices AS li INNER JOIN Project AS p ON p.id = li.project_id) ".
                                                                                                        "WHERE li.due_date >= '$tempDate' AND li.due_date < '$date' ".
                                                                                                        "AND p.company_id = '".$_SESSION["company_id"]."'");
                                        $invoiced_diverse_income                                    = q("SELECT SUM(li.diverse_income + (li.diverse_income * (li.vat / 100))) ".
                                                                                                        "FROM (LoadInvoices AS li INNER JOIN Project AS p ON p.id = li.project_id) ".
                                                                                                        "WHERE li.due_date >= '$tempDate' AND li.due_date < '$date' ".
                                                                                                        "AND p.company_id = '".$_SESSION["company_id"]."'");
                                        $expenses                                                   = q("SELECT SUM(amount) FROM Budget WHERE due_date >= '$tempDate' AND due_date < '$date' ".
                                                                                                        "AND company_id = '".$_SESSION["company_id"]."'");

                                        $balance1                                                   += ($invoiced_consulting + $invoiced_diverse_income) - $expenses;
                                        $balance3                                                   = $balance1 + $balance2;
                                    }else       {
                                        $invoiced_consulting                                        = q("SELECT SUM(li.consulting + (li.consulting * (li.vat / 100))) ".
                                                                                                        "FROM (LoadInvoices AS li INNER JOIN Project AS p ON p.id = li.project_id) ".
                                                                                                        "WHERE li.due_date < '$date' AND p.company_id = '".$_SESSION["company_id"]."'");
                                        $invoiced_diverse_income                                    = q("SELECT SUM(li.diverse_income + (li.diverse_income * (li.vat / 100))) ".
                                                                                                        "FROM (LoadInvoices AS li INNER JOIN Project AS p ON p.id = li.project_id) ".
                                                                                                        "WHERE li.due_date < '$date' AND p.company_id = '".$_SESSION["company_id"]."'");
                                        $expenses                                                   = q("SELECT SUM(amount) FROM Budget WHERE due_date < '$date' ".
                                                                                                        "AND company_id = '".$_SESSION["company_id"]."'");

                                        $balance1                                                   += ($invoiced_consulting + $invoiced_diverse_income) - $expenses;
                                        $balance3                                                   = $balance1 + $balance2;
                                    }

                                    while ($date <= $date_to)   {
                                        $start_balance                                              = q("SELECT amount FROM StartBalance WHERE date = '$date' ".
                                                                                                        "AND company_id = '".$_SESSION["company_id"]."'");

                                        if ($start_balance != 0)
                                            $balance1                                               = $start_balance;

                                        $invoiced_consulting                                        = q("SELECT SUM(li.consulting + (li.consulting * (li.vat / 100))) ".
                                                                                                        "FROM (LoadInvoices AS li INNER JOIN Project AS p ON p.id = li.project_id) ".
                                                                                                        "WHERE li.due_date = '$date' AND p.company_id = '".$_SESSION["company_id"]."'");
                                        $invoiced_diverse_income                                    = q("SELECT SUM(li.diverse_income + (li.diverse_income * (li.vat / 100))) ".
                                                                                                        "FROM (LoadInvoices AS li INNER JOIN Project AS p ON p.id = li.project_id) ".
                                                                                                        "WHERE li.due_date = '$date' AND p.company_id = '".$_SESSION["company_id"]."'");
                                        $invoiced_expenses                                          = q("SELECT SUM(li.expenses + (li.expenses * (li.vat / 100))) FROM (LoadInvoices AS li ".
                                                                                                        "INNER JOIN Project AS p ON p.id = li.project_id) WHERE li.due_date = '$date' ".
                                                                                                        "AND p.company_id = '".$_SESSION["company_id"]."'");
                                        $expenses                                                   = q("SELECT SUM(amount) FROM Budget WHERE due_date = '$date' ".
                                                                                                        "AND company_id = '".$_SESSION["company_id"]."'");
                                        $budget_expenses                                            = q("SELECT SUM(amount + (amount * (vat / 100))) FROM ExpenseBudget WHERE due_date = '$date' ".
                                                                                                        "AND company_id = '".$_SESSION["company_id"]."'");

                                        $balance1                                                   += ($invoiced_consulting + $invoiced_diverse_income) - $expenses;
                                        $balance2                                                   += ($invoiced_expenses - ($budget_expenses + $invoiced_diverse_income));
                                        $balance3                                                   = $balance1 + $balance2;

                                        if ($start_balance != 0)
                                            $start_balance_array[$date]                             = number_format($start_balance, 2, ".", "");
                                        else
                                            $start_balance_array[$date]                             = "-";

                                        if (($invoiced_consulting + $invoiced_diverse_income) > 0)
                                            $invoices_consulting_array[$date]                      = number_format(($invoiced_consulting + $invoiced_diverse_income), 2, ".", "");
                                        else
                                            $invoices_consulting_array[$date]                      = "-";

                                        if (($invoiced_expenses - $invoiced_diverse_income) > 0)
                                            $invoices_expense_array[$date]                          = number_format(($invoiced_expenses - $invoiced_diverse_income), 2, ".", "");
                                        else
                                            $invoices_expense_array[$date]                          = "-";

                                        if ($expenses > 0)
                                            $budget_assigned_array[$date]                           = number_format($expenses, 2, ".", "");
                                        else
                                            $budget_assigned_array[$date]                           = "-";

                                        if ($budget_expenses > 0)
                                            //$budget_expense_array[$date]                            = number_format(($budget_expenses + $invoiced_diverse_income), 2, ".", "");
                                            $budget_expense_array[$date]                            = number_format($budget_expenses, 2, ".", "");
                                        else
                                            $budget_expense_array[$date]                            = "-";

                                        $consulting_array[$date]                                    = number_format($balance1, 2, ".", "");
                                        $expenses_array[$date]                                      = number_format($balance2, 2, ".", "");
                                        $combination_array[$date]                                   = number_format($balance3, 2, ".", "");

                                        $plus++;
                                        $date                                                                = getDates($date_from, $plus);
                                    }
                                    
                                    $graph                                                          = createXMLString($date_from, $date_to, $consulting_array, $expenses_array, $combination_array);
                                    $displayString                                                  = renderChart("fusion/MSLine.swf", "", $graph, "multigraph1", 800, 400, false, false);
                                    echo "$displayString";
                                ?>
                            </td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td colspan="100%" class="centerdata">
                                <br/>
                                <?php
                                    $displayString = "";
                                    $table = "TimeSheet";
                                    $where = "";                // AND status = '2'
                                    $projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p 
                                                                INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) 
                                                                WHERE cot.company_id = '".$_SESSION["company_id"]."' 
                                                                AND p.completed = '0' AND p.status = 'active'
                                                                AND p.deleted = '0' AND p.completed IN (0,1) ORDER BY UPPER(p.name)");
                                    function createPositiveXMLString($projects,$table,$where) {
                                        //  Create XML String
                                        $xmlString = "";
                                        //  Calculate Values
                                        if (is_array($projects)) {
                                            $xmlString = "<chart caption='Positive Work In Process' palette='4' decimals='2' enableSmartLabels='1' showBorder='1' ".fusionChart().">";
                                            foreach ($projects as $project) {
                                                $project_name = removeSC($project[1]);
                                                $booked = q("SELECT SUM(time_spent * rate) FROM $table WHERE project_id = '".$project[0]."' AND company_id = '".$_SESSION["company_id"]."' $where");
                                                $invoiced = q("SELECT SUM(consulting + (consulting * (vat / 100))) FROM LoadInvoices WHERE project_id = '".$project[0]."' AND company_id = '".$_SESSION["company_id"]."'");
                                                $wip = $booked - $invoiced;

                                                if ($wip >= 0)
                                                    $xmlString .= "<set label='".$project_name."' value='$wip'/>";
                                            }
                                            $xmlString .= "</chart>";
                                        }
                                        return $xmlString;
                                    }
                                    $graph = createPositiveXMLString($projects,$table,$where);

                                    if ($graph != "")   {
                                        $displayString .= renderChart("fusion/Pie3D.swf", "", $graph, "multigraph2", 800, 400, false, false);
                                        echo $displayString;
                                    }
                                ?>
                            </td><br/><br/>
                        </tr>
                    </table>
    
                </form>
            </td>
        </tr>
    </table>
<?php
    }
    //  Print Footer
    print_footer();
?>
