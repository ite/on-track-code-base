<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("fusion/FusionCharts.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    if (!hasAccess("REP_BUDGET_PERIOD"))
        header("Location: noaccess.php");

    //  Set Report Generated Status
    $generated = "0";
    $plannedTot = 0;
    $actualTot = 0;
    $invoiceTot = 0;
    $invoiceDueTot = 0;

    function createDataSet($date_from, $date_to, $total, $invoiceDate) {
        global $plannedTot;
        global $actualTot;
        global $invoiceTot;
        global $invoiceDueTot;

        $dataSetString = "";

        //  Planned Budget
        $dataSetString	.= "<dataset seriesName='Planned Running Cost'>";

        for ($i = 0; $i < $total; $i++) {
            $date = getNextMonthStart($date_from, $i);
            $year = substr($date, 0, 4);
            $month = substr($date, 5, 2);
            $planned = q("SELECT SUM(planned) FROM Budget WHERE year = '".$year."' AND month = '".$month."' AND company_id = '".$_SESSION["company_id"]."'");

            if ($planned == "")
                $planned = 0;

            $plannedTot += $planned;

            $dataSetString .= "<set value='".number_format($planned, 2, ".", "")."'/>";
        }

        $dataSetString .= "</dataset>";

        //  Actual Budget
        $dataSetString	.= "<dataset seriesName='Actual Running Cost'>";

        for ($i = 0; $i < $total; $i++) {
            $date = getNextMonthStart($date_from, $i);
            $year = substr($date, 0, 4);
            $month = substr($date, 5, 2);
            $actual = q("SELECT SUM(amount) FROM Budget WHERE year = '".$year."' AND month = '".$month."' AND company_id = '".$_SESSION["company_id"]."'");

            if ($actual == "")
                $actual = 0;

            $actualTot += $actual;

            $dataSetString .= "<set value='".number_format($actual, 2, ".", "")."'/>";
        }

        $dataSetString .= "</dataset>";

        //  Invoiced
        $dataSetString	.= "<dataset seriesName='Invoiced This Month (incl. VAT)'>";

        for ($i = 0; $i < $total; $i++) {
            $consulting = q("SELECT SUM(li.consulting + (li.consulting * (li.vat / 100))) FROM (LoadInvoices AS li) ".
                            "WHERE li.create_date >= '".getNextMonthStart($date_from, $i)."' AND li.create_date < '".getNextMonthStart($date_from,($i+1))."' ".
                            "AND li.company_id = '".$_SESSION["company_id"]."'");
            $diverse_income = q("SELECT SUM(li.diverse_income + (li.diverse_income * (li.vat / 100))) ".
                                "FROM (LoadInvoices AS li) ".
                                "WHERE li.create_date >= '".getNextMonthStart($date_from, $i)."' AND li.create_date < '".getNextMonthStart($date_from,($i+1))."' ".
                                "AND li.company_id = '".$_SESSION["company_id"]."'");
            $invoiced = $consulting + $diverse_income;

            if ($invoiced == "")
                $invoiced = 0;

            $invoiceTot += $invoiced;

            $dataSetString .= "<set value='".number_format($invoiced, 2, ".", "")."'/>";
        }

        $dataSetString .= "</dataset>";

        //  Invoice Due Current Month
        $dataSetString	.= "<dataset seriesName='Invoice(s) Due This Month (incl. VAT)'>";

        for ($i = 0; $i < $total; $i++) {
            $consulting = q("SELECT SUM(li.consulting + (li.consulting * (li.vat / 100))) FROM (LoadInvoices AS li) ".
                            "WHERE li.due_date >= '".getNextMonthStart($date_from, $i)."' AND li.due_date < '".getNextMonthStart($date_from,($i+1))."' ".
                            "AND li.company_id = '".$_SESSION["company_id"]."'");
            $diverse_income = q("SELECT SUM(li.diverse_income + (li.diverse_income * (li.vat / 100))) ".
                                "FROM (LoadInvoices AS li) ".
                                "WHERE li.due_date >= '".getNextMonthStart($date_from, $i)."' AND li.due_date < '".getNextMonthStart($date_from,($i+1))."' ".
                                "AND li.company_id = '".$_SESSION["company_id"]."'");
            $invoiced = $consulting + $diverse_income;

            if ($invoiced == "")
                $invoiced = 0;

			$invoiceDueTot += $invoiced;

            $dataSetString .= "<set value='".number_format($invoiced, 2, ".", "")."'/>";
        }

        $dataSetString .= "</dataset>";

        return $dataSetString;
    }

    function createXMLString($date_from, $date_to, $total, $invoiceDate) {
        $xmlString = "";

        $xmlString .= "<chart caption='Running Cost vs Invoiced (Consulting %2B Diverse Income) Report' shownames='1' showvalues='0' numberPrefix='".$_SESSION["currency"]."' showSum='1' decimals='2' ".
                            "overlapColumns='0' formatNumberScale='0' formatNumber='0' labelDisplay='rotate' ".fusionChart().">";
        //  Create Categories
        $xmlString .= "<categories>";

        for ($i = 0; $i < $total; $i++)	{
            $xmlString .= "<category label='".getNextMonth($date_from, $i)."' />";
        }

        $xmlString .= "</categories>";

        //  Create Dataset - Per Employee
        $xmlString .= createDataSet($date_from, $date_to, $total, $invoiceDate);

        $xmlString .= "</chart>";

        return $xmlString;
    }

    //  Create Display Information Function
    if (isset($_POST["save"]) && $_POST["save"] === "1") {
        $errorMessage = "";

        $date_from = addslashes(strip_tags($_POST["date_from"]));
        $date_to = addslashes(strip_tags($_POST["date_to"]));
        $invoiceDate = addslashes(strip_tags($_POST["invoiceDate"]));

        $invoiceDate = ($invoiceDate == "0") ? "create_date" : "due_date";

        $displayString = "";

        $date_from = getMonthStart($date_from);
        $date_to = getMonthEnd($date_to);

        $total = getMonthPeriod($date_from, $date_to);

        $graph = createXMLString($date_from, $date_to, $total, $invoiceDate);
        $displayString = renderChart("fusion/MSLine.swf", "", $graph, "multigraph", 800, 400, false, false);

        global $plannedTot;
        global $actualTot;
        global $invoiceTot;
        global $invoiceDueTot;

        $plannedAvg = $plannedTot / $total;
        $actualAvg = $actualTot / $total;
        $invoiceAvg = $invoiceTot / $total;
        $invoiceDueAvg = $invoiceDueTot / $total;

        $tableString = "<tr><td align='center' colspan='".($total + 1)."'>".
                            "<table class='on-table-center on-table'>".
                                "<tr><th colspan='100%' class='centerdata'>Averages for Selected Period</th></tr>".
                                "<tr><td></td><td class='centerdata'>Monthly Average <i>(".$_SESSION["currency"].")</i></td></tr>".
                                "<tr><td>Planned Running Cost</td><td class='rightdata'>".number_format($plannedAvg, 2, ".", "")."</td></tr>".
                                "<tr><td>Actual Running Cost</td><td class='rightdata'>".number_format($actualAvg, 2, ".", "")."</td></tr>".
                                "<tr><td>Invoiced <i>(incl. VAT)</i></td><td class='rightdata'>".number_format($invoiceAvg, 2, ".", "")."</td></tr>".
                                "<tr><td>Expected <i>(incl. VAT)</i></td><td class='rightdata'>".number_format($invoiceDueAvg, 2, ".", "")."</td></tr>".
                                "<tfoot><tr><td colspan='100%'></td></tr></tfoot>".
                            "</table>".
                        "</td></tr>";

        if ($displayString != "") {
            $displayString = "<tr><td class='on-table-clear' colspan='".($total + 1)."'><a>Running Cost vs Invoiced (Consulting + Diverse Income) Report</a>".
                                    "</td></tr><tr><td colspan='".($total + 1)."'><br/>".$displayString."<br/></td></tr>".$tableString;

            $generated = "1";
        }
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("5", "reports");

    if ($errorMessage != "")
    {
        echo "<p align='center' style='padding:0px;'><strong><font color='#999999'>$errorMessage</font></strong></p>";
        echo "<br/>";
    }
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    jQuery(function(){
	jQuery('#date_from').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_from').val(),
		current: jQuery('#date_from').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_from').val()))
			_date = new Date();
			else _date = jQuery('#date_from').val();
			jQuery('#date_from').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_from').val(formated);
			jQuery('#date_from').DatePickerHide();
		}
	});
    })

    jQuery(function(){
	jQuery('#date_to').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_to').val(),
		current: jQuery('#date_to').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_to').val()))
			_date = new Date();
			else _date = jQuery('#date_to').val();
			jQuery('#date_to').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_to').val(formated);
			jQuery('#date_to').DatePickerHide();
		}
	});
    })

    function check()
    {
        var valid                                                       = 1;

        //  Check That Date From Is Entered
        if (document.forms["report_time_comparison"].date_from.value == "")
        {
            ShowLayer("dateFrom", "block");
            valid                                                       = 0;
        }
        //  Check That Entered Date From Is Valid
        else
        {
            ShowLayer("dateFrom", "none");

            if (!validation("date", document.forms["report_time_comparison"].date_from.value))
            {
                ShowLayer("dateFromDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("dateFromDiv", "none");
        }

        //  Check That Date To Is Entered
        if (document.forms["report_time_comparison"].date_to.value == "")
        {
            ShowLayer("dateTo", "block");
            valid                                                       = 0;
        }
        //  Check That Entered Date To Is Valid
        else
        {
            ShowLayer("dateTo", "none");

            if (!validation("date", document.forms["report_time_comparison"].date_to.value))
            {
                ShowLayer("dateToDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("dateToDiv", "none");
        }

        if (valid == 1)
        {
            document.forms["report_time_comparison"].save.value         = 1;
            document.forms["report_time_comparison"].submit();
        }
    }
</script>
<?php
    $projectTypes = q("SELECT id, type FROM ProjectTypes WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY type");
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="report_time_comparison">
                    <div id="" style="<?php if ($generated == "1") echo "display: none;"; else echo "display: block"; ?>">
                        <table width="100%">
                            <tr>
                                <td class="centerdata">
                                    <h6>Running Cost vs Invoiced Report</h6>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Date From:
                                </td>
                                <td width="50%">
                                    <input class="on-field-date" id="date_from" name="date_from" type="text" style="text-align:right;" value="<?php
                                        if ($_POST["date_from"] != "") echo "".$_POST["date_from"]; else echo "".getMonthStart($today); ?>">
                                    <div id="dateFrom" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                    <div id="dateFromDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Date To:
                                </td>
                                <td width="50%">
                                    <input class="on-field-date" id="date_to" name="date_to" type="text" style="text-align:right;" value="<?php
                                        if ($_POST["date_to"] != "") echo "".$_POST["date_to"]; else echo "".getMonthEnd($today); ?>">
                                    <div id="dateTo" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                    <div id="dateToDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Invoice Date:
                                </td>
                                <td width="50%">
                                    <input method="post" name="invoiceDate" type="radio" value="0" checked><a>Created Date</a><br/>
                                    <input method="post" name="invoiceDate" type="radio" value="1"><a>Due Date</a><br/>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <input name="btnGenerate" onClick="check();" type="button" value="Generate Report">
                        <input method="post" name="save" type="hidden" value="0" />
                    </div>
                </form>
                <div id="report" style="<?php if ($generated == "1") echo "display: block;"; else echo "display: none"; ?>">
                    <?php
                        echo "<table class='on-table-center on-table'>";
                            echo "".$displayString;
                            echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        echo "</table>";
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
