<?php
    include("_db.php");
    include("_dates.php");

    function budgetSpawn($company_id = "", $datePassed = "") {
        if ($company_id != "")  $companies = q("SELECT id, name FROM Company WHERE id = $company_id ORDER BY id");
        else                    $companies = q("SELECT id, name FROM Company ORDER BY id");

        if (is_array($companies)) {
            foreach ($companies as $c) {
                if ($datePassed != "")  $date = $datePassed;
                else                    $date = date("Y-m-d");

                for ($i = 0; $i < 3; $i++) {
                    $year = substr($date, 0, strpos($date, "-"));
                    $month = substr($date, (strpos($date, "-") + 1), 2);
                    $prevDate = getPreviousMonth($year, $month);

                    $exists = q("SELECT * FROM Budget WHERE year = '".$year."' AND month = '".$month."' AND company_id = '".$c[0]."'");

                    if (!is_array($exists)) {
                        $categories = q("SELECT DISTINCT(c.id), c.name FROM (Category AS c INNER JOIN Elements AS e ON c.id = e.parent_id) WHERE c.company_id = '".$c[0]."' AND e.status = '1'");

                        if (is_array($categories)) {
                            foreach ($categories as $category) {
                                $elements = q("SELECT id, name FROM Elements WHERE parent_id = '".$category[0]."' AND status = '1'");

                                if (is_array($elements)) {
                                    foreach ($elements as $element) {
                                        $testYear = substr($prevDate, 0, strpos($prevDate, "-"));
                                        $testMonth = substr($prevDate, (strpos($prevDate, "-") + 1), 2);

                                        $info = q("SELECT planned, amount, due_date FROM Budget WHERE year = '$testYear' AND month = '$testMonth' AND element_id = '".$element[0]."'");

                                        if (is_array($info)) {
                                            if ($c[0] != "1" && $c[0] != "52")     {
                                                $planned = $info[0][0];
                                                $amount = $info[0][0];          //  @ Spawn, Actual = Planned
                                                $due_date = ($info[0][2] != "") ? getDates($info[0][2], 30) : "";
                                            } else      {
                                                $planned = $info[0][1];         //  @ Spawn, Planned = Actual
                                                $amount = "0.00";
                                                $due_date = ($info[0][2] != "") ? date("Y-m-d", strtotime("+1 month", strtotime($info[0][2]))) : "";
                                            }

                                            $insert = q("INSERT INTO Budget (year, month, element_id, planned, amount, due_date, company_id) ".
                                                        "VALUES ('$year', '$month', '".$element[0]."', '".$planned."', '".$amount."', '$due_date', '".$c[0]."')");
                                        } else  {
                                            //   Budget doesn't exist yet, create one
                                            $planned = "0.00";
                                            $amount = "0.00";
                                            $due_date = ""; 

                                            $insert = q("INSERT INTO Budget (year, month, element_id, planned, amount, due_date, company_id) ".
                                                        "VALUES ('$year', '$month', '".$element[0]."', '".$planned."', '".$amount."', '$due_date', '".$c[0]."')");
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $date = getNextMonthStart($date, 1);
                }
            }
        }

        return;
    }

    $scriptStart = my_microtime();
?>
<html>
    <head>
        <title>
            On-Track - Budget Spawning Script...
        </title>
    </head>
    <body>
        <h1 align="center">
            Script - Budget Spawning Script...
        </h1>
        <?php
            budgetSpawn();

            echo "Total Time  [".number_format(round(my_microtime()-$scriptStart,3),3)."s]<br/>";

            echo "<p align='center'>Budget spawning script done executing</p>";
        ?>
        <form action="index.php" method="post">
            <center><input type="submit" value="Return"/></center>
        </form>
    </body>
</html>
