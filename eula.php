<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("_functions.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    //  If btnAgree Was Pressed
    if (isset($_POST["btnAgree"]))
    {
        $insert                                                         = q("INSERT INTO EULA (user_id) VALUES ('".$_SESSION["user_id"]."')");

        $_SESSION["eula_agree"]                                         = true;

        header("Location: home.php");
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0", "eula");
?>
    <table width="100%">
        <tr>
            <td class="centerdata" valign="top">
                <form action="" method="post" name="eula">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>END-USER LICENSE AGREEMENT FOR ON-TRACK</h6>
                            </td>
                        </tr>
                    </table>
                    <p align="justify" style="padding:4px 4px 4px 4px;margin:4px 4px 4px 4px;font-size:12px;font-family:Courier New;">
                        IMPORTANT PLEASE READ THE TERMS AND CONDITIONS OF THIS LICENSE AGREEMENT CAREFULLY BEFORE CONTINUING WITH USAGE OF THIS PROGRAM.<br/><br/>
                        <i>integrITy engineering</i>'s (ITE) End-User License Agreement ("EULA") is a legal agreement between you (either an individual or a single 
                        entity) and ITE for the software product(s) identified above which may include associated software components, media, printed materials, and 
                        "online" or electronic documentation. By installing, copying, or otherwise using the SOFTWARE PRODUCT, you agree to be bound by the terms of this 
                        EULA. This license agreement represents the entire agreement concerning the program between you and ITE, (referred to as "licenser"), and it 
                        supersedes any prior proposal, representation, or understanding between the parties. If you do not agree to the terms of this EULA, do not use 
                        the SOFTWARE PRODUCT.<br/><br/>
                        The SOFTWARE PRODUCT is protected by copyright laws and international copyright treaties, as well as other intellectual property laws and 
                        treaties. The SOFTWARE PRODUCT is licensed, not sold.<br/>
                        <p align="justify" style="padding:4px 4px 4px 4px;margin:4px 4px 4px 4px;">
                            <a style="font-size:12px;font-family:Courier New;">1. GRANT OF LICENSE.</a>
                            <p align="justify" style="padding:4px 4px 4px 4px;margin:4px 4px 4px 4px;font-size:12px;font-family:Courier New;">
                                The SOFTWARE PRODUCT is licensed as follows:<br/>
                                <b style="font-size:12px;font-family:Courier New;">(a) Use.</b><br/> 
                                    ITE grants you the right to use the SOFTWARE PRODUCT and all functionality that has been granted via the login issued to the user.
                                    <br/> 
                            </p>
                        </p>
                        <p align="justify" style="padding:4px 4px 4px 4px;margin:4px 4px 4px 4px;">
                            <a style="font-size:12px;font-family:Courier New;">2. DESCRIPTION OF OTHER RIGHTS AND LIMITATIONS.</a>
                            <p align="justify" style="padding:4px 4px 4px 4px;margin:4px 4px 4px 4px;font-size:12px;font-family:Courier New;">
                                <b style="font-size:12px;font-family:Courier New;">(a) Prohibition on Reverse Engineering, Decompilation, and Disassembly.</b><br/> 
                                    You may not reverse engineer, decompile, or disassemble the SOFTWARE PRODUCT, except and only to the extent that such activity is 
                                    expressly permitted by applicable law notwithstanding this limitation.<br/> 
                                <b style="font-size:12px;font-family:Courier New;">(b) Rental.</b><br/> 
                                    You may not rent, lease, or lend the SOFTWARE PRODUCT.<br/> 
                                <b style="font-size:12px;font-family:Courier New;">(c) Support and Maintenance Services.</b><br/> 
                                    The monthly fee for the SOFTWARE PRODUCT does not include any support or maintenance. Support and maintenance will be given on a 
                                    time cost basis or a custom SLA can be negotiated with ITE. Any supplemental software code provided to you as part of the Support 
                                    Services shall be considered part of the SOFTWARE PRODUCT and subject to the terms and conditions of this EULA.<br/> 
                                <b style="font-size:12px;font-family:Courier New;">(d) Compliance with Applicable Laws.</b><br/> 
                                    You must comply with all applicable laws regarding use of the SOFTWARE PRODUCT.<br/> 
                            </p>
                        </p>
                        <p align="justify" style="padding:4px 4px 4px 4px;margin:4px 4px 4px 4px;">
                            <a style="font-size:12px;font-family:Courier New;">3. TERMINATION.</a>
                            <p align="justify" style="padding:4px 4px 4px 4px;margin:4px 4px 4px 4px;font-size:12px;font-family:Courier New;">
                                Without prejudice to any other rights, ITE may terminate this EULA if you fail to comply with the terms and conditions of this EULA.<br/> 
                            </p>
                        </p>
                        <p align="justify" style="padding:4px 4px 4px 4px;margin:4px 4px 4px 4px;">
                            <a style="font-size:12px;font-family:Courier New;">4. COPYRIGHT.</a>
                            <p align="justify" style="padding:4px 4px 4px 4px;margin:4px 4px 4px 4px;font-size:12px;font-family:Courier New;">
                                All title, including but not limited to copyrights, in and to the SOFTWARE PRODUCT and any copies thereof are owned by ITE. All title 
                                and intellectual property rights in and to the content which may be accessed through use of the SOFTWARE PRODUCT is the property of the 
                                respective content owner and may be protected by applicable copyright or other intellectual property laws and treaties. This EULA grants 
                                you no rights to use such content. All rights not expressly granted are reserved by ITE.<br/> 
                            </p>
                        </p>
                        <p align="justify" style="padding:4px 4px 4px 4px;margin:4px 4px 4px 4px;">
                            <a style="font-size:12px;font-family:Courier New;">5. NO WARRANTIES.</a>
                            <p align="justify" style="padding:4px 4px 4px 4px;margin:4px 4px 4px 4px;font-size:12px;font-family:Courier New;">
                                ITE expressly disclaims any warranty for the SOFTWARE PRODUCT. The SOFTWARE PRODUCT is provided "as is" without any express or implied 
                                warranty of any kind, including but not limited to any warranties of merchantability, noninfringement , or fitness of a particular 
                                purpose. ITE does not warrant or assume responsibility for the accuracy or completeness of any information, text, graphics, links or 
                                other items contained within the SOFTWARE PRODUCT. ITE makes no warranties respecting any harm that may be caused by the transmission 
                                of a computer virus, worm, time bomb, logic bomb, or other such computer program. ITE further expressly disclaims any warranty or 
                                representation to Authorized Users or to any third party.<br/> 
                            </p>
                        </p>
                        <p align="justify" style="padding:4px 4px 4px 4px;margin:4px 4px 4px 4px;">
                            <a style="font-size:12px;font-family:Courier New;">6. LIMITATION OF LIABILITY.</a>
                            <p align="justify" style="padding:4px 4px 4px 4px;margin:4px 4px 4px 4px;font-size:12px;font-family:Courier New;">
                                In no event shall ITE be liable for any damages (including, without limitation, lost profits, business interruption, or lost information) 
                                rising out of  'Authorized Users' use of or inability to use the SOFTWARE PRODUCT, even if ITE has been advised of the possibility of 
                                such damages. In no event will ITE be liable for loss of data or for indirect, special, incidental, consequential (including lost 
                                profit), or other damages based in contract, tort or otherwise. ITE shall have no liability with respect to the content of the SOFTWARE 
                                PRODUCT or any part thereof, including but not limited to errors or omissions contained therein, libel, infringements of rights of 
                                publicity, privacy, trademark rights, business interruption, personal injury, loss of privacy, moral rights or the disclosure of 
                                confidential information.<br/> 
                            </p>
                        </p>
                    </p>
                    <?php
                        if (!$_SESSION["eula_agree"] === true)
                            echo "<input name='btnAgree' tabindex='1' type='submit' value='I Agree To The Terms'>";
                    ?>
                    <br/><br/>
                </form>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>