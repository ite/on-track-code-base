<?php
    include("_db.php");
    include("_dates.php");
?>
<html>
    <head>
        <title>
            On-Track - Recovery Script...
        </title>
    </head>
    <body>
        <h1 align="center">
            Install
        </h1>
        <?php
            $date = date("Y-m-d H:i:s");
            $user = "admin";

            $companyID = q("SELECT id FROM Company WHERE name = 'DG Consult'");

            $projects = q("SELECT id, name FROM Project WHERE company_id = '".$companyID."'");
            $employees = q("SELECT e.id, e.email FROM (Employee as e INNER JOIN Company_Users as cu ON e.id = cu.user_id) ".
                            "WHERE cu.company_id = '".$companyID."' AND e.email != 'admin' ORDER BY e.id");

            echo "Projects      [".count($projects)."]<br/>";
            echo "Employees     [".count($employees)."]<br/>";

            $inserts = 0;

            $activityTypeID = q("SELECT id FROM ActivityTypes WHERE type = 'Engineering' AND company_id = '".$companyID."'");
            $activityID = q("SELECT id FROM Activities WHERE name = 'Normal time' AND parent_id = '".$activityTypeID."'");

            if (is_array($employees))    {
                foreach ($employees as $e)       {
                    if (!exist("user_defaults", "companyID = '".$companyID."' AND userID = '".$e[0]."'"))
                        $update = q("INSERT INTO user_defaults (companyID,userID,activityType,activity) VALUES ('".$companyID."','".$e[0]."','".$activityTypeID."','".$activityID."')");
                    else
                        $update = q("UPDATE user_defaults SET activityType = '".$activityTypeID."', activity = '".$activityID."' WHERE companyID = '".$companyID."' AND userID = '".$e[0]."'");
                }
            }

            if (is_array($projects))    {
                foreach ($projects as $p)       {
                    if (is_array($employees))    {
                        foreach ($employees as $e)       {
                            $manager = 0;

                            if ($e[1] == "johandup@dgconsult.co.za" ||
                                $e[1] == "don@dgconsult.co.za" ||
                                $e[1] == "andre@dgconsult.co.za" ||
                                $e[1] == "hendri@dgconsult.co.za" ||
                                $e[1] == "wilma@dgconsult.co.za" ||
                                $e[1] == "magdel@dgconsult.co.za")
                                $manager = 1;

                            if (!exist("Project_User", "project_id = '".$p[0]."' AND company_id = '".$companyID."' AND user_id = '".$e[0]."'"))       {
                                $insert = q("INSERT INTO Project_User (project_id,company_id,user_id,manager,user_budget,user_tariff,dateCreated,createdBy,dateUpdated,updatedBy) ".
                                            "VALUES ('".$p[0]."','".$companyID."','".$e[0]."','".$manager."','','1','".$date."','".$user."','".$date."','".$user."')");

                                $inserts++;
                            }
                        }
                    }
                }
            }

            echo "INSERTS       [".$inserts."]<br/>";

            echo "<p align='center'>Script completed successfully</p>";
        ?>
        <form action="index.php" method="post">
            <center><input type="submit" value="Return" /></center>
        </form>
    </body>
</html>
