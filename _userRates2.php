<?php
    include("_db.php");
    include("_dates.php");

    $scriptStart = my_microtime();
?>
<html>
    <head>
        <title>
            On-Track - Recovery Script...
        </title>
    </head>
    <body>
        <h1 align="center">
            Install - Rates/Company
        </h1>
        <?php
            //  Update Values/IDS
            $update = q("UPDATE Project_User AS pu ".
                            "SET pu.rate = (SELECT IF(pu.user_tariff = 1,e.tariff1,IF(pu.user_tariff = 2,e.tariff2,IF(pu.user_tariff = 3,e.tariff3,IF(pu.user_tariff = 4,e.tariff4,IF(pu.user_tariff = 5,e.tariff5,''))))) ".
                        "FROM Employee AS e WHERE e.id = pu.user_id)");

            $update = q("UPDATE Project_User AS pu SET pu.rateID = (SELECT ur.id FROM user_rates AS ur WHERE ur.companyid = pu.company_id AND ur.userid = pu.user_id AND ur.rate = pu.rate)");

            $update = q("UPDATE Project_User SET rateID = 0 WHERE rateID IS NULL");
            $update = q("UPDATE Project_User SET rate = '' WHERE rate IS NULL");

            $alter = q("ALTER TABLE Project_User CHANGE COLUMN rate rate VARCHAR(10) NOT NULL DEFAULT ''");
            $alter = q("ALTER TABLE Project_User CHANGE COLUMN rateID rateID SMALLINT UNSIGNED NOT NULL DEFAULT 0");

            echo "Total Time  [".number_format(round(my_microtime()-$scriptStart,3),3)."s]<br/>";

            echo "<p align='center'>Rates/Company script done executing</p>";
        ?>
        <form action="index.php" method="post">
            <center><input type="submit" value="Return"/></center>
        </form>
    </body>
</html>
