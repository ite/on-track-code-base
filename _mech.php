<?php
    include("_db.php");
    include("_dates.php");
?>
<html>
    <head>
        <title>
            On-Track - Recovery Script...
        </title>
    </head>
    <body>
        <h1 align="center">
            Install
        </h1>
        <?php
            $date = date("Y-m-d H:i:s");
            $user = "admin";

			$activityTypes[] = "Accounting";
			$activityTypes[] = "Accounting Admin";
			$activityTypes[] = "Admin";
			$activityTypes[] = "Documentation";
			$activityTypes[] = "Marketing";
			$activityTypes[] = "Office Management";
			$activityTypes[] = "Project Management";
			$activityTypes[] = "Risk Work";
			$activityTypes[] = "Staff Admin";
			$activityTypes[] = "Supervision";
			$activityTypes[] = "Travel";

			$activities["Accounting"][] = "Cash Book";
			$activities["Accounting"][] = "Creditor Payments";
			$activities["Accounting"][] = "Debtors";
			$activities["Accounting"][] = "IT troubleshooting";
			$activities["Accounting"][] = "Pastel";
			$activities["Accounting"][] = "Petty Cash";
			$activities["Accounting"][] = "Reconciliations";
			$activities["Accounting"][] = "Staff expenses";

			$activities["Accounting Admin"][] = "Administration";
			$activities["Accounting Admin"][] = "Correspondence";
			$activities["Accounting Admin"][] = "Filing";
			$activities["Accounting Admin"][] = "Management Reports";
			$activities["Accounting Admin"][] = "Meetings";
			$activities["Accounting Admin"][] = "Queries";
			$activities["Accounting Admin"][] = "Systems & Procedures";
			$activities["Accounting Admin"][] = "Tender Documents";

			$activities["Admin"][] = "Agenda";
			$activities["Admin"][] = "Booklets";
			$activities["Admin"][] = "Correspondence";
			$activities["Admin"][] = "Distribution of Correspondence";
			$activities["Admin"][] = "E-mail admin";
			$activities["Admin"][] = "Faxes";
			$activities["Admin"][] = "Filing";
			$activities["Admin"][] = "Letters";
			$activities["Admin"][] = "Meetings";
			$activities["Admin"][] = "Minutes";
			$activities["Admin"][] = "Notes";
			$activities["Admin"][] = "Payment Cert";
			$activities["Admin"][] = "Photocopies";
			$activities["Admin"][] = "Presentations";
			$activities["Admin"][] = "Reports";
			$activities["Admin"][] = "Site Instructions";
			$activities["Admin"][] = "Tenders";

			$activities["Documentation"][] = "Check Documentation";
			$activities["Documentation"][] = "Design Development";
			$activities["Documentation"][] = "Inhouse meetings";
			$activities["Documentation"][] = "Meeting - Local Authority";
			$activities["Documentation"][] = "Meeting - Sub-contractor";
			$activities["Documentation"][] = "Meeting with Contractor";
			$activities["Documentation"][] = "Municipal Drawings";
			$activities["Documentation"][] = "Municipal Submission";
			$activities["Documentation"][] = "Site Instructions";
			$activities["Documentation"][] = "Site Measurements";
			$activities["Documentation"][] = "Site Query";
			$activities["Documentation"][] = "Sketch Plans";
			$activities["Documentation"][] = "Telecommunication - Joe Craig";
			$activities["Documentation"][] = "Tender Documentation";
			$activities["Documentation"][] = "Working docs";

			$activities["Marketing"][] = "Adverts - Magazines";
			$activities["Marketing"][] = "Adverts - Newspapers";
			$activities["Marketing"][] = "Articles - Magazines";
			$activities["Marketing"][] = "Articles - Newspapers";
			$activities["Marketing"][] = "Company profiles";
			$activities["Marketing"][] = "General";
			$activities["Marketing"][] = "Promotions";

			$activities["Office Management"][] = "Administrative";
			$activities["Office Management"][] = "Annual leave";
			$activities["Office Management"][] = "Camera - managing";
			$activities["Office Management"][] = "Co-ordination of deliveries";
			$activities["Office Management"][] = "Company events";
			$activities["Office Management"][] = "Course / Seminar";
			$activities["Office Management"][] = "Doctors Appointm";
			$activities["Office Management"][] = "Family Resp Leave";
			$activities["Office Management"][] = "General office duties";
			$activities["Office Management"][] = "IT - General";
			$activities["Office Management"][] = "IT - On track admin";
			$activities["Office Management"][] = "IT - Rocketseed";
			$activities["Office Management"][] = "KSM - Personal";
			$activities["Office Management"][] = "Managing of vehicles";
			$activities["Office Management"][] = "Maternity Leave";
			$activities["Office Management"][] = "Meeting - Representatives";
			$activities["Office Management"][] = "Office Meeting";
			$activities["Office Management"][] = "Other - specify";
			$activities["Office Management"][] = "Other time off";
			$activities["Office Management"][] = "Public Holiday";
			$activities["Office Management"][] = "Sick Leave";
			$activities["Office Management"][] = "SM - Personal";
			$activities["Office Management"][] = "Study Leave";
			$activities["Office Management"][] = "Tea / Coffee break";
			$activities["Office Management"][] = "Training of staff";

			$activities["Project Management"][] = "Client Meetings";
			$activities["Project Management"][] = "Consultants Meetings";
			$activities["Project Management"][] = "Contract admin";
			$activities["Project Management"][] = "Contractor\'s Queries";
			$activities["Project Management"][] = "Design meetings";
			$activities["Project Management"][] = "End-user Meeting";
			$activities["Project Management"][] = "Extension of time";
			$activities["Project Management"][] = "Meeting - Contractor";
			$activities["Project Management"][] = "Meeting - Local Authority";
			$activities["Project Management"][] = "Meeting - Sub-contractor";
			$activities["Project Management"][] = "Other";
			$activities["Project Management"][] = "Prepare for Meeting";
			$activities["Project Management"][] = "Tender Inspections";

			$activities["Risk Work"][] = "Booklets";
			$activities["Risk Work"][] = "Client meeting";
			$activities["Risk Work"][] = "Company Profiles";
			$activities["Risk Work"][] = "Consultant meeting";
			$activities["Risk Work"][] = "Design";
			$activities["Risk Work"][] = "Documentation";
			$activities["Risk Work"][] = "Inn-house meetings";
			$activities["Risk Work"][] = "Tenders";

			$activities["Staff Admin"][] = "Contracts & Procedures";
			$activities["Staff Admin"][] = "Industrial Relations";
			$activities["Staff Admin"][] = "Leave admin";
			$activities["Staff Admin"][] = "Salary admin & queries";
			$activities["Staff Admin"][] = "Time Sheets";

			$activities["Supervision"][] = "Other";
			$activities["Supervision"][] = "Scheduled SI";
			$activities["Supervision"][] = "Scheduled SM";

			$activities["Travel"][] = "From Site";
			$activities["Travel"][] = "To and From Site";
			$activities["Travel"][] = "To Site";

            $companyID = q("SELECT id FROM Company WHERE name = 'Mech Architects'");

            echo $companyID." - Mech Architects<br/><br/>";

            $inserts = 0;

            if (is_array($activityTypes))    {
                foreach ($activityTypes as $at)       {
                    if (!exist("ActivityTypes", "company_id = '".$companyID."' AND type = '".$at."'"))       {
                        $insert = q("INSERT INTO ActivityTypes (company_id,type,dateCreated,createdBy,dateUpdated,updatedBy) ".
                                    "VALUES ('".$companyID."','".$at."','".$date."','".$user."','".$date."','".$user."')");

                        $inserts++;
                    }

                    if (is_array($activities[$at]))    {
                        $parentID = q("SELECT id FROM ActivityTypes WHERE company_id = '".$companyID."' AND type = '".$at."'");

                        foreach ($activities[$at] as $a)       {
                            if (!exist("Activities", "parent_id = '".$parentID."' AND name = '".$a."'"))       {
                                $insert = q("INSERT INTO Activities (parent_id,name,active,dateCreated,createdBy,dateUpdated,updatedBy) ".
                                            "VALUES ('".$parentID."','".$a."','1','".$date."','".$user."','".$date."','".$user."')");

                                $inserts++;
                            }
                        }
                    }
                }
            }

            echo "INSERTS [".$inserts."]<br/>";

            echo "<p align='center'>Script completed successfully</p>";
        ?>
        <form action="index.php" method="post">
            <center><input type="submit" value="Return" /></center>
        </form>
    </body>
</html>
