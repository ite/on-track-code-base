<?php
    include("_db.php");

    $projectID = addslashes(strip_tags($_GET["projectID"]));
    $companyID = addslashes(strip_tags($_GET["companyID"]));

    $projectName = q("SELECT name FROM Project WHERE id = '".$projectID."'");
    $managingCompany = q("SELECT company_id FROM Project WHERE id = '".$projectID."'");
    $managingCompany = q("SELECT name FROM Company WHERE id = '".$companyID."'");

    //$url = "http://ontrack.integrityengineering.co.za/scripts/";    //  Script Page URL
    //$url = "http://www.integrityengineering.co.za/ontrack_test/scripts/";    //  Script Page URL
    $url = "http://www.integrityengineering.co.za/ontest/scripts/";    //  Script Page URL
    //$url = "http://localhost/ontrack_v2/ontrack/scripts/";    //  Script Page URL

    $url1 = "sp_status.php?projectID=".$projectID."&companyID=".$companyID."&status=acceptBoth";    //  Accept Both Company && Team Participation
    $url2 = "sp_status.php?projectID=".$projectID."&companyID=".$companyID."&status=acceptCompany"; //  Accept Company Participation, Decline Team Participation
    $url3 = "sp_status.php?projectID=".$projectID."&companyID=".$companyID."&status=decline";       //  Decline Company Participation
?>
<html>
    <head>
        <title>
            On-Track - Shared Project Online Invitation
        </title>
        <link href="images/icons/bookings.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
        <link href="include/style.css" rel="stylesheet" type="text/css" />
        <link href="CSSFiles/styles.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <center>
            <div style="height:500px; background-image:url(CSSFiles/im/status.png)">
                <br>
                <img src ="CSSFiles/im/logo.png"/>
                <div style="height:40px">&nbsp;</div>
                <div style="text-align:justify;line-height:20px;width:800px;">
                    Please note that you've been selected to participate in a shared project on On-Track.<br/>
                    <br/>
                    <strong>Project Name:</strong> <?php echo $projectName; ?><br/>
                    <strong>Managing Company:</strong> <?php echo $managingCompany; ?><br/>
                    <br/>
                    You need to respond to this by clicking on one of the following links:<br/>
                    <br/>
                    To participate in the project and give the shared project manager the privilege of managing your project team, 
                    <a href="<?php echo $url.$url1; ?>" />Click Here</a>
                    <br/>
                    To participate in the project, but manage your own project team, 
                    <a href="<?php echo $url.$url2; ?>" />Click Here</a>
                    <br/>
                    If you choose not to participate in the project, 
                    <a href="<?php echo $url.$url3; ?>" />Click Here</a>
                    <br/><br/>
                    Regards,<br/>
                    Shared Project Manager(s)<br/>
                </div>
            </div>
        </center>
    </body>
</html>
