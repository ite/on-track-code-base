<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("_functions.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

//    if ($_SESSION["logged_in"] === true && !($_SESSION["usertype"] === "0"))
//        header("Location: home.php");

    if ($_SESSION["email"] != "admin")
        header("Location: noaccess.php");

    if (!hasAccess("COMP_MAN"))
        header("Location: noaccess.php");

    $display_link                                                       = "companies.php?alphabet";

    //  Set Alphabet Letter
    $alphabet                                                           = "";
    $alphabet                                                           = $_GET["alphabet"];

    //  Module Assignment Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")        {
        $errorMessage                                                   = "";

        $array                                                          = preg_split("/_/", $_POST["checkbox"]);

        if ($arr[0][0])
            $array                                                      = array_slice($array, 0, count($array) - 1);

        if ($array[0][0])       {
            foreach ($array as $a)
            {
                $company_id                                             = substr($a, 0, strpos($a, ":"));
                $module_id                                              = substr($a, (strpos($a, ":") + 1), strlen($a));

                $company                                                = q("SELECT name FROM Company WHERE id = '$company_id'");
                $module                                                 = q("SELECT name FROM Modules WHERE id = '$module_id'");

                //  Assign Company To Module
                if ($_POST[$a])
                {
                    $exist                                              = q("SELECT * FROM Module_Users WHERE company_id = '$company_id' AND module_id = '$module_id'");

                    if (!$exist[0][0])
                        $insert                                         = q("INSERT INTO Module_Users (company_id, module_id) VALUES ('$company_id', '$module_id')");

                    if ($insert)
                    {
                        $time                                           = date("H:i:s");

                        $logs                                           = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time) ".
                                                                            "VALUES ('$module assigned to $company', 'Insert', 'Module_Users', ".
                                                                            "'".$_SESSION["email"]."', '$today', '$time')");
                    }
                }
                //  Remove Company From Module
                else
                {
                    $exist                                              = q("SELECT * FROM Module_Users WHERE company_id = '$company_id' AND module_id = '$module_id'");

                    if ($exist[0][0])
                        $delete                                         = q("DELETE FROM Module_Users WHERE company_id = '$company_id' AND module_id = '$module_id'");

                    if ($delete)
                    {
                        $time                                           = date("H:i:s");

                        $logs                                           = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time) ".
                                                                            "VALUES ('$module removed from $company', 'Delete', 'Module_Users', ".
                                                                            "'".$_SESSION["email"]."', '$today', '$time')");
                    }
                }

                //  Rebuild company Menus
                $menus_ = getMenus_();
                $users = q("SELECT cu.company_id, cu.user_id, c.name, e.email FROM (Company_Users AS cu INNER JOIN Company AS c ON c.id = cu.company_id ".
                                "INNER JOIN Employee AS e ON e.id = cu.user_id) WHERE cu.company_id = '$company_id' ORDER BY c.name, e.email");
                $drop = q("DELETE FROM MenuIndex WHERE company_id = '$company_id'");
                foreach($users as $u)	{
                    $lookup = addMenuIndex($menus_, $u[0], $u[1], $u[3]);
                }
            }

            if ($insert || $delete)
                $errorMessage                                           = "Module Assignment(s) Successful";
        }
    }

    //  If Lock Employee Was Clicked
    if (isset($_POST["lockBox"]) && $_POST["lockBox"] === "1")
    {
	$id                                                             = $_POST["idBox"];
        $company_info                                                   = q("SELECT name FROM Company WHERE id = '$id'");
        $lock                                                           = q("UPDATE Company SET locked = '1' WHERE id = '$id'");

        if ($lock)
        {
            $time                                                       = date("H:i:s");

            $logs                                                       = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time) ".
                                                                            "VALUES ('".$company_info." locked', 'Update', 'Company', '".$_SESSION["email"]."', ".
                                                                            "'$today', '$time')");

            $errorMessage                                               = "Company Locked Successfully...";
        }
    }

    //  If Lock Employee Was Clicked
    if (isset($_POST["unlockBox"]) && $_POST["unlockBox"] === "1")
    {
	$id                                                             = $_POST["idBox"];
        $company_info                                                   = q("SELECT name FROM Company WHERE id = '$id'");
        $unlock                                                         = q("UPDATE Company SET locked = '0' WHERE id = '$id'");

        if ($unlock)
        {
            $time                                                       = date("H:i:s");

            $logs                                                       = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time) ".
                                                                            "VALUES ('".$company_info." unlocked', 'Update', 'Company', '".$_SESSION["email"]."', ".
                                                                            "'$today', '$time')");

            $errorMessage                                               = "Company Unlocked Successfully...";
        }
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0", "companies", "companies");

    if ($errorMessage != "")
        echo "<p class='centerdata' style='padding:0px;'><strong><font color='#999999'>$errorMessage</font></strong></p>";
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    function assign()
    {
        document.forms["companies"].save.value                          = 1;
        document.forms["companies"].submit();
    }

    function lock(id)
    {
        document.forms["companies"].idBox.value                         = id;
        document.forms["companies"].lockBox.value                       = 1;
        document.forms["companies"].submit();
    }

    function unlock(id)
    {
        document.forms["companies"].idBox.value                         = id;
        document.forms["companies"].unlockBox.value                     = 1;
        document.forms["companies"].submit();
    }
</script>
    <table width="100%">
        <tr>
            <td class="centerdata" valign="top">
                <form action="" method="post" name="companies">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>
                                    Company List
                                </h6>
                            </td>
                        </tr>
                    </table>
                    <?php
                        echo "<br/>";
                        echo "| <a href='$display_link='>View All Companies</a> |";
                        echo "<br/><br/>";
                        echo "<a href='$display_link=A'>A</a> | <a href='$display_link=B'>B</a> | ".
                            "<a href='$display_link=C'>C</a> | <a href='$display_link=D'>D</a> | ".
                            "<a href='$display_link=E'>E</a> | <a href='$display_link=F'>F</a> | ".
                            "<a href='$display_link=G'>G</a> | <a href='$display_link=H'>H</a> | ".
                            "<a href='$display_link=I'>I</a> | <a href='$display_link=J'>J</a> | ".
                            "<a href='$display_link=K'>K</a> | <a href='$display_link=L'>L</a> | ".
                            "<a href='$display_link=M'>M</a> | <a href='$display_link=N'>N</a> | ".
                            "<a href='$display_link=O'>O</a> | <a href='$display_link=P'>P</a> | ".
                            "<a href='$display_link=Q'>Q</a> | <a href='$display_link=R'>R</a> | ".
                            "<a href='$display_link=S'>S</a> | <a href='$display_link=T'>T</a> | ".
                            "<a href='$display_link=U'>U</a> | <a href='$display_link=V'>V</a> | ".
                            "<a href='$display_link=W'>W</a> | <a href='$display_link=X'>X</a> | ".
                            "<a href='$display_link=Y'>Y</a> | <a href='$display_link=Z'>Z</a>";

                            $locked = (isset($_POST["c_status"])) ? $_POST["c_status"] : "0";
                    ?>
                    <br/><br/>
                    <input name="btnAdd" onClick="location.href='company_add.php';" tabindex="1" type="button" value="Add New Company">
                    <br/>
                    <input name="btnAssign" onClick="assign();" type="button" value="Assign Module(s)">
                    <br/><br/>
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                Company Status:
                            </td>
                            <td width="50%">
                                <select class="on-field" id="c_status" name="c_status" method="post" onChange="submit();">
                                    <option value="all" <?php if ($locked == "all") echo "selected"; ?>>All Companies&nbsp;&nbsp;</option>
                                    <option value="0" <?php if ($locked == "0") echo "selected"; ?>>Unlocked&nbsp;&nbsp;</option>
                                    <option value="1" <?php if ($locked == "1") echo "selected"; ?>>Locked&nbsp;&nbsp;</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <table class="on-table-center on-table ">
                        <!--  Table Headings   -->
                        <tr>
                            <th>
                                    Company Name
                            </th>
                            <th>
                                    Lock/Unlock
                            </th>
                            <?php
                                //  nModules                            = Number of Modules
                                $nModules                               = q("SELECT COUNT(id) FROM Modules");
                                $modules                                = q("SELECT id, name FROM Modules ORDER BY name");

                                if ($nModules > 1)
                                {
                                    foreach ($modules as $module)
                                    {
                                        echo "<th>";
                                                $module_length          = strlen($module[1]);

                                                for ($i = 0; $i < $module_length; $i++)
                                                    echo substr($module[1], $i, 1)."<br/>";
                                        echo "</th>";
                                    }
                                }
                                else if ($nModules == 1)
                                {
                                    echo "<th>". $modules[0][1]. "</td>";
                                }
                            ?>
                        </tr>
                        <!--  Table Information   -->
                        <?php
                            $locked = ($locked == "all") ? "" : " AND locked = '".$locked."'";
                            $companies                                  = q("SELECT id, name, locked FROM Company WHERE name LIKE '$alphabet%' $locked ORDER BY name");

                            if (is_array($companies))        {
                                foreach ($companies as $company)
                                {
                                    echo "<tr>";
                                        echo "<td>";
                                            echo "<a href='company_edit.php?id=".$company[0]."'>".$company[1]."</a>";
                                        echo "</td>";
                                        //  Lock/Unlock Button
                                        echo "<td class='centerdata'>";
                                            if ($company[2] === "0")
                                                echo "<input name='btnLock' onClick=\"lock(".$company[0].");\" type='button' value='Lock Company   '>";
                                            else
                                                echo "<input name='btnUnlock' onClick=\"unlock(".$company[0].");\" type='button' value='Unlock Company'>";
                                        echo "</td>";
                                        //  Module Checkboxes
                                        if ($nModules > 1)
                                        {
                                            foreach ($modules as $module)
                                            {
                                                $checkbox               .= "".$company[0].":".$module[0]."_";
                                                $checked                = q("SELECT id FROM Module_Users WHERE company_id = '".$company[0]."' ".
                                                                            "AND module_id = '".$module[0]."'");

                                                echo "<td class='centerdata'>";
                                                    if ($checked)
                                                        echo "<input name='".$company[0].":".$module[0]."' type='checkbox' value='1' checked>";
                                                    else
                                                        echo "<input name='".$company[0].":".$module[0]."' type='checkbox' value='1'>";
                                                echo "</td>";
                                            }
                                        }
                                        else if ($nModules == 1)
                                        {
                                            $checkbox                   .= "".$company[0].":".$modules[0][0]."_";
                                            $checked                    = q("SELECT id FROM Module_Users WHERE company_id = '".$company[0]."' ".
                                                                            "AND module_id = '".$modules[0][0]."'");

                                            echo "<td class='centerdata'>";
                                                if ($checked)
                                                    echo "<input name='".$company[0].":".$modules[0][0]."' type='checkbox' value='1' checked>";
                                                else
                                                    echo "<input name='".$company[0].":".$modules[0][0]."' type='checkbox' value='1'>";
                                            echo "</td>";
                                        }
                                    echo "</tr>";
                                }
                            }
                            else
                            {
                                echo "<tr>";
                                    echo "<td colspan='100%' class='centerdata'>";
                                        if ($alphabet == "")
                                            echo "No companies available";
                                        else
                                            echo "No companies available under $alphabet";
                                    echo "</td>";
                                echo "</tr>";
                            }
                            echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        ?>
                    </table>
                    <br/>
                    &nbsp;<input name="btnAssign" onClick="assign();" type="button" value="Assign Module(s)">
                    <input method="post" name="save" type="hidden" value="0" />
                    <?php
                        echo "<input name='checkbox' type='hidden' value='".$checkbox."'>";
                    ?>
                    <br/>
                    <input name="btnAdd" onClick="location.href='company_add.php';" tabindex="1" type="button" value="Add New Company">
                    <input name="idBox" type="hidden" value="">
                    <input name="lockBox" type="hidden" value="">
                    <input name="unlockBox" type="hidden" value="">
                </form>
            </td>
        </tr>
        <tr>
            <td class='centerdata'>
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
