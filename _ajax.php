<?php
    session_start();

    include("_db.php");
    include("_dates.php");
    include("_functions.php");

    //////////////////////////////////////////////////
    //  GET SESSION VARIABLE VALUES HERE
    //////////////////////////////////////////////////
    $companyID = addslashes(strip_tags($_SESSION["company_id"]));
    $userID = addslashes(strip_tags($_SESSION["user_id"]));
    $userName = addslashes(strip_tags($_SESSION["email"]));
    //////////////////////////////////////////////////

    if (isset($data))   unset($data);

    $func = (isset($_POST["func"])) ? $_POST["func"] : $_GET["func"];

    switch ($func)	{
        //////////////////////////////////////////////////
        //  RUNNING DIRECT QUERIES
        case "query_direct":	{
            $query = $_POST["query"];

            //$query = preg_replace("/\\\/", "", $query);

            $data = q($query);

            echo json_encode($data);
            break;
        }
        //  RUNNING DIRECT QUERIES
        case "recordExists":	{
            $table = $_POST["table"];
            $where = $_POST["where"];

            $data = (exist($table, $where)) ? "1" : "0";

            echo json_encode($data);
            break;
        }
        //////////////////////////////////////////////////
        //  SETTING SESSION VARIABLES
        case "setSessionVariable":	{
            $sessionName = addslashes(strip_tags($_POST["sessionName"]));
            $sessionValue = addslashes(strip_tags($_POST["sessionValue"]));

            $sessionName = preg_split("/#/", $sessionName);
            $sessionValue = preg_split("/#/", $sessionValue);

            for ($i = 0; $i < count($sessionName); $i++)
                $_SESSION[$sessionName[$i]] = $sessionValue[$i];

            if (is_null($data))	$data = "";

            echo json_encode($data);
            break;
        }
        //////////////////////////////////////////////////
        //  AUTOCOMPLETE FUNCTIONS
        //////////////////////////////////////////////////
        //  GET COMPANIES
        case "get_companies":	{
            unset($data);

            $alpha = addslashes(strip_tags($_GET["q"]));
            $companies = addslashes(strip_tags($_SESSION["companies"]));

            $companies = preg_split("/_/", $companies);

            $data = q("SELECT id,name FROM Company WHERE name LIKE '%".$alpha."%' AND locked = '0' ORDER BY name");

            if (is_array($data))   {
                foreach ($data as $d)  {
                    if (!in_array($d[0], $companies))
                        echo "".$d[1]."|".$d[0]."\n";
                }
            }

            break;
        }
        //////////////////////////////////////////////////
        //  GET USERS
        case "get_users":	{
            unset($data);

            $alpha = addslashes(strip_tags($_GET["q"]));
            $companyID = addslashes(strip_tags($_SESSION["companyValue"]));
            $users = addslashes(strip_tags($_SESSION["usersList"]));

            $users = preg_replace("/_/", ",", $users);
            $users = substr($users, 0, -1);

            $subWhere = ($users != "") ? "AND e.id NOT IN (".$users.") " : "";

            $data = q("SELECT e.id,e.lstname,e.frstname,e.email FROM (Employee AS e INNER JOIN Company_Users AS cu ON e.id = cu.user_id) ".
                        "WHERE cu.company_id = '".$companyID."' AND e.email != 'admin' AND e.deleted = '0' AND (e.lstname LIKE '%".$alpha."%' OR e.frstname LIKE '%".$alpha."%' OR e.email LIKE '%".$alpha."%') ".
                        "$subWhere ORDER BY e.lstname,e.frstname,e.email");

            if (is_array($data))   {
                foreach ($data as $d)  {
                    echo "".$d[1].", ".$d[2]." [".$d[3]."]"."|".$d[0]."\n";
                }
            }

            break;
        }
        //////////////////////////////////////////////////
        //  GET COMPANIES
        case "get_vehicles":	{
            unset($data);

            $alpha = addslashes(strip_tags($_GET["q"]));
            $vehicles = addslashes(strip_tags($_SESSION["vehicles"]));

            $vehicles = preg_split("/_/", $vehicles);

            $data = q("SELECT id,type FROM Vehicle WHERE company_id = '".$companyID."' AND type LIKE '%".$alpha."%' AND deleted = '0' ORDER BY type");

            if (is_array($data))   {
                foreach ($data as $d)  {
                    if (!in_array($d[0], $vehicles))
                        echo "".$d[1]."|".$d[0]."\n";
                }
            }

            break;
        }
        //////////////////////////////////////////////////
        //  ADMIN FUNCTIONS
        //////////////////////////////////////////////////
        //  USER ROLE ASSIGNMENT
        case "get_role_assign": {
            unset($data);

            $userID = addslashes(strip_tags($_POST["userid"]));

            $data = q("SELECT id,companyid,userid,roleid FROM user_role WHERE userid = '".$userID."' AND companyID = '".$companyID."'");
            //$data = q("SELECT id,companyid,userid,roleid FROM user_role WHERE userid = '".$userID."' AND companyID = '1'");

            if (is_null($data))	$data = "";

            echo json_encode($data);
            break;
        }
        //////////////////////////////////////////////////
        //  ROLE ACTION ASSIGNMENT
        case "get_action_assign":	{
            unset($data);

            $roleID = addslashes(strip_tags($_POST["roleid"]));

            $data = q("SELECT id,companyid,roleid,actionid FROM role_action WHERE roleid = '".$roleID."' AND companyID = '".$companyID."'");
            //$data = q("SELECT id,companyid,roleid,actionid FROM role_action WHERE roleid = '".$roleID."' AND companyID = '1'");

            if (is_null($data))	$data = "";

            echo json_encode($data);
            break;
        }
        //////////////////////////////////////////////////
        //  VEHICLE ASSIGNMENT
        case "get_vehicle_assign":	{
            unset($data);

            $userID = addslashes(strip_tags($_POST["userid"]));

            $data = q("SELECT v.id, v.type FROM Vehicle AS v INNER JOIN vehicle_user AS vu ON vu.vehicle_id = v.id WHERE vu.user_id = '".$userID."' AND v.company_id = '".$companyID."' AND v.deleted = '0' ORDER BY v.type");

            if (is_null($data))	$data = "";

            echo json_encode($data);
            break;
        }
        //////////////////////////////////////////////////
        //  COMPANY LEVEL FUNCTIONS
        //////////////////////////////////////////////////
        //  COMPANY ASSIGNMENT
        case "get_company_assign":	{
            unset($data);

            $userID = addslashes(strip_tags($_POST["userid"]));

            $data = q("SELECT c.id, c.name FROM Company AS c INNER JOIN Company_Users AS cu ON cu.company_id = c.id WHERE cu.user_id = '".$userID."' ORDER BY c.name");

            if (is_null($data))	$data = "";

            echo json_encode($data);
            break;
        }
        //////////////////////////////////////////////////
        //  GET LIST OF PROJECTS
        case "get_projects":	{
            unset($data);

            $alpha = addslashes(strip_tags($_POST["alpha"]));
            $completed = addslashes(strip_tags($_POST["completed"]));
            $type = addslashes(strip_tags($_POST["type"]));
            $status = addslashes(strip_tags($_POST["status"]));

            if (strlen($alpha) > 1) {
                $where .= "AND p.name LIKE '%".$alpha."%' ";
                $descr = " with '$alpha' in name";
            }
            else if (strlen($alpha) == 1)    {
                $where .= "AND p.name LIKE '".$alpha."%' ";
                $descr = " under $alpha";
            }

            if ($type == "invoicable")
                $where .= "AND cot.total_budget > 0  ";
            else if ($type == "non_invoicable")
                $where .= "AND cot.total_budget = '' ";

            if ($status == "active")
                $where .= "AND p.status = '$status' ";
            else if ($status == "proposal")
                $where .= "AND p.status = '$status' ";

            $projects = q("SELECT DISTINCT(p.id), p.name, p.completed, p.p_type, cot.teamManage FROM (Project AS p INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) ".
                            "WHERE p.completed = '".$completed."' AND p.deleted = '0' AND cot.status = '1' AND cot.company_id = '".$companyID."' $where ORDER BY UPPER(p.name)");
            //$projects = q("SELECT DISTINCT(p.id), p.name, p.completed, p.p_type, cot.teamManage, p.client_id FROM (Project AS p INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) ".
            //                "WHERE p.completed = '".$completed."' AND p.deleted = '0' AND cot.status = '1' AND cot.company_id = '".$companyID."' $where ORDER BY UPPER(p.name)");

            if (!is_array($projects))
                $data = array(array("","No projects available".$descr, "0"));
            else        {
                $isManager = q("SELECT ur.id,r.id FROM user_role AS ur INNER JOIN roles AS r ON ur.roleid = r.id WHERE r.role = 'MAN' AND ur.userid = '$userID' AND ur.companyid = '$companyID' AND r.companyid = '".$companyID."'");
                $isManager = ($isManager[0][0] != 0) ? 1 : 0;

                foreach ($projects as $project)      {
                    if ($project[3] != "CP" && $userName != "admin" && !$isManager)    {
                        $isPM = q("SELECT manager FROM Project_User WHERE user_id = '".$userID."' AND project_id = '".$project[0]."' AND company_id = '".$companyID."'");

                        if ($project[4] == 2)   $isPM = 1;

                        if ($isPM == 1)         $data[] = $project;
                    }
                    else if ($userName != "admin" && !$isManager)	{
                        $isPM = q("SELECT manager FROM Project_User WHERE user_id = '".$userID."' AND project_id = '".$project[0]."' AND company_id = '".$companyID."'");

                        if ($isPM == 1)         $data[] = $project;
                    }
                    else
                        $data[] = $project;
                }
            }

            if (is_null($data))	$data = "";

            echo json_encode($data);
            break;
        }
        //////////////////////////////////////////////////
        //  GET LIST OF ACTIVITY TYPES & ACTIVITIES
        case "get_activityTypes":	{
            unset($data);

            $activityTypes = q("SELECT id,type FROM ActivityTypes WHERE company_id = '".$companyID."' AND active = '1' ORDER BY type");

            if (is_array($activityTypes))    {
                $r = 0;

                foreach ($activityTypes as $activityType) {
                    unset($activities);
                    unset($subInfo);

                    $c = 0;

                    $data[$r][$c++] = $activityType[0];
                    $data[$r][$c++] = $activityType[1];

                    $activities = q("SELECT id,name,active,dateCreated,createdBy,dateUpdated,updatedBy FROM Activities WHERE parent_id = '".$activityType[0]."' ORDER BY name");

                    if (is_array($activities))  {
                        $subInfo = array();

                        foreach ($activities as $activity)    {
                            $subInfo[] = $activity;
                        }
                    }

                    if (is_array($subInfo))
                        $data[$r][$c++] = $subInfo;
                    else
                        $data[$r][$c++] = array(array("-","-","-","-","-","-","-"));

                    $r++;
                }
            }
            else
                $data[] = array("-","-",array(array("-","-","-","-","-","-","-")));

            if (is_null($data))	$data = "";

            echo json_encode($data);
            break;
        }
        //////////////////////////////////////////////////
        //  GET LIST OF ACTIVITY TYPES & ACTIVITIES
        case "get_locations":	{
            unset($data);

            $data = q("SELECT id, description, latitude, longitude, createdBy, dateCreated, updatedBy, dateUpdated FROM locations WHERE company_id = '".$companyID."' ORDER BY description");

            if (is_null($data))	$data = "";

            echo json_encode($data);
            break;
        }
        //////////////////////////////////////////////////
        //  GET LIST OF PROJECT AREAS
        case "get_areas":	{
            unset($data);

            $pID = addslashes(strip_tags($_POST["pID"]));

            $p_Info = q("SELECT id,name FROM Project WHERE id = '".$pID."' ORDER BY name");

            if (is_array($p_Info))    {
                $r = 0;

                foreach ($p_Info as $p) {
                    $c = 0;

                    $data[$r][$c++] = $p[0];
                    $data[$r][$c++] = $p[1];

                    $p_areas = q("SELECT id,name,active,dateCreated,createdBy,dateUpdated,updatedBy FROM areas WHERE pID = '".$pID."' ORDER BY name");

                    if (is_array($p_areas))  {
                        $subInfo = array();

                        foreach ($p_areas as $p_area)    {
                            $subInfo[] = $p_area;
                        }
                    }

                    if (is_array($subInfo))
                        $data[$r][$c++] = $subInfo;
                    else
                        $data[$r][$c++] = array(array("-","-","-","-","-","-","-"));

                    $r++;
                }
            }

            if (is_null($data))	$data = "";

            echo json_encode($data);
            break;
        }
        //////////////////////////////////////////////////
        //  SHARED PROJECT LEVEL FUNCTIONS
        //////////////////////////////////////////////////
        //  GET LIST OF SHARED PROJECTS
        case "get_sp_projects":	{
            unset($data);

            $alpha = addslashes(strip_tags($_POST["alpha"]));
            $completed = addslashes(strip_tags($_POST["completed"]));

            $where = "";

            //if ($userName != "admin")   $where .= "AND pu.user_id = '".$userID."' AND pu.spm_manager = '1' ";

            if (strlen($alpha) > 1) {
                $where = "AND p.name LIKE '%".$alpha."%' ";
                $descr = " with '$alpha' in name";
            }
            else if (strlen($alpha) == 1)    {
                $where = "AND p.name LIKE '".$alpha."%' ";
                $descr = " under $alpha";
            }

            $data = q("SELECT DISTINCT(p.id),p.name,p.completed FROM (Project AS p INNER JOIN Project_User AS pu ON p.id = pu.project_id INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id)
                        WHERE p.p_type = 'SP' AND p.completed = '".$completed."' AND cot.company_id = '".$companyID."' AND pu.company_id  = '".$companyID."' $where ORDER BY p.name");

            if (!is_array($data))
                $data = array(array("","No projects available".$descr, "0"));

            if (is_null($data))	$data = "";

            echo json_encode($data);
            break;
        }
        //////////////////////////////////////////////////
        //  GET LIST OF ACTIVITY TYPES & ACTIVITIES
        case "get_sp_activityTypes":	{
            unset($data);

            $spID = addslashes(strip_tags($_POST["spID"]));

            $sp_activityTypes = q("SELECT id,type FROM ActivityTypes WHERE pID = '".$spID."' AND active = '1' ORDER BY type");

            if (is_array($sp_activityTypes))    {
                $r = 0;

                foreach ($sp_activityTypes as $sp_activityType) {
                    $c = 0;

                    $data[$r][$c++] = $sp_activityType[0];
                    $data[$r][$c++] = $sp_activityType[1];

                    $sp_activities = q("SELECT id,name,active,dateCreated,createdBy,dateUpdated,updatedBy FROM Activities WHERE parent_id = '".$sp_activityType[0]."' ORDER BY name");

                    if (is_array($sp_activities))  {
                        $subInfo = array();

                        foreach ($sp_activities as $sp_activity)    {
                            $subInfo[] = $sp_activity;
                        }
                    }

                    if (is_array($subInfo))
                        $data[$r][$c++] = $subInfo;
                    else
                        $data[$r][$c++] = array(array("-","-","-","-","-","-","-"));

                    $r++;
                }
            }
            else
                $data[] = array("-","-",array(array("-","-","-","-","-","-","-")));

            if (is_null($data))	$data = "";

            echo json_encode($data);
            break;
        }
        //////////////////////////////////////////////////
        //  GET LIST OF PROJECT AREAS
        case "get_sp_areas":	{
            unset($data);

            $spID = addslashes(strip_tags($_POST["spID"]));

            $sp_Info = q("SELECT id,name FROM Project WHERE id = '".$spID."' ORDER BY name");

            if (is_array($sp_Info))    {
                $r = 0;

                foreach ($sp_Info as $sp) {
                    $c = 0;

                    $data[$r][$c++] = $sp[0];
                    $data[$r][$c++] = $sp[1];

                    $sp_areas = q("SELECT id,name,active,dateCreated,createdBy,dateUpdated,updatedBy FROM areas WHERE pID = '".$spID."' ORDER BY name");

                    if (is_array($sp_areas))  {
                        $subInfo = array();

                        foreach ($sp_areas as $sp_area)    {
                            $subInfo[] = $sp_area;
                        }
                    }

                    if (is_array($subInfo))
                        $data[$r][$c++] = $subInfo;
                    else
                        $data[$r][$c++] = array(array("-","-","-","-","-","-","-"));

                    $r++;
                }
            }

            if (is_null($data))	$data = "";

            echo json_encode($data);
            break;
        }
        //////////////////////////////////////////////////
        //  RESEND PROJECT INVITE
        case "resendInvite":    { // spID, pName, pTeamID, teamCompanyID
            unset($data);

            $projectID = addslashes(strip_tags($_POST["projectID"]));
            $projectName = addslashes(strip_tags($_POST["projectName"]));
            $companyID = addslashes(strip_tags($_POST["companyID"]));

            $update = q("UPDATE companiesOnTeam SET locked = '0' WHERE project_id = '".$projectID."' AND company_id = '".$companyID."'");

            sp_Invite($projectID, $projectName, $companyID);

            if (is_null($data))	$data = "";

            echo json_encode($data);
            break;
        }
        //////////////////////////////////////////////////
        //  BOOKING FUNCTIONS
        //////////////////////////////////////////////////
        //  CHECK IF USER OVER BUDGET ON PROJECT
        case "checkTimeBudget":     {
            unset($data);

            $projectID = addslashes(strip_tags($_POST["projectID"]));

            $budget = q("SELECT user_budget FROM Project_User WHERE user_id = '".$userID."' AND project_id = '".$projectID."' AND company_id = '".$companyID."'");
            $rate = q("SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID."' ".
                        "AND pu.company_id = '".$companyID."' AND pu.project_id = '".$projectID."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID."'");
            //$tariff_id = q("SELECT user_tariff FROM Project_User WHERE user_id = '".$userID."' AND project_id = '".$projectID."' AND company_id = '".$companyID."'");
            //$rate = q("SELECT tariff".$tariff_id." FROM Employee WHERE id = '".$userID."'");

            if ($budget > 0 && $rate > 0)       {
                $available = $budget;

                $time = q("SELECT SUM(time_spent * rate) FROM TimeSheet WHERE user_id = '".$userID."' AND project_id = '".$projectID."' AND company_id = '".$companyID."'");
                //$expenses = q("SELECT SUM(expense) FROM ExpenseSheet WHERE user_id = '".$userID."' AND project_id = '".$projectID."' AND company_id = '".$companyID."'");

                $available -= ($time + $expenses);
            }
            else        {
                $available = "N/A";
            }

            if ($available == "N/A")
                $data = array($available,$available);
            else
                $data = array($available,"".number_format(($available / $rate), 2, ".", "")." Hours");
                //$data = array($available,"".number_format(($available / $rate), 2, ".", "")." Hours/".$_SESSION["currency"]." ".number_format($available, 2, ".", ""));

            if (is_null($data))	$data = "";

            echo json_encode($data);
            break;
        }
        //////////////////////////////////////////////////
        //  GET LIST OF PROJECT AREAS
        case "get_area_info":	{
            $projectID = addslashes(strip_tags($_POST["projectID"]));

            $data = q("SELECT id,name FROM areas WHERE pID = '".$projectID."' AND active = '1' ORDER BY name");

            if (is_null($data))	$data = "";

            echo json_encode($data);
            break;
        }
        //////////////////////////////////////////////////
        //  GET LIST OF ACTIVITY TYPES
        case "get_activity_types":	{
            unset($data);

            $projectType = addslashes(strip_tags($_POST["projectType"]));
            $projectID = addslashes(strip_tags($_POST["projectID"]));

            $data = ($projectType == "CP") ? q("SELECT id,type FROM ActivityTypes WHERE company_id = '".$companyID."' AND active = '1' ORDER BY type")
                                            :q("SELECT id,type FROM ActivityTypes WHERE pID = '".$projectID."' AND active = '1' ORDER BY type");

            if (is_null($data))	$data = "";

            echo json_encode($data);
            break;
        }
        //////////////////////////////////////////////////
        //  GET LIST OF ACTIVITIES
        case "get_activities":	{
            unset($data);

            $activityType = addslashes(strip_tags($_POST["activityType"]));

            $data = q("SELECT id,name FROM Activities WHERE parent_id = '".$activityType."' AND active = '1' ORDER BY name");

            if (is_null($data))	$data = "";

            echo json_encode($data);
            break;
        }
        //////////////////////////////////////////////////
        //  OTHER FUNCTIONS
        //////////////////////////////////////////////////
        //  GET HEADER INFO
        case "getHeaderInfo":	{
            unset($data);

            //$data[] = $_SESSION["email"];
            $data[] = q("SELECT CONCAT(frstname, ' ', lstname) FROM Employee WHERE id = '".$userID."' ");
            $comp = q("SELECT name FROM Company WHERE id = '".$companyID."'");

            if ($comp == "0")	$comp = "None Selected";

            $data[] = $comp;
//            $data[] = $_SESSION["project"];

            echo json_encode($data);
            break;
        }
        //////////////////////////////////////////////////
        //  GET PROFILE INFO
        case "getProfileDetails":       {
            unset($data);

            if (!exist("user_defaults", "companyID = '".$companyID."' AND userID = '".$userID."'"))
                $insert = q("INSERT INTO user_defaults (companyID,userID) VALUES ('".$companyID."','".$userID."')");

            $user_defaults = q("SELECT landingPage, activityType, activity, vehicle FROM user_defaults WHERE companyID = '".$companyID."' AND userID = '".$userID."'");
            $userDetails = q("SELECT frstname, lstname, email FROM Employee WHERE id = '".$userID."'");

            $data = array($user_defaults[0][0], $user_defaults[0][1], $user_defaults[0][2], $user_defaults[0][3], $userDetails[0][0], $userDetails[0][1], $userDetails[0][2]);

            if (is_null($data))	$data = "";

            echo json_encode($data);
            break;
        }
        //////////////////////////////////////////////////
        //  UPDATE PROFILE INFO
        case "updateDetails":       {
            unset($data);

            $landingPage = addslashes(strip_tags($_POST["landingPage"]));
            $activityType = addslashes(strip_tags($_POST["activityType"]));
            $activity = addslashes(strip_tags($_POST["activity"]));
            $vehicle = addslashes(strip_tags($_POST["vehicle"]));
            $frstname = addslashes(strip_tags($_POST["frstname"]));
            $lstname = addslashes(strip_tags($_POST["lstname"]));
            $email = addslashes(strip_tags($_POST["email"]));

            $oldLandingPage = q("SELECT landingPage FROM user_defaults WHERE companyID = '".$companyID."' AND userID = '".$userID."'");
            $oldActivityType = q("SELECT activityType FROM user_defaults WHERE companyID = '".$companyID."' AND userID = '".$userID."'");
            $oldActivity = q("SELECT activity FROM user_defaults WHERE companyID = '".$companyID."' AND userID = '".$userID."'");
            $oldVehicle = q("SELECT vehicle FROM user_defaults WHERE companyID = '".$companyID."' AND userID = '".$userID."'");

			if ($activityType == "null")	$activityType= 0;
			if ($activity == "null")			$activity = 0;
			if ($vehicle == "null")			$vehicle = 0;

            if ($oldLandingPage != $landingPage)
                $update = q("UPDATE user_defaults SET landingPage = '".$landingPage."' WHERE companyID = '".$companyID."' AND userID = '".$userID."'");

            if ($oldActivityType != $activityType)
                $update = q("UPDATE user_defaults SET activityType = '".$activityType."' WHERE companyID = '".$companyID."' AND userID = '".$userID."'");

            if ($oldActivity != $activity)
                $update = q("UPDATE user_defaults SET activity = '".$activity."' WHERE companyID = '".$companyID."' AND userID = '".$userID."'");

            if ($oldVehicle != $vehicle)
                $update = q("UPDATE user_defaults SET vehicle = '".$vehicle."' WHERE companyID = '".$companyID."' AND userID = '".$userID."'");

            $oldFrstName = q("SELECT frstname FROM Employee WHERE id = '".$userID."'");
            $oldLstName = q("SELECT lstname FROM Employee WHERE id = '".$userID."'");
            $oldEmail = q("SELECT email FROM Employee WHERE id = '".$userID."'");

            if ($oldFrstName != $frstname)
                $update = q("UPDATE Employee SET frstname = '".$frstname."' WHERE id = '".$userID."'");

            if ($oldLstName != $lstname)
                $update = q("UPDATE Employee SET lstname = '".$lstname."' WHERE id = '".$userID."'");

            if ($oldEmail != $email)    {
                if (!exist("Employee", "id != '".$userID."' AND email = '".$email."'"))
                    $update = q("UPDATE Employee SET email = '".$email."' WHERE id = '".$userID."'");
                else    {
                    $data = array("ERROR", "Email address already used for another user, please use another email address.");

                    echo json_encode($data);
                    break;
                }
            }

            $_SESSION["user"] = $frstname." ".$lstname;

            $_SESSION["default_activityType"] = $activityType;
            $_SESSION["default_activity"] = $activity;
            $_SESSION["default_vehicle"] = $vehicle;

            $data = array("SUCCESS", "Changes successfully made...");

            if (is_null($data))	$data = "";

            echo json_encode($data);
            break;
        }
        //////////////////////////////////////////////////
        //  UPDATE PASSWORD
        case "updatePassword":       {
            unset($data);

            $currentPW = addslashes(strip_tags($_POST["currentPW"]));
            $newPW = addslashes(strip_tags($_POST["newPW"]));

            $salt = q("SELECT salt FROM Employee WHERE id = '".$userID."'");
            $currentPW = generateHash($currentPW, $salt);

            if (exist("Employee", "id = '".$userID."' AND password = '".$currentPW."'"))        {
                $newPW= generateHash($newPW);

                $update = q("UPDATE Employee SET password = '$newPW', salt = '$origsalt' WHERE id = '".$userID."'");
            } else      {
                $data = array("ERROR", "Current password incorrect, please enter the correct password...");

                echo json_encode($data);
                break;
            }

            $data = array("SUCCESS", "Password changed successfully...");

            if (is_null($data))	$data = "";

            echo json_encode($data);
            break;
        }
        //////////////////////////////////////////////////
        //  CHECK CURRENT PASSWORD
        case "checkPassword":       {
            unset($data);

            $change = 0;

            $salt = q("SELECT salt FROM Employee WHERE id = '".$userID."'");
            $password = generateHash("password", $salt);

            if (exist("Employee", "id = '".$userID."' AND password = '".$password."'"))
                $change = 1;

            $data = $change;

            if (is_null($data))	$data = "";

            echo json_encode($data);
            break;
        }
        //////////////////////////////////////////////////
        //  CHECK IF ACTIONS NEED TO BE RE-ISSUED
        case "checkActions":       {
            unset($data);

            // Re-Issue User Actions
            if (!exist("reissue_actions", "companyID = '".$companyID."' AND userID = '".$userID."'"))
                $insert = q("INSERT INTO reissue_actions (companyID,userID,processed) VALUES ('".$companyID."','".$userID."','0')");

            if (exist("reissue_actions", "companyID = '".$companyID."' AND userID = '".$userID."' AND processed = '0'"))  {
                $actions = q("SELECT a.`action` FROM (((roles AS r INNER JOIN role_action AS ra ON r.id = ra.roleid) INNER JOIN user_role AS ur ON r.id = ur.roleid) INNER JOIN actions AS a ON ra.actionid = a.id) ".
                                "WHERE ur.companyid = '".$companyID."' AND ur.userid = '".$userID."' AND r.companyid = '".$companyID."'");

                if ($userName == "admin")       $actions = q("SELECT `action` FROM actions");

                if (is_array($actions))
                    foreach ($actions as $a)
                        $actionArr[] = $a[0];

                $_SESSION["actions"] = $actionArr;

                $update = q("UPDATE reissue_actions SET processed = 1 WHERE companyID = '".$companyID."' AND userID = '".$userID."'");
            }

            if (is_null($data))	$data = "";

            echo json_encode($data);
            break;
        }
        //////////////////////////////////////////////////
        //  GET USER TARIFF RATE(S)
        case "get_user_rates":	{
            unset($data);

            $userID = addslashes(strip_tags($_POST["userID"]));
            $companyID = addslashes(strip_tags($_POST["companyID"]));

            $rates = q("SELECT id, rate FROM user_rates WHERE userid = '".$userID."' AND companyid = '".$companyID."' AND active = 1 ORDER BY rate+0");
            $rates = sortArrayByColumn($rates, 1);

            $data[] = array(0,"0.00");

            if (is_array($rates))       {
                foreach ($rates as $r)  {
                    $data[] = $r;
                }
            }

            //$rates = q("SELECT tariff1,tariff2,tariff3,tariff4,tariff5 FROM Employee WHERE id = '".$userID."'");

            /*
            if (is_array($rates))   {
                $r = 0;

                for ($i = 1; $i <= 5; $i++) {
                    if ($rates[0][$i - 1] > 0) {
                        $data[$r][0] = $i;
                        $data[$r++][1] = $rates[0][$i - 1];
                    }
                }
            }
            */

            if (is_null($data))	$data = "";

            echo json_encode($data);
            break;
        }
        //////////////////////////////////////////////////
        //  CHECK USER PROJECT MANAGER STATUS
        case "check_pm_status":	{
            unset($data);

            $userID = addslashes(strip_tags($_POST["userID"]));
            $companyID = addslashes(strip_tags($_POST["companyID"]));

            $isPM = q("SELECT ur.id,r.id FROM (user_role AS ur INNER JOIN roles AS r ON ur.roleid = r.id) WHERE r.role = 'PM' AND ur.userid='".$userID."' AND ur.companyid = '".$companyID."' AND r.companyid = '".$companyID."'");
            $isPM = ($isPM[0][0] != 0) ? 1 : 0;

            $data[] = $isPM;

            echo json_encode($data);
            break;
        }
        //////////////////////////////////////////////////
        //  MENUS, NEWS & HELP FUNCTIONS
        //////////////////////////////////////////////////
        //  MENUS - MAIN
        case "getMenuItems":	{
            unset($data);

            $menuLvl = addslashes(strip_tags($_POST["menuLvl"]));
            $parentID = addslashes(strip_tags($_POST["parentID"]));

            if ($companyID != "")       {
                $menuItems = q("SELECT id,parentID,level,ranking,urlLink,menuDescr,icon, CONCAT(menuDescr,'[',urlLink ,']') FROM menus WHERE parentID = '".$parentID."' AND menuType = 'MAIN' ORDER BY ranking");

                if (is_array($menuItems))   {
                    foreach ($menuItems as $menuItem)  {
                        $subMenus = q("SELECT id,parentID,level,ranking,urlLink,menuDescr,icon,CONCAT(menuDescr,'[',urlLink ,']') FROM menus WHERE parentID = '".$menuItem[0]."' AND menuType = 'MAIN' ORDER BY ranking");

                        $isParent = (is_array($subMenus)) ? 1 : 0;

                        $menuItem[] = $isParent;

                        $actions = q("SELECT a.id, a.action FROM (menuAction AS ma INNER JOIN actions AS a ON a.id = ma.actionID) WHERE ma.menuID = '".$menuItem[0]."'");

                        if ($_SESSION["email"] === "admin")  {
                            if ($menuItem[5] != "Report Own Time/Expenses")
                                $data[] = $menuItem;
                        }
                        else if (is_array($actions))   {
                            foreach ($actions as $a)  {
                                if (hasAccess($a[1]))
                                    $data[] = $menuItem;
                            }
                        }
                    }
                }

                $data = filterQueryData($data, 9, 7);
            }

            if (is_null($data))	$data = "";

            echo json_encode($data);
            break;
        }
        //////////////////////////////////////////////////
        //  MENUS - SETTINGS
        case "getSettingItems":	{
            unset($data);

            $menuItems = q("SELECT id,urlLink,menuDescr,icon,modal,CONCAT(menuDescr,'[',urlLink ,']') FROM menus WHERE menuType = 'SETTINGS' ORDER BY ranking");

            if (is_array($menuItems))   {
                foreach ($menuItems as $menuItem)  {
                    $actions = q("SELECT a.id, a.action FROM (menuAction AS ma INNER JOIN actions AS a ON a.id = ma.actionID) WHERE ma.menuID = '".$menuItem[0]."'");

                    if ($_SESSION["email"] == "admin")
                        $data[] = $menuItem;
                    else if (is_array($actions))   {
                        foreach ($actions AS $a)  {
                            if (hasAccess($a[1]))
                                $data[] = $menuItem;
                        }
                    }
                }
            }

            $data = filterQueryData($data, 6, 5);

            if (is_null($data))	$data = "";

            echo json_encode($data);
            break;
        }
        //////////////////////////////////////////////////
        //  NEWS
        case "getNews":	{
            unset($data);

            $headings = q("SELECT id, heading1, date FROM RSS ORDER BY date DESC, time DESC LIMIT 1");

            if (is_array($headings))     {
                $headings = filterQueryData($headings, 3, 1);

                foreach ($headings as $heading) {
                    $records = q("SELECT id, description, date FROM RSS WHERE heading1 = '".$heading[1]."' AND date = '".$heading[2]."' ORDER BY date DESC, time DESC");

                    if (is_array($records))     {

                       $records = filterQueryData($records, 3, 1);

                        foreach ($records as $record)   {
                            $data[] = "<strong>".$heading[2]." - ".$heading[1].":</strong> ".$record[1];
                        }
                    }
                }
            }

            if (is_array($data))        $data = array_slice($data, 0, 3);

            if (is_null($data))	$data = "";

            echo json_encode($data);
            break;
        }
        //////////////////////////////////////////////////
        //  HELP
        case "getHelp":	{
            unset($data);

            $page = addslashes(strip_tags($_POST["page"]));

            $helpInfo = q("SELECT heading, content FROM help WHERE page = '".$page."'");

            if (is_array($helpInfo))    {
                $data[] = $helpInfo[0];
                $data[] = $helpInfo[1];
            }

            if (is_null($data))	$data = "";

            echo json_encode($data);
            break;
        }
        //////////////////////////////////////////////////
        //  SEND MAIL
        case "sendEmail":	{
            unset($data);

			global $serverName;

            $subject = addslashes(strip_tags($_POST["subject"]));
            $message = addslashes(strip_tags($_POST["desc"]));

            $name_ = q("SELECT lstname, frstname FROM Employee WHERE id = '".$userID."'");

            if ($_SESSION["email"] == "admin")	{
                $email_ = "ffaul@integrityengineering.co.za";
            } else	{
                $email_ = str_replace("ite.co.za","integrityengineering.co.za", $_SESSION["email"]);
            }

			if ($serverName == "demo.on-track.ws" || $serverName == "localhost")
				$from = addslashes(strip_tags($_POST["from"]));
			else	{
				if ($subject != "Password Request") {
					$from = addslashes(strip_tags($_POST["from"]));
				}
				else        {
					$info = preg_split("/ /", $message);
					$name_ = q("SELECT lstname, frstname FROM Employee WHERE email = '".$info[1]."'");
					$from = $name_[0][1]." ".$name_[0][0]."<".$info[1].">";
				}
			}

            //$from = $name_[0][1]." ".$name_[0][0]."<".$email_.">";
            $to = "support@integrityengineering.co.za";
            $reply_to = $from;
            $subject = "On-Track: ".$subject;

            sendmail($to, $subject, $message, $from, $reply_to);

            $data = "success";

            echo json_encode($data);
            break;
        }
        //////////////////////////////////////////////////
        //  GET LINKED EXPENSES OF INVOICE
        case "getLinkedExpenses":	{
            unset($data);

            $invoiceID = addslashes(strip_tags($_POST["invoiceID"]));

            //  Get Linked Expenses
            $expenses = q("SELECT id, descr, amount, (amount + (amount * (vat / 100))), due_date FROM ExpenseBudget WHERE company_id = '".$companyID."' AND  invoice_id = '".$invoiceID."' ORDER BY due_date");

            if (is_array($expenses)) {
                foreach ($expenses as $expense) {
                    if (!(hasAccess("EXPENSE_MANAGE"))) $expense[] = "0";
                    else                                $expense[] = "1";

                    $data[] = $expense;
                }
            }

            if (is_null($data))	$data = "";
            echo json_encode($data);

            break;
        }
        //////////////////////////////////////////////////
        //  GET LINKED STATEMENT ENTRIES OF INVOICE
        case "getStatementLinks":	{
            unset($data);

            $elementType = addslashes(strip_tags($_POST["elementType"]));
            $elementID = addslashes(strip_tags($_POST["elementID"]));

            //  Get Linked Expenses
            $data = q("SELECT s.date, s.description, s.amount FROM ((Statement AS s INNER JOIN StatementLink AS sl ON s.id = sl.statement_id) INNER JOIN ElementTypes AS et ON et.id = sl.elementType) ".
                            "WHERE et.name = '".$elementType."' AND et.company_id = '".$companyID."' AND sl.element_id = '".$elementID."' ORDER BY s.date, s.description");

            if (is_null($data))	$data = "";
            echo json_encode($data);

            break;
        }
//	link bank statement entries to ontrack elements
        case "linkStatement":	{
            unset($data);
            $act = addslashes(strip_tags($_POST["act"]));
            $statement_id = addslashes(strip_tags($_POST["statement_id"]));
            $element_id = addslashes(strip_tags($_POST["element_id"]));
            $type = addslashes(strip_tags($_POST["type"]));
			if ($act == "ADD")	{
				if ($type === "INVOICE")	{
					$value = "((consulting+expenses)+(consulting+expenses)*vat/100)";
					$amount = q("SELECT ROUND($value,2),L.id FROM LoadInvoices AS L WHERE L.id = '$element_id'");
					$amount2 = q("SELECT ROUND(amount,2),date FROM Statement AS S WHERE S.id = '$statement_id'");
					if ($amount[0][0] == $amount2[0][0])	{	//amounts are equal, now update duedate and paid of invoice
						$update = q("UPDATE LoadInvoices SET due_date = '".$amount2[0][1]."', paid = 1 WHERE id = '$element_id'");
					}
					$exist = q("SELECT id,id FROM StatementLink WHERE company_id = '".$_SESSION["company_id"]."' AND statement_id = $statement_id AND
								element_id = $element_id AND elementType = (SELECT id FROM ElementTypes WHERE name = '$type' AND company_id = '".$_SESSION["company_id"]."')");
					if (!is_array($exist))
					$insert = q("INSERT INTO StatementLink (company_id,statement_id,element_id,elementType) VALUES ".
						"('".$_SESSION["company_id"]."',$statement_id,$element_id,
							(SELECT id FROM ElementTypes WHERE name = '$type' AND company_id = '".$_SESSION["company_id"]."'))");
					$data = q("SELECT due_date,$value,'".$amount2[0][0]."' FROM LoadInvoices AS L WHERE L.id = '$element_id'");
				}
				else if ($type === "EXPENSE") {
					$value = "((amount)+(amount)*vat/100)";
					$amount = q("SELECT -ROUND($value,2),L.id FROM ExpenseBudget AS L WHERE L.id = '$element_id'");
					$amount2 = q("SELECT ROUND(amount,2),date FROM Statement AS S WHERE S.id = '$statement_id'");
					if ($amount[0][0] == $amount2[0][0])	{	//amounts are equal, now update duedate and paid of invoice
						$update = q("UPDATE ExpenseBudget SET due_date = '".$amount2[0][1]."', paid = 1 WHERE id = '$element_id'");
					}
					else	{	//amounts aren't equal, create new expense and udpate original expense's amount accordingly
						$exp = q("SELECT * FROM ExpenseBudget WHERE id = '".$amount[0][1]."'");
						$e = $exp[0];					//convenience
						$am = -($amount2[0][0]);		//convenience
						$vat = $e[4];
						if ($vat != 0)
                                                    $am = $am/(1+$vat/100);	//subtract VAT from bankstatement entry
						$am = round($am,2);
						//get count of expenses linked to the same invoice_id to add do desc field in order to keep them unique
						$base = (strpos($e[2], "__") === false) ? $e[2] : substr($e[2],0,strpos($e[2], "__")-1);
						$expCount = q("SELECT count(id) FROM ExpenseBudget WHERE invoice_id = '$e[6]' AND ".
										"(SUBSTRING(descr,1,LOCATE('__',descr)-1) = '$base' OR descr = '$base')");
						//insert new expense and update orig's amount accordingly
						$insert = q("INSERT INTO ExpenseBudget (project_id,descr,amount,vat,due_date,invoice_id,company_id,paid) VALUES ".
							"('$e[1]','$base"."__$expCount','$am','$e[4]','".$amount2[0][1]."','$e[6]','$e[7]',1)");
						$update = q("UPDATE ExpenseBudget SET amount = amount - $am WHERE id = '$element_id'");
						$element_id = q("SELECT id FROM ExpenseBudget WHERE ".
											"project_id = '$e[1]' ".
											"AND descr = '$base"."__$expCount' ".
											"AND amount = '$am' ".
											"AND due_date = '".$amount2[0][1]."' ".
											"AND invoice_id = '$e[6]' ".
											"AND company_id = '$e[7]' ".
											"ORDER BY id DESC LIMIT 1");
					}
					$exist = q("SELECT id,id FROM StatementLink WHERE company_id = '".$_SESSION["company_id"]."' AND statement_id = $statement_id AND
								element_id = $element_id AND elementType = (SELECT id FROM ElementTypes WHERE name = '$type' AND company_id = '".$_SESSION["company_id"]."')");
					if (!is_array($exist))
					$insert = q("INSERT INTO StatementLink (company_id,statement_id,element_id,elementType) VALUES ".
						"('".$_SESSION["company_id"]."',$statement_id,$element_id,
							(SELECT id FROM ElementTypes WHERE name = '$type' AND company_id = '".$_SESSION["company_id"]."'))");
					$data = q("SELECT due_date,-ROUND($value,2),'".$amount2[0][0]."' FROM ExpenseBudget AS L WHERE L.id = '$element_id'");
				}
				else if ($type === "BUDGET")	{
                                        //TODO: MK to ask JC if updated = 1 for budget update should come here
					$value = "(planned)";
					$amount2 = q("SELECT ROUND(amount,2),date FROM Statement AS S WHERE S.id = '$statement_id'");
					$update = q("UPDATE Budget SET amount = amount - '".($amount2[0][0])."' WHERE id = '$element_id'");
					$exist = q("SELECT id,id FROM StatementLink WHERE company_id = '".$_SESSION["company_id"]."' AND statement_id = $statement_id AND
								element_id = $element_id AND elementType = (SELECT id FROM ElementTypes WHERE name = '$type' AND company_id = '".$_SESSION["company_id"]."')");
					if (!is_array($exist))
					$insert = q("INSERT INTO StatementLink (company_id,statement_id,element_id,elementType) VALUES ".
						"('".$_SESSION["company_id"]."',$statement_id,$element_id,
							(SELECT id FROM ElementTypes WHERE name = '$type' AND company_id = '".$_SESSION["company_id"]."'))");
					$data = q("SELECT due_date,-ROUND($value,2),'".$amount2[0][0]."' FROM Budget AS L WHERE L.id = '$element_id'");
				}
			}
			else if ($act == "DELETE")	{
				if ($type === "INVOICE")	{
					$value = "((consulting+expenses)+(consulting+expenses)*vat/100)";
					$update = q("UPDATE LoadInvoices SET paid = 0 WHERE id = '$element_id'");
					$insert = q("INSERT INTO StatementLink (company_id,statement_id,element_id,elementType) VALUES ".
						"('".$_SESSION["company_id"]."',$statement_id,$element_id,
							(SELECT id FROM ElementTypes WHERE name = '$type' AND company_id = '".$_SESSION["company_id"]."'))");
					$data = q("SELECT due_date,$value FROM LoadInvoices AS L WHERE L.id = '$element_id'");
				}
				else if ($type === "EXPENSE") {
					$value = "((amount)+(amount)*vat/100)";
					$update = q("UPDATE ExpenseBudget SET paid = 0 WHERE id = '$element_id'");
					$insert = q("INSERT INTO StatementLink (company_id,statement_id,element_id,elementType) VALUES ".
						"('".$_SESSION["company_id"]."',$statement_id,$element_id,
							(SELECT id FROM ElementTypes WHERE name = '$type' AND company_id = '".$_SESSION["company_id"]."'))");
					$data = q("SELECT due_date,-ROUND($value,2) FROM ExpenseBudget AS L WHERE L.id = '$element_id'");
				}
				else if ($type === "BUDGET")	{
                                        //TODO: MK to ask JC if updated = 1 for budget update should come here
					$value = "(planned)";
					$amount2 = q("SELECT ROUND(amount,2),date FROM Statement AS S WHERE S.id = '$statement_id'");
					$update = q("UPDATE Budget SET amount = amount + '".($amount2[0][0])."' WHERE id = '$element_id'");
					$data = q("SELECT due_date,-ROUND($value,2) FROM Budget AS L WHERE L.id = '$element_id'");
				}
				$delete = q("DELETE FROM StatementLink WHERE company_id = '".$_SESSION["company_id"]."' AND statement_id = '$statement_id' AND ".
					"element_id = '$element_id' AND elementType =
						(SELECT id FROM ElementTypes WHERE name = '$type' AND company_id = '".$_SESSION["company_id"]."')");
			}
            if (is_null($data))	$data = "";
            echo json_encode($data);
            break;
        }
        //	if multiple invoices were linked to 1 statement and the amount matches, all those invoices' due_date and paid values should be updated
        case "updateInvoices":	{
            unset($data);
            $act = addslashes(strip_tags($_POST["act"]));
            $statement_id = addslashes(strip_tags($_POST["statement_id"]));
            $type = addslashes(strip_tags($_POST["type"]));

			$value = "((consulting+expenses)+(consulting+expenses)*vat/100)";
			$amount2 = q("SELECT ROUND(amount,2),date FROM Statement AS S WHERE S.id = '$statement_id'");
			$invoices = q("SELECT L.id,element_id FROM LoadInvoices AS L INNER JOIN StatementLink as SL ON L.id = SL.element_id WHERE SL.statement_id = '$statement_id'");
			foreach($invoices as $invoice)	{
				$update = q("UPDATE LoadInvoices SET due_date = '".$amount2[0][1]."', paid = 1 WHERE id = '".$invoice[0]."'");
				//echo "UPDATE LoadInvoices SET due_date = '".$amount2[0][1]."', paid = 1 WHERE id = '".$invoice[0]."'";
			}

            $data = "";
            echo json_encode($data);
            break;
        }
        //	search for an ontrack element to match link to the bank statement
        case "searchLink":	{
            unset($data);
            $typeFilterM = addslashes(strip_tags($_POST["typeFilterM"]));
			if ($typeFilterM != "")
				if (strlen($typeFilterM) < 3)	{
					$data = "";
					echo json_encode($data);
					break;
				}
            $typeM = addslashes(strip_tags($_POST["typeM"]));
            $typeDateM = addslashes(strip_tags($_POST["typeDateM"]));
			$type = q("SELECT name FROM ElementTypes WHERE id = '$typeM'");

			if ($type === "INVOICE")	{
				if ($typeFilterM != "")
					$filter = "AND name LIKE '%$typeFilterM%' ";
				$value = "((consulting+expenses)+(consulting+expenses)*vat/100)";
				$data = q("SELECT due_date,'INVOICE',name,ROUND($value,2),ROUND(ABS($amount-$value),2) AS A,'UNLINKED',L.id FROM LoadInvoices AS L ".
					"WHERE L.company_id = '".$_SESSION["company_id"]."' AND paid = 0 $filter".
					"ORDER BY due_date");
			}
			else if ($type === "EXPENSE")	{
				if ($typeFilterM != "")
					$filter = "AND descr LIKE '%$typeFilterM%' ";
				$value = "((amount)+(amount)*vat/100)";
				$data = q("SELECT due_date,'EXPENSE',descr,-(ROUND($value,2)),ROUND(ABS(-$amount-$value),2) AS A,'UNLINKED',E.id FROM ExpenseBudget AS E ".
					"LEFT JOIN StatementLink AS SL ON E.id = SL.element_id AND SL.elementType = (SELECT id FROM ElementTypes WHERE company_id = '".$_SESSION["company_id"]."' AND name = 'EXPENSE') ".
					"WHERE E.company_id = '".$_SESSION["company_id"]."' AND paid = 0 $filter".
					"AND SL.id IS NULL ".
					"ORDER BY due_date");
			}
			else if ($type === "BUDGET")	{
				$dateFrom = ">= '".date('Ym',strtotime("-3 months"))."' ";//TODO should be 1year. maybe make it 6months?
				if ($typeDateM != "")
					$dateFrom = "= '$typeDateM' ";
				if ($typeFilterM != "")
					$filter = "AND CONCAT(DATE_FORMAT(CONCAT(B.year, '-', B.month, '-01'),'%b'),' - ',C.name,' - ',E.name) LIKE '%$typeFilterM%' ";
				$value = "(planned)";
				$data = q("SELECT B.due_date,'BUDGET',CONCAT(DATE_FORMAT(CONCAT(B.year, '-', B.month, '-01'),'%b'),' - ',C.name,' - ',E.name), ".
					"-(ROUND($value,2)),ROUND(ABS(-$amount-$value),2) AS A, 'UNLINKED',B.id ".
					"FROM Budget AS B ".
					"INNER JOIN Elements AS E ON B.element_id = E.id ".
					"INNER JOIN Category AS C ON E.parent_id = C.id ".
					"WHERE B.company_id = '".$_SESSION["company_id"]."' AND CONCAT(B.year, B.month) $dateFrom $filter AND E.status = '1' ".
					"ORDER BY B.due_date");
			}

            if (is_null($data))	$data = "";
            echo json_encode($data);
            break;
        }
        ////////////////
        //Update Statement Description
        ////////////////
        case "statementUpdate":	{
            unset($data);
            $statement_id = addslashes(strip_tags($_POST["statement_id"]));
            $description = addslashes(strip_tags($_POST["description"]));
            $update = q("UPDATE Statement SET description_new = '$description' WHERE id = '$statement_id'");

            if (is_null($data))	$data = "";
            echo json_encode($data);
            break;
        }

        case "statementDelete":	{
            unset($data);
            $statement_id = addslashes(strip_tags($_POST["statement_id"]));
            $delete = q("DELETE FROM Statement WHERE id = '$statement_id'");

            if (is_null($data))	$data = "";
            echo json_encode($data);
            break;
        }

        //////////////////////////////////////////////////
        //  CHECK IF ITEM SEEN
         case "setSeen":	{
            unset($data);

            $videoName = addslashes(strip_tags($_POST["video"]));

            $update = q("UPDATE seen SET $videoName = 1 WHERE user_id = '".$userID."'");

            if (is_null($data))	$data = "";

            echo json_encode($data);
            break;
        }
        //////////////////////////////////////////////////
        //  REGISTRATION FUNCTIONS
        //////////////////////////////////////////////////
        //  ON-TRACK REGISTRATION
        case "registration":	{
            unset($data);

            $companyInfo = $_POST["companyInfo"];
            $userInfo = $_POST["userInfo"];
            $rateInfo = $_POST["rateInfo"];
            $projectTypeInfo = $_POST["projectTypeInfo"];
            $activityTypeInfo = $_POST["activityTypeInfo"];
            $vehicleInfo = $_POST["vehicleInfo"];
            $projectInfo = $_POST["projectInfo"];

            //  Step 1 - Company Information
            $insert = q("INSERT INTO Company (name,rate,currency,notifyPeriod,locked) VALUES ('".$companyInfo[0][0]."','55.00','".$companyInfo[0][1]."',3,'0')");

            $companyID = q("SELECT id FROM Company WHERE name = '".$companyInfo[0][0]."'");

            //  Step 2 - Personal Information
            $password = generateHash($userInfo[0][4]);

            $insert = q("INSERT INTO Employee (lstname,frstname,email,password,salt,company_id,cost,demo_user,deleted,locked,activationDate,usertype) VALUES ".
                        "('".$userInfo[0][1]."','".$userInfo[0][0]."','".$userInfo[0][2]."','$password','$origsalt',".$companyID.",'".$userInfo[0][5]."',1,0,0,'".date("Y-m-d")."',0)");

            $userID = q("SELECT id FROM Employee WHERE email = '".$userInfo[0][2]."'");
            $userEmail = $userInfo[0][2];

            $insert = q("INSERT INTO seen (user_id,comingSoon,intro,tutorial,ctrlF5,social,blog) VALUES ('".$userID."',1,1,1,1,1,1)");

            $insert = q("INSERT INTO roles (companyid,role,descr,active,dflt) SELECT ".$companyID.",role,descr,active,dflt FROM roles WHERE companyid = 0");
            $insert = q("INSERT INTO role_action (companyid,roleid,actionid) SELECT ".$companyID.",roleid,actionid FROM role_action WHERE companyid = 0");

            $roles = q("SELECT id,role FROM roles WHERE companyid = 0 ORDER BY id");

            if (is_array($roles))       {
                foreach ($roles as $r)  {
                    $oldRoleID = $r[0];
                    $newRoleID = q("SELECT id FROM roles WHERE companyid = '".$companyID."' AND role = '".$r[1]."'");

                    $update = q("UPDATE role_action SET roleid = '".$newRoleID."' WHERE companyid = '".$companyID."' AND roleid = '".$oldRoleID."'");
                }
            }

            $insert = q("INSERT INTO user_role(companyid,userid,roleid) VALUES ".
                    "('".$companyID."','".$userID."',(SELECT id FROM roles WHERE role = 'MAN' AND companyid = '".$companyID."')),".
                    "('".$companyID."','".$userID."',(SELECT id FROM roles WHERE role = 'PM' AND companyid = '".$companyID."')),".
                    "('".$companyID."','".$userID."',(SELECT id FROM roles WHERE role = 'FM' AND companyid = '".$companyID."')),".
                    "('".$companyID."','".$userID."',(SELECT id FROM roles WHERE role = 'TM' AND companyid = '".$companyID."')),".
                    "('".$companyID."','".$userID."',(SELECT id FROM roles WHERE role = 'CL' AND companyid = '".$companyID."'))");

            $insert = q("INSERT INTO Company_Users (user_id,company_id) VALUES ('".$userID."','".$companyID."')");

            if (is_array($rateInfo))    {
                foreach ($rateInfo as $rate)    {
                    $insert = q("INSERT INTO user_rates(companyid,userid,rate) VALUES (".$companyID.",".$userID.",'".number_format((double)$rate, 2, ".", "")."')");
                }
            }

            $insert = q("INSERT INTO user_leave(companyid,userid) VALUES ('".$companyID."','".$userID."')");

            if (is_array($userInfo[0][6]))    {
                foreach ($userInfo[0][6] as $leave)    {
                    $update = q("UPDATE user_leave SET ".$leave[0]." = '".$leave[1]."' WHERE companyid = '".$companyID."' AND userid = '".$userID."'");
                }
            }

            updateMenuIndex($companyID,$userID);

            //  Step 3 - Project Type(s)
            if (is_array($projectTypeInfo))    {
                foreach ($projectTypeInfo as $projectType)    {
                    $insert = q("INSERT INTO ProjectTypes (type,company_id) VALUES ('".$projectType."','".$companyID."')");
                }
            }

            //  Step 4 - Activity Type(s) & Activities
            if (is_array($activityTypeInfo))    {
                foreach ($activityTypeInfo as $activityType)    {
                    $master = $activityType[0];
                    $subs = preg_split("/,/",$activityType[1]);

                    $insert = q("INSERT INTO ActivityTypes (type,company_id,active,dateCreated,createdBy,dateUpdated,updatedBy) VALUES ".
                                    "('".$master."','".$companyID."',1,'".date("Y-m-d")."','".$userEmail."','".date("Y-m-d")."','".$userEmail."')");

                    $masterID = q("SELECT id FROM ActivityTypes WHERE company_id = '".$companyID."' AND type = '".$master."'");

                    if (is_array($subs))    {
                        foreach ($subs as $sub)    {
                            $insert = q("INSERT INTO Activities (parent_id,name,active,dateCreated,createdBy,dateUpdated,updatedBy) VALUES ".
                                            "('".$masterID."','".$sub."',1,'".date("Y-m-d")."','".$userEmail."','".date("Y-m-d")."','".$userEmail."')");
                        }
                    }
                }
            }

            //  Step 5 - Vehicle(s)
            if (is_array($vehicleInfo))    {
                foreach ($vehicleInfo as $vehicle)    {
                    $insert = q("INSERT INTO Vehicle (type,rate,company_id,deleted) VALUES ('".$vehicle[0]."','".number_format((double)$vehicle[1], 2, ".", "")."','".$companyID."',0)");
                }
            }

            //  Step 6 - Project Information
            $typeID = q("SELECT id FROM ProjectTypes WHERE type = '".$projectInfo[0][3]."' AND company_id = '".$companyID."'");
            $rateID = q("SELECT id FROM user_rates WHERE companyid = '".$companyID."' AND userid = '".$userID."' AND rate = '".$projectInfo[0][13]."'");
            $invoiceCycleID = q("SELECT id FROM dropdowns WHERE categoryID = (SELECT id FROM categories WHERE code = 'invoiceCycle') AND name = '1 Month'");

            $insert = q("INSERT INTO Project (name,reg_date,submitted_by_id,type_id,due_date,company_id,completed,status,invoiceCycle,travelRate,deleted,dateCreated,createdBy,dateUpdated,updatedBy) VALUES ".
                        "('".$projectInfo[0][1]."','".$projectInfo[0][2]."',0,".$typeID.",'".$projectInfo[0][10]."','".$companyID."',0,'".$projectInfo[0][0]."','".$invoiceCycleID."','".$projectInfo[0][11]."',0,'".date("Y-m-d")."','".$userEmail."','".date("Y-m-d")."','".$userEmail."')");

            $projectID = q("SELECT id FROM Project WHERE company_id = '".$companyID."' AND name = '".$projectInfo[0][1]."'");
            $vat = ($projectInfo[0][8] != "") ? $projectInfo[0][8] : 15;
            $buffer = ($projectInfo[0][9] != "") ? $projectInfo[0][9] : 0;

            $insert = q("INSERT INTO companiesOnTeam (project_id,company_id,total_budget,consulting,expenses,diverse_income,vat,buffer,status,teamManage,locked,invoiceCycle,deleted,overBudget,OBEmailSent,dateCreated,createdBy,dateUpdated,updatedBy) VALUES ".
                        "('".$projectID."','".$companyID."','".$projectInfo[0][4]."','".$projectInfo[0][5]."','".$projectInfo[0][6]."','".$projectInfo[0][7]."','".$vat."','".$buffer."',1,2,0,0,0,0,0,'".date("Y-m-d")."','".$userEmail."','".date("Y-m-d")."','".$userEmail."')");

            $insert = q("INSERT INTO Project_User (company_id,user_id,rateID,user_budget,project_id,manager,deleted,dateCreated,createdBy,dateUpdated,updatedBy,spm_manager) VALUES ".
                        "('".$companyID."','".$userID."','".$rateID."','".$projectInfo[0][12]."','".$projectID."',1,0,'".date("Y-m-d")."','".$userEmail."','".date("Y-m-d")."','".$userEmail."',0)");

            //  Set Session Variables
            $_SESSION["logged_in"] = true;
            $_SESSION["user_id"] = $userID;
            $_SESSION["email"] = $userEmail;
            $_SESSION["primary_company"] = $companyID;
            $_SESSION["user"] = q("SELECT CONCAT(frstname, ' ', lstname) FROM Employee WHERE id = '".$_SESSION["user_id"]."' ");

            $_SESSION["company_id"] = $_SESSION["primary_company"];
            $_SESSION["company_name"] = q("SELECT name FROM Company WHERE id = '".$_SESSION["primary_company"]."'");
            $_SESSION["currency"] = q("SELECT currency FROM Company WHERE id = '".$_SESSION["primary_company"]."'");

            $actions = q("SELECT a.`action` FROM (((roles AS r INNER JOIN role_action AS ra ON r.id = ra.roleid) INNER JOIN user_role AS ur ".
                            "ON r.id = ur.roleid) INNER JOIN actions AS a ON ra.actionid = a.id) ".
                            "WHERE ur.companyid = '".$_SESSION["company_id"]."' AND ra.companyid = '".$_SESSION["company_id"]."' AND ur.userid = '".$_SESSION["user_id"]."' ".
                                "AND r.companyid = '".$_SESSION["company_id"]."'");

            if (is_array($actions))
                foreach ($actions as $a)
                    $actionArr[] = $a[0];

            $_SESSION["actions"] = $actionArr;

            $modules = q("SELECT m.code, m.name FROM (Modules AS m INNER JOIN Module_Users AS mu ON m.id = mu.module_id) WHERE mu.company_id = '".$_SESSION["company_id"]."' AND m.code = 'approval'");

            if (!is_array($modules))        $_SESSION["approvalModule"] = 0;
            else                            $_SESSION["approvalModule"] = 1;

            $data = "eula.php";

            if (is_null($data))	$data = "";

            echo json_encode($data);
            break;
        }
        //////////////////////////////////////////////////
        //  APPROVAL MODULE FUNCTIONS
        //////////////////////////////////////////////////
        //  GET PROJECTS BASED ON SELECT DETAILS
        case "getApprovalProjects":	{
            unset($data);

			$report = addslashes(strip_tags($_POST["report"]));
			$level = addslashes(strip_tags($_POST["level"]));
			$entryStatus = addslashes(strip_tags($_POST["entryStatus"]));
			$projectType = addslashes(strip_tags($_POST["projectType"]));
			$mainProjectType = addslashes(strip_tags($_POST["mainProjectType"]));

			$where = "";

			if ($level == "SP")	$where .= "AND p.p_type = 'SP' AND pu.spm_manager = '1' ";

			if ($entryStatus == "unapproved" && $report == "0")			$where .= "AND (CASE WHEN p.p_type = 'CP'
																					THEN (ts.status IN ('0','1') OR es.status IN ('0','1'))
																					ELSE (ts.status IN ('0','1') OR es.status IN ('0','1') OR ts.status2 IN ('0','1') OR es.status2 IN ('0','1'))
																				END) ";
			else if ($entryStatus == "unapproved" && $report == "1")	$where .= "AND (CASE WHEN p.p_type = 'CP'
																					THEN (ts.status IN ('0') OR es.status IN ('0'))
																					ELSE (ts.status IN ('0') OR es.status IN ('0') OR ts.status2 IN ('0') OR es.status2 IN ('0'))
																				END) ";
			else if ($entryStatus == "updated")							$where .= "AND (CASE WHEN p.p_type = 'CP'
																					THEN (ts.status IN ('1') OR es.status IN ('1'))
																					ELSE (ts.status IN ('1') OR es.status IN ('1') OR ts.status2 IN ('1') OR es.status2 IN ('1'))
																				END) ";
			else if ($entryStatus == "approved")						$where .= "AND (CASE WHEN p.p_type = 'CP'
																					THEN (ts.status IN ('2') OR es.status IN ('2'))
																					ELSE (ts.status IN ('2') OR es.status IN ('2') OR ts.status2 IN ('2') OR es.status2 IN ('2'))
																				END) ";
			else if ($entryStatus == "declined")						$where .= "AND (CASE WHEN p.p_type = 'CP'
																					THEN (ts.status IN ('3') OR es.status IN ('3'))
																					ELSE (ts.status IN ('3') OR es.status IN ('3') OR ts.status2 IN ('3') OR es.status2 IN ('3'))
																				END) ";

			$where .= ($projectType == "invoiceable") ? "AND cot.total_budget > 0 " : "AND cot.total_budget = '' ";
			$where .= ($mainProjectType != "null") ? "AND p.type_id = '".$mainProjectType."' " : "";

			$data = q("SELECT DISTINCT(p.id), p.name
							FROM (Project AS p
								INNER JOIN Project_User AS pu ON pu.project_id = p.id
								INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
								LEFT JOIN ApprovedTime AS ts ON ts.project_id = p.id
								LEFT JOIN ApprovedExpense AS es ON es.project_id = p.id)
							WHERE pu.user_id = '".$_SESSION["user_id"]."'
								AND cot.company_id = '".$_SESSION["company_id"]."'
								AND pu.company_id = '".$_SESSION["company_id"]."'
								AND p.completed = '0'
								AND p.deleted = '0'
								$where
							ORDER BY UPPER(p.name)");

            echo json_encode($data);
            break;
        }
        //////////////////////////////////////////////////
        //  GET COMPANIES BASED ON SELECT DETAILS
        case "getApprovalCompanies":	{
            unset($data);

			$project = addslashes(strip_tags($_POST["project"]));

			$data = q("SELECT DISTINCT(c.id), c.name
						FROM (Company AS c
							INNER JOIN companiesOnTeam AS cot ON c.id = cot.company_id
							INNER JOIN Project AS p ON p.id =  cot.project_id)
						WHERE cot.project_id = '".$project."' ORDER BY c.name");

            echo json_encode($data);
            break;
        }
        //////////////////////////////////////////////////
        //  GET LIST OF ACTIVITY TYPES
        case "getActivityTypes":	{
            unset($data);

            $projectID = addslashes(strip_tags($_POST["projectID"]));
			$projectType = q("SELECT p_type FROM Project WHERE id = '".$projectID."'");

            $data = ($projectType == "CP") ? q("SELECT id,type FROM ActivityTypes WHERE company_id = '".$companyID."' AND active = '1' ORDER BY type")
                                            :q("SELECT id,type FROM ActivityTypes WHERE pID = '".$projectID."' AND active = '1' ORDER BY type");

            if (is_null($data))	$data = "";

            echo json_encode($data);
            break;
        }
        //////////////////////////////////////////////////
        //  DATE FUNCTION(S)
        //////////////////////////////////////////////////
        case "getDates":	{
            unset($data);

			$project = addslashes(strip_tags($_POST["project"]));
			$dateType = addslashes(strip_tags($_POST["dateType"]));

			if ($dateType == "currentwk")      {
				$dateFrom = currentWeekStart();
				$dateTo = date("Y-m-d");
			}
			else if ($dateType == "previouswk")        {
				$dateFrom = getDates(currentWeekStart(), -7);
				$dateTo = getDates(currentWeekStart(), -1);
			}
			else if ($dateType == "currentmnth")       {
				$dateFrom = getMonthStart(date("Y-m-d"));
				$dateTo = date("Y-m-d");
			}
			else if ($dateType == "previousmnth")      {
				$dateFrom = getPreviousMonthStart();
				$dateTo = getMonthEnd(getPreviousMonthStart());
			}
			else if ($dateType == "projectLifetime")      {
				$dateFromT = q("SELECT MIN(date) FROM ApprovedTime WHERE project_id = '".$project."'");
				$dateFromE = q("SELECT MIN(date) FROM ApprovedExpense WHERE project_id = '".$project."'");

				if ($dateFromE == null)	$dateFromE = $dateFromT;

				$dateFrom = ($dateFromT <= $dateFromE) ? $dateFromT : $dateFromE;
				$dateTo = date("Y-m-d");
			}

			if ($dateType != "custom")	$data = array($dateFrom, $dateTo);

            echo json_encode($data);
            break;
        }

		case "getGPSLogbookData":	{
            unset($data);

			$data = "failed";

			// -- On-Track variables -- //
			$user_email = addslashes(strip_tags($_POST["user_email"]));
			$user_id = addslashes(strip_tags($_POST["user_id"]));

			// -- Logbook API variables to Retrieve data from GPS Logbook ONLINE -- //
			$logbook_user_id = "";
			$logbook_username = "OnTrack";
			$logbook_password = "8EAD4425-4181-4FC8-8F2F-78CFCE8A07FC";

			// Get Logbook User ID
			$host = "https://api.gpslogbook.co.za/otapi/User/GetId/?email=".$user_email;
			$CURL = curl_init();
			curl_setopt($CURL, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($CURL, CURLOPT_SSL_VERIFYPEER, false);
			//curl_setopt($CURL, CURLOPT_CERTINFO, false);
			curl_setopt($CURL, CURLOPT_URL, $host);
			curl_setopt($CURL, CURLOPT_USERPWD, $logbook_username . ":" . $logbook_password);
			$CURL_return_data = curl_exec($CURL);
			$header = curl_getinfo($CURL);
			if ($CURL_return_data == false)	{
				curl_close($CURL);
			} else	{
				// Display Returned Data
				curl_close($CURL);
				$logbook_user_id = json_decode($CURL_return_data);
				$logbook_user_id = $logbook_user_id->User->UserId;
			}

			// Get Logbook User Data
			unset($CURL_return_data);

			// Ensure only non-existent/newest entries are imported
			$since_date = "";
			$last_trip_end = q("SELECT MAX(endDateLocal) FROM gps_trip_data WHERE user_id = '".$user_id."' LIMIT 1");
			if(strlen($last_trip_end) == 19) {
				$since_date = "since/".date("Y/m/d/H/i/s", strtotime($last_trip_end));
            }
            
            if ($_SESSION["company_name"] == "OMI Solutions" && $since_date == "") {
                $since_date = "since/2020/01/01/00/00/00";
            }

			$host = "https://api.gpslogbook.co.za/otapi/".$logbook_user_id."/trips/".$since_date;
			$CURL = curl_init();
			curl_setopt($CURL, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($CURL, CURLOPT_SSL_VERIFYPEER, false);
			//curl_setopt($CURL, CURLOPT_CERTINFO, false);
			curl_setopt($CURL, CURLOPT_URL, $host);
			curl_setopt($CURL, CURLOPT_USERPWD, $logbook_username . ":" . $logbook_password);
			$CURL_return_data = curl_exec($CURL);
			$header = curl_getinfo($CURL);

			if ($CURL_return_data == false)	{
				curl_close($CURL);
			} else	{
				curl_close($CURL);
				$trips = $CURL_return_data;
			}
			$trips = json_decode($trips);

			if(count($trips->Devices) > 0) {
				// Insert data to On-Track DB
				$date = date("Y-m-d H:i:s");
				foreach($trips->Devices[0]->Trips as $tripInfo) {
                    if ($_SESSION["company_name"] == "OMI Soluttions") {
                        if ($tripInfo->Designation == "B") {
                            $insert = q("INSERT INTO gps_trip_data(user_id, totalDistanceMetres, totalTimeSeconds, maxSpeedKmH, avgSpeedKmH, startDateUtc, startDateLocal, endDateLocal, startLat, startLong, endLat, endLong, comment, dateImported) VALUES (
                                '".$user_id."',
                                '".$tripInfo->TotalDistanceMetres."',
                                '".$tripInfo->TotalTimeSeconds."',
                                '".$tripInfo->MaxSpeedKmH."',
                                '".$tripInfo->AvgSpeedKmH."',
                                '".$tripInfo->StartDateUtc."',
                                '".date("Y-m-d H:i:s", strtotime($tripInfo->StartDateLocal))."',
                                '".date("Y-m-d H:i:s", strtotime($tripInfo->EndDateLocal))."',
                                '".$tripInfo->StartLat."',
                                '".$tripInfo->StartLong."',
                                '".$tripInfo->EndLat."',
                                '".$tripInfo->EndLong."',
                                '".$tripInfo->Comment."',
                                '".$date."'
                            )");        
                        }
                    } else {
                        $insert = q("INSERT INTO gps_trip_data(user_id, totalDistanceMetres, totalTimeSeconds, maxSpeedKmH, avgSpeedKmH, startDateUtc, startDateLocal, endDateLocal, startLat, startLong, endLat, endLong, comment, dateImported) VALUES (
                            '".$user_id."',
                            '".$tripInfo->TotalDistanceMetres."',
                            '".$tripInfo->TotalTimeSeconds."',
                            '".$tripInfo->MaxSpeedKmH."',
                            '".$tripInfo->AvgSpeedKmH."',
                            '".$tripInfo->StartDateUtc."',
                            '".date("Y-m-d H:i:s", strtotime($tripInfo->StartDateLocal))."',
                            '".date("Y-m-d H:i:s", strtotime($tripInfo->EndDateLocal))."',
                            '".$tripInfo->StartLat."',
                            '".$tripInfo->StartLong."',
                            '".$tripInfo->EndLat."',
                            '".$tripInfo->EndLong."',
                            '".$tripInfo->Comment."',
                            '".$date."'
                        )");    
                    }
				}
				$data ="success";
			}

			echo  json_encode($data);
            break;
		}

		case "locationAddUpdate": {
            unset($data);
            $location_id = addslashes(strip_tags($_POST["location_id"]));
            $description = addslashes(strip_tags($_POST["description"]));
            $latitude = addslashes(strip_tags($_POST["latitude"]));
            $longitude = addslashes(strip_tags($_POST["longitude"]));
			$companyID = $_SESSION["company_id"];

			// - Update Location - //
			if($location_id != "" && $location_id > 0) {
				$nameExists = q("SELECT description, latitude, longitude FROM locations WHERE description = '".$description."' AND company_id = '".$companyID."' AND id NOT IN (".$location_id.") LIMIT 1");
				if(is_array($nameExists)) {
					$data = "Location '".$nameExists[0][0]."' already exists";
				}else {
					$date = date("Y-m-d H:i:s");
					$user = $_SESSION["email"];
					$update = q("UPDATE locations SET
						description = '".$description."',
						updatedBy = '".$user."',
						dateUpdated = '".$date."'
						WHERE company_id = '".$companyID."' AND id = '".$location_id."'
					");
					$logs = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id)
						VALUES (
							'".$description." updated',
							'update',
							'locations',
							'".$user."',
							'".date("Y-m-d")."',
							'".date("H:i:s")."',
							'".$companyID."'
					)");
					$data['msg'] = "updated";
					$data['description'] = $description;
				}
			}else {
				// - Add Location - //
				$latLongExists = q("SELECT description, latitude, longitude FROM locations WHERE latitude = '".$latitude."' AND longitude = '".$longitude."' AND company_id = '".$companyID."' LIMIT 1");
				if(is_array($latLongExists)) {
					$data = "Coordinates (".$latLongExists[0][1].", ".$latLongExists[0][2].") already exists";
				}else {
					$nameExists = q("SELECT description, latitude, longitude FROM locations WHERE description = '".$description."' AND company_id = '".$companyID."' LIMIT 1");
					if(is_array($nameExists)) {
						$data = "Location '".$nameExists[0][0]."' already exists";
					}else {
						$date = date("Y-m-d H:i:s");
						$user = $_SESSION["email"];
						$insert = q("INSERT INTO locations (company_id, description, latitude, longitude, createdBy, dateCreated, updatedBy, dateUpdated)
							VALUES (
								'".$companyID."',
								'".$description."',
								'".$latitude."',
								'".$longitude."',
								'".$user."',
								'".$date."',
								'".$user."',
								'".$date."'
						)");
						$logs = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id)
							VALUES (
								'".$description." created',
								'insert',
								'locations',
								'".$user."',
								'".date("Y-m-d")."',
								'".date("H:i:s")."',
								'".$companyID."'
						)");
						$data['msg'] = 'added';
						$data['description'] = $description;
						$data['added_location_id'] = q("SELECT id FROM locations WHERE company_id = '".$companyID."' AND latitude = '".$latitude."' AND longitude = '".$longitude."' LIMIT 1");
					}
				}
			}

            if (is_null($data))	$data = "";
            echo json_encode($data);

            break;
        }

		case "toggleGPSEntryType": {
            unset($data);
            $change_type = addslashes(strip_tags($_POST["change_type"]));
            $gps_entry_id = addslashes(strip_tags($_POST["gps_entry_id"]));

			$change_type = ($change_type == "Business") ? "Business" : "Private";

			if($gps_entry_id == "All") {
				$rowIDs =  addslashes(strip_tags($_POST["rowIDs"]));
				if($rowIDs != "") {
					$data = q("UPDATE gps_trip_data SET designation = '".$change_type."' WHERE user_id = '".$_SESSION['user_id']."' AND id IN (".$rowIDs.")");
				}
			}else if($gps_entry_id > 0) {
				$data = q("UPDATE gps_trip_data SET designation = '".$change_type."' WHERE user_id = '".$_SESSION['user_id']."' AND id = '".$gps_entry_id."' ");
			}

			if (is_null($data))	$data = "";
            echo json_encode($data);

            break;
		}

        //////////////////////////////////////////////////
        //  DEFAULT FUNCTION
        //////////////////////////////////////////////////
        default:	{
            echo json_encode("ERROR: Ajax.php->DEFAULT was reached. Fix function reference");
            break;
        }
    }
?>
