<?php
    session_start();

    include("_db.php");

    $q = addslashes($_GET["q"]);
    $projectStatus = addslashes($_GET["status"]);
    $projectType = addslashes($_GET["type"]);
    $completed = addslashes($_GET["completed"]);

    if ($completed == "completed")
        $completed = "1";
    else
        $completed = "0";

    if ($projectStatus == "all") {
        if ($projectType == "invoicable") {
            //  nProjects                   = Number of Contacts
            $nProjects                      = q("SELECT COUNT(id) FROM Project WHERE name LIKE '%$q%' AND completed = '$completed' ".
                                                                        "AND total_budget > 0 AND company_id = '".$_SESSION["company_id"]."'");
            $projects                       = q("SELECT id, name FROM Project WHERE name LIKE '%$q%' AND completed = '$completed' ".
                                                                            "AND total_budget > 0 AND company_id = '".$_SESSION["company_id"]."' ORDER BY name");
        }
        else if ($projectType == "non_invoicable") {
            //  nProjects                   = Number of Contacts
            $nProjects                      = q("SELECT COUNT(id) FROM Project WHERE name LIKE '%$q%' AND completed = '$completed' ".
                                                                            "AND total_budget = '' AND company_id = '".$_SESSION["company_id"]."'");
            $projects                       = q("SELECT id, name FROM Project WHERE name LIKE '%$q%' AND completed = '$completed' ".
                                                                            "AND total_budget = '' AND company_id = '".$_SESSION["company_id"]."' ORDER BY name");
        }
        else {
            //  nProjects                   = Number of Contacts
            $nProjects                      = q("SELECT COUNT(id) FROM Project WHERE name LIKE '%$q%' AND completed = '$completed' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."'");
            $projects                       = q("SELECT id, name FROM Project WHERE name LIKE '%$q%' AND completed = '$completed' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."' ORDER BY name");
        }
    }
    else {
        if ($projectType == "invoicable") {
            //  nProjects                   = Number of Contacts
            $nProjects                      = q("SELECT COUNT(id) FROM Project WHERE name LIKE '%$q%' AND completed = '$completed' ".
                                                                            "AND total_budget > 0 AND status = '$projectStatus' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."'");
            $projects                       = q("SELECT id, name FROM Project WHERE name LIKE '%$q%' AND completed = '$completed' ".
                                                                            "AND total_budget > 0 AND status = '$projectStatus' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."' ORDER BY name");
        }
        else if ($projectType == "non_invoicable") {
            //  nProjects                   = Number of Contacts
            $nProjects                      = q("SELECT COUNT(id) FROM Project WHERE name LIKE '%$q%' AND completed = '$completed' ".
                                                                            "AND total_budget = '' AND status = '$projectStatus' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."'");
            $projects                       = q("SELECT id, name FROM Project WHERE name LIKE '%$q%' AND completed = '$completed' ".
                                                                            "AND total_budget = '' AND status = '$projectStatus' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."' ORDER BY name");
        }
        else {
            //  nProjects                   = Number of Contacts
            $nProjects                      = q("SELECT COUNT(id) FROM Project WHERE name LIKE '%$q%' AND completed = '$completed' ".
                                                                            "AND status = '$projectStatus' AND company_id = '".$_SESSION["company_id"]."'");
            $projects                       = q("SELECT id, name FROM Project WHERE name LIKE '%$q%' AND completed = '$completed' ".
                                                                            "AND status = '$projectStatus' AND company_id = '".$_SESSION["company_id"]."' ORDER BY name");
        }
    }

    echo "<table cellpadding='0' cellspacing='0'>";
        //  <!--  Headings   -->
        echo "<tr>";
            echo "<td align='center' style='color:#FFFFFF;".$top.$left.$right.$bottom.$background."'>";
                echo "<strong>";
                    echo "&nbsp;Project Name&nbsp;";
                echo "</strong>";
            echo "</td>";
            echo "<td align='center' style='color:#FFFFFF;".$top.$left.$right.$bottom.$background."'>";
                echo "<strong>";
                    if ($completed == "completed")
                    echo "&nbsp;Re-open Project&nbsp;";
                    else
                    echo "&nbsp;Complete Project&nbsp;";
                echo "</strong>";
            echo "</td>";
        echo "</tr>";

    if ($nProjects > 1)
    {
                                    foreach ($projects as $project)
                                    {
                                        echo "<tr>";
                                            echo "<td align='left' style='".$left.$bottom."'>";
                                                echo "&nbsp;<a href='project_edit.php?id=".$project[0]."' style='".$link."'>".$project[1]."</a>&nbsp;";
                                            echo "</td>";
                                            //  Complete Button
                                            echo "<td align='left' style='".$left.$right.$bottom."'>";
                                                echo "<input name='btnComplete' onClick=\"complete_confirmation(".$project[0].", 1);\" type='button' ".
                                                    "value='Complete Project'>";
                                            echo "</td>";
                                        echo "</tr>";
                                    }
                                }
                                else if ($nProjects == 1)
                                {
                                    echo "<tr>";
                                        echo "<td align='left' style='".$left.$bottom."'>";
                                            echo "&nbsp;<a href='project_edit.php?id=".$projects[0][0]."' style='".$link."'>".$projects[0][1]."</a>&nbsp;";
                                        echo "</td>";
                                        //  Complete Button
                                        echo "<td align='left' style='".$left.$right.$bottom."'>";
                                            echo "<input name='btnComplete' onClick=\"complete_confirmation(".$projects[0][0].", 1);\" type='button' ".
                                                "value='Complete Project'>";
                                        echo "</td>";
                                    echo "</tr>";
                                }
                                else
                                {
                                    echo "<tr>";
                                        echo "<td align='center' colspan='2' style='".$left.$bottom.$right."'>";
                                            if ($alphabet == "")
                                                echo "&nbsp;No projects available&nbsp;";
                                            else
                                                echo "&nbsp;No projects available under $alphabet&nbsp;";
                                        echo "</td>";
                                    echo "</tr>";
                                }
    echo "</table>";

    include("_dbclose.php");
?>