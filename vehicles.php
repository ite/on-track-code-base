<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

	if (!hasAccess("VEH_MANAGE"))
		header("Location: noaccess.php");


    $display_link                                                       = "vehicles.php?alphabet";

    //  Set Alphabet Letter
    $alphabet                                                           = "";
    $alphabet                                                           = $_GET["alphabet"];

    //  If Delete Vehicle Was Clicked
    if (isset($_POST["deleteBox"]) && $_POST["deleteBox"] === "1")
    {
	$id                                                             = $_POST["idBox"];
        $delete                                                         = $_POST["delete"];
        $vehicle_info                                                   = q("SELECT type FROM Vehicle WHERE id = '$id'");
        $setStatus                                                      = q("UPDATE Vehicle SET deleted = '$delete' WHERE id = '$id'");

        if ($setStatus) {
            $time                                                       = date("H:i:s");

            $logs                                                       = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('".$vehicle_info." deleted', 'Update', 'Vehicle', '".$_SESSION["email"]."', ".
                                                                            "'$today', '$time', '".$_SESSION["company_id"]."')");

            $errorMessage                                               = "Vehicle Deleted/Undeleted Successfully...";
        }
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0", "vehicles", "vehicles");

    if ($errorMessage != "")
    {
        echo "<div style='width:100%; text-align:center'><strong><font class='on-validate-error'>$errorMessage</font></strong></div>";
        echo "<br/>";
    }
?>
<script language="JavaScript">
    function deleted(id, del) {
        document.forms["vehicles"].idBox.value                          = id;
        document.forms["vehicles"].delete.value                         = del;
        document.forms["vehicles"].deleteBox.value                      = 1;
        document.forms["vehicles"].submit();
    }
</script>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata">
                <form action="" method="post" name="vehicles">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>
                                    Vehicle List
                                </h6>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <br/>
                            </td>
                        </tr>
                    </table>
                    <?php
                        echo "<br/>";
                        echo "| <a href='$display_link='>View All Vehicles</a> |";
                        echo "<br/><br/>";
                        echo "<a href='$display_link=A'>A</a> | <a href='$display_link=B'>B</a> | ".
                            "<a href='$display_link=C'>C</a> | <a href='$display_link=D'>D</a> | ".
                            "<a href='$display_link=E'>E</a> | <a href='$display_link=F'>F</a> | ".
                            "<a href='$display_link=G'>G</a> | <a href='$display_link=H'>H</a> | ".
                            "<a href='$display_link=I'>I</a> | <a href='$display_link=J'>J</a> | ".
                            "<a href='$display_link=K'>K</a> | <a href='$display_link=L'>L</a> | ".
                            "<a href='$display_link=M'>M</a> | <a href='$display_link=N'>N</a> | ".
                            "<a href='$display_link=O'>O</a> | <a href='$display_link=P'>P</a> | ".
                            "<a href='$display_link=Q'>Q</a> | <a href='$display_link=R'>R</a> | ".
                            "<a href='$display_link=S'>S</a> | <a href='$display_link=T'>T</a> | ".
                            "<a href='$display_link=U'>U</a> | <a href='$display_link=V'>V</a> | ".
                            "<a href='$display_link=W'>W</a> | <a href='$display_link=X'>X</a> | ".
                            "<a href='$display_link=Y'>Y</a> | <a href='$display_link=Z'>Z</a>";
                    ?>
                    <br/><br/>
                    <input name="btnAdd" onClick="location.href='vehicle_add.php';" tabindex="1" type="button" value="Add New Vehicle">
                    <br/>
                    <input name="btnEdit" onClick="location.href='vehicle_edit.php';" tabindex="2" type="button" value="Edit Vehicle(s)">
                    <br/><br/>
                    <table class="on-table-center on-table" width="40%">
                        <!--  Table Headings   -->
                        <tr>
                            <th width="75%">
                                    Vehicle Name
                            </th>
                             <th>
                                 <?php echo "Vehicle Rate <i>(".$_SESSION["currency"].")</i>"; ?>
                            </th>
                            <th>
                                Delete/Undelete
                            </th>
                        </tr>
                        <!--  Table Information   -->
                        <?php
                                //  nVehicles                           = Number of Vehicles
                                $nVehicles                              = q("SELECT COUNT(id) FROM Vehicle WHERE type LIKE '$alphabet%' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."'");
                                $vehicles                               = q("SELECT id, type, rate, deleted FROM Vehicle WHERE type LIKE '$alphabet%' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."' ORDER BY type");

                            if ($nVehicles > 1)
                            {
                                foreach ($vehicles as $vehicle)
                                {
                                    echo "<tr>
                                                <td>".$vehicle[1]."</td>
                                                <td class='rightdata'>".number_format($vehicle[2], 2, ".", "")."</td>";
                                                //  Delete/Undelete Button
                                      echo "<td align='center'>";
                                                if ($vehicle[3] === "0")
                                                    echo "<input name='btnDelete' onClick=\"deleted(".$vehicle[0].", '1');\" type='button' value='Delete Vehicle    '>";
                                                else
                                                    echo "<input name='btnDelete' onClick=\"deleted(".$vehicle[0].", '0');\" type='button' value='Undelete Vehicle'>";
                                            echo "</td>";
                                    echo "</tr>";
                                }
                            }
                            else if ($nVehicles == 1)
                            {
                                echo "<tr>
                                            <td>".$vehicles[0][1]."</td>
                                            <td class='rightdata'>".number_format($vehicles[0][2], 2, ".", "")."</td>";
                                        //  Delete/Undelete Button
                                        echo "<td class='centerdata'>";
                                            if ($vehicles[0][3] === "0")
                                                echo "<input name='btnDelete' onClick=\"deleted(".$vehicles[0][0].", '1');\" type='button' value='Delete Vehicle    '>";
                                            else
                                                echo "<input name='btnDelete' onClick=\"deleted(".$vehicles[0][0].", '0');\" type='button' value='Undelete Vehicle'>";
                                        echo "</td>";
                                echo "</tr>";
                            }
                            else
                            {
                                echo "<tr>
                                            <td class='centerdata' colspan='100%'>";
                                        if ($alphabet == "")
                                            echo "No vehicles available";
                                        else
                                            echo "No vehicles available under $alphabet";
                                    echo "</td>";
                                echo "</tr>";
                            }
                            echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        ?>
                    </table>
                    <br/>
                    <input name="btnEdit" onClick="location.href='vehicle_edit.php';" tabindex="3" type="button" value="Edit Vehicle(s)">
                    <br/>
                    <input name="btnAdd" onClick="location.href='vehicle_add.php';" tabindex="4" type="button" value="Add New Vehicle">
                    <input name="idBox" type="hidden" value="">
                    <input name="delete" type="hidden" value="">
                    <input name="deleteBox" type="hidden" value="">
                </form>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
