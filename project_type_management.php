<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

	if (!hasAccess("PROJTYPE_MANAGE"))
		header("Location: noaccess.php");

    $display_link                                                       = "project_type_management.php?alphabet";

    //  Set Alphabet Letter
    $alphabet                                                           = "";
    $alphabet                                                           = $_GET["alphabet"];

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("1", "project_types");
?>
<script language="JavaScript">
    jQuery(function()    {
        jQuery(".status").change(function() {
            var fieldID = jQuery(this).attr("id");
            var cls = (jQuery(this).attr("checked")) ? "active" : "inactive";

            jQuery("#link"+fieldID).removeClass("active").removeClass("inactive").addClass(cls);

            var active = (jQuery(this).attr("checked")) ? "1" : "0";
            var query = "UPDATE ProjectTypes SET active = '"+active+"' WHERE id = '"+fieldID+"'";
            jQuery.post("_ajax.php", {func: "query_direct", query: query}, function(data)	{});
        });
    });
</script>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="project_type_management">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Project Type List</h6>
                            </td>
                        </tr>
                    </table>
                    <?php
                        echo "<br/>";
                        echo "| <a href='$display_link='>View All Project Types</a> |";
                        echo "<br/><br/>";
                        echo "<a href='$display_link=A'>A</a> | <a href='$display_link=B'>B</a> | ".
                            "<a href='$display_link=C'>C</a> | <a href='$display_link=D'>D</a> | ".
                            "<a href='$display_link=E'>E</a> | <a href='$display_link=F'>F</a> | ".
                            "<a href='$display_link=G'>G</a> | <a href='$display_link=H'>H</a> | ".
                            "<a href='$display_link=I'>I</a> | <a href='$display_link=J'>J</a> | ".
                            "<a href='$display_link=K'>K</a> | <a href='$display_link=L'>L</a> | ".
                            "<a href='$display_link=M'>M</a> | <a href='$display_link=N'>N</a> | ".
                            "<a href='$display_link=O'>O</a> | <a href='$display_link=P'>P</a> | ".
                            "<a href='$display_link=Q'>Q</a> | <a href='$display_link=R'>R</a> | ".
                            "<a href='$display_link=S'>S</a> | <a href='$display_link=T'>T</a> | ".
                            "<a href='$display_link=U'>U</a> | <a href='$display_link=V'>V</a> | ".
                            "<a href='$display_link=W'>W</a> | <a href='$display_link=X'>X</a> | ".
                            "<a href='$display_link=Y'>Y</a> | <a href='$display_link=Z'>Z</a>";
                    ?>
                    <br/><br/>
                    <input name="btnAdd" onClick="location.href='project_type_add.php';" tabindex="1" type="button" value="Add New Project Type">
                    <br/><br/>
                    <!--  Headings   -->
                    <table class="on-table-center on-table">
                        <tr>
                            <th>
                                    Project Type
                            </th>
                            <th>
                                    Active
                            </th>
                        </tr>
                        <!--  Table Information   -->
                        <?php
                            $projectTypes = q("SELECT id, type, active FROM ProjectTypes WHERE company_id = '".$_SESSION["company_id"]."' AND type LIKE '$alphabet%' ORDER BY type");

                            if (is_array($projectTypes))        {
                                foreach ($projectTypes as $projectType) {
                                    echo "<tr>";
                                        echo "<td>";
                                            $class = ($projectType[2] == "1") ? "active" : "inactive";
                                            echo "<a id='link".$projectType[0]."' name='link".$projectType[0]."' class='".$class."' href='project_type_edit.php?id=".$projectType[0]."'>".$projectType[1]."</a>";
                                        echo "</td>";
                                        echo "<td class='centerdata'>";
                                            $checked = ($projectType[2] == "1") ? "checked" : "";
                                            echo "<input id='".$projectType[0]."' name='".$projectType[0]."' type='checkbox' class='status' $checked />";
                                        echo "</td>";
                                    echo "</tr>";
                                }
                            } else      {
                                echo "<tr>";
                                    echo "<td class='centerdata' colspan='2'>";
                                        if ($alphabet == "")    echo "nbsp;No project types available";
                                        else                    echo "No project types available under $alphabet";
                                    echo "</td>";
                                echo "</tr>";
                            }
                            echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        ?>
                    </table>
                    <br/>
                    <input name="btnAdd" onClick="location.href='project_type_add.php';" tabindex="1" type="button" value="Add New Project Type">
                </form>
            </td>
        </tr>
        <tr>
            <td class="centerdata">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
