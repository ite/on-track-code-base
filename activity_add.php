<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

	if (!hasAccess("ATYPE_MANAGE"))
		header("Location: noaccess.php");

    //  Insert Activity Type And Sub Activities Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")
    {
        $errorMessage                                                   = "";

        //  Get Information
        $activity_type_name                                             = addslashes(strip_tags($_POST["activity_type_name"]));

        //  Check If Activity Type Exists In Database
        $exist                                                          = q("SELECT id FROM ActivityTypes WHERE type = '$activity_type_name' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."'");

        if (!$exist)
        {
            //  Insert Activity Type
            $insert                                                     = q("INSERT INTO ActivityTypes (type, company_id) ".
                                                                            "VALUES ('$activity_type_name', '".$_SESSION["company_id"]."')");

            //  Insert Sub Activities
            if ($insert)
            {
                $parent_id                                              = q("SELECT id FROM ActivityTypes WHERE type = '$activity_type_name' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."'");

                //  Get Information
                for ($i = 1; $i <= 10; $i++)
                {
                    $activity_name                                      = addslashes(strip_tags($_POST["activity_name_$i"]));

                    if ($activity_name != "")
                        $insert                                         = q("INSERT INTO Activities (parent_id, name) VALUES ('$parent_id', '$activity_name')");
                }
            }

            if ($insert)
            {
                $time                                                   = date("H:i:s");

                $logs                                                   = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('$activity_type_name and activities created', 'Insert', 'Activities', ".
                                                                            "'".$_SESSION["email"]."', '$today', '$time', '".$_SESSION["company_id"]."')");

                header("Location: activity_type_management.php");
            }
        }
        else
            $errorMessage                                               = "Activity Type Already Exists";
    }

    if ($errorMessage != "")
        echo "<p align='center' style='padding:0px;'><strong><font class='on-validate-error'>$errorMessage</font></strong></p>";

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("1", "activity_types");
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    var cal                                                             = new CodeThatCalendar(caldef1);

    function check(box)
    {
        var valid                                                       = 1;

        //  Check That Project Type Is Entered
        if (document.forms["activity_add"].activity_type_name.value == "")
        {
            ShowLayer("activityTypeName", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("activityTypeName", "none");

        if (valid == 1)
        {
            document.forms["activity_add"].save.value                   = 1;
            document.forms["activity_add"].submit();
        }
    }
</script>
    <table width="100%">
        <tr height="380px">
            <td class='centerdata' valign="top">
                <form action="" method="post" name="activity_add">
                    <table width="100%">
                        <tr>
                            <td class='centerdata'>
                                <h6>Add New Activity Type</h6>
                            </td>
                        </tr>
                        <tr>
                            <td class='centerdata'>
                                <br/>
                            </td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                    Activity Type
                            </td>
                            <td class="on-description-left" width="50%">
                                    &nbsp;&nbsp;Activity
                            </td>
                        </tr>
                        <tr>
                            <td class="rightdata" valign="top" width="50%">
                                <input class="on-field" name="activity_type_name" tabindex="1" type="text">
                                <div id="activityTypeName" style="display: none;"><font style="padding-right:30px;" class="on-validate-error">* Activity Type must be entered</font></div>
                            </td>
                            <td width="50%">
                                <?php
                                    for ($i = 1; $i <= 10; $i++)
                                        echo "<input class='on-field' name='activity_name_".$i."' tabindex='".($i + 1)."' type='text'><br/>";
                                ?>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <input name="btnAdd" onClick="check();" tabindex="12" type="button" value="Add Activity">
                    <input method="post" name="save" type="hidden" value="0" />
                </form>
            </td>
        </tr>
        <tr>
            <td class='centerdata'>
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
