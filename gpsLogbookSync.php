<?php
	$logbook_username = "OnTrack";
	$logbook_password = "8EAD4425-4181-4FC8-8F2F-78CFCE8A07FC";

	$companies = q("SELECT id,name FROM Company WHERE gpsLogbook = '1' AND locked = '0'");

	if (is_array($companies))	{
		foreach ($companies as $company)	{
			$users = q("SELECT e.id,e.email ".
								"FROM (Employee AS e ".
									"INNER JOIN Company_Users AS cu ON e.id = cu.user_id) ".
								"WHERE cu.company_id = '".$company[0]."' ".
									"AND e.email != 'admin' ".
									"AND e.deleted = '0'");

			if (is_array($users))	{
				foreach ($users as $user)	{
					$logbook_user_id = "";

					// Get Logbook User ID
					$host = "https://api.gpslogbook.co.za/otapi/User/GetId/?email=".$user[1];
					$CURL = curl_init();
					curl_setopt($CURL, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($CURL, CURLOPT_SSL_VERIFYPEER, false);
					//curl_setopt($CURL, CURLOPT_CERTINFO, false);
					curl_setopt($CURL, CURLOPT_URL, $host);
					curl_setopt($CURL, CURLOPT_USERPWD, $logbook_username . ":" . $logbook_password);
					$CURL_return_data = curl_exec($CURL);
					$header = curl_getinfo($CURL);
					if ($CURL_return_data == false)	{
						curl_close($CURL);
					} else	{
						// Display Returned Data
						curl_close($CURL);
						$logbook_user_id = json_decode($CURL_return_data);
						$logbook_user_id = $logbook_user_id->User->UserId;
					}

					// Get Logbook User Data
					unset($CURL_return_data);

					if ($logbook_user_id != "" && is_numeric($logbook_user_id))	{
						// Ensure only non-existent/newest entries are imported
						$since_date = "";

						$last_trip_end = q("SELECT MAX(endDateLocal) FROM gps_trip_data WHERE user_id = '".$user[0]."' LIMIT 1");

						if (strlen($last_trip_end) == 19) $since_date = "since/".date("Y/m/d/H/i/s", strtotime($last_trip_end));

						if ($company[1] == "OMI Solutions" && $since_date == "") {
							$since_date = "since/2020/01/01/00/00/00";
						}

						$host = "https://api.gpslogbook.co.za/otapi/".$logbook_user_id."/trips/".$since_date;
						$CURL = curl_init();
						curl_setopt($CURL, CURLOPT_RETURNTRANSFER, true);
						curl_setopt($CURL, CURLOPT_SSL_VERIFYPEER, false);
						//curl_setopt($CURL, CURLOPT_CERTINFO, false);
						curl_setopt($CURL, CURLOPT_URL, $host);
						curl_setopt($CURL, CURLOPT_USERPWD, $logbook_username . ":" . $logbook_password);
						$CURL_return_data = curl_exec($CURL);
						$header = curl_getinfo($CURL);

						if ($CURL_return_data == false)	{
							curl_close($CURL);
						} else	{
							curl_close($CURL);
							$trips = $CURL_return_data;
						}

						$trips = json_decode($trips);

						if (count($trips->Devices) > 0)	{
							// Insert data to On-Track DB
							$date = date("Y-m-d H:i:s");

							foreach($trips->Devices[0]->Trips as $tripInfo)	{
								if ($company[1] == "OMI Solutions") {
									if ($tripInfo->Designation == "B") {
										$insert = q("INSERT INTO gps_trip_data(user_id, totalDistanceMetres, totalTimeSeconds, maxSpeedKmH, avgSpeedKmH, startDateUtc, startDateLocal, endDateLocal, startLat, startLong, endLat, endLong, comment, dateImported) VALUES (
											'".$user_id."',
											'".$tripInfo->TotalDistanceMetres."',
											'".$tripInfo->TotalTimeSeconds."',
											'".$tripInfo->MaxSpeedKmH."',
											'".$tripInfo->AvgSpeedKmH."',
											'".$tripInfo->StartDateUtc."',
											'".date("Y-m-d H:i:s", strtotime($tripInfo->StartDateLocal))."',
											'".date("Y-m-d H:i:s", strtotime($tripInfo->EndDateLocal))."',
											'".$tripInfo->StartLat."',
											'".$tripInfo->StartLong."',
											'".$tripInfo->EndLat."',
											'".$tripInfo->EndLong."',
											'".$tripInfo->Comment."',
											'".$date."'
										)");
									}
								} else {
									$insert = q("INSERT INTO gps_trip_data(user_id, totalDistanceMetres, totalTimeSeconds, maxSpeedKmH, avgSpeedKmH, startDateUtc, startDateLocal, endDateLocal, startLat, startLong, endLat, endLong, comment, dateImported) VALUES (
										'".$user_id."',
										'".$tripInfo->TotalDistanceMetres."',
										'".$tripInfo->TotalTimeSeconds."',
										'".$tripInfo->MaxSpeedKmH."',
										'".$tripInfo->AvgSpeedKmH."',
										'".$tripInfo->StartDateUtc."',
										'".date("Y-m-d H:i:s", strtotime($tripInfo->StartDateLocal))."',
										'".date("Y-m-d H:i:s", strtotime($tripInfo->EndDateLocal))."',
										'".$tripInfo->StartLat."',
										'".$tripInfo->StartLong."',
										'".$tripInfo->EndLat."',
										'".$tripInfo->EndLong."',
										'".$tripInfo->Comment."',
										'".$date."'
									)");
					}
							}
						}
					}
				}
			}
		}
	}
?>
