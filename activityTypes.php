<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    if (!hasAccess("ATYPE_MANAGE"))
        header("Location: noaccess.php");

    $index = 1;

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0");
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    var activityTypes_LOAD;
    var project = "<?php echo $_GET["project"]; ?>";

    jQuery(function()    {
        activityTypes_LOAD = function()  {
            jQuery.post("_ajax.php", {func: "get_activityTypes"}, function(data)	{
                data = json_decode(data);

                jQuery("#tableContent tbody>tr").not(":first").not(":last").remove();
                var lstRow = jQuery("#tableContent tbody>tr:last");

                jQuery.each(data,function(i, v)	{
                    var n = lstRow.clone(true);

                    n.children().eq(0).children().eq(0).text((v[1] != null) ? v[1] : "");
                    n.children().eq(0).children().eq(0).attr("id", (v[0] != null) ? v[0] : "");
                    n.children().eq(0).children().eq(0).attr("name", (v[0] != null) ? v[0] : "");

                    var subs = "";

                    jQuery.each(v[2],function(j, w)	{
                        var cls = (w[2] == 1) ? "active" : "inactive";
                        var val = (w[1] != null) ? w[1] : "";

                        if (w[1] == "-")        displayBool = false;
                        else                    displayBool = true;

                        subs += "<a class='"+cls+"'>"+val+"</a><br/>";
                    });

                    n.children().eq(1).html(subs);

                    for (var a = 0; a < 4; a++) {
                        var subs = "";

                        jQuery.each(v[2],function(j, w)	{
                            var val = (w[3 + a] != null) ? w[3 + a] : "";

                            subs += ""+val+"<br/>";
                        });

                        n.children().eq(2 + a).html(subs);
                    }

                    n.insertBefore(lstRow);
                });

                if (data != "")         lstRow.remove();
            });
        };

        activityTypes_LOAD();

        jQuery(".btnAdd").click(function() {
            jQuery("#form").attr("action", "activityTypesAddEdit.php");
            jQuery("#form").submit();
        });

        jQuery(".btnEdit").click(function() {
            jQuery("#id").val(jQuery(this).attr("id"));
            jQuery("#form").attr("action", "activityTypesAddEdit.php");
            jQuery("#form").submit();
        });
    });
</script>
<table width="100%">
    <tr height="380px">
        <td class="centerdata">
            <form id="form" name="form" action="" method="post">
                <table width="100%">
                    <tr>
                        <td class="centerdata">
                            <h6>Activity Type List</h6>
                        </td>
                    </tr>
                </table>
                <br/>
                    <div class="on-20px">
                    <table id="tableContent" name="tableContent" class="on-table-center on-table">
                        <tr>
                            <th>Activity Type</th>
                            <th>Activity</th>
                            <th>Date Created</th>
                            <th>Created By</th>
                            <th>Date Updated</th>
                            <th>Updated By</th>
                        </tr>
                        <!--  Table Information   -->
                        <?php
                            echo "<tr>";
                                echo "<td style='white-space:nowrap'>";
                                    echo "<a id='' name='' class='btnEdit' tabindex='".$index++."'></a>";
                                echo "</td>";
                                echo "<td style='white-space:nowrap'></td>";
                                echo "<td style='white-space:nowrap'></td>";
                                echo "<td></td>";
                                echo "<td style='white-space:nowrap'></td>";
                                echo "<td></td>";
                            echo "</tr>";
                            echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        ?>
                    </table>
                    </div>
                    <br/>
                    <input id="btnAdd" name="btnAdd" class="btnAdd" tabindex="<?php echo $index++; ?>" type="button" value="Add New">
                <input id="id" name="id" type="hidden" value="">
            </form>
        </td>
    </tr>
    <tr>
        <td>
            <br/>
        </td>
    </tr>
</table>
<?php
    //  Print Footer
    print_footer();
?>
