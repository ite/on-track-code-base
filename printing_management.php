<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

	if (!hasAccess("PRINTING_MANAGE"))
		header("Location: noaccess.php");

    $display_link                                                       = "printing_management.php?alphabet";

    //  Set Alphabet Letter
    $alphabet                                                           = "";
    $alphabet                                                           = $_GET["alphabet"];

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("1", "activity_types");
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="activity_type_management">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Printing List</h6>
                            </td>
                        </tr>
                    </table>
                    <?php
                        echo "<br/>";
                        echo "| <a href='$display_link='>View All Printing Types</a> |";
                        echo "<br/><br/>";
                        echo "<a href='$display_link=A'>A</a> | <a href='$display_link=B'>B</a> | ".
                            "<a href='$display_link=C'>C</a> | <a href='$display_link=D'>D</a> | ".
                            "<a href='$display_link=E'>E</a> | <a href='$display_link=F'>F</a> | ".
                            "<a href='$display_link=G'>G</a> | <a href='$display_link=H'>H</a> | ".
                            "<a href='$display_link=I'>I</a> | <a href='$display_link=J'>J</a> | ".
                            "<a href='$display_link=K'>K</a> | <a href='$display_link=L'>L</a> | ".
                            "<a href='$display_link=M'>M</a> | <a href='$display_link=N'>N</a> | ".
                            "<a href='$display_link=O'>O</a> | <a href='$display_link=P'>P</a> | ".
                            "<a href='$display_link=Q'>Q</a> | <a href='$display_link=R'>R</a> | ".
                            "<a href='$display_link=S'>S</a> | <a href='$display_link=T'>T</a> | ".
                            "<a href='$display_link=U'>U</a> | <a href='$display_link=V'>V</a> | ".
                            "<a href='$display_link=W'>W</a> | <a href='$display_link=X'>X</a> | ".
                            "<a href='$display_link=Y'>Y</a> | <a href='$display_link=Z'>Z</a>";
                    ?>
                    <br/><br/>
                    <input name="btnAdd" onClick="location.href='printing_add.php';" tabindex="1" type="button" value="Add New Printing Type">
                    <br/><br/>
                    <!--  Headings   -->
                    <table class="on-table-center on-table">
                        <tr>
                            <th class="centerdata">Printing Type</th>
                            <th class="centerdata">Colour</th>
                            <th class="centerdata">Cost</th>
                        </tr>
                        <!--  Table Information   -->
                        <?php
                            $disbursementTypes                          = q("SELECT id, type FROM DisbursementTypes WHERE company_id = '".$_SESSION["company_id"]."' ".
                                                                            "AND type LIKE '$alphabet%' ORDER BY type");

                            if (is_array($disbursementTypes)) {
                                foreach ($disbursementTypes as $disbursementType) {
                                    $disbursements                      = q("SELECT id, name, cost FROM Disbursements WHERE parent_id = '".$disbursementType[0]."' ".
                                                                            "ORDER BY name");

                                    echo "<tr>";
                                        echo "<td>";
                                            echo "<a href='printing_edit.php?id=".$disbursementType[0]."'>".$disbursementType[1]."</a>";
                                        echo "</td>";
                                        if (is_array($disbursements)) {
                                            echo "<td>";
                                                echo "<table width='100%'>";
                                                    foreach ($disbursements as $disbursement) {
                                                        echo "<tr>";
                                                            echo "<td class='noborder'>";
                                                                echo stripslashes($disbursement[1]);
                                                            echo "</td>";
                                                        echo "</tr>";
                                                    }
                                                echo "</table>";
                                            echo "</td>";
                                            echo "<td>";
                                                echo "<table width='100%'>";
                                                    foreach ($disbursements as $disbursement) {
                                                        echo "<tr>";
                                                            echo "<td class='noborder rightdata'>";
                                                                echo number_format($disbursement[2], 2, ".", "");
                                                            echo "</td>";
                                                        echo "</tr>";
                                                    }
                                                echo "</table>";
                                            echo "</td>";
                                        }
                                        else {
                                            echo "<td>";
                                                echo "-";
                                            echo "</td>";
                                        }
                                    echo "</tr>";
                                }
                            }
                            else {
                                echo "<tr>";
                                    echo "<td class='centerdata' colspan='3'>";
                                        if ($alphabet == "")
                                            echo "No printing types available";
                                        else
                                            echo "No printing types available under $alphabet";
                                    echo "</td>";
                                echo "</tr>";
                            }
                            echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        ?>
                    </table>
                    <br/>
                    <input name="btnAdd" onClick="location.href='printing_add.php';" tabindex="1" type="button" value="Add New Printing Type">
                </form>
            </td>
        </tr>
        <tr>
            <td class="centerdata">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
