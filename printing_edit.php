<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

	if (!hasAccess("PRINTING_MANAGE"))
		header("Location: noaccess.php");

    //  Update Disbursement Type And Sub Disbursement Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")
    {
        $errorMessage                                                   = "";

        //  Get Information
        $id                                                             = $_GET["id"];
        $disbursement_type_info_old                                     = q("SELECT type FROM DisbursementTypes WHERE id = '$id'");
        $disbursement_type_info_new                                     = addslashes(strip_tags($_POST["disbursement_type_name"]));

        $disbursements                                                  = q("SELECT id, name, cost FROM Disbursements WHERE parent_id = '$id' ORDER BY name");

        //  Check If Activity Type Exists In Database
        if ($disbursement_type_info_old != $disbursement_type_info_new) {
            $exist                                                      = q("SELECT id FROM DisbursementTypes WHERE type = '$disbursement_type_info_new' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."'");

            $do_update                                                  = true;
        }
        else {
            $exist                                                      = false;

            $do_update                                                  = false;
        }

        if (!$exist) {
            //  Update Disbursement Type If Necessary
            if ($do_update)
                $update                                                 = q("UPDATE DisbursementTypes SET type = '$disbursement_type_info_new' WHERE id = '$id'");

            //  Sub Disbursement
            if (is_array($disbursements)) {
                foreach ($disbursements as $disbursement) {
                    $disbursement_name_old                              = $disbursement[1];
                    $disbursement_cost_old                              = $disbursement[2];
                    $disbursement_name_new                              = addslashes(strip_tags($_POST["disbursement_name_$disbursement[0]"]));
                    $disbursement_cost_new                              = addslashes(strip_tags($_POST["disbursement_cost_$disbursement[0]"]));

                    if ($disbursement_name_old != $disbursement_name_new && $disbursement_name_new != "")
                        $update                                         = q("UPDATE Disbursements SET name = '$disbursement_name_new' WHERE id = '$disbursement[0]'");

                    if ($disbursement_cost_old != $disbursement_cost_new && $disbursement_cost_new != "")
                        $update                                         = q("UPDATE Disbursements SET cost = '$disbursement_cost_new' WHERE id = '$disbursement[0]'");
                }

                //  New Sub Disbursement
                $disbursement_name                                      = addslashes(strip_tags($_POST["disbursement_name_new"]));
                $disbursement_cost                                      = addslashes(strip_tags($_POST["disbursement_cost_new"]));

                if ($disbursement_name != "" && $disbursement_cost != "")
                    $insert                                             = q("INSERT INTO Disbursements (parent_id, name, cost) ".
                                                                            "VALUES ('$id', '$disbursement_name', '$disbursement_cost')");
            }
            else {
                for ($i = 1; $i <= 5; $i++) {
                    $disbursement_name                                  = addslashes(strip_tags($_POST["disbursement_name_$i"]));
                    $disbursement_cost                                  = addslashes(strip_tags($_POST["disbursement_cost_$i"]));

                    if ($disbursement_name != "" && $disbursement_cost != "")
                        $insert                                         = q("INSERT INTO Disbursements (parent_id, name, cost) ".
                                                                            "VALUES ('$id', '$disbursement_name', '$disbursement_cost')");
                }
            }

            if ($errorMessage == "") {
                $time                                                   = date("H:i:s");

                $logs                                                   = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('$disbursement_name_new type and printing updated', 'Update', 'Disbursement', ".
                                                                            "'".$_SESSION["email"]."', '$today', '$time', '".$_SESSION["company_id"]."')");

                header("Location: printing_management.php");
            }
        }
        else
            $errorMessage                                               = "Colour Type Already Exists";
    }

    if ($errorMessage != "")
        echo "<p align='center' style='padding:0px;'><strong><font class='on-validate-error'>$errorMessage</font></strong></p>";

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("1", "activity_types");
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    function check(box) {
        var valid                                                       = 1;

        //  Check That Project Type Is Entered
        if (document.forms["printing_edit"].disbursement_type_name.value == "") {
            ShowLayer("disbursementTypeName", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("disbursementTypeName", "none");

        if (valid == 1) {
            document.forms["printing_edit"].save.value                  = 1;
            document.forms["printing_edit"].submit();
        }
    }
</script>
<?php
    $id                                                                 = $_GET["id"];
    $disbursement_type_info                                             = q("SELECT type FROM DisbursementTypes WHERE id = '$id'");
    $disbursements                                                      = q("SELECT id, name, cost FROM Disbursements WHERE parent_id = '$id' ORDER BY name");
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="printing_edit">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Edit Printing Type</h6>
                            </td>
                        </tr>
                        <tr>
                            <td class="centerdata">
                                <br/>
                            </td>
                        </tr>
                    </table>
                    <table class="on-table-center">
                        <tr>
                            <td class="on-description-left">
                                    Printing Type
                            </td>
                            <td class="on-description-left">
                                Colour
                            </td>
                            <td class="on-description-left">
                                Cost
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="top">
                                <input class="on-field" name="disbursement_type_name" tabindex="1" type="text" value="<?php echo $disbursement_type_info; ?>">
                                <div id="disbursementTypeName" style="display: none;"><font class="on-validate-error">* Printing Type must be entered&nbsp;</font></div>
                            </td>
                            <td align="left" valign="top">
                                <?php
                                    $index                              = 1;

                                    if (is_array($disbursements)) {
                                        foreach ($disbursements as $disbursement)
                                            echo "<input class='on-field' name='disbursement_name_".$disbursement[0]."' tabindex='".($index++)."' type='text' value='".
                                                str_replace("'", "&#39;" , $disbursement[1])."'><br/>";

                                        echo "<input class='on-field' name='disbursement_name_new' tabindex='".($index++)."' type='text'><br/>";
                                    }
                                    else {
                                        for ($i = 1; $i <= 5; $i++)
                                            echo "<input class='on-field' name='disbursement_name_".$i."' tabindex='".($index++)."' type='text'><br/>";
                                    }
                                ?>
                            </td>
                            <td align="left" valign="top">
                                <?php
                                    $index                              = 1;

                                    if (is_array($disbursements)) {
                                        foreach ($disbursements as $disbursement)
                                            echo "<input class='on-field-date' name='disbursement_cost_".$disbursement[0]."' tabindex='".($index++)."' type='text' ".
                                                "style='text-align:right;' value='".$disbursement[2]."'><br/>";

                                        echo "<input class='on-field-date' name='disbursement_cost_new' tabindex='".($index++)."' type='text' style='text-align:right;'><br/>";
                                    }
                                    else {
                                        for ($i = 1; $i <= 5; $i++)
                                            echo "<input class='on-field-date' name='disbursement_cost_".$i."' tabindex='".($index++)."' type='text' style='text-align:right;'><br/>";
                                    }
                                ?>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <input name="btnAdd" onClick="check();" tabindex="<?php echo "".($index + 1); ?>" type="button" value="Update Printing">
                    <input method="post" name="save" type="hidden" value="0" />
                </form>
            </td>
        </tr>
        <tr>
            <td class="centerdata">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
