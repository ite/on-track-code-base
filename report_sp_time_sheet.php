<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("include/sajax.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");
        
    if (!hasAccess("REP_SP_TIME_SHEET"))
        header("Location: noaccess.php");
        
    // Class for table background color
    $colorClass = "OnTrack_#_";            

    ///////////////////////////
    //  Sajax
    function getCompanies($id) {
       return q("SELECT DISTINCT(c.id), c.name FROM (Company AS c INNER JOIN companiesOnTeam AS cot ON c.id = cot.company_id INNER JOIN Project AS p ON p.id =  cot.project_id) WHERE cot.project_id = '$id' ORDER BY c.name"); 
    }
    function getEmployees($id) {
        return q("SELECT id, CONCAT(lstname, ', ', frstname) FROM Employee WHERE company_id = '$id'");
    }
    function getAreas($id){
        return q("SELECT ar.id, ar.name FROM (areas AS ar INNER JOIN Project as p ON ar.pID = p.id) WHERE ar.active = '1' AND p.p_type='SP' AND ar.pID='".$id."' ORDER BY ar.name");
    } 
    function getActivityTypes($id) {
        return q("SELECT id, type FROM ActivityTypes WHERE pID = '$id' ORDER BY type");
    }
    function getActivities($id) {
        return q("SELECT id, name FROM Activities WHERE parent_id = '$id' ORDER BY name");
    }

    $sajax_request_type = "GET";
    sajax_init();    
    sajax_export("getCompanies");    
    sajax_export("getEmployees");    
    sajax_export("getAreas");
    sajax_export("getActivityTypes");
    sajax_export("getActivities");
    sajax_handle_client_request();
    ///////////////////////////

    //  Set Report Generated Status
    $generated = "0";
    
    // Excel export Variables
    $row1Head = "";
    $row2Head = "";
    $row3Head = "";
    $row4Head = "";
    $row = 0;
    $totalColCount = 0;
    
    GLOBAL $excelheadings;
    
    function printHeadings($val, $level, $cols){
        GLOBAL $excelheadings;
        if($cols == 6){               $excelheadings[$level][] = $val;for($i=1; $i<=$cols; $i++)$excelheadings[$level][] ="";
        }else  if($cols == 7){        $excelheadings[$level][] = $val;for($i=1; $i<=$cols; $i++)$excelheadings[$level][] ="";
        }else  if($cols == 8){        $excelheadings[$level][] = $val;for($i=1; $i<=$cols; $i++)$excelheadings[$level][] ="";
        }else  if($cols == 9){        $excelheadings[$level][] = $val;for($i=1; $i<=$cols; $i++)$excelheadings[$level][] ="";}
    }

    //  Create Display Information Function
    if (isset($_POST["save"]) && $_POST["save"] === "1") 
    {
        GLOBAL $excelheadings;
        $errorMessage = "";

        $reportOn = $_POST["reportOn"];

        // POST Values
        $project_id = $_POST["project"];
        $company_id = $_POST["company"];
        $area_id = $_POST["area"];
        $employee_id = $_POST["employee"];
        $activity_type_id = $_POST["activityType"];
        $activity_id = $_POST["activity"];
        $date_type = $_POST["dateType"];
        
        //  Get Dates
        if ($date_type == "currentwk") {    $date_from = currentWeekStart();    $date_to = $today;}
        else if ($date_type == "previouswk") {  $date_from = getDates(currentWeekStart(), -7);  $date_to = getDates(currentWeekStart(), -1);}
        else if ($date_type == "currentmnth") { $date_from = getMonthStart($today); $date_to = $today;}
        else if ($date_type == "previousmnth") { $date_from = getPreviousMonthStart();   $date_to = getMonthEnd(getPreviousMonthStart());}
        else if ($date_type == "projectLifetime")      {
            $date_fromT = q("SELECT MIN(date) FROM TimeSheet WHERE project_id = '".$project_id."'");
            $date_fromE = q("SELECT MIN(date) FROM ExpenseSheet WHERE project_id = '".$project_id."'");
            $date_from = ($date_fromT <= $date_fromE) ? $date_fromT : $date_fromE;
            $date_to = $today;
        }
        else if ($date_type == "custom") {$date_from = addslashes(strip_tags($_POST["date_from"])); $date_to = addslashes(strip_tags($_POST["date_to"]));}
        //  Timestamp
        if ($date_from === $date_to)
            $timestamp = "Time Period: ".$date_from;
        else
            $timestamp = "Time Period: ".$date_from." - ".$date_to;

        $table = ($reportOn == "approved") ? "ApprovedTime" : "TimeSheet";
        $where = ($reportOn == "approved") ? "ts.status2 = '2' AND " : "";

        //////////   Get Data  ////////////////
        $tableHeads = "<tr>";
        $headings = "";
        $select = "";
        $from .= "Project AS p 
                            INNER JOIN $table AS ts ON ts.project_id = p.id ";        
        
        // Project Info
        $projInfo = q("SELECT name FROM Project WHERE id='".$project_id."' ");
        $headings .= "<tr><td class='on-table-clear' colspan='100%'><a>Shared Project: ".$projInfo."<a></td></tr>";
        $row1Head = "Shared Project: ".$projInfo;
        $where .= "ts.projectType = 'SP' AND ts.project_id = '".$project_id."' AND ts.date >= '$date_from' AND ts.date <= '$date_to' ";        
        
        // Get Areas
        $hasAreas = 0;
        if($area_id != "" && $area_id != 'undefined'){
            $select .= " ar.name, ";
            $from .= "LEFT JOIN areas AS ar ON ar.id = ts.area_id ";
            if($_POST["area"] != "null"){
                $where  .= "AND ts.area_id = '".$area_id."' AND ar.id = '".$area_id."' ";
            }else{
//                $where  .= "AND ar.id = ts.area_id ";
            }
            $tableHeads .= "<th>Area</th>";
            $hasAreas = 1;
        }
        
        // Order By
         if($hasAreas)
            $order = "ar.name, ts.date, ts.on_date, ts.on_time";
        else
            $order = "ts.date, ts.on_date, ts.on_time";
        
        // Get Activity 
        $select .= " a.name, at.type ";//selected here but join later. needed to do the join in this order
        $from .= "INNER JOIN Activities AS a ON a.id = ts.activity_id ";
    if($_POST["activity"] != "null")
        $where .= "AND ts.activity_id = '".$activity_id."' ";
        $tableHeads .= "<th>Activity</th>";
        
         // Get Activity Type
        $select .= " ";	//selected above with activities
        $from .= "INNER JOIN ActivityTypes AS at ON at.id = a.parent_id ";
    if($_POST["activityType"] != "null")
        $where .= "AND a.id = ts.activity_id AND at.id = '".$activity_type_id."' ";
    else
//        $where .= "AND a.id = ts.activity_id AND ts.activity_id = a.id ";
        $tableHeads .= "<th>Activity Type</th>";
        
        // Get Company info
    if($company_id != "null"){
        $company_name = q("SELECT name FROM Company WHERE id = '".$company_id."' ");
        $headings .= "<tr><td class='on-table-clear' colspan='100%'>Report for: ".$company_name."</td></tr>";
        $row2Head = "Report for: ".$company_name;
        $from .= "INNER JOIN Company AS c ON ts.company_id = c.id ";
        $where .=" AND ts.company_id = '".$company_id."' AND ts.company_id = '".$company_id."' ";
        $showCompany = 0;
    }else{
        $headings .= "<tr><td class='on-table-clear' colspan='100%'>Report for: All Companies</td></tr>";
        $row2Head = "Report for: All Companies";
        $select .= ", c.name";
        $from .= "INNER JOIN Company AS c ON ts.company_id = c.id ";
//        $where .= " AND cot.company_id = ts.company_id ";
        $tableHeads .= "<th>Company Name</th>"; 
        $showCompany = 1;
    }
        
        // Get employee info  
        if($_POST["employee"] != 'null'){
            $employee_name = q("SELECT CONCAT(lstname, ', ', frstname) FROM Employee WHERE id = '".$employee_id."' ");
            $headings .= "<tr><td class='on-table-clear' colspan=' 100% '>Employee: ".$employee_name."</td></tr>";
            $row3Head = "Employee: ".$employee_name;
            $from .= "INNER JOIN Employee AS e ON e.id = ts.user_id ";
            $where .= "AND e.id = '".$employee_id."' ";
            $showEmployee = 0;
        }else{
            $row3Head = "Employee: All Employees";
            $select .= ", CONCAT(e.lstname, ', ', e.frstname) ";
            $from .= "INNER JOIN Employee AS e ON e.id = ts.user_id ";
//            $where .= "AND e.id = '".$employee_id."' ";
            $tableHeads .= "<th>Employee</th>";
            $showEmployee = 1;
        }
        
        // Time Sheet Information
        $tableHeads .= "<th>Description</th>
                                    <th>Date</th>
                                    <th>Rate</th>
                                    <th>Time Spent</th>
                                    <th>Cost</th>"; 

        $select .= ", ts.descr, ts.date, ts.rate, ts.time_spent";
        $tableHeads .= "</tr>";
       
        if($hasAreas)
            $exceldata[$row][] = $colorClass."Area";                             // 
            $exceldata[$row][] = $colorClass."Activity Type";               // 
            $exceldata[$row][] = $colorClass."Activity";                        // 
        if($showCompany)
            $exceldata[$row][] = $colorClass."Company Name";            // 
        if($showEmployee)
            $exceldata[$row][] = $colorClass."Employee";                     // 
            $exceldata[$row][] = $colorClass."Description";                 // 
            $exceldata[$row][] = $colorClass."Date";                            // 
            $exceldata[$row][] = $colorClass."Rate";                            // 
            $exceldata[$row][] = $colorClass. "Time Spent";                 // 
            $exceldata[$row][] = $colorClass. "Cost";                 // 
        $row++;
        
        $headings .= "<tr><td class='on-table-clear' colspan=' 100% '>".$timestamp."</td></tr>";
            $row4Head = $timestamp;
            
        $query = "SELECT $select FROM $from WHERE $where ORDER BY $order, ts.on_date, ts.on_time";

        $info = q($query);
    
        $displayString = "";

        //  Create Display String
        if (is_array($info)) {
            $displayString .= "".$tableHeads;           
            $total = 0;
            $totalCost = 0;
            $colCount = 0;
            $columns = 0;
            
            foreach ($info as $r) {
                $displayString .= "<tr>";
                //////////  TOTAL  //////////
                $total += $r[count($r)-1];
                $columns = count($r)-2;
                
                //////  -- EXCEL --  //////
                for($i=0; $i<$columns+2; $i++){
                    if($r[$i] == "" || $r[$i] == null){
                        $r[$i] = "-";
                    }
                    $exceldata[$row][] = $r[$i];
                }

                $displayString .= "<td style='white-space:nowrap'>".$r[0]."</td>";
                $displayString .= "<td style='white-space:nowrap'>".$r[1]."</td>";
                $displayString .= "<td style='white-space:nowrap'>".$r[2]."</td>";
                    
                if($hasAreas && $showCompany && $showEmployee){
                $displayString .= "<td style='white-space:nowrap'>".$r[3]."</td>";
                    $displayString .= "<td style='white-space:nowrap'>".$r[4]."</td>";
                    $displayString .= "<td>".$r[5]."</td>";
                    $displayString .= "<td style='white-space:nowrap' class='rightdata'>".$r[6]."</td>";
                    $displayString .= "<td class='rightdata'>".$r[7]."</td>";
                    $displayString .= "<td class='rightdata'>".$r[8]."</td>";
                }else if(!$hasAreas && $showCompany && $showEmployee){
                    $displayString .= "<td style='white-space:nowrap'>".$r[3]."</td>";
                    $displayString .= "<td>".$r[4]."</td>";
                    $displayString .= "<td style='white-space:nowrap'>".$r[5]."</td>";
                    $displayString .= "<td class='rightdata'>".$r[6]."</td>";
                    $displayString .= "<td class='rightdata'>".$r[7]."</td>";
                }else if(!$hasAreas && !$showCompany && $showEmployee){
                    $displayString .= "<td>".$r[3]."</td>";
                    $displayString .= "<td style='white-space:nowrap'>".$r[4]."</td>";
                    $displayString .= "<td class='rightdata'>".$r[5]."</td>";
                    $displayString .= "<td class='rightdata'>".$r[6]."</td>";
                }else if($hasAreas && !$showCompany && $showEmployee){
                    $displayString .= "<td style='white-space:nowrap'>".$r[3]."</td>";
                    $displayString .= "<td>".$r[4]."</td>";
                    $displayString .= "<td style='white-space:nowrap'>".$r[5]."</td>";
                    $displayString .= "<td class='rightdata'>".$r[6]."</td>";
                    $displayString .= "<td class='rightdata'>".$r[7]."</td>";
                }else if(!$hasAreas && !$showCompany && !$showEmployee){
                    $displayString .= "<td style='white-space:nowrap'>".$r[3]."</td>";
                    $displayString .= "<td class='rightdata'>".$r[4]."</td>";
                    $displayString .= "<td class='rightdata'>".$r[5]."</td>";
                }else if($hasAreas && !$showCompany && !$showEmployee){
                    $displayString .= "<td>".$r[3]."</td>";
                    $displayString .= "<td style='white-space:nowrap'>".$r[4]."</td>";
                    $displayString .= "<td class='rightdata'>".$r[5]."</td>";
                    $displayString .= "<td class='rightdata'>".$r[6]."</td>";
                }
                    
                $colCount = 0;
                $totalCost  += ($r[$columns] * $r[$columns+1]);
                
                $exceldata[$row][] = ($r[$columns] * $r[$columns+1]);
                $row++;
                
                $displayString .= "<td class='rightdata'>".number_format(($r[$columns] * $r[$columns+1]), 2, ".", "")."</td>";       // COST
                $displayString .= "</tr>";
            }
            
            // Print blanks before Total
            for($i=0; $i<$columns; $i++){
                $exceldata[$row][] = "";  
            }
            $exceldata[$row][] = "Total: "; 
            $exceldata[$row][] = number_format($total, 2, ".", ""); 
            $exceldata[$row][] = number_format($totalCost, 2, ".", ""); 
            
            $displayString .= "<tr>";
            $displayString .= "<td class='on-table-total' colspan='".(5 + $hasAreas + $showCompany + $showEmployee )."'>Total:</td>
                                         <td class='on-table-total'>".number_format($total, 2, ".", "")."</td>
                                         <td class='on-table-total'>".number_format($totalCost, 2, ".", "")."</td>";
            $displayString .= "</tr>";            
        }

        if ($displayString != "")
            $generated                                                  = "1";

    }
    
    printHeadings($row1Head, 0, $columns+2);
    printHeadings($row2Head, 1, $columns+2);
    printHeadings($row3Head, 2, $columns+2);
    printHeadings($row4Head, 3, $columns+2);
    
    //print_r($excelheadings);
    //print_r($exceldata);
   
    //  Print Header
    print_header();
    //  Print Menu
    print_menus("3", "reports");
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    jQuery(function(){
	jQuery('#date_from').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_from').val(),
		current: jQuery('#date_from').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_from').val()))
			_date = new Date();
			else _date = jQuery('#date_from').val();
			jQuery('#date_from').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_from').val(formated);
			jQuery('#date_from').DatePickerHide();
		}
	});
    })

    jQuery(function(){
	jQuery('#date_to').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_to').val(),
		current: jQuery('#date_to').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_to').val()))
			_date = new Date();
			else _date = jQuery('#date_to').val();
			jQuery('#date_to').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_to').val(formated);
			jQuery('#date_to').DatePickerHide();
		}
	});

        jQuery("#project").change(function()    {
            if (jQuery(this).val() != "null")   {
                jQuery("#dateType option[value='projectLifetime']").removeAttr("disabled");
            } else      {
                jQuery("#dateType option[value='projectLifetime']").attr("disabled","disabled");
                jQuery("#dateType option[value='currentwk']").attr("selected", "selected");
            }
        });
    })

    //  Sajax
    <?php sajax_show_javascript(); ?>

    function check()
    {
        var valid                                                       = 1;
        
        // Validate Project selection
        if (document.forms["report"].project.value == "null")
        {
            ShowLayer("projectErrorDiv", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("projectErrorDiv", "none");

        if (document.forms["report"].dateType.value == "custom") {
            //  Check That Date From Is Entered
            if (document.forms["report"].date_from.value == "") {
                ShowLayer("dateFrom", "block");
                valid                                                   = 0;
            }
            //  Check That Entered Date From Is Valid
            else {
                ShowLayer("dateFrom", "none");

                if (!validation("date", document.forms["report"].date_from.value)) {
                    ShowLayer("dateFromDiv", "block");
                    valid                                               = 0;
                }
                else
                    ShowLayer("dateFromDiv", "none");
            }

            //  Check That Date To Is Entered
            if (document.forms["report"].date_to.value == "") {
                ShowLayer("dateTo", "block");
                valid                                                   = 0;
            }
            //  Check That Entered Date To Is Valid
            else {
                ShowLayer("dateTo", "none");

                if (!validation("date", document.forms["report"].date_to.value)) {
                    ShowLayer("dateToDiv", "block");
                    valid                                               = 0;
                }
                else
                    ShowLayer("dateToDiv", "none");
            }
        }

        if (valid == 1) {
            document.forms["report"].save.value                         = 1;
            document.forms["report"].submit();
        }
    }
    
    var hasAreas = 1;

    function dateSelection() {
        if (document.forms["report"].dateType.value == "custom")
            ShowLayer("datesDiv", "block");
        else
            ShowLayer("datesDiv", "none");
    }

    ///////////////////////////
    //  Sajax
    function ClearOptions(OptionList) {
        for (x = OptionList.length; x >= 0; x--)
        {
            OptionList[x]                                               = null;
        }
    }
    
    // Set Company
    function setCompanies(data) {
        var c                                                           = 0;
        if(!data)
            ShowLayer("emloyeeDiv", "none");
        
        document.forms["report"].company.options[(c++)]                         = new Option("All Companies", "null");
        
        for (var i in data)
          eval("document.forms['report'].company.options["+(c++)+"] = new Option('" + data[i][1] + "', '" + data[i][0] + "');");
    }

    // Set Employees
    function setEmployees(data) {
        var c                                                           = 0;
            
        document.forms["report"].employee.options[(c++)]                         = new Option("All Employees", "null");
        
        for (var i in data)
          eval("document.forms['report'].employee.options["+(c++)+"] = new Option('" + data[i][1] + "', '" + data[i][0] + "');");
    }
    
    //Set Areas
    function setAreas(data) {
        var c                                                           = 0;
        if(!data){
            ShowLayer("areaDiv", "none");
            hasAreas = 0;
        }else{
            ShowLayer("areaDiv", "block");
            hasAreas = 1;
        }
        
        if(hasAreas)
        document.forms["report"].area.options[(c++)]                         = new Option("All Areas", "null");

        for (var i in data)
            eval("document.forms['report'].area.options["+(c++)+"] = new Option('" + data[i][1] + "', '" + data[i][0] + "');");
    }
    
    // Set Activity Types
    function setActivityTypes(data) {
        var c                                                           = 0;
        if(!data)
            ShowLayer("activityTypeDiv", "none");

        document.forms["report"].activityType.options[(c++)]                         = new Option("All Activity Types", "null");

        for (var i in data)
            eval("document.forms['report'].activityType.options["+(c++)+"] = new Option('" + data[i][1] + "', '" + data[i][0] + "');");
    }
    
    // Set Activities
    function setActivities(data) {
        var c                                                           = 0;
        if(!data)
            ShowLayer("activityDiv", "none");

        document.forms["report"].activity.options[(c++)]                         = new Option("All Activities", "null");

        for (var i in data)
            eval("document.forms['report'].activity.options["+(c++)+"] = new Option('" + data[i][1] + "', '" + data[i][0] + "');");
    }
    
    // Get Companies
    function getCompanies(select) {
        ClearOptions(document.report.company);
        if (select.value != "null")
        {
            ShowLayer("companyDiv", "block");
        }else{
            ShowLayer("companyDiv", "none");
        }
        x_getCompanies(select.value, setCompanies);
        
        if (select.value != "null")
            ShowLayer("dateShowDiv", "block");
        
    }
    
    // Get Employees
    function getEmployees(select) {
        ClearOptions(document.report.employee);
        if (select.value != "null")
        {
            ShowLayer("employeeDiv", "block");
        }else{
            ShowLayer("employeeDiv", "none");
        }
            x_getEmployees(select.value, setEmployees);
    }
    
    // Get Areas
    function getAreas(select) {
        ClearOptions(document.report.area);
            ShowLayer("areaDiv", "block");
            x_getAreas(select.value, setAreas);
    }
    
    // Get Activity Types
    function getActivityTypes(select) {
        ClearOptions(document.report.activityType);
            ShowLayer("activityTypeDiv", "block");
            x_getActivityTypes(select.value, setActivityTypes);
    }
    
     // Get Activities
    function getActivities(select) {
        ClearOptions(document.report.activity);
            ShowLayer("activityDiv", "block");
            x_getActivities(select.value, setActivities);
    }

    function getData(select) {
        ClearOptions(document.report.activity);

        if (select.value != "null")
        {
            ShowLayer("activityDiv", "block");
            x_getActivities(select.value, setActivities);
        }
        else{
            ShowLayer("activityDiv", "none");
        }
    }
    ///////////////////////////
</script>
<?php
	$projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p 
                            INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) 
                            WHERE cot.company_id = '".$_SESSION["company_id"]."' 
                            AND p.completed = '0' 
                            AND p.deleted = '0' 
                            AND p.p_type='SP' ORDER BY UPPER(p.name)");
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata">
            
                <form action="" method="post" name="report">
                    <div id="" style="<?php if ($generated == "1") echo "display: none;"; else echo "display: block"; ?>">
                        
                        <table width="100%">
                            <tr>
                                <td class="centerdata">
                                    <h6>Shared Project Time Sheet Report</h6>
                                </td>
                            </tr>
                        </table>
                        <br>   
                        <div id="projectDiv" style="display: block;">
                            <table width="100%">                            
                                <tr>
                                    <td class="on-description" width="50%">
                                        Report On:
                                    </td>
                                    <td width="50%">
                                        <select class="on-field" method="post" name="reportOn">
                                            <option value="normal">Actual Time</option>
                                            <option value="approved">Approved Time</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr><td colspan="100%"><br/></td></tr>
                                <tr>
                                    <td class="on-description" width="50%">
                                        Select Shared Project:
                                    </td>
                                    <td width="50%">
                                        <select class="on-field" method="post" id="project" name="project" onChange="getCompanies(project); getAreas(project); getActivityTypes(project)">
                                            <option value="null">--  Select a Shared Project  --</option>
                                            <?php
                                                if (is_array($projects))
                                                    foreach ($projects as $project)
                                                        echo "<option value='".$project[0]."'>".$project[1]."</option>";
                                            ?>
                                        </select>
                                        <div id="projectErrorDiv" style="display: none;"><font class="on-validate-error">* Shared Project must be selected</font></div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="companyDiv" style="display: none;"> 
                            <table width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Select Company:
                                    </td>
                                    <td width="50%">
                                        <select class="on-field" method="post" name="company" onChange="getEmployees(company)">
                                            <option value="null">--  Select a Company  --</option>
                                            <?php
                                                if (is_array($companies))
                                                    foreach ($companies as $company)
                                                        if (isset($_POST["company"]) && $_POST["company"] == $company[0])
                                                            echo "<option value='".$company[0]."' selected>".$company[1]."</option>";
                                                        else
                                                            echo "<option value='".$company[0]."'>".$company[1]."</option>";
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                            
                        <div id="areaDiv" style="display: none;">
                            <table width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Select Area:
                                    </td>
                                    <td width="50%">
                                        <select class="on-field" method="post" name="area">
                                             <option value="null">--  Select a Area  --</option>
                                            <?php
                                                if (is_array($areas))
                                                    foreach ($areas as $area)
                                                        if ($_POST["area"] == $area[0])
                                                            echo "<option value='".$area[0]."' selected>".$area[1]."</option>";
                                                        else
                                                            echo "<option value='".$area[0]."'>".$area[1]."</option>";
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                                
                        <div id="employeeDiv" style="display: none;"> 
                            <table width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Select Employee:
                                    </td>
                                    <td width="50%">
                                        <select class="on-field" method="post" id="employee" name="employee">
                                            <option value="null">--  Select Employee  --</option>
                                            <?php
                                                if (is_array($employees))
                                                    foreach ($employees as $employee)
                                                        if ($_POST["employee"] == $employee[0])
                                                            echo "<option value='".$employee[0]."' selected>".$employee[1].", ".$employee[2]."</option>";
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                            
                        <div id="activityTypeDiv" style="display: none;"> 
                            <table width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Activity Type:
                                    </td>
                                    <td width="50%">
                                        <select class="on-field" method="post" name="activityType" onChange="getActivities(activityType)">
                                            <option value="null">All Activity Types</option>
                                            <?php
                                                if (is_array($activityTypes))
                                                    foreach ($activityTypes as $activityType)
                                                        if ($_POST["activityType"] == $activityType[0])
                                                            echo "<option value='".$activityType[0]."' selected>".$activityType[1]."</option>";
                                                        else
                                                            echo "<option value='".$activityType[0]."'>".$activityType[1]."</option>";
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                            
                        <div id="activityDiv" style="display: none;">
                            <table width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Activity:
                                    </td>
                                    <td width="50%">
                                        <select class="on-field" method="post" name="activity">
                                            <option value="null">All Activities</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        
                        <div id="dateShowDiv" style="display: none">
                            <table width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Date Type(s):
                                    </td>
                                    <td width="50%">
                                        <select class="on-field" method="post" id="dateType" name="dateType" onChange="dateSelection();">
                                            <option value="currentwk">Current Week</option>
                                            <option value="previouswk">Previous Week</option>
                                            <option value="currentmnth">Current Month</option>
                                            <option value="previousmnth">Previous Month</option>
                                            <option value="projectLifetime" disabled>Project Lifetime</option>
                                            <option value="custom">Custom Dates</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                            
                        <div id="datesDiv" style="display: none;">       
                            <table width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Date From:
                                    </td>
                                    <td width="50%">
                                        <input class="on-field-date" id="date_from" name="date_from" type="text" style="text-align:right;" value="<?php
                                            if ($_POST["date_from"] != "") echo "".$_POST["date_from"]; else echo "".getMonthStart($today); ?>">
                                        <div id="dateFrom" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                        <div id="dateFromDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="on-description" width="50%">
                                        Date To:
                                    </td>
                                    <td width="50%">
                                        <input class="on-field-date" id="date_to" name="date_to" type="text" style="text-align:right;" value="<?php
                                            if ($_POST["date_to"] != "") echo "".$_POST["date_to"]; else echo "".getMonthEnd($today); ?>">
                                        <div id="dateTo" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                        <div id="dateToDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        
                        <br/>
                        <input name="btnGenerate" onClick="check();" type="button" value="Generate Report">
                        <input method="post" name="save" type="hidden" value="0" />
                        
                    </div>
                </form>
                
                <div id="report" style="<?php if ($generated == "1") echo "display: block;"; else echo "display: none"; ?>">
                    <?php
                        echo "<div class='on-20px'>
                                    <table class='on-table on-table-center'>".$headings.$displayString."
                                        <tfoot><tr><td colspan='100%'></td></tr></tfoot>
                                    </table>
                                </div>";
                        ///////////////////////////
                        //  Set Export Information
                        $_SESSION["fileName"] = "Report SP Time Sheet";
                        $_SESSION["fileData"] = array_merge($excelheadings, $exceldata);
                        ///////////////////////////

                        echo "<input name='btnExport' onClick=\"location.href='_export.php'\" type='button' value='Export to Excel' />";
                    ?>
                </div>
                
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
