<?php
/*
*  File:        _menus.php
*  Updated On:
*  Author:      integrITyEngineering
*/
    //////////////////////////////////////////////////
    //  GET MENUS
    function getMenus($companyID)	{
        if ($companyID != "")       {
            $menus = array();

            $parentID = 0;

            $menuItems = q("SELECT m.id,m.parentID,m.level,m.ranking,m.urlLink,m.menuDescr,m.icon FROM menus AS m INNER JOIN MenuIndex as mi ON m.id = mi.menu_id ".
                            "WHERE mi.company_id = $companyID AND mi.user_id = ".$_SESSION["user_id"]." ORDER BY mi.id");

            if (is_array($menuItems))   {
                foreach ($menuItems as $menuItem)  {
                    if ($menuItem[1] == "0" && $menuItem[4] == "bookings.php" && $menuItem[5] == "Bookings")    {
                        $landingPage = q("SELECT landingPage FROM user_defaults WHERE companyID = '".$companyID."' AND userID = '".$_SESSION["user_id"]."'");

                        $landingPage = ($landingPage == "dashboard.php") ? "bookings.php" : $landingPage;

                        $menus[] = array($menuItem[0],$menuItem[1],$menuItem[2],$menuItem[3],$landingPage,$menuItem[5],$menuItem[6]);
                    } else      $menus[] = $menuItem;
                }
            }
        }
    ?>
        <ul id="nav">
    <?php
        if (is_array($menus))   {
            $first = 0;
            $last = count($menus) - 1;

            //////////////////////////////////////////////////
            //  PRINT FIRST MENU ITEM
            //////////////////////////////////////////////////
            if ($menus[$first][2] < $menus[$first + 1][2])      {
                $href = ($menus[$first][4] != "") ? "href='".$menus[$first][4]."'" : "";
            ?>
            <li><a class="on-but" <?php echo $href; ?>><span><div class="on-but-pad"><?php echo $menus[$first][5]; ?></div></span></a>
                <ul>
            <?php
            }
            else if ($menus[$first][2] == $menus[$first + 1][2])        {
                $href = ($menus[$first][4] != "") ? "href='".$menus[$first][4]."'" : "";
            ?>
            <li><a class="on-but" <?php echo $href; ?>><span><div class="on-but-pad"><?php echo $menus[$first][5]; ?></div></span></a></li>
            <?php
            }

            //////////////////////////////////////////////////
            //  PRINT MENU ITEMS
            //////////////////////////////////////////////////
            for ($i = 1; $i < $last; ++$i)      {
                $href = ($menus[$i][4] != "") ? "href='".$menus[$i][4]."'" : "";

                //  CHECKING NEXT/PREVIOUS & CURRENT MENU LEVELS
                if ($menus[$i][2] < $menus[$i + 1][2])  {
                    if ($menus[$i][2] == "0")   {
                    ?>
            <li><a class="on-but" <?php echo $href; ?>><span><div class="on-but-pad"><?php echo $menus[$i][5]; ?></div></span></a>
                    <?php
                    }
                    else        {
                    ?>
            <li><a <?php echo $href; ?>><?php echo $menus[$i][5]; ?></a>
                    <?php
                    }
                    ?>
                <ul>
                    <?php
                }
                else if ($menus[$i][2] > $menus[$i + 1][2])     {
                ?>
            <li><a <?php echo $href; ?>><?php echo $menus[$i][5]; ?></a></li>
                    <?php
                    for ($j = 0; $j < ($menus[$i][2] - $menus[$i + 1][2]); ++$j)        {
                    ?>
                </ul>
            </li>
                    <?php
                    }
                }
                else    {
                    if ($menus[$i][2] == "0")   {
                    ?>
            <li><a class="on-but" <?php echo $href; ?>><span><div class="on-but-pad"><?php echo $menus[$i][5]; ?></div></span></a></li>
                    <?php
                    }
                    else        {
                    ?>
            <li><a <?php echo $href; ?>><?php echo $menus[$i][5]; ?></a></li>
                    <?php
                    }
                }
            }

            //////////////////////////////////////////////////
            //  PRINT LAST MENU ITEM
            //////////////////////////////////////////////////
            if ($menus[$first][2] < $menus[$last][2])   {
                $href = ($menus[$last][4] != "") ? "href='".$menus[$last][4]."'" : "";
            ?>
            <li><a <?php echo $href; ?>><?php echo $menus[$last][5]; ?></a></li>
                <?php
                for ($j = 0; $j < $menus[$last][2]; ++$j)       {
                ?>
                </ul>
            </li>
                <?php
                }
            }
            else if ($menus[$first][2] == $menus[$last][2])     {
                if ($menus[$last][2] == "0")    {
                ?>
            <li><a class="on-but" <?php echo $href; ?>><span><div class="on-but-pad"><?php echo $menus[$last][5]; ?></div></span></a></li>
                <?php
                }
                else    {
                ?>
            <li><a <?php echo $href; ?>><?php echo $menus[$last][5]; ?></a></li>
                <?php
                }
            }
        }
        ?>
            <br/>
            <li><a class="on-but" name="modal_help"><span><div class="on-but-pad">Contact Us</div></span></a></li>
        </ul>
<?php
    }
    //////////////////////////////////////////////////
?>
