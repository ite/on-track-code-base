<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if ($_SESSION["logged_in"] === true && !($_SESSION["usertype"] < "4"))
        header("Location: home.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    //  Set Report Generated Status
    $generated                                                          = "0";

    //  Get Expenses Info
    function getExpenses($project_id, $date_from, $date_to, $total_cost)
    {
        $top                                                            = "border-top:1px solid #336699;";
        $bottom                                                         = "border-bottom:1px solid #336699;";
        $left                                                           = "border-left:1px solid #336699;";
        $right                                                          = "border-right:1px solid #336699;";
        $background                                                     = "background-color:#6699CC;";

        $expense_based                                                  = "";

        //  $nExpenses                                                  =  Number of Expenses Booked
        $nExpenses                                                      = q("SELECT COUNT(id) FROM ExpenseSheet WHERE project_id = '$project_id' ".
                                                                            "AND date >= '$date_from' AND date <= '$date_to'");
        $expenses                                                       = q("SELECT id, descr, expense FROM ExpenseSheet WHERE project_id = '$project_id' ".
                                                                            "AND date >= '$date_from' AND date <= '$date_to' ORDER BY descr");

        if ($nExpenses > 1)
        {
            $expense_based                                              .= "<tr><td align='left' style='".$left.$bottom."' valign='top'>&nbsp;Expenses&nbsp;</td>";

            $expense_based                                              .= "<td align='left' style='".$left."'><table cellpadding='0' cellspacing='0' width='100%'>";

            foreach ($expenses as $expense)
                $expense_based                                          .= "<tr><td align='left' style='".$bottom."'>&nbsp;".$expense[1]."&nbsp;</td></tr>";

            $expense_based                                              .= "</table></td><td align='left' style='".$left.$right."'><table cellpadding='0' ".
                                                                            "cellspacing='0' width='100%'>";

            foreach ($expenses as $expense)
            {
                $total_cost                                             = $total_cost + $expense[2];

                $expense_based                                          .= "<tr><td align='right' style='".$bottom."'>".number_format($expense[2], 2, ".", "").
                                                                            "</td></tr>";
            }

            $expense_based                                              .= "</table></td>";
        }
        else if ($nExpenses == 1)
        {
            $total_cost                                                 = $total_cost + $expenses[0][2];
            $expense_based                                              .= "<tr><td align='left' style='".$left.$bottom."' valign='top'>&nbsp;Expenses&nbsp;</td>";
            $expense_based                                              .= "<td align='left' style='".$left."'><table cellpadding='0' cellspacing='0' width='100%'>".
                                                                            "<tr><td align='left' style='".$bottom."'>&nbsp;".$expenses[0][1]."&nbsp;</td>".
                                                                            "</table></td><td align='left' style='".$left.$right."'><table cellpadding='0' ".
                                                                            "cellspacing='0' width='100%'><tr><td align='right' style='".$bottom."'>".
                                                                            number_format($expenses[0][2], 2, ".", "")."</td></tr></table></td>";
        }

        $expense_based                                                  .= "<tr><td align='right' colspan='2' style='".$top.$left.$bottom.$background."'><a>".
                                                                            "Total <i>(".$_SESSION["currency"].")</i>:&nbsp;</a></td><td align='right' style='".$top.
                                                                            $left.$right.$bottom."'>".number_format($total_cost, 2, ".", "")."</td></tr>";

        return "".$expense_based;
    }

    //  Create Display Information Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")
    {
        $errorMessage                                                   = "";

        $project_id                                                     = $_POST["project"];
        $report_type                                                    = $_POST["reportType"];
        $date_from                                                      = addslashes(strip_tags($_POST["date_from"]));
        $date_to                                                        = addslashes(strip_tags($_POST["date_to"]));
        $displayString                                                  = "";

        ///////////////////////////
        //  Get Info From Tables
        ///////////////////////////
        $project_name                                                   = q("SELECT name From Project WHERE id = '$project_id'");

        if ($report_type == "cost")
        {
            $total_cost                                                 = 0;

            //  Create Heading
            $table_headers                                              = "<tr><td align='center' style='".$top.$left.$bottom.$background."'><a>&nbsp;Activity Type&nbsp;".
                                                                            "</a></td><td align='center' style='".$top.$left.$bottom.$background."'><a>&nbsp;Activity".
                                                                            "&nbsp;</a></td><td align='center' style='".$top.$left.$right.$bottom.$background."'><a>".
                                                                            "&nbsp;Cost <i>(".$_SESSION["currency"].")</i>&nbsp;</a></td>";

            ///////////////////////////
            //  Create Display
            ///////////////////////////
            //  Time Booked Section
            //  $nActivityTypes                                         = Number of Activities
            $nActivityTypes                                             =q("SELECT COUNT(DISTINCT(at.id)) FROM (ActivityTypes AS at LEFT JOIN Activities AS a ".
                                                                            "ON at.id = a.parent_id LEFT JOIN TimeSheet AS ts ON ts.activity_id = a.id) ".
                                                                            "WHERE ts.project_id = '$project_id' AND ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                                                            "AND at.company_id = '".$_SESSION["company_id"]."'");
            $activityTypes                                              =q("SELECT DISTINCT(at.id), at.type FROM (ActivityTypes AS at LEFT JOIN Activities AS a ".
                                                                            "ON at.id = a.parent_id LEFT JOIN TimeSheet AS ts ON ts.activity_id = a.id) ".
                                                                            "WHERE ts.project_id = '$project_id' AND ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                                                            "AND at.company_id = '".$_SESSION["company_id"]."' ORDER BY at.type");

            if ($nActivityTypes > 1)
            {
                foreach ($activityTypes as $activityType)
                {
                    //  nActivities                                     = Number of Activities per Type
                    $nActivities                                        = q("SELECT COUNT(a.id) FROM (TimeSheet AS ts INNER JOIN Activities AS a ".
                                                                            "ON a.id = ts.activity_id) WHERE a.parent_id = '".$activityType[0]."' ".
                                                                            "AND ts.project_id = '$project_id' AND ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                                                            "GROUP BY a.id");
                    $activities                                         = q("SELECT a.id, a.name, SUM(ts.rate * ts.time_spent) FROM (TimeSheet AS ts INNER JOIN ".
                                                                            "Activities AS a ON a.id = ts.activity_id) WHERE a.parent_id = '".$activityType[0]."' ".
                                                                            "AND ts.project_id = '$project_id' AND ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                                                            "GROUP BY a.id ORDER BY a.name");

                    $time_based                                         .= "<tr><td align='left' style='".$left.$bottom."' valign='top'>&nbsp;".$activityType[1].
                                                                            "&nbsp;</td>";

                    if ($nActivities > 1)
                    {
                        $time_based                                     .= "<td align='left' style='".$left."'><table cellpadding='0' cellspacing='0' width='100%'>";

                        foreach ($activities as $activity)
                            $time_based                                 .= "<tr><td align='left' style='".$bottom."'>&nbsp;".$activity[1]."&nbsp;</td></tr>";

                        $time_based                                     .= "</table></td><td align='left' style='".$left.$right."'><table cellpadding='0' ".
                                                                            "cellspacing='0' width='100%'>";

                        foreach ($activities as $activity)
                        {
                            $total_cost                                 = $total_cost + $activity[2];

                            $time_based                                 .= "<tr><td align='right' style='".$bottom."'>".number_format($activity[2], 2, ".", "").
                                                                            "</td></tr>";
                        }

                        $time_based                                     .= "</table></td>";
                    }
                    else if ($nActivities == 1)
                    {
                        $total_cost                                     = $total_cost + $activities[0][2];
                        $time_based                                     .= "<td align='left' style='".$left.$bottom."'>&nbsp;".$activities[0][1]."&nbsp;</td>".
                                                                            "<td align='right' style='".$left.$right.$bottom."'>".
                                                                            number_format($activities[0][2], 2, ".", "")."</td>";
                    }

                    $time_based                                         .= "</tr>";
                }
            }
            else if ($nActivityTypes == 1)
            {
                //  nActivities                                         = Number of Activities per Type
                $nActivities                                            = q("SELECT COUNT(a.name) FROM (TimeSheet AS ts INNER JOIN Activities AS a ".
                                                                            "ON a.id = ts.activity_id) WHERE a.parent_id = '".$activityTypes[0]."' ".
                                                                            "AND ts.project_id = '$project_id' AND ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                                                            "GROUP BY a.id");
                $activities                                             = q("SELECT a.name, SUM(ts.rate * ts.time_spent) FROM (TimeSheet AS ts INNER JOIN ".
                                                                            "Activities AS a ON a.id = ts.activity_id) WHERE a.parent_id = '".$activityTypes[0]."' ".
                                                                            "AND ts.project_id = '$project_id' AND ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                                                            "GROUP BY a.id ORDER BY a.name");

                $time_based                                             .= "<tr><td align='left' style='".$left.$bottom."' valign='top'>&nbsp;".$activityTypes[0][1].
                                                                            "&nbsp;</td>";

                if ($nActivities > 1)
                {
                    $time_based                                         .= "<td align='left' style='".$left."'><table cellpadding='0' cellspacing='0' width='100%'>";

                    foreach ($activities as $activity)
                        $time_based                                     .= "<tr><td align='left' style='".$bottom."'>&nbsp;".$activity[1]."&nbsp;</td></tr>";

                    $time_based                                         .= "</table></td><td align='left' style='".$left.$right."'><table cellpadding='0' ".
                                                                            "cellspacing='0' width='100%'>";

                    foreach ($activities as $activity)
                    {
                        $total_cost                                     = $total_cost + $activity[2];

                        $time_based                                     .= "<tr><td align='right' style='".$bottom."'>".number_format($activity[2], 2, ".", "").
                                                                            "</td></tr>";
                    }

                    $time_based                                         .= "</table></td>";
                }
                else if ($nActivities == 1)
                {
                    $total_cost                                         = $total_cost + $activities[0][2];
                    $time_based                                         .= "<td align='left' style='".$left.$bottom."'>&nbsp;".$activities[0][1]."&nbsp;</td>".
                                                                            "<td align='right' style='".$left.$right.$bottom."'>".
                                                                            number_format($activities[0][2], 2, ".", "")."</td>";
                }
            }

            $expense_based                                              = getExpenses($project_id, $date_from, $date_to, $total_cost);
            $display                                                    = $table_headers.$time_based.$expense_based;
        }
        else if ($report_type == "time")
        {
            $total_cost                                                 = 0;
            $total_time                                                 = 0;

            //  Create Heading
            $table_headers                                              = "<tr><td align='center' style='".$top.$left.$bottom.$background."'><a>&nbsp;Activity Type&nbsp;".
                                                                            "</a></td><td align='center' style='".$top.$left.$bottom.$background."'><a>&nbsp;Activity".
                                                                            "&nbsp;</a></td><td align='center' style='".$top.$left.$right.$bottom.$background."'><a>".
                                                                            "&nbsp;Time Spent&nbsp;</a></td>";

            ///////////////////////////
            //  Create Display
            ///////////////////////////
            //  Time Booked Section
            //  $nActivityTypes                                         = Number of Activities
            $nActivityTypes                                             =q("SELECT COUNT(DISTINCT(at.id)) FROM (ActivityTypes AS at LEFT JOIN Activities AS a ".
                                                                            "ON at.id = a.parent_id LEFT JOIN TimeSheet AS ts ON ts.activity_id = a.id) ".
                                                                            "WHERE ts.project_id = '$project_id' AND ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                                                            "AND at.company_id = '".$_SESSION["company_id"]."'");
            $activityTypes                                              =q("SELECT DISTINCT(at.id), at.type FROM (ActivityTypes AS at LEFT JOIN Activities AS a ".
                                                                            "ON at.id = a.parent_id LEFT JOIN TimeSheet AS ts ON ts.activity_id = a.id) ".
                                                                            "WHERE ts.project_id = '$project_id' AND ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                                                            "AND at.company_id = '".$_SESSION["company_id"]."' ORDER BY at.type");

            if ($nActivityTypes > 1)
            {
                foreach ($activityTypes as $activityType)
                {
                    //  nActivities                                     = Number of Activities per Type
                    $nActivities                                        = q("SELECT COUNT(a.id) FROM (TimeSheet AS ts INNER JOIN Activities AS a ".
                                                                            "ON a.id = ts.activity_id) WHERE a.parent_id = '".$activityType[0]."' ".
                                                                            "AND ts.project_id = '$project_id' AND ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                                                            "GROUP BY a.id");
                    $activities                                         = q("SELECT a.id, a.name, SUM(ts.time_spent) FROM (TimeSheet AS ts INNER JOIN ".
                                                                            "Activities AS a ON a.id = ts.activity_id) WHERE a.parent_id = '".$activityType[0]."' ".
                                                                            "AND ts.project_id = '$project_id' AND ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                                                            "GROUP BY a.id ORDER BY a.name");

                    $time_based                                         .= "<tr><td align='left' style='".$left.$bottom."' valign='top'>&nbsp;".$activityType[1].
                                                                            "&nbsp;</td>";

                    if ($nActivities > 1)
                    {
                        $time_based                                     .= "<td align='left' style='".$left."'><table cellpadding='0' cellspacing='0' width='100%'>";

                        foreach ($activities as $activity)
                            $time_based                                 .= "<tr><td align='left' style='".$bottom."'>&nbsp;".$activity[1]."&nbsp;</td></tr>";

                        $time_based                                     .= "</table></td><td align='left' style='".$left.$right."'><table cellpadding='0' ".
                                                                            "cellspacing='0' width='100%'>";

                        foreach ($activities as $activity)
                        {
                            $total_time                                 = $total_time + $activity[2];

                            $time_based                                 .= "<tr><td align='right' style='".$bottom."'>".number_format($activity[2], 2, ".", "").
                                                                            "</td></tr>";
                        }

                        $time_based                                     .= "</table></td>";
                    }
                    else if ($nActivities == 1)
                    {
                        $total_time                                     = $total_time + $activities[0][2];
                        $time_based                                     .= "<td align='left' style='".$left.$bottom."'>&nbsp;".$activities[0][1]."&nbsp;</td>".
                                                                            "<td align='right' style='".$left.$right.$bottom."'>".
                                                                            number_format($activities[0][2], 2, ".", "")."</td>";
                    }

                    $time_based                                         .= "</tr>";
                }
            }
            else if ($nActivityTypes == 1)
            {
                //  nActivities                                         = Number of Activities per Type
                $nActivities                                            = q("SELECT COUNT(a.name) FROM (TimeSheet AS ts INNER JOIN Activities AS a ".
                                                                            "ON a.id = ts.activity_id) WHERE a.parent_id = '".$activityTypes[0]."' ".
                                                                            "AND ts.project_id = '$project_id' AND ts.date >= '$date_from' AND ts.date <= '$date_to'");
                $activities                                             = q("SELECT a.name, SUM(ts.time_spent) FROM (TimeSheet AS ts INNER JOIN ".
                                                                            "Activities AS a ON a.id = ts.activity_id) WHERE a.parent_id = '".$activityTypes[0]."' ".
                                                                            "AND ts.project_id = '$project_id' AND ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                                                            "GROUP BY a.id ORDER BY name");

                $time_based                                             .= "<tr><td align='left' style='".$left.$bottom."' valign='top'>&nbsp;".$activityTypes[0][1].
                                                                            "&nbsp;</td>";

                if ($nActivities > 1)
                {
                    $time_based                                         .= "<td align='left' style='".$left."'><table cellpadding='0' cellspacing='0' width='100%'>";

                    foreach ($activities as $activity)
                        $time_based                                     .= "<tr><td align='left' style='".$bottom."'>&nbsp;".$activity[0]."&nbsp;</td></tr>";

                    $time_based                                         .= "</table></td><td align='left' style='".$left.$right."'><table cellpadding='0' ".
                                                                            "cellspacing='0' width='100%'>";

                    foreach ($activities as $activity)
                    {
                        $total_time                                     = $total_time + $activity[1];

                        $time_based                                     .= "<tr><td align='right' style='".$bottom."'>".number_format($activity[2], 2, ".", "").
                                                                            "</td></tr>";
                    }

                    $time_based                                         .= "</table></td>";
                }
                else if ($nActivities == 1)
                {
                    $total_time                                         = $total_time + $activities[0][2];
                    $time_based                                         .= "<td align='left' style='".$left.$bottom."'>&nbsp;".$activities[0][1]."&nbsp;</td>".
                                                                            "<td align='right' style='".$left.$right.$bottom."'>".
                                                                            number_format($activities[0][2], 2, ".", "")."</td>";
                }
            }

            $time_based                                                 .= "<tr><td align='right' colspan='2' style='".$top.$left.$bottom.$background."'><a>".
                                                                            "Total:&nbsp;</a></td><td align='right' style='".$top.$left.$right.$bottom."'>".
                                                                            number_format($total_time, 2, ".", "")."</td></tr><tr><td><br/></td></tr>";
            $display                                                    .= "".$table_headers.$time_based;

            $table_headers                                              = "<tr><td align='center' style='".$top.$left.$bottom.$background."'><a>&nbsp;Activity Type&nbsp;".
                                                                            "</a></td><td align='center' style='".$top.$left.$bottom.$background."'><a>&nbsp;Activity".
                                                                            "&nbsp;</a></td><td align='center' style='".$top.$left.$right.$bottom.$background."'><a>".
                                                                            "&nbsp;Cost <i>(R)</i>&nbsp;</a></td>";
            $expense_based                                              = getExpenses($project_id, $date_from, $date_to, $total_cost);
            $display                                                    .= "".$table_headers.$expense_based;
        }

        if ($display != "")
            $displayString                                              = "<tr><td colspan='3' style='".$top.$left.$right.$bottom.$background."'><a>Project Report</a>".
                                                                            "</td></tr><tr><td colspan='3' style='".$top.$left.$right.$bottom.$background."'>".
                                                                            "<a>Project:&nbsp;&nbsp;".$project_name."</a></td></tr>".$display;

        if ($displayString != "")
            $generated                                                  = "1";
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("3", "reports");
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    jQuery(function(){
	jQuery('#date_from').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_from').val(),
		current: jQuery('#date_from').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_from').val()))
			_date = new Date();
			else _date = jQuery('#date_from').val();
			jQuery('#date_from').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_from').val(formated);
			jQuery('#date_from').DatePickerHide();
		}
	});
    })

    jQuery(function(){
	jQuery('#date_to').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_to').val(),
		current: jQuery('#date_to').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_to').val()))
			_date = new Date();
			else _date = jQuery('#date_to').val();
			jQuery('#date_to').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_to').val(formated);
			jQuery('#date_to').DatePickerHide();
		}
	});
    })

    function check()
    {
        var valid                                                       = 1;

        //  Check That Employee Is Selected
        if (document.forms["report_project"].project.value == "null")
        {
            ShowLayer("projectDiv", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("projectDiv", "none");

        //  Check That Date From Is Entered
        if (document.forms["report_project"].date_from.value == "")
        {
            ShowLayer("dateFrom", "block");
            valid                                                       = 0;
        }
        //  Check That Entered Date From Is Valid
        else
        {
            ShowLayer("dateFrom", "none");

            if (!validation("date", document.forms["report_project"].date_from.value))
            {
                ShowLayer("dateFromDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("dateFromDiv", "none");
        }

        //  Check That Date To Is Entered
        if (document.forms["report_project"].date_to.value == "")
        {
            ShowLayer("dateTo", "block");
            valid                                                       = 0;
        }
        //  Check That Entered Date To Is Valid
        else
        {
            ShowLayer("dateTo", "none");

            if (!validation("date", document.forms["report_project"].date_to.value))
            {
                ShowLayer("dateToDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("dateToDiv", "none");
        }

        if (valid == 1)
        {
            document.forms["report_project"].save.value                 = 1;
            document.forms["report_project"].submit();
        }
    }
</script>
<?php
    //  nProjects                                                       = Number of Contacts
    $nProjects                                                          = q("SELECT COUNT(id) FROM Project WHERE completed = 0 ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."'");
    $projects                                                           = q("SELECT id, name FROM Project WHERE completed = 0 ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."' ORDER BY name");
?>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr height="380px">
            <td align="center" valign="top">
                <form action="" method="post" name="report_project">
                    <div id="" style="<?php if ($generated == "1") echo "display: none;"; else echo "display: block"; ?>">
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td align="center">
                                    <a style="font-size:12px;font-weight:bold;">
                                        Project Report
                                    </a>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <table cellpadding="0" cellspacing="2" width="100%">
                            <tr>
                                <td align="right" valign="top" style="padding-top:4px;" width="50%">
                                    <strong>
                                        Select Project:&nbsp;
                                    </strong>
                                </td>
                                <td align="left" width="50%">
                                    &nbsp;<select method="post" id="project" name="project">
                                        <option value="null">--  Select A Project  --&nbsp;&nbsp;</option>
                                        <?php
                                            if ($nProjects > 1)
                                                foreach ($projects as $project)
                                                    if ($_POST["project"] == $project[0])
                                                        echo "<option value='".$project[0]."' selected>".$project[1]."&nbsp;&nbsp;</option>";
                                                    else
                                                        echo "<option value='".$project[0]."'>".$project[1]."&nbsp;&nbsp;</option>";
                                            else if ($nProjects == 1)
                                                if ($_POST["project"] == $projects[0][0])
                                                    echo "<option value='".$projects[0][0]."' selected>".$projects[0][1]."&nbsp;&nbsp;".
                                                        "</option>";
                                                else
                                                    echo "<option value='".$projects[0][0]."'>".$projects[0][1]."&nbsp;&nbsp;</option>";
                                        ?>
                                    </select>
                                    <div id="projectDiv" style="display: none;"><font color="#FF5900">* Project must be selected</font></div>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" valign="top" style="padding-top:4px;" width="50%">
                                    <strong>
                                        Report Type:&nbsp;
                                    </strong>
                                </td>
                                <td align="left" width="50%">
                                    &nbsp;<input method="post" name="reportType" type="radio" value="time" checked>&nbsp;<a>Time Based</a><br/>
                                    &nbsp;<input method="post" name="reportType" type="radio" value="cost">&nbsp;<a>Cost Based</a><br/>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" valign="top" style="padding-top:4px;" width="50%">
                                    <strong>
                                        Date From:&nbsp;
                                    </strong>
                                </td>
                                <td align="left" width="50%">
                                    &nbsp;<input id="date_from" name="date_from" type="text" style="text-align:right;" value="<?php
                                        if ($_POST["date_from"] != "") echo "".$_POST["date_from"]; else echo "".getMonthStart($today); ?>">
                                    <div id="dateFrom" style="display: none;"><font color="#FF5900">* Date must be entered</font></div>
                                    <div id="dateFromDiv" style="display: none;"><font color="#FF5900">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" valign="top" style="padding-top:4px;" width="50%">
                                    <strong>
                                        Date To:&nbsp;
                                    </strong>
                                </td>
                                <td align="left" width="50%">
                                    &nbsp;<input id="date_to" name="date_to" type="text" style="text-align:right;" value="<?php
                                        if ($_POST["date_to"] != "") echo "".$_POST["date_to"]; else echo "".getMonthEnd($today); ?>">
                                    <div id="dateTo" style="display: none;"><font color="#FF5900">* Date must be entered</font></div>
                                    <div id="dateToDiv" style="display: none;"><font color="#FF5900">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <input name="btnGenerate" onClick="check();" type="button" value="Generate Report">
                        <input method="post" name="save" type="hidden" value="0" />
                    </div>
                </form>
                <div id="report" style="<?php if ($generated == "1") echo "display: block;"; else echo "display: none"; ?>">
                    <?php
                        echo "<table cellpadding='0' cellspacing='0'>";
                            echo "".$displayString;
                        echo "</table>";
                        echo "<br/>";

                        ///////////////////////////
                        //  Set Export Information
                        $_SESSION["file_name"]                          = "".date("Ymd")."_-_Project_Report.xls";
                        $_SESSION["file_data"]                          = "".$displayString;
                        ///////////////////////////

                        echo "<input name='btnExport' onClick=\"location.href='_export.php'\" type='button' value='Export to Excel' />";
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>