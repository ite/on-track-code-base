<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    //if ($_SESSION["logged_in"] === true && !($_SESSION["usertype"] < "3"))
    //    header("Location: home.php");
        
    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    if ($_SESSION["email"] != "admin")
        header("Location: noaccess.php");

    if (!hasAccess("ACTION_MAN"))
        header("Location: noaccess.php");

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0", "vehicles");
?>
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script type="text/JavaScript">
    function validate_form( ){
        
        // More fields to be validated
        var description = document.assign_actions.action_name.value;
        var keyword = document.assign_actions.action_keyword.value;
        
        var valid = true;
        var errorString = '';
        
        if ( description == "" ){
                ShowLayer("actDesDiv", "block");
                valid = false;
        }else{
                ShowLayer("actDesDiv", "none");
                valid = true;
        }
        
        if(keyword == ""){
                ShowLayer("keywDiv", "block");
                valid = false;
        }else{
                ShowLayer("keywDiv", "none");
                valid = true;
        }
        
        //Form submit function
        function submitform()
        {
            document.assign_actions.submit();
        }
        
        // Submit form if valid
        if(valid){
            submitform();
        }
     }
</script>

<?php
        if($_POST['action_name'] && $_POST['action_keyword']){
            $actionName = $_POST['action_name'];
            $actionKeyword = $_POST['action_keyword'];
                
            $insert = q("INSERT INTO actions (action, descr,active) VALUES ('".$actionKeyword."', '".$actionName."', '1') ");           // Correct Query
            
            // Add a LOG of the addition
            $what = "Action: (".$actionKeyword.") added to the Actions table";
            $byUser = $_SESSION["email"];
            $onDate = date("Y-m-d");
            $onTime = date("h:i:s");
            $compId = $_SESSION["company_id"];
            
            $insert = q("INSERT INTO Logs (what,access,on_table,by_user,on_date,on_time,company_id) VALUES ( '".$what."','Insert','Actions','".$byUser."','".$onDate."','".$onTime."','".$compId."' )");
        }
    ?>

<?php
    $query = q('SELECT id FROM actions');
    foreach($query as $r){
        if ($_POST["action_".$r[0].""]){  
            $set_to_inactive = q("UPDATE actions SET active='0' WHERE id='".$r[0]."' AND active='1' ");
            // Add Querys to Delte from other tables as well !!!
            $actionKeyword  = q("SELECT action FROM actions WHERE id='".$r[0]."' ");
            // Add a LOG of the addition
            $what = "Action: (".$actionKeyword.") was set to Inactive";
            $byUser = $_SESSION["email"];
            $onDate = date("Y-m-d");
            $onTime = date("h:i:s");
            $compId = $_SESSION["company_id"];
            
            $insert = q("INSERT INTO Logs (what,access,on_table,by_user,on_date,on_time,company_id) VALUES ( '".$what."','Alter','Actions','".$byUser."','".$onDate."','".$onTime."','".$compId."' )");
        }
    }
?>
    
    <table width="90%">
        <tr height="380px">
            <td class='centerdata' valign="top">
                <form action="" method="post" name="assign_actions">
                    <table width="100%">
                        <tr>
                            <td class='centerdata'><h6>Action Management</h6></td>
                        </tr>
                        <?php if($_POST['action_name'] && $_POST['action_keyword']) echo "<tr><td class='centerdata'><br/><font style='color:#028200'> - Action has been added - </font></td></tr>"; ?>
                        <tr>
                            <td align="center">
                                <br/>
                            </td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">Action Description:</td>
                            <td><input class="on-field" name="action_name" size="30" type="text">
                            <div id="actDesDiv" style="display: none;"><font class="on-validate-error">* Please fill in a Action Description</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">Keyword (eg. EMP_EDIT):</td>
                            <td><input class="on-field" name="action_keyword" size="30" type="text">
                            <div id="keywDiv" style="display: none;"><font class="on-validate-error">* Please fill in a Keyword (eg. EMP_EDIT)</font></div>
                            </td>
                        </tr>
                        <tr class="on-description">
                            <td class='centerdata' colspan="2"><br><input name="btnAddAction" type="button" onclick="validate_form()" value="Add Action"><br><br></td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">Actions:</td>
                            <td width="50%">
                               <!-- -->
                               <?php
                                        $query = "SELECT id,descr,action FROM actions WHERE active='1' ";
                                        $result = q($query);
                                    ?>
                                    <table class="on-table on-lefttable" >      
                                        <th colspan="100%"></th>
                                    <?php
                                    $counter = 0;
                                    $colorCounter = 0;
                                    foreach($result as $r){
                                        if($colorCounter == 0){
                                            echo "<tr>
                                                        <td>" . $r[1] . "</td><td><input id='action_".$r[0]."' name='action_".$r[0]."' type='submit' value='Delete' /></td>
                                                    </tr>";
                                        }else if($colorCounter == 1){
                                            echo "<tr>
                                                        <td>" . $r[1] . "</td><td><input id='action_".$r[0]."' name='action_".$r[0]."' type='submit' value='Delete'/></td>
                                                    </tr>";
                                        }
                                        $counter++;
                                        $colorCounter ++;
                                        if($colorCounter ==2){
                                            $colorCounter = 0;
                                        }
                                    }
                                     echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                                    ?>
                                    </table>
                               <!-- -->
                            </td>
                        </tr>
                    </table>
                    
                </form>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
    
<?php
    //  Print Footer
    print_footer();
?>
