<?php
    //  Create Row Records
    function createRecords()
    {
        //  Set Table Border Details
        $bottom                                                         = "border-bottom: 1px solid orange";

        $recordDisplay                                                  = "";

        ///////////////////////////
        //  Get Item Infomation From Database
        ///////////////////////////
        //  nHeadings                                                   = Number of Headings
        $nHeadings                                                      = q("SELECT COUNT(DISTINCT(heading1)) FROM RSS");
        $headings                                                       = q("SELECT DISTINCT(heading1), date FROM RSS ORDER BY date DESC, time DESC");

        if ($nHeadings > 1)
        {
            foreach ($headings as $heading)
            {
                //  nRecords                                            = Number of Records
                $nRecords                                               = q("SELECT COUNT(id) FROM RSS WHERE heading1 = '".$heading[0]."'");
                $records                                                = q("SELECT heading2, heading3, description FROM RSS WHERE heading1 = '".$heading[0]."' ".
                                                                            "AND date = '".$heading[1]."' ORDER BY date DESC, time DESC");

                $recordDisplay                                          .= "<tr>\n<td style='".$bottom."'>\n<br/>\n<h3>\n".$heading[1]."\t\t".$heading[0].
                                                                            "\n</h3>\n<br/>\n";

                if ($nRecords > 1)
                {
                    foreach ($records as $record)
                    {
                        $h4                                             = "";

                        if ($record[0] != "")
                            $h4                                         .= "<h4>\n&nbsp;".$record[0];

                        if ($record[1] != "")
                            $h4                                         .= " - ".$record[1]."\n</h4>\n";
                        else
                            $h4                                         .= "</h4>\n";

                        $description                                    = "";

                        for ($i = 0; $i <= strlen($record[2]); $i++)
                        {
                            if (substr($record[2], $i, 1) == "\n")
                                $description                            .= "<br/>&nbsp;";
                            else
                                $description                            .= "".substr($record[2], $i, 1);
                        }

                        $recordDisplay                                  .= $h4."<p>\n&nbsp;".$description."\n</p>\n<br/>\n";
                    }
                }
                else if ($nRecords == 1)
                {
                    $h4                                                 = "";

                    if ($records[0][0] != "")
                        $h4                                             .= "<h4>\n&nbsp;".$records[0][0];

                    if ($records[0][1] != "")
                        $h4                                             .= " - ".$records[0][1]."\n</h4>\n";
                    else
                        $h4                                             .= "</h4>\n";

                    $description                                        = "";

                    for ($i = 0; $i <= strlen($records[0][2]); $i++)
                    {
                        if (substr($records[0][2], $i, 1) == "\n")
                            $description                                .= "<br/>&nbsp;";
                        else
                            $description                                .= "".substr($records[0][2], $i, 1);
                    }

                    $recordDisplay                                      .= $h4."<p>\n&nbsp;".$description."\n</p>\n<br/>\n";
                }

                $recordDisplay                                          .= "</td>\n</tr>\n";
            }
        }
        else if ($nHeadings == 1)
        {
            //  nRecords                                                = Number of Records
            $nRecords                                                   = q("SELECT COUNT(id) FROM RSS WHERE heading1 = '".$headings."'");
            $records                                                    = q("SELECT heading2, heading3, description FROM RSS WHERE heading1 = '".$headings[0][0]."' ".
                                                                            "AND date = '".$headings[0][1]."' ORDER BY date DESC, time DESC");

            $recordDisplay                                              .= "<tr>\n<td style='".$bottom."'>\n<br/>\n<h3>\n".$headings[0][1]."\t\t".$headings[0][0].
                                                                            "\n</h3>\n<br/>\n";

            if ($nRecords > 1)
            {
                foreach ($records as $record)
                {
                    $h4                                                 = "";

                    if ($record[0] != "")
                        $h4                                             .= "<h4>\n&nbsp;".$record[0];

                    if ($record[1] != "")
                        $h4                                             .= " - ".$record[1]."\n</h4>\n";
                    else
                        $h4                                             .= "</h4>\n";

                    $description                                        = "";

                    for ($i = 0; $i <= strlen($record[2]); $i++)
                    {
                        if (substr($record[2], $i, 1) == "\n")
                            $description                                .= "<br/>&nbsp;";
                        else
                            $description                                .= "".substr($record[2], $i, 1);
                    }

                    $recordDisplay                                      .= $h4."<p>\n&nbsp;".$description."\n</p>\n<br/>\n";
                }
            }
            else if ($nRecords == 1)
            {
                $h4                                                     = "";

                if ($records[0][0] != "")
                    $h4                                                 .= "<h4>\n&nbsp;".$records[0][0];

                if ($records[0][1] != "")
                    $h4                                                 .= " - ".$records[0][1]."\n</h4>\n";
                else
                    $h4                                                 .= "</h4>\n";

                $description                                            = "";

                for ($i = 0; $i <= strlen($records[0][2]); $i++)
                {
                    if (substr($records[0][2], $i, 1) == "\n")
                        $description                                    .= "<br/>&nbsp;";
                    else
                        $description                                    .= "".substr($records[0][2], $i, 1);
                }

                $recordDisplay                                          .= $h4."<p>\n&nbsp;".$description."\n</p>\n<br/>\n";
            }

            $recordDisplay                                              .= "</td>\n</tr>\n";
        }
        else
        {
            $recordDisplay                                              .= "<tr>\n<td>\n".
                                                                            "<p>\n<br\>\nNo news/updates to display...<br/>\n".
                                                                            "</p>\n</td>\n</tr>\n";
        }

        return $recordDisplay;
    }

    //  Create XML Code For RSS Feed
    function createNewsDisplay()
    {
        $displayString                                                  = "";

        $displayString                                                  .= "<table style='padding:4px 4px 4px 4px;margin:4px 4px 4px 4px;' width='100%'>\n";

        //  Create Channel
        $displayString                                                  .= "<tr>\n<td width='100%'>\n<h1>\nOn-Track - News Page\n</h1>\n</td>\n</tr>\n";
        $displayString                                                  .= "<tr>\n<td width='100%'>\n<h2>\nLatest News/Updates on On-Track\n</h2>\n</td>\n</tr>\n";

        //  Get Items
        $displayString                                                  .= createRecords();

        $displayString                                                  .= "</table>";

        //  Write XML Code For RSS Feed To File
        $file_handling                                                  = fopen("_news.php", "w");
        fwrite($file_handling, $displayString);
        fclose($file_handling);

        return;
    }
?>