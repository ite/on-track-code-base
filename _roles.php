<?php
    include("_db.php");
    include("_dates.php");
?>
<html>
    <head>
        <title>
            On-Track - Recovery Script...
        </title>
    </head>
    <body>
        <h1 align="center">
            Install - Roles
        </h1>
        <?php
/*
            //  Roles To Which Users Can Be Assigned
            $drop = q("DROP TABLE IF EXISTS roles");
            $create = q("CREATE TABLE roles ( ".
                        "id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,".
                        "companyid SMALLINT UNSIGNED NOT NULL DEFAULT 0,".
                        "role VARCHAR(12),".
                        "descr VARCHAR(255),".
                        "active TINYINT(3) DEFAULT 1)");

            $insert = q("INSERT INTO roles(role,descr) VALUES ('ADM','Admin')");
            $insert = q("INSERT INTO roles(role,descr) VALUES ('MAN','Manager')");
            $insert = q("INSERT INTO roles(role,descr) VALUES ('SPM','Shared Project Manager')");
            $insert = q("INSERT INTO roles(role,descr) VALUES ('PM','Project Manager')");
            $insert = q("INSERT INTO roles(role,descr) VALUES ('FM','Financial Manager')");
            $insert = q("INSERT INTO roles(role,descr) VALUES ('TM','Type Manager')");
            $insert = q("INSERT INTO roles(role,descr) VALUES ('CL','Client')");
            $insert = q("INSERT INTO roles(role,descr) VALUES ('CA','Company Administrator')");

            //  Actions Defined To Be Assigned To Roles
            $drop = q("DROP TABLE IF EXISTS actions");
            $create = q("CREATE TABLE actions ( ".
                        "id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,".
                        "action VARCHAR(20),".
                        "descr VARCHAR(255),".
                        "active TINYINT(3) DEFAULT 1)");

            $a[] = "'CHNG_PW','Change Password'";
            $a[] = "'EULA','End-User Licence Agreement'";
            $a[] = "'TIME_MANAGE','Manage own time'";
            $a[] = "'EXPENSE_MANAGE_OWN','Manage own expenses'";
            $a[] = "'TIME_MANAGE_ALL','Manage all time'";
            $a[] = "'EXPENSE_MANAGE_ALL','Manage all expenses'";
            $a[] = "'LEAVE_MANAGE_OWN','Manage own leave'";
            $a[] = "'SHAREDRES_MANAGE_OWN','Manage own shared resources'";
            $a[] = "'BOOKING_MANAGE','Edit and remove own bookings'";
            $a[] = "'LOGS_VIEW_OWN','View own logs'";
            $a[] = "'REP_TIME_EX','Report time and expense'";
            $a[] = "'SHAREDRES_MANAGE','Manage shared resources'";
            $a[] = "'SHAREDRES_BOOK','Book Shared Resources'";
            $a[] = "'VEH_MANAGE','Manage vehicles'";
            $a[] = "'RA_MANAGEMENT','Role and Action Management'";
            $a[] = "'AREA_MANAGE','Manage project areas'";
            $a[] = "'ATYPE_MANAGE','Manage areas, activityTypes and Activities'";
            $a[] = "'PROJTYPE_MANAGE','Manage project types'";
            $a[] = "'BUDGET_CAT_MANAGE','Manage budget categories'";
            $a[] = "'BUDGET_MANAGE','Manage budget'";
            $a[] = "'INVOICE_MANAGE','Manage invoices'";
            $a[] = "'EXPENSE_MANAGE','Manage expenses'";
            $a[] = "'EMP_ADD_REMOVE','Add and remove employees'";
            $a[] = "'EMP_CTC_MANAGE','Manage employee cost to company'";
            $a[] = "'EMP_TARIFF_MANAGE','Manage employee tariffs'";
            $a[] = "'PROJ_MANAGE','Manage projects'";
            $a[] = "'EMP_EDIT','Edit current employees. Cannot view cost to company or tariffs'";
            $a[] = "'SLA_MANAGE','Manage SLA'";
            $a[] = "'PRINTING_MANAGE','Manage printing'";
            $a[] = "'APPROVAL_MANAGE','Approval module'";
            $a[] = "'INVOICE_VIEW','View and edit date and paid fields on invoices'";
            $a[] = "'EXPENSE_VIEW','View and edit date and paid fields on expenses'";
            $a[] = "'BUDGET_VIEW','View budget'";
            $a[] = "'LOGS_VIEW_ALL','View all logs'";
            $a[] = "'LEAVE_MANAGE_ALL','Manage all leave'";
            $a[] = "'SPM_MANAGE','Shared Project module'";
            $a[] = "'REP_TIME_SHEET','Report time sheet'";
            $a[] = "'REP_EX_SHEET','Report expense sheet'";
            $a[] = "'REP_ACTIVE_PROJ','Report active projects'";
            $a[] = "'REP_PROJ_BREAK','Report project breakdown'";
            $a[] = "'REP_PROJ_INFO','Report project info'";
            $a[] = "'REP_PROJECT','Report project'";
            $a[] = "'REP_PROJ_LIST','Report project list'";
            $a[] = "'REP_PROGRESS','Report progress'";
            $a[] = "'REP_WORK_IN_PR','Report work in process'";
            $a[] = "'REP_WORK_IN_PR_M','Report work in process months'";
            $a[] = "'REP_SUMMARY','Report summary'";
            $a[] = "'REP_SUMMARY_EX','Report summary expenses'";
            $a[] = "'REP_SUMMARY_TIME','Report summary time'";
            $a[] = "'REP_TYPE_SUM','Report summary type'";
            $a[] = "'REP_LEAVE','Report leave'";
            $a[] = "'REP_CASH_FLOW','Report cash flow forecast'";
            $a[] = "'REP_IN_VS_CTC','Report invoiced vs cost to company'";
            $a[] = "'REP_INCOME_FORECAST','Report income forecast'";
            $a[] = "'REP_IN_FORE_INVOICE','Report income forecast invoice'";
            $a[] = "'REP_IN_FORE_TIME','Report income forecast time'";
            $a[] = "'REP_GROWTH_ANALYSIS','Report growth analysis'";
            $a[] = "'REP_BUDGET','Report budget'";
            $a[] = "'REP_TIME_COM',' Report time comparison'";
            $a[] = "'REP_PROJ_TYPE_COM','Report project type comparison'";
            $a[] = "'REP_RUNNING_PROF','Report running profit'";
            $a[] = "'REP_LOADED_INV','Report loaded invoices'";
            $a[] = "'REP_INVOICE_TRACK','Report invoice tracking'";
            $a[] = "'REP_SP_TIME_SHEET','Report shared project time sheet'";
            $a[] = "'REP_SP_EX_SHEET','Report shared project expense sheet'";
            $a[] = "'REP_SP_SUMMARY_TIME','Report shared project summary time'";
            $a[] = "'REP_SP_SUMMARY_EX','Report shared project summary expense'";
            $a[] = "'REP_SP_BREAK','Report shared project breakdown'";
            $a[] = "'REP_SP_SUMMARY','Report shared project summary'";
            $a[] = "'REP_SP_INV','Report shared project loaded invoices'";
            $a[] = "'REP_BUDGET_PERIOD','Report budget period'";
            //$a[] = "'REP_ANALYSIS','Report analysis'";
            //$a[] = "'REP_GANTT','Report gantt'";
            //$a[] = "'REP_TIME_PROJ','Report time project'";
            $a[] = "'REP_COMPANY_BREAK','Report company breakdown'";
            $a[] = "'REP_COMPANY_SUMM','Report company summary'";

            $a[] = "'STATEMENT_MAN','Manage Statements'";
            $a[] = "'COMP_MAN','Company Management'";
            $a[] = "'RESOURCE_MAN','Resource Management'";
            $a[] = "'MODULE_MAN','Module Management'";
            $a[] = "'ACTION_MAN','Action Management'";
            $a[] = "'ROLE_MAN','Role Management'";
            $a[] = "'NEWS_MAN','News Management'";
            $a[] = "'COMP_ASSIGN','Company Assignment'";
            $a[] = "'ACTION_ASSIGN','Action Assignment'";
            $a[] = "'ROLE_ASSIGN','Role Assignment'";
            $a[] = "'RESOURCE_ASSIGN','Resource Assignment'";
            $a[] = "'REP_CASH_FLOW2','Report Cash Flow Forecast V2'";
            $a[] = "'INVOICE_MANAGE_NEW','Manage invoices [NEW]'";
            $a[] = "'INVOICE_VIEW_NEW','View and edit date and paid fields on invoices [NEW]'";
            $a[] = "'REP_ANALYSIS','Report analysis'";
            $a[] = "'CLIENT_MANAGE','Manage Client(s)'";
            $a[] = "'ACCOUNT_MANAGE','Manage Bank Account(s)'";
            $a[] = "'INVOICE_PROCESS','Invoice Processing'";
            $a[] = "'REP_INV_VS_CTC_OWN','Report own invoiced vs cost to company'";
            $a[] = "'REP_INV_VS_CTC_OWN_V2','Report own invoiced vs cost to company [Version 2]'";
            $a[] = "'REP_IN_VS_CTC_V2','Report invoiced vs cost to company [Version 2]'";
            $a[] = "'REP_LOGBOOK','Logbook Report'";
            $a[] = "'PAYMENT_CERTIFICATE','Invoice and payment certificates'";
            $a[] = "'REP_OVER_BUD_PROJ_CONS','Report project(s) over budget - consulting based'";
*/
            $a[] = "'VEHICLE_ASSIGN','Vehicle Assignment'";
            $a[] = "'LOCATION_MANAGEMENT','Location Management'";
            $a[] = "'GPS_LOGBOOK_MANAGEMENT','GPS Logbook Management'";

            sort($a);

            foreach ($a as $d)  {
                $tmp = preg_split("/,/",$d);
                $action = $tmp[0];
                $descr = $tmp[1];

                if (!exist("actions","action = ".$action." AND descr = ".$descr.""))
                    $insert = q("INSERT INTO actions(action,descr) VALUES ($d)");
            }

/*
            //  BRIDGE roles -> actions
            $drop = q("DROP TABLE IF EXISTS role_action");
            $create = q("CREATE TABLE role_action ( ".
                        "id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,".
                        "companyid SMALLINT UNSIGNED NOT NULL,".
                        "roleid SMALLINT UNSIGNED NOT NULL,".
                        "actionid SMALLINT UNSIGNED NOT NULL)");

            $companies = q("SELECT id, name FROM Company ORDER BY id");

            if (is_array($companies))   {
                //  Defaults
                $cc = "ADM";
                $a = q("SELECT action FROM actions ORDER BY id");
                foreach ($a as $d)      $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES (0,(SELECT id FROM roles WHERE role = '$cc'),(SELECT id FROM actions WHERE action = '".$d[0]."'))");

                $cc = "MAN";
                $a = array("EMP_TARIFF_MANAGE","EMP_CTC_MANAGE","LEAVE_MANAGE_ALL","LOGS_VIEW_ALL","AREA_MANAGE","REP_TIME_SHEET","REP_EX_SHEET","REP_ACTIVE_PROJ","REP_PROJ_BREAK","REP_PROJ_INFO","REP_TYPE_SUM","REP_PROJ_LIST",
                            "REP_PROGRESS","REP_WORK_IN_PR","REP_WORK_IN_PR_M","REP_SUMMARY","REP_SUMMARY_EX","REP_SUMMARY_TIME","REP_LEAVE","REP_CASH_FLOW","REP_IN_VS_CTC","REP_INCOME_FORECAST","REP_IN_FORE_INVOICE",
                            "REP_IN_FORE_TIME","REP_GROWTH_ANALYSIS","REP_BUDGET","REP_BUDGET_PERIOD","REP_TIME_COM","REP_PROJ_TYPE_COM","REP_RUNNING_PROF","REP_LOADED_INV","REP_INVOICE_TRACK","REP_SP_TIME_SHEET","REP_SP_EX_SHEET",
                            "REP_SP_SUMMARY_TIME","REP_SP_SUMMARY_EX","REP_SP_BREAK","REP_SP_SUMMARY","REP_SP_INV");
                sort($a);
                foreach ($a as $d)      $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES (0,(SELECT id FROM roles WHERE role = '$cc'),(SELECT id FROM actions WHERE action = '$d'))");

                $cc = "SPM";
                $a = array("SPM_MANAGE","REP_SP_TIME_SHEET","REP_SP_EX_SHEET","REP_SP_SUMMARY_TIME","REP_SP_SUMMARY_EX","REP_SP_BREAK","REP_SP_SUMMARY","REP_SP_INV","REP_LEAVE");
                sort($a);
                foreach ($a as $d)      $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES (0,(SELECT id FROM roles WHERE role = '$cc'),(SELECT id FROM actions WHERE action = '$d'))");

                $cc = "PM";
                $a = array("EMP_TARIFF_MANAGE","PROJ_MANAGE","SLA_MANAGE","APPROVAL_MANAGE","INVOICE_VIEW","EXPENSE_VIEW","VEH_MANAGE","PRINTING_MANAGE","AREA_MANAGE","REP_LEAVE","REP_TIME_SHEET","REP_EX_SHEET",
                            "REP_ACTIVE_PROJ","REP_PROJ_BREAK","REP_PROJ_INFO","REP_TYPE_SUM","REP_PROJ_LIST","REP_PROGRESS","REP_WORK_IN_PR","REP_WORK_IN_PR_M","REP_SUMMARY","REP_SUMMARY_EX","REP_SUMMARY_TIME");
                sort($a);
                foreach ($a as $d)      $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES (0,(SELECT id FROM roles WHERE role = '$cc'),(SELECT id FROM actions WHERE action = '$d'))");

                $cc = "FM";
                $a = array("BUDGET_CAT_MANAGE","BUDGET_MANAGE","INVOICE_MANAGE","EXPENSE_MANAGE","EMP_TARIFF_MANAGE","EMP_CTC_MANAGE","REP_LEAVE","REP_CASH_FLOW","REP_IN_VS_CTC","REP_INCOME_FORECAST",
                            "REP_IN_FORE_INVOICE","REP_IN_FORE_TIME","REP_GROWTH_ANALYSIS","REP_BUDGET","REP_BUDGET_PERIOD","REP_TIME_COM","REP_PROJ_TYPE_COM","REP_RUNNING_PROF","REP_LOADED_INV","REP_INVOICE_TRACK");
                sort($a);
                foreach ($a as $d)      $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES (0,(SELECT id FROM roles WHERE role = '$cc'),(SELECT id FROM actions WHERE action = '$d'))");

                $cc = "TM";
                $a = array("AREA_MANAGE","ATYPE_MANAGE","PROJTYPE_MANAGE");
                sort($a);
                foreach ($a as $d)      $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES (0,(SELECT id FROM roles WHERE role = '$cc'),(SELECT id FROM actions WHERE action = '$d'))");

                $cc = "CL";
                $a = array("CHNG_PW","EULA","TIME_MANAGE","EXPENSE_MANAGE_OWN","LEAVE_MANAGE_OWN","SHAREDRES_BOOK","LOGS_VIEW_OWN","BOOKING_MANAGE");
                sort($a);
                foreach ($a as $d)      $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES (0,(SELECT id FROM roles WHERE role = '$cc'),(SELECT id FROM actions WHERE action = '$d'))");

                $cc = "CA";
                $a = array("VEH_MANAGE","PRINTING_MANAGE","REP_LEAVE","EMP_ADD_REMOVE","EMP_TARIFF_MANAGE","EMP_CTC_MANAGE","EMP_EDIT");
                sort($a);
                foreach ($a as $d)      $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES (0,(SELECT id FROM roles WHERE role = '$cc'),(SELECT id FROM actions WHERE action = '$d'))");

                $cc = "ADM";
                $a = array("COMP_MAN","RESOURCE_MAN","MODULE_MAN","ACTION_MAN","ROLE_MAN","NEWS_MAN","COMP_ASSIGN","ACTION_ASSIGN","ROLE_ASSIGN","RESOURCE_ASSIGN");
                sort($a);

                foreach ($a as $d)      $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES (0,(SELECT id FROM roles WHERE role = '$cc'),(SELECT id FROM actions WHERE action = '$d'))");

                foreach ($companies as $c)      {
                    foreach ($a as $d)  $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES ('".$c[0]."',(SELECT id FROM roles WHERE role = '$cc'),(SELECT id FROM actions WHERE action = '$d'))");
                }

                //foreach ($companies as $c)      $insert = q("INSERT INTO role_action (companyid, roleid, actionid) SELECT ".$c[0].", roleid, actionid FROM role_action WHERE companyid = 0");
            }
*/
            //$insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES (1,(SELECT id FROM roles WHERE role = 'MAN'),(SELECT id FROM actions WHERE action = 'INVOICE_MANAGE_NEW'))");
            //$insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES (1,(SELECT id FROM roles WHERE role = 'MAN'),(SELECT id FROM actions WHERE action = 'INVOICE_VIEW_NEW'))");
            //$delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'CL') AND actionid = (SELECT id FROM actions WHERE action = 'REP_TIME_EX')");
/*
            $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'MAN') AND actionid = (SELECT id FROM actions WHERE action = 'NEWS_MAN')");
            $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'SPM') AND actionid = (SELECT id FROM actions WHERE action = 'NEWS_MAN')");
            $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'PM') AND actionid = (SELECT id FROM actions WHERE action = 'NEWS_MAN')");
            $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'FM') AND actionid = (SELECT id FROM actions WHERE action = 'NEWS_MAN')");
            $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'TM') AND actionid = (SELECT id FROM actions WHERE action = 'NEWS_MAN')");
            $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'CL') AND actionid = (SELECT id FROM actions WHERE action = 'NEWS_MAN')");
            $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'CA') AND actionid = (SELECT id FROM actions WHERE action = 'NEWS_MAN')");
            $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES (1,(SELECT id FROM roles WHERE role = 'MAN'),(SELECT id FROM actions WHERE action = 'NEWS_MAN'))");

            // Default roles for managers
            $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'MAN') AND actionid = (SELECT id FROM actions WHERE action = 'EMP_ADD_REMOVE') AND companyid ='0' ");
            $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES ('0',(SELECT id FROM roles WHERE role = 'MAN'),(SELECT id FROM actions WHERE action = 'EMP_ADD_REMOVE'))");

            $companies = q("SELECT id, name FROM Company ORDER BY id");
            if(is_array($companies))    {
                foreach($companies as $c)       {
                    $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'MAN') AND actionid = (SELECT id FROM actions WHERE action = 'EMP_ADD_REMOVE') AND companyid = '".$c[0]."'");
                    $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES ('".$c[0]."',(SELECT id FROM roles WHERE role = 'MAN'),(SELECT id FROM actions WHERE action = 'EMP_ADD_REMOVE'))");
                }
            }
*/
/*
            // Default Roles
            $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES ('0',(SELECT id FROM roles WHERE role = 'MAN'),(SELECT id FROM actions WHERE action = 'REP_TIME_EX'))");
            $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES ('0',(SELECT id FROM roles WHERE role = 'PM'),(SELECT id FROM actions WHERE action = 'REP_TIME_EX'))");

            $companies = q("SELECT id, name FROM Company ORDER BY id");
            if(is_array($companies))    {
                foreach($companies as $c)       {
                    $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'MAN') AND actionid = (SELECT id FROM actions WHERE action = 'REP_TIME_EX') AND companyid = '".$c[0]."'");
                    $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'PM') AND actionid = (SELECT id FROM actions WHERE action = 'REP_TIME_EX') AND companyid = '".$c[0]."'");
                    $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES ('".$c[0]."',(SELECT id FROM roles WHERE role = 'MAN'),(SELECT id FROM actions WHERE action = 'REP_TIME_EX'))");
                    $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES ('".$c[0]."',(SELECT id FROM roles WHERE role = 'PM'),(SELECT id FROM actions WHERE action = 'REP_TIME_EX'))");
                }
            }

            // Default Roles
            $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'MAN') AND actionid = (SELECT id FROM actions WHERE action = 'PROJ_MANAGE') AND companyid ='0' ");
            $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'PM') AND actionid = (SELECT id FROM actions WHERE action = 'PROJ_MANAGE') AND companyid ='0' ");
            $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES ('0',(SELECT id FROM roles WHERE role = 'MAN'),(SELECT id FROM actions WHERE action = 'PROJ_MANAGE'))");
            $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES ('0',(SELECT id FROM roles WHERE role = 'PM'),(SELECT id FROM actions WHERE action = 'PROJ_MANAGE'))");

            $companies = q("SELECT id, name FROM Company ORDER BY id");
            if(is_array($companies))    {
                foreach($companies as $c)       {
                    $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'MAN') AND actionid = (SELECT id FROM actions WHERE action = 'PROJ_MANAGE') AND companyid = '".$c[0]."'");
                    $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'PM') AND actionid = (SELECT id FROM actions WHERE action = 'PROJ_MANAGE') AND companyid = '".$c[0]."'");
                    $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES ('".$c[0]."',(SELECT id FROM roles WHERE role = 'MAN'),(SELECT id FROM actions WHERE action = 'PROJ_MANAGE'))");
                    $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES ('".$c[0]."',(SELECT id FROM roles WHERE role = 'PM'),(SELECT id FROM actions WHERE action = 'PROJ_MANAGE'))");
                }
            }

            // Default Roles
            $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'PM') AND actionid = (SELECT id FROM actions WHERE action = 'SLA_MANAGE') AND companyid ='0' ");

            $companies = q("SELECT id, name FROM Company ORDER BY id");
            if(is_array($companies))    {
                foreach($companies as $c)       {
                    if ($c[0] != "1")
                        $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'PM') AND actionid = (SELECT id FROM actions WHERE action = 'SLA_MANAGE') AND companyid = '".$c[0]."'");
                }
            }

            // Default Roles
            $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'MAN') AND actionid = (SELECT id FROM actions WHERE action = 'RESOURCE_MAN') AND companyid ='0' ");
            $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES ('0',(SELECT id FROM roles WHERE role = 'MAN'),(SELECT id FROM actions WHERE action = 'RESOURCE_MAN'))");

            $companies = q("SELECT id, name FROM Company ORDER BY id");
            if(is_array($companies))    {
                foreach($companies as $c)       {
                    $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'MAN') AND actionid = (SELECT id FROM actions WHERE action = 'RESOURCE_MAN') AND companyid = '".$c[0]."'");
                    $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES ('".$c[0]."',(SELECT id FROM roles WHERE role = 'MAN'),(SELECT id FROM actions WHERE action = 'RESOURCE_MAN'))");
                }
            }

            // Default Roles
            $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'MAN') AND actionid = (SELECT id FROM actions WHERE action = 'ACTION_ASSIGN') AND companyid ='0' ");
            $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'MAN') AND actionid = (SELECT id FROM actions WHERE action = 'ROLE_ASSIGN') AND companyid ='0' ");
            $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'MAN') AND actionid = (SELECT id FROM actions WHERE action = 'ROLE_MAN') AND companyid ='0' ");
            $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES ('0',(SELECT id FROM roles WHERE role = 'MAN'),(SELECT id FROM actions WHERE action = 'ACTION_ASSIGN'))");
            $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES ('0',(SELECT id FROM roles WHERE role = 'MAN'),(SELECT id FROM actions WHERE action = 'ROLE_ASSIGN'))");
            $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES ('0',(SELECT id FROM roles WHERE role = 'MAN'),(SELECT id FROM actions WHERE action = 'ROLE_MAN'))");

            $companies = q("SELECT id, name FROM Company ORDER BY id");
            if(is_array($companies))    {
                foreach($companies as $c)       {
                    $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'MAN') AND actionid = (SELECT id FROM actions WHERE action = 'ACTION_ASSIGN') AND companyid = '".$c[0]."'");
                    $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'MAN') AND actionid = (SELECT id FROM actions WHERE action = 'ROLE_ASSIGN') AND companyid = '".$c[0]."'");
                    $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'MAN') AND actionid = (SELECT id FROM actions WHERE action = 'ROLE_MAN') AND companyid = '".$c[0]."'");
                    $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES ('".$c[0]."',(SELECT id FROM roles WHERE role = 'MAN'),(SELECT id FROM actions WHERE action = 'ACTION_ASSIGN'))");
                    $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES ('".$c[0]."',(SELECT id FROM roles WHERE role = 'MAN'),(SELECT id FROM actions WHERE action = 'ROLE_ASSIGN'))");
                    $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES ('".$c[0]."',(SELECT id FROM roles WHERE role = 'MAN'),(SELECT id FROM actions WHERE action = 'ROLE_MAN'))");
                }
            }

            $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES (1,(SELECT id FROM roles WHERE role = 'MAN' AND companyid = '1'),(SELECT id FROM actions WHERE action = 'ACCOUNT_MANAGE'))");
            $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES (1,(SELECT id FROM roles WHERE role = 'MAN' AND companyid = '1'),(SELECT id FROM actions WHERE action = 'CLIENT_MANAGE'))");
            $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES (1,(SELECT id FROM roles WHERE role = 'FM' AND companyid = '1'),(SELECT id FROM actions WHERE action = 'INVOICE_PROCESS'))");
*/

            //$insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES (1,(SELECT id FROM roles WHERE role = 'MAN' AND companyid = '1'),(SELECT id FROM actions WHERE action = 'REP_IN_VS_CTC_V2'))");
            //$insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES (1,(SELECT id FROM roles WHERE role = 'FM' AND companyid = '1'),(SELECT id FROM actions WHERE action = 'REP_IN_VS_CTC_V2'))");
            //$insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES (1,(SELECT id FROM roles WHERE role = 'CL' AND companyid = '1'),(SELECT id FROM actions WHERE action = 'REP_INV_VS_CTC_OWN_V2'))");
			//$insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES (1,(SELECT id FROM roles WHERE role = 'CL' AND companyid = '1'),(SELECT id FROM actions WHERE action = 'REP_LOGBOOK'))");
			//$insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES (1,(SELECT id FROM roles WHERE role = 'MAN' AND companyid = '1'),(SELECT id FROM actions WHERE action = 'PAYMENT_CERTIFICATE'))");
			//$insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES (1,(SELECT id FROM roles WHERE role = 'FM' AND companyid = '1'),(SELECT id FROM actions WHERE action = 'PAYMENT_CERTIFICATE'))");

/*
            //  BRIDGE users -> roles
            $drop = q("DROP TABLE IF EXISTS user_role");
            $create = q("CREATE TABLE user_role ( ".
                        "id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,".
                        "companyid SMALLINT UNSIGNED NOT NULL,".
                        "userid SMALLINT UNSIGNED NOT NULL,".
                        "roleid SMALLINT UNSIGNED NOT NULL,".
                        "dateupdated CHAR(20),".
                        "byuser VARCHAR(60))");

            $companies = q("SELECT id, name FROM Company ORDER BY id");

            //  Usertypes Array With Default Roles Per Usertype
            $ut[1] = array("MAN","SPM","PM","FM","CL");//Manager
            $ut[2] = array("PM","CL");//Project Manager
            $ut[3] = array("FM","CL");//Financial Manager
            $ut[4] = array("CL");//Client

            if (is_array($companies))   {
                foreach ($companies as $c)      {
                    $insert = q("INSERT INTO user_role(companyid,userid,roleid) VALUES (".$c[0].",(SELECT id FROM Employee WHERE email = 'admin'),(SELECT id FROM roles WHERE role = 'ADM'))");

                    $users = q("SELECT e.id,e.usertype FROM (Employee AS e INNER JOIN Company_Users AS cu ON e.id = cu.user_id) WHERE cu.company_id = '".$c[0]."' AND e.email != 'admin' ORDER BY e.id");

                    if (is_array($users))       {
                        foreach ($users as $u)  {
                            if (is_array($ut[$u[1]]))   {
                                foreach ($ut[$u[1]] as $d)      $insert = q("INSERT INTO user_role(companyid,userid,roleid) VALUES (".$c[0].",".$u[0].",(SELECT id FROM roles WHERE role = '$d'))");
                            }
                        }
                    }
                }
            }
*/
        ?>
        <form action="index.php" method="post">
            <center><input type="submit" value="Return"/></center>
        </form>
    </body>
</html>
