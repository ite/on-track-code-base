<?php
    include("_db.php");
    include("_dates.php");

    $scriptStart = my_microtime();
?>
<html>
    <head>
        <title>
            On-Track - Script...
        </title>
    </head>
    <body>
        <h1 align="center">
            Script - Over Budget/Due Date Check [Cron Job]
        </h1>
        <?php
            $date = date("Y-m-d");

            //$alter = q("ALTER TABLE companiesOnTeam ADD COLUMN OBEmailSent TINYINT UNSIGNED NOT NULL DEFAULT 0 AFTER deleted");
            //$alter = q("ALTER TABLE companiesOnTeam ADD COLUMN overBudget TINYINT UNSIGNED NOT NULL DEFAULT 0 AFTER deleted");
            //$update = q("UPDATE companiesOnTeam SET overBudget = '0'");

            $companies = q("SELECT id, name FROM Company ORDER BY id");

            if (is_array($companies))   {
                foreach ($companies as $c)      {
                    echo $c[1]."<br/>";

                    $projects = q("SELECT DISTINCT(p.id), p.name, p.due_date FROM (Project AS p INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) ".
                                    "WHERE p.completed = '0' AND p.deleted = '0' AND cot.status = '1' AND cot.deleted = '0' AND cot.overBudget = '0' AND cot.company_id = '".$c[0]."' ORDER BY UPPER(p.name)");

                    if (is_array($projects))    {
                        foreach ($projects as $p)       {
                            $totalBudget = q("SELECT total_budget FROM companiesOnTeam WHERE project_id = '".$p[0]."' AND company_id = '".$c[0]."'");

                            if (is_numeric($totalBudget) && $totalBudget != "") {
                                $sumTime = q("SELECT SUM(rate * time_spent) FROM TimeSheet WHERE project_id = '".$p[0]."' AND company_id = '".$c[0]."'");
                                $sumExpense = q("SELECT SUM(expense) FROM ExpenseSheet WHERE project_id = '".$p[0]."' AND company_id = '".$c[0]."'");

                                if (number_format($totalBudget, 2, ".", "") <= number_format(($sumTime + $sumExpense), 2, ".", ""))     {
                                    echo $p[1]." - Over Budget<br/>";
                                    $update = q("UPDATE companiesOnTeam SET overBudget = '1' WHERE project_id = '".$p[0]."' AND company_id = '".$c[0]."'");
                                }

                                if ($p[2] < $date && $p[2] != "")
                                    echo $p[1]." - Over Due Date. Due Date [".$p[2]."]<br/>";
                            }
                        }
                    }

                    echo "<br/>";
                }
            }

            echo "Total Time  [".number_format(round(my_microtime()-$scriptStart,3),3)."s]<br/>";

            echo "<p align='center'>Rates/Company script done executing</p>";
        ?>
        <form action="index.php" method="post">
            <center><input type="submit" value="Return"/></center>
        </form>
    </body>
</html>
