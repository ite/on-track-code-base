<?php
    include("_db.php");
    include("_dates.php");

    $_SESSION["email"] = "admin";
?>
<html>
    <head>
        <title>
            On-Track - Menus...
        </title>
    </head>
    <body>
        <?php
            $menus = array();

            function getSubMenus($parentID) {
                global $menus;

                $subMenus = q("SELECT id,parentID,level,ranking,urlLink,menuDescr,icon,CONCAT(menuDescr,'[',urlLink ,']') FROM menus WHERE parentID = '".$parentID."' AND menuType = 'MAIN' ORDER BY ranking");

                if (is_array($subMenus))        {
                    foreach ($subMenus as $subMenu)     {
                        $actions = q("SELECT a.id, a.action FROM (menuAction AS ma INNER JOIN actions AS a ON a.id = ma.actionID) WHERE ma.menuID = '".$subMenu[0]."'");

                        if ($_SESSION["email"] === "admin")  {
                            if ($subMenu[5] != "Report Own Time/Expenses")
                                $menus[] = $subMenu;

                            getSubMenus($subMenu[0]);
                        }
                        else if (is_array($actions))   {
                            foreach ($actions as $a)  {
                                if (hasAccess($a[1]))   {
                                    $menus[] = $subMenu;

                                    getSubMenus($subMenu[0]);
                                }
                            }
                        }
                    }
                }

                return;
            }

            $parentID = 0;

            $menuItems = q("SELECT id,parentID,level,ranking,urlLink,menuDescr,icon,CONCAT(menuDescr,'[',urlLink ,']') FROM menus WHERE parentID = '".$parentID."' AND menuType = 'MAIN' ORDER BY ranking");

            if (is_array($menuItems))   {
                foreach ($menuItems as $menuItem)  {
                    $actions = q("SELECT a.id, a.action FROM (menuAction AS ma INNER JOIN actions AS a ON a.id = ma.actionID) WHERE ma.menuID = '".$menuItem[0]."'");

                    if ($_SESSION["email"] === "admin")  {
                        if ($menuItem[5] != "Report Own Time/Expenses")
                            $menus[] = $menuItem;

                        getSubMenus($menuItem[0]);
                    }
                    else if (is_array($actions))   {
                        foreach ($actions as $a)  {
                            if (hasAccess($a[1]))       {
                                $menus[] = $menuItem;

                                getSubMenus($menuItem[0]);
                            }
                        }
                    }
                }
            }

            $data = array();

            foreach ($menus as $m)
                if (!in_array($m, $data))
                    $data[] = $m;

            $menus = $data;

            foreach ($menus as $m)      {
                for ($i = 0; $i <= (2 * $m[2]); $i++) echo "&nbsp;&nbsp;&nbsp;";

                echo $m[5]."<br/>";
            }
        ?>
</body>
</html>
