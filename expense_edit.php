<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("_functions.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

	if (!hasAccess("EXPENSE_MANAGE"))
		header("Location: noaccess.php");

    $project_id                                                         = "null";

    if (isset($_POST["project"]))
        $project_id                                                     = $_POST["project"];

    //  Update Expense Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")
    {
        $errorMessage                                                   = "";

        //  Get Information
        $id                                                             = $_GET["id"];
        $project_id                                                     = $_POST["project"];
        $description                                                    = addslashes(strip_tags($_POST["description"]));
        $expense                                                        = addslashes(strip_tags($_POST["expense"]));
        $vat                                                            = addslashes(strip_tags($_POST["VAT"]));
        $due_date                                                       = addslashes(strip_tags($_POST["due_date"]));

        if ($expense != "")
            $expense                                                    = number_format($expense, 2, ".", "");

        if ($vat != "")
            $vat                                                        = round($vat, 2);
        else
            $vat                                                        = 0;

        //  Get Old Amount
        $old_amount                                                     = q("SELECT amount FROM ExpenseBudget WHERE id = '$id'");
        $old_date                                                       = q("SELECT due_date FROM ExpenseBudget WHERE id = '$id'");
        $old_vat                                                       = q("SELECT vat FROM ExpenseBudget WHERE id = '$id'");

        $invoice_id                                                     = q("SELECT invoice_id FROM ExpenseBudget WHERE id = '$id'");

        //  Check If Expense Exists In Database
        $exist                                                          = q("SELECT id FROM ExpenseBudget WHERE descr = '$description' AND project_id = '$project_id' ".
                                                                            "AND id != '$id'");

        if (submitDateCheck($due_date))   {
        if (!$exist && $due_date != "")
        {
            $update                                                     = q("UPDATE ExpenseBudget SET descr = '$description', amount = '$expense', vat = '$vat', ".
                                                                            "due_date = '$due_date' WHERE id = '$id'");

            if ($update)
            {
                //  Update Expense Diverse If Invoice && Expense Are Linked
                if ($invoice_id != 0) {
                    $invExp                                             = q("SELECT expenses FROM LoadInvoices WHERE id = '$invoice_id'");
                    $totalExp                                           = q("SELECT SUM(amount) FROM ExpenseBudget WHERE invoice_id = '$invoice_id'");
                    $invDiv                                             = number_format(($invExp - $totalExp), 2, ".", "");

                    $update                                             = q("UPDATE LoadInvoices SET diverse_income = '$invDiv' WHERE id = '$invoice_id'");
                }

                //  Call Recalculation Function
				$old_expense = ($old_amount+($old_amount*($old_vat/100)));
				$new_expense = ($expense+($expense*($vat/100)));

                $amount = -($new_expense - $old_expense);
                if ($amount != 0)
                    statusUpdateAmount($old_date, $amount, $_SESSION["company_id"]);

                if ($old_date != $due_date)	{
					statusUpdateDate($old_date, $due_date, "expense", $new_expense, $_SESSION["company_id"]);
				}

                $project_name                                           = q("SELECT name FROM Project WHERE id = '$project_id'");

                $time                                                   = date("H:i:s");

                $message                                                = "Expense has been updated by ".$_SESSION["email"]." on ".$today." at ".$time.
                                                                            "\nProject Name:\t\t".$project_name."\nExpense Description:\t".$description.
                                                                            "\nExpense:\t\t\t".$_SESSION["currency"]." ".$expense."\nDue Date:\t\t\t".$due_date;
                $notify_id                                              = q("SELECT id FROM Notify WHERE code = 'Expense'");
                $events                                                 = q("INSERT INTO Events (date, time, message, notify_id, processed, user_id, company_id) ".
                                                                            "VALUES ('".$today."', '".$time."', '$message', '$notify_id', '0', ".
                                                                            "'".$_SESSION["user_id"]."', '".$_SESSION["company_id"]."')");

                $logs                                                   = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('".$description." update', 'Update', 'ExpenseBudget', '".$_SESSION["email"]."', ".
                                                                            "'$today', '$time', '".$_SESSION["company_id"]."')");

                header("Location: expense_planning.php");
            }
        }
        else if ($due_date == "")
            $errorMessage                                               = "Due Date Should Be Entered";
        else
            $errorMessage                                               = "Expense Already Exists";
        }
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("2_1", "budget");

    if ($errorMessage != "")
    {
        if ($errorMessage == "Please note that we are currently busy with data management, no entries are allowed for 2010-08-26. Please check back in 24 Hours.")
            echo "<p align='center' style='padding:0px;'><strong style='font-size:12px;'><font class='on-validate-error'>$errorMessage</font></strong></p>";
        else
            echo "<p align='center' style='padding:0px;'><strong><font class='on-validate-error'>$errorMessage</font></strong></p>";
        echo "<br/>";
    }
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    jQuery(function(){
	jQuery('#due_date').DatePicker({
		format:'Y-m-d',
		date: jQuery('#due_date').val(),
		current: jQuery('#due_date').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#due_date').val()))
			_date = new Date();
			else _date = jQuery('#due_date').val();
			jQuery('#due_date').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#due_date').val(formated);
			jQuery('#due_date').DatePickerHide();
		}
	});
    })

    function check(box)
    {
        var valid                                                       = 1;

        //  Check That Invoice Name Is Entered
        if (document.forms["expense_edit"].description.value == "")
        {
            ShowLayer("Description", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("Description", "none");

        //  Check That Expenses Is Valid, If Entered
        if (document.forms["expense_edit"].expense.value != "")
        {
            if (!validation("currency", document.forms["expense_edit"].expense.value))
            {
                ShowLayer("expenseDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("expenseDiv", "none");
        }
        else
            ShowLayer("expenseDiv", "none");

        //  Check That Diverse Income Is Valid, If Entered
        if (document.forms["expense_edit"].VAT.value != "")
        {
            if (!validation("percentage", document.forms["expense_edit"].VAT.value))
            {
                ShowLayer("vatDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("vatDiv", "none");
        }
        else
            ShowLayer("vatDiv", "none");

        //  Check That Due Date Is Entered
        if (document.forms["expense_edit"].due_date.value == "")
        {
            ShowLayer("dueDate", "block");
            valid                                                       = 0;
        }
        //  Check That Entered Due Date Is Valid
        else
        {
            ShowLayer("dueDate", "none");

            if (!validation("date", document.forms["expense_edit"].due_date.value))
            {
                ShowLayer("dueDateDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("dueDateDiv", "none");
        }

        if (valid == 1)
        {
            document.forms["expense_edit"].save.value                   = 1;
            document.forms["expense_edit"].submit();
        }
    }
</script>
<?php
    $id                                                                 = $_GET["id"];
    $expense_info                                                       = q("SELECT project_id, descr, amount, vat, due_date FROM ExpenseBudget WHERE id = '$id'");
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="expense_edit">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Edit Expense</h6>
                            </td>
                        </tr>
                        <tr>
                            <td class="centerdata">
                                <br/>
                            </td>
                        </tr>
                    </table>
                    <input method="post" name="project" type="hidden" value="<?php echo $expense_info[0][0]; ?>" />
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                Expense Description:
                            </td>
                            <td width="50%">
                                <input class="on-field" name="description" tabindex="1" type="text" value="<?php echo $expense_info[0][1]; ?>"><div id="Description"
                                    style="display: none;"><font class="on-validate-error">* Expense description must be entered</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                Expense <i>(<?php echo "".$_SESSION["currency"]; ?>)</i>:
                            </td>
                            <td align="left" width="50%">
                                <input class="on-field" name="expense" style="text-align:right;" tabindex="2" type="text" value="<?php
                                    echo "".number_format($expense_info[0][2], 2, ".", ""); ?>">
                                    <font class="on-description-left">* excl</font><div id="expenseDiv" style="display: none;"><font class="on-validate-error">* Entered amount must be numeric, eg.
                                    123.45</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                VAT <i>(%)</i>:
                            </td>
                            <td align="left" width="50%">
                                <input class="on-field-date" name="VAT" style="text-align:right;" tabindex="3" type="text" value="<?php echo $expense_info[0][3]; ?>"><div id="vatDiv"
                                    style="display: none;"><font class="on-validate-error">* Entered amount must be a percentage, eg. 100%</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                Due Date:
                            </td>
                            <td align="left" width="50%">
                                <input class="on-field-date" id="due_date" name="due_date" tabindex="4" type="text" style="text-align:right;" value="<?php echo $expense_info[0][4]; ?>">
                                <div id="dueDate" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                <div id="dueDateDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <input name="btnAdd" onClick="check();" tabindex="6" type="button" value="Update Expense">
                    <input method="post" name="save" type="hidden" value="0" />
                </form>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
