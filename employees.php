<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    if (!hasAccess("EMP_EDIT") && !hasAccess("EMP_CTC_MANAGE") && !hasAccess("EMP_TARIFF_MANAGE") && !hasAccess("EMP_ADD_REMOVE"))
        header("Location: noaccess.php");

    if (hasAccess("EMP_ADD_REMOVE"))
        $RA_ADD = 1;

    $display_link                                                       = "employees.php?userType";

    //  Set User Type
    $userType                                                           = "all";
    $userType                                                           = $_GET["userType"];

    //  If Reset Password Was Clicked
    if (isset($_POST["assignBox"]) && $_POST["assignBox"] === "1")      {
        $array = split("_", $_POST["checkbox"]);

        if ($arr[0][0]) $array = array_slice($array, 0, count($array) - 1);

        if ($array[0][0])       {
            foreach ($array as $a)      {
                //  Demo User
                if ($_POST["demo".$a])  $update = q("UPDATE Employee SET demo_user = '1' WHERE id = '$a'");
                //  Not Demo User
                else                    $update = q("UPDATE Employee SET demo_user = '0' WHERE id = '$a'");
            }
        }

        if ($update)    {
            $time = date("H:i:s");

            $logs = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                        "VALUES ('Demo users assigned', 'Update', 'Employee', '".$_SESSION["email"]."', '$today', '$time', '".$_SESSION["company_id"]."')");

            $errorMessage = "Demo Users Assigned Successfully...";
        }
    }

    //  If Reset Password Was Clicked
    if (isset($_POST["resetBox"]) && $_POST["resetBox"] === "1")        {
	$id                                                             = $_POST["idBox"];
        $employee_info                                                  = q("SELECT frstname, lstname FROM Employee WHERE id = '$id'");
        $password                                                       = generateHash("password");
        $reset                                                          = q("UPDATE Employee SET locked = '0', password = '$password', salt = '$origsalt' WHERE id = '$id'");

        if ($reset)
        {
            $time                                                       = date("H:i:s");

            $logs                                                       = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('".$employee_info[0][0]." ".$employee_info[0][1]." password reset', 'Update', ".
                                                                            "'Employee', '".$_SESSION["email"]."', '$today', '$time', '".$_SESSION["company_id"]."')");

            $errorMessage                                               = "Password Reset Successfully...";
        }
    }

    //  If Lock Employee Was Clicked
    if (isset($_POST["lockBox"]) && $_POST["lockBox"] === "1")
    {
	$id                                                             = $_POST["idBox"];
        $employee_info                                                  = q("SELECT frstname, lstname FROM Employee WHERE id = '$id'");
        $lock                                                           = q("UPDATE Employee SET locked = '1' WHERE id = '$id'");

        if ($lock)
        {
            $time                                                       = date("H:i:s");

            $logs                                                       = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('".$employee_info[0][0]." ".$employee_info[0][1]." locked', 'Update', ".
                                                                            "'Employee', '".$_SESSION["email"]."', '$today', '$time', '".$_SESSION["company_id"]."')");

            $errorMessage                                               = "Employee Locked Successfully...";
        }
    }

    //  If UnLock Employee Was Clicked
    if (isset($_POST["unlockBox"]) && $_POST["unlockBox"] === "1")
    {
	$id                                                             = $_POST["idBox"];
        $employee_info                                                  = q("SELECT frstname, lstname FROM Employee WHERE id = '$id'");
        $unlock                                                         = q("UPDATE Employee SET locked = '0' WHERE id = '$id'");

        if ($unlock)
        {
            $time                                                       = date("H:i:s");

            $logs                                                       = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('".$employee_info[0][0]." ".$employee_info[0][1]." unlocked', 'Update', ".
                                                                            "'Employee', '".$_SESSION["email"]."', '$today', '$time', '".$_SESSION["company_id"]."')");

            $errorMessage                                               = "Employee Unlocked Successfully...";
        }
    }

    //  If Delete Employee Was Clicked
    if (isset($_POST["deleteBox"]) && $_POST["deleteBox"] === "1")
    {
	$id                                                             = $_POST["idBox"];
        $employee_info                                                  = q("SELECT frstname, lstname FROM Employee WHERE id = '$id'");
        $lock                                                           = q("UPDATE Employee SET deleted = '1', locked = '1', activationDate = '".date("Y-m-d")."' WHERE id = '$id'");

        if ($lock)
        {
            $time                                                       = date("H:i:s");

            $logs                                                       = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('".$employee_info[0][0]." ".$employee_info[0][1]." deleted', 'Update', ".
                                                                            "'Employee', '".$_SESSION["email"]."', '$today', '$time', '".$_SESSION["company_id"]."')");

            $errorMessage                                               = "Employee Deleted Successfully...";
        }
    }

    //  If Undelete Employee Was Clicked
    if (isset($_POST["undeleteBox"]) && $_POST["undeleteBox"] === "1")
    {
	$id                                                             = $_POST["idBox"];
        $employee_info                                                  = q("SELECT frstname, lstname FROM Employee WHERE id = '$id'");
        $unlock                                                         = q("UPDATE Employee SET deleted = '0', locked = '0' WHERE id = '$id'");

        if ($unlock)
        {
            $time                                                       = date("H:i:s");

            $logs                                                       = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('".$employee_info[0][0]." ".$employee_info[0][1]." undeleted', 'Update', ".
                                                                            "'Employee', '".$_SESSION["email"]."', '$today', '$time', '".$_SESSION["company_id"]."')");

            $errorMessage                                               = "Employee Undeleted Successfully...";
        }
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0", "user", "employees");

    if ($errorMessage != "")
    {
        echo "<p align='center' style='padding:0px;'><strong><font color='#999999'>$errorMessage</font></strong></p>";
        echo "<br/>";
    }
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    function assign()
    {
        document.forms["employees"].assignBox.value                     = 1;
        document.forms["employees"].submit();
    }

    function lock(id)
    {
        document.forms["employees"].idBox.value                         = id;
        document.forms["employees"].lockBox.value                       = 1;
        document.forms["employees"].submit();
    }

    function unlock(id)
    {
        document.forms["employees"].idBox.value                         = id;
        document.forms["employees"].unlockBox.value                     = 1;
        document.forms["employees"].submit();
    }

    function deleted(id)
    {
        document.forms["employees"].idBox.value                         = id;
        document.forms["employees"].deleteBox.value                     = 1;
        document.forms["employees"].submit();
    }

    function undelete(id)
    {
        document.forms["employees"].idBox.value                         = id;
        document.forms["employees"].undeleteBox.value                   = 1;
        document.forms["employees"].submit();
    }

    function reset_confirmation(id)
    {
        document.forms["employees"].idBox.value                         = id;
        document.forms["employees"].resetBox.value                      = 1;
        document.forms["employees"].submit();
    }
</script>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="employees">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>
                                    Employee List
                                </h6>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <?php
                    if ($RA_ADD)        {
                    ?>
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                Show Deleted Users:
                            </td>
                            <td class="on-description-left" width="50%">
                                <?php
                                    $checked = ($_POST["showDeleted"] != "") ? "checked" : "";
                                ?>
                                <input id="showDeleted" name="showDeleted" type="checkbox" tabindex="1" value="1" <?php echo $checked; ?> onClick="submit();" />
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                Show Locked Users:
                            </td>
                            <td class="on-description-left" width="50%">
                                <?php
                                    $checked = ($_POST["showLocked"] != "") ? "checked" : "";

                                    if ($_POST["showed"] == "") $checked = "checked";
                                ?>
                                <input id="showLocked" name="showLocked" type="checkbox" tabindex="1" value="1" <?php echo $checked; ?> onClick="submit();" />
                            </td>
                        </tr>
                    </table>
                    <?php
                    }
                    ?>
                    <?php
                        echo "<br/>";
                            echo "<a href='$display_link=all'>View All Employees</a>";
                        echo "<br/><br/>";

                        $roles = q("SELECT id, descr FROM roles WHERE active = '1' AND role != 'ADM' AND companyid = '".$_SESSION["company_id"]."' ORDER BY id");

                        if (is_array($roles))   {
                            $i = 1;
                            $j = count($roles);

                            foreach ($roles as $r)      {
                                echo "<a href='$display_link=".$r[0]."'>".$r[1]."</a>";

                                if ($i != $j)   echo "<a> | </a>";

                                $i++;
                            }
                        }
                    ?>
                    <br/><br/>
                    <?php
//                        if ($_SESSION["usertype"] === "0")
                        if ($RA_ADD)	{
                            echo "<input name='btnAdd' onClick=\"location.href='employee_add.php';\" tabindex='1' type='button' value='Add New Employee'>";
//                            echo "<br/>";
//                            echo "<input name='btnAssign' onClick='assign();' type='button' value='Assign Demo Users'>";
                            echo "<br/><br/>";
                        }
                    ?>
                    <!--  Headings   -->
                    <div class="on-20px">
                    <table class="on-table-center on-table">
                        <tr>
                            <th>
                                    Surname
                            </th>
                            <th>
                                    First Name
                            </th>
                            <?php	
//                                if ($_SESSION["usertype"] === "0")
                                {
                                        echo "<th>";
                                            echo "Email Address";
                                        echo "</td>";
                                            if ($RA_ADD)	{
                                                    echo "<th>";
                                                                    echo "Lock/Unlock";
                                                    echo "</td>";
                                                    echo "<th>";
                                                                    echo "Delete/Undelete";
                                                    echo "</td>";
                                                    echo "<th>";
                                                                    echo "Reset Password";
                                                    echo "</td>";
//										echo "<td align='center' style='color:#FFFFFF;".$top.$left.$right.$bottom.$background."'>";
//											echo "<strong>";
//												echo "Demo";
//											echo "</strong>";
//										echo "</td>";
                                            }
                                }
                            ?>
                        </tr>
                        <!--  Table Information   -->
                        <?php
                            $where = ($userType != "all") ? " AND ur.roleid = '".$userType."'" : "";

                            //$employees = q("SELECT DISTINCT(e.id), convert(e.lstname USING UTF8), e.frstname, e.email, e.locked, e.deleted, e.demo_user ".
                            //                "FROM (Employee AS e INNER JOIN Company_Users AS cu ON e.id = cu.user_id INNER JOIN user_role AS ur ON ur.userid = e.id) ".
                            //                "WHERE cu.company_id = '".$_SESSION["company_id"]."' AND ur.companyid = '".$_SESSION["company_id"]."' AND e.email != 'admin' ".
                            //                "AND e.company_id = '".$_SESSION["company_id"]."' $where ORDER BY e.lstname, frstname");

                            if ($RA_ADD)        {
                                $where .= ($_POST["showDeleted"]) ? "AND e.deleted IN ('0','1') " : "AND e.deleted = '0' ";
                                $where .= ($_POST["showLocked"] || $_POST["showed"] == "") ? "AND e.locked IN ('0','1') " : "AND e.locked = '0' ";

                                $employees = q("SELECT DISTINCT(e.id), convert(e.lstname USING UTF8), e.frstname, e.email, e.locked, e.deleted, e.demo_user, e.activationDate ".
                                                "FROM (Employee AS e INNER JOIN Company_Users AS cu ON e.id = cu.user_id INNER JOIN user_role AS ur ON ur.userid = e.id) ".
                                                "WHERE cu.company_id = '".$_SESSION["company_id"]."' AND ur.companyid = '".$_SESSION["company_id"]."' AND e.email != 'admin' ".
                                                "$where ORDER BY e.lstname, frstname");
                            } else
                                $employees = q("SELECT DISTINCT(e.id), convert(e.lstname USING UTF8), e.frstname, e.email, e.locked, e.deleted, e.demo_user, e.activationDate ".
                                                "FROM (Employee AS e INNER JOIN Company_Users AS cu ON e.id = cu.user_id INNER JOIN user_role AS ur ON ur.userid = e.id) ".
                                                "WHERE cu.company_id = '".$_SESSION["company_id"]."' AND ur.companyid = '".$_SESSION["company_id"]."' AND e.email != 'admin' ".
                                                "AND e.deleted = '0' $where ORDER BY e.lstname, frstname");

                            if (is_array($employees)) {
                                foreach ($employees as $employee) {
                                    echo "<tr>";
                                        echo "<td>";
                                            echo "".$employee[1]."";
                                        echo "</td>";
                                        echo "<td>";
                                            echo "".$employee[2]."";
                                        echo "</td>";
//                                        if ($_SESSION["usertype"] === "0")
                                        {
                                            echo "<td>";
                                                echo "<a href='employee_edit.php?id=".$employee[0]."'>".$employee[3]."</a>";
                                            echo "</td>";

                                            if ($RA_ADD)	{
                                                    //  Lock/Unlock Button
                                                    echo "<td class='centerdata'>";
                                                            $disable = ($employee[5] === "1") ? "disabled" : "";

                                                            if ($employee[4] === "0")
                                                                    echo "<input name='btnLock' onClick=\"lock(".$employee[0].");\" type='button' $disable value='Lock Employee   '>";
                                                            else
                                                                    echo "<input name='btnUnlock' onClick=\"unlock(".$employee[0].");\" type='button' $disable value='Unlock Employee'>";
                                                    echo "</td>";
                                                    //  Delete/Undelete Button
                                                    $daysActive = getDays($employee[7],date("Y-m-d"));
                                                    $disabled = ($daysActive >= 30) ? "" : "disabled";
                                                    if ($_SESSION["email"] == "admin") $disabled = "";
                                                    echo "<td class='centerdata'>";
                                                            if ($employee[5] === "0")
                                                                    echo "<input name='btnDelete' $disabled onClick=\"deleted(".$employee[0].");\" type='button' value='Delete Employee    '>";
                                                            else
                                                                    echo "<input name='btnUndelete' onClick=\"undelete(".$employee[0].");\" type='button' value='Undelete Employee'>";
                                                    echo "</td>";
                                                    //  Reset Password Button
                                                    echo "<td class='centerdata'>";
                                                            echo "<input name='btnReset' onClick=\"reset_confirmation(".$employee[0].");\" type='button' ".
                                                                    "value='Reset Password'>";
                                                    echo "</td>";
                                                    //  Demo User Checkbox
//												echo "<td align='center'>";
//													$checkbox               .= "".$employee[0]."_";
//
//													if ($employee[6] == "1")
//														echo "<input name='demo".$employee[0]."' tabindex=1 type='checkbox' value='1' checked>";
//													else
//														echo "<input name='demo".$employee[0]."' tabindex=1 type='checkbox' value='1'>";
//												echo "</td>";
                                            }
                                        }
                                    echo "</tr>";
                                }
                                echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                            }
                            else {
                                echo "<tr>";
									if ($RA_ADD)	$colspan = 6;
									else			$colspan = 3;
									echo "<td class='centerdata' colspan='$colspan'>";
										echo "No Employees available of current type";
									echo "</td>";
                                echo "</tr>";
                            }
                        ?>
                    </table>
                    </div>
                    <?php
//                        if ($_SESSION["usertype"] === "0") {
//                            echo "<br/>";
//                            echo "<input name='btnAssign' onClick='assign();' type='button' value='Assign Demo Users'>";
						if ($RA_ADD)	{
                            echo "<br/>";
                            echo "<input name='btnAdd' onClick=\"location.href='employee_add.php';\" tabindex='1' type='button' value='Add New Employee'>";
						}
                    ?>
                    <input name="assignBox" type="hidden" value="">
                    <input name="idBox" type="hidden" value="">
                    <?php
                        echo "<input name='checkbox' type='hidden' value='".$checkbox."'>";
                    ?>
                    <input name="showed" type="hidden" value="1">
                    <input name="resetBox" type="hidden" value="">
                    <input name="lockBox" type="hidden" value="">
                    <input name="unlockBox" type="hidden" value="">
                    <input name="deleteBox" type="hidden" value="">
                    <input name="undeleteBox" type="hidden" value="">
                </form>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
