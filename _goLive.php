<?php
    include("_db.php");
    include("_dates.php");
?>
<html>
    <head>
        <title>
            On-Track - Go Live Script...
        </title>
    </head>
    <body>
        <h1 align="center">
            Install
        </h1>
        <?php
            $date = date("Y-m-d H:i:s");
            $user = "admin";

            ////////////////////////////////////////
            //  ROLES
            ////////////////////////////////////////
            unset($a);

            //$a[] = "'REP_ANALYSIS','Report analysis'";
            $a[] = "'CLIENT_MANAGE','Manage Client(s)'";
            $a[] = "'ACCOUNT_MANAGE','Manage Bank Account(s)'";
            $a[] = "'INVOICE_PROCESS','Invoice Processing'";

            sort($a);

            foreach ($a as $d)  {
                $tmp = preg_split("/,/",$d);
                $action = $tmp[0];
                $descr = $tmp[1];

                if (!exist("actions","action = ".$action." AND descr = ".$descr.""))    $insert = q("INSERT INTO actions(action,descr) VALUES ($d)");
            }

            $alter = q("ALTER TABLE companiesOnTeam ADD COLUMN OBEmailSent TINYINT UNSIGNED NOT NULL DEFAULT 0 AFTER deleted");
            $alter = q("ALTER TABLE companiesOnTeam ADD COLUMN overBudget TINYINT UNSIGNED NOT NULL DEFAULT 0 AFTER deleted");

            $alter = q("ALTER TABLE Employee ADD COLUMN activationDate CHAR(20)");
            $update = q("UPDATE Employee SET activationDate = '2011-01-01'");

            // Default Roles
            $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES ('0',(SELECT id FROM roles WHERE role = 'MAN'),(SELECT id FROM actions WHERE action = 'REP_TIME_EX'))");
            $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES ('0',(SELECT id FROM roles WHERE role = 'PM'),(SELECT id FROM actions WHERE action = 'REP_TIME_EX'))");

            $companies = q("SELECT id, name FROM Company ORDER BY id");
            if(is_array($companies))    {
                foreach($companies as $c)       {
                    $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'MAN') AND actionid = (SELECT id FROM actions WHERE action = 'REP_TIME_EX') AND companyid = '".$c[0]."'");
                    $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'PM') AND actionid = (SELECT id FROM actions WHERE action = 'REP_TIME_EX') AND companyid = '".$c[0]."'");
                    $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES ('".$c[0]."',(SELECT id FROM roles WHERE role = 'MAN'),(SELECT id FROM actions WHERE action = 'REP_TIME_EX'))");
                    $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES ('".$c[0]."',(SELECT id FROM roles WHERE role = 'PM'),(SELECT id FROM actions WHERE action = 'REP_TIME_EX'))");
                }
            }

            // Default Roles
            $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'MAN') AND actionid = (SELECT id FROM actions WHERE action = 'PROJ_MANAGE') AND companyid ='0' ");
            $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'PM') AND actionid = (SELECT id FROM actions WHERE action = 'PROJ_MANAGE') AND companyid ='0' ");
            $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES ('0',(SELECT id FROM roles WHERE role = 'MAN'),(SELECT id FROM actions WHERE action = 'PROJ_MANAGE'))");
            $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES ('0',(SELECT id FROM roles WHERE role = 'PM'),(SELECT id FROM actions WHERE action = 'PROJ_MANAGE'))");

            $companies = q("SELECT id, name FROM Company ORDER BY id");
            if(is_array($companies))    {
                foreach($companies as $c)       {
                    $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'MAN') AND actionid = (SELECT id FROM actions WHERE action = 'PROJ_MANAGE') AND companyid = '".$c[0]."'");
                    $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'PM') AND actionid = (SELECT id FROM actions WHERE action = 'PROJ_MANAGE') AND companyid = '".$c[0]."'");
                    $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES ('".$c[0]."',(SELECT id FROM roles WHERE role = 'MAN'),(SELECT id FROM actions WHERE action = 'PROJ_MANAGE'))");
                    $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES ('".$c[0]."',(SELECT id FROM roles WHERE role = 'PM'),(SELECT id FROM actions WHERE action = 'PROJ_MANAGE'))");
                }
            }

            // Default Roles
            $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'PM') AND actionid = (SELECT id FROM actions WHERE action = 'SLA_MANAGE') AND companyid ='0' ");

            $companies = q("SELECT id, name FROM Company ORDER BY id");
            if(is_array($companies))    {
                foreach($companies as $c)       {
                    if ($c[0] != "1")
                        $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'PM') AND actionid = (SELECT id FROM actions WHERE action = 'SLA_MANAGE') AND companyid = '".$c[0]."'");
                }
            }

            // Default Roles
            $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'MAN') AND actionid = (SELECT id FROM actions WHERE action = 'RESOURCE_MAN') AND companyid ='0' ");
            $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES ('0',(SELECT id FROM roles WHERE role = 'MAN'),(SELECT id FROM actions WHERE action = 'RESOURCE_MAN'))");

            $companies = q("SELECT id, name FROM Company ORDER BY id");
            if(is_array($companies))    {
                foreach($companies as $c)       {
                    $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'MAN') AND actionid = (SELECT id FROM actions WHERE action = 'RESOURCE_MAN') AND companyid = '".$c[0]."'");
                    $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES ('".$c[0]."',(SELECT id FROM roles WHERE role = 'MAN'),(SELECT id FROM actions WHERE action = 'RESOURCE_MAN'))");
                }
            }

            // Default Roles
            $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'MAN') AND actionid = (SELECT id FROM actions WHERE action = 'ACTION_ASSIGN') AND companyid ='0' ");
            $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'MAN') AND actionid = (SELECT id FROM actions WHERE action = 'ROLE_ASSIGN') AND companyid ='0' ");
            $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'MAN') AND actionid = (SELECT id FROM actions WHERE action = 'ROLE_MAN') AND companyid ='0' ");
            $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES ('0',(SELECT id FROM roles WHERE role = 'MAN'),(SELECT id FROM actions WHERE action = 'ACTION_ASSIGN'))");
            $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES ('0',(SELECT id FROM roles WHERE role = 'MAN'),(SELECT id FROM actions WHERE action = 'ROLE_ASSIGN'))");
            $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES ('0',(SELECT id FROM roles WHERE role = 'MAN'),(SELECT id FROM actions WHERE action = 'ROLE_MAN'))");

            $companies = q("SELECT id, name FROM Company ORDER BY id");

            if(is_array($companies))    {
                foreach($companies as $c)       {
                    $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'MAN') AND actionid = (SELECT id FROM actions WHERE action = 'ACTION_ASSIGN') AND companyid = '".$c[0]."'");
                    $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'MAN') AND actionid = (SELECT id FROM actions WHERE action = 'ROLE_ASSIGN') AND companyid = '".$c[0]."'");
                    $delete = q("DELETE FROM role_action WHERE roleid = (SELECT id FROM roles WHERE role = 'MAN') AND actionid = (SELECT id FROM actions WHERE action = 'ROLE_MAN') AND companyid = '".$c[0]."'");
                    $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES ('".$c[0]."',(SELECT id FROM roles WHERE role = 'MAN'),(SELECT id FROM actions WHERE action = 'ACTION_ASSIGN'))");
                    $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES ('".$c[0]."',(SELECT id FROM roles WHERE role = 'MAN'),(SELECT id FROM actions WHERE action = 'ROLE_ASSIGN'))");
                    $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES ('".$c[0]."',(SELECT id FROM roles WHERE role = 'MAN'),(SELECT id FROM actions WHERE action = 'ROLE_MAN'))");
                }
            }

            $alter = q("ALTER TABLE roles ADD COLUMN companyid SMALLINT UNSIGNED NOT NULL DEFAULT 0 AFTER id");

            //  INSERT DEFAULT ROLES FOR EACH COMPANY
            $companies = q("SELECT id, name FROM Company ORDER BY id");

            if (is_array($companies))   {
                foreach ($companies as $c)      {
                    $insert = q("INSERT INTO roles (companyid, role, descr, active) SELECT ".$c[0].", role, descr, active FROM roles WHERE companyid = 0");
                }
            }

            //  UPDATE ROLE_ACTION[ROLEID] & USER_ROLE[ROLEID] TO CORRECT ROLEID IN ROLES FOR EACH COMPANY
            $companies = q("SELECT id, name FROM Company ORDER BY id");
            $roles = q("SELECT id,role FROM roles WHERE companyid = 0 ORDER BY id");

            if (is_array($companies))   {
                foreach ($companies as $c)      {
                    if (is_array($roles))   {
                        foreach ($roles as $r)      {
                            $oldRoleID = $r[0];
                            $newRoleID = q("SELECT id FROM roles WHERE companyid = '".$c[0]."' AND role = '".$r[1]."'");

                            $update = q("UPDATE role_action SET roleid = '".$newRoleID."' WHERE companyid = '".$c[0]."' AND roleid = '".$oldRoleID."'");
                            $update = q("UPDATE user_role SET roleid = '".$newRoleID."' WHERE companyid = '".$c[0]."' AND roleid = '".$oldRoleID."'");
                        }
                    }
                }
            }

            $alter = q("ALTER TABLE roles ADD COLUMN dflt TINYINT UNSIGNED NOT NULL DEFAULT 0 AFTER active");
            $update = q("UPDATE roles SET dflt = '1' WHERE companyid = '0'");

            $roles = q("SELECT role, descr FROM roles");

            if (is_array($roles))       {
                foreach ($roles as $r)  {
                    $update = q("UPDATE roles SET dflt = '1' WHERE companyid != '0' AND role = '".$r[0]."' AND descr = '".$r[1]."'");
                }
            }

            $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES (1,(SELECT id FROM roles WHERE role = 'MAN' AND companyid = '1'),(SELECT id FROM actions WHERE action = 'ACCOUNT_MANAGE'))");
            $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES (1,(SELECT id FROM roles WHERE role = 'MAN' AND companyid = '1'),(SELECT id FROM actions WHERE action = 'CLIENT_MANAGE'))");
            $insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES (1,(SELECT id FROM roles WHERE role = 'FM' AND companyid = '1'),(SELECT id FROM actions WHERE action = 'INVOICE_PROCESS'))");

            $drop = q("DROP TABLE IF EXISTS user_leave;");
            $create = q("CREATE TABLE user_leave (
                                  id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
                                  companyid SMALLINT UNSIGNED,INDEX(companyid),
                                  userid SMALLINT UNSIGNED,INDEX(userid),
                                  annual VARCHAR(10) NOT NULL DEFAULT '21',
                                  sick VARCHAR(10) NOT NULL DEFAULT '12',
                                  special VARCHAR(10) NOT NULL DEFAULT '5',
                                  study VARCHAR(10) NOT NULL DEFAULT '',
                                  unpaid VARCHAR(10) NOT NULL DEFAULT '',
                                  maternity VARCHAR(10) NOT NULL DEFAULT '')");

            $employees = q("SELECT cu.company_id, cu.user_id, c.name, e.email FROM Company_Users AS cu INNER JOIN Company AS c ON c.id = cu.company_id INNER JOIN Employee AS e ON e.id = cu.user_id ".
                            "WHERE e.email != 'admin' ORDER BY c.id, e.id");

            if (is_array($employees))   {
                foreach ($employees as $e)       {
                    if (!exist("user_leave","companyid = '".$e[0]."' AND userid = '".$e[1]."'"))
                        $insert = q("INSERT INTO user_leave(companyid,userid) VALUES ('".$e[0]."','".$e[1]."')");
                }
            }

            $alter = q("ALTER TABLE LeaveBooked ADD COLUMN company_id SMALLINT UNSIGNED AFTER id");
            $update = q("UPDATE LeaveBooked AS lb SET lb.company_id = (SELECT e.company_id FROM Employee AS e WHERE e.id = lb.user_id)");

            $drop = q("DROP TABLE IF EXISTS landingPage;");
            $create = q("CREATE TABLE landingPage (
                                  id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
                                  companyID SMALLINT UNSIGNED,INDEX(companyid),
                                  userID SMALLINT UNSIGNED,INDEX(userid),
                                  landingPage VARCHAR(255) NOT NULL DEFAULT 'bookings.php')");

            $employees = q("SELECT cu.company_id, cu.user_id, c.name, e.email FROM Company_Users AS cu INNER JOIN Company AS c ON c.id = cu.company_id INNER JOIN Employee AS e ON e.id = cu.user_id ORDER BY c.id, e.id");

            if (is_array($employees))   {
                foreach ($employees as $e)       {
                    if (!exist("landingPage","companyID = '".$e[0]."' AND userID = '".$e[1]."'"))
                        $insert = q("INSERT INTO landingPage(companyID,userID) VALUES ('".$e[0]."','".$e[1]."')");
                }
            }

        ?>
        <form action="index.php" method="post">
            <center><input type="submit" value="Return"/></center>
        </form>
    </body>
</html>
