<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("_functions.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if ($_SESSION["logged_in"] === true && !($_SESSION["usertype"] === "0"))
        header("Location: home.php");

    //  Insert Company Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")
    {
        $errorMessage                                                   = "";

        //  Get Information
        $company_name                                                   = addslashes(strip_tags($_POST["company_name"]));
        $company_rate                                                   = addslashes(strip_tags($_POST["company_rate"]));
        $company_currency                                               = addslashes(strip_tags($_POST["company_currency"]));
        $company_vat                                                    = addslashes(strip_tags($_POST["company_vat"]));
        $sars_compliance												= addslashes(strip_tags($_POST["sars_compliance"]));
        $backing_documents												= addslashes(strip_tags($_POST["backing_documents"]));
        $gpsLogbook												= addslashes(strip_tags($_POST["gpsLogbook"]));

        $company_rate												= number_format($company_rate, 2, ".", "");
		$sars_compliance											= ($sars_compliance == "") ? 0 : 1;
		$backing_documents										= ($backing_documents == "") ? 0 : 1;
		$gpsLogbook												= ($gpsLogbook == "") ? 0 : 1;

        //  Check If Company Exists In Database
        $exist                                                          = q("SELECT id FROM Company WHERE name = '$company_name'");

        if (!$exist)
        {
            $insert                                                     = q("INSERT INTO Company (name, rate, currency, vat, sars_compliance, backing_documents, gpsLogbook, locked) ".
                                                                            "VALUES ('$company_name', '$company_rate', '$company_currency', '$company_vat', '$sars_compliance', '$backing_documents', '$gpsLogbook', 0)");

            if ($insert)
            {
                //  Assign Admin To Company
                $company_id                                             = q("SELECT id FROM Company WHERE name = '$company_name'");

                $insert                                                 = q("INSERT INTO Company_Users (user_id, company_id) VALUES ('1', '$company_id')");
                $insert                                                 = q("INSERT INTO roles (companyid, role, descr, active, dflt) SELECT ".$company_id.", role, descr, active, dflt FROM roles WHERE companyid = 0");
                $insert                                                 = q("INSERT INTO role_action (companyid, roleid, actionid) SELECT ".$company_id.", roleid, actionid FROM role_action WHERE companyid = 0");

                //  Update roleid
                $roles = q("SELECT id,role FROM roles WHERE companyid = 0 ORDER BY id");

                if (is_array($roles))   {
                    foreach ($roles as $r)      {
                        $oldRoleID = $r[0];
                        $newRoleID = q("SELECT id FROM roles WHERE companyid = '".$company_id."' AND role = '".$r[1]."'");

                        $update = q("UPDATE role_action SET roleid = '".$newRoleID."' WHERE companyid = '".$company_id."' AND roleid = '".$oldRoleID."'");
                        $update = q("UPDATE user_role SET roleid = '".$newRoleID."' WHERE companyid = '".$company_id."' AND roleid = '".$oldRoleID."'");
                    }
                }

                updateMenuIndex($company_id,1);

                $time                                                   = date("H:i:s");

                $logs                                                   = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time) ".
                                                                            "VALUES ('$company_name added', 'Insert', 'Company', '".$_SESSION["email"]."', ".
                                                                            "'$today', '$time')");

                header("Location: companies.php");
            }
        }
        else
            $errorMessage                                               = "Company Already Exists";
    }

    if ($errorMessage != "")
        echo "<p align='center' style='padding:0px;'><strong><font color='#999999'>$errorMessage</font></strong></p>";

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0", "companies");
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    var cal                                                             = new CodeThatCalendar(caldef1);

    function check()
    {
        var valid                                                       = 1;

        //  Check That Company Name Is Entered
        if (document.forms["company_add"].company_name.value == "")
        {
            ShowLayer("companyName", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("companyName", "none");

        //  Check That Company Rate Is Valid If Entered
        if (document.forms["company_add"].company_rate.value != "")
        {
            if (!validation("currency", document.forms["company_add"].company_rate.value))
            {
                ShowLayer("companyRate", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("companyRate", "none");
        }
        else
            ShowLayer("companyRate", "none");

        if (valid == 1)
        {
            document.forms["company_add"].save.value                    = 1;
            document.forms["company_add"].submit();
        }
    }
</script>
    <table width="100%">
        <tr>
            <td class="centerdata" valign="top">
                <form action="" method="post" name="company_add">
                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Add New Company</h6>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                    Company Name:
                            </td>
                            <td align="left" width="50%">
                                <input class="on-field" name="company_name" tabindex="1" type="text">
                                <div id="companyName" style="display: none;"><font class="on-validate-error">* Name must be entered</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                    Rate/Employee:
                            </td>
                            <td align="left" width="50%">
                                <input class="on-field" name="company_rate" tabindex="2" type="text">
                                <div id="companyRate" style="display: none;"><font class="on-validate-error">* Entered amount must be numeric, eg. 123.45</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                    Currency:
                            </td>
                            <td align="left" width="50%">
                                <input class="on-field" name="company_currency" tabindex="3" type="text">
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                    Default VAT:
                            </td>
                            <td align="left" width="50%">
                                <input class="on-field" name="company_vat" tabindex="3" type="text">
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                    Require SARS Logbook Compliance:
                            </td>
                            <td align="left" width="50%">
                                <input class="on-field" name="sars_compliance" tabindex="4" type="checkbox" value="1">
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                    Enforce Backing Documentation Uploads:
                            </td>
                            <td align="left" width="50%">
                                <input class="on-field" name="backing_documents" tabindex="4" type="checkbox" value="1">
                            </td>
                        </tr>
                         <tr>
                            <td class="on-description" width="50%">
                                    Use GPS Logbook:
                            </td>
                            <td align="left" width="50%">
                                <input class="on-field" name="gpsLogbook" tabindex="4" type="checkbox" value="1">
                            </td>
                        </tr>
                   </table>
                    <br/>
                    <input name="btnAdd" onClick="check();" tabindex="4" type="button" value="Add Company">
                    <input method="post" name="save" type="hidden" value="0" />
                </form>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>