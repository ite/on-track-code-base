<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");
        
    if (!hasAccess("REP_COMPANY_BREAK"))
        header("Location: noaccess.php");
        
    // Class for table background color
    $colorClass = "OnTrack_#_";

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("6", "reports");
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="report_company_breakdown">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Company Breakdown Report</h6>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <?php
                        ///////////////////////////
                        //  Get Information
                        ///////////////////////////
                        //  nCompanies                                  = Number of Companies
                        $companies                                      = q("SELECT id, name, rate FROM Company ORDER BY name");
                        ///////////////////////////
                        //  Create Information String
                        ///////////////////////////
                        //  Table Headers
                        $display_string                                 = "";
                        $grand_total                                    = 0;
                        
                        $row                                                              = 0;
                        
                        $excelheadings[$row][]                                     = "Report: Company Breakdown"; 
                        $excelheadings[$row][]                                     = "";    
                        $excelheadings[$row][]                                     = "";
                            $row++;
                        $excelheadings[$row][]                                     = $colorClass."Employee Name  "; 
                        $excelheadings[$row][]                                     = $colorClass."Rate/User (".$_SESSION["currency"].")";    
                        $excelheadings[$row][]                                     = $colorClass."Total (".$_SESSION["currency"].")";
                            $row++;
                        
                        $headers                                        = "<tr>
                                                                                    <th>Employee Name</th>
                                                                                    <th>Rate/User <i>(".$_SESSION["currency"].")</i></th>
                                                                                    <th>Total <i>(".$_SESSION["currency"].")</i></th>
                                                                                </tr>";

                        //  Table Information
                        if (is_array($companies))    {
                            foreach ($companies as $company)    {
                            
                                $string                                 = "";

                                $users                                  = q("SELECT id, lstname, frstname FROM Employee WHERE company_id = '".$company[0]."' ".
                                                                            "AND email != 'admin' AND demo_user = '0' AND deleted = '0' AND id IN (SELECT user_id ".
                                                                            "FROM Company_Users WHERE company_id = '".$company[0]."') ORDER BY lstname, frstname");
                                                                            

                                if ($company[2] != ""){
                                    $rate                               = "".number_format($company[2], 2, ".", "");
                                }else{
                                    $rate                               = "-";
                                }

                                $total                                  = $company[2] * count($users);

                                if ($total != 0)
                                    $total                              = "".number_format($total, 2, ".", "");
                                else
                                    $total                              = "-";
                                    
                                
                                if (is_array($users)){ 
                                        
                                        $row++;
                                    $exceldata[$row][]           = "Company: ".$company[1];
                                    $exceldata[$row][]           = "";
                                    $exceldata[$row][]           = "";
                                    
                                    foreach ($users as $user){
                                        $string                         .= "<tr><td>".$user[1].", ".$user[2]."</td>".
                                                                            "<td class='rightdata'>".$rate."</td>".
                                                                            "<td class='rightdata'>".$rate."</td></tr>";
                                            $row++;
                                        $exceldata[$row][]           = $user[1].", ".$user[2];
                                        $exceldata[$row][]           = $rate;
                                        $exceldata[$row][]           = $rate;
                                            $row++;
                                    }
                                    
                                    $exceldata[$row][]           = $colorClass."";
                                    $exceldata[$row][]           = $colorClass."Total: (".$_SESSION["currency"].")";
                                    $exceldata[$row][]           = $colorClass.$total ;
                                        $row++;
                                    $exceldata[$row][]           = "";
                                    $exceldata[$row][]           = "";
                                    $exceldata[$row][]           = "";
                                        
                                }else{
                                    $exceldata[$row][]           = "-";
                                    $exceldata[$row][]           = "-";
                                    $exceldata[$row][]           = "-";
                                }

                                if ($string != ""){
                                    $display_string                     .= "<tr>
                                                                                            <td colspan='3'><h6>".$company[1]."</h6></td>
                                                                                        </tr>".$headers.$string."
                                                                                        <tr>
                                                                                            <td class='on-table-total' colspan='2'>Total <i>(".$_SESSION["currency"].")</i>:</td>
                                                                                            <td class='on-table-total'>".$total."</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan='100%'><br/></td>
                                                                                        </tr>";
                                }
                                //$row++;
                            }
                        }
                        ///////////////////////////
                        //  Display Information
                        if ($display_string != "")
                        {
                            echo "<div class='on-20px'><table class='on-table-center on-table'>";
                                echo "".$display_string;
                                echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                            echo "</table></div>";

                            ///////////////////////////
                            //  Set Export Information
                            $_SESSION["fileName"] = "Company Breakdown Report";
                            $_SESSION["fileData"] = array_merge($excelheadings, $exceldata);
                            ///////////////////////////

                            echo "<input name='btnExport' onClick=\"location.href='_export.php'\" type='button' value='Export to Excel' />";
                        }
                        ///////////////////////////
                    ?>
                </form>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>