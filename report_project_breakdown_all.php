<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    
    unset($_SESSION["fileName"]);
    unset($_SESSION["fileData"]);

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");
        
    if (!hasAccess("REP_PROJ_BREAK"))
        header("Location: noaccess.php");
        
    // Class for table background color
    $colorClass = "OnTrack_#_";

    $displayString = "";

    // Get Full Project Breakdown - All Projects (Open & Closed)
    $projectBreakdown = q("SELECT p.number, p.name, pt.type, e.lstname, e.frstname, pu.user_budget, ur.rate,
                            (SELECT SUM(time_spent * rate) FROM TimeSheet WHERE user_id = e.id AND project_id = p.id),
                            (SELECT SUM(time_spent) FROM TimeSheet WHERE user_id = e.id AND project_id = p.id)
                            FROM Employee as e 
                            INNER JOIN Project_User as pu ON e.id = pu.user_id 
                            INNER JOIN Project as p ON p.id = pu.project_id 
                            LEFT JOIN ProjectTypes pt ON pt.id = p.type_id
                            LEFT JOIN user_rates ur ON ur.id = pu.rateId
                            WHERE p.company_id = '".$_SESSION["company_id"]."'
                            ORDER BY p.name ASC, pu.manager DESC, e.lstname ASC, e.frstname ASC");

    if (is_array($projectBreakdown)) {
        $row = 0;
    
        $excelheadings[$row][] = "Report: Consulting Breakdown (All Projects)"; 
        $excelheadings[$row][] = "";
        $excelheadings[$row][] = "";
        $excelheadings[$row][] = "";
        $excelheadings[$row][] = "";
        $excelheadings[$row][] = "";
        $excelheadings[$row][] = "";
        $excelheadings[$row][] = "";
        $excelheadings[$row][] = "";
        $excelheadings[$row][] = "";
        $excelheadings[$row][] = "";
        $excelheadings[$row][] = "";
        $row++;

        $excelheadings[$row][] = $colorClass ."Project Number"; 
        $excelheadings[$row][] = $colorClass ."Project Name"; 
        $excelheadings[$row][] = $colorClass ."Project Type"; 
        $excelheadings[$row][] = $colorClass ."Surname"; 
        $excelheadings[$row][] = $colorClass ."Name"; 
        $excelheadings[$row][] = $colorClass ."Allocated Project Budget";
        $excelheadings[$row][] = $colorClass ."Rate on Project"; 
        $excelheadings[$row][] = $colorClass ."Allocated Time/Person"; 
        $excelheadings[$row][] = $colorClass ."Total Budget Used/Person (Time Booked)"; 
        $excelheadings[$row][] = $colorClass ."Time Used/Person"; 
        $excelheadings[$row][] = $colorClass ."Budget Left/Person"; 
        $excelheadings[$row][] = $colorClass ."Total Time Left/Person"; 
        $row++;
        
        ///////////////////////////
        //  Create Table Headers
        $headers = "<tr>\
            <th>Project Number</th>
            <th>Project Name</th>
            <th>Project Type</th>
            <th>Surname</th>
            <th>Name</th>
            <th>Allocated Project Budget <i>(".$_SESSION["currency"].")</i></th>
            <th>Rate on Project <i>(".$_SESSION["currency"]."/h)</i></th>
            <th>Allocated Time/Person</th>
            <th>Total Budget Used/Person <i>(".$_SESSION["currency"].")</i><br/>(Time Booked)</th>
            <th>Time Used/Person</th>
            <th>Budget Left/Person <i>(".$_SESSION["currency"]."/h)</i></th>
            <th>Total Time Left/Person</th>
        </tr>";

        foreach ($projectBreakdown as $pb) {
            $displayString .= "<tr>";
                $displayString .= "<td>".$pb[0]."</td>";
                $exceldata[$row][] = $pb[0];

                $displayString .= "<td>".$pb[1]."</td>";
                $exceldata[$row][] = $pb[1];

                $displayString .= "<td>".$pb[2]."</td>";
                $exceldata[$row][] = $pb[2];

                $displayString .= "<td>".$pb[3]."</td>";
                $exceldata[$row][] = $pb[3];

                $displayString .= "<td>".$pb[4]."</td>";
                $exceldata[$row][] = $pb[4];

                $displayString .= "<td class='rightdata'>".number_format((double)$pb[5], 2, ".", "")."</td>";
                $exceldata[$row][] = number_format((double)$pb[5], 2, ".", "");

                $displayString .= "<td class='rightdata'>".number_format((double)$pb[6], 2, ".", "")."</td>";
                $exceldata[$row][] = number_format((double)$pb[6], 2, ".", "");

                $allocatedTime = null;
                if (is_numeric($pb[6]) && ((double)$pb[6]) > 0) {
                    $allocatedTime = number_format((double)($pb[5] / $pb[6]), 2, ".", "");
                    $displayString .= "<td class='rightdata'>".$allocatedTime."</td>";
                    $exceldata[$row][] = $allocatedTime;
                } else {
                    $displayString .= "<td></td>";
                    $exceldata[$row][] = "";
                }

                if (is_numeric($pb[7])) {
                    $displayString .= "<td class='rightdata'>".number_format((double)($pb[7]), 2, ".", "")."</td>";
                    $exceldata[$row][] = number_format((double)($pb[7]), 2, ".", "");
                } else {
                    $displayString .= "<td></td>";
                    $exceldata[$row][] = "";
                }

                if (is_numeric($pb[8])) {
                    $displayString .= "<td class='rightdata'>".number_format((double)($pb[8]), 2, ".", "")."</td>";
                    $exceldata[$row][] = number_format((double)($pb[8]), 2, ".", "");
                } else {
                    $displayString .= "<td></td>";
                    $exceldata[$row][] = "";
                }
                
                if (is_numeric($pb[7])) {
                    if(($pb[5] - $pb[7]) >= 0)
                        $displayString .= "<td class='rightdata'>".number_format((double)($pb[5] - $pb[7]), 2, ".", "")."</td>";
                    else
                        $displayString .= "<td class='rightdata' style='color:#FF5900'>".number_format((double)($pb[5] - $pb[7]), 2, ".", "")."</td>";

                    $exceldata[$row][] = number_format((double)($$pb[5] - $pb[7]), 2, ".", "");
                } else {
                    $displayString .= "<td></td>";
                    $exceldata[$row][] = "";
                }
                
                if (is_numeric($allocatedTime)) {
                    if(($allocatedTime - $pb[8]) >= 0)
                        $displayString .= "<td class='rightdata'>".number_format((double)($allocatedTime - $pb[8]), 2, ".", "")."</td>";
                    else
                        $displayString .= "<td class='rightdata' style='color:#FF5900'>".number_format((double)($allocatedTime - $pb[8]), 2, ".", "")."</td>";

                    $exceldata[$row][] = number_format((double)($allocatedTime - $pb[8]), 2, ".", "");
                } else {
                    $displayString .= "<td></td>";
                    $exceldata[$row][] = "";
                }
            $displayString .= "</tr>";
            $row++;
        }
    }

    if ($displayString != "") {
        $displayString = $headers.$displayString;
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0", "reports");
?>
    <table width="100%">
        <tr>
            <td class="centerdata">
                <div id="report">
                    <?php
                        echo "<div class='on-20px'><table class='on-table-center on-table'>";
                            echo "<h6>Full Project Breakdown Report</h6>";
                            echo "".$displayString;
                            echo  "<tfoot>
                                            <tr>
                                                <td colspan='100%'></td>
                                            </tr>
                                        </tfoot>";
                        echo "</table>
                                </div>";
                        echo "<br/>";

                        if ($displayString != "") {
                            ///////////////////////////                        
                            $_SESSION["fileName"] = "Project Breakdown Report - Full";
                            $_SESSION["fileData"] = array_merge($excelheadings, $exceldata);
                            ///////////////////////////

                            echo "<input name='btnExport' onClick=\"location.href='_export.php'\" type='button' value='Export to Excel' />";
                        }
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>