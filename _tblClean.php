<?php
    session_start();

    include("_db.php");

    $scriptStart = my_microtime();
?>
<html>
    <head>
        <title>
            On-Track - Recovery Script...
        </title>
    </head>
    <body>
        <h1 align="center">
            Install - Rates/Company
        </h1>
        <?php
            //  Remove Entries With No Links
            $delete = q("DELETE FROM Project_User WHERE company_id NOT IN (SELECT id FROM Company)");
            $delete = q("DELETE FROM Project_User WHERE user_id NOT IN (SELECT id FROM Employee)");
            $delete = q("DELETE FROM Project_User WHERE project_id NOT IN (SELECT id FROM Project)");

            $delete = q("DELETE FROM Company_Users WHERE company_id NOT IN (SELECT id FROM Company)");
            $delete = q("DELETE FROM Company_Users WHERE user_id NOT IN (SELECT id FROM Employee)");

            $companies = q("SELECT id,name FROM Company");

            if (is_array($companies))       {
                foreach ($companies as $c)      {
                    $cID = $c[0];

                    $users = q("SELECT e.id, e.email FROM (Employee AS e INNER JOIN Company_Users AS cu ON e.id = cu.user_id) WHERE cu.company_ID = '".$cID."' ORDER BY e.id");

                    if (is_array($users))       {
                        $ul = "";

                        foreach ($users as $u)  $ul .= $u[0].",";

                        $ul = substr($ul, 0, -1);

                        if ($ul != "")  $delete = q("DELETE FROM Project_User WHERE user_id NOT IN (".$ul.") AND company_id = '".$cID."'");
                    }
                }
            }

            echo "Total Time  [".number_format(round(my_microtime()-$scriptStart,3),3)."s]<br/>";

            echo "<p align='center'>Rates/Company script done executing</p>";
        ?>
        <form action="index.php" method="post">
            <center><input type="submit" value="Return"/></center>
        </form>
    </body>
</html>
