<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!$_SESSION["eula_agree"] === true)
        header("Location: logout.php");

	if ($_GET["menu"] == "") {
		if (!isset($_SESSION["company_id"]))
			$company_id                                                     = null;

		if (isset($_POST["company"]) && is_numeric($_POST["company"])) {
			$_SESSION["company_id"]                                         = $_POST["company"];
			$_SESSION["company_name"]                                       = q("SELECT name FROM Company WHERE id = '".$_POST["company"]."'");
			$_SESSION["currency"]                                           = q("SELECT currency FROM Company WHERE id = '".$_POST["company"]."'");

                        $_SESSION["actions"] = q("SELECT a.`action` FROM (((roles AS r INNER JOIN role_action AS ra ON r.id = ra.roleid) INNER JOIN user_role AS ur ON r.id = ur.roleid) ".
                                                    "INNER JOIN actions AS a ON ra.actionid = a.id) WHERE ur.companyid = '".$_SESSION["company_id"]."' AND ur.userid = '".$_SESSION['user_id']."'");

			if ($_SESSION["email"] == "admin")
                            $_SESSION["actions"] = q("SELECT `action` FROM actions");

			header("Location: bookings.php");
		}
		else if (isset($_POST["company"]) && !is_numeric($_POST["company"])) {
			unset($_SESSION["company_id"]);
			unset($_SESSION["company_name"]);
			unset($_SESSION["currency"]);
		}

		if ($_SESSION["logged_in"] === true)
		{
			$count                                                          = q("SELECT COUNT(c.id) FROM (Company as c INNER JOIN Company_Users as cu ".
                                                                                                "ON c.id = cu.company_id) WHERE cu.user_id = '".$_SESSION["user_id"]."' ".
                                                                                                "AND c.locked = '0'");
			//$count                                                          = q("SELECT COUNT(id) FROM Company_Users WHERE user_id = '".$_SESSION["user_id"]."'");

			if ($count == 0)
				header("Location: locked.php");
			else if ($count == 1)
			{
				$_SESSION["company_id"]                                     = q("SELECT c.id FROM (Company as c INNER JOIN Company_Users as cu ".
																				"ON c.id = cu.company_id) WHERE cu.user_id = '".$_SESSION["user_id"]."' ".
																				"AND c.locked = '0'");
				$_SESSION["company_name"]                                   = q("SELECT name FROM Company WHERE id = '".$_SESSION["company_id"]."'");
				$_SESSION["currency"]                                       = q("SELECT currency FROM Company WHERE id = '".$_SESSION["company_id"]."'");

				$_SESSION["actions"] = q("SELECT a.`action` FROM (((roles AS r INNER JOIN role_action AS ra ON r.id = ra.roleid) INNER JOIN user_role AS ur ON r.id = ur.roleid) ".
                                                            "INNER JOIN actions AS a ON ra.actionid = a.id) WHERE ur.companyid = '".$_SESSION["company_id"]."' AND ur.userid = '".$_SESSION['user_id']."'");

                                if ($_SESSION["email"] == "admin")
                                    $_SESSION["actions"] = q("SELECT `action` FROM actions");

				if ($_SESSION["user_id"] != "93" && $_SESSION["user_id"] != "1")
					header("Location: bookings.php");
				else if ($_SESSION["user_id"] == "93")
					header("Location: invoice_load.php");
			}
		}
	}

    //  Print Header
    print_header();
    //  Print Menu
	if ($_GET["menu"] == "")
		print_menus("0", "home");
	else
		print_menus($_GET["menu"], "home");

	if ($_GET["menu"] == "") {
?>

    <div style="text-align:center"><h6 style="font-size:16pt">Welcome to On-Track</h6><br></div>

    <div id="flashContent" style="text-align:center; margin-left:auto; margin-right:auto;">
			<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="700" height="350" id="introvideo" align="middle">
				<param name="movie" value="flashvideo/introvideo.swf" />
				<param name="quality" value="high" />
				<param name="bgcolor" value="#ffffff" />
				<param name="play" value="true" />
				<param name="loop" value="false" />
				<param name="wmode" value="window" />
				<param name="scale" value="showall" />
				<param name="menu" value="false" />
				<param name="devicefont" value="false" />
				<param name="salign" value="" />
				<param name="allowScriptAccess" value="sameDomain" />
				<!--[if !IE]>-->
				<object type="application/x-shockwave-flash" data="flashvideo/introvideo.swf" width="700" height="350">
					<param name="movie" value="introvideo.swf" />
					<param name="quality" value="high" />
					<param name="bgcolor" value="#ffffff" />
					<param name="play" value="true" />
					<param name="loop" value="false" />
					<param name="wmode" value="window" />
					<param name="scale" value="showall" />
					<param name="menu" value="false" />
					<param name="devicefont" value="false" />
					<param name="salign" value="" />
					<param name="allowScriptAccess" value="sameDomain" />
				<!--<![endif]-->
					<a href="http://www.adobe.com/go/getflash">
						<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
					</a>
				<!--[if !IE]>-->
				</object>
				<!--<![endif]-->
			</object>
		</div>
        
        <div style="text-align:center"><br><a href="home.php" style="font-size:10pt"> - Continue - </a><br></div>
<?php
	}

    //  Print Footer
    print_footer();
?>
