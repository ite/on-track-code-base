<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("include/sajax.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    if (!hasAccess("REP_SUMMARY_EX"))
        header("Location: noaccess.php");

    // Class for table background color
    $colorClass = "OnTrack_#_";

    ///////////////////////////
    //  Sajax
    function getProjects($status, $type) {
        if ($status == "all")   $status = " AND p.completed IN (0,1)";
        else                    $status = " AND p.completed = '".$status."'";

        if ($type == "null")
            $projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p 
                            INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) 
                            WHERE cot.company_id = '".$_SESSION["company_id"]."' 
                            AND p.deleted = '0' $status ORDER BY UPPER(p.name)");
        else
            $projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p 
                            INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) 
                            WHERE cot.company_id = '".$_SESSION["company_id"]."' 
                            AND p.deleted = '0' 
                            AND p.type_id='$type' $status ORDER BY UPPER(p.name)");

        return $projects;
    }

    $sajax_request_type = "GET";
    sajax_init();
    sajax_export("getProjects");
    sajax_handle_client_request();
    ///////////////////////////

    //  Set Report Generated Status
    $generated = "0";

    function createRow($employee, $project_id, $project_type_id, $project_Type, $date_from, $date_to,$reportOn,$projectStatus)        {
        GLOBAL $exceldata;
        GLOBAL $row;
        GLOBAL $total_expenses;

        $where = "";

        if ($project_type_id != "null" && is_numeric($project_type_id))
            $where .= "AND p.type_id = '$project_type_id' ";

        $table = ($reportOn == "approved") ? "ApprovedExpense" : "ExpenseSheet";
        $where .= ($reportOn == "approved") ? "AND es.status = '2' " : "";

        if ($projectStatus == "all")    $where .= " AND p.completed IN (0,1) ";
        else                            $where .= " AND p.completed = '".$projectStatus."' ";

        $rowString = "";

        if ($project_id == "all")       {
            $where .= ($project_Type == "both") ? "" : (($project_Type == "invoicable") ? "AND cot.total_budget > 0 " : "AND cot.total_budget = '' ");

            $expenses = q("SELECT at.type, SUM(es.expense) FROM (Employee AS e 
                                INNER JOIN Company_Users AS cu ON e.id = cu.user_id 
                                INNER JOIN $table AS es ON e.id = es.user_id 
                                INNER JOIN Project AS p ON p.id = es.project_id 
                                INNER JOIN companiesOnTeam AS cot ON es.project_id = cot.project_id
                                LEFT JOIN Activities AS a ON es.activity_id = a.id 
                                LEFT JOIN ActivityTypes AS at ON at.id = a.parent_id) 
                            WHERE es.user_id = '".$employee[0]."' 
                                AND cu.company_id = '".$_SESSION["company_id"]."' 
                                AND es.company_id = '".$_SESSION["company_id"]."' 
                                AND cot.company_id = '".$_SESSION["company_id"]."' 
                                AND e.email != 'admin' 
                                AND es.date >= '$date_from'
                                AND es.date <= '$date_to' 
                                AND es.projectType ='CP' $where 
                            GROUP BY at.type 
                            ORDER BY UPPER(at.type)");
        } else  {
            $expenses = q("SELECT at.type, SUM(es.expense) FROM (Employee AS e 
                                INNER JOIN Company_Users AS cu ON e.id = cu.user_id 
                                INNER JOIN $table AS es ON e.id = es.user_id 
                                INNER JOIN Project AS p ON p.id = es.project_id 
                                INNER JOIN companiesOnTeam AS cot ON es.project_id = cot.project_id
                                LEFT JOIN Activities AS a ON es.activity_id = a.id 
                                LEFT JOIN ActivityTypes AS at ON at.id = a.parent_id) 
                            WHERE es.user_id = '".$employee[0]."' 
                                AND cu.company_id = '".$_SESSION["company_id"]."' 
                                AND es.company_id = '".$_SESSION["company_id"]."' 
                                AND cot.company_id = '".$_SESSION["company_id"]."' 
                                AND e.email != 'admin' 
                                AND es.date >= '$date_from'
                                AND es.date <= '$date_to' 
                                AND p.id = '$project_id' $where 
                            GROUP BY at.type 
                            ORDER BY UPPER(at.type)");
        }

        if (is_array($expenses))        {
            foreach ($expenses as $ex)  {
                $rowString .= "<tr>
                                    <td align='left'>".$employee[1]." ".$employee[2]."</td>
                                    <td>".$ex[0]."</td>
                                    <td class='rightdata'>".number_format($ex[1], 2, ".", "")."</td>
                                    <td class='rightdata'>".number_format($ex[1], 2, ".", "")."</td>
                                </tr>";

                $exceldata[$row][] = $employee[1]." ".$employee[2];
                $exceldata[$row][] = $ex[0];
                $exceldata[$row][] = number_format($ex[1], 2, ".", "");
                $exceldata[$row][] = number_format($ex[1], 2, ".", "");
                $row++;

                $total_expenses += $ex[1];
            }
        }

        return $rowString;
    }

    //  Create Display Information Function
    if (isset($_POST["save"]) && $_POST["save"] === "1") {
        $errorMessage = "";
        $displayString = "";
        $where = "";

        $reportOn = $_POST["reportOn"];
        $projectStatus = $_POST["projectStatus"];

        $project_type_id = $_POST["projectTypeID"];
        $project_id = $_POST["project"];
        $date_from = addslashes(strip_tags($_POST["date_from"]));
        $date_to = addslashes(strip_tags($_POST["date_to"]));

        if ($project_type_id != "null" && is_numeric($project_type_id))
            $where .= "AND p.type_id = '$project_type_id' ";

        $table = ($reportOn == "approved") ? "ApprovedExpense" : "ExpenseSheet";
        $where .= ($reportOn == "approved") ? "AND es.status = '2' " : "";

        if ($projectStatus == "all")    $where .= " AND p.completed IN (0,1) ";
        else                            $where .= " AND p.completed = '".$projectStatus."' ";

        ///////////////////////////
        //  Get Info From Tables
        ///////////////////////////
        if ($project_id == "all") {
            $project_name = "All Projects <a class='on-validate-error'>(Shared Projects not included)</a>";
            $project_Type = $_POST["projectType"];
        } else  {
            $project_name = q("SELECT name From Project WHERE id = '$project_id'");
            $project_Type = "both";
        }

        if ($project_id == "all")       {
            $where .= ($project_Type == "both") ? "" : (($project_Type == "invoicable") ? "AND cot.total_budget > 0 " : "AND cot.total_budget = '' ");

            $employees = q("SELECT DISTINCT(e.id), e.frstname, e.lstname, e.email FROM (Employee AS e 
                                INNER JOIN Company_Users AS cu ON e.id = cu.user_id 
                                INNER JOIN $table AS es ON e.id = es.user_id 
                                INNER JOIN Project AS p ON p.id = es.project_id 
                                INNER JOIN companiesOnTeam AS cot ON es.project_id = cot.project_id) 
                            WHERE cu.company_id = '".$_SESSION["company_id"]."' 
                                AND es.company_id = '".$_SESSION["company_id"]."' 
                                AND cot.company_id = '".$_SESSION["company_id"]."' 
                                AND e.email != 'admin' 
                                AND es.date >= '$date_from'
                                AND es.date <= '$date_to' 
                                AND es.projectType ='CP' $where 
                            ORDER BY e.lstname, e.frstname");
        } else  {
            $employees = q("SELECT DISTINCT(e.id), e.frstname, e.lstname, e.email FROM (Employee AS e 
                                INNER JOIN Company_Users AS cu ON e.id = cu.user_id 
                                INNER JOIN $table AS es ON e.id = es.user_id 
                                INNER JOIN Project AS p ON p.id = es.project_id 
                                INNER JOIN companiesOnTeam AS cot ON es.project_id = cot.project_id) 
                            WHERE cu.company_id = '".$_SESSION["company_id"]."' 
                                AND es.company_id = '".$_SESSION["company_id"]."' 
                                AND cot.company_id = '".$_SESSION["company_id"]."' 
                                AND e.email != 'admin' 
                                AND es.date >= '$date_from'
                                AND es.date <= '$date_to' 
                                AND p.id = '$project_id' $where 
                            ORDER BY e.lstname, e.frstname");
        }

        ///////////////////////////
        //  Create Table Headers
        $headers = "<tr>
                        <th class='centerdata'>Employee Name</th>
                        <th class='centerdata'>Activity Type</th>
                        <th class='centerdata'>Expenses</th>
                        <th class='centerdata'>Total <i>(".$_SESSION["currency"].")</i></th>
                    </tr>";

        $row = 0;

        $exceldata[$row][] = "Summary Report (Expenses) : ".$date_from."  -  ".$date_to;
        $exceldata[$row][] = "";
        $exceldata[$row][] = "";
        $exceldata[$row][] = "";
        $row++;

        $exceldata[$row][] = "Project: ".strip_tags($project_name);
        $exceldata[$row][] = "";
        $exceldata[$row][] = "";
        $exceldata[$row][] = "";
        $row++;

        $exceldata[$row][] = $colorClass."Employee Name";
        $exceldata[$row][] = $colorClass."Activity Type";
        $exceldata[$row][] = $colorClass."Expenses";
        $exceldata[$row][] = $colorClass."Total (".$_SESSION["currency"].")";
        $row++;

        if (is_array($employees))       {
            foreach ($employees as $employee)   {
                $displayString .= createRow($employee, $project_id, $project_type_id, $project_Type, $date_from, $date_to,$reportOn,$projectStatus);
            }
        }

        $displayString .= "<tr><td class='on-table-total'></td><td class='on-table-total'>Grand Total <i>(".$_SESSION["currency"].")</i>:</td>";
        $displayString .= "<td class='on-table-total'>".number_format($total_expenses, 2, ".", "")."</td>";
        $displayString .= "<td class='on-table-total'>".number_format($total_expenses, 2, ".", "")."</td></tr>";

        $exceldata[$row][] ="";
        $exceldata[$row][] = "Grand Total (".$_SESSION["currency"].")";
        $exceldata[$row][] = number_format($total_expenses, 2, ".", "");
        $exceldata[$row][] = number_format($total_expenses, 2, ".", "");

        $displayString = "<tr><td class='on-table-clear' colspan='100%'><a>Summary Report (Expenses) ".$date_from." - ".$date_to."</a></td></tr>
                            <tr><td class='on-table-clear' colspan='100%'>Project: ".$project_name."</td></tr>".
                            $headers.$displayString;

        if ($displayString != "")       $generated = "1";
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("3", "reports");
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    jQuery(function(){
	jQuery('#date_from').DatePicker({
            format:'Y-m-d',
            date: jQuery('#date_from').val(),
            current: jQuery('#date_from').val(),
            starts: 1,
            position: 'right',
            onBeforeShow: function(){
                var _date;
                if (!validation('date',jQuery('#date_from').val()))     _date = new Date();
                else                                                    _date = jQuery('#date_from').val();
                jQuery('#date_from').DatePickerSetDate(_date, true);
            },
            onChange: function(formated, dates){
                jQuery('#date_from').val(formated);
                jQuery('#date_from').DatePickerHide();
            }
	});

	jQuery('#date_to').DatePicker({
            format:'Y-m-d',
            date: jQuery('#date_to').val(),
            current: jQuery('#date_to').val(),
            starts: 1,
            position: 'right',
            onBeforeShow: function(){
                var _date;
                if (!validation('date',jQuery('#date_to').val()))       _date = new Date();
                else                                                    _date = jQuery('#date_to').val();
                jQuery('#date_to').DatePickerSetDate(_date, true);
            },
            onChange: function(formated, dates){
                jQuery('#date_to').val(formated);
                jQuery('#date_to').DatePickerHide();
            }
	});
    });

    function check()    {
        var valid = 1;

        //  Check That Employee Is Selected
        if (document.forms["report_summary"].project.value == "null")   {
            ShowLayer("projectDiv", "block");
            valid = 0;
        }
        else
            ShowLayer("projectDiv", "none");

        //  Check That Date From Is Entered
        if (document.forms["report_summary"].date_from.value == "")     {
            ShowLayer("dateFrom", "block");
            valid = 0;
        }
        //  Check That Entered Date From Is Valid
        else    {
            ShowLayer("dateFrom", "none");

            if (!validation("date", document.forms["report_summary"].date_from.value))  {
                ShowLayer("dateFromDiv", "block");
                valid = 0;
            }
            else
                ShowLayer("dateFromDiv", "none");
        }

        //  Check That Date To Is Entered
        if (document.forms["report_summary"].date_to.value == "")       {
            ShowLayer("dateTo", "block");
            valid = 0;
        }
        //  Check That Entered Date To Is Valid
        else    {
            ShowLayer("dateTo", "none");

            if (!validation("date", document.forms["report_summary"].date_to.value))    {
                ShowLayer("dateToDiv", "block");
                valid = 0;
            }
            else
                ShowLayer("dateToDiv", "none");
        }

        if (valid == 1) {
            document.forms["report_summary"].save.value = 1;
            document.forms["report_summary"].submit();
        }
    }

    function displayCheck(select)       {
        if (select.value == "all")      ShowLayer("projectTypeDiv", "block");
        else                            ShowLayer("projectTypeDiv", "none");
    }

    ///////////////////////////
    //  Sajax
    <?php sajax_show_javascript(); ?>

    function ClearOptions(OptionList)   {
        for (x = OptionList.length; x >= 0; x--)        {
            OptionList[x] = null;
        }
    }

    function setProjects(data)  {
        var c = 0;
        document.report_summary.project.options[(c++)] = new Option("--  Select A Project  --", "null");
        document.report_summary.project.options[(c++)] = new Option("All Projects", "all");

        for (var i in data)
            eval("document.report_summary.project.options["+(c++)+"] = new Option('" + data[i][1] + "', '" + data[i][0] + " ');")
    }

    function getProjects(select1,select2) {
        var status = eval("document.report_summary."+select1+".value;");
        var type = eval("document.report_summary."+select2+".value;");;

        ClearOptions(document.report_summary.project);
        x_getProjects(status, type, setProjects);
    }
    ///////////////////////////
</script>
<?php
    $n[0][0] = "0"; $n[0][1] = "Shared Projects";
    $projectTypes = q("SELECT id, type FROM ProjectTypes WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY type");

    if (!is_array($projectTypes))       $projectTypes = $n;
    else                                $projectTypes = array_merge($n,$projectTypes);

    $projectTypes = sortArrayByColumn($projectTypes, 1);
    
    $projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) 
                    WHERE cot.company_id = '".$_SESSION["company_id"]."' AND p.completed = '0' AND p.deleted = '0' ORDER BY UPPER(p.name)");

?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="report_summary">
                    <div id="" style="<?php if ($generated == "1") echo "display: none;"; else echo "display: block"; ?>">
                        <table width="100%">
                            <tr>
                                <td class="centerdata">
                                    <h6>Summary Report (Expense Based)</h6>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Report On:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="reportOn">
                                        <option value="normal">Actual Time</option>
                                        <option value="approved">Approved Time</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Project Status:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="projectStatus" onChange="getProjects('projectStatus','projectTypeID');">
                                        <option value="all">Completed & Uncompleted</option>
                                        <option value="0" selected>Uncompleted</option>
                                        <option value="1">Completed</option>
                                    </select>
                                </td>
                            </tr>
                            <tr><td colspan="100%"><br/></td></tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Project Type:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="projectTypeID" onChange="getProjects('projectStatus','projectTypeID');">
                                        <option value="null">All Project Types</option>
                                        <?php
                                            if (is_array($projectTypes))
                                                foreach ($projectTypes as $projectType)
                                                    if ($_POST["projectTypeID"] == $projectType[0])
                                                        echo "<option value='".$projectType[0]."' selected>".$projectType[1]."</option>";
                                                    else
                                                        echo "<option value='".$projectType[0]."'>".$projectType[1]."</option>";
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Project:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" id="project" name="project" onChange="displayCheck(project);">
                                        <option value="null">--  Select A Project  --</option>
                                        <option value="all">All Projects</option>
                                        <?php
                                            if (is_array($projects))
                                                foreach ($projects as $project)
                                                    if (isset($_POST["project"]) && $_POST["project"] == $project[0])
                                                        echo "<option value='".$project[0]."' selected>".$project[1]."</option>";
                                                    else
                                                        echo "<option value='".$project[0]."'>".$project[1]."</option>";
                                        ?>
                                    </select>
                                    <div id="projectDiv" style="display: none;"><font class="on-validate-error">* Project must be selected</font></div>
                                </td>
                            </tr>
                        </table>
                        <div id="projectTypeDiv" style="display: none;">
                            <table width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Project Type:
                                    </td>
                                    <td class="on-description-left"width="50%">
                                        <input method="post" name="projectType" type="radio" value="both" checked><a>Invoicable &amp; Non-Invoicable</a><br/>
                                        <input method="post" name="projectType" type="radio" value="invoicable"><a>Invoicable</a><br/>
                                        <input method="post" name="projectType" type="radio" value="non_invoicable"><a>Non-Invoicable</a><br/>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Date From:
                                </td>
                                <td width="50%">
                                    <input class="on-field-date" id="date_from" name="date_from" type="text" style="text-align:right;" value="<?php
                                        if ($_POST["date_from"] != "") echo "".$_POST["date_from"]; else echo "".getMonthStart($today); ?>">
                                    <div id="dateFrom" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                    <div id="dateFromDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Date To:
                                </td>
                                <td width="50%">
                                    <input class="on-field-date" id="date_to" name="date_to" type="text" style="text-align:right;" value="<?php
                                        if ($_POST["date_to"] != "") echo "".$_POST["date_to"]; else echo "".getMonthEnd($today); ?>">
                                    <div id="dateTo" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                    <div id="dateToDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <input name="btnGenerate" onClick="check();" type="button" value="Generate Report">
                        <input method="post" name="save" type="hidden" value="0" />
                    </div>
                </form>
                <div id="report" style="<?php if ($generated == "1") echo "display: block;"; else echo "display: none"; ?>">
                    <?php
                        echo "<table class='on-table-center on-table'>";
                            echo "".$displayString;
                            echo  "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        echo "</table>";
                        echo "<br/>";

                        ///////////////////////////
                        //  Set Export Information
                        $_SESSION["fileName"] = "Report Summary (Expenses)";
                        $_SESSION["fileData"] = $exceldata;
                        ///////////////////////////

                        echo "<input name='btnExport' onClick=\"location.href='_export.php'\" type='button' value='Export to Excel' />";
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
