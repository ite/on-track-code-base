<?php

    //  Set Time Zone
    date_default_timezone_set("Africa/Johannesburg");
    session_set_cookie_params(86400);
    ini_set('session.gc_maxlifetime', 86400);
    
    session_start();

    include("conn/_dbconn.php");

	//  GLOBAL LINKS HERE
	$serverName = getServerName();

	if ($serverName == "live.on-track.ws" || $serverName == "live.on-track.net.za")	{
		$file_directory = "file_repo/";
//		$file_directory = "/file_repo/ontrack_live/";
        $urlLink = "http://live.on-track.ws/";
        $analytics = "live.js";
	} else if ($serverName == "demo.on-track.ws")	{
		$file_directory = "/file_repo/ontrack_demo/";
        $urlLink = "http://demo.on-track.ws/";
        $analytics = "demo.js";
	} else if ($serverName == "test.on-track.ws")	{
		$file_directory = "/file_repo/ontrack_test/";
        $urlLink = "http://test.on-track.ws/";
        $analytics = "test.js";
	} else	{
		$file_directory = "files/";
        $urlLink = "http://localhost/On-Track/";
        $analytics = "local.js";
	}

	function getServerName() {
		return $_SERVER["SERVER_NAME"];
	}

    //  Get Micro Time
    function my_microtime($precision = 4) {     return round(microtime(true), $precision);      }

    // check if the user has access
    function hasAccess($action)    {
		if (!is_array($_SESSION["actions"]))	return 0;
        return (in_array($action, $_SESSION["actions"]));
/*
        if (is_array($_SESSION["actions"]))
            foreach($_SESSION["actions"] as $a) {
                if ($a[0] == $action)
                    return 1;
            }
		return 0;
*/
    }

    //  Get Fusion Chart Background && Text Colour Variable
    function fusionChart()      {
        $canvasbgColor = "E0DFDD";
        $baseFontColor = "1F1E1C";
        $bgColor = "E0DFDD";
        $legendBgColor = "E0DFDD";
        $legendBorderColor = "E0DFDD";

        return " showCanvasBg='0' baseFontColor='$baseFontColor' bgColor='$bgColor' bgAlpha='100' canvasBgAlpha='100' legendBgColor='$legendBgColor' legendBorderColor='$legendBorderColor' legendShadow='0'";
    }

    //  Set Table Border & Background Details
    $top = "border-top: 1px solid #0A0F15;";
    $left = "border-left: 1px solid #0A0F15;";
    $right = "border-right: 1px solid #0A0F15;";
    $bottom = "border-bottom: 1px solid #0A0F15;";
    $background = "background-color:#3E638F;";

    // //  Set Time Zone
    // date_default_timezone_set("Africa/Johannesburg");
    // session_set_cookie_params(86400);
    // ini_set('session.gc_maxlifetime', 86400);

    //  Set Link Details
    $link = "color:#336699;font-weight:normal;";

    //  Set Current Days Date
    $today = date("Y-m-d");

    //  Fix Array
    function fixarr($arr) {
        if (!is_array($arr)) {
			if ($arr == "0")	$arr = "";
            $a[] = $arr;
            $arr = $a;
            unset($a);
        }

        return $arr;
    }

    //  Mailing Function
    function sendmail($t, $s, $m, $f, $r, $html = false)    {
        if ($html === false) {
            $headers = "From: $f"."\r\n".
                "Reply-To: $r"."\r\n".
                "X-Mailer: PHP/".phpversion();
        }
        else    {
            $headers = "MIME-Version: 1.0"."\r\n".
                "Content-type: text/html; charset=utfa"."\r\n".
                "From: $f"."\r\n" .
                "Reply-To: $r"."\r\n";
        }

        return (mail($t, $s, $m, $headers)) ? 1 : 0;
    }

    //  Password Hash Generation
    define("SALT_LENGTH", 9);
    $origsalt = "";

    function generateHash($plainText, $salt = null) {
		if ($salt === null)
            $salt = substr(md5(uniqid(rand(), true)), 0, SALT_LENGTH);
		else
            $salt = substr($salt, 0, SALT_LENGTH);

		global $origsalt;

		$origsalt = $salt;

		return $salt.hash("sha512", ($salt.$plainText));
    }

    //  MySQL Query Run Procedure
    function q($query, $assoc = 0) {
        global $dblink;

        $r = @mysqli_query($dblink, $query);

        if (mysqli_errno($dblink)) {
            $error = "MYSQL ERROR #".mysqli_errno($dblink)." : <small>".mysqli_error($dblink)."</small><br><VAR>$query</VAR>";

            echo($error);

            return FALSE;
        }

        if (strtolower(substr($query, 0, 6)) != "select")
            return array(mysqli_affected_rows(), mysqli_insert_id());

        $count = @mysqli_num_rows($r);

        if (!$count)
            return 0;

        if ($count == 1) {
            if ($assoc)
                $f = mysqli_fetch_assoc($r);
            else
                $f = mysqli_fetch_row($r);

            mysqli_free_result($r);

            if (count($f) == 1) {
                list($key) = array_keys($f);

                return $f[$key];
            }
            else {
                $all = array();
                $all[] = $f;

                return $all;
            }
        }
        else {
            $all = array();

            for ($i = 0; $i < $count; $i++) {
                if ($assoc)
                    $f = mysqli_fetch_assoc($r);
                else
                    $f = mysqli_fetch_row($r);

                $all[] = $f;
            }

            mysqli_free_result($r);

            return $all;
        }
    }

    //  Sort Array By Column
    function sortArrayByColumn($array, $column) {
        $size = count($array);

        for ($a = 0; $a < ($size - 1); $a++) {
            for ($b = $a + 1; $b < $size; $b++) {
                if ($array[$a][$column] == $array[$b][$column]) {
                    if ($array[$a][$column] < $array[$b][$column]) {
                        $keep = $array[$a];
                        $array[$a] = $array[$b];
                        $array[$b] = $keep;
                    }
                }
                else if ($array[$a][$column] > $array[$b][$column]) {
                    $keep = $array[$a];
                    $array[$a] = $array[$b];
                    $array[$b] = $keep;
                }
            }
        }

        return $array;
    }

    //  Filter Through Query Data - Remove Duplicates
    function filterQueryData($query, $count, $watch) {
        $i = 0;

        for ($a = 0; $a < $count; $a++)
            $new[$i][$a] = $query[0][$a];

        if (is_array($query))
            foreach ($query as $q) {
                if (!($q[$watch] == $new[$i][$watch])) {
                    $i++;

                    for ($a = 0; $a < $count; $a++)
                        $new[$i][$a] = $q[$a];
                }
            }

        return $new;
    }

    //  Check If Record Exists
    function exist($table, $where) {
        $exist = q("SELECT * FROM $table WHERE $where");

        if (is_array($exist))
            return true;
        else
            return false;
    }

    //  Check If Date Is Public Holiday
    function isPublicHoliday($date) {
        $publicHoliday = q("SELECT * FROM publicHolidays WHERE date = '".$date."'");

        if (is_array($publicHoliday))   return true;
        else                            return false;
    }

    //  Check How Many Public Holiday Between Dates
    function totalPublicHolidays($dateFrom, $dateTo) {
        $publicHolidays = q("SELECT COUNT(id) FROM publicHolidays WHERE date BETWEEN '".$dateFrom."' AND '".$dateTo."'");

        return $publicHolidays;
    }

    //  Check If User Is/Was On Leave
    function checkLeave($dateFrom, $dateTo, $companyID, $userID)  {
        $date = $dateFrom;

        while ($date <= $dateTo)        {
            $check = q("SELECT * FROM LeaveBooked WHERE company_id = '".$companyID."' AND user_id = '".$userID."' AND '".$date."' BETWEEN start_date AND end_date");

            if (is_array($check))       return true;

            $date = getDates($date, 1);
        }

        return false;
    }

    // Check If User Has Seen @Object
    function seen($user, $object)       {
        $q = q("SELECT id, $object FROM seen WHERE user_id = '".$user."' LIMIT 1");     // Select User data
        if (!is_array($q))      {
            $insert = q("INSERT INTO seen (user_id) VALUES ('".$user."')");     // Insert User ID
            return 0;
        }

        if ($q[0][1] != 1)      return 0;
        else                    return 1;
    }

    // Check If Invoice Has Payment Certificate
    function hasPaymentCertificate($invoiceID)       {
		global $file_directory;

		if (file_exists(preg_replace("/ /","_",$file_directory."certificates/Payment Certificate - ".$invoiceID.".xlsx")))	return 1;
		else	return 0;
    }

    // Check If Invoice Has Payment Certificate
    function hasBackingDocumentation($invoiceID)       {
		global $file_directory;
		$files = 0;

		$entries = q("SELECT expensesheet_id, id FROM ApprovedExpense WHERE invoiceID = '".$invoiceID."'");

		if (is_array($entries))	{
			foreach ($entries as $e)	{
				if (is_dir($file_directory.$e[0]))	$files++;
			}
		}

		return ($files > 0) ? 1 : 0;
    }

    // Check If Expense Has Backing Documentation
    function hasBackingDocuments($expenseID)       {
		global $file_directory;
		$files = 0;

		if (is_dir($file_directory.$expenseID))	$files++;

		return ($files > 0) ? 1 : 0;
    }

    //  Returns ARGB Color Code For Excel Exports
    function getARGBColor($cls) {
        $color = "";

        switch ($cls)   {
            case "OnTrack":{
                $color = "FFDCDCDC";        // Remember FF in front of RGB val
                break;
            }
        }

        return $color;
    }

	//  Get File Content Type
	function content_type($filename) {
        $mime_types = array(

            "txt" => "text/plain",
            "htm" => "text/html",
            "html" => "text/html",
            "php" => "text/html",
            "css" => "text/css",
            "js" => "application/javascript",
            "json" => "application/json",
            "xml" => "application/xml",
            "swf" => "application/x-shockwave-flash",
            "flv" => "video/x-flv",

            // images
            "png" => "image/png",
            "jpe" => "image/jpeg",
            "jpeg" => "image/jpeg",
            "jpg" => "image/jpeg",
            "gif" => "image/gif",
            "bmp" => "image/bmp",
            "ico" => "image/vnd.microsoft.icon",
            "tiff" => "image/tiff",
            "tif" => "image/tiff",
            "svg" => "image/svg+xml",
            "svgz" => "image/svg+xml",

            // archives
            "zip" => "application/zip",
            "rar" => "application/x-rar-compressed",
            "exe" => "application/x-msdownload",
            "msi" => "application/x-msdownload",
            "cab" => "application/vnd.ms-cab-compressed",

            // audio/video
            "mp3" => "audio/mpeg",
            "qt" => "video/quicktime",
            "mov" => "video/quicktime",

            // adobe
            "pdf" => "application/pdf",
            "psd" => "image/vnd.adobe.photoshop",
            "ai" => "application/postscript",
            "eps" => "application/postscript",
            "ps" => "application/postscript",

            // ms office
            "doc" => "application/msword",
            "docx" => "application/msword",
            "rtf" => "application/rtf",
            "xls" => "application/vnd.ms-excel",
            "xlsx" => "application/vnd.ms-excel",
            "ppt" => "application/vnd.ms-powerpoint",

            // open office
            "odt" => "application/vnd.oasis.opendocument.text",
            "ods" => "application/vnd.oasis.opendocument.spreadsheet",
        );

        //$ext = strtolower(array_pop(explode(".",$filename)));
        $ext = substr(strrchr($filename, "."), 1);

        if (array_key_exists($ext, $mime_types))	{
            return $mime_types[$ext];
        }	elseif (function_exists("finfo_open"))	{
            $finfo = finfo_open(FILEINFO_MIME);
            $mimetype = finfo_file($finfo, $filename);
            finfo_close($finfo);
            return $mimetype;
        } else	{
            return "application/octet-stream";
        }
    }

	// Calculate distance between two lat,lon points in KMs
	function distance_between_points($lat1, $lon1, $lat2, $lon2) {
        $constant = 180 / pi();
        $radius = 6378.1; //  KM radius of Earth

        return $radius * acos(sin($lat1/$constant) * sin($lat2/$constant) + cos($lat1/$constant) * cos($lat2/$constant) *  cos($lon2/$constant - $lon1/$constant)); // KM
    }


?>
