<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("fusion/FusionCharts.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    if (!hasAccess("REP_INV_VS_CTC_OWN") && !hasAccess("REP_INV_VS_CTC_OWN_V2"))
        header("Location: noaccess.php");

    //  Set Report Generated Status
    $generated                                                          = "0";

    $fontColor = "";

    function createXMLString($date_from, $date_to, $employee_id,$table,$where)  {
        $xmlString                                                      = "";

        $xmlString                                                      = "<chart palette='1' caption='Invoice Potential vs. Cost to Company Report' shownames='1' showvalues='0' ".
                                                                            "numberPrefix='".$_SESSION["currency"]."' showSum='1' decimals='0' overlapColumns='0' ".
                                                                            "formatNumberScale='0' formatNumber='0' labelDisplay='rotate' ".fusionChart().">";

        //  Create Categories
        $xmlString                                                      .= "<categories>";

        if ($employee_id == "all")
        {
            //  nEmployees                                              = Number of Employees
            $nEmployees                                                 = q("SELECT COUNT(DISTINCT(e.id)) FROM (Employee AS e INNER JOIN Company_Users ".
                                                                            "AS cu ON e.id = cu.user_id INNER JOIN $table AS ts ON e.id = ts.user_id) ".
                                                                            "WHERE cu.company_id = '".$_SESSION["company_id"]."' AND ts.date >= '$date_from' ".
                                                                            "AND ts.date <= '$date_to' AND e.email != 'admin' $where");
            $employees                                                  = q("SELECT DISTINCT(e.id), e.lstname, e.frstname FROM (Employee AS e INNER JOIN Company_Users ".
                                                                            "AS cu ON e.id = cu.user_id INNER JOIN $table AS ts ON e.id = ts.user_id) ".
                                                                            "WHERE cu.company_id = '".$_SESSION["company_id"]."' AND ts.date >= '$date_from' ".
                                                                            "AND ts.date <= '$date_to' AND e.email != 'admin' $where ORDER BY e.lstname, frstname");

            //  Create Categories
            if ($nEmployees > 1)
            {
                foreach ($employees as $employee)
                    $xmlString                                          .= "<category label='".$employee[1].", ".$employee[2]."'></category>";
            }
            else if ($nEmployees == 1)
                $xmlString                                              .= "<category label='".$employees[0][1].", ".$employees[0][2]."'></category>";
        }
        else
        {
            $employee_info                                              = q("SELECT lstname, frstname FROM Employee WHERE id = '$employee_id'");

            $xmlString                                                  .= "<category label='".$employee_info[0][0].", ".$employee_info[0][1]."'></category>";
        }

        $xmlString                                                      .= "</categories>";

        //  Create Arrays
        $invoiced_array                                                 = array();
        $cost_array                                                     = array();

        if ($employee_id == "all")
        {
            if ($nEmployees > 1)
            {
                foreach ($employees as $employee)
                {
                    $invoiced                                           = q("SELECT SUM(ts.rate * ts.time_spent) FROM ($table AS ts INNER JOIN companiesOnTeam AS cot ".
                                                                            "ON ts.project_id = cot.project_id)  WHERE cot.total_budget > 0 AND ts.user_id = '".$employee[0]."' ".
                                                                            "AND ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                                                            "AND ts.company_id = '".$_SESSION["company_id"]."' $where");
                    $cost                                               = q("SELECT (SUM(ts.time_spent) * e.cost) FROM ($table AS ts INNER JOIN Employee AS e ".
                                                                            "ON ts.user_id = e.id INNER JOIN Project AS p ON p.id = ts.project_id) ".
                                                                            "WHERE ts.user_id = '".$employee[0]."' AND ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                                                            "AND p.company_id = '".$_SESSION["company_id"]."' $where");

                    $invoiced_array[$employee[0]]                       = $invoiced;
                    $cost_array[$employee[0]]                           = $cost;
                }
            }
            else if ($nEmployees == 1)
            {
                $invoiced                                               = q("SELECT SUM(ts.rate * ts.time_spent) FROM ($table AS ts INNER JOIN companiesOnTeam AS cot ".
                                                                            "ON ts.project_id = cot.project_id) WHERE cot.total_budget > 0 AND ts.user_id = '".$employees[0][0]."' ".
                                                                            "AND ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                                                            "AND cot.company_id = '".$_SESSION["company_id"]."' $where");
                $cost                                                   = q("SELECT (SUM(ts.time_spent) * e.cost) FROM ($table AS ts INNER JOIN Employee AS e ".
                                                                            "ON ts.user_id = e.id INNER JOIN Project AS p ON p.id = ts.project_id) ".
                                                                            "WHERE ts.user_id = '".$employees[0][0]."' AND ts.date >= '$date_from' ".
                                                                            "AND ts.date <= '$date_to' AND p.company_id = '".$_SESSION["company_id"]."' $where");

                $invoiced_array[$employees[0][0]]                       = $invoiced;
                $cost_array[$employees[0][0]]                           = $cost;
            }
        }
        else
        {
            $invoiced                                                   = q("SELECT SUM(ts.rate * ts.time_spent) FROM ($table AS ts INNER JOIN companiesOnTeam AS cot ".
                                                                            "ON ts.project_id = cot.project_id) WHERE cot.total_budget > 0 AND ts.user_id = '".$employee_id."' ".
                                                                            "AND ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                                                            "AND cot.company_id = '".$_SESSION["company_id"]."' $where");
            $cost                                                       = q("SELECT (SUM(ts.time_spent) * e.cost) FROM ($table AS ts INNER JOIN Employee AS e ".
                                                                            "ON ts.user_id = e.id INNER JOIN Project AS p ON p.id = ts.project_id) ".
                                                                            "WHERE ts.user_id = '".$employee_id."' AND ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                                                            "AND p.company_id = '".$_SESSION["company_id"]."' $where");

            $invoiced_array[$employee_id]                               = $invoiced;
            $cost_array[$employee_id]                                   = $cost;
        }

        //  Create Dataset - Invoiced per Employee
        $xmlString                                                      .= "<dataset seriesName='Invoice Potential' color='0033FF' showValues='0'>";

        if ($employee_id == "all")
        {
            if ($nEmployees > 1)
                foreach ($employees as $employee)
                    $xmlString                                          .= "<set value='".$invoiced_array[$employee[0]]."'/>";
            else if ($nEmployees == 1)
                $xmlString                                              .= "<set value='".$invoiced_array[$employees[0][0]]."'/>";
        }
        else
            $xmlString                                                  .= "<set value='".$invoiced_array[$employee_id]."'/>";

        $xmlString                                                      .= "</dataset>";

        //  Create Dataset - Cost to Company per Employee
        $xmlString                                                      .= "<dataset seriesName='Cost to Company' color='FF0033' showValues='0'>";

        if ($employee_id == "all")
        {
            if ($nEmployees > 1)
                foreach ($employees as $employee)
                    $xmlString                                          .= "<set value='".$cost_array[$employee[0]]."'/>";
            else if ($nEmployees == 1)
                $xmlString                                              .= "<set value='".$cost_array[$employees[0][0]]."'/>";
        }
        else
            $xmlString                                                  .= "<set value='".$cost_array[$employee_id]."'/>";

        $xmlString                                                      .= "</dataset>";

        //  Create Dataset - Invoice Potential - Cost to Company per EmployeeAverage
        $xmlString                                                      .= "<dataset seriesName='Invoice Potential - Cost to Company' color='33FF00' showValues='0'>";

        if ($employee_id == "all")
        {
            if ($nEmployees > 1)
                foreach ($employees as $employee)
                    $xmlString                                          .= "<set value='".($invoiced_array[$employee[0]] - $cost_array[$employee[0]])."'/>";
            else if ($nEmployees == 1)
                $xmlString                                              .= "<set value='".($invoiced_array[$employees[0][0]] - $cost_array[$employees[0][0]])."'/>";
        }
        else
            $xmlString                                                  .= "<set value='".($invoiced_array[$employee_id] - $cost_array[$employee_id])."'/>";

        $xmlString                                                      .= "</dataset>";

        $xmlString                                                      .= "<trendLines>";
        $xmlString                                                      .= "<line startValue='0' color='000000' showOnTop='1' thickness='4' />";
        $xmlString                                                      .= "</trendLines>";

        $xmlString                                                      .= "</chart>";

        return $xmlString;
    }

    function createTable($date_from, $date_to, $employee_id,$table,$where)
    {

        $tableString                                                    = "";

        $headers                                                        = "<tr>
                                                                                    <th>Employee Name</th>
                                                                                    <th>Invoice Potential <i>(".$_SESSION["currency"].")</i><br/><i>(excl. VAT)</i></th>
                                                                                    <th>Cost to<br/>Company <i>(".$_SESSION["currency"].")</i><br/><i>(excl. VAT)</i></td>
                                                                                    <th>Differance <i>(".$_SESSION["currency"].")</i><br/><i>(excl. VAT)</i></th>
                                                                                </tr>";

        if ($employee_id == "all")
        {
            //  nEmployees                                              = Number of Employees
            $nEmployees                                                 = q("SELECT COUNT(DISTINCT(e.id)) FROM (Employee AS e INNER JOIN Company_Users ".
                                                                            "AS cu ON e.id = cu.user_id INNER JOIN $table AS ts ON e.id = ts.user_id) ".
                                                                            "WHERE cu.company_id = '".$_SESSION["company_id"]."' AND ts.date >= '$date_from' ".
                                                                            "AND ts.date <= '$date_to' AND e.email != 'admin' $where");
            $employees                                                  = q("SELECT DISTINCT(e.id), e.lstname, e.frstname FROM (Employee AS e INNER JOIN Company_Users ".
                                                                            "AS cu ON e.id = cu.user_id INNER JOIN $table AS ts ON e.id = ts.user_id) ".
                                                                            "WHERE cu.company_id = '".$_SESSION["company_id"]."' AND ts.date >= '$date_from' ".
                                                                            "AND ts.date <= '$date_to' AND e.email != 'admin' $where ORDER BY e.lstname, frstname");
        }
        else
        {
            $employee_info                                              = q("SELECT lstname, frstname FROM Employee WHERE id = '$employee_id'");
        }

        //  Create Arrays
        if ($employee_id == "all")
        {
            if ($nEmployees > 1)
            {
                foreach ($employees as $employee)
                {
                    $invoiced                                           = q("SELECT SUM(ts.rate * ts.time_spent) FROM ($table AS ts INNER JOIN companiesOnTeam AS cot ".
                                                                            "ON ts.project_id = cot.project_id) WHERE cot.total_budget > 0 AND ts.user_id = '".$employee[0]."' ".
                                                                            "AND ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                                                            "AND cot.company_id = '".$_SESSION["company_id"]."' $where");
                    $cost                                               = q("SELECT (SUM(ts.time_spent) * e.cost) FROM ($table AS ts INNER JOIN Employee AS e ".
                                                                            "ON ts.user_id = e.id INNER JOIN Project AS p ON p.id = ts.project_id) ".
                                                                            "WHERE ts.user_id = '".$employee[0]."' AND ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                                                            "AND p.company_id = '".$_SESSION["company_id"]."' $where");

                    $tableString                                        .= "<tr>".
                                                                            "<td>".$employee[1].", ".$employee[2]."</td>".
                                                                            "<td class='rightdata'>".number_format($invoiced, 2, ".", "")."</td>".
                                                                            "<td class='rightdata'>".number_format($cost, 2, ".", "")."</td>";
                                                                            if(($invoiced - $cost) < 0)
                                                                                $fontColor = "style='color:#ff0000'";
                                                                            else
                                                                                $fontColor = "";

            $tableString .=                                         "<td ".$fontColor ." class='rightdata'>".number_format(($invoiced - $cost), 2, ".", "")."</td></tr>";
                }
            }
            else if ($nEmployees == 1)
            {
                $invoiced                                               = q("SELECT SUM(ts.rate * ts.time_spent) FROM ($table AS ts INNER JOIN companiesOnTeam AS cot ".
                                                                            "ON ts.project_id = cot.project_id) WHERE cot.total_budget > 0 AND ts.user_id = '".$employees[0][0]."' ".
                                                                            "AND ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                                                            "AND cot.company_id = '".$_SESSION["company_id"]."' $where");
                $cost                                                   = q("SELECT (SUM(ts.time_spent) * e.cost) FROM ($table AS ts INNER JOIN Employee AS e ".
                                                                            "ON ts.user_id = e.id INNER JOIN Project AS p ON p.id = ts.project_id) ".
                                                                            "WHERE ts.user_id = '".$employees[0][0]."' AND ts.date >= '$date_from' ".
                                                                            "AND ts.date <= '$date_to' AND p.company_id = '".$_SESSION["company_id"]."' $where");

                $tableString                                            .= "<tr>".
                                                                            "<td>".$employees[0][1].", ".
                                                                            $employees[0][2]."</td>".
                                                                            "<td class='rightdata'>".number_format($invoiced, 2, ".", "")."</td>".
                                                                            "<td class='rightdata'>".number_format($cost, 2, ".", "")."</td>";
                                                                            if(($invoiced - $cost) < 0)
                                                                                $fontColor = "style='color:#ff0000'";
                                                                            else
                                                                                $fontColor = "";

            $tableString .=                                         "<td ".$fontColor ." class='rightdata'>".number_format(($invoiced - $cost), 2, ".", "")."</td></tr>";
            }
        }
        else
        {
            $invoiced                                                   = q("SELECT SUM(ts.rate * ts.time_spent) FROM ($table AS ts INNER JOIN companiesOnTeam AS cot ".
                                                                            "ON ts.project_id = cot.project_id) WHERE cot.total_budget > 0 AND ts.user_id = '".$employee_id."' ".
                                                                            "AND ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                                                            "AND cot.company_id = '".$_SESSION["company_id"]."' $where");
            $cost                                                       = q("SELECT (SUM(ts.time_spent) * e.cost) FROM ($table AS ts INNER JOIN Employee AS e ".
                                                                            "ON ts.user_id = e.id INNER JOIN Project AS p ON p.id = ts.project_id) ".
                                                                            "WHERE ts.user_id = '".$employee_id."' AND ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                                                            "AND p.company_id = '".$_SESSION["company_id"]."' $where");

            $tableString                                                .= "<tr>".
                                                                            "<td>".$employee_info[0][0].", ".
                                                                            $employee_info[0][1]."</td>".
                                                                            "<td class='rightdata'>".number_format($invoiced, 2, ".", "")."</td>".
                                                                            "<td class='rightdata'>".number_format($cost, 2, ".", "")."</td>";
                                                                    if(($invoiced - $cost) < 0)
                                                                        $fontColor = "style='color:#ff0000'";
                                                                    else
                                                                       $fontColor = "";

            $tableString .=                                         "<td ".$fontColor ." class='rightdata'>".number_format(($invoiced - $cost), 2, ".", "")."</td></tr>";
        }

        if ($tableString != "")
            $tableString                                                = $headers.$tableString;

        return $tableString;
    }

    //  Create Display Information Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")
    {
        $errorMessage                                                   = "";

        $reportOn = $_POST["reportOn"];

        $employee_id                                                    = $_SESSION["user_id"];
        $date_from                                                      = addslashes(strip_tags($_POST["date_from"]));
        $date_to                                                        = addslashes(strip_tags($_POST["date_to"]));
        $displayString                                                  = "";

        $table = ($reportOn == "approved") ? "ApprovedTime" : "TimeSheet";
        $where = ($reportOn == "approved") ? "AND ts.status = '2' " : "";

        $graph                                                          = createXMLString($date_from, $date_to, $employee_id,$table,$where);
        $displayString                                                  = renderChart("fusion/MSColumn3D.swf", "", $graph, "multigraph", 800, 400, false, false);


        if ($displayString != "")
        {
            $displayString                                              = "<tr>
                                                                                        <td class='on-table-clear' colspan='4'><a>Invoice Potential vs. Cost to Company</a></td>
                                                                                    </tr><tr>
                                                                                        <td class='on-table-clear' colspan='4'>".$date_from." to ".$date_to."</td>
                                                                                    </tr><td colspan='4'><br/>".$displayString."<br/></td></tr>".
                                                                                        createTable($date_from, $date_to, $employee_id,$table,$where);

            $generated                                                  = "1";
        }
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("5", "reports");

    if ($errorMessage != "")
    {
        echo "<p align='center' style='padding:0px;'><strong><font class='on-validate-error'>$errorMessage</font></strong></p>";
        echo "<br/>";
    }
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    jQuery(function(){
	jQuery('#date_from').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_from').val(),
		current: jQuery('#date_from').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_from').val()))
			_date = new Date();
			else _date = jQuery('#date_from').val();
			jQuery('#date_from').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_from').val(formated);
			jQuery('#date_from').DatePickerHide();
		}
	});
    })

    jQuery(function(){
	jQuery('#date_to').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_to').val(),
		current: jQuery('#date_to').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_to').val()))
			_date = new Date();
			else _date = jQuery('#date_to').val();
			jQuery('#date_to').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_to').val(formated);
			jQuery('#date_to').DatePickerHide();
		}
	});
    })

    function check()
    {
        var valid                                                       = 1;

        //  Check That Date From Is Entered
        if (document.forms["report_invoice_vs_cost"].date_from.value == "")
        {
            ShowLayer("dateFrom", "block");
            valid                                                       = 0;
        }
        //  Check That Entered Date From Is Valid
        else
        {
            ShowLayer("dateFrom", "none");

            if (!validation("date", document.forms["report_invoice_vs_cost"].date_from.value))
            {
                ShowLayer("dateFromDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("dateFromDiv", "none");
        }

        //  Check That Date To Is Entered
        if (document.forms["report_invoice_vs_cost"].date_to.value == "")
        {
            ShowLayer("dateTo", "block");
            valid                                                       = 0;
        }
        //  Check That Entered Date To Is Valid
        else
        {
            ShowLayer("dateTo", "none");

            if (!validation("date", document.forms["report_invoice_vs_cost"].date_to.value))
            {
                ShowLayer("dateToDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("dateToDiv", "none");
        }

        if (valid == 1)
        {
            document.forms["report_invoice_vs_cost"].save.value         = 1;
            document.forms["report_invoice_vs_cost"].submit();
        }
    }
</script>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="report_invoice_vs_cost">
                    <div id="" style="<?php if ($generated == "1") echo "display: none;"; else echo "display: block"; ?>">
                        <table width="100%">
                            <tr>
                                <td class="centerdata">
                                    <h6>Invoice Potential vs. Cost to Company Report</h6>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Report On:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="reportOn">
                                        <option value="normal">Actual Time</option>
                                        <option value="approved">Approved Time</option>
                                    </select>
                                </td>
                            </tr>
                            <tr><td colspan="100%"><br/></td></tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Date From:
                                </td>
                                <td width="50%">
                                    <input class="on-field-date" id="date_from" name="date_from" type="text" style="text-align:right;" value="<?php
                                        if ($_POST["date_from"] != "") echo "".$_POST["date_from"]; else echo "".getMonthStart($today); ?>">
                                    <div id="dateFrom" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                    <div id="dateFromDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Date To:
                                </td>
                                <td width="50%">
                                    <input class="on-field-date" id="date_to" name="date_to" type="text" style="text-align:right;" value="<?php
                                        if ($_POST["date_to"] != "") echo "".$_POST["date_to"]; else echo "".getMonthEnd($today); ?>">
                                    <div id="dateTo" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                    <div id="dateToDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <input name="btnGenerate" onClick="check();" type="button" value="Generate Report">
                        <input method="post" name="save" type="hidden" value="0" />
                    </div>
                </form>
                <div id="report" style="<?php if ($generated == "1") echo "display: block;"; else echo "display: none"; ?>">
                    <?php
                        echo "<table class='on-table-center on-table'>";
                            echo "".$displayString;
                            echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        echo "</table>";
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
