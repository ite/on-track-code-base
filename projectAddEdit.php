<?php
    //TODO:  check if record exists
    //TODO:  check if update necessary
    session_start();

    include("_db.php");
    include("graphics.php");
    include("_functions.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    if (!hasAccess("PROJ_MANAGE"))
        header("Location: noaccess.php");

    //Check if the user is a PM of this project or not OR if the user has a MANAGER ROLE
    $isManager = q("SELECT ur.id,r.id FROM user_role AS ur INNER JOIN roles AS r ON ur.roleid = r.id WHERE r.role = 'MAN' AND r.companyid = '".$_SESSION["company_id"]."' AND ur.userid = '".$_SESSION['user_id']."' ".
                        "AND ur.companyid = '".$_SESSION['company_id']."'");
    $isManager = ($isManager[0][0] != 0) ? 1 : 0;

    if ($_SESSION["email"] != "admin" && !$isManager)   {
        $check = q("SELECT manager FROM Project_User WHERE user_id = '".$_SESSION["user_id"]."' AND project_id = '".$_POST["id"]."' AND company_id = '".$_SESSION["company_id"]."'");

        $projectInfo = q("SELECT p.p_type, cot.teamManage FROM (Project AS p INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) WHERE p.id = '".$_POST["id"]."' AND cot.company_id = '".$_SESSION["company_id"]."'");

        if ($projectInfo[0][0] == "SP" && $projectInfo[0][1] == 2)      $check = 1;

        if ($check != 1 && $_POST["id"] != "")        header("Location: noaccess.php");
    }

    function getProjectTeam($pID) {
        unset($team);

        $r = 0;

        if ($pID != "")  {
            /*
            $data = q("SELECT pu.id,pu.project_id,pu.company_id,pu.user_id,CONCAT(e.lstname, ', ', e.frstname, ' [', e.email, ']') AS user,pu.manager,".
                        "pu.user_budget,pu.user_tariff FROM (Project_User AS pu INNER JOIN Employee AS e ON e.id = pu.user_id) ".
                        "WHERE pu.company_id = '".$_SESSION["company_id"]."' AND pu.project_id = '".$pID."' AND pu.deleted = '0' ".
                        "ORDER BY e.lstname, e.frstname, e.email");
            */
            $data = q("SELECT pu.id,pu.project_id,pu.company_id,pu.user_id,CONCAT(e.lstname, ', ', e.frstname, ' [', e.email, ']') AS user,pu.manager,".
                        "pu.user_budget,pu.rateID FROM (Project_User AS pu INNER JOIN Employee AS e ON e.id = pu.user_id) ".
                        "WHERE pu.company_id = '".$_SESSION["company_id"]."' AND pu.project_id = '".$pID."' AND pu.deleted = '0' ".
                        "ORDER BY e.lstname, e.frstname, e.email");
        }
        else    {
            $memberTblCntr = (isset($_POST["memberTblCntr"])) ? addslashes(strip_tags($_POST["memberTblCntr"])) : 0;

            if ($memberTblCntr >= 1)  {
                for ($j = 0; $j < $memberTblCntr; $j++)  {
                    unset($members);

                    $members[] = (isset($_POST["hiddenUserID_".$j])) ? addslashes(strip_tags($_POST["hiddenUserID_".$j])) : "";
                    $members[] = (isset($_POST["pID_".$j])) ? addslashes(strip_tags($_POST["pID_".$j])) : "";
                    $members[] = (isset($_POST["company_id_".$j])) ? addslashes(strip_tags($_POST["company_id_".$j])) : "";
                    $members[] = (isset($_POST["userID_".$j])) ? addslashes(strip_tags($_POST["userID_".$j])) : "";
                    $members[] = (isset($_POST["userID_".$j]) && is_numeric($_POST["userID_".$j])) ? q("SELECT CONCAT(lstname, ', ', frstname, ' [', email, ']') FROM Employee WHERE id = '".addslashes(strip_tags($_POST["userID_".$j]))."'") : "";
                    $members[] = (isset($_POST["manager_".$j])) ? addslashes(strip_tags($_POST["manager_".$j])) : "";
                    $members[] = (isset($_POST["budget_".$j]) && is_numeric($_POST["budget_".$j])) ? number_format(addslashes(strip_tags($_POST["budget_".$j])), 2, ".", "") : "";
                    $members[] = (isset($_POST["rate_".$j])) ? addslashes(strip_tags($_POST["rate_".$j])) : "";

                    $data[] = $members;
                }
            }
        }

        if (is_array($data))
            $team = $data;

        return $team;
    }

    function getProjectInfo($id) {
        unset($info);

        if ($id != "")  {
            $data = q("SELECT p.id,p.company_id,p.status,p.name,p.number,p.descr,p.prefix,p.reg_date,p.submitted_by_id,p.type_id,c.total_budget,c.consulting,c.expenses,c.diverse_income,c.vat,".
                            "c.buffer,p.due_date,p.travelRate,p.completed,p.p_type,c.teamManage ".
                        "FROM (Project AS p INNER JOIN companiesOnTeam AS c ON p.id = c.project_id) WHERE p.id = '$id' AND c.company_id = '".$_SESSION["company_id"]."'");

            if (is_array($data))
                $info = $data[0];
        }
        else    {
            global $today;

            $info[] = (isset($_POST["id"])) ? addslashes(strip_tags($_POST["id"])) : "";
            $info[] = addslashes(strip_tags($_SESSION["company_id"]));
            $info[] = (isset($_POST["status"])) ? addslashes(strip_tags($_POST["status"])) : "proposal";
            $info[] = (isset($_POST["name"])) ? addslashes(strip_tags($_POST["name"])) : "";
            $info[] = (isset($_POST["number"])) ? addslashes(strip_tags($_POST["number"])) : "";
            $info[] = (isset($_POST["descr"])) ? addslashes(strip_tags($_POST["descr"])) : "";
            $info[] = (isset($_POST["prefix"])) ? addslashes(strip_tags($_POST["prefix"])) : "";
            $info[] = (isset($_POST["reg_date"])) ? $_POST["reg_date"] : $today;
            $info[] = (isset($_POST["submitted_by_id"])) ? $_POST["submitted_by_id"] : "";
            $info[] = (isset($_POST["type_id"]) && $_POST["type_id"] != "null") ? $_POST["type_id"] : "0";
            $info[] = (isset($_POST["total_budget"]) && is_numeric($_POST["total_budget"])) ? number_format(addslashes(strip_tags($_POST["total_budget"])), 2, ".", "") : "";
            $info[] = (isset($_POST["consulting"]) && is_numeric($_POST["consulting"])) ? number_format(addslashes(strip_tags($_POST["consulting"])), 2, ".", "") : "";
            $info[] = (isset($_POST["expenses"]) && is_numeric($_POST["expenses"])) ? number_format(addslashes(strip_tags($_POST["expenses"])), 2, ".", "") : "";
            $info[] = (isset($_POST["diverse_income"]) && is_numeric($_POST["diverse_income"])) ? number_format(addslashes(strip_tags($_POST["diverse_income"])), 2, ".", "") : "";
            $info[] = (isset($_POST["vat"])) ? addslashes(strip_tags($_POST["vat"])) : addslashes(strip_tags($_SESSION["VAT"]));
            $info[] = (isset($_POST["buffer"])) ? addslashes(strip_tags($_POST["buffer"])) : "0";
            $info[] = (isset($_POST["due_date"])) ? addslashes(strip_tags($_POST["due_date"])) : "";
            $info[] = (isset($_POST["travelRate"])) ? $_POST["travelRate"] : "";
            $info[] = (isset($_POST["completed"])) ? $_POST["completed"] : "0";
            $info[] = (isset($_POST["p_type"])) ? $_POST["p_type"] : "CP";
            $info[] = (isset($_POST["teamManage"])) ? $_POST["teamManage"] : "2";
        }

        return $info;
    }

    if (isset($_POST["save"]) && $_POST["save"] === "1")    {
        $errorMessage = "";

        $fields = array("id","company_id","status","name","number","descr","prefix","reg_date","submitted_by_id","type_id","total_budget","consulting","expenses","diverse_income","vat","buffer","due_date","travelRate","completed","p_type","teamManage");
        $projectInfo = getProjectInfo("");
        $date = date("Y-m-d H:i:s");
        $user = $_SESSION["email"];

        if ($projectInfo[0] == "")  {                       //  DO INSERT
            $query = "INSERT INTO Project (";
            $values = "('";

            for ($i = 1; $i < count($fields); $i++)    {
                if ($i < 10 || $i > 15 && $i != 20)   {
                    $query .= $fields[$i].",";
                    $values .= $projectInfo[$i]."','";
                }
            }

            $query .= "dateCreated,createdBy,dateUpdated,updatedBy) VALUES ";
            $values .= $date."','".$user."','".$date."','".$user."')";

            $query .= $values;

            $insert = q($query);

            //COT
            $pid = q("SELECT id FROM Project WHERE company_id = '".$projectInfo[1]."' AND name = '".$projectInfo[3]."'");
            $query = "INSERT INTO companiesOnTeam (";
            $values = "('";

            for ($i = 1; $i < count($fields); $i++)    {
                if (!($i < 10 || $i > 15 && $i != 20))   {
                    $query .= $fields[$i].",";
                    $values .= $projectInfo[$i]."','";
                }
            }

            $query .= "company_id,project_id,status,dateCreated,createdBy,dateUpdated,updatedBy) VALUES ";
            $values .= $projectInfo[1]."','$pid','1','".$date."','".$user."','".$date."','".$user."')";

            $query .= $values;

            $insert = q($query);

            if ($insert)   {
                $logs = q("INSERT INTO Logs (what,access,on_table,by_user,on_date,on_time,company_id) VALUES ".
                            "('".$projectInfo[3]." created','Insert','Project','".$user."','".date("Y-m-d")."','".date("H:i:s")."','".$projectInfo[1]."')");
            }
        }
        else if (is_numeric($projectInfo[0]))    {          //  DO UPDATE
            $query = "UPDATE Project SET ";

            for ($i = 1; $i < count($fields); $i++)    {
                if ($i < 10 || $i > 15 && $i != 20) 	{
                    $query .= $fields[$i]." = '".$projectInfo[$i]."',";
                }
            }

            $query .= "dateUpdated = '".$date."',updatedBy = '".$user."' WHERE id = '".$projectInfo[0]."'";

            $update = q($query);

            //COT
            $query = "UPDATE companiesOnTeam SET ";

            for ($i = 1; $i < count($fields); $i++)    {
                if (!($i < 10 || $i > 15 && $i != 20))   {
                    $query .= $fields[$i]." = '".$projectInfo[$i]."',";
                }
            }

            $oldTotalBudget = q("SELECT total_budget FROM companiesOnTeam WHERE project_id = '".$projectInfo[0]."' AND company_id = '".$_SESSION["company_id"]."'");
            $overBudget = q("SELECT overBudget FROM companiesOnTeam WHERE project_id = '".$projectInfo[0]."' AND company_id = '".$_SESSION["company_id"]."'");

            if ($overBudget != "0")     {
                if (number_format($oldTotalBudget, 2, ".", "") != number_format($projectInfo[7], 2, ".", ""))
                    $overBudget = 0;
            }

            $query .= "overBudget = '".$overBudget."',dateUpdated = '".$date."',updatedBy = '".$user."' WHERE project_id = '".$projectInfo[0]."' AND company_id = '".$_SESSION["company_id"]."'";

            $update = q($query);

            if ($update)   {
                $logs = q("INSERT INTO Logs (what,access,on_table,by_user,on_date,on_time,company_id) VALUES ".
                            "('".$projectInfo[3]." updated','Update','Project','".$user."','".date("Y-m-d")."','".date("H:i:s")."','".$projectInfo[1]."')");
            }
        }

        if (is_numeric($projectInfo[0]) && $projectInfo[0] != "")
            $pID = $projectInfo[0];
        else
            $pID = q("SELECT id FROM Project WHERE company_id = '".$projectInfo[1]."' AND name = '".$projectInfo[3]."'");

        $projectTeam = getProjectTeam("");

        if (is_array($projectTeam)) {
            foreach ($projectTeam as $u)   {
                if ($u[0] == "")  {                 //  DO INSERT
                    $action = "Insert";
                    $msg = $u[3]." added to project team for ".$projectInfo[3];

                    $manager = ($u[5] == "") ? 0 : 1;

                    //$query = "INSERT INTO Project_User (project_id,company_id,user_id,manager,user_budget,user_tariff,dateCreated,createdBy,dateUpdated,updatedBy) VALUES ";
                    $query = "INSERT INTO Project_User (project_id,company_id,user_id,manager,user_budget,rateID,dateCreated,createdBy,dateUpdated,updatedBy) VALUES ";
                    $values = "('".$pID."','".$u[2]."','".$u[3]."','".$manager."','".$u[6]."','".$u[7]."','".$date."','".$user."','".$date."','".$user."')";

                    $query .= $values;
                }
                else if (is_numeric($u[0]))    {    //  DO UPDATE
                    $action = "Update";
                    $msg = $u[3]." details updated ".$projectInfo[3];

                    $manager = ($u[5] == "") ? 0 : 1;

                    //$query = "UPDATE Project_User SET project_id = '".$pID."',company_id = '".$u[2]."',user_id = '".$u[3]."',manager = '".$manager."',".
                    //            "user_budget = '".$u[6]."',user_tariff = '".$u[7]."',dateUpdated = '".$date."',updatedBy = '".$user."' WHERE id  = '".$u[0]."'";
                    $query = "UPDATE Project_User SET project_id = '".$pID."',company_id = '".$u[2]."',user_id = '".$u[3]."',manager = '".$manager."',".
                                "user_budget = '".$u[6]."',rateID = '".$u[7]."',dateUpdated = '".$date."',updatedBy = '".$user."' WHERE id  = '".$u[0]."'";
                }

                $runQuery = q($query);

                if ($runQuery)   {
                    $logs = q("INSERT INTO Logs (what,access,on_table,by_user,on_date,on_time,company_id) VALUES ".
                                "('".$msg." updated','".$action."','Project_User','".$user."','".date("Y-m-d")."','".date("H:i:s")."','".$projectInfo[1]."')");
                }
            }
        }

        header("Location: projects.php");
    }
    else
        $projectInfo = getProjectInfo($_POST["id"]);

    $index = 1;
    $currency = "<i>(".$_SESSION["currency"].")</i>";
    $isSP = ($projectInfo[19] == "SP") ? 1 : 0;

    if ($errorMessage != "")    {
        if ($errorMessage == "Please note that we are currently busy with data management, no entries are allowed for 2010-08-26. Please check back in 24 Hours.")
            echo "<p class='centerdata' style='padding:0px;'><strong style='font-size:12px;'><font class='on-validate-error'>$errorMessage</font></strong></p>";
        else
            echo "<p class='centerdata' style='padding:0px;'><strong><font class='on-validate-error'>$errorMessage</font></strong></p>";
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus();
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<link rel="stylesheet" type="text/css" href="include/jquery.autocomplete.css" />
<script type='text/javascript' src='include/jquery.autocomplete.js'></script>
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
	function is_array(input){
		return typeof(input) == "object" && (input instanceof Array);
	}

	var hiddenField = "";
	var companyvalue;
	var usersList;

	jQuery(function()    {
	<?php
		if (!$isSP)   {
	?>
		//  Calendar(s)
		jQuery("#reg_date").DatePicker( {
			format:"Y-m-d",
			date: jQuery("#reg_date").val(),
			current: jQuery("#reg_date").val(),
			starts: 1,
			position: "right",
			onBeforeShow: function()    {
				var _date = (!validation("date", jQuery("#reg_date").val())) ? new Date() : jQuery("#reg_date").val();
				jQuery("#reg_date").DatePickerSetDate(_date, true);
			},
			onChange: function(formated, dates) {
				jQuery("#reg_date").val(formated);
				jQuery("#reg_date").DatePickerHide();
			}
		});

		jQuery("#due_date").DatePicker( {
			format:"Y-m-d",
			date: jQuery("#due_date").val(),
			current: jQuery("#due_date").val(),
			starts: 1,
			position: "right",
			onBeforeShow: function()    {
				var _date = (!validation("date", jQuery("#due_date").val())) ? new Date() : jQuery("#due_date").val();
				jQuery("#due_date").DatePickerSetDate(_date, true);
			},
			onChange: function(formated, dates) {
				jQuery("#due_date").val(formated);
				jQuery("#due_date").DatePickerHide();
			}
		});
	<?php
		}
	?>

		//////////////////////////////
		//  Autocomplete Functions
		function findValue(li)  {
			if (li == null) return alert("No match!");

			var sValue = li.extra[0];
			var tValue = li.selectValue;
		}

		function selectItem(li) {
			findValue(li);
		}

		function setVariables(field, fieldValue)    {
			companyValue = fieldValue;
			usersList = jQuery("#hiddenUsers").val();

			jQuery.post("_ajax.php", {func: "setSessionVariable", sessionName: "companyValue#usersList", sessionValue: companyValue+"#"+usersList}, function(data){});
		}

		jQuery(".autocomplete").focus(function() {
			setVariables(jQuery(this).attr("id"), jQuery(this).attr("name"));
		});

		<?php
			jQAutoComplete("#", "userName", "get_users");
		?>
		//////////////////////////////

		//////////////////////////////
		//  Team Creation
		jQuery(".addUserBtn").click(function()    {
			var inputField = "#"+jQuery(this).attr("name");
			var inputValue = jQuery(inputField).val();
			var tbl = "#"+jQuery(this).attr("id");

			var alreadyInList = false;

			jQuery("table"+tbl+" tbody>tr").not(":first").not(":last").each(function(i,v){
				if (jQuery(this).children().eq(0).children().eq(4).text() == inputValue)	alreadyInList = true;
			});

			if (inputValue != "" && !alreadyInList)   {
				var query = "SELECT id FROM Employee WHERE CONCAT(lstname, ', ', frstname, ' [', email, ']') = '" + addslashes(inputValue) + "'";

				jQuery.post("_ajax.php", {func: "query_direct", query: query}, function(data)	{
					data = json_decode(data);

					var check = data;

					if (check != 0 && validation("number", check))   {
						var field;

						var hiddenTbl = "#hidden_memberTbl";
						var tblCntr = tbl+"Cntr";
						var tblDiv = tbl.replace("Tbl", "Div");
						var tblLstRow = tbl+"LstRow";

						var company_id = jQuery(inputField).attr("name");

						var counter = jQuery(tblCntr).val();
						var lstRow = jQuery(hiddenTbl + " tbody>tr:last");

						var columns = 5;
						var n = lstRow.clone(true);

						for (var i = 0; i < columns; i++)   {
							n.children().eq(i).children().each(function(j, v) {
								if (n.children().eq(i).children().eq(j).attr("id") != "")   {
									field = n.children().eq(i).children().eq(j).attr("id");

									field = field + "_" + counter;

									n.children().eq(i).children().eq(j).attr("id", field);
									n.children().eq(i).children().eq(j).attr("name", field)
								}
							});
						}

						n.insertBefore(jQuery(tblLstRow));

						var pID = jQuery("#id").val();
						var hiddenValue = jQuery("#hiddenUsers").val();

						n.children().eq(0).children().eq(0).attr("value", "");
						n.children().eq(0).children().eq(1).attr("value", pID);
						n.children().eq(0).children().eq(2).attr("value", company_id);
						n.children().eq(0).children().eq(3).attr("value", data);

						n.children().eq(0).children().eq(4).text(inputValue);

						hiddenValue = hiddenValue + data + "_";

						jQuery("#hiddenUsers").val(hiddenValue);

						jQuery.post("_ajax.php", {func: "get_user_rates", userID: data, companyID: company_id}, function(data)	{
							data = json_decode(data);

							field = "#"+n.children().eq(3).children().eq(0).attr("id");

							jQuery.each(data,function(i, v)	{
								jQuery(field).append(jQuery("<option></option>").attr("value",v[0]).text(v[1]));
							});
						});

						jQuery.post("_ajax.php", {func: "check_pm_status", userID: data, companyID: company_id}, function(data)	{
							data = json_decode(data);

							field = "#"+n.children().eq(1).children().eq(0).attr("id");

							if (data != 1)      jQuery(field).attr("disabled", "true");
						});

						setVariables("", company_id, "getUser");

						jQuery(inputField).val("").focus();

						counter++;

						if (counter > 0)    jQuery(tblDiv).show();

						jQuery(tblCntr).val(counter);

						jQuery(inputField)[0].autocompleter.flushCache();
					}
				});
			}

			jQuery(inputField).val("");
		});

		jQuery(".showUsers").click(function()    {
			var divName = "#show"+jQuery(this).attr("id");

			if (jQuery(this).children().eq(1).text() == "Hide User(s)")    {
				var imgSrc = "images/icons/expand.ico";
				var title = "Expand";
				var display = "Show User(s)";
			}
			else    {
				var imgSrc = "images/icons/collapse.ico";
				var title = "Collapse";
				var display = "Hide User(s)";
			}

			jQuery(this).children().eq(0).attr("src", imgSrc);
			jQuery(this).children().eq(0).attr("title", title);
			jQuery(this).children().eq(1).text(display);

			jQuery(divName).toggle();
		});

		jQuery(".deleteUserBtn").click(function()    {
			var hiddenValue = jQuery("#hiddenUsers").val();
			var value = jQuery(this).parent().parent().children().eq(0).children().eq(3).attr("value");

			hiddenValue = hiddenValue.replace(value+"_", "");

			jQuery("#hiddenUsers").val(hiddenValue);

			jQuery(this).parent().parent().remove();

			var query = "DELETE FROM Project_User WHERE id = '" + jQuery(this).attr("id") + "'";

			jQuery.post("_ajax.php", {func: "query_direct", query: query}, function(data)	{});
		});

		jQuery("#consulting, #expenses").change(function()    {
			var total_budget = jQuery("#total_budget").val();
			var consultingVal = jQuery("#consulting").val();
			var expensesVal = jQuery("#expenses").val();

			//  Consulting
			if (consultingVal != "" && expensesVal != "" && validation("currency", consultingVal) && validation("currency", expensesVal))  {
				total_budget = parseFloat(consultingVal) + parseFloat(expensesVal);

				jQuery("#total_budget").val(total_budget);
			}
		});

		jQuery("input[id='status']").change(function()   {
			if (jQuery(this).val() == "proposal")
				jQuery("#viewActive").hide();
			else
				jQuery("#viewActive").show();
		});
		//////////////////////////////

		//////////////////////////////
		//  Add/Update Button Click
		jQuery("#btnAddEdit").click(function()    {
			var valid = true;
			var test;

			//  Check That Project Does Not Exist
			var id = jQuery("#id").val();
			var name = jQuery("#name").val();
			var company_id = jQuery("#company_id").val();

			var query = (id == "") ? "SELECT * FROM Project WHERE name = '"+addslashes(name)+"' AND company_id = '"+company_id+"'"
								: "SELECT * FROM Project WHERE name = '"+addslashes(name)+"' AND company_id = '"+company_id+"' AND id != '"+id+"'";

			jQuery.post("_ajax.php", {func: "query_direct", query: query}, function(data)	{
				data = json_decode(data);

				test = !is_array(data);

				if (!test)  jQuery("#nameDuplicate").show();
				else        jQuery("#nameDuplicate").hide();

				valid &= test;

				var fields = new Array("name","reg_date","total_budget","consulting","expenses","diverse_income","vat","buffer","due_date");

				for (var i = 0; i < fields.length; i++)  {
					var field = fields[i];
					var cls = jQuery("#"+field).attr("class");

					cls = cls.replace(/ project/gi, "");
					cls = (cls != "") ? cls.split(" ") : "";

					if (cls != "")  {
						for (var j = 0; j < cls.length; j++)  {
							var divName = (cls[j] == "entered") ? "#"+field+"Empty" : "#"+field+"Div";
							test = validation(cls[j], jQuery("#"+field).val());

							if (!test)  jQuery(divName).show();
							else        jQuery(divName).hide();

							valid &= test;
						}
					}
				}

				//  Check Budgets
				test = checkBudget();

				if (!test)  jQuery("#total_budget_Over").show();
				else        jQuery("#total_budget_Over").hide();

				valid &= test;

				//  Check That User Budgets Not More Than Consulting Budget
				var users = jQuery("#memberTblCntr").val();

				if (jQuery("#teamManage").val() == "2" && users >= 1)  {
					var consultingBudget = parseFloat(jQuery("#consulting").val());
					var userBudgets = 0;

					for (var k = 0; k < users; k++) {
						var userBudget = jQuery("#budget_"+k).val();

						if (userBudget != "" && validation("currency", userBudget)) userBudgets += parseFloat(userBudget);
					}

					if (consultingBudget != "" && validation("currency", consultingBudget) && userBudgets != "" && validation("currency", userBudgets))     {
						test = (consultingBudget >= userBudgets);

						if (!test)  jQuery("#consultingOver").show();
						else        jQuery("#consultingOver").hide();

						valid &= test;
					}
				}

				var users = jQuery("#memberTblCntr").val();

				//  Check That At Least 1 Member Has Been Added To Team
				var tmError = true;

				tmError &= (users >= 1);

				if (!tmError)   jQuery("#teamDiv").show();
				else            jQuery("#teamDiv").hide();

				valid &= tmError;

				//  Check That At Least 1 Project Manager Selected
				var pmError = true;

				if (jQuery("#teamManage").val() == "2" && users >= 1)  {
					var pmCount = 0;

					for (var k = 0; k < users; k++) {
						if (jQuery("#manager"+"_"+k).attr("checked"))  pmCount++;
					}

					if (!(pmCount >= 1))    {
						jQuery("#showUsers").show();

						var imgSrc = "images/icons/collapse.ico";
						var title = "Collapse";
						var display = "Hide User(s)";
					}
					else    {
						jQuery("#showUsers").hide();

						var imgSrc = "images/icons/expand.ico";
						var title = "Expand";
						var display = "Show User(s)";
					}

					jQuery("#showUsers").parent().children().eq(3).children().eq(0).attr("src", imgSrc);
					jQuery("#showUsers").parent().children().eq(3).children().eq(0).attr("title", title);
					jQuery("#showUsers").parent().children().eq(3).children().eq(1).text(display);

					pmError &= (pmCount >= 1);
				}

				if (!pmError)   jQuery("#teamPMDiv").show();
				else            jQuery("#teamPMDiv").hide();

				valid &= pmError;

				if (valid) {
					jQuery("#save").val("1");
					jQuery("#projectAddEdit").submit();
				}
			});
		});
		//////////////////////////////
	});

	function checkBudget()  {
		var budget = eval("document.forms['projectAddEdit'].total_budget.value;");
		var consulting = eval("document.forms['projectAddEdit'].consulting.value;");
		var expenses = eval("document.forms['projectAddEdit'].expenses.value;");
		var total = parseFloat(0);

		if (budget != "" && validation("currency", budget))  {
			budget = parseFloat(budget);

			if (consulting != "" && validation("currency", consulting)) total += parseFloat(consulting);
			if (expenses != "" && validation("currency", expenses))     total += parseFloat(expenses);

			return (total <= budget) ? true : false;
		}

		return true;
	}
</script>
<?php
    $employees = q("SELECT e.id, e.lstname, e.frstname FROM (Employee AS e INNER JOIN Company_Users AS cu ON e.id = cu.user_id) ".
                    "WHERE cu.company_id = '".$_SESSION["company_id"]."' AND e.email != 'admin' AND e.deleted = '0' ORDER BY e.lstname, frstname");
    $projectTypes = q("SELECT id, type, active FROM ProjectTypes WHERE company_id = '".$_SESSION["company_id"]."' AND active = '1' OR id = '".$projectInfo[6]."' ORDER BY type");
?>
<table width="100%">
    <tr height="380px">
        <td class="centerdata" valign="top">
            <form id="projectAddEdit" name="projectAddEdit" action="" method="post">
                <table width="100%">
                    <tr>
                        <td class="centerdata">
                            <h6>
                                <?php echo ($projectInfo[0] == "") ? "Add" : "Update"; ?> Project
                            </h6>
                        </td>
                    </tr>
                </table>
                <br/>
                <table cellpadding="0" cellspacing="2" width="100%">
                    <input id="id" name="id" type="hidden" value="<?php echo stripslashes($projectInfo[0]); ?>">
                    <input id="company_id" name="company_id" type="hidden" value="<?php echo stripslashes($projectInfo[1]); ?>">
                    <tr>
                        <td class="on-description" width="50%">
                            Proposal State
                            <input id="status" name="status" type="radio" tabindex="<?php echo $index; ?>" value="proposal" <?php
                                echo (stripslashes($projectInfo[2]) == "proposal") ? "checked" : ""; ?> <?php echo ($isSP) ? "disabled" : ""; ?>>
                        </td>
                        <td class="on-description-left" width="50%">
                            <input id="status" name="status" type="radio" tabindex="<?php echo $index; ?>" value="active" <?php
                                echo (stripslashes($projectInfo[2]) == "active") ? "checked" : ""; ?>>
                            Active State
                        </td>
                    </tr>
                    <tr>
                        <td class="on-description" width="50%">
                            Project Name:
                        </td>
                        <td width="50%">
                            <input id="name" name="name" type="text" class="on-field required project" tabindex="<?php echo $index++; ?>" size="50" value="<?php echo stripslashes($projectInfo[3]); ?>" <?php echo ($isSP) ? "readonly" : ""; ?>>
                            <div id="nameDiv" name="nameDiv" class="error"><font class="on-description-left">* Project name must be entered</font></div>
                            <div id="nameDuplicate" name="nameDuplicate" class="error"><font class="on-description-left">* Project name already in use</font></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="on-description" width="50%">
                            Project Number:
                        </td>
                        <td width="50%">
                            <input id="number" name="number" type="text" class="on-field" tabindex="<?php echo $index++; ?>" size="50" value="<?php echo stripslashes($projectInfo[4]); ?>" <?php echo ($isSP) ? "readonly" : ""; ?>>
                        </td>
                    </tr>
                    <tr>
                        <td class="on-description" width="50%">
                            Project Descr:
                        </td>
                        <td width="50%">
                            <input id="descr" name="descr" type="text" class="on-field" tabindex="<?php echo $index++; ?>" size="50" value="<?php echo stripslashes($projectInfo[5]); ?>" <?php echo ($isSP) ? "readonly" : ""; ?>>
                        </td>
                    </tr>
                    <tr>
                        <td class="on-description" width="50%">
                            Project Invoice Prefix:
                        </td>
                        <td width="50%">
                            <input id="prefix" name="prefix" type="text" class="on-field" tabindex="<?php echo $index++; ?>" size="50" value="<?php echo stripslashes($projectInfo[6]); ?>" <?php echo ($isSP) ? "readonly" : ""; ?>>
                        </td>
                    </tr>
                    <tr>
                        <td class="on-description" width="50%">
                            Registration Date:
                        </td>
                        <td width="50%">
                            <input id="reg_date" name="reg_date" type="text" class="on-field-date entered date" tabindex="<?php echo $index++; ?>" value="<?php echo $projectInfo[7]; ?>" <?php echo ($isSP) ? "readonly" : ""; ?>>
                            <div id="reg_dateEmpty" name="reg_dateEmpty" class="error"><font class="on-description-left">* Registration date must be entered</font></div>
                            <div id="reg_dateDiv" name="reg_dateDiv" class="error"><font class="on-description-left">* Registration date not valid, eg.<?php echo "".date("Y-m-d"); ?></font></div>
                        </td>
                    </tr>
                    <?php
//                        if (!$isSP)   {
                        if (1 == 0)   {
                    ?>
                    <tr>
                        <td class="on-description" width="50%">
                            Submitted By:
                        </td>
                        <td width="50%">
                            <select class="on-field" id="submitted_by_id" name="submitted_by_id" tabindex="<?php echo $index++; ?>">
                                <option value="null">--  Select Employee --</option>
                                <?php
                                    if (is_array($employees))    {
                                        foreach ($employees as $employee)
                                            if ($projectInfo[8] == $employee[0])
                                                echo "<option value='".$employee[0]."' selected>".$employee[1].", ".$employee[2]."</option>";
                                            else
                                                echo "<option value='".$employee[0]."'>".$employee[1].", ".$employee[2]."</option>";
                                    }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <?php
                        }
                        else
                            echo "<input id='submitted_by_id' name='submitted_by_id' type='hidden' value='0'>";
                    ?>
                    <tr>
                        <td class="on-description" width="50%">
                            Project Type:
                        </td>
                        <td width="50%">
                            <select class="on-field" id="type_id" name="type_id" tabindex="<?php echo $index++; ?>">
                                <option value="null" <?php echo ($isSP) ? "disabled" : ""; ?>>--  Select Project Type --</option>
                                <?php
                                    if ($isSP)   {
                                        echo "<option value='0' selected>Shared Project</option>";
                                    }
                                    else if (is_array($projectTypes)) {
                                        foreach ($projectTypes as $projectType) {
                                            $cls = ($projectType[2] == "1") ? "" : "style='color:#AA0000;'";

                                            if ($projectInfo[9] == $projectType[0])
                                                echo "<option value='".$projectType[0]."' $cls selected>".$projectType[1]."</option>";
                                            else
                                                echo "<option value='".$projectType[0]."' $cls>".$projectType[1]."</option>";
                                        }
                                    }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="on-description" width="50%">
                            Total Project Budget <?php echo $currency; ?>:
                        </td>
                        <td width="50%">
                            <input id="total_budget" name="total_budget" type="text" class="on-field currency" tabindex="<?php echo $index++; ?>" value="<?php echo stripslashes($projectInfo[10]); ?>" <?php echo ($isSP) ? "readonly" : ""; ?>>
                            <font class="on-description-left">* excl</font>
                            <div id="total_budgetDiv" name="total_budgetDiv" class="error"><font class="on-description-left">* Entered amount must be numeric, eg. 123.45</font></div>
                            <div id="total_budget_Over" name="total_budget_Over" class="error"><font class="on-description-left">* Consulting and expenses more than total budget</font></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="on-description" width="50%">
                            Consulting Budget <?php echo $currency; ?>:
                        </td>
                        <td width="50%">
                            <input id="consulting" name="consulting" type="text" class="on-field currency" tabindex="<?php echo $index++; ?>" value="<?php echo stripslashes($projectInfo[11]); ?>" <?php echo ($isSP && stripslashes($projectInfo[20]) == "1") ? "readonly" : ""; ?>>
                            <font class="on-description-left">* excl</font>
                            <div id="consultingDiv" name="consultingDiv" class="error"><font class="on-description-left">* Entered amount must be numeric, eg. 123.45</font></div>
                            <div id="consultingOver" name="consultingOver" class="error"><font class="on-description-left">* User budget more than consulting budget</font></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="on-description" width="50%">
                            Expense Budget <?php echo $currency; ?>:
                        </td>
                        <td width="50%">
                            <input id="expenses" name="expenses" type="text" class="on-field currency" tabindex="<?php echo $index++; ?>" value="<?php echo stripslashes($projectInfo[12]); ?>" <?php echo ($isSP && stripslashes($projectInfo[20]) == "1") ? "readonly" : ""; ?>>
                            <font class="on-description-left">* excl</font>
                            <div id="expensesDiv" name="expensesDiv" class="error"><font class="on-description-left">* Entered amount must be numeric, eg. 123.45</font></div>
                        </td>
                    </tr>
                </table>
                <div id="viewActive" name="viewActive" class="<?php echo ($projectInfo[2] == "proposal") ? "hidden" : ""; ?>">
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                Diverse Income Budget <?php echo $currency; ?>:
                            </td>
                            <td width="50%">
                                <input id="diverse_income" name="diverse_income" type="text" class="on-field currency" tabindex="<?php echo $index++; ?>" value="<?php echo stripslashes($projectInfo[13]); ?>">
                                <div id="diverse_incomeDiv" name="diverse_incomeDiv" class="error"><font class="on-description-left">* Entered amount must be numeric, eg. 123.45</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                VAT <i>(%)</i>:
                            </td>
                            <td width="50%">
                                <input id="vat" name="vat" type="text" class="on-field-date percentage" tabindex="<?php echo $index++; ?>" value="<?php echo stripslashes($projectInfo[14]); ?>" <?php echo ($isSP && stripslashes($projectInfo[20]) == "1") ? "readonly" : ""; ?>>
                                <div id="vatDiv" id="vatDiv" class="error"><font class="on-description-left">* Entered amount must be a percentage, eg. 100%</font></div>
                            </td>
                        </tr>
                        <?php
                            if (!$isSP)   {
                        ?>
                        <tr>
                            <td class="on-description" width="50%">
                                Buffer <i>(%)</i>:
                            </td>
                            <td width="50%">
                                <input id="buffer" name="buffer" type="text" class="on-field-date percentage" tabindex="<?php echo $index++; ?>" value="<?php echo stripslashes($projectInfo[15]); ?>">
                                <div id="bufferDiv" id="bufferDiv" class="error"><font class="on-description-left">* Entered amount must be a percentage, eg. 100%</font></div>
                            </td>
                        </tr>
                        <?php
                            }
                            else
                                echo "<input id='buffer' name='buffer' type='hidden' value='0'>";
                        ?>
                        <tr>
                            <td class="on-description" width="50%">
                                Due Date:
                            </td>
                            <td width="50%">
                                <input id="due_date" name="due_date" type="text" class="on-field-date date2" tabindex="<?php echo $index++; ?>" value="<?php echo stripslashes($projectInfo[16]); ?>" <?php echo ($isSP) ? "readonly" : ""; ?>>
                                <div id="due_dateDiv" name="due_dateDiv" class="error"><font class="on-description-left">* Due date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                            </td>
                        </tr>
                        <?php
                            if (!$isSP)   {
                        ?>
                        <tr>
                            <td class="on-description" width="25%">
                                Travelling Rate:
                            </td>
                            <td style="text-align:left;" width="50%">
                                <select class="on-field" id="travelRate" name="travelRate" tabindex="<?php echo $index++; ?>">
                                    <?php
                                        $travelRates = q("SELECT id, name, deflt FROM dropdowns WHERE categoryID = (SELECT id FROM categories WHERE code = 'travelRate') AND active = 1 ORDER BY ranking");

                                        if (is_array($travelRates))  {
                                            foreach ($travelRates as $v)   {
                                                $selected = (stripslashes($projectInfo[17]) == $v[0]) ? "selected" : (($v[2] === "1" && stripslashes($projectInfo[17]) == "0") ? "selected" : "");

                                                echo "<option value='".$v[0]."' $selected>".$v[1]."</option>";
                                            }
                                        }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <?php
                            }
                            else
                                echo "<input id='travelRate' name='travelRate' type='hidden' value='".stripslashes($projectInfo[17])."'>";
                        ?>
                        <?php
                            if (!$isSP)   {
                        ?>
                        <tr>
                            <td class="on-description" width="50%">
                                Completed:
                            </td>
                            <td class="on-description-left" width="50%">
                                &nbsp;&nbsp;<input id="completed" name="completed" type="checkbox" tabindex="<?php echo $index++; ?>" value="1" <?php echo ($projectInfo[18] == "1") ? "checked" : ""; ?>>
                            </td>
                        </tr>
                        <?php
                            }
                            else
                                echo "<input id='completed' name='completed' type='hidden' value='".stripslashes($projectInfo[18])."'>";
                        ?>
                    </table>
                </div>
                <br/>
                <?php
                    $projectTeam = getProjectTeam($projectInfo[0]);
                    $readonly = ($isSP && stripslashes($projectInfo[20]) == "1") ? "readonly" : "";
                ?>
                <table cellpadding="0" cellspacing="2" width="100%">
                    <tr>
                        <td class="centerdata" colspan="2">
                            <h6>Team Definition &amp; Budget Assignment</h6>
                            <br/><br/>
                        </td>
                    </tr>
                    <tr>
                        <td class="on-description" width="50%">
                            Please start typing user name:
                        </td>
                        <td width="50%">
                            <table>
                                <tr>
                                    <td>
                                        <input class="on-field autocomplete" id="userName" name="<?php echo $_SESSION["company_id"]; ?>" type="text" value="" <?php echo $readonly; ?> />
                                    <td>
                                    <td>
                                        <img style="margin-top:6px" id="memberTbl" name="userName" class="addUserBtn" src="images/icons/add.ico" title="Add" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="centerdata" colspan="2">
                            <br/>
                            <div id="teamDiv" name="teamDiv" class="error"><font class="on-description-left">* Please ensure that at least 1 member has been added to team</font></div>
                            <div id="teamPMDiv" name="teamPMDiv" class="error"><font class="on-description-left">* Please ensure that at least 1 project manager is selected</font></div>
                                <a id="Users" name="Users" class="showUsers"><img src="images/icons/collapse.ico" title="Collapse" /><i>Hide User(s)</i></a>
                                <div id="showUsers" name="showUsers">
                                    <div id="memberDiv" name="memberDiv" class="<?php echo (!is_array($projectTeam)) ? "hidden" : ""; ?>">
                                        <table class="on-table-center on-table" id="memberTbl" name="memberTbl">
                                            <tr>
                                                <th>User Details</th>
                                                <th>PM</th>
                                                <th>Budget</th>
                                                <th>Rate</th>
                                                <th></th>
                                            </tr>
                                            <?php
                                                $counter = 0;

                                                if (is_array($projectTeam))  {
                                                    $users = "";

                                                    foreach ($projectTeam as $u) {
                                                        $users .= stripslashes($u[3])."_";

                                                        echo "<tr>";
                                                            echo "<td>";
                                                                echo "<input id='hiddenUserID_".$counter."' name='hiddenUserID_".$counter."' type='hidden' value='".stripslashes($u[0])."' />";
                                                                echo "<input id='pID_".$counter."' name='pID_".$counter."' type='hidden' value='".stripslashes($u[1])."' />";
                                                                echo "<input id='company_id_".$counter."' name='company_id_".$counter."' type='hidden' value='".stripslashes($u[2])."' />";
                                                                echo "<input id='userID_".$counter."' name='userID_".$counter."' type='hidden' value='".stripslashes($u[3])."' />";
                                                                echo "<a>".stripslashes($u[4])."</a>";
                                                            echo "</td>";

                                                            $checked = (stripslashes($u[5])) ? "checked" : "";

                                                            echo "<td>";
                                                                if ($isSP && stripslashes($projectInfo[20]) == "1") {
                                                                    echo "<input id='manager_".$counter."' name='manager_".$counter."' type='hidden' value='".stripslashes($u[5])."' />";
                                                                    echo "<input id='manager_".$counter."Disp' name='manager_".$counter."Disp' type='checkbox' tabindex='".$index."' $checked value='1' disabled />";
                                                                }
                                                                else    {
                                                                    $checkPM = q("SELECT ur.id,r.id FROM (user_role AS ur INNER JOIN roles AS r ON ur.roleid = r.id)
                                                                                    WHERE r.role = 'PM' AND r.companyid = '".$_SESSION["company_id"]."' AND ur.userid = '".stripslashes($u[3])."' ".
                                                                                        "AND ur.companyid = '".stripslashes($u[2])."'");
                                                                    $disabled = ($checkPM[0][0] != 0) ? "" : "disabled";

                                                                    echo "<input id='manager_".$counter."' name='manager_".$counter."' type='checkbox' tabindex='".$index."' $checked $disabled value='1' />";
                                                                }
                                                            echo "</td>";
                                                            echo "<td><input id='budget_".$counter."' name='budget_".$counter."' type='text' class='currency' tabindex=".$index."' value='".stripslashes($u[6])."' $readonly /></td>";
                                                            echo "<td>";
                                                                echo "<select id='rate_".$counter."' name='rate_".$counter."' tabindex='".$index."'>";
                                                                    echo "<option value='0'>0.00</option>";
                                                                    //$rates = q("SELECT tariff1, tariff2, tariff3, tariff4, tariff5 FROM Employee WHERE id = '".$u[3]."'");
                                                                    $rates = q("SELECT id, rate FROM user_rates WHERE userid = '".$u[3]."' AND companyid = '".$u[2]."' AND active = 1");
                                                                    $rates = sortArrayByColumn($rates, 1);
                                                                    $data = $rates;

                                                                    /*
                                                                    if (is_array($rates))   {
                                                                        $r = 0;

                                                                        for ($i = 1; $i <= 5; $i++) {
                                                                            if ($rates[0][$i - 1] > 0) {
                                                                                $data[$r][0] = $i;
                                                                                $data[$r++][1] = $rates[0][$i - 1];
                                                                            }
                                                                        }
                                                                    }
                                                                    */

                                                                    if (is_array($data))  {
                                                                        foreach ($data as $d)   {
                                                                            $selected = (stripslashes($u[7]) == $d[0]) ? "selected" : (($isSP && stripslashes($projectInfo[20]) == "1") ? "disabled" : "");

                                                                            echo "<option value='".$d[0]."' $selected>".$d[1]."</option>";
                                                                        }
                                                                    }
                                                                echo "</select>";
                                                            echo "</td>";
                                                            echo "<td>";
                                                                if ($isSP && stripslashes($projectInfo[20]) == "1")
                                                                    echo "&nbsp;";
                                                                else
                                                                    echo "<img id='".stripslashes($u[0])."' name='".stripslashes($u[0])."' src='images/icons/delete.ico' class='deleteUserBtn' title='Delete' />";
                                                            echo "</td>";
                                                        echo "</tr>";

                                                        $counter++;
                                                    }
                                                }
                                            ?>
                                            <tr id="memberTblLstRow" name="memberTblLstRow"></tr>
                                            <tfoot><tr><td colspan='100%'></td></tr></tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
                <div class="hidden">
                    <table id="hidden_memberTbl" name="hidden_memberTbl">
                        <tr>
                            <td>
                                <input id="hiddenUserID" name="hiddenUserID" type="hidden" value="" />
                                <input id="spID" name="spID" type="hidden" value="" />
                                <input id="company_id" name="company_id" type="hidden" value="" />
                                <input id="userID" name="userID" type="hidden" value="" />
                                <a></a>
                            </td>
                            <td><input id="manager" name="manager" type="checkbox" tabindex="<?php echo $index; ?>" value="1" /></td>
                            <td><input id="budget" name="budget" type="text" class="currency" tabindex="<?php echo $index; ?>" value="" /></td>
                            <td><select id="rate" name="rate" tabindex="<?php echo $index; ?>"></select></td>
                            <td><img id="" name="" src="images/icons/delete.ico" class="deleteUserBtn" title="Delete" /></td>
                        </tr>
                    </table>
                </div>
                <br/>
                <input id="btnAddEdit" name="btnAddEdit" tabindex="<?php echo $index++; ?>" type="button" value="<?php echo ($projectInfo[0] == "") ? "Add" : "Update"; ?>">
                <input id="save" name="save" type="hidden" value="0" />
                <input id="memberTblCntr" name="memberTblCntr" type="hidden" value="<?php echo $counter; ?>" />
                <input id="hiddenUsers" name="hiddenUsers" type="hidden" value="<?php echo $users; ?>" />
                <input id="p_type" name="p_type" type="hidden" value="<?php echo $projectInfo[19]; ?>" />
                <input id="teamManage" name="teamManage" type="hidden" value="<?php echo $projectInfo[20]; ?>" />
            </form>
        </td>
    </tr>
    <tr>
        <td class="centerdata">
            <br/>
        </td>
    </tr>
</table>
<?php
    //  Print Footer
    print_footer();
?>
