<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("include/sajax.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    if (!hasAccess("REP_PROJ_INFO"))
        header("Location: noaccess.php");

    // Class for table background color
    $colorClass = "OnTrack_#_";

    ///////////////////////////
    //  Sajax
    function getProjects($status, $type) {
        if ($status == "all")   $status = " AND p.completed IN (0,1)";
        else                    $status = " AND p.completed = '".$status."'";

        if ($type == "null")
            $projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p
                            INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id)
                            WHERE cot.company_id = '".$_SESSION["company_id"]."'
                            AND p.deleted = '0' $status ORDER BY UPPER(p.name)");
        else
            $projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p
                            INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id)
                            WHERE cot.company_id = '".$_SESSION["company_id"]."'
                            AND p.deleted = '0'
                            AND p.type_id='$type' $status ORDER BY UPPER(p.name)");

        return $projects;
    }

    $sajax_request_type = "GET";
    sajax_init();
    sajax_export("getProjects");
    sajax_handle_client_request();
    ///////////////////////////

    //  Set Report Generated Status
    $generated = "0";

    //  Create Display Information Function
    if (isset($_POST["save"]) && $_POST["save"] === "1") {
        $errorMessage = "";

        $reportOn = $_POST["reportOn"];
        $projectStatus = $_POST["projectStatus"];

        $project_type_id = $_POST["projectType"];
        $project_id = $_POST["project"];
        $displayString = "";
        $where = "";

        if ($project_type_id != "null" && is_numeric($project_type_id))
            $where .= "AND p.type_id = '$project_type_id' ";

        if (is_numeric($project_id) && $project_id != "all")
            $where .= "AND p.id = '$project_id' ";

        if ($projectStatus == "all")    $projectStatus = " AND p.completed IN (0,1) ";
        else                            $projectStatus = " AND p.completed = '".$projectStatus."' ";

        $projects = q("SELECT DISTINCT(p.id), p.name, p.due_date, manager_id, cot.total_budget FROM (Project AS p
                        INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id)
                        WHERE cot.company_id = '".$_SESSION["company_id"]."'
                        $projectStatus
                        AND p.deleted = '0' $where ORDER BY UPPER(p.name)");

        $table = ($reportOn == "approved") ? "ApprovedTime" : "TimeSheet";
        $where = ($reportOn == "approved") ? "AND status = '2' " : "";

        $table2 = ($reportOn == "approved") ? "ApprovedExpense" : "ExpenseSheet";
        $where2 = ($reportOn == "approved") ? "AND status = '2' " : "";

        $row                                                              = 0;

        $excelheadings[$row][]                                     = "Project Overview Report"; // 1
        for($i=0; $i <15; $i++)
            $excelheadings[$row][]                                     = "";                          // 2-14

        $exceldata[$row][]                                     = $colorClass."Project Name";
        $exceldata[$row][]                                     = $colorClass."Due Date";
        $exceldata[$row][]                                     = $colorClass."Manager";
        $exceldata[$row][]                                     = $colorClass."Budget (".$_SESSION["currency"].")(excl. VAT)";
        $exceldata[$row][]                                     = $colorClass."Invoiced (".$_SESSION["currency"].")(excl. VAT)";
        $exceldata[$row][]                                     = $colorClass."WIP (".$_SESSION["currency"].")(excl. VAT)";
        $exceldata[$row][]                                     = $colorClass."Not Invoiced (".$_SESSION["currency"].")(excl. VAT)";
        $exceldata[$row][]                                     = $colorClass."Total Consulting Booked (".$_SESSION["currency"].")(excl. VAT)";
        $exceldata[$row][]                                     = $colorClass."Total Expenses Booked (".$_SESSION["currency"].")(excl. VAT)";
        // List of Invoices
        $exceldata[$row][]                                     = "";
        $exceldata[$row][]                                     = $colorClass."Invoice Name/Number";
        $exceldata[$row][]                                     = $colorClass."Date Created";
        $exceldata[$row][]                                     = $colorClass."Date Due";
        $exceldata[$row][]                                     = $colorClass."Consulting (excl. VAT)";
        $exceldata[$row][]                                     = $colorClass."Expenses (excl. VAT)";
        $exceldata[$row][]                                     = $colorClass."Paid";                        // 14
        $row++;

        ///////////////////////////
        //  Create Table Headers
        $headers = "<tr>
                        <th>Project<br/>Name</th>
                        <th>Due<br/>Date</th>
                        <th>Manager</th>
                        <th>Budget<i>(".$_SESSION["currency"].")</i><br/><i>(excl. VAT)</i></th>
                        <th>Invoiced<i>(".$_SESSION["currency"].")</i><br/><i>(excl. VAT)</i></th>
                        <th>WIP<i>(".$_SESSION["currency"].")</i><br/><i>(excl. VAT)</i></th>
                        <th>Not Invoiced <i>(".$_SESSION["currency"].")</i><br/><i>(excl. VAT)</i></th>
                        <th>Total<br/>Consulting<br/>Booked <i>(".$_SESSION["currency"].")</i><br/><i>(excl. VAT)</i></th>
                        <th>Total<br/>Expenses<br/>Booked <i>(".$_SESSION["currency"].")</i><br/><i>(excl. VAT)</i></th>
                        <th style='background-color:#36332a'><font style='size:17pt; color:orange'>List of Invoices</font><br/>
                            <table>
                                <tr>
                                    <td style='min-width: 375px; border-bottom: 0px; background-color:#36332a; padding-right:50px;'>Invoice Name/Number</td>
                                    <td style='min-width:100px; border-bottom: 0px; background-color:#36332a'>Date Created</td>
                                    <td style='min-width:75px; border-bottom: 0px; background-color:#36332a'>Date Due</td>
                                    <td style='border-bottom: 0px; background-color:#36332a'>Consulting<br/><i>(excl. VAT)</i></td>
                                    <td style='border-bottom: 0px; background-color:#36332a'>Expenses<br/><i>(excl. VAT)</i></td>
                                    <td style='border-bottom: 0px; background-color:#36332a'>Paid</td>
                                </tr>
                            </table>
                        </th>
                    </tr>";

        if (is_array($projects)) {
            foreach ($projects as $project) {
                $displayString .= "<tr>";
                    $displayString .= "<td style='white-space:nowrap;'>".$project[1]."</td>";
                    $displayString .= "<td style='white-space:nowrap;' class='rightdata'>".$project[2]."</td>";

                    //$manager = q("SELECT CONCAT(lstname, ', ', frstname) FROM Employee WHERE id = '".$project[3]."'");
                    $tmp = q("SELECT user_id FROM Project_User WHERE company_id = '".$_SESSION["company_id"]."' AND project_id = '".$project[0]."' AND manager = '1' LIMIT 1");
                    $manager = q("SELECT CONCAT(lstname, ', ', frstname) FROM Employee WHERE id = '".$tmp."'");

                    $displayString .= "<td style='white-space:nowrap;' >".$manager."</td>";
                    $displayString .= "<td class='rightdata'>".number_format((double)$project[4], 2, ".", "")."</td>";

                    $invoiced = q("SELECT SUM(consulting + expenses) FROM LoadInvoices WHERE project_id = '".$project[0]."' AND company_id = '".$_SESSION["company_id"]."'");

                    $displayString .= "<td class='rightdata'>".number_format((double)$invoiced, 2, ".", "")."</td>";

                    $wip_booked = q("SELECT SUM(time_spent * rate) FROM $table WHERE project_id = '".$project[0]."' AND company_id = '".$_SESSION["company_id"]."' $where");
                    $wip_invoiced = q("SELECT SUM(consulting) FROM LoadInvoices WHERE project_id = '".$project[0]."' AND company_id = '".$_SESSION["company_id"]."'");
                    $wip = $wip_booked - $wip_invoiced;

					$text_color = "color:#FFFFFF;";

					if ($wip < 0)
						$wip = 0;

					if ($wip < 0)
						$text_color = "color:#FF0000;";

					$displayString .= "<td style='white-space:nowrap; ".$text_color."' class='rightdata'>".number_format((double)$wip, 2, ".", "")."</td>";

					$saldo = $project[4] - $invoiced ;

					$text_color = "color:#FFFFFF;";

					if ($saldo < 0)
						$text_color = "color:#FF0000;";

					$displayString .= "<td class='rightdata' style='white-space:nowrap; ".$text_color."'>".number_format((double)$saldo, 2, ".", "")."</td>";

                    $consulting_booked = q("SELECT SUM(time_spent * rate) FROM $table WHERE project_id = '".$project[0]."' AND company_id = '".$_SESSION["company_id"]."' $where");

					$displayString .= "<td class='rightdata' style='white-space:nowrap;'>".number_format((double)$consulting_booked, 2, ".", "")."</td>";

                    $expenses_booked = q("SELECT SUM(expense) FROM $table2 WHERE project_id = '".$project[0]."' AND company_id = '".$_SESSION["company_id"]."' $where2");

					$displayString .= "<td class='rightdata' style='white-space:nowrap;'>".number_format((double)$expenses_booked, 2, ".", "")."</td>";

					$invoices = q("SELECT name, create_date, due_date, consulting, expenses, paid FROM LoadInvoices WHERE project_id = '".$project[0]."'
											AND company_id = '".$_SESSION["company_id"]."' ORDER BY due_date");

                    $displayString .= "<td>";
                        if (is_array($invoices)) {
							$count = 0;
                            $displayString .= "<table>";

                            foreach ($invoices as $invoice) {
                                $displayString .= "<tr>";
                                    $displayString .= "<td style='white-space:nowrap; min-width:420px;' class='noborder'>".$invoice[0]."</td>";
                                    $displayString .= "<td style='white-space:nowrap' class='noborder'>".$invoice[1]."</td>";
                                    $displayString .= "<td style='white-space:nowrap' class='noborder'>".$invoice[2]."</td>";
                                    $displayString .= "<td style='min-width:85px;' class='rightdata noborder'>".number_format((double)$invoice[3], 2, ".", "")."</td>";
                                    $displayString .= "<td style='min-width:85px;' class='rightdata noborder'>".number_format((double)$invoice[4], 2, ".", "")."</td>";

                                    if ($invoice[5] == "1"){
                                        $displayString .= "<td class='noborder'>
                                                                        <input disabled type='checkbox' value='1' checked>
                                                                    </td>";
                                    }else{
                                        $displayString .= "<td class='noborder'>
                                                                        <input disabled type='checkbox' value='1'>
                                                                    </td>";
                                    }
                                $displayString .= "</tr>";

								if ($count == 0)	{
									$exceldata[$row][]                                     = $project[1];
									$exceldata[$row][]                                     = $project[2];
									$exceldata[$row][]                                     = $manager;
									$exceldata[$row][]                                     = $project[4];
									$exceldata[$row][]                                     = $invoiced;
									$exceldata[$row][]                                     = $wip;
									$exceldata[$row][]                                     = $saldo;            // 7
									$exceldata[$row][]                                     = $consulting_booked;            // 7
									$exceldata[$row][]                                     = $expenses_booked;            // 7
								} else	{
									$exceldata[$row][]                                     = "";
									$exceldata[$row][]                                     = "";
									$exceldata[$row][]                                     = "";
									$exceldata[$row][]                                     = "";
									$exceldata[$row][]                                     = "";
									$exceldata[$row][]                                     = "";
									$exceldata[$row][]                                     = "";
									$exceldata[$row][]                                     = "";
									$exceldata[$row][]                                     = "";
								}
                                $exceldata[$row][]                                     = " ";                  // Spacer
                                $exceldata[$row][]                                     = $invoice[0];
                                $exceldata[$row][]                                     = $invoice[1];
                                $exceldata[$row][]                                     = $invoice[2];
                                $exceldata[$row][]                                     = $invoice[3];
                                $exceldata[$row][]                                     = $invoice[4];
                                if($invoice[5] == 1)
                                    $exceldata[$row][]                                  = "Yes";
                                else
                                    $exceldata[$row][]                                  = "No";
                                $exceldata[$row][]                                     = " ";               // 14

                                $count++;

                                $row++;
                            }
                            $displayString .= "</table>";
                        }else{
                                $displayString .= "&nbsp;&nbsp;&nbsp;&nbsp; -";
                            $exceldata[$row][]                                     = $project[1];
                            $exceldata[$row][]                                     = $project[2];
                            $exceldata[$row][]                                     = $manager;
                            $exceldata[$row][]                                     = $project[4];
                            $exceldata[$row][]                                     = $invoiced;
                            $exceldata[$row][]                                     = $wip;
                            $exceldata[$row][]                                     = $saldo;
							$exceldata[$row][]                                     = $consulting_booked;            // 7
							$exceldata[$row][]                                     = $expenses_booked;            // 7
                            for($i=0; $i <7; $i++)
                                $exceldata[$row][]                                  = " - ";                  // 8 - 14
                            $row++;
                        }
                    $displayString .= "</td>";
                $displayString .= "</tr>";
            }
        }

        if ($displayString != "") {
            $displayString = "<tr><td class='on-table-clear' colspan='10'><a>Project Overview Report<a></td></tr>".$headers.$displayString;
            $generated = "1";
        }
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("3", "reports");
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    function check() {
        var valid = 1;

        if (document.forms["report"].project.value == "null") {
            ShowLayer("projectDiv", "block");
            valid = 0;
        }
        else
            ShowLayer("projectDiv", "none");

        if (valid == 1) {
            document.forms["report"].save.value = 1;
            document.forms["report"].submit();
        }
    }

    ///////////////////////////
    //  Sajax
    <?php sajax_show_javascript(); ?>

    function ClearOptions(OptionList) {
        for (x = OptionList.length; x >= 0; x--) {
            OptionList[x] = null;
        }
    }

    function setProjects(data) {
        var c = 0;

        document.forms["report"].project.options[(c++)] = new Option("--  Select A Project  --", "null");
        document.forms["report"].project.options[(c++)] = new Option("All Projects", "all");

        for (var i in data)
            eval("document.forms['report'].project.options["+(c++)+"] = new Option('" + data[i][1] + "', '" + data[i][0] + " ');");
    }

    function getProjects(select1,select2) {
        var status = eval("document.report."+select1+".value;");
        var type = eval("document.report."+select2+".value;");;

        ClearOptions(document.report.project);
        x_getProjects(status, type, setProjects);
    }
    ///////////////////////////
</script>
<?php
    $n[0][0] = "0"; $n[0][1] = "Shared Projects";
    $projectTypes = q("SELECT id, type FROM ProjectTypes WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY type");
    if (!is_array($projectTypes))   $projectTypes = $n;
    else                            $projectTypes = array_merge($n,$projectTypes);
    $projectTypes = sortArrayByColumn($projectTypes, 1);

	$projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) ".
		"WHERE cot.company_id = '".$_SESSION["company_id"]."' AND p.completed = '0' AND p.deleted = '0' ORDER BY UPPER(p.name)");

?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="report">
                    <div id="" style="<?php if ($generated == "1") echo "display: none;"; else echo "display: block"; ?>">
                        <table width="100%">
                            <tr>
                                <td class="centerdata">
                                    <h6>Project Overview Report</h6>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Report On:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="reportOn">
                                        <option value="normal">Actual Time</option>
                                        <option value="approved">Approved Time</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Project Status:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="projectStatus" onChange="getProjects('projectStatus','projectType');">
                                        <option value="all">Completed & Uncompleted</option>
                                        <option value="0" selected>Uncompleted</option>
                                        <option value="1">Completed</option>
                                    </select>
                                </td>
                            </tr>
                            <tr><td colspan="100%"><br/></td></tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Project Type:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="projectType" onChange="getProjects('projectStatus','projectType');">
                                        <option value="null">All Project Types</option>
                                        <?php
                                            if (is_array($projectTypes))
                                                foreach ($projectTypes as $projectType)
                                                    if ($_POST["projectType"] == $projectType[0])
                                                        echo "<option value='".$projectType[0]."' selected>".$projectType[1]."</option>";
                                                    else
                                                        echo "<option value='".$projectType[0]."'>".$projectType[1]."</option>";
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Project:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" id="project" name="project">
                                        <option value="null">--  Select A Project  --</option>
                                        <option value="all">All Projects</option>
                                        <?php
                                            if (is_array($projects))
                                                foreach ($projects as $project)
                                                    if (isset($_POST["project"]) && $_POST["project"] == $project[0])
                                                        echo "<option value='".$project[0]."' selected>".$project[1]."</option>";
                                                    else
                                                        echo "<option value='".$project[0]."'>".$project[1]."</option>";
                                        ?>
                                    </select>
                                    <div id="projectDiv" style="display: none;"><font class="on-validate-error">* Project must be selected</font></div>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <input name="btnGenerate" onClick="check();" type="button" value="Generate Report">
                        <input method="post" name="save" type="hidden" value="0" />
                    </div>
                </form>
                <div id="report" style="<?php if ($generated == "1") echo "display: block;"; else echo "display: none"; ?>">
                    <?php
                        echo "<div class='on-20px'><table class='on-table-center on-table'>";
                            echo "".$displayString;
                            echo  "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        echo "</table></div>";
                        echo "<br/>";

                        ///////////////////////////
                        //  Set Export Information
                        $_SESSION["fileName"] = "Project Overview Report";
                        $_SESSION["fileData"] = array_merge($excelheadings, $exceldata);
                        ///////////////////////////

                        echo "<input name='btnExport' onClick=\"location.href='_export.php'\" type='button' value='Export to Excel' />";
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
