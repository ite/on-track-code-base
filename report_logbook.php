<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("include/sajax.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    if (!hasAccess("REP_LOGBOOK"))
        header("Location: noaccess.php");

    // Class for table background color
    $colorClass = "OnTrack_#_";

    //  Set Report Generated Status
    $generated = "0";

    //  Create Display Information Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")        {
        unset($exceldata);

        $errorMessage = "";

        $reportOn = $_POST["reportOn"];
        $vehicle_id = $_POST["vehicle"];
        $date_type = $_POST["dateType"];

        //  Get Dates
        if ($date_type == "currentwk")  {
            $date_from = currentWeekStart();
            $date_to = $today;
        }
        else if ($date_type == "previouswk")    {
            $date_from = getDates(currentWeekStart(), -7);
            $date_to = getDates(currentWeekStart(), -1);
        }
        else if ($date_type == "currentmnth")   {
            $date_from = getMonthStart($today);
            $date_to = $today;
        }
        else if ($date_type == "previousmnth")  {
            $date_from = getPreviousMonthStart();
            $date_to = getMonthEnd(getPreviousMonthStart());
        }
        else if ($date_type == "projectLifetime")      {
            $date_from = q("SELECT MIN(date) FROM ExpenseSheet WHERE project_id = '".$project_id."'");
            $date_to = $today;
        }
        else if ($date_type == "custom")        {
            $date_from = addslashes(strip_tags($_POST["date_from"]));
            $date_to = addslashes(strip_tags($_POST["date_to"]));
        }

        $table = ($reportOn == "approved") ? "ApprovedExpense" : "ExpenseSheet";
        $where = ($reportOn == "approved") ? "AND es.status = '2' " : "";

		if ($vehicle_id == "all")	{
			$vehicle = "All Vehicles";
			$expenseHeadings = array("Project","Vehicle","Date","Opening km's","Closing km's","Total km's","Private km's","Business km's","Rate <i>(".$_SESSION["currency"].")</i>","Total Amount <i>(".$_SESSION["currency"].")</i>",
										"Business Travel Details <i>(where, reason for visit)</i>","Actual Fuel & Oil Costs <i>(".$_SESSION["currency"].")</i>",
										"Actual Repair & Maintenance Costs <i>(".$_SESSION["currency"].")</i>");

			$expenseInfo = q("SELECT p.name,v.type,es.date,es.odoStart,es.odoFinish,es.kilometers,es.privateKM,es.businessKM,es.rate,es.expense,es.descr,'',''
								FROM ($table AS es
									INNER JOIN Project AS p ON es.project_id = p.id
									INNER JOIN dropdowns AS dd ON es.expense_type_id = dd.id
									LEFT JOIN Vehicle AS v ON es.vehicle_id = v.id)
								WHERE es.date >= '".$date_from."' AND es.date <= '".$date_to."' AND es.company_id = '".$_SESSION["company_id"]."' AND dd.name = 'Driving' $where
								ORDER BY v.type, es.odoStart, es.odoFinish");
		} else	{
			$vehicle = q("SELECT type FROM Vehicle WHERE id = '".$vehicle_id."'");
			$expenseHeadings = array("Project","Date","Opening km's","Closing km's","Total km's","Private km's","Business km's","Rate <i>(".$_SESSION["currency"].")</i>","Total Amount <i>(".$_SESSION["currency"].")</i>",
										"Business Travel Details <i>(where, reason for visit)</i>","Actual Fuel & Oil Costs <i>(".$_SESSION["currency"].")</i>",
										"Actual Repair & Maintenance Costs <i>(".$_SESSION["currency"].")</i>");

			$expenseInfo = q("SELECT p.name,es.date,es.odoStart,es.odoFinish,es.kilometers,es.privateKM,es.businessKM,es.rate,es.expense,es.descr,'',''
								FROM ($table AS es
									INNER JOIN Project AS p ON es.project_id = p.id
									INNER JOIN dropdowns AS dd ON es.expense_type_id = dd.id
									LEFT JOIN Vehicle AS v ON es.vehicle_id = v.id)
								WHERE es.date >= '".$date_from."' AND es.date <= '".$date_to."' AND es.company_id = '".$_SESSION["company_id"]."' AND dd.name = 'Driving' AND v.id = '".$vehicle_id."' $where
								ORDER BY v.type, es.odoStart, es.odoFinish");
		}

        //  Timestamp
        $timestamp = ($date_from === $date_to) ? "Time Period: ".$date_from : "Time Period: ".$date_from." - ".$date_to;
        $reportHead = array("<a>Report: Logbook</a>","<a>Report For: ".$vehicle."</a>",$timestamp);

        if (is_array($expenseInfo))     {
            $columns = count($expenseHeadings) - 1;

            $displayString = "";
            $row = 0;

            //  HTML Display
            foreach ($reportHead as $rh)
                $displayString .= "<tr><td class='on-table-clear' colspan='".($columns + 1)."'>".$rh."</td></tr>";

            //  EXCEL DISPLAY
            foreach ($reportHead as $rh)        {
                $exceldata[$row][] = strip_tags($rh);

                for ($a = 0; $a < $columns; $a++)       $exceldata[$row][] = "";

                $row++;
            }

            $total_km = 0;
            $total_private = 0;
            $total_business = 0;

            //  Headings
            //  HTML Display
            $displayString .= "<tr>";

            foreach ($expenseHeadings as $h)    $displayString .= "<th>".$h."</th>";

            $displayString .= "</tr>";

            //  EXCEL DISPLAY
            foreach ($expenseHeadings as $h)    $exceldata[$row][] = $colorClass.strip_tags($h);

            $row++;

            //  Data
            foreach ($expenseInfo as $r)        {
                $col = 0;
                $displayString .= "<tr>";

                foreach ($r as $d)      {
                    if ($expenseHeadings[$col] == "Total km's")	$total_km += $d;
                    if ($expenseHeadings[$col] == "Private km's")	$total_private += $d;
                    if ($expenseHeadings[$col] == "Business km's")	$total_business += $d;

                    if (preg_match("/^((\d{2}(([02468][048])|([13579][26]))[\-\/\s]?((((0?[13578])|(1[02]))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-\/\s]?((0?[1-9])|([1-2][0-9])))))|(\d{2}(([02468][1235679])|([13579][01345789]))[\-\/\s]?((((0?[13578])|(1[02]))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-\/\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))(\s(((0?[1-9])|(1[0-2]))\:([0-5][0-9])((\s)|(\:([0-5][0-9])\s))([AM|PM|am|pm]{2,2})))?$/", $d))
                        $displayString .= "<td style='white-space:nowrap'>".$d."</td>";
                    else if (is_numeric($d))
                        $displayString .= "<td style='white-space:nowrap' class='rightdata'>".number_format($d, 2, ".", "")."</td>";
                    else if ($d == "")
                        $displayString .= "<td style='white-space:nowrap'>-</td>";
                    else
                        $displayString .= "<td style='white-space:nowrap'>".$d."</td>";

                    $exceldata[$row][] .= $d;

                    $col++;
                }

                $displayString .= "</tr>";
                $row++;
            }

            $colspan = ($vehicle_id == "all") ? 5 : 4;

            $displayString .= "<tr>
                                    <td class='on-table-total' colspan='".$colspan."'><a style='color:#FFFFFF;'>Total</a></td>
                                    <td class='on-table-total'>".number_format($total_km, 2, ".", "")."</td>
                                    <td class='on-table-total'>".number_format($total_private, 2, ".", "")."</td>
                                    <td class='on-table-total'>".number_format($total_business, 2, ".", "")."</td>
                                    <td class='on-table-total'></td>
                                    <td class='on-table-total'></td>
                                    <td class='on-table-total'></td>
                                    <td class='on-table-total'></td>
                                    <td class='on-table-total'></td>
                                </tr>";

            for ($a = 0; $a < $colspan - 1; $a++)       $exceldata[$row][] = "";

            $exceldata[$row][] .= "Total (".$_SESSION["currency"]."):";
            $exceldata[$row][] .= $total_km;
            $exceldata[$row][] .= $total_private;
            $exceldata[$row][] .= $total_business;
            $exceldata[$row][] .= "";
            $exceldata[$row][] .= "";
            $exceldata[$row][] .= "";
            $exceldata[$row][] .= "";
            $exceldata[$row][] .= "";

            $row++;
        }

        if ($displayString != "")
            $generated = "1";
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("3", "reports");
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    jQuery(function(){
		jQuery('#date_from').DatePicker({
			format:'Y-m-d',
			date: jQuery('#date_from').val(),
			current: jQuery('#date_from').val(),
			starts: 1,
			position: 'right',
			onBeforeShow: function(){
				var _date;
				if (!validation('date',jQuery('#date_from').val()))
				_date = new Date();
				else _date = jQuery('#date_from').val();
				jQuery('#date_from').DatePickerSetDate(_date, true);
			},
			onChange: function(formated, dates){
				jQuery('#date_from').val(formated);
				jQuery('#date_from').DatePickerHide();
			}
		});

        jQuery('#date_to').DatePicker({
			format:'Y-m-d',
			date: jQuery('#date_to').val(),
			current: jQuery('#date_to').val(),
			starts: 1,
			position: 'right',
			onBeforeShow: function(){
				var _date;
				if (!validation('date',jQuery('#date_to').val()))
				_date = new Date();
				else _date = jQuery('#date_to').val();
				jQuery('#date_to').DatePickerSetDate(_date, true);
			},
			onChange: function(formated, dates){
				jQuery('#date_to').val(formated);
				jQuery('#date_to').DatePickerHide();
			}
		});
    });

    function check() {
        var valid = 1;

        if (document.forms["report"].dateType.value == "custom") {
            //  Check That Date From Is Entered
            if (document.forms["report"].date_from.value == "") {
                ShowLayer("dateFrom", "block");
                valid = 0;
            }
            //  Check That Entered Date From Is Valid
            else {
                ShowLayer("dateFrom", "none");

                if (!validation("date", document.forms["report"].date_from.value)) {
                    ShowLayer("dateFromDiv", "block");
                    valid = 0;
                }
                else
                    ShowLayer("dateFromDiv", "none");
            }

            //  Check That Date To Is Entered
            if (document.forms["report"].date_to.value == "") {
                ShowLayer("dateTo", "block");
                valid = 0;
            }
            //  Check That Entered Date To Is Valid
            else {
                ShowLayer("dateTo", "none");

                if (!validation("date", document.forms["report"].date_to.value)) {
                    ShowLayer("dateToDiv", "block");
                    valid = 0;
                }
                else
                    ShowLayer("dateToDiv", "none");
            }
        }

        if (valid == 1) {
            document.forms["report"].save.value = 1;
            document.forms["report"].submit();
        }
    }

    function dateSelection() {
        if (document.forms["report"].dateType.value == "custom")
            ShowLayer("datesDiv", "block");
        else
            ShowLayer("datesDiv", "none");
    }
</script>
<?php
    $vehicles = q("SELECT id, type FROM Vehicle WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY type");
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="report">
                    <div id="" style="<?php if ($generated == "1") echo "display: none;"; else echo "display: block"; ?>">
                        <table width="100%">
                            <tr>
                                <td class="centerdata">
                                    <h6>Logbook Report</h6>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Report On:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="reportOn">
                                        <option value="normal">Actual Expenses</option>
                                        <option value="approved">Approved Expenses</option>
                                    </select>
                                </td>
                            </tr>
							<tr>
								<td class="on-description" width="50%">
									Vehicle:
								</td>
								<td width="50%">
									<select class="on-field" method="post" name="vehicle">
										<option value="all">All Vehicles</option>
										<?php
											if (is_array($vehicles))
												foreach ($vehicles as $vehicle)
													if ($_POST["vehicle"] == $vehicle[0])
														echo "<option value='".$vehicle[0]."' selected>".$vehicle[1]."</option>";
													else
														echo "<option value='".$vehicle[0]."'>".$vehicle[1]."</option>";
										?>
									</select>
								</td>
							</tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Date Type(s):
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" id="dateType" name="dateType" onChange="dateSelection();">
                                        <option value="currentwk">Current Week</option>
                                        <option value="previouswk">Previous Week</option>
                                        <option value="currentmnth">Current Month</option>
                                        <option value="previousmnth">Previous Month</option>
                                        <option value="projectLifetime" disabled>Project Lifetime</option>
                                        <option value="custom">Custom Dates</option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                        <div id="datesDiv" style="display: none;">
                            <table cellpadding="0" cellspacing="2" width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Date From:
                                    </td>
                                    <td width="50%">
                                        <input class="on-field-date" id="date_from" name="date_from" type="text" style="text-align:right;" value="<?php
                                            if ($_POST["date_from"] != "") echo "".$_POST["date_from"]; else echo "".getMonthStart($today); ?>">
                                        <div id="dateFrom" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                        <div id="dateFromDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="on-description" width="50%">
                                        Date To:
                                    </td>
                                    <td width="50%">
                                        <input class="on-field-date" id="date_to" name="date_to" type="text" style="text-align:right;" value="<?php
                                            if ($_POST["date_to"] != "") echo "".$_POST["date_to"]; else echo "".getMonthEnd($today); ?>">
                                        <div id="dateTo" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                        <div id="dateToDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <br/>
                        <input name="btnGenerate" onClick="check();" type="button" value="Generate Report">
                        <input method="post" name="save" type="hidden" value="0" />
                    </div>
                </form>
                <div id="report" style="<?php if ($generated == "1") echo "display: block;"; else echo "display: none"; ?>">
                    <?php
                        echo "<div class='on-20px'><table class='on-table-center on-table'>";
                            echo "".$displayString;
                            echo  "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        echo "</table></div>";
                        echo "<br/>";

                        ///////////////////////////
                        //  Set Export Information
                        $_SESSION["fileName"] = "Logbook Report";
                        $_SESSION["fileData"] = $exceldata;
                        ///////////////////////////

                        echo "<input name='btnExport' onClick=\"location.href='_export.php'\" type='button' value='Export to Excel' />";
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
