<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("include/sajax.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

   if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    if (!hasAccess("STATEMENT_MAN"))
        header("Location: noaccess.php");

    //  Create Display Information Function
    if (isset($_POST["save"]) && $_POST["save"] === "1") 
    {
        GLOBAL $excelheadings;
        
        $errorMessage = "";
        $date_type = $_POST["dateType"];

        //  Get Dates
        if ($date_type == "currentwk") {
            $date_from = currentWeekStart();
            $date_to = $today;
        }
        else if ($date_type == "previouswk") {
            $date_from = getDates(currentWeekStart(), -7);
            $date_to = getDates(currentWeekStart(), -1);
        }
        else if ($date_type == "currentmnth") {
            $date_from = getMonthStart($today);
            $date_to = $today;
        }
        else if ($date_type == "previousmnth") {
            $date_from = getPreviousMonthStart();
            $date_to = getMonthEnd(getPreviousMonthStart());
        }
        else if ($date_type == "custom") {
            $date_from = addslashes(strip_tags($_POST["date_from"]));
            $date_to = addslashes(strip_tags($_POST["date_to"]));
        }

        //  Timestamp
        if ($date_from === $date_to)
            $timestamp = "Time Period: ".$date_from;
        else
            $timestamp = "Time Period: ".$date_from." - ".$date_to;

        $account_id = $_POST["account"];

		//SET where clause
        $where = "";
        if($account_id != null && is_numeric($account_id))
            $where .= " AND s.account = '$account_id' ";
        $where .= " AND s.date BETWEEN '$date_from' AND '$date_to' ";
        
		//GET transactions
		//	all transactions with non-zero amounts
		$trans = q("SELECT s.date,CONCAT(a.name, ' - ', s.account),s.description,s.amount,s.id,s.description_new,sl.id FROM Statement AS s 
                            INNER JOIN Account AS a ON s.account = a.account 
							LEFT JOIN StatementLink AS sl ON s.id = sl.statement_id
                            WHERE s.company_id = '".$_SESSION['company_id']."' AND s.amount != 0 $where ORDER BY date,amount");

		$displayString = "<table style='margin-left:auto; margin-right:auto' class='on-table'><tr><td class='on-table-clear' colspan='8'><a>Bank Statements Update<a></td></tr>";
		$displayString .= "<tr><th>Date</th><th>Account</th><th>Description</th><th>Updated Description</th><th>Amount</th><th></th></tr>";
		if (is_array($trans))
		foreach($trans as $tt)	{
			$displayString .=  "<tr>
                                            <td style='white-space:nowrap;'><input type='hidden' value='".$tt[4]."'/>".$tt[0]."</td>
											<td>".$tt[1]."</td>
											<td>".$tt[2]."</td>
                                            <td><input class='descr' id='".$tt[4]."' type='text' size='50' value='".$tt[5]."'/><input type='hidden' value='".$tt[4]."'/></td>
											<td class='rightdata' style='white-space:nowrap;border-right:1px solid #36332a;'>".$tt[3]."</td>";
			if ($tt[6] == "")
				$displayString .= 			"<td><input class='delete' type='image' id='".$tt[4]."' src='images/icons/delete.ico' /></td>";
			else
				$displayString .= 			"<td></td>";
//				$displayString .= 			"<td><input class='delete_fail' type='image' src='images/icons/delete.ico' /></td>";
			$displayString .=				"</tr>";
		}
		$displayString .= "</table>";
        
        if ($displayString != "")
            $generated                                                  = "1";
    }
    
    //  Print Header
    print_header();
    //  Print Menu
	if ($_GET["menu"] == "")
		print_menus("0", "home");
	else
		print_menus($_GET["menu"], "home");

	?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    jQuery(function(){

	//Update Description
	///////////////////////////////////////////////
	jQuery(".descr").change(function()    {
		var statement_id = jQuery(this).next().val();
		var description = jQuery(this).val();
		jQuery.post("_ajax.php", {func: "statementUpdate", statement_id: statement_id, description: description}, function(data){});
	});

	jQuery(".delete").click(function()    {
		var statement_id = jQuery(this).attr('id');
		var row = jQuery(this).parent().parent();
		var answer = confirm("Sure you want to delete this statement entry?");
		if (answer)
			jQuery.post("_ajax.php", {func: "statementDelete", statement_id: statement_id}, function(data){
				row.remove();
			});
	});

	jQuery(".delete_fail").click(function()    {
		alert("You can only delete unlinked statement entries");
	});

	jQuery('#date_from').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_from').val(),
		current: jQuery('#date_from').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_from').val()))
			_date = new Date();
			else _date = jQuery('#date_from').val();
			jQuery('#date_from').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_from').val(formated);
			jQuery('#date_from').DatePickerHide();
		}
	});
	jQuery('#date_to').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_to').val(),
		current: jQuery('#date_to').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_to').val()))
			_date = new Date();
			else _date = jQuery('#date_to').val();
			jQuery('#date_to').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_to').val(formated);
			jQuery('#date_to').DatePickerHide();
		}
	});
    });

    //  Sajax
    <?php sajax_show_javascript(); ?>

    function check()
    {
        var valid                                                       = 1;

        if (document.forms["report"].dateType.value == "custom") {
            //  Check That Date From Is Entered
            if (document.forms["report"].date_from.value == "") {
                ShowLayer("dateFrom", "block");
                valid                                                   = 0;
            }
            //  Check That Entered Date From Is Valid
            else {
                ShowLayer("dateFrom", "none");

                if (!validation("date", document.forms["report"].date_from.value)) {
                    ShowLayer("dateFromDiv", "block");
                    valid                                               = 0;
                }
                else
                    ShowLayer("dateFromDiv", "none");
            }

            //  Check That Date To Is Entered
            if (document.forms["report"].date_to.value == "") {
                ShowLayer("dateTo", "block");
                valid                                                   = 0;
            }
            //  Check That Entered Date To Is Valid
            else {
                ShowLayer("dateTo", "none");

                if (!validation("date", document.forms["report"].date_to.value)) {
                    ShowLayer("dateToDiv", "block");
                    valid                                               = 0;
                }
                else
                    ShowLayer("dateToDiv", "none");
            }
        }

        if (valid == 1) {
            document.forms["report"].save.value                         = 1;
            document.forms["report"].submit();
        }
    }

    function dateSelection() {
        if (document.forms["report"].dateType.value == "custom")
            ShowLayer("datesDiv", "block");
        else
            ShowLayer("datesDiv", "none");
    }


</script>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata">
<?php
			if ($generated == "1")	{
				echo $displayString;
			}
			else	{
?>
                <form action="" method="post" name="report">
                    <div id="" style="<?php if ($generated == "1") echo "display: none;"; else echo "display: block"; ?>">
                        <table width="100%">
                            <tr>
                                <td class="centerdata">
                                    <h6>Statements Update</h6>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Account:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="account">
                                        <option value="null">All</option>
                                        <?php
                                        	$accounts = q("SELECT account,name FROM Account WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY id");
                                            if (is_array($accounts))
                                                foreach ($accounts as $ac)
                                                    if ($_POST["account"] == $ac[0])
                                                        echo "<option value='".$ac[0]."' selected>".$ac[1]."</option>";
                                                    else
                                                        echo "<option value='".$ac[0]."'>".$ac[1]."</option>";
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Date Type(s):
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="dateType" onChange="dateSelection();">
                                        <option value="currentwk">Current Week</option>
                                        <option value="previouswk">Previous Week</option>
                                        <option value="currentmnth">Current Month</option>
                                        <option value="previousmnth">Previous Month</option>
                                        <option value="custom">Custom Dates</option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                        <div id="datesDiv" style="display: none;">
                            <table width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Date From:
                                    </td>
                                    <td width="50%">
                                        <input class="on-field-date" id="date_from" name="date_from" type="text" style="text-align:right;" value="<?php
                                            if ($_POST["date_from"] != "") echo "".$_POST["date_from"]; else echo "".getMonthStart($today); ?>">
                                        <div id="dateFrom" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                        <div id="dateFromDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="on-description" width="50%">
                                        Date To:
                                    </td>
                                    <td width="50%">
                                        <input class="on-field-date" id="date_to" name="date_to" type="text" style="text-align:right;" value="<?php
                                            if ($_POST["date_to"] != "") echo "".$_POST["date_to"]; else echo "".getMonthEnd($today); ?>">
                                        <div id="dateTo" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                        <div id="dateToDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <br/>
                        <input name="btnGenerate" onClick="check();" type="button" value="Load">
                        <input method="post" name="save" type="hidden" value="0" />
                    </div>
                </form>
<?php
			}
?>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
