<?php
    session_start();

    include("../_db.php");

    $projectID = addslashes(strip_tags($_GET["projectID"]));
    $companyID = addslashes(strip_tags($_GET["companyID"]));
    $status = addslashes(strip_tags($_GET["status"]));

    if (is_numeric($projectID) && is_numeric($companyID))   {
        $info = q("SELECT locked,status,teamManage FROM companiesOnTeam WHERE project_id = '".$projectID."' AND company_id = '".$companyID."'");

        if (is_array($info))   {
            if ($info[0][0] == 0)   {
                switch ($status)	{
                    case "acceptBoth":	{
                        $msg = "Thank you for becoming part of the project team, your action is being processed...";
                        $update = q("UPDATE companiesOnTeam SET status = '1', teamManage = '1', locked = '1' ".
                                        "WHERE project_id = '".$projectID."' AND company_id = '".$companyID."'");

                        break;
                    }
                    case "acceptCompany":	{
                        $msg = "Thank you for becoming part of the project team, your action is being processed...";
                        $update = q("UPDATE companiesOnTeam SET status = '1', teamManage = '2', locked = '1' ".
                                        "WHERE project_id = '".$projectID."' AND company_id = '".$companyID."'");

                        break;
                    }
                    case "decline":	{
                        $msg = "Thank you for considering being part of the project team, hope you will become part of the team in future projects.<br/>".
                                    "Your action is being processed";
                        $update = q("UPDATE companiesOnTeam SET status = '2', teamManage = '2', locked = '1' ".
                                        "WHERE project_id = '".$projectID."' AND company_id = '".$companyID."'");

                        break;
                    }
                    default:	{
                        $msg = "ERROR: DEFAULT was reached. Fix function/status reference";

                        break;
                    }
                }
            }
            else    {
                $msg = ($info[0][1] == 1 && $info[0][2] == 1) ? "Your company has already become part of the project team, thank you for your participation" 
                            : (($info[0][1] == 1 && $info[0][2] == 2) ? "Your company has already become part of the project team, thank you for your participation" 
                            : "Your company has already decided not to become part of the project team, hope you will become part of the team in future projects.");
            }
        }
    }
?>
<html>
    <head>
        <title>
            On-Track - Shared Project Status Update Script
        </title>
        <link href="../images/icons/bookings.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
        <link href="../include/style.css" rel="stylesheet" type="text/css" />
        <link href="../CSSFiles/styles.css" rel="stylesheet" type="text/css" />
    </head>
    <script type="text/JavaScript" src="../include/jquery.js"></script>
    <script language="JavaScript">
        var closeWindow;

        jQuery(function()    {
            closeWindow = function(e)  {
                var sec = parseFloat(jQuery("#countDown").text());
                var timer = setInterval(function() { 
                    jQuery("#countDown").text(--sec);
                    if (sec == 0)   {
                        clearInterval(timer);
                        window.close();
                    }
                }, 1000);
            }
            closeWindow();
        });
    </script>
    <body>
        <center>
            <div style="height:250px; background-image:url(../CSSFiles/im/notifyWall.png)">
                <br>
                <img src ="../CSSFiles/im/logo.png"/>
                <div style="height:40px">&nbsp;</div>
                <font style='font-weight:bold; font-size:10pt; color:#000000'><?php echo $msg; ?></font>
                <div style="height:58px;">&nbsp;</div>
                <a>This window will close in <span id="countDown" name="countDown">10</span> seconds</a>
            </div>
        </center>
    </body>
</html>
