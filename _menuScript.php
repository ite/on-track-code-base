<?php
    include("_db.php");
    include("_dates.php");
?>
<html>
    <head>
        <title>
            On-Track - Recovery Script...
        </title>
    </head>
    <body>
        <h1 align="center">
            Install - Menu Script
        </h1>
        <?php
            $date = date("Y-m-d H:i:s");
            $user = "admin";

            $drop = q("DROP TABLE IF EXISTS menus");
            $create = q("CREATE TABLE menus (".
                        "id MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,".
                        "parentID MEDIUMINT UNSIGNED NOT NULL DEFAULT 0, INDEX(parentID),".
                        "display TINYINT UNSIGNED NOT NULL DEFAULT 1,".
                        "modal TINYINT UNSIGNED NOT NULL DEFAULT 0,".
                        "newPage TINYINT UNSIGNED NOT NULL DEFAULT 0,".
                        "level TINYINT UNSIGNED NOT NULL DEFAULT 0, INDEX(level),".
                        "ranking TINYINT UNSIGNED NOT NULL DEFAULT 1, INDEX(ranking),".
                        "menuType VARCHAR(10) NOT NULL DEFAULT 'MAIN',".    //  MAIN || SETTINGS
                        "icon VARCHAR(255) NOT NULL DEFAULT '', ".
                        "urlLink VARCHAR(255) NOT NULL,".
                        "menuDescr VARCHAR(255) NOT NULL)");

            //////////////////////////////////////////////////
            //  MAIN MENU ITEMS
            //////////////////////////////////////////////////
            $menuType = "MAIN";
            $ranking = 0;
            //////////////////////////////////////////////////
            //  LEVEL 0
            $level = 0;

            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (0,".$level.",".$ranking++.",'".$menuType."','bookings.php','Bookings')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (0,".$level.",".$ranking++.",'".$menuType."','','Projects')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (0,".$level.",".$ranking++.",'".$menuType."','','Approval')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (0,".$level.",".$ranking++.",'".$menuType."','','Finances')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (0,".$level.",".$ranking++.",'".$menuType."','','Reports')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (0,".$level.",".$ranking++.",'".$menuType."','','Admin')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (0,".$level.",".$ranking++.",'".$menuType."','','On-Track Admin')");

            //////////////////////////////////////////////////
            //  LEVEL 1
            $level++;

            $parentID = q("SELECT id FROM menus WHERE urlLink = 'bookings.php' AND menuDescr = 'Bookings'");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','book_time.php','Time')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','book_expense.php','Expenses')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','bookings.php','Time/Expenses')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','gpslogbook_trip_management.php','Trip Management')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','book_leave.php','Leave')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','book_resource.php','Shared Resources')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','booking_management.php','Manage Bookings')");
            //$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_inv_vs_ctc_own.php','Report Invoice Potential vs Cost to Company')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_inv_vs_ctc_own_v2.php','Report Invoice Potential vs Cost to Company')");

            $parentID = q("SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Projects'");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','sp_projects.php','Shared Projects')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','projects.php','Company Projects')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','sla.php','SLA')");

            $parentID = q("SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Approval'");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','_module_handler.php?module=approval&what=display&level=SP','Shared Projects')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','_module_handler.php?module=approval&what=display&level=CP','Company Projects')");

            $parentID = q("SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Finances'");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','budget_assignment.php','Running Cost')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','invoice_load.php','Invoices')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','expense_planning.php','Expenses')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','clients.php','Clients')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','blankPage.php','Statements')");

            $parentID = q("SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Reports'");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report.php','Personal Time/Expenses')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_time_sheet.php','Project')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_sp_time_sheet.php','Shared Project')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_cash_flow_forecast.php','Business')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','','Approvals')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_leave.php','Leave')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_logbook.php','Log Book')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_company_breakdown.php','Administrative')");

            $parentID = q("SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Admin'");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','management.php','Time/Expense Management')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','','Vehicles')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','employees.php?userType=all','Employees')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','printing_management.php','Printing')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','locations.php','Locations')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','resources.php','Resources')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','role_manager.php','Role Management')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','role_assignment.php','Role/Action Assignment')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','logs.php','Logs')");

            $parentID = q("SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'On-Track Admin'");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','companies.php','Companies')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','modules.php','Module Management')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','action_manager.php','Action Management')");
            //$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','news_add.php','News')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','company_assignment.php','Privilege Assignment')");

            //////////////////////////////////////////////////
            //  LEVEL 2
            $level++;

            $parentID = q("SELECT id FROM menus WHERE urlLink = 'sp_projects.php' AND menuDescr = 'Shared Projects' AND parentID = (SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Projects' AND level = ".($level - 2).")");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','sp_areas.php','Area and Type Management')");

            $parentID = q("SELECT id FROM menus WHERE urlLink = 'projects.php' AND menuDescr = 'Company Projects' AND parentID = (SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Projects' AND level = ".($level - 2).")");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','projects.php','Manage Projects')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','areas.php','Area and Type Management')");

            $parentID = q("SELECT id FROM menus WHERE urlLink = 'budget_assignment.php' AND menuDescr = 'Running Cost' AND parentID = (SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Finances' AND level = ".($level - 2).")");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','budget_assignment.php','Management')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','categories.php','Definition')");

            $parentID = q("SELECT id FROM menus WHERE urlLink = 'invoice_load.php' AND menuDescr = 'Invoices' AND parentID = (SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Finances' AND level = ".($level - 2).")");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','invoice_load.php','Manage')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','invoice_loadNEW.php','Manage [NEW]')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','invoice_processing.php','Processing')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','invoice_payment_certificate.php','Invoice & Payment Certificate')");

            $parentID = q("SELECT id FROM menus WHERE urlLink = '_module_handler.php?module=approval&what=display&level=SP' AND menuDescr = 'Shared Projects' AND parentID = (SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Approval' AND level = ".($level - 2).")");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','_module_handler.php?module=approval&what=display&level=SP','Approve')");

            $parentID = q("SELECT id FROM menus WHERE urlLink = '_module_handler.php?module=approval&what=display&level=CP' AND menuDescr = 'Company Projects' AND parentID = (SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Approval' AND level = ".($level - 2).")");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','_module_handler.php?module=approval&what=display&level=CP','Approve')");

            $parentID = q("SELECT id FROM menus WHERE urlLink = 'blankPage.php' AND menuDescr = 'Statements' AND parentID = (SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Finances' AND level = ".($level - 2).")");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','statements.php?link=INVOICE','Link')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','statements_unlink.php','Unlink')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','statements_update.php','Edit')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','statements_import.php','Import')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','account_manage.php','Bank Account(s)')");

            $parentID = q("SELECT id FROM menus WHERE urlLink = 'report_time_sheet.php' AND menuDescr = 'Project' AND parentID = (SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Reports' AND level = ".($level - 2).")");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_time_sheet.php','Time Sheet')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_expense_sheet.php','Expense Sheet')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_time_expense.php','Time & Expenses')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_active_projects.php','Project List')");
            //$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_over_budget_projects.php','Projects Over Budget')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_over_budget_projects_consulting.php','Projects Over Budget (Consulting)')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_project_breakdown.php','Consulting Breakdown')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_project_breakdown_all.php','Project Breakdown (Full)')");
            //$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_project.php','Per Project')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_project_list.php','User/Project List')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_progress.php','Progress')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_work_in_process.php','Work In Process')");
            //$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_gantt.php','Gantt')");
            //$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','invoice_create.php','Invoice Info')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_summary_time.php','Summaries')");

            $parentID = q("SELECT id FROM menus WHERE urlLink = 'report_sp_time_sheet.php' AND menuDescr = 'Shared Project' AND parentID = (SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Reports' AND level = ".($level - 2).")");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_sp_time_sheet.php','Time Sheet')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_sp_expense_sheet.php','Expense Sheet')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_sp_project_breakdown.php','Consulting Breakdown')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_sp_invoice.php','Invoices')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_sp_summary_time.php','Summaries')");

            $parentID = q("SELECT id FROM menus WHERE urlLink = 'report_cash_flow_forecast.php' AND menuDescr = 'Business' AND parentID = (SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Reports' AND level = ".($level - 2).")");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_cash_flow_forecast.php','Cash Flow Forecast')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_cash_flow_forecast2.php','Cash Flow Forecast V2')");
            //$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_invoiced_vs_cost_to_company.php','Invoice Potential vs Cost to Company')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_invoiced_vs_cost_to_company_v2.php','Invoice Potential vs Cost to Company')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_income_forecast_time.php','Income Forecast')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_growth_analysis.php','Growth Analysis')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_budget.php','Running Cost Report')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_budget_breakdown.php','Running Cost Breakdown')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_analysis.php','Business Analysis')");
            //$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_running_profit.php','Running Profit')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_time_comparison.php','Time Comparison')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_project_type_comparison.php','Project Type Comparison')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_cumulative_income.php','Cumulative Income')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_budget_period.php','Running Cost vs Invoiced')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_loaded_invoices.php','Loaded Invoices')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_invoice_tracking.php','Invoice Tracking')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_statements.php','Linked Statements')");

            $parentID = q("SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Approvals' AND parentID = (SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Reports' AND level = ".($level - 2).")");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','_module_handler.php?module=approval&what=report&level=SP','Shared Projects')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','_module_handler.php?module=approval&what=report&level=CP','Company Projects')");

            $parentID = q("SELECT id FROM menus WHERE urlLink = 'report_company_breakdown.php' AND menuDescr = 'Administrative' AND parentID = (SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Reports' AND level = ".($level - 2).")");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_company_breakdown.php','Company Breakdown')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_company_summary.php','Company Summary')");

            $parentID = q("SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Vehicles' AND parentID = (SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Admin' AND level = ".($level - 2).")");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','vehicles.php','Add/Edit Vehicles')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','vehicle_assignment.php','Vehicle Assignment')");

            $parentID = q("SELECT id FROM menus WHERE urlLink = 'role_assignment.php' AND menuDescr = 'Role/Action Assignment' AND parentID = (SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Admin' AND level = ".($level - 2).")");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','action_assignment.php','Action Assignment')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','role_assignment.php','Role Assignment')");

            $parentID = q("SELECT id FROM menus WHERE urlLink = 'company_assignment.php' AND menuDescr = 'Privilege Assignment' AND parentID = (SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'On-Track Admin' AND level = ".($level - 2).")");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','company_assignment.php','Company Assignment')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','resource_assignment.php','Shared Resource Assignment')");

            //////////////////////////////////////////////////
            //  LEVEL 3
            $level++;

            $parentID = q("SELECT id FROM menus WHERE urlLink = 'sp_areas.php' AND menuDescr = 'Area and Type Management' AND parentID = (SELECT id FROM menus WHERE urlLink = 'sp_projects.php' AND menuDescr = 'Shared Projects' AND level = ".($level - 2).")");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','sp_areas.php','Areas')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','sp_activityTypes.php','Activities')");

            $parentID = q("SELECT id FROM menus WHERE urlLink = 'statements.php?link=INVOICE' AND menuDescr = 'Link' AND parentID = (SELECT id FROM menus WHERE urlLink = 'blankPage.php' AND menuDescr = 'Statements' AND level = ".($level - 2).")");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','statements.php?link=BUDGET','Running Cost')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','statements.php?link=INVOICE','Invoices')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','statements.php?link=EXPENSE','Expenses')");

            $parentID = q("SELECT id FROM menus WHERE urlLink = 'areas.php' AND menuDescr = 'Area and Type Management' AND parentID = (SELECT id FROM menus WHERE urlLink = 'projects.php' AND menuDescr = 'Company Projects' AND level = ".($level - 2).")");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','areas.php','Areas')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','activityTypes.php','Activities')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','project_type_management.php','Project Types')");

            $parentID = q("SELECT id FROM menus WHERE urlLink = 'report_sp_summary_time.php' AND menuDescr = 'Summaries' AND parentID = (SELECT id FROM menus WHERE urlLink = 'report_sp_time_sheet.php' AND menuDescr = 'Shared Project' AND level = ".($level - 2).")");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_sp_summary_time.php','Time')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_sp_summary_expenses.php','Expenses')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_sp_summary.php','Time & Expenses')");

            $parentID = q("SELECT id FROM menus WHERE urlLink = 'report_summary_time.php' AND menuDescr = 'Summaries' AND parentID = (SELECT id FROM menus WHERE urlLink = 'report_time_sheet.php' AND menuDescr = 'Project' AND level = ".($level - 2).")");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_summary_time.php','Time')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_summary_expenses.php','Expenses')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_summary.php','Time & Expenses')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_project_info.php','Project Overview')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_summary_type.php','Type Summary')");

            $parentID = q("SELECT id FROM menus WHERE urlLink = 'report_work_in_process.php' AND menuDescr = 'Work In Process' AND parentID = (SELECT id FROM menus WHERE urlLink = 'report_time_sheet.php' AND menuDescr = 'Project' AND level = ".($level - 2).")");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_work_in_process.php','Project Lifetime')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_work_in_process_months.php','Past 6 Months')");

            $parentID = q("SELECT id FROM menus WHERE urlLink = 'report_income_forecast_time.php' AND menuDescr = 'Income Forecast' AND parentID = (SELECT id FROM menus WHERE urlLink = 'report_cash_flow_forecast.php' AND menuDescr = 'Business' AND level = ".($level - 2).")");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_income_forecast_time.php','Time Based')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_income_forecast_invoice.php','Invoice Based')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_income_forecast.php','Summary (Average)')");

            //////////////////////////////////////////////////
            //  SETTINGS MENU ITEMS
            //////////////////////////////////////////////////
            $menuType = "SETTINGS";
            $ranking = 0;
            $level = 0;

            $insert = q("INSERT INTO menus (parentID,modal,level,ranking,menuType,urlLink,menuDescr) VALUES (0,1,".$level.",".$ranking++.",'".$menuType."','modal_profile','Profile Settings')");
            //$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (0,".$level.",".$ranking++.",'".$menuType."','change_password.php','Change Password')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (0,".$level.",".$ranking++.",'".$menuType."','notifications.php','Notifications')");
            //$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (0,".$level.",".$ranking++.",'".$menuType."','','Themes')");
            $insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (0,".$level.",".$ranking++.",'".$menuType."','eula.php','EULA')");
            //////////////////////////////////////////////////

            $drop = q("DROP TABLE IF EXISTS menuAction");
            $create = q("CREATE TABLE menuAction (".
                        "id MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,".
                        "menuID MEDIUMINT UNSIGNED NOT NULL, INDEX(menuID),".
                        "actionID MEDIUMINT UNSIGNED NOT NULL, INDEX(actionID))");

            //////////////////////////////////////////////////
            //  ACCESS FOR EACH MENU ITEM
            //////////////////////////////////////////////////
            $actions = array("TIME_MANAGE","EXPENSE_MANAGE_OWN","BOOKING_MANAGE","LEAVE_MANAGE_OWN","SHAREDRES_BOOK","LOGS_VIEW_OWN","EMP_EDIT","EMP_ADD_REMOVE","TIME_MANAGE_ALL",
                                "EXPENSE_MANAGE_ALL","EMP_CTC_MANAGE","EMP_TARIFF_MANAGE","PROJ_MANAGE","VEH_MANAGE","SLA_MANAGE","ATYPE_MANAGE","AREA_MANAGE","PROJTYPE_MANAGE",
                                "PRINTING_MANAGE","BUDGET_CAT_MANAGE","BUDGET_MANAGE","BUDGET_VIEW","INVOICE_MANAGE","INVOICE_VIEW","EXPENSE_MANAGE","EXPENSE_VIEW","STATEMENT_MAN",
                                "LOGS_VIEW_ALL","APPROVAL_MANAGE","SPM_MANAGE","LEAVE_MANAGE_ALL","RA_MANAGEMENT","REP_TIME_EX","REP_ACTIVE_PROJ","REP_ANALYSIS","REP_STATEMENT",
                                "REP_BUDGET","REP_BUDGET_PERIOD","REP_CASH_FLOW","REP_CASH_FLOW2","REP_COMPANY_BREAK","REP_INVOICE_TRACK","REP_COMPANY_SUMM","REP_EX_SHEET","REP_GANTT",
                                "REP_GROWTH_ANALYSIS","REP_INCOME_FORECAST","REP_IN_FORE_INVOICE","REP_IN_FORE_TIME","REP_PROJ_BREAK","REP_IN_VS_CTC","REP_LEAVE",
                                "REP_LOADED_INV","REP_PROGRESS","REP_TYPE_SUM","REP_PROJ_INFO","REP_PROJ_LIST","REP_PROJ_TYPE_COM","REP_RUNNING_PROF","REP_SUMMARY",
                                "REP_SUMMARY_EX","REP_SUMMARY_TIME","REP_TIME_COM","REP_TIME_PROJ","REP_TIME_SHEET","REP_WORK_IN_PR","REP_WORK_IN_PR_M","CHNG_PW","EULA",
                                "REP_SP_TIME_SHEET","REP_SP_EX_SHEET","REP_SP_SUMMARY_TIME","REP_SP_SUMMARY_EX","REP_SP_SUMMARY","REP_SP_BREAK","REP_SP_INV",
                                "COMP_MAN","RESOURCE_MAN","MODULE_MAN","ACTION_MAN","ROLE_MAN","NEWS_MAN","COMP_ASSIGN","ACTION_ASSIGN","ROLE_ASSIGN","RESOURCE_ASSIGN","INVOICE_MANAGE_NEW","INVOICE_VIEW_NEW",
                                "CLIENT_MANAGE","ACCOUNT_MANAGE","INVOICE_PROCESS","REP_INV_VS_CTC_OWN","REP_INV_VS_CTC_OWN_V2","REP_IN_VS_CTC_V2","REP_LOGBOOK","PAYMENT_CERTIFICATE",
								"REP_OVER_BUD_PROJ_CONS","VEHICLE_ASSIGN","LOCATION_MANAGEMENT","GPS_LOGBOOK_MANAGEMENT");

            sort($actions);

            if (is_array($actions))   {
                foreach ($actions as $action)  {
                    unset($menuItems);

                    switch($action)    {
                        case "GPS_LOGBOOK_MANAGEMENT":
                            $menuItems = array("0#bookings.php#Bookings","1#gpslogbook_trip_management.php#Trip Management#0#bookings.php#Bookings");
                            break;
                        case "TIME_MANAGE":
                            $menuItems = array("0#bookings.php#Bookings","1#book_time.php#Time#0#bookings.php#Bookings","1#bookings.php#Time/Expenses#0#bookings.php#Bookings");
                            break;
                        case "REP_INV_VS_CTC_OWN":
                            //$menuItems = array("0#bookings.php#Bookings","1#report_inv_vs_ctc_own.php#Report Invoice Potential vs Cost to Company#0#bookings.php#Bookings");
                            $menuItems = array("0#bookings.php#Bookings","1#report_inv_vs_ctc_own_v2.php#Report Invoice Potential vs Cost to Company#0#bookings.php#Bookings");
                            break;
                        case "REP_INV_VS_CTC_OWN_V2":
                            $menuItems = array("0#bookings.php#Bookings","1#report_inv_vs_ctc_own_v2.php#Report Invoice Potential vs Cost to Company#0#bookings.php#Bookings");
                            break;
                        case "EXPENSE_MANAGE_OWN":
                            $menuItems = array("0#bookings.php#Bookings","1#book_expense.php#Expenses#0#bookings.php#Bookings","1#bookings.php#Time/Expenses#0#bookings.php#Bookings");
                            break;
                        case "BOOKING_MANAGE":
                            $menuItems = array("0#bookings.php#Bookings","1#booking_management.php#Manage Bookings#0#bookings.php#Bookings");
                            break;
                        case "TIME_MANAGE_ALL":
                        case "EXPENSE_MANAGE_ALL":
                            $menuItems = array("0##Admin","1#management.php#Time/Expense Management#0##Admin");
                            break;
                        case "LEAVE_MANAGE_OWN":
                            $menuItems = array("0#bookings.php#Bookings","1#book_leave.php#Leave#0#bookings.php#Bookings");
                            break;
                        case "SHAREDRES_BOOK":
                            $menuItems = array("0#bookings.php#Bookings","1#book_resource.php#Shared Resources#0#bookings.php#Bookings");
                            break;
                        case "LOGS_VIEW_ALL":
                        case "LOGS_VIEW_OWN":
                            $menuItems = array("0##Admin","1#logs.php#Logs#0##Admin");
                            break;
                        case "EMP_EDIT":
                            $menuItems = array("0##Admin","1#employees.php?userType=all#Employees#0##Admin");
                            break;
                        case "EMP_ADD_REMOVE":
                        case "EMP_CTC_MANAGE":
                        case "EMP_TARIFF_MANAGE":
                            $menuItems = array("0##Admin","1#employees.php?userType=all#Employees#0##Admin");
                            break;
                        case "PROJ_MANAGE":
                            $menuItems = array("0##Projects","1#projects.php#Company Projects#0##Projects","2#projects.php#Manage Projects#1#projects.php#Company Projects#0##Projects");
                            break;
                        case "VEH_MANAGE":
                            $menuItems = array("0##Admin","1##Vehicles#0##Admin","2#vehicles.php#Add/Edit Vehicles#1##Vehicles#0##Admin");
                            break;
                        case "VEHICLE_ASSIGN":
                            $menuItems = array("0##Admin","1##Vehicles#0##Admin","2#vehicle_assignment.php#Vehicle Assignment#1##Vehicles#0##Admin");
                            break;
                        case "RESOURCE_MAN":
                            $menuItems = array("0##Admin","1#resources.php#Resources#0##Admin");
                            break;
                        case "ACTION_ASSIGN":
                            $menuItems = array("0##Admin","1#role_assignment.php#Role/Action Assignment#0##Admin",
                                                "2#action_assignment.php#Action Assignment#1#role_assignment.php#Role/Action Assignment#0##Admin");
                            break;
                        case "ROLE_ASSIGN":
                            $menuItems = array("0##Admin","1#role_assignment.php#Role/Action Assignment#0##Admin",
                                                "2#role_assignment.php#Role Assignment#1#role_assignment.php#Role/Action Assignment#0##Admin");
                            break;
                        case "ROLE_MAN":
                            $menuItems = array("0##Admin","1#role_manager.php#Role Management#0##Admin");
                            break;
                        case "SLA_MANAGE":
                            $menuItems = array("0##Projects","1#sla.php#SLA#0##Projects");
                            break;
                        case "ATYPE_MANAGE":
                            $menuItems = array("0##Projects","1#projects.php#Company Projects#0##Projects","2#projects.php#Manage Projects#1#projects.php#Company Projects#0##Projects",
                                                "2#areas.php#Area and Type Management#1#projects.php#Company Projects#0##Projects",
                                                "3#activityTypes.php#Activities#2#areas.php#Area and Type Management#1#projects.php#Company Projects");
                            break;
                        case "AREA_MANAGE":
                            $menuItems = array("0##Projects","1#projects.php#Company Projects#0##Projects","2#projects.php#Manage Projects#1#projects.php#Company Projects#0##Projects",
                                                "2#areas.php#Area and Type Management#1#projects.php#Company Projects#0##Projects",
                                                "3#areas.php#Areas#2#areas.php#Area and Type Management#1#projects.php#Company Projects");
                            break;
                        case "PROJTYPE_MANAGE":
                            $menuItems = array("0##Projects","1#projects.php#Company Projects#0##Projects","2#projects.php#Manage Projects#1#projects.php#Company Projects#0##Projects",
                                                "2#areas.php#Area and Type Management#1#projects.php#Company Projects#0##Projects",
                                                "3#project_type_management.php#Project Types#2#areas.php#Area and Type Management#1#projects.php#Company Projects");
                            break;
                        case "LOCATION_MANAGEMENT":
                            $menuItems = array("0##Admin","1#locations.php#Locations#0##Admin");
                            break;
                        case "PRINTING_MANAGE":
                            $menuItems = array("0##Admin","1#printing_management.php#Printing#0##Admin");
                            break;
                        case "BUDGET_CAT_MANAGE":
                            $menuItems = array("0##Finances","1#budget_assignment.php#Running Cost#0##Finances","2#categories.php#Definition#1#budget_assignment.php#Running Cost#0##Finances");
                            break;
                        case "BUDGET_MANAGE":
                            $menuItems = array("0##Finances","1#budget_assignment.php#Running Cost#0##Finances","2#budget_assignment.php#Management#1#budget_assignment.php#Running Cost#0##Finances",
                                                "0#notifications.php#Notifications");
                            break;
                        case "BUDGET_VIEW":
                            $menuItems = array("0##Finances","1#budget_assignment.php#Running Cost#0##Finances","2#budget_assignment.php#Management#1#budget_assignment.php#Running Cost#0##Finances");
                            break;
                        case "INVOICE_MANAGE":
                            $menuItems = array("0##Finances","1#invoice_load.php#Invoices#0##Finances","2#invoice_load.php#Manage#1#invoice_load.php#Invoices#0##Finances",
                                                "0##Reports","1#report_time_sheet.php#Project#0##Reports",
                                                "2#invoice_create.php#Invoice Info#1#report_time_sheet.php#Project#0##Reports","0#notifications.php#Notifications");
                            break;
                        case "INVOICE_PROCESS":
                            $menuItems = array("0##Finances","1#invoice_load.php#Invoices#0##Finances","2#invoice_processing.php#Processing#1#invoice_load.php#Invoices#0##Finances");
                            break;
                        case "PAYMENT_CERTIFICATE":
                            $menuItems = array("0##Finances","1#invoice_load.php#Invoices#0##Finances","2#invoice_payment_certificate.php#Invoice & Payment Certificate#1#invoice_load.php#Invoices#0##Finances");
                            break;
                        case "INVOICE_VIEW":
                            $menuItems = array("0##Finances","1#invoice_load.php#Invoices#0##Finances","2#invoice_load.php#Manage#1#invoice_load.php#Invoices#0##Finances");
                            break;
                        case "INVOICE_MANAGE_NEW":
                            $menuItems = array("0##Finances","1#invoice_load.php#Invoices#0##Finances","2#invoice_loadNEW.php#Manage [NEW]#1#invoice_load.php#Invoices#0##Finances",
                                                "0##Reports","1#report_time_sheet.php#Project#0##Reports",
                                                "2#invoice_create.php#Invoice Info#1#report_time_sheet.php#Project#0##Reports","0#notifications.php#Notifications");
                            break;
                        case "INVOICE_VIEW_NEW":
                            $menuItems = array("0##Finances","1#invoice_load.php#Invoices#0##Finances","2#invoice_loadNEW.php#Manage [NEW]#1#invoice_load.php#Invoices#0##Finances");
                            break;
                        case "CLIENT_MANAGE":
                            $menuItems = array("0##Finances","1#clients.php#Clients#0##Finances");
                            break;
                        case "ACCOUNT_MANAGE":
                            $menuItems = array("0##Finances","1#blankPage.php#Statements#0##Finances",
                                                "2#account_manage.php#Bank Account(s)#1#blankPage.php#Statements#0##Finances");
                            break;
                        case "STATEMENT_MAN":
                            $menuItems = array("0##Finances","1#blankPage.php#Statements#0##Finances",
                                                "2#statements.php?link=INVOICE#Link#1#blankPage.php#Statements#0##Finances",
                                                "2#statements_unlink.php#Unlink#1#blankPage.php#Statements#0##Finances",
                                                "2#statements_update.php#Edit#1#blankPage.php#Statements#0##Finances",
                                                "2#statements_import.php#Import#1#blankPage.php#Statements#0##Finances",
                                                "3#statements.php?link=BUDGET#Running Cost#2#statements.php?link=INVOICE#Link#1#blankPage.php#Statements#0##Finances",
                                                "3#statements.php?link=INVOICE#Invoices#2#statements.php?link=INVOICE#Link#1#blankPage.php#Statements#0##Finances",
                                                "3#statements.php?link=EXPENSE#Expenses#2#statements.php?link=INVOICE#Link#1#blankPage.php#Statements#0##Finances");
                            break;
                        case "REP_STATEMENT":
                            $menuItems = array("0##Reports","1#report_cash_flow_forecast.php#Business#0##Reports",
                                                "2#report_statements.php#Linked Statements#1#report_cash_flow_forecast.php#Business#0##Reports");
                            break;
                        case "EXPENSE_MANAGE":
                            $menuItems = array("0##Finances","1#expense_planning.php#Expenses#0##Finances","0#notifications.php#Notifications");
                            break;
                        case "EXPENSE_VIEW":
                            $menuItems = array("0##Finances","1#expense_planning.php#Expenses#0##Finances");
                            break;
                        case "APPROVAL_MANAGE":
                            $menuItems = array("0##Approval","0##Reports","1#_module_handler.php?module=approval&what=display&level=CP#Company Projects#0##Approval",
                                                "1##Approvals#0##Reports",
                                                "2#_module_handler.php?module=approval&what=display&level=CP#Approve#1#_module_handler.php?module=approval&what=display&level=CP#Company Projects#0##Approval",
                                                "2#_module_handler.php?module=approval&what=report&level=CP#Company Projects#1##Approvals#0##Reports",
                                                "0#notifications.php#Notifications");
                            break;
                        case "SPM_MANAGE":
                            $menuItems = array("0##Projects","0##Approval","0##Reports","1#sp_projects.php#Shared Projects#0##Projects","1#_module_handler.php?module=approval&what=display&level=SP#Shared Projects#0##Approval",
                                                "1#report_time_sheet.php#Project#0##Reports","1##Approvals#0##Reports",
                                                "2#sp_areas.php#Area and Type Management#1#sp_projects.php#Shared Projects#0##Projects",
                                                "2#_module_handler.php?module=approval&what=display&level=SP#Approve#1#_module_handler.php?module=approval&what=display&level=SP#Shared Projects#0##Approval",
                                                "2#report_project_list.php#User/Project List#1#report_time_sheet.php#Project#0##Reports",
                                                "2#_module_handler.php?module=approval&what=report&level=SP#Shared Projects#1##Approvals#0##Reports",
                                                "3#sp_areas.php#Areas#2#sp_areas.php#Area and Type Management#1#sp_projects.php#Shared Projects",
                                                "3#sp_activityTypes.php#Activities#2#sp_areas.php#Area and Type Management#1#sp_projects.php#Shared Projects");
                            break;
                        case "REP_TIME_EX":
                            $menuItems = array("0##Reports","1#report_time_sheet.php#Project#0##Reports",
                                                "2#report_time_expense.php#Time & Expenses#1#report_time_sheet.php#Project#0##Reports");
                            break;
                        case "REP_ACTIVE_PROJ":
                            $menuItems = array("0##Reports","1#report_time_sheet.php#Project#0##Reports",
                                                "2#report_active_projects.php#Project List#1#report_time_sheet.php#Project#0##Reports"/*,
                                                "2#report_over_budget_projects.php#Projects Over Budget#1#report_time_sheet.php#Project#0##Reports"*/);
                            break;
						case "REP_OVER_BUD_PROJ_CONS":
                            $menuItems = array("0##Reports","1#report_time_sheet.php#Project#0##Reports",
                                                "2#report_over_budget_projects_consulting.php#Projects Over Budget (Consulting)#1#report_time_sheet.php#Project#0##Reports");
                            break;
                        case "REP_BUDGET":
                            $menuItems = array("0##Reports","1#report_cash_flow_forecast.php#Business#0##Reports",
                                                "2#report_budget.php#Running Cost Report#1#report_cash_flow_forecast.php#Business#0##Reports",
                                                "2#report_budget_breakdown.php#Running Cost Breakdown#1#report_cash_flow_forecast.php#Business#0##Reports",
                                                "2#report_budget_period.php#Running Cost vs Invoiced#1#report_cash_flow_forecast.php#Business#0##Reports");
                            break;
                        case "REP_BUDGET_PERIOD":
                            $menuItems = array("0##Reports","1#report_cash_flow_forecast.php#Business#0##Reports",
                                                "2#report_budget_period.php#Running Cost vs Invoiced#1#report_cash_flow_forecast.php#Business#0##Reports");
                            break;
                        case "REP_CASH_FLOW":
                            $menuItems = array("0##Reports","1#report_cash_flow_forecast.php#Business#0##Reports",
                                                "2#report_cash_flow_forecast.php#Cash Flow Forecast#1#report_cash_flow_forecast.php#Business#0##Reports");
                            break;
                        case "REP_CASH_FLOW2":
                            $menuItems = array("0##Reports","1#report_cash_flow_forecast.php#Business#0##Reports",
                                                "2#report_cash_flow_forecast2.php#Cash Flow Forecast V2#1#report_cash_flow_forecast.php#Business#0##Reports");
                            break;
                        case "REP_COMPANY_BREAK":
                            $menuItems = array("0##Reports","1#report_company_breakdown.php#Administrative#0##Reports",
                                                "2#report_company_breakdown.php#Company Breakdown#1#report_company_breakdown.php#Administrative#0##Reports");
                            break;
                        case "REP_INVOICE_TRACK":
                            $menuItems = array("0##Reports","1#report_cash_flow_forecast.php#Business#0##Reports",
                                                "2#report_invoice_tracking.php#Invoice Tracking#1#report_cash_flow_forecast.php#Business#0##Reports");
                            break;
                        case "REP_COMPANY_SUMM":
                            $menuItems = array("0##Reports","1#report_company_breakdown.php#Administrative#0##Reports",
                                                "2#report_company_summary.php#Company Summary#1#report_company_breakdown.php#Administrative#0##Reports");
                            break;
                        case "REP_EX_SHEET":
                            $menuItems = array("0##Reports","1#report_time_sheet.php#Project#0##Reports",
                                                "2#report_expense_sheet.php#Expense Sheet#1#report_time_sheet.php#Project#0##Reports");
                            break;
                        case "REP_GANTT":
                            $menuItems = array("0##Reports","1#report_time_sheet.php#Project#0##Reports",
                                                "2#report_gantt.php#Gantt#1#report_time_sheet.php#Project#0##Reports");
                            break;
                        case "REP_GROWTH_ANALYSIS":
                            $menuItems = array("0##Reports","1#report_cash_flow_forecast.php#Business#0##Reports",
                                                "2#report_growth_analysis.php#Growth Analysis#1#report_cash_flow_forecast.php#Business#0##Reports");
                            break;
                        case "REP_INCOME_FORECAST":
                            $menuItems = array("0##Reports","1#report_cash_flow_forecast.php#Business#0##Reports",
                                                "2#report_income_forecast_time.php#Income Forecast#1#report_cash_flow_forecast.php#Business#0##Reports",
                                                "3#report_income_forecast.php#Summary (Average)#2#report_income_forecast_time.php#Income Forecast#1#report_cash_flow_forecast.php#Business");
                            break;
                        case "REP_IN_FORE_INVOICE":
                            $menuItems = array("0##Reports","1#report_cash_flow_forecast.php#Business#0##Reports",
                                                "2#report_income_forecast_time.php#Income Forecast#1#report_cash_flow_forecast.php#Business#0##Reports",
                                                "3#report_income_forecast_invoice.php#Invoice Based#2#report_income_forecast_time.php#Income Forecast#1#report_cash_flow_forecast.php#Business");
                            break;
                        case "REP_IN_FORE_TIME":
                            $menuItems = array("0##Reports","1#report_cash_flow_forecast.php#Business#0##Reports",
                                                "2#report_income_forecast_time.php#Income Forecast#1#report_cash_flow_forecast.php#Business#0##Reports",
                                                "3#report_income_forecast_time.php#Time Based#2#report_income_forecast_time.php#Income Forecast#1#report_cash_flow_forecast.php#Business");
                            break;
                        case "REP_PROJ_BREAK":
                            $menuItems = array("0##Reports","1#report_time_sheet.php#Project#0##Reports",
                                                "2#report_project_breakdown.php#Consulting Breakdown#1#report_time_sheet.php#Project#0##Reports",
                                                "2#report_project_breakdown_all.php#Project Breakdown (Full)#1#report_time_sheet.php#Project#0##Reports");
                            break;
                        case "REP_IN_VS_CTC":
                            /*
							$menuItems = array("0##Reports","1#report_cash_flow_forecast.php#Business#0##Reports",
                                                "2#report_invoiced_vs_cost_to_company.php#Invoice Potential vs Cost to Company#1#report_cash_flow_forecast.php#Business#0##Reports");
							*/
                            $menuItems = array("0##Reports","1#report_cash_flow_forecast.php#Business#0##Reports",
                                                "2#report_invoiced_vs_cost_to_company_v2.php#Invoice Potential vs Cost to Company#1#report_cash_flow_forecast.php#Business#0##Reports");
                            break;
                        case "REP_IN_VS_CTC_V2":
                            $menuItems = array("0##Reports","1#report_cash_flow_forecast.php#Business#0##Reports",
                                                "2#report_invoiced_vs_cost_to_company_v2.php#Invoice Potential vs Cost to Company#1#report_cash_flow_forecast.php#Business#0##Reports");
                            break;
                        case "REP_LEAVE":
                            $menuItems = array("0##Reports","1#report_leave.php#Leave#0##Reports");
                            break;
                        case "REP_LOGBOOK":
                            $menuItems = array("0##Reports","1#report_logbook.php#Log Book#0##Reports");
                            break;
                        case "REP_LOADED_INV":
                            $menuItems = array("0##Reports","1#report_cash_flow_forecast.php#Business#0##Reports",
                                                "2#report_loaded_invoices.php#Loaded Invoices#1#report_cash_flow_forecast.php#Business#0##Reports");
                            break;
                        case "REP_PROGRESS":
                            $menuItems = array("0##Reports","1#report_time_sheet.php#Project#0##Reports",
                                                "2#report_progress.php#Progress#1#report_time_sheet.php#Project#0##Reports");
                            break;
                        case "REP_TYPE_SUM":
                            $menuItems = array("0##Reports","1#report_time_sheet.php#Project#0##Reports",
                                                "2#report_summary_time.php#Summaries#1#report_time_sheet.php#Project#0##Reports",
                                                "3#report_summary_type.php#Type Summary#2#report_summary_time.php#Summaries#1#report_time_sheet.php#Project");
                            break;
                        case "REP_PROJ_INFO":
                            $menuItems = array("0##Reports","1#report_time_sheet.php#Project#0##Reports",
                                                "2#report_summary_time.php#Summaries#1#report_time_sheet.php#Project#0##Reports",
                                                "3#report_project_info.php#Project Overview#2#report_summary_time.php#Summaries#1#report_time_sheet.php#Project");
                            break;
                        case "REP_PROJ_LIST":
                            $menuItems = array("0##Reports","1#report_time_sheet.php#Project#0##Reports",
                                                "2#report_project_list.php#User/Project List#1#report_time_sheet.php#Project#0##Reports");
                            break;
                        case "REP_PROJ_TYPE_COM":
                            $menuItems = array("0##Reports","1#report_cash_flow_forecast.php#Business#0##Reports",
                                                "2#report_project_type_comparison.php#Project Type Comparison#1#report_cash_flow_forecast.php#Business#0##Reports",
                                                "2#report_cumulative_income.php#Cumulative Income#1#report_cash_flow_forecast.php#Business#0##Reports");
                            break;
                        case "REP_ANALYSIS":
                            $menuItems = array("0##Reports","1#report_cash_flow_forecast.php#Business#0##Reports",
                                                "2#report_analysis.php#Business Analysis#1#report_cash_flow_forecast.php#Business#0##Reports");
                            break;
                        case "REP_RUNNING_PROF":
                            $menuItems = array("0##Reports","1#report_cash_flow_forecast.php#Business#0##Reports",
                                                "2#report_running_profit.php#Running Profit#1#report_cash_flow_forecast.php#Business#0##Reports");
                            break;
                        case "REP_SUMMARY":
                            $menuItems = array("0##Reports","1#report_time_sheet.php#Project#0##Reports",
                                                "2#report_summary_time.php#Summaries#1#report_time_sheet.php#Project#0##Reports",
                                                "3#report_summary.php#Time & Expenses#2#report_summary_time.php#Summaries#1#report_time_sheet.php#Project");
                            break;
                        case "REP_SUMMARY_EX":
                            $menuItems = array("0##Reports","1#report_time_sheet.php#Project#0##Reports",
                                                "1#report_summary_time.php#Summaries#1#report_time_sheet.php#Project#0##Reports",
                                                "3#report_summary_expenses.php#Expenses#2#report_summary_time.php#Summaries#1#report_time_sheet.php#Project");
                            break;
                        case "REP_SUMMARY_TIME":
                            $menuItems = array("0##Reports","1#report_time_sheet.php#Project#0##Reports",
                                                "2#report_summary_time.php#Summaries#1#report_time_sheet.php#Project#0##Reports",
                                                "3#report_summary_time.php#Time#2#report_summary_time.php#Summaries#1#report_time_sheet.php#Project");
                            break;
                        case "REP_TIME_COM":
                            $menuItems = array("0##Reports","1#report_cash_flow_forecast.php#Business#0##Reports",
                                                "2#report_time_comparison.php#Time Comparison#1#report_cash_flow_forecast.php#Business#0##Reports");
                            break;
                        case "REP_TIME_SHEET":
                            $menuItems = array("0##Reports","1#report_time_sheet.php#Project#0##Reports",
                                                "2#report_time_sheet.php#Time Sheet#1#report_time_sheet.php#Project#0##Reports");
                            break;
                        case "REP_WORK_IN_PR":
                            $menuItems = array("0##Reports","1#report_time_sheet.php#Project#0##Reports",
                                                "2#report_work_in_process.php#Work In Process#1#report_time_sheet.php#Project#0##Reports",
                                                "3#report_work_in_process.php#Project Lifetime#2#report_work_in_process.php#Work In Process#1#report_time_sheet.php#Project");
                            break;
                        case "REP_WORK_IN_PR_M":
                            $menuItems = array("0##Reports","1#report_time_sheet.php#Project#0##Reports",
                                                "2#report_work_in_process.php#Work In Process#1#report_time_sheet.php#Project#0##Reports",
                                                "3#report_work_in_process_months.php#Past 6 Months#2#report_work_in_process.php#Work In Process#1#report_time_sheet.php#Project");
                            break;
                        case "CHNG_PW":
                            $menuItems = array("0#modal_profile#Profile Settings");
                            break;
                        case "EULA":
                            $menuItems = array("0#eula.php#EULA");
                            break;

                        case "REP_SP_TIME_SHEET":
                            $menuItems = array("0##Reports","1#report_sp_time_sheet.php#Shared Project#0##Reports",
                                                "2#report_sp_time_sheet.php#Time Sheet#1#report_sp_time_sheet.php#Shared Project#0##Reports");
                            break;
                        case "REP_SP_EX_SHEET":
                            $menuItems = array("0##Reports","1#report_sp_time_sheet.php#Shared Project#0##Reports",
                                                "2#report_sp_expense_sheet.php#Expense Sheet#1#report_sp_time_sheet.php#Shared Project#0##Reports");
                            break;
                        case "REP_SP_SUMMARY_TIME":
                            $menuItems = array("0##Reports","1#report_sp_time_sheet.php#Shared Project#0##Reports",
                                                "2#report_sp_summary_time.php#Summaries#1#report_sp_time_sheet.php#Shared Project#0##Reports",
                                                "3#report_sp_summary_time.php#Time#2#report_sp_summary_time.php#Summaries#1#report_sp_time_sheet.php#Shared Project");
                            break;
                        case "REP_SP_SUMMARY_EX":
                            $menuItems = array("0##Reports","1#report_sp_time_sheet.php#Shared Project#0##Reports",
                                                "1#report_sp_summary_time.php#Summaries#1#report_sp_time_sheet.php#Shared Project#0##Reports",
                                                "3#report_sp_summary_expenses.php#Expenses#2#report_sp_summary_time.php#Summaries#1#report_sp_time_sheet.php#Shared Project");
                            break;
                        case "REP_SP_SUMMARY":
                            $menuItems = array("0##Reports","1#report_sp_time_sheet.php#Shared Project#0##Reports",
                                                "2#report_sp_summary_time.php#Summaries#1#report_sp_time_sheet.php#Shared Project#0##Reports",
                                                "3#report_sp_summary.php#Time & Expenses#2#report_sp_summary_time.php#Summaries#1#report_sp_time_sheet.php#Shared Project");
                            break;
                        case "REP_SP_BREAK":
                            $menuItems = array("0##Reports","1#report_sp_time_sheet.php#Shared Project#0##Reports",
                                                "2#report_sp_project_breakdown.php#Consulting Breakdown#1#report_sp_time_sheet.php#Shared Project#0##Reports");
                            break;
                        case "REP_SP_INV":
                            $menuItems = array("0##Reports","1#report_sp_time_sheet.php#Shared Project#0##Reports",
                                                "2#report_sp_invoice.php#Invoices#1#report_sp_time_sheet.php#Shared Project#0##Reports");
                            break;
                        case "COMP_MAN":
                            $menuItems = array("0##On-Track Admin","1#companies.php#Companies#0##On-Track Admin");
                            break;
                        case "MODULE_MAN":
                            $menuItems = array("0##On-Track Admin","1#modules.php#Module Management#0##On-Track Admin");
                            break;
                        case "ACTION_MAN":
                            $menuItems = array("0##On-Track Admin","1#action_manager.php#Action Management#0##On-Track Admin");
                            break;
                        //case "NEWS_MAN":
                        //    $menuItems = array("0##On-Track Admin","1#news_add.php#News#0##On-Track Admin");
                        //    break;
                        case "COMP_ASSIGN":
                            $menuItems = array("0##On-Track Admin","1#company_assignment.php#Privilege Assignment#0##On-Track Admin",
                                                "2#company_assignment.php#Company Assignment#1#company_assignment.php#Privilege Assignment#0##On-Track Admin");
                            break;
                        case "RESOURCE_ASSIGN":
                            $menuItems = array("0##On-Track Admin","1#company_assignment.php#Privilege Assignment#0##On-Track Admin",
                                                "2#resource_assignment.php#Shared Resource Assignment#1#company_assignment.php#Privilege Assignment#0##On-Track Admin");
                            break;
                        default:
                            break;
                    }

                    if (is_array($menuItems))   {
                        foreach ($menuItems as $menuItem)  {
                            if ($menuItem != "")   {
                                $menuItem = preg_split("/#/", $menuItem);

                                $actionID = q("SELECT id FROM actions WHERE action = '".$action."'");

                                if ($menuItem[0] == 0)
                                    $menuID = q("SELECT id FROM menus WHERE level = '".$menuItem[0]."' AND urlLink = '".$menuItem[1]."' ".
                                                    "AND menuDescr = '".$menuItem[2]."'");
                                else if ($menuItem[0] > 1)
                                    $menuID = q("SELECT id FROM menus WHERE level = '".$menuItem[0]."' AND urlLink = '".$menuItem[1]."' ".
                                                "AND menuDescr = '".$menuItem[2]."' AND parentID = (SELECT id FROM menus WHERE level = ".$menuItem[3]." ".
                                                "AND urlLink = '".$menuItem[4]."' AND menuDescr = '".$menuItem[5]."' AND parentID = (SELECT id FROM menus ".
                                                    "WHERE level = ".$menuItem[6]." AND urlLink = '".$menuItem[7]."' AND menuDescr = '".$menuItem[8]."'))");
                                else
                                    $menuID = q("SELECT id FROM menus WHERE level = '".$menuItem[0]."' AND urlLink = '".$menuItem[1]."' ".
                                                "AND menuDescr = '".$menuItem[2]."' AND parentID = (SELECT id FROM menus WHERE level = ".$menuItem[3]." ".
                                                "AND urlLink = '".$menuItem[4]."' AND menuDescr = '".$menuItem[5]."')");

                                $insert = q("INSERT INTO menuAction (menuID, actionID) VALUES (".$menuID.", ".$actionID.")");
                            }
                        }
                    }
                }
            }
        ?>
        <form action="index.php" method="post">
            <center><input type="submit" value="Return"/></center>
        </form>
    </body>
</html>
