<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

	//if (!hasAccess("VEH_MANAGE"))
	//	header("Location: noaccess.php");


    $display_link                                                       = "contacts.php?id=".$_GET["id"]."&alphabet";

    //  Set Alphabet Letter
    $alphabet                                                           = "";
    $alphabet                                                           = $_GET["alphabet"];

    //  If Delete Contact Was Clicked
    if (isset($_POST["deleteBox"]) && $_POST["deleteBox"] === "1")
    {
        $id                                                             = $_POST["idBox"];
        $delete                                                         = $_POST["delete"];
        $contact_info                                                  = q("SELECT CONCAT(firstName,', ', lastName) FROM Contact WHERE id = '$id'");
        $setStatus                                                      = q("UPDATE Contact SET deleted = '$delete' WHERE id = '$id'");

        if ($setStatus) {
            $time                                                       = date("H:i:s");

            $logs                                                       = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('".$contact_info." deleted', 'Update', 'Contact', '".$_SESSION["email"]."', ".
                                                                            "'$today', '$time', '".$_SESSION["company_id"]."')");

            $errorMessage                                               = "Contact Deleted/Undeleted Successfully...";
        }
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0", "clients", "clients");

    if ($errorMessage != "")
    {
        echo "<div style='width:100%; text-align:center'><strong><font class='on-validate-error'>$errorMessage</font></strong></div>";
        echo "<br/>";
    }
?>
<script language="JavaScript">
    function deleted(id, del) {
        document.forms["contacts"].idBox.value                          = id;
        document.forms["contacts"].delete.value                         = del;
        document.forms["contacts"].deleteBox.value                      = 1;
        document.forms["contacts"].submit();
    }
</script>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata">
                <form action="" method="post" name="contacts">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>
                                   Contact List
                                </h6>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <br/>
                            </td>
                        </tr>
                    </table>
                    <?php
                        echo "<br/>";
                        echo "| <a href='$display_link='>View All Contacts</a> |";
                        echo "<br/><br/>";
                        echo "<a href='$display_link=A'>A</a> | <a href='$display_link=B'>B</a> | ".
                            "<a href='$display_link=C'>C</a> | <a href='$display_link=D'>D</a> | ".
                            "<a href='$display_link=E'>E</a> | <a href='$display_link=F'>F</a> | ".
                            "<a href='$display_link=G'>G</a> | <a href='$display_link=H'>H</a> | ".
                            "<a href='$display_link=I'>I</a> | <a href='$display_link=J'>J</a> | ".
                            "<a href='$display_link=K'>K</a> | <a href='$display_link=L'>L</a> | ".
                            "<a href='$display_link=M'>M</a> | <a href='$display_link=N'>N</a> | ".
                            "<a href='$display_link=O'>O</a> | <a href='$display_link=P'>P</a> | ".
                            "<a href='$display_link=Q'>Q</a> | <a href='$display_link=R'>R</a> | ".
                            "<a href='$display_link=S'>S</a> | <a href='$display_link=T'>T</a> | ".
                            "<a href='$display_link=U'>U</a> | <a href='$display_link=V'>V</a> | ".
                            "<a href='$display_link=W'>W</a> | <a href='$display_link=X'>X</a> | ".
                            "<a href='$display_link=Y'>Y</a> | <a href='$display_link=Z'>Z</a>";
                    ?>
                    <br/><br/>
                    <input name="btnAdd" onClick="location.href='contact_add.php<?php echo "?id=".$_GET["id"]; ?>';" tabindex="1" type="button" value="Add New Contact">
                    <br/><br/><br/>
                    <h6 style='font-size:14px;'>
                        <?php 
                            echo "Contacts for: <font style='color:orange'>";
                            echo q("SELECT companyName FROM Client WHERE id='".$_GET["id"]."' ");
                            echo "</font>";
                        ?>
                    </h6>
                    <table class="on-table-center on-table" width="50%">
                        <!--  Table Headings   -->
                        <tr>
                            <th width="50%">
                                Contact Name
                            </th>
                            <th>
                                Email Address
                            </th>
                            <th>
                                Delete/Undelete
                            </th>
                        </tr>
                        <!--  Table Information   -->
                        <?php
                        
                            $where = "";
                            
                            if($_GET["id"]) {
                                $where = " AND client_id = '".$_GET["id"]."' ";
                            }
                                //  nContacts                           = Number of Contacts
                                $nContacts                              = q("SELECT COUNT(id) FROM Contact WHERE firstName LIKE '$alphabet%' 
                                                                                    AND company_id = '".$_SESSION["company_id"]."' ".$where."");
                                $contacts                               = q("SELECT id, client_id, firstName, lastName, email, deleted FROM Contact WHERE firstName LIKE '$alphabet%' 
                                                                                    AND company_id = '".$_SESSION["company_id"]."' ".$where." ORDER BY firstName");

                            if ($nContacts > 1)
                            {
                                foreach ($contacts as $contacts)
                                {
                                    echo "<tr>";
                                        echo "<td><a href='contact_edit.php?id=".$contacts[0]."&cid=".$_GET["id"]."'>".$contacts[2].", ".$contacts[3]."</a></td>";
                                        echo "<td style='padding-left:30px;'>".$contacts[4]."</a></td>";
                                                //  Delete/Undelete Button
                                      echo "<td align='center'>";
                                                if ($contacts[5] === "0")
                                                    echo "<input name='btnDelete' onClick=\"deleted(".$contacts[0].", '1');\" type='button' value='Delete Contact     '>";
                                                else
                                                    echo "<input name='btnDelete' onClick=\"deleted(".$contacts[0].", '0');\" type='button' value='Undelete Contact'>";
                                            echo "</td>";
                                    echo "</tr>";
                                }
                            }
                            else if ($nContacts == 1)
                            {
                                echo "<tr>";
                                    echo "<td><a href='contact_edit.php?id=".$contacts[0][0]."&cid=".$_GET["id"]."'>".$contacts[0][2].", ".$contacts[0][3]."</a></td>";
                                    echo "<td style='padding-left:30px;'>".$contacts[0][4]."</a></td>";
                                        //  Delete/Undelete Button
                                        echo "<td class='centerdata'>";
                                            if ($contacts[0][5] === "0")
                                                echo "<input name='btnDelete' onClick=\"deleted(".$contacts[0][0].", '1');\" type='button' value='Delete Contact    '>";
                                            else
                                                echo "<input name='btnDelete' onClick=\"deleted(".$contacts[0][0].", '0');\" type='button' value='Undelete Contact'>";
                                        echo "</td>";
                                echo "</tr>";
                            }
                            else
                            {
                                echo "<tr>
                                            <td class='centerdata' colspan='100%'>";
                                        if ($alphabet == "")
                                            echo "No contacts available";
                                        else
                                            echo "No contacts available under $alphabet";
                                    echo "</td>";
                                echo "</tr>";
                            }
                            echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        ?>
                    </table>
                    <br/>
                    <input name="btnAdd" onClick="location.href='contact_add.php<?php echo "?id=".$_GET["id"]; ?>';" tabindex="4" type="button" value="Add New Contact">
                    <br><br><br>
                    <input name="btnAdd" onClick="location.href='clients.php';" tabindex="4" type="button" value="Return to Clients">
                    <input name="idBox" type="hidden" value="">
                    <input name="delete" type="hidden" value="">
                    <input name="deleteBox" type="hidden" value="">
                </form>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
