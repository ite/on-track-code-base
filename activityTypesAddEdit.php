<?php
    //TODO:  check if record exists
    //TODO:  check if update necessary
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

	if (!hasAccess("ATYPE_MANAGE"))
		header("Location: noaccess.php");

    function getATInfo($id) {
        unset($info);

        if ($id != "")  {
            $data = q("SELECT id,company_id,type FROM ActivityTypes WHERE id = '".$id."' LIMIT 1");

            if (is_array($data))
                $info = $data[0];

            $data = q("SELECT id,name,active FROM Activities WHERE parent_id = '".$id."' ORDER BY name");

            if (is_array($data))
                $info[] = $data;
        }
        else    {
            $info[] = (isset($_POST["id"])) ? addslashes(strip_tags($_POST["id"])) : "";
            $info[] = (isset($_POST["company_id"])) ? addslashes(strip_tags($_POST["company_id"])) : $_SESSION["company_id"];;
            $info[] = (isset($_POST["name"])) ? addslashes(strip_tags($_POST["name"])) : "";

            $cntr = (isset($_POST["activitiesTblCntr"])) ? addslashes(strip_tags($_POST["activitiesTblCntr"])) : 1;

            if ($cntr >= 1)  {
                $subInfo = array();

                for ($i = 1; $i < $cntr; $i++)  {
                    if (addslashes(strip_tags($_POST["name".$i])) != "")   {
                        unset($data);

                        $data[] = (isset($_POST["id".$i])) ? addslashes(strip_tags($_POST["id".$i])) : "";
                        $data[] = (isset($_POST["name".$i])) ? addslashes(strip_tags($_POST["name".$i])) : "";
                        $data[] = (isset($_POST["active".$i])) ? addslashes(strip_tags($_POST["active".$i])) : "";

                        $subInfo[] = $data;
                    }
                }
            }

            if (is_array($subInfo))
                $info[] = $subInfo;
        }

        return $info;
    }

    if (isset($_POST["save"]) && $_POST["save"] === "1")    {
        $errorMessage = "";

        $activityInfo = getATInfo("");
        $date = date("Y-m-d H:i:s");
        $user = $_SESSION["email"];
        $companyID = $_SESSION["company_id"];

        if ($activityInfo[0] == "")  {               //  DO INSERT
            $query = "INSERT INTO ActivityTypes (company_id,type,dateCreated,createdBy,dateUpdated,updatedBy) VALUES ";
            $values .= "('".$activityInfo[1]."','".$activityInfo[2]."','".$date."','".$user."','".$date."','".$user."')";

            $query .= $values;

            $insert = q($query);

            if ($insert)   {
                if (is_array($activityInfo[3]))   {
                    $activityTypeID = q("SELECT id FROM ActivityTypes WHERE company_id = '".$activityInfo[1]."' AND type = '".$activityInfo[2]."'");
                    $query = "INSERT INTO Activities (parent_id,name,dateCreated,createdBy,dateUpdated,updatedBy) VALUES ";
                    $values = "";

                    foreach ($activityInfo[3] as $a)    {
                        if ($a[1] != "")
                            $values .= "('".$activityTypeID."','".$a[1]."','".$date."','".$user."','".$date."','".$user."'),";
                    }

                    $values = substr($values, 0, -1);

                    $query .= $values;

                    $insert = q($query);
                }

                $logs = q("INSERT INTO Logs (what,access,on_table,by_user,on_date,on_time,company_id) VALUES ".
                            "('".$activityInfo[2]." created','Insert','sp_activityTypes','".$user."','".date("Y-m-d")."','".date("H:i:s")."','".$companyID."')");
            }

        }
        else if (is_numeric($activityInfo[0]))    {  //  DO UPDATE
            $query = "UPDATE ActivityTypes SET company_id = '".$activityInfo[1]."',type = '".$activityInfo[2]."',dateUpdated = '".$date."',updatedBy = '".$user."' ".
                        "WHERE id = '".$activityInfo[0]."'";

            $update = q($query);

            if ($update)   {
                if (is_array($activityInfo[3]))   {
                    $activityTypeID = $activityInfo[0];

                    foreach ($activityInfo[3] as $a)    {
                        if ($a[0] != "" && is_numeric($a[0]))   {
                            $active = ($a[2] == "") ? 0 : 1;
                            $query = "UPDATE Activities SET parent_id = '".$activityTypeID."',name = '".$a[1]."',active = '".$active."',".
                                        "dateUpdated = '".$date."',updatedBy = '".$user."' WHERE id = '".$a[0]."'";
                        }
                        else if ($a[1] != "")    {
                            $query = "INSERT INTO Activities (parent_id,name,dateCreated,createdBy,dateUpdated,updatedBy) VALUES ";
                            $values = "('".$activityTypeID."','".$a[1]."','".$date."','".$user."','".$date."','".$user."')";

                            $query .= $values;
                        }

                        $runQuery = q($query);
                    }
                }

                $logs = q("INSERT INTO Logs (what,access,on_table,by_user,on_date,on_time,company_id) VALUES ".
                            "('".$activityInfo[2]." updated','Update','activityTypes','".$user."','".date("Y-m-d")."','".date("H:i:s")."','".$companyID."')");
            }
        }

        header("Location: activityTypes.php");
    }
    else
        $activityInfo = getATInfo($_POST["id"]);

    $index = 1;

    if ($errorMessage != "")
        echo "<p align='center' style='padding:0px;'><strong><font class='on-validate-error'>$errorMessage</font></strong></p>";

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0");
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    jQuery(function()    {
        jQuery("#addRow").click(function()    {
            var tbl = "#"+jQuery(this).attr("href");
            var tblCntr = "#"+jQuery(this).attr("href")+"Cntr";

            var lstRow = jQuery(tbl + " tbody>tr:last");
            var counter = jQuery(tblCntr).val();
            var n = lstRow.clone(true);

            n.children().eq(0).children().eq(0).attr("id", "id"+counter);
            n.children().eq(0).children().eq(0).attr("name", "id"+counter);
            n.children().eq(0).children().eq(0).attr("value", "");
            n.children().eq(0).children().eq(1).attr("id", "name"+counter);
            n.children().eq(0).children().eq(1).attr("name", "name"+counter);
            n.children().eq(0).children().eq(1).attr("value", "");
            n.children().eq(0).children().eq(2).attr("id", "nameDiv"+counter);
            n.children().eq(0).children().eq(2).attr("name", "nameDiv"+counter);
            n.children().eq(0).children().eq(2).hide();

            n.insertAfter(lstRow);
            counter++;

            jQuery(tblCntr).val(counter);
        });

        jQuery("#btnAddEdit").click(function()    {
            var valid = true;
            var test;

            var fields = new Array("name");

            for (var i = 0; i < fields.length; i++)  {
                var field = fields[i];
                var cls = jQuery("#"+field).attr("class");

                cls = (cls != "") ? cls.split(" ") : "";

                if (cls != "")  {
                    for (j = 0; j < cls.length; j++)  {
                        var divName = (cls[j] == "entered") ? "#"+field+"Empty" : "#"+field+"Div";
                        test = validation(cls[j], jQuery("#"+field).val());

                        if (!test)  jQuery(divName).show();
                        else        jQuery(divName).hide();

                        valid &= test;
                    }
                }
            }

            var activities = jQuery("#activitiesTblCntr").val();

            for (var i = 1; i < activities; i++) {
                cls = jQuery("#name"+i).attr("class");
                cls = (cls != "") ? cls.split(" ") : "";

                if (cls != "")  {
                    for (j = 0; j < cls.length; j++)  {
                        var divName = (cls[j] == "entered") ? "#nameEmpty"+i : "#nameDiv"+i;
                        test = validation(cls[j], jQuery("#name"+i).val());

                        if (!test)  jQuery(divName).show();
                        else        jQuery(divName).hide();

                        valid &= test;
                    }
                }
            }

            if (valid) {
                jQuery("#save").val("1");
                jQuery("#content").submit();
            }
        });
    });
</script>
<table width="100%">
    <tr height="380px">
        <td class="centerdata">
        <form action="" method="post" id="content" name="content">
                <table width="100%">
                    <tr>
                        <td class="centerdata">
                            <h6><?php echo ($activityInfo[0] == "") ? "Add" : "Update"; ?> Activity Type &amp; Activities</h6>
                        </td>
                    </tr>
                    <tr>
                        <td class="centerdata">
                            <br/>
                        </td>
                    </tr>
                </table>
                <table class="on-table-center">
                        <tr>
                            <td class="on-description-left"> Activity Type</td>
                            <td class="on-description-left"> Activity<div style="top:10px; right:0; position:absolute;">Active</div></td>
                        </tr>
                        <tr>
                            <input id="id" name="id" type="hidden" value="<?php echo stripslashes($activityInfo[0]); ?>" />
                            <input id="company_id" name="company_id" type="hidden" value="<?php echo stripslashes($activityInfo[1]); ?>" />
                            <td class="rightdata" valign="top" rowspan="100%">
                                <input id="name" name="name" type="text" class="on-field entered non-numeric" tabindex="<?php echo $index++; ?>" value="<?php echo stripslashes($activityInfo[2]); ?>" />
                                <div id="nameEmpty" name="nameEmpty" class="error"><font style='padding-right:30px;' class="on-validate-error">* Activity Type must be entered</font></div>
                                <div id="nameDiv" name="nameDiv" class="error"><font style='padding-right:30px;' class="on-validate-error">* Activity Type must be alpha-numeric, eg. AT 145</font></div>
                            </td>
                            <td>
                            <table id="activitiesTbl" name="activitiesTbl" width="100%">
                            <?php
                                $counter = 1;

                                if (is_array($activityInfo[3])) {
                                    foreach ($activityInfo[3] as $activity) {
                                        echo "<tr>";
                                            echo "<td class='rightdata'>";
                                                echo "<input id='id".$counter."' name='id".$counter."' type='hidden' value='".stripslashes($activity[0])."' />";
                                                echo "<input class='on-field entered non-numeric' id='name".$counter."' name='name".$counter."' type='text' tabindex='".$index."' value='".stripslashes($activity[1])."' />";
                                                            $checked = ($activity[2] == "1") ? "checked" : "";
                                                echo "<div id='nameEmpty".$counter."' name='nameEmpty".$counter."' class='error'><font class='on-validate-error'>* Activity name must be entered</font></div>";
                                                echo "<div id='nameDiv".$counter."' name='nameDiv".$counter."' class='error'><font class='on-validate-error'>* Activity name must be alpha-numeric</font></div>";
                                            echo "</td>";
                                            echo "<td class='centerdata' style='padding-left:50px; padding-right:10px; padding-top:8px;'>
                                                        <input id='active".$counter."' name='active".$counter."' type='checkbox' tabindex='".$index++."' value='1' $checked />";
                                            echo "</td>";
                                        echo "</tr>";
                                        $counter++;
                                    }
                                }

                                echo "<tr>";
                                    echo "<td>";
                                        echo "<input id='id".$counter."' name='id".$counter."' type='hidden' value='' />";
                                        echo "<input class='on-field non-numeric' id='name".$counter."' name='name".$counter."' type='text' tabindex='".$index++."' value='' />";
                                        //echo "<div id='nameEmpty".$counter."' name='nameEmpty".$counter."' class='error'><font class='on-validate-error'>* Activity name must be entered</font></div>";
                                        echo "<div id='nameDiv".$counter."' name='nameDiv".$counter."' class='error'><font class='on-validate-error'>* Activity name must be alpha-numeric</font></div>";
                                    echo "</td><td></td>";
                                echo "</tr>";
                                $counter++;
                            ?>
                            </table>
                            <input style="margin-left:5px;" id="addRow" name="addRow" href="activitiesTbl" type="button" value="+" tabindex="<?php echo $index++; ?>" />
                        </td>
                    </tr>
                </table>
                <br/>
                <input id="btnAddEdit" name="btnAddEdit" tabindex="<?php echo $index++; ?>" type="button" value="<?php echo ($activityInfo[0] == "") ? "Add" : "Update"; ?>" />
                <input id="save" name="save" type="hidden" value="0" />
                <input id="activitiesTblCntr" name="activitiesTblCntr" type="hidden" value="<?php echo $counter; ?>" />
            </form>
        </td>
    </tr>
    <tr>
        <td>
            <br/>
        </td>
    </tr>
</table>
<?php
    //  Print Footer
    print_footer();
?>
