<?php
	session_start();

	include("_db.php");
	include("_functions.php");
	include("graphics.php");
	include("include/sajax.php");

	if (!$_SESSION["logged_in"] === true)
		header("Location: login.php");

	if (!$_SESSION["eula_agree"] === true)
		header("Location: logout.php");

	if (!isset($_SESSION["company_id"]))
		header("Location: home.php");

	if (!hasAccess("BOOKING_MANAGE"))
		header("Location: noaccess.php");

	///////////////////////////
	//  Sajax
	$sajax_request_type = "GET";
	sajax_init();

	sajax_handle_client_request();
	///////////////////////////

	$user_name = q("SELECT CONCAT(frstname,' ',lstname) FROM Employee WHERE id = '".$_SESSION["user_id"]."'");
	$user_id = $_SESSION["user_id"];

	global $file_directory;

	if (isset($_POST["btnUpdate"])) {
		unset($_POST["btnUpdate"]);

		//  Time Sheet Check Box Array
		$arrayOne                                                          = preg_split("/_/", addslashes(strip_tags($_POST["checkbox1"])));
		if ($arrayOne[0][0])                                                 $arrayOne = array_slice($arrayOne, 0, count($arrayOne) - 1);
		if ($arrayOne[0][0])
			foreach ($arrayOne as $a){
				if ($_POST["time_delete".$a])  {
					$delete = q("DELETE FROM ApprovedTime WHERE timesheet_id = '$a'");
					$delete = q("DELETE FROM TimeSheet WHERE id = '$a'");
				}
			}
		if ($delete){
			$time                                                       = date("H:i:s");
			$logs                                                       = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id)
																					VALUES('Timesheet Entries Deleted', 'Delete', 'TimeSheet', '".$_SESSION["email"]."', '$today', '$time', '".$_SESSION["company_id"]."')");
		}
		//  Expense Sheet Check Box Array
		$arrayTwo                                                          = preg_split("/_/", addslashes(strip_tags($_POST["checkbox2"])));
		if ($arrayTwo[0][0])                                                 $arrayTwo = array_slice($arrayTwo, 0, count($arrayTwo) - 1);
		if ($arrayTwo[0][0])
			foreach ($arrayTwo as $a){
				if ($_POST["expense_delete".$a])       {
					$delete = q("DELETE FROM ApprovedExpense WHERE expensesheet_id = '$a'");
					$delete = q("DELETE FROM ExpenseSheet WHERE id = '$a'");

					//  Remove backing documentation if any
					deleteDir($file_directory.$a);
				}
			}
		if ($delete){
			$time                                                       = date("H:i:s");
			$logs                                                       = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id)
																					VALUES ('ExpenseSheet Entries Deleted', 'Delete', 'ExpenseSheet', '".$_SESSION["email"]."', '$today', '$time', '".$_SESSION["company_id"]."')");
		}

		// Other
		$array = preg_split("/_/", addslashes(strip_tags($_POST["idBox"])));
		$arrayExpenseFields = preg_split("/_/", addslashes(strip_tags($_POST["idBox2"])));

		if (is_array($array))
			$array = array_slice($array, 0, count($array) - 1);
		if (is_array($arrayExpenseFields))
			$arrayExpenseFields = array_slice($arrayExpenseFields, 0, count($arrayExpenseFields) - 1);

		if (isset($timeArr))
			unset($timeArr);

		$timeArr[] = number_format("0.25", 2, ".", "");
		$timeArr[] = number_format("0.50", 2, ".", "");
		$timeArr[] = number_format("0.75", 2, ".", "");

		for ($i = 1; $i <= 12; $i += 0.5)
			$timeArr[] = number_format($i, 2, ".", "");

		foreach ($array as $a)  {
			$id = substr($a, 0, strpos($a, ":"));
			$locked = substr($a, (strpos($a, ":") + 1), strlen($a));

			if (is_numeric($id))    {
				if (addslashes(strip_tags($_POST["TIME".$id])) == "1")	{
					$date = addslashes(strip_tags($_POST["A".$id]));
					$descr = addslashes(strip_tags($_POST["B".$id]));
					$time = number_format(addslashes(strip_tags($_POST["C".$id])), 2, ".", "");

					if (submitDateCheck($date)) {
						if (in_array($time, $timeArr, true) && $locked == "0")  {
							$update = q("UPDATE TimeSheet SET date = '$date', descr = '$descr', time_spent = '$time' WHERE id = '$id'");
							$update = q("UPDATE ApprovedTime SET date = '$date', descr = '$descr', time_spent = '$time' WHERE timesheet_id = '$id'");
						}
					}
				}
			}
		}

		foreach ($arrayExpenseFields as $a)  {
			$id = substr($a, 0, strpos($a, ":"));
			$locked = substr($a, (strpos($a, ":") + 1), strlen($a));

			if (is_numeric($id))    {
				if (addslashes(strip_tags($_POST["EXPENSE".$id])) == "1")	{
					$date = addslashes(strip_tags($_POST["X".$id]));
					$descr = addslashes(strip_tags($_POST["Y".$id]));
					$val = number_format(addslashes(strip_tags($_POST["Z".$id])), 2, ".", "");

					if (submitDateCheck($date)) {
						if ($locked == "0")  {
							$update = q("UPDATE ExpenseSheet SET date = '$date', descr = '$descr', expense = '$val' WHERE id = '$id'");
							$update = q("UPDATE ApprovedExpense SET date = '$date', descr = '$descr', expense = '$val' WHERE expensesheet_id = '$id'");
						}
					}
				}
			}
		}

		if ($errorMessage == "")    header("Location: booking_management.php");
	}

	//  Print Header
	print_header();
	//  Print Menu
	print_menus("0", "bookings", "bookings");

	if ($errorMessage != "")    {
		if ($errorMessage == "Please note that we are currently busy with data management, no entries are allowed for 2010-08-26. Please check back in 24 Hours.")
			echo "<p align='center' style='padding:0px;'><strong style='font-size:12px;'><font class='on-validate-error'>$errorMessage</font></strong></p>";
		else
			echo "<p align='center' style='padding:0px;'><strong><font class='on-validate-error'>$errorMessage</font></strong></p>";
		echo "<br/>";
	}

	$projects = q("SELECT DISTINCT(p.id), p.name, p_type FROM (Project AS p INNER JOIN Project_User AS pu ON p.id = pu.project_id INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) ".
					"WHERE pu.user_id = '".$_SESSION["user_id"]."' AND cot.company_id = '".$_SESSION["company_id"]."' AND pu.company_id = '".$_SESSION["company_id"]."' AND p.completed = '0' AND p.deleted = '0' ".
					"AND pu.deleted = '0' ORDER BY UPPER(p.name)");
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
	//  Calendar
	jQuery(function(){
		jQuery('#date_from').DatePicker({
			format:'Y-m-d',
			date: jQuery('#date_from').val(),
			current: jQuery('#date_from').val(),
			starts: 1,
			position: 'right',
			onBeforeShow: function(){
				var _date;
				if (!validation('date',jQuery('#date_from').val()))
				_date = new Date();
				else _date = jQuery('#date_from').val();
				jQuery('#date_from').DatePickerSetDate(_date, true);
			},
			onChange: function(formated, dates){
				jQuery('#date_from').val(formated);
				jQuery('#date_from').DatePickerHide();
			}
		});

		jQuery('#date_to').DatePicker({
			format:'Y-m-d',
			date: jQuery('#date_to').val(),
			current: jQuery('#date_to').val(),
			starts: 1,
			position: 'right',
			onBeforeShow: function(){
				var _date;
				if (!validation('date',jQuery('#date_to').val()))
				_date = new Date();
				else _date = jQuery('#date_to').val();
				jQuery('#date_to').DatePickerSetDate(_date, true);
			},
			onChange: function(formated, dates){
				jQuery('#date_to').val(formated);
				jQuery('#date_to').DatePickerHide();
			}
		});

		// WATCH FOR CHANGE
		jQuery(".watch-for-change").change(function()	{
			var field = jQuery(this).attr("id").substr(0,1);
			var id = jQuery(this).attr("id").substr(1);

			if (field == "A" || field == "B" || field == "C")
				jQuery("#TIME"+id).val("1");
			else
				jQuery("#EXPENSE"+id).val("1");
		});
	});

    function check()
    {
        var valid                                                       = 1;

        if (document.forms["bookings"].dateType.value == "custom")
        {
            //  Check That Date From Is Entered
            if (document.forms["bookings"].date_from.value == "")
            {
                ShowLayer("dateFrom", "block");
                valid                                                   = 0;
            }
            //  Check That Entered Date From Is Valid
            else
            {
                ShowLayer("dateFrom", "none");

                if (!validation("date", document.forms["bookings"].date_from.value))
                {
                    ShowLayer("dateFromDiv", "block");
                    valid                                               = 0;
                }
                else
                    ShowLayer("dateFromDiv", "none");
            }

            //  Check That Date To Is Entered
            if (document.forms["bookings"].date_to.value == "")
            {
                ShowLayer("dateTo", "block");
                valid                                                   = 0;
            }
            //  Check That Entered Date To Is Valid
            else
            {
                ShowLayer("dateTo", "none");

                if (!validation("date", document.forms["bookings"].date_to.value))
                {
                    ShowLayer("dateToDiv", "block");
                    valid                                               = 0;
                }
                else
                    ShowLayer("dateToDiv", "none");
            }
        }

        if (valid == 1)
        {
            document.forms["bookings"].save.value                         = 1;
            document.forms["bookings"].submit();
        }
    }

    function dateSelection()
    {
        if (document.forms["bookings"].dateType.value == "custom")
            ShowLayer("datesDiv", "block");
        else
            ShowLayer("datesDiv", "none");
    }

</script>
<table width="100%">
	<tr height="380px">
		<td class="centerdata" valign="top">
			<form action="" id="bookings" name="bookings" method="post">
				<table width="100%">
					<tr>
						<td class="centerdata">
							<h6>
								Manage Bookings
							</h6>
						</td>
					</tr>
				</table>
				<br/>

				<table width="100%">
					<tr>
						<td class="on-description" width="50%">
								Project Name:
						</td>
						<td style="padding-top:2px;" width="50%">
							<select id="project" name="project" method="post" class="on-field required" tabindex="<?php echo $index++; ?>">
								<!--<option value="null">--  Select A Project  --</option>-->
								<option value="all">-- All Projects --</option>
								<?php
									if (is_array($projects))    {
										foreach ($projects as $project) {
											$value = ($project[2] == "CP") ? "CP_".$project[0]: "SP_".$project[0];

											if ($_POST["project"] == $value)    echo "<option value='".$value."' selected>".$project[1]."</option>";
											else                                echo "<option value='".$value."'>".$project[1]."</option>";
										}
									}
								?>
							</select>
							<div id="projectDiv" name="projectDiv" class="error">* Project must be selected</div>
						</td>
					</tr>
				</table>

				 <table width="100%">
							<tr>
								<td class="on-description" width="50%">
									Date Type(s):
								</td>
								<td width="50%">
									<select class="on-field" method="post" name="dateType" onChange="dateSelection();">
										<option value="currentwk">Current Week</option>
										<option value="previouswk">Previous Week</option>
										<option value="currentmnth">Current Month</option>
										<option value="previousmnth">Previous Month</option>
										<option value="custom">Custom Dates</option>
									</select>
								</td>
							</tr>
						</table>

						<div id="datesDiv" style="display: none;">
							<table width="100%">
								<tr>
									<td class="on-description" width="50%">
										Date From:
									</td>
									<td width="50%">
										<input class="on-field-date" id="date_from" name="date_from" type="text" style="text-align:right;" value="<?php
											if ($_POST["date_from"] != "") echo "".$_POST["date_from"]; else echo "".getMonthStart($today); ?>">
										<div id="dateFrom" style="display: none;"><font color="#FF5900">* Date must be entered</font></div>
										<div id="dateFromDiv" style="display: none;"><font color="#FF5900">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font>
										</div>
									</td>
								</tr>
								<tr>
									<td class="on-description" width="50%">
										Date To:
									</td>
									<td>
										<input class="on-field-date" id="date_to" name="date_to" type="text" style="text-align:right;" value="<?php
											if ($_POST["date_to"] != "") echo "".$_POST["date_to"]; else echo "".getMonthEnd($today); ?>">
										<div id="dateTo" style="display: none;"><font color="#FF5900">* Date must be entered</font></div>
										<div id="dateToDiv" style="display: none;"><font color="#FF5900">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
									</td>
								</tr>
							</table>
						</div>

						<input name="btnGenerate" onClick="check();" type="button" value="Generate Report">
						<input method="post" name="save" type="hidden" value="0" />
				<?php

				if (isset($_POST["save"]) && $_POST["save"] === "1")
				{
					GLOBAL $excelheadings;

					$errorMessage = "";
					$date_type = $_POST["dateType"];

					//  Get Dates
					if ($date_type == "currentwk") {
						$date_from = currentWeekStart();
						$date_to = $today;
					}
					else if ($date_type == "previouswk") {
						$date_from = getDates(currentWeekStart(), -7);
						$date_to = getDates(currentWeekStart(), -1);
					}
					else if ($date_type == "currentmnth") {
						$date_from = getMonthStart($today);
						$date_to = $today;
					}
					else if ($date_type == "previousmnth") {
						$date_from = getPreviousMonthStart();
						$date_to = getMonthEnd(getPreviousMonthStart());
					}
					else if ($date_type == "custom") {
						$date_from = addslashes(strip_tags($_POST["date_from"]));
						$date_to = addslashes(strip_tags($_POST["date_to"]));
					}

					//  Timestamp
					if ($date_from === $date_to)
						$timestamp = "Time Period: ".$date_from;
					else
						$timestamp = "Time Period: ".$date_from." - ".$date_to;
				}else{
					$date_from = currentWeekStart();
					$date_to = $today;
				}

				GLOBAL $excelheadings;
				GLOBAL $exceldata;
				GLOBAL $ShowExportButton;
				$ShowExportButton = 0;
				$row = 0;

					///// -- Time Sheet Info -- /////
					$user_name = q("SELECT CONCAT(frstname, ' ', lstname) FROM Employee WHERE id = '".$_SESSION['user_id']."' ");
					$weekStart = (isset($_POST["save"]) && $_POST["save"] === "1")  ? $date_from : currentWeekStart();
					$date = (isset($_POST["save"]) && $_POST["save"] === "1")  ? $date_to : $today;

					$finalDisplay = "";
					$display = "";

					 ///////////////////////////////
					$hasAreas = 0;                                               // Does Areas exist for the user in expense or time sheet table

					$areasTime = q("SELECT project_id, area_id FROM TimeSheet WHERE user_id = ".$_SESSION["user_id"]." AND area_id > 0 AND date BETWEEN '".$weekStart."' AND '".$today."'");
					$areasExpense = q("SELECT project_id, area_id FROM ExpenseSheet WHERE  user_id = ".$_SESSION["user_id"]." AND area_id > 0 AND date BETWEEN '".$weekStart."' AND '".$today."'");

					if(is_array($areasTime) || is_array($areasExpense)) {                              // Time
						$hasAreas = 1;
					}

					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
				if($hasAreas)
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
						$row++;

					$exceldata[$row][] = "Booking Management";
					$exceldata[$row][] = "";
				if($hasAreas)
					$exceldata[$row][] = "";
					$exceldata[$row][] = "";
					$exceldata[$row][] = "";
					$exceldata[$row][] = "";
					$exceldata[$row][] = "";
					$exceldata[$row][] = "";
						$row++;

				$project_id = substr($_POST["project"], 3);

				$pName = q("SELECT name FROM Project WHERE id = '".$project_id."' ");

				if($_POST["project"] == "null" || $_POST["project"] == "all"){
					$pName = "All Projects";
				}

					$exceldata[$row][] = "Project: $pName";
					$exceldata[$row][] = "";
				if($hasAreas)
					$exceldata[$row][] = "";
					$exceldata[$row][] = "";
					$exceldata[$row][] = "";
					$exceldata[$row][] = "";
					$exceldata[$row][] = "";
					$exceldata[$row][] = "";
						$row++;

				$exceldata[$row][] = $timestamp;
					$exceldata[$row][] = "";
				if($hasAreas)
					$exceldata[$row][] = "";
					$exceldata[$row][] = "";
					$exceldata[$row][] = "";
					$exceldata[$row][] = "";
					$exceldata[$row][] = "";
					$exceldata[$row][] = "";
						$row++;
				////////////////////////////////
				$extraCounter = 0;
				if($hasAreas){
					$areaData = "<th>Area</th>";
					$extraCounter  = 1;
				}else{
					$areaData = "";
					$extraCounter  = 0;
				}

					$printed_headings = 0;

						//  Daily Summary - Time Booked
						$headings = "<tr>".
										"<th>Date</th>".
										"<th>Project Name</th>".
										$areaData.
										"<th>Activity Type</th>".
										"<th>Activity</th>".
										"<th>Description</th>".
										"<th>&nbsp;</th>
										<th>Time</th>
										<th>Delete</th>
									</tr>";

					$exceldata[$row][] = "Date";
					$exceldata[$row][] = "Project Name";
				if($hasAreas)
					$exceldata[$row][] = "Area";
					$exceldata[$row][] = "Activity Type";
					$exceldata[$row][] = "Activity";
					$exceldata[$row][] = "Description";
					$exceldata[$row][] = "Amount";
					$exceldata[$row][] = "Time";
						$row++;

					$exceldata[$row][] = " Booked Time Information: ".$user_name;
					$exceldata[$row][] = "";
				if($hasAreas)
					$exceldata[$row][] = "";
					$exceldata[$row][] = "";
					$exceldata[$row][] = "";
					$exceldata[$row][] = "";
					$exceldata[$row][] = "";
					$exceldata[$row][] = "";
						$row++;

					$dayHours = 0;
					$totalHours = 0;
					$timeDisplay = "";

					$checkbox1 = "";
					$checkbox2 = "";

				$where = "";
					 //Project Selection
				if (isset($_POST["project"])) {
					if($_POST["project"] == "null" || $_POST["project"] == "all"){
						$where .= "";
					}else{
						$project_id = substr($_POST["project"], 3);
						$where .= " AND p.id = $project_id";
					}
				}

	//               $where .= " AND ts.date >= '$date_from' AND ts.date <= '$date_to' ";
					while ($date >= $weekStart)    {
						$dayHours = 0;
						$timeDisplay = "";

						$timeSummary = q("SELECT ts.id,ts.projectType,ts.project_id,ts.area_id,ts.activity_id,ts.descr,ts.time_spent,ts.locked FROM (Project AS p
											INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
											INNER JOIN TimeSheet AS ts ON ts.project_id = p.id)
											WHERE ts.company_id = '".$_SESSION["company_id"]."'
												AND cot.company_id = '".$_SESSION["company_id"]."'
												AND ts.user_id = '".$_SESSION["user_id"]."'
												AND p.deleted = '0'
												AND ts.date = '$date'
												$where
											ORDER BY ts.on_date, ts.on_time");

						if (is_array($timeSummary))   {
							foreach ($timeSummary as $s) {
									$project = q("SELECT name FROM Project WHERE id = '".$s[2]."'") ;

									$idBox .= "".$s[0].":".$s[7]."_";
									$dayHours += $s[6];
									$totalHours += $s[6];

									$readonly = ($s[7] != "0") ? "readonly" : "";
									$disabled = ($s[7] != "0") ? "disabled" : "";

									if ($s[1] == "CP")  {
										///////////////
										$activityType = q("SELECT type FROM ActivityTypes WHERE id = (SELECT parent_id FROM Activities WHERE id = '".$s[4]."')");
										$activity = q("SELECT name FROM Activities WHERE id = '".$s[4]."'");
										if($hasAreas){
											$area = q("SELECT name FROM areas WHERE id = ".$s[3]." ");
											if($s[3] != 0){
												$aData = "<td style='white-space:nowrap'>".$area."</td>";
											}else{
												$aData = "<td style='white-space:nowrap'>-</td>";
											}
										}else{
											$aData = "";
										}
										/////////////////
									}
									else { // if Shared Project
										/////////////////
										$activityType = q("SELECT type FROM ActivityTypes WHERE id = (SELECT parent_id FROM Activities WHERE id = '".$s[4]."')");
										$activity = q("SELECT name FROM Activities WHERE id = '".$s[4]."'");
										if($hasAreas){
											$area = q("SELECT name FROM areas WHERE id = ".$s[3]." ");
											if($s[3] != 0){
												$aData = "<td style='white-space:nowrap'>".$area."</td>";
											}else{
												$aData = "<td style='white-space:nowrap'>-</td>";
											}
										}else{
											$aData = "";
										}
										/////////////////
									}

								$checkbox1      .= "".$s[0]."_";

								$timeDisplay .= "<tr>".
													"<td>
														<input id='TIME".$s[0]."' name='TIME".$s[0]."' type='hidden' value='0'>
														<input id='A".$s[0]."' name='A".$s[0]."' type='text' size='12' $readonly  value='".$date."' tabindex='".$index."' class='watch-for-change'>
													</td>
													<td style='white-space:nowrap'>".$project."</td>".
													$aData.
													"<td style='white-space:nowrap'>".$activityType."</td>
													<td style='white-space:nowrap'>".$activity."</td>
													<td>
														<input id='B".$s[0]."' name='B".$s[0]."' type='text' size='50' $readonly value='".str_replace("'", "&#39;" , $s[5])."' tabindex='".$index."' class='watch-for-change'>
													</td>
													<td>
														&nbsp;
													</td>
													<td>
														<input id='C".$s[0]."' class='number watch-for-change' name='C".$s[0]."' type='text' size='12' $readonly value='".number_format($s[6], 2, ".", "")."' tabindex='".$index."'>
													</td>
													<td class='centerdata'>
														<input method='post' name='time_delete".$s[0]."' type='checkbox' $readonly $disabled value='1'>
													</td>
												</tr>";

								$exceldata[$row][] = $date;
								$exceldata[$row][] = $project;
							if($hasAreas)
								$exceldata[$row][] = $area;
								$exceldata[$row][] = $activityType;
								$exceldata[$row][] = $activity;
								$exceldata[$row][] = str_replace("'", "&#39;" , $s[5]);
								$exceldata[$row][] = "";
								$exceldata[$row][] =$s[6];
									$row++;
							}

							if($printed_headings == 1)
								$headings = "";

								$display .= $headings.$timeDisplay;

							$printed_headings = 1;
							$ShowExportButton = 1;
						}
						$date = getDates($date, -1);
					}

					if ($display != ""){
							$exceldata[$row][] = "";
							$exceldata[$row][] = "";
						if($hasAreas)
							$exceldata[$row][] = "";
							$exceldata[$row][] = "";
							$exceldata[$row][] = "";
							$exceldata[$row][] = "";
							$exceldata[$row][] = "Total: ";
							$exceldata[$row][] = $totalHours;
								$row++;
						$totalHours = "<tr><td class='on-table-total' colspan='".(5+$extraCounter)."'>&nbsp;</td>
														<td class='on-table-total' style='white-space:nowrap'>Total:</td>
														<td class='on-table-total'>".number_format($totalHours, 2, ".", "")."</td>
														<td class='on-table-total'>&nbsp;</td>
												</tr>";
						$finalDisplay .=  "<tr><td class='on-table-clear' colspan='6".+$extraCounter."'>
														<a>Booked <font class='on-validate-error'>Time</font> Information: ".$user_name."</a></td></tr>".$display.$totalHours;
					}

	////////////////////////////// EXPENSE /////////////////////////////////////
					$weekStart = (isset($_POST["save"]) && $_POST["save"] === "1")  ? $date_from : currentWeekStart();
					$date = (isset($_POST["save"]) && $_POST["save"] === "1")  ? $date_to : $today;

					//  Daily Summary - Expenses Booked
					$index++;
					$display = "";

				////////////////////////////////
				$extraCounter = 0;
				if($hasAreas){
					$areaData = "<th>Area</th>";
					$extraCounter  = 1;
				}else{
					$areaData = "";
					$extraCounter  = 0;
				}
					$printed_headings2 = 0;

					$headings = "<tr>".
									"<th>Date</th>".
									"<th>Project Name</th>".
									$areaData.
									"<th>Expense Type - Expense Info</th>".
									"<th>Activity Type - Activity</th>".
									"<th>Description</th>".
									"<th>Amount</th>".
									"<th>Expense $currency</th>".
									"<th>Delete</th>".
								"</tr>";

					$exceldata[$row][] = "";
					$exceldata[$row][] = "";
				if($hasAreas)
					$exceldata[$row][] = "";
					$exceldata[$row][] = "";
					$exceldata[$row][] = "";
					$exceldata[$row][] = "";
					$exceldata[$row][] = "";
					$exceldata[$row][] = "";
						$row++;

					$exceldata[$row][] = "Booked Expenses Information: ".$user_name;
					$exceldata[$row][] = "";
				if($hasAreas)
					$exceldata[$row][] = "";
					$exceldata[$row][] = "";
					$exceldata[$row][] = "";
					$exceldata[$row][] = "";
					$exceldata[$row][] = "";
					$exceldata[$row][] = "";
						$row++;

					$dayExpenses = 0;
					$totalExpenses = 0;
					$expenseDisplay = "";

					$where2 = "";
					 //Project Selection
				if (isset($_POST["project"])) {
					if($_POST["project"] == "null" || $_POST["project"] == "all"){
						$where2 .= "";
					}else{
						$project_id = substr($_POST["project"], 3);
						$where2 .= " AND p.id = $project_id";
					}
				}

	//               $where2 .= " AND es.date >= '$date_from' AND es.date <= '$date_to' ";

					while ($date >= $weekStart)    {
						$dayExpenses = 0;
						$expenseDisplay = "";

						$expenseSummary = q("SELECT es.projectType,es.project_id,es.area_id,es.activity_id,es.expense_type_id,v.type,d.parent_id,d.name,es.descr,es.otherDescr,es.kilometers,es.total,es.expense,es.id,es.locked
											FROM (Project AS p
												INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
												INNER JOIN ExpenseSheet AS es ON es.project_id = p.id
												LEFT JOIN Vehicle AS v ON v.id = es.vehicle_id
												LEFT JOIN Disbursements AS d ON d.id = es.disbursement_id)
											WHERE cot.company_id = '".$_SESSION["company_id"]."'
												AND es.company_id = '".$_SESSION["company_id"]."'
												AND es.user_id = '".$_SESSION["user_id"]."'
												AND es.date = '$date'
												$where2
											ORDER BY es.expense_type_id, v.type, d.name");

						if (is_array($expenseSummary))   {
							foreach ($expenseSummary as $s) {
								$project = q("SELECT name FROM Project WHERE id = '".$s[1]."'") ;

								$idBox2 .= "".$s[13].":".$s[14]."_";
								$dayExpenses += $s[12];
								$totalExpenses += $s[12];

								$readonly = ($s[14] != "0") ? "readonly" : "";
								$disabled = ($s[14] != "0") ? "disabled" : "";

								$expenseType = q("SELECT name FROM dropdowns WHERE id = '".$s[4]."'");
								$expenseDescr = ($expenseType == "Driving") ? $s[5] : (($expenseType == "Printing") ? q("SELECT type FROM DisbursementTypes WHERE id = '".$s[6]."'")." [".$s[7]."]" :
												(($expenseType == "Accommodation" || $expenseType == "Flights" || $expenseType == "Car Hire") ? $s[9] : ""));

								$expenseDescr = ($expenseDescr != "") ? $expenseType." - ".$expenseDescr : $expenseType;

								if ($s[0] == "CP")  {
									//////////////////////////
										$activityType2 = q("SELECT type FROM ActivityTypes WHERE id = (SELECT parent_id FROM Activities WHERE id = '".$s[3]."')");
										$activity2 = q("SELECT name FROM Activities WHERE id = '".$s[3]."'");
										if($hasAreas){
											$area = q("SELECT name FROM areas WHERE id = ".$s[2]." ");
											if($s[2] != 0){
												$aData = "<td style='white-space:nowrap'>".$area."</td>";
											}else{
												$aData = "<td style='white-space:nowrap'>-</td>";
											}
										}else{
											$aData = "";
										}
									/////////////////////////
								}
								else {       // if Shared Project
									////////////////////////////
									$activityType2 = q("SELECT type FROM ActivityTypes WHERE id = (SELECT parent_id FROM Activities WHERE id = '".$s[3]."')");
									$activity2 = q("SELECT name FROM Activities WHERE id = '".$s[3]."'");
									if($hasAreas){
										$area = q("SELECT name FROM areas WHERE id = ".$s[2]." ");
										if($s[2] != 0){
											$aData = "<td style='white-space:nowrap'>".$area."</td>";
										}else{
											$aData = "<td style='white-space:nowrap'>-</td>";
										}
									}else{
										$aData = "";
									}
									/////////////////////////////
								}
								if($s[9].$s[10] == "")
								$s[9] = " - ";

								$checkbox2      .= "".$s[13]."_";

								$activitiesDescr = $activityType2." - ".$activity2;

								if ($s[10].$s[11] == "") $s[10] = " - ";

								$expenseDisplay .= "<tr>".
														"<td>".
															"<input id='EXPENSE".$s[13]."' name='EXPENSE".$s[13]."' type='hidden' value='0'>".
															"<input id='X".$s[13]."' name='X".$s[13]."' type='text' size='12' $readonly  value='".$date."' class='watch-for-change'>".
														"</td>".
														"<td><div class='projectReportName'>".$project.
														((hasBackingDocuments($s[13])) ? " <img onClick='window.open(\"serveFile.php?type=files&expense=".$s[13]."\")' class='hand' title='Click to download backing document(s)' src='images/icons/download.png'/>" : "").
														"</div></td>".
														$aData.
														 "<td>".$expenseDescr."</td>".
														"<td>".$activitiesDescr."</td>".
														"<td><input id='Y".$s[13]."' name='Y".$s[13]."' type='text' size='50' $readonly  value='".str_replace("'", "&#39;" , $s[8])."' class='watch-for-change'></td>
														<td class='rightdata'>".$s[10].$s[11]."</td>
														<td class='rightdata'>";
								$isEditable = q("SELECT name FROM dropdowns WHERE id = '".$s[4]."' ");
								if($isEditable == "Expense" || $isEditable == "Car Hire" || $isEditable == "Flights" ||$isEditable == "Toll Gate"){
									$expenseDisplay .= "<input id='Z".$s[13]."' class='number watch-for-change' name='Z".$s[13]."' type='text' size='12' $readonly value='".number_format($s[12], 2, ".", "")."' tabindex='".$index."'>";
								}else{
									$expenseDisplay .= "<input id='Z".$s[13]."' class='number' name='Z".$s[13]."' type='text' size='12' readonly  value='".number_format($s[12], 2, ".", "")."' tabindex='".$index."'>";
								}
								$expenseDisplay .= "</td><td class='centerdata'>
																	<input method='post' name='expense_delete".$s[13]."' type='checkbox' $readonly $disabled value='1'>
																</td>
															</tr>";



								$exceldata[$row][] = $date;
								$exceldata[$row][] = $project;
							if($hasAreas)
								$exceldata[$row][] =$area;
								$exceldata[$row][] = $expenseDescr;
								$exceldata[$row][] = $activitiesDescr;
								$exceldata[$row][] = $s[8];
								$exceldata[$row][] = $s[9].$s[10];
								$exceldata[$row][] = $s[11];
									$row++;

							}

							if($printed_headings2 == 1)
								$headings = "";

							$displayBlock = "";
							$displayFoot = "";

							$display .= $headings.$expenseDisplay;

							$printed_headings2 = 1;
							$ShowExportButton = 1;
						}
						$date = getDates($date, -1);
					}

					if($display != ""){
						$display .= "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
					}

					if($printed_headings == 1 && $printed_headings2 == 1){
								$displayBlock = "<tr><td style='background-color:transparent' colspan='100%'><br></td></tr>";
					}

					if ($display != ""){
							$exceldata[$row][] = "";
							$exceldata[$row][] = "";
						if($hasAreas)
							$exceldata[$row][] = "";
							$exceldata[$row][] = "";
							$exceldata[$row][] = "";
							$exceldata[$row][] = "";
							$exceldata[$row][] = "Total (".$_SESSION["currency"]."):";
							$exceldata[$row][] = $totalExpenses;
								$row++;

						$totalExpenses = "<tr><td class='on-table-total' colspan='".(5+$extraCounter)."'>&nbsp;</td>
														<td class='on-table-total' style='white-space:nowrap'>Total (".$_SESSION["currency"]."):</td>
														<td class='on-table-total'>".number_format($totalExpenses, 2, ".", "")."</td>
														<td class='on-table-total'>&nbsp;</td>
												</tr>";
						$finalDisplay .=  $displayBlock."<tr><td class='on-table-clear' align='center' colspan='6".+$extraCounter."'><a>Booked <font class='on-validate-error'>
														Expenses</font> Information: ".$user_name."</a></td></tr>".$display.$totalExpenses;
					}

					if ($finalDisplay != "")    {
						echo "<div class='on-20px'><table class='on-table on-table-center'>$finalDisplay</table></div>";
						echo "<table class='on-table-center'><tr><td><input id='btnUpdate' name='btnUpdate' type='submit' value='Apply Changes' tabindex='".++$index."'></tr></td></table>";
						echo "<input method='post' name='idBox' type='hidden' value='".$idBox."' />";
						echo "<input method='post' name='idBox2' type='hidden' value='".$idBox2."' />";
					}

					echo "<input method='post' name='checkbox1' type='hidden' value='".$checkbox1."'>";
					echo "<input method='post' name='checkbox2' type='hidden' value='".$checkbox2."'>";


					///////////////////////////
					//  Set Export Information
					if(is_array($excelheadings) && is_array($exceldata)){
						$_SESSION["fileName"] = "Booking Management";
						$_SESSION["fileData"] = array_merge($excelheadings, $exceldata);
						///////////////////////////
						if($ShowExportButton == 1)
						echo "<br><input name='btnExport' onClick=\"location.href='_export.php'\" type='button' value='Export to Excel' />";
					}

				?>
			</form>
		</td>
	</tr>
	<tr>
		<td align="center">
			<br/>
		</td>
	</tr>
</table>
<?php
	//  Print Footer
	print_footer();
?>
