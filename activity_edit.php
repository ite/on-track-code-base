<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

	if (!hasAccess("ATYPE_MANAGE"))
		header("Location: noaccess.php");

    //  Update Activity Type And Sub Activities Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")
    {
        $errorMessage                                                   = "";

        //  Get Information
        $id                                                             = $_GET["id"];
        $activity_type_info_old                                         = q("SELECT type FROM ActivityTypes WHERE id = '$id'");
        $activity_type_info_new                                         = addslashes(strip_tags($_POST["activity_type_name"]));

        //  nActivities                                                 = Number of Activities per Type
        $nActivities                                                    = q("SELECT COUNT(id) FROM Activities WHERE parent_id = '$id'");
        $activities                                                     = q("SELECT id, name FROM Activities WHERE parent_id = '$id' ORDER BY name");

        //  Check If Activity Type Exists In Database
        if ($activity_type_info_old != $activity_type_info_new)
        {
            $exist                                                      = q("SELECT id FROM ActivityTypes WHERE type = '$activity_type_info_new' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."'");

            $do_update                                                  = true;
        }
        else
        {
            $exist                                                      = false;

            $do_update                                                  = false;
        }

        if (!$exist)
        {
            //  Update Activity Type If Necessary
            if ($do_update)
                $update                                                 = q("UPDATE ActivityTypes SET type = '$activity_type_info_new' WHERE id = '$id'");

            //  Sub Activities
            if ($nActivities > 1)
            {
                foreach ($activities as $activity)
                {
                    $activity_name_old                                  = $activity[1];
                    $activity_name_new                                  = addslashes(strip_tags($_POST["activity_name_$activity[0]"]));

                    if ($activity_name_old != $activity_name_new && $activity_name_new != "")
                        $update                                         = q("UPDATE Activities SET name = '$activity_name_new' WHERE id = '$activity[0]'");
                }

                //  New Sub Activity
                $activity_name                                          = addslashes(strip_tags($_POST["activity_name_".($nActivities + 1).""]));

                if ($activity_name != "")
                    $insert                                             = q("INSERT INTO Activities (parent_id, name) VALUES ('$id', '$activity_name')");
            }
            else if ($nActivities == 1)
            {
                $activity_name_old                                      = $activities[0][1];
                $activity_name_new                                      = addslashes(strip_tags($_POST["activity_name_$activities[0][0]"]));

                if ($activity_name_old != $activity_name_new && $activity_name_new != "")
                    $update                                         = q("UPDATE Activities SET name = '$activity_name_new' WHERE id = '$activities[0][0]'");

                //  New Sub Activity
                $activity_name                                          = addslashes(strip_tags($_POST["activity_name_".($nActivities + 1).""]));

                if ($activity_name != "")
                    $insert                                             = q("INSERT INTO Activities (parent_id, name) VALUES ('$id', '$activity_name')");
            }
            else
            {
                for ($i = 1; $i <= 5; $i++)
                {
                    $activity_name                                      = addslashes(strip_tags($_POST["activity_name_$i"]));

                    if ($activity_name != "")
                        $insert                                         = q("INSERT INTO Activities (parent_id, name) VALUES ('$id', '$activity_name')");
                }
            }

            if ($errorMessage == "")
            {
                $time                                                   = date("H:i:s");

                $logs                                                   = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('$activity_type_info_new and activities updated', 'Update', 'Activities', ".
                                                                            "'".$_SESSION["email"]."', '$today', '$time', '".$_SESSION["company_id"]."')");

                header("Location: activity_type_management.php");
            }
        }
        else
            $errorMessage                                               = "Activity Type Already Exists";
    }

    if ($errorMessage != "")
        echo "<p align='center' style='padding:0px;'><strong><font class='on-validate-error'>$errorMessage</font></strong></p>";

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("1", "activity_types");
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    var cal                                                             = new CodeThatCalendar(caldef1);

    function check(box)
    {
        var valid                                                       = 1;

        //  Check That Project Type Is Entered
        if (document.forms["activity_edit"].activity_type_name.value == "")
        {
            ShowLayer("activityTypeName", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("activityTypeName", "none");

        if (valid == 1)
        {
            document.forms["activity_edit"].save.value                  = 1;
            document.forms["activity_edit"].submit();
        }
    }
</script>
<?php
    $id                                                                 = $_GET["id"];
    $activity_type_info                                                 = q("SELECT type FROM ActivityTypes WHERE id = '$id'");

    //  nActivities                                                     = Number of Activities per Type
    $nActivities                                                        = q("SELECT COUNT(id) FROM Activities WHERE parent_id = '$id'");
    $activities                                                         = q("SELECT id, name FROM Activities WHERE parent_id = '$id' ORDER BY name");
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="activity_edit">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Edit Activity Type</h6>
                            </td>
                        </tr>
                        <tr>
                            <td class="centerdata">
                                <br/>
                            </td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                    Activity Type
                            </td>
                            <td class="on-description-left" width="50%">
                                    &nbsp;&nbsp;Activity
                            </td>
                        </tr>
                        <tr>
                            <td class="rightdata" valign="top" width="50%">
                                <input class="on-field" name="activity_type_name" tabindex="1" type="text" value="<?php echo $activity_type_info; ?>">
                                <div id="activityTypeName" style="display: none;"><font style="padding-right:25px;"class="on-validate-error">* Activity Type must be entered&nbsp;</font></div>
                            </td>
                            <td width="50%">
                                <?php
                                    $index                              = 1;

                                    if ($nActivities > 1)
                                    {
                                        foreach ($activities as $activity)
                                            echo "<input class='on-field' name='activity_name_".$activity[0]."' tabindex='".($index + 1)."' type='text' value='".$activity[1]."'><br/>";

                                        echo "<input class='on-field' name='activity_name_".($nActivities + 1)."' tabindex='".($index + 1)."' type='text'><br/>";
                                    }
                                    else if ($nActivities == 1)
                                    {
                                        echo "<input class='on-field' name='activity_name_".$activities[0][0]."' tabindex='".($index + 1)."' type='text' value='".$activities[0][1]."'><br/>";

                                        echo "<input class='on-field' name='activity_name_".($nActivities + 1)."' tabindex='".($index + 1)."' type='text'><br/>";
                                    }
                                    else
                                    {
                                        for ($i = 1; $i <= 5; $i++)
                                            echo "<input class='on-field' name='activity_name_".$i."' tabindex='".($index + 1)."' type='text'><br/>";
                                    }
                                ?>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <input name="btnAdd" onClick="check();" tabindex="<?php echo "".($index + 1); ?>" type="button" value="Update Activity">
                    <input method="post" name="save" type="hidden" value="0" />
                </form>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
