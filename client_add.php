<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    if (!hasAccess("CLIENT_MANAGE"))
        header("Location: noaccess.php");

    //  Insert Client Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")	{
		include("InvoiceEngine.php");
        $ie_info = q("SELECT invoice_ID, invoice_UN, invoice_PW FROM Company WHERE id = '".$_SESSION["company_id"]."'");
		$ie = new InvoiceEngine($ie_info[0][0],$ie_info[0][1],$ie_info[0][2]); //TODO: get from db


		
        $errorMessage                                                   = "";

        //  Get Information
        $client_name = addslashes(strip_tags($_POST["client_name"]));
        $address_1 = addslashes(strip_tags($_POST["address_1"]));
        $address_2 = addslashes(strip_tags($_POST["address_2"]));
        $city_ = addslashes(strip_tags($_POST["city_"]));
        $province_ = addslashes(strip_tags($_POST["province_"]));
        $areaCode_ = addslashes(strip_tags($_POST["areaCode_"]));
        $country_ = addslashes(strip_tags($_POST["country_"]));
        $website_ = addslashes(strip_tags($_POST["website_"]));
        $phoneNumber_ = addslashes(strip_tags($_POST["phoneNumber_"]));
        $tax_id_ = addslashes(strip_tags($_POST["tax_id_"]));

        //  Check If Client Exists In Database
        $exist = q("SELECT id FROM Client WHERE companyName = '$client_name' AND company_id = '".$_SESSION["company_id"]."'");

        if (!$exist)    {
			//API call
			$external_id = $ie->client_new($client_name, $address_1, $address_2, $city_, $province_, $areaCode, $country_, $website_, $phoneNumber_, "VAT Number", $tax_id_);

			$insert = q("INSERT INTO Client (company_id, companyName, address1, address2, city, province, areaCode, country, webURL, 
					phoneNum, tax_id, external_id) 
				VALUES ('".$_SESSION["company_id"]."', '".$client_name."', '".$address_1."', '".$address_2."', '".$city_."', 
					'".$province_."','".$areaCode_."', '".$country_."', '".$website_."', '".$phoneNumber_."', '".$tax_id_."', '$external_id')");

            if ($insert)    {
                $time                                                   = date("H:i:s");
                $logs                                                   = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                                "VALUES ('$client_name created', 'Insert', 'Client', '".$_SESSION["email"]."', ".
                                                                                "'$today', '$time', '".$_SESSION["company_id"]."')");
                $errorMessage                                           = "Client Added Successfully";
                header("Location: clients.php");
            }
        }else   {
            $errorMessage                                               = "Client Already Exists";
        }
    }

    if ($errorMessage != "")    {
        echo "<p align='center' style='padding:0px;'><strong><font class='on-validate-error'>$errorMessage</font></strong></p>";
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0", "vehicles");
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    function check()
    {
        var valid                                                       = 1;

        //  Check That Client Name Is Entered
        if (document.forms["client_add"].client_name.value == "")   {
            ShowLayer("clientName", "block");
            valid                                                       = 0;
        }else   {
            ShowLayer("clientName", "none");
        }
        /*
        //  Check That Address1 Is Entered
        if (document.forms["client_add"].address_1.value == "") {
            ShowLayer("address1Empty", "block");
            valid                                                       = 0;
        }else   {
            ShowLayer("address1Empty", "none");
        }
        //  Check That Address2 Is Entered
        if (document.forms["client_add"].address_2.value == "") {
            ShowLayer("address2Empty", "block");
            valid                                                       = 0;
        }else   {
            ShowLayer("address2Empty", "none");
        }
        //  Check That City Is Entered
        if (document.forms["client_add"].city_.value == "") {
            ShowLayer("city", "block");
            valid                                                       = 0;
        }else   {
            ShowLayer("city", "none");
        }
        //  Check That Area Code Is Entered
        if (document.forms["client_add"].areaCode_.value == "") {
            ShowLayer("areaCode", "block");
            valid                                                       = 0;
        }else   {
            ShowLayer("areaCode", "none");
        }
        //  Check That Province Is Entered
        if (document.forms["client_add"].province_.value == "") {
            ShowLayer("province", "block");
            valid                                                       = 0;
        }else   {
            ShowLayer("province", "none");
        }
        //  Check That Country Is Entered
        if (document.forms["client_add"].country_.value == "") {
            ShowLayer("country", "block");
            valid                                                       = 0;
        }else   {
            ShowLayer("country", "none");
        }
        */
        //  Check That Phone number Is Entered
        if (document.forms["client_add"].phoneNumber_.value == "") {
            ShowLayer("phoneNumberEmpty", "block");
            valid                                                       = 0;
        }else   {
            ShowLayer("phoneNumberEmpty", "none");
        }
        /*
        //  Check That Website Is Entered
        if (document.forms["client_add"].website_.value == "") {
            ShowLayer("website", "block");
            valid                                                       = 0;
        }else   {
            ShowLayer("website", "none");
        }
        */
        if (valid == 1)
        {
            document.forms["client_add"].save.value                    = 1;
            document.forms["client_add"].submit();
        }
    }
</script>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata">
                <form action="" method="post" name="client_add">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>
                                    Add New Client
                                </h6>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <br/>
                            </td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                    Client/Company Name:
                            </td>
                            <td width="50%">
                                <input class="on-field" name="client_name" tabindex="1" size="60" type="text">
                                <div id="clientName" style="display: none;"><font class="on-validate-error">* Name must be entered</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                Address 1
                            </td>
                            <td width="50%">
                                <input class="on-field" name="address_1" tabindex="2" type="text">
                                <div id="address1Empty" style="display: none;"><font class="on-validate-error">* Address1 must be entered</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                Address 2
                            </td>
                            <td width="50%">
                                <input class="on-field" name="address_2" tabindex="2" type="text">
                                <div id="address2Empty" style="display: none;"><font class="on-validate-error">* Address2 must be entered</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                City
                            </td>
                            <td width="50%">
                                <input class="on-field" name="city_" tabindex="2" type="text">
                                <div id="city" style="display: none;"><font class="on-validate-error">* City must be entered</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                Area Code
                            </td>
                            <td width="50%">
                                <input class="on-field-date" name="areaCode_" style="text-align:right" tabindex="2" type="text">
                                <div id="areaCode" style="display: none;"><font class="on-validate-error">* Area Code must be entered</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                Province
                            </td>
                            <td width="50%">
                                <input class="on-field" name="province_" tabindex="2" type="text">
                                <div id="province" style="display: none;"><font class="on-validate-error">* Province must be entered</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                Country
                            </td>
                            <td width="50%">
                                <input class="on-field" name="country_" tabindex="2" type="text">
                                <div id="country" style="display: none;"><font class="on-validate-error">* Country must be entered</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                Phone Number
                            </td>
                            <td width="50%">
                                <input class="on-field" name="phoneNumber_" tabindex="2" type="text">
                                <div id="phoneNumberEmpty" style="display: none;"><font class="on-validate-error">* Phone Number must be entered</font></div>
                                <!--<div id="phoneNumber" style="display: none;"><font class="on-validate-error">* Entered number must be a phone number, eg. 0825552222</font></div>-->
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                Website
                            </td>
                            <td width="50%">
                                <input class="on-field" name="website_" tabindex="2" type="text">
                                <div id="website" style="display: none;"><font class="on-validate-error">* Website URL must be entered</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                VAT Number
                            </td>
                            <td width="50%">
                                <input class="on-field" name="tax_id_" tabindex="2" type="text">
                                <div id="tax_id" style="display: none;"><font class="on-validate-error">* VAT Number must be entered</font></div>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <input name="btnAdd" onClick="check();" tabindex="3" type="button" value="Add Client">
                    <input method="post" name="save" type="hidden" value="0" />
                </form>
            </td>
        </tr>
        <tr>
            <td>
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
