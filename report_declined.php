<?php
    session_start();
    
    include("_db.php");
    include("graphics.php");
    include("include/sajax.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");
        
    //if (!hasAccess("REP_DECLINED"))
        //header("Location: noaccess.php");

    function getCompanies($id) {
       return q("SELECT DISTINCT(c.id), c.name FROM (Company AS c 
                        INNER JOIN companiesOnTeam AS cot ON c.id = cot.company_id 
                        INNER JOIN Project AS p ON p.id =  cot.project_id) 
                        WHERE cot.project_id = '$id' ORDER BY c.name"); 
    }
    
    $sajax_request_type = "GET";
    sajax_init();    
    sajax_export("getCompanies");    
    sajax_handle_client_request();

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("3", "reports");

/////////////////////////////////////////////////////////
/*     --  DECLINED TIME/EXPENSE REPORT   --    */
/////////////////////////////////////////////////////////
    //  Get Declined Time/Expense Information - Report
        ?>
        <link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
        <script type="text/javascript" src="include/datepicker.js"></script>
        <script language="JavaScript" src="include/validation.js"></script>
        <script language="JavaScript">
            //  Calendar
            jQuery(function(){
                //  Calendar(s)
                jQuery("#date_from").DatePicker({
                        format:"Y-m-d",
                        date: jQuery("#date_from").val(),
                        current: jQuery("#date_from").val(),
                        starts: 1,
                        position: "right",
                        onBeforeShow: function(){
                            var _date = (!validation("date",jQuery("#date_from").val())) ? new Date() : jQuery("#date_from").val();
                            jQuery("#date_from").DatePickerSetDate(_date, true);
                        },
                        onChange: function(formated, dates){
                            jQuery("#date_from").val(formated);
                            jQuery("#date_from").DatePickerHide();
                        }
                });

                jQuery("#date_to").DatePicker({
                        format:"Y-m-d",
                        date: jQuery("#date_to").val(),
                        current: jQuery("#date_to").val(),
                        starts: 1,
                        position: "right",
                        onBeforeShow: function(){
                            var _date = (!validation("date",jQuery("#date_to").val())) ? new Date() : jQuery("#date_to").val();
                            jQuery("#date_to").DatePickerSetDate(_date, true);
                        },
                        onChange: function(formated, dates){
                            jQuery("#date_to").val(formated);
                            jQuery("#date_to").DatePickerHide();
                        }
                });
            });

            function display()  {
                var valid = 1;

                //  Check That Project Is Selected
                if (document.forms["getDisplay"].project.value == "null")   {
                    ShowLayer("projectDiv", "block");
                    valid = 0;
                }
                else
                    ShowLayer("projectDiv", "none");

                //  Check That Date From Is Entered
                if (document.forms["getDisplay"].date_from.value == "") {
                    ShowLayer("dateFrom", "block");
                    valid = 0;
                }
                //  Check That Entered Date From Is Valid
                else    {
                    ShowLayer("dateFrom", "none");

                    if (!validation("date", document.forms["getDisplay"].date_from.value))  {
                        ShowLayer("dateFromDiv", "block");
                        valid = 0;
                    }
                    else
                        ShowLayer("dateFromDiv", "none");
                }

                //  Check That Date To Is Entered
                if (document.forms["getDisplay"].date_to.value == "")   {
                    ShowLayer("dateTo", "block");
                    valid = 0;
                }
                //  Check That Entered Date To Is Valid
                else    {
                    ShowLayer("dateTo", "none");

                    if (!validation("date", document.forms["getDisplay"].date_to.value))    {
                        ShowLayer("dateToDiv", "block");
                        valid = 0;
                    }
                    else
                        ShowLayer("dateToDiv", "none");
                }

                if (valid == 1) {
                    document.forms["getDisplay"].save.value = 1;
                    document.forms["getDisplay"].submit();
                }
            }
            
            <?php sajax_show_javascript(); ?>
            ///////////////////////////
            //  Sajax
            function ClearOptions(OptionList) {
                for (x = OptionList.length; x >= 0; x--){
                    OptionList[x]                                               = null;
                }
            }
            
            // Set Company
            function setCompanies(data) {
                var c                                                           = 0;
                document.forms["getDisplay"].company.options[(c++)]             = new Option("All Companies", "null");
                for (var i in data)
                  eval("document.forms['getDisplay'].company.options["+(c++)+"] = new Option('" + data[i][1] + "', '" + data[i][0] + "');");
            }
            
            // Get Companies
            function getCompanies(select) {
                ClearOptions(document.forms['getDisplay'].company);
                if (select.value != "null"){
                    ShowLayer("companyDiv", "block");
                }else{
                    ShowLayer("companyDiv", "none");
                }
                x_getCompanies(select.value, setCompanies);
            }
        </script>
        <?php
        //  Set Current Days Date
        $today = date("Y-m-d");

        $where = "";

        //if ($_GET["type"] == "SP")      
            $where2 .= "AND p.p_type = 'SP' AND pu.spm_manager = '1' ";

        //  Company Projects
        $projects = q("SELECT DISTINCT(p.id), p.name, p_type FROM (Project AS p 
                                INNER JOIN Project_User AS pu ON p.id = pu.project_id 
                                INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) 
                                WHERE pu.user_id = '".$_SESSION["user_id"]."' 
                                AND cot.company_id = '".$_SESSION["company_id"]."' 
                                AND pu.company_id = '".$_SESSION["company_id"]."' 
                                AND p.completed = '0' 
                                AND p.deleted = '0' 
                                AND pu.deleted = '0' $where2
                                ORDER BY UPPER(p.name)");

        echo "<table width='100%'>";
            echo "<tr>";
                echo "<td class='centerdata'>";
                    echo "<form action='' method='post' name='getDisplay'>";
                        echo "<table width='100%'>";
                            echo "<tr>";
                                echo "<td class='centerdata'>";
                                    echo "<h6>";
                                        echo "Approval Time/Expense Report";
                                    echo "</h6>";
                                echo "</td>";
                            echo "</tr>";
                        echo "</table>";
                        
                            echo "<br/>";
                        echo "<table width='100%'>";
                            echo "<tr>";
                                echo "<td class='on-description' width='50%'>";
                                    echo "Select Project:";
                                echo "</td>";
                                echo "<td  width='50%'>";
                                    echo "<select method='post' id='project' name='project' class='on-field required' tabindex='1' onChange='getCompanies(project)'>";
                                        echo "<option value='null'>--  Select Project  --</option>";
                                        if (is_array($projects))    {
                                            foreach ($projects as $project) {
                                                $value = $project[0];

                                                if ($_POST["project"] == $value)
                                                    echo "<option value='".$value."' selected>".$project[1]."</option>";
                                                else
                                                    echo "<option value='".$value."'>".$project[1]."</option>";
                                            }
                                        }
                                    echo "</select>";
                                    echo "<div id='projectDiv' style='display: none;'><font class='on-validate-error'>* Project must be selected</font></div>";
                                echo "</td>";
                            echo "</tr>";
                        echo "</table>";
                        
                        echo " <div id='companyDiv' style='display: none;'> ";
                            echo "<table width='100%'>";
                            echo "<tr>";
                                echo "<td class='on-description' width='50%'>";
                                    echo "Select Company:";
                                echo "</td>";
                                echo "<td  width='50%'>";
                                    echo "<select method='post' id='company' name='company' class='on-field required' tabindex='1'>";
                                        echo "<option value='null'>--  Select Company  --</option>";
                                        if (is_array($companies))    {
                                            foreach ($companies as $company) {
                                                $value = $company[0];

                                                if ($_POST["company"] == $value)
                                                    echo "<option value='".$value."' selected>".$company[1]."</option>";
                                                else
                                                    echo "<option value='".$value."'>".$company[1]."</option>";
                                            }
                                        }
                                    echo "</select>";
                                    echo "<div id='projectDiv' style='display: none;'><font class='on-validate-error'>* Project must be selected</font></div>";
                                echo "</td>";
                            echo "</tr>";
                        echo "</table>";
                    echo "</div>";
                            
                        echo "<table width='100%'>";
                            echo "<tr>";
                                echo "<td class='on-description' width='50%'>";
                                    echo "Select Status:";
                                echo "</td>";
                                echo "<td  width='50%'>";
                                    echo "<select method='post' id='status' name='status' class='on-field required' tabindex='1'>";
                                        echo "<option value='null' selected>All</option>";
                                        echo "<option value='0'>Unapproved</option>";
                                        echo "<option value='1'>Updated</option>";
                                        echo "<option value='2'>Approved</option>";
                                        echo "<option value='3'>Declined</option>";
                                    echo "</select>";
                                    echo "<div id='projectDiv' style='display: none;'><font class='on-validate-error'>* Project must be selected</font></div>";
                                echo "</td>";
                            echo "</tr>";
                            
                            echo "<tr>";
                                echo "<td class='on-description' width='50%'>";
                                    echo "Date From:";
                                echo "</td>";
                                echo "<td  width='50%'>";
                                    echo "<input id='date_from' name='date_from' class='on-field-date date' type='text' style='text-align:right;' value='".getMonthStart($today)."'>";
                                    echo "<div id='dateFrom' style='display: none;'><font class='on-validate-error'>* Date must be entered</font></div>";
                                    echo "<div id='dateFromDiv' style='display: none;'><font class='on-validate-error'>* Date not valid, eg. ".date("Y-m-d")."</font></div>";
                                echo "</td>";
                            echo "</tr>";
                            echo "<tr>";
                                echo "<td class='on-description' width='50%'>";
                                    echo "Date To:";
                                echo "</td>";
                                echo "<td  width='50%'>";
                                    echo "<input id='date_to' name='date_to' class='on-field-date date' type='text' style='text-align:right;' value='".getMonthEnd($today)."'>";
                                    echo "<div id='dateTo' style='display: none;'><font class='on-validate-error'>* Date must be entered</font></div>";
                                    echo "<div id='dateToDiv' style='display: none;'><font class='on-validate-error'>* Date not valid, eg. ".date("Y-m-d")."</font></div>";
                                echo "</td>";
                            echo "</tr>";
                        echo "</table>";
                        echo "<br/>";
                        echo "<input name='btnDisplay' onClick='display();' tabindex='2' type='button' value='Display Information'>";
                        echo "<input method='post' name='save' type='hidden' value='0' />";
                    echo "</form>";

                    //  If Display Button Is Pressed
                                        //  If Display Button Is Pressed
                    if (isset($_POST["save"]) && $_POST["save"] === "1")    {
                        ///////////////////////////
                        
                        GLOBAL $excelheadings;
                        GLOBAL $exceldata;
                        
                        //  Get Information - Time
                        $projectID = $_POST["project"];
                        $project_name = q("SELECT name FROM Project WHERE id = '".$projectID."'");
                        
                        $approveStatus = $_POST["status"];
                        $date_from = addslashes(strip_tags($_POST["date_from"]));
                        $date_to = addslashes(strip_tags($_POST["date_to"]));

                        $projectType = q("SELECT p_type FROM Project WHERE id = '".$projectID."'");
                        $projectCID = q("SELECT company_id FROM Project WHERE id = '".$projectID."'");

                        $isSharedProject = ($projectType == "CP") ? 0 : 1;
                        if($approveStatus == "all")
                            $where = ($isSharedProject == 1) ? "AND (ta.status = '2' AND ta.status2 = '2') OR (ta.status = '3' AND ta.status2 = '3') " : "AND ta.status = '2' OR ta.status = '3' ";
                        else if($approveStatus == "0")
                            $where = ($isSharedProject == 1) ? "AND ta.status = '0' AND ta.status2 = '0' " : "AND ta.status = '0' ";
                        else if($approveStatus == "1")
                            $where = ($isSharedProject == 1) ? "AND ta.status = '1' AND ta.status2 = '1' " : "AND ta.status = '1' ";
                        else if($approveStatus == "2")
                            $where = ($isSharedProject == 1) ? "AND ta.status = '2' AND ta.status2 = '2' " : "AND ta.status = '2' ";
                        else if($approveStatus == "3")
                            $where = ($isSharedProject == 1) ? "AND ta.status = '3' AND ta.status2 = '3' " : "AND ta.status = '3' ";

                        if($_POST["company"] && $_POST["company"] != "null")
                            $where .= "AND cu.company_id = '".$_POST["company"]."' ";
                        else if($projectCID != $_SESSION["company_id"])
                            $where .= "AND cu.company_id = '".$_SESSION["company_id"]."' ";
                            
                            ////
                            $managerType = ($isSharedProject == 1) ? "AND ta.status = '2' AND ta.status2 = '2' " : "AND ta.status = '2' ";
                            $checkAreas = q("SELECT ar.name FROM (ApprovedTime AS ta 
                                                        INNER JOIN areas AS ar ON ar.id = ta.area_id) 
                                                        WHERE ta.project_id = '$projectID' $managerType");
                                                        
                           //if(is_array($checkAreas) || strlen($checkAreas >1)){
                                //$areaSelect = ", ar.name";                
                                //$areaJoin = " INNER JOIN areas AS ar ON ar.id = ta.area_id ";
                                $areaTH = "<th>Area</th>";
                                $hasArea = 1;
                                $areaTime = 1;
                           // }else{
                           //     $areaSelect = "";                
                           //     $areaJoin = "";
                           //     $areaTH ="";
                           //     $hasArea = 0;
                           // } 
                            
                         if($isSharedProject == 1){
                            $status2 = "ta.status2,";
                            $statusBool = 1;
                        }else{
                            $status2 = "";
                            $statusBool = 0;
                        }
                                
                        $records = q("SELECT DISTINCT(ta.id), CONCAT(e.lstname, ', ', e.frstname) AS emp, ta.date, at.type, a.name, ta.descr, ta.status, ".$status2." ta.rate, ta.time_spent 
                                                ,ar.name
                                                FROM (ApprovedTime AS ta 
                                                INNER JOIN Activities AS a ON ta.activity_id = a.id 
                                                INNER JOIN ActivityTypes AS at ON a.parent_id = at.id 
                                                INNER JOIN Employee AS e ON e.id = ta.user_id 
                                                LEFT JOIN areas AS ar ON ar.id = ta.area_id
                                                INNER JOIN Company_Users AS cu ON e.id = cu.user_id) 
                                                WHERE ta.project_id = '$projectID' 
                                                $where 
                                                AND ta.date >= '$date_from' 
                                                AND ta.date <= '$date_to' ORDER BY ta.date");
                                                
                        ///////////////////////////
                        //  Display Information
                        $display = "";
                        $total_time = 0;

                        //  Timestamp
                        if ($date_from === $date_to)    $timestamp = "Time Period: ".$date_from;
                        else                            $timestamp = "Time Period: ".$date_from." - ".$date_to;
                        
                        $excelheadings[$row][] = "";      
                        $excelheadings[$row][] = "";
                        $excelheadings[$row][] = "";
                        $excelheadings[$row][] = "";
                        $excelheadings[$row][] = "";
                        $excelheadings[$row][] = "";
                        $excelheadings[$row][] = "";
                    if($isSharedProject == 1)
                        $excelheadings[$row][] = "";
                        $excelheadings[$row][] = "";
                        $excelheadings[$row][] = "";
                            $row++;
                            
                        $exceldata[$row][] = "Approval Report (Time)";             
                        $exceldata[$row][] = "";                        
                        $exceldata[$row][] = "";                 
                        $exceldata[$row][] = "";                            
                        $exceldata[$row][] = "";                            
                        $exceldata[$row][] = "";                 
                        $exceldata[$row][] = ""; 
                    if($isSharedProject == 1)
                        $exceldata[$row][] = "";        
                        $exceldata[$row][] = "";        
                        $exceldata[$row][] = ""; 
                            $row++;
                            
                        $exceldata[$row][] = "Project Name: ".$project_name;             
                        $exceldata[$row][] = "";                        
                        $exceldata[$row][] = "";                 
                        $exceldata[$row][] = "";                            
                        $exceldata[$row][] = "";                            
                        $exceldata[$row][] = "";                 
                        $exceldata[$row][] = ""; 
                    if($isSharedProject == 1)
                        $exceldata[$row][] = "";        
                        $exceldata[$row][] = "";        
                        $exceldata[$row][] = ""; 
                            $row++;
                            
                        $exceldata[$row][] = $timestamp;             
                        $exceldata[$row][] = "";                        
                        $exceldata[$row][] = "";                 
                        $exceldata[$row][] = "";                            
                        $exceldata[$row][] = "";                            
                        $exceldata[$row][] = "";                 
                        $exceldata[$row][] = ""; 
                    if($isSharedProject == 1)
                        $exceldata[$row][] = "";        
                        $exceldata[$row][] = "";        
                        $exceldata[$row][] = ""; 
                            $row++;
                        
                        $exceldata[$row][] = "Employee Name";             
                        $exceldata[$row][] = "Date";                        
                        $exceldata[$row][] = "Area";                 
                        $exceldata[$row][] = "Activity Type";                            
                        $exceldata[$row][] = "Activity";                            
                        $exceldata[$row][] = "Description";                 
                        $exceldata[$row][] = "PM Status";   
                    if($isSharedProject == 1)
                        $exceldata[$row][] = "SPM Status";        
                        $exceldata[$row][] = "Time Spent";        
                        $exceldata[$row][] = "Cost"; 
                            $row++;

                        //  Table Headers
                        $report_heading = "<tr><td class='on-table-clear' colspan='100%'><a>Approval<font style='color:orange'> Time </font>Report</a></td></tr>
                                                <tr><td class='on-table-clear' colspan='100%'>Project Name: ".$project_name."</td></tr>".
                                            "<tr><td class='on-table-clear' colspan='100%'>".$timestamp."</td></tr>";
                        $headers1 = "<tr>".
                                        "<th>Employee Name</th>".
                                        "<th>Date</th>".
                                        $areaTH.
                                        "<th>Activity Type</th>".
                                        "<th>Activity</th>".
                                        "<th>Description</th>".
                                        "<th>PM Status</th>";
                        if($isSharedProject == 1)
                            $headers1 .= "<th>SPM Status</th>";
                        $headers1 .= "<th>Time Spent</th>".
                                        "<th>Cost <i>(".$_SESSION["currency"].")</i></th>".
                                    "</tr>";
                                    
                        $status = array();
                        $status[0] = "<font class='on-validate-error'>Unapproved</font>";
                        $status[1] = "Updated";
                        $status[2] = "Approved";
                        $status[3] = "Declined";

                        //  Table Information
                        if (is_array($records)) {
                            foreach ($records as $record) {
                                $status[0] = "<font class='on-validate-error'>Unapproved</font>";
                                $display .= "<tr>";
                                    $display .= "<td style='white-space:nowrap'>".$record[1]."</td>";   //  Employee Name
                                    $display .= "<td style='white-space:nowrap' class='rightdata'>".$record[2]."</td>";  //  Date
                                if($hasArea)
                                    $display .= "<td style='white-space:nowrap'>".$record[9+$statusBool]."</td>";    // Area
                                    $display .= "<td style='white-space:nowrap'>".$record[3]."</td>";   //  Activity Type
                                    $display .= "<td style='white-space:nowrap'>".$record[4]."</td>";   //  Activity
                                    $display .= "<td>".$record[5]."</td>";    //  Description
                                    $display .= "<td style='white-space:nowrap'>".$status[$record[6]]."</td>";   //  PM Status
                                if($status2 != "")
                                    $display .= "<td style='white-space:nowrap'>".$status[$record[7]]."</td>";   //  SPM Status

                                    //  Time Spent    
                                    $total_time += $record[8+$statusBool];

                                    $display .= "<td class='rightdata'>".number_format((double)$record[8+$statusBool], 2, ".", "")."</td>";

                                    //  Cost
                                    $total_cost += ($record[7+$statusBool] * $record[8+$statusBool]);
                               
                                    $display .= "<td class='rightdata'>".number_format((double)($record[7+$statusBool] * $record[8+$statusBool]), 2, ".", "")."</td>";
                                $display .= "</tr>";
                                
                                $status[0] = "Unapproved";
                                
                                $exceldata[$row][] = $record[1];      
                                $exceldata[$row][] = $record[2];
                                $exceldata[$row][] = $record[9+$statusBool];
                                $exceldata[$row][] = $record[3];
                                $exceldata[$row][] = $record[4];
                                $exceldata[$row][] = $record[5];
                                $exceldata[$row][] = $status[$record[6]];
                            if($isSharedProject == 1)
                                $exceldata[$row][] = $status[$record[7]];
                                $exceldata[$row][] = $record[8+$statusBool];
                                $exceldata[$row][] = $record[7+$statusBool] * $record[8+$statusBool];
                                    $row++;
                            }
                        }
                        else
                            $display .= "<tr><td class='centerdata' colspan='100%'>No information to display</td></tr>";

                        if ($total_time > 0 || $total_cost > 0) {
                            $display .= "<tr>";
                                $display .= "<td class='on-table-total' colspan='".(6+$hasArea+$statusBool)."'>Total:</td>";
                                $display .= "<td class='on-table-total'>".number_format((double)$total_time, 2, ".", "")."</td>";
                                $display .= "<td class='on-table-total'>".number_format((double)$total_cost, 2, ".", "")."</td>";
                            $display .= "</tr>";
                            
                            $exceldata[$row][] = "";      
                            $exceldata[$row][] = "";
                            $exceldata[$row][] = "";
                            $exceldata[$row][] = "";
                            $exceldata[$row][] = "";
                            $exceldata[$row][] = "";
                        if($isSharedProject == 1)
                            $exceldata[$row][] = "";
                            $exceldata[$row][] = "Total:";
                            $exceldata[$row][] = $total_time;
                            $exceldata[$row][] = $total_cost;
                                $row++;
                        }

                        $displayTime = $report_heading.$headers1.$display;
                        ///////////////////////////

                        //  EXPENSES
                        ///////////////////////////
                        //  Get Information - Expenses
                        if($approveStatus == "all")
                            $where = ($isSharedProject == 1) ? "AND (ae.status = '2' AND ae.status2 = '2') OR (ae.status = '3' AND ae.status2 = '3') " : "AND ae.status = '2' OR ae.status = '3' ";
                        else if($approveStatus == "0")
                            $where = ($isSharedProject == 1) ? "AND ae.status = '0' AND ae.status2 = '0' " : "AND ae.status = '0' ";
                        else if($approveStatus == "1")
                            $where = ($isSharedProject == 1) ? "AND ae.status = '1' AND ae.status2 = '1' " : "AND ae.status = '1' ";
                        else if($approveStatus == "2")
                            $where = ($isSharedProject == 1) ? "AND ae.status = '2' AND ae.status2 = '2' " : "AND ae.status = '2' ";
                        else if($approveStatus == "3")
                            $where = ($isSharedProject == 1) ? "AND ae.status = '3' AND ae.status2 = '3' " : "AND ae.status = '3' ";
                        
                        if($_POST["company"] && $_POST["company"] != "null")
                            $where .= "AND cu.company_id = '".$_POST["company"]."' ";
                        else if ($projectCID != $_SESSION["company_id"])
                            $where .= "AND cu.company_id = '".$_SESSION["company_id"]."' ";

                         $managerType = ($isSharedProject == 1) ? "AND ae.status = '2' AND ae.status2 = '2' " : "AND ae.status = '2' ";
                            $checkAreas = q("SELECT ar.name FROM (ApprovedExpense AS ae 
                                                        INNER JOIN areas AS ar ON ar.id = ae.area_id) 
                                                        WHERE ae.project_id = '$projectID' $managerType");
                                                        
                           //if(is_array($checkAreas) || strlen($checkAreas >1)){
                           //     $areaSelect = ", ar.name";                
                           //     $areaJoin = " INNER JOIN areas AS ar ON ar.id = ae.area_id ";
                                $areaTH = "<th>Area</th>";
                                $hasArea = 1;
                           // }else{
                           //     $areaSelect = "";                
                          //      $areaJoin = "";
                          //      $areaTH ="";
                          //      $hasArea = 0;
                          //  } 
                            
                            if($areaTime = 1){
                                $areaTH = "<th>Area</th>";
                                $hasArea = 1;
                            }
                            
                         if($isSharedProject == 1){
                            $status2 = "ae.status2,";
                            $statusBool = 1;
                        }else{
                            $status2 = "";
                            $statusBool = 0;
                        }
                        
                        $records = q("SELECT DISTINCT(ae.id), CONCAT(e.lstname, ', ', e.frstname) AS emp, ae.date, v.type, ae.descr, ae.status, ".$status2." ae.kilometers, ae.expense 
                                                ,ar.name
                                                FROM (ApprovedExpense AS ae 
                                                LEFT JOIN Vehicle AS v ON ae.vehicle_id = v.id 
                                                INNER JOIN Employee AS e ON e.id = ae.user_id
                                                LEFT JOIN areas AS ar ON ar.id = ae.area_id
                                                INNER JOIN Company_Users AS cu ON e.id = cu.user_id) 
                                                WHERE ae.project_id = '$projectID' 
                                                $where 
                                                AND ae.date >= '$date_from' 
                                                AND ae.date <= '$date_to' ORDER BY ae.date");

                        ///////////////////////////
                        //  Display Information
                        $display = "";
                        $total_cost2 = 0;

                        //  Table Headers
                        $report_heading = "<tr><td class='on-table-clear' colspan='100%'><a>Approval<font style='color:orange'> Expense </font>Report</a></td></tr>
                                                <tr><td class='on-table-clear' colspan='100%'>Project Name: ".$project_name."</td></tr>".
                                            "<tr><td class='on-table-clear' colspan='100%'>".$timestamp."</td></tr>";
                        $headers2 = "<tr>".
                                        "<th>Employee Name</th>".
                                        "<th>Date</th>".
                                        $areaTH.
                                        "<th>Vehicle</th>".
                                        "<th colspan='2'>Description</th>".
                                        "<th>PM Status</th>";
                        if($isSharedProject == 1)
                            $headers2 .= "<th>SPM Status</th>";
                        $headers2 .= "<th>Kilometers</th>".
                                        "<th>Expense <i>(".$_SESSION["currency"].")</i></th>".
                                    "</tr>";
                                    
                        $status = array();
                        $status[0] = "<font class='on-validate-error'>Unapproved</font>";
                        $status[1] = "Updated";
                        $status[2] = "Approved";
                        $status[3] = "Declined";

                        //  Table Information
                        if (is_array($records)) {
                        
                            $exceldata[$row][] = "";      
                            $exceldata[$row][] = "";
                            $exceldata[$row][] = "";
                            $exceldata[$row][] = "";
                            $exceldata[$row][] = "";
                            $exceldata[$row][] = "";
                            $exceldata[$row][] = "";
                        if($isSharedProject == 1)
                            $exceldata[$row][] = "";
                            $exceldata[$row][] = "";
                            $exceldata[$row][] = "";
                                $row++;
                                
                            $exceldata[$row][] = "Approval Report (Expense)";      
                            $exceldata[$row][] = "";
                            $exceldata[$row][] = "";
                            $exceldata[$row][] = "";
                            $exceldata[$row][] = "";
                            $exceldata[$row][] = "";
                            $exceldata[$row][] = "";
                        if($isSharedProject == 1)
                            $exceldata[$row][] = "";
                            $exceldata[$row][] = "";
                            $exceldata[$row][] = "";
                                $row++;
                                
                            $exceldata[$row][] = "Employee Name";             
                            $exceldata[$row][] = "Date";                        
                            $exceldata[$row][] = "Area";                 
                            $exceldata[$row][] = "Vehicle";                            
                            $exceldata[$row][] = "";                            
                            $exceldata[$row][] = "Description";                 
                            $exceldata[$row][] = "PM Status";   
                        if($isSharedProject == 1)
                            $exceldata[$row][] = "SPM Status";        
                            $exceldata[$row][] = "KM/Amount";        
                            $exceldata[$row][] = "Expense"; 
                                $row++;
                        
                            foreach ($records as $record)   {
                                $status[0] = "<font class='on-validate-error'>Unapproved</font>";
                                $display .= "<tr>";
                                    $display .= "<td style='white-space:nowrap'>".$record[1]."</td>";   //  Employee Name
                                    $display .= "<td style='white-space:nowrap' class='rightdata'>".$record[2]."</td>";   //  Date
                                    if($hasArea)
                                        $display .= "<td style='white-space:nowrap'>".$record[8+$statusBool]."</td>";    // Area
                                    if($record[3] == "")
                                        $display .= "<td style='white-space:nowrap'>-</td>";   //  Vehicle
                                    else
                                        $display .= "<td style='white-space:nowrap'>".$record[3]."</td>";   //  Vehicle
                                    $display .= "<td colspan='2'>".$record[4]."</td>";   //  Description
                                    $display .="<td style='white-space:nowrap'>".$status[$record[5]]."</td>";      // PM Status
                                      if($status2 != "")
                                        $display .="<td style='white-space:nowrap'>".$status[$record[6]]."</td>";      // SPM Status
                                    
                                    $display .= "<td class='rightdata'>".number_format((double)$record[6+$statusBool], 2, ".", "")."</td>";    //  Kilometers

                                    //  Cost
                                    $total_expense += $record[7+$statusBool];
                                    $display .= "<td class='rightdata'>".number_format((double)$record[7+$statusBool], 2, ".", "")."</td>";   // Expense
                                $display .= "</tr>";
                                
                                $status[0] = "Unapproved";
                                
                                $exceldata[$row][] = $record[1];      
                                $exceldata[$row][] = $record[2];
                                $exceldata[$row][] = $record[8+$statusBool];
                                $exceldata[$row][] = $record[3];
                                $exceldata[$row][] = "";
                                $exceldata[$row][] =$record[4];
                                $exceldata[$row][] = $status[$record[5]];
                            if($isSharedProject == 1)
                                $exceldata[$row][] = $status[$record[6]];
                                $exceldata[$row][] =$record[6+$statusBool];
                                $exceldata[$row][] = $record[7+$statusBool];
                                    $row++;
                            }

                            if ($total_expense > 0) {
                                $display .= "<tr>";
                                    $display .= "<td class='on-table-total' colspan='".(7+$hasArea+$statusBool)."'>Total:</td>";
                                    $display .= "<td class='on-table-total'>".number_format((double)$total_expense, 2, ".", "")."</td>";
                                $display .= "</tr>";
                                
                                $exceldata[$row][] = "";      
                                $exceldata[$row][] = "";  
                                $exceldata[$row][] = "";  
                                $exceldata[$row][] = "";  
                                $exceldata[$row][] = "";
                                $exceldata[$row][] = "";  
                                $exceldata[$row][] = "";  
                            if($isSharedProject == 1)
                                $exceldata[$row][] = "";  
                                $exceldata[$row][] = "Total: ";  
                                $exceldata[$row][] = $total_expense; 
                                    $row++;
                            }

                            $display .= "<tr>";
                                $display .= "<td class='on-table-total' colspan='".(7+$hasArea+$statusBool)."'>Grand Total:</td>";
                                $display .= "<td class='on-table-total'>".number_format((double)$total_expense + $total_cost, 2, ".", "")."</td>";
                            $display .= "</tr>";
                            
                            $exceldata[$row][] = "";      
                            $exceldata[$row][] = "";  
                            $exceldata[$row][] = "";  
                            $exceldata[$row][] = "";  
                            $exceldata[$row][] = "";
                            $exceldata[$row][] = "";  
                            $exceldata[$row][] = ""; 
                        if($isSharedProject == 1)
                            $exceldata[$row][] = "";  
                            $exceldata[$row][] = "Grand Total: ";  
                            $exceldata[$row][] = $total_expense + $total_cost; 
                                $row++;

                            $displayExpenses = $report_heading.$headers2.$display;
                        }

                        echo "<br/>";
                        
                        if($displayTime != "" && $displayExpenses != ""){
                            echo "<div class='on-20px'><table class='on-table-center on-table'>".$displayTime."<tr><td style='background-color:transparent' colspan='100%'>&nbsp;</td></tr>".$displayExpenses."</table></div>";
                        }else{
                            if ($displayTime != "") echo "<div class='on-20px'><table class='on-table-center on-table'>".$displayTime."</table></div>";
                            if ($displayExpenses != "") echo "<div class='on-20px'><table class='on-table-center on-table'>".$displayExpenses."</table></div>";
                        }

                                               ///////////////////////////
                        //  Set Export Information
                        $_SESSION["fileName"] = "Approval Report";
                        $_SESSION["fileData"] = array_merge($excelheadings, $exceldata);
                        ///////////////////////////

                        echo "<br><input name='btnExport' onClick=\"location.href='_export.php'\" type='button' value='Export to Excel' /><br><br>";

                    }
                echo "</td>";
            echo "</tr>";
        echo "</table>";

    //  Print Footer
    print_footer();
?>
