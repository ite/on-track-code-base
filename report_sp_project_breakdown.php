<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("include/sajax.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");
        
    if (!hasAccess("REP_SP_BREAK"))
        header("Location: noaccess.php");
        
    // Class for table background color
    $colorClass = "OnTrack_#_";            
        
    function getCompanies($id) {
       return q("SELECT DISTINCT(c.id), c.name FROM (Company AS c 
                    INNER JOIN companiesOnTeam AS cot ON c.id = cot.company_id 
                    INNER JOIN Project AS p ON p.id =  cot.project_id) 
                    WHERE cot.project_id = '$id' ORDER BY c.name"); 
    }
    
    function getAreas($id){
        return q("SELECT ar.id, ar.name FROM (areas AS ar 
                        INNER JOIN Project as p ON ar.pID = p.id) 
                        WHERE ar.active = '1' 
                        AND p.p_type='SP' 
                        AND ar.pID='".$id."' ORDER BY ar.name");
    } 

    $sajax_request_type = "GET";
    sajax_init();
    sajax_export("getCompanies");
    sajax_export("getAreas");
    sajax_handle_client_request();
    ///////////////////////////

    //  Set Report Generated Status
    $generated                                                          = "0";

    //  Create Display Information Function
    if (isset($_POST["save"]) && $_POST["save"] === "1") {
        $errorMessage                                                   = "";

        $reportOn = $_POST["reportOn"];

        $project_id                                                     = $_POST["project"];
        $company_id                                                  = $_POST["company"];
        $area_id                                                          = $_POST["area"];
        $displayString                                                = "";

        ///////////////////////////
        //  Get Info From Tables
        ///////////////////////////
        $project_name                                                  = q("SELECT name FROM Project WHERE id = '$project_id' ");
        if($company_id  == "null")   // All companies
            $project_budget                                               = q("SELECT consulting FROM companiesOnTeam WHERE project_id = '$project_id' AND company_id = '0' ");
        else                                  // Selected Company
            $project_budget                                               = q("SELECT consulting FROM companiesOnTeam WHERE project_id = '$project_id' AND company_id = '$company_id' ");
            
        if($project_budget == "")
            $project_budget = 0;
            
        $company_name                                              = q("SELECT name FROM Company WHERE id = '$company_id' ");
        $allCompanies                                                 = q("SELECT c.id, c.name FROM (Company AS c 
                                                                                        INNER JOIN companiesOnTeam AS cot ON cot.company_id = c.id) 
                                                                                        WHERE cot.project_id = '$project_id' 
                                                                                        AND c.id = cot.company_id ORDER BY c.name ASC");
        $area_name                                                      = q("SELECT name FROM areas WHERE id = '$area_id' ");
        
        ///////////////////////// Excel Export Start ///////////////////////////
        $row                                                                  = 0;
        
        $excelheadings[$row][]                                     = ""; 
        $excelheadings[$row][]                                     = "";
        $excelheadings[$row][]                                     = ""; 
        $excelheadings[$row][]                                     = ""; 
        $excelheadings[$row][]                                     = ""; 
        $excelheadings[$row][]                                     = ""; 
        $excelheadings[$row][]                                     = "";        // 7
            $row++;    
            
        $exceldata[$row][]                                                 = "Report: Shared Project Time Breakdown";
        $exceldata[$row][]                                                 = "";
        $exceldata[$row][]                                                 = "";
        $exceldata[$row][]                                                 = "";
        $exceldata[$row][]                                                 = "";
        $exceldata[$row][]                                                 = "";
        $exceldata[$row][]                                                 = "";
            $row++;    
            
        $exceldata[$row][]                                                 = "Shared Project: ".$project_name;
        $exceldata[$row][]                                                 = "";
        $exceldata[$row][]                                                 = "";
        $exceldata[$row][]                                                 = "";
        $exceldata[$row][]                                                 = "";
        $exceldata[$row][]                                                 = "";
        $exceldata[$row][]                                                 = "";
            $row++;    
            
        if($company_name != ""){
            $companyName                                                = $company_name;
        }else{
            $companyName                                                = "All Companies";
        }
        $exceldata[$row][]                                                 = "Company: ".$companyName;
        $exceldata[$row][]                                                 = "";
        $exceldata[$row][]                                                 = "";
        $exceldata[$row][]                                                 = "";
        $exceldata[$row][]                                                 = "";
        $exceldata[$row][]                                                 = "";
        $exceldata[$row][]                                                 = "";
            $row++;
        
        if($area_name != ""){
            $exceldata[$row][]                                                 = "Area: ".$area_name;
            $exceldata[$row][]                                                 = "";
            $exceldata[$row][]                                                 = "";
            $exceldata[$row][]                                                 = "";
            $exceldata[$row][]                                                 = "";
            $exceldata[$row][]                                                 = "";
            $exceldata[$row][]                                                 = "";
                $row++;
        }

        $exceldata[$row][]                                               = $colorClass."Employee Name"; 
        $exceldata[$row][]                                               = $colorClass."Total Budget - Allocated/Person";
        $exceldata[$row][]                                               = $colorClass."Total Time - Allocated/Person"; 
        $exceldata[$row][]                                               = $colorClass."Total Budget - Used/Person"; 
        $exceldata[$row][]                                               = $colorClass."Total Time - Used/Person"; 
        $exceldata[$row][]                                               = $colorClass."Total Budget - Left/Person"; 
        $exceldata[$row][]                                               = $colorClass."Total Time - Left/Person"; 
            $row++;
        /////////////////////////////////////////////////////////////////////////////////////////////////
        
        $totalUsedViewed = 0;

        $table = ($reportOn == "approved") ? "ApprovedTime" : "TimeSheet";
        $where = ($reportOn == "approved") ? "AND ts.status2 = '2' " : "";

        // HIER : Doen die reg
            $timeUsedAll                                                   = q("SELECT SUM(time_spent * rate) FROM $table 
                                                                                            WHERE project_id = '".$project_id."' $where");
                                                                                        
            //echo "<a>Total: $timeUsedAll</a><br>";

        // (1) - All Companies (All Areas) OR All companies (Selected Area) 
        if($company_name == ""){        
            foreach($allCompanies as $company){
                $table = ($reportOn == "approved") ? "ApprovedTime" : "TimeSheet";
                $where = ($reportOn == "approved") ? "AND ts.status2 = '2' " : "";

                if($area_id > 0){
                    $where .= " AND area_id = '$area_id'";
                }
                $checkData = q("SELECT id, id FROM $table WHERE project_id = '".$project_id."' AND company_id = '$company[0]' $where ");
                if(is_array($checkData)){
                    $displayString                                              .= "<tr>
                                                                                                <td class='on-table-clear' colspan='100%'><a>Company: $company[1]</a></td>
                                                                                            </tr>";
                    $employees                                                  = q("SELECT e.id, e.lstname, e.frstname, pu.user_budget FROM (Employee as e 
                                                                                                INNER JOIN Project_User AS pu ON e.id = pu.user_id) 
                                                                                                WHERE pu.project_id = '$project_id' 
                                                                                                AND pu.company_id = '$company[0]' AND e.deleted = '0'
                                                                                                ORDER BY pu.manager DESC, e.lstname, e.frstname ASC");
                                                                                                
                    $exceldata[$row][]                                           = "Company Name: $company[1]";
                    $exceldata[$row][]                                           = "";
                    $exceldata[$row][]                                          = "";
                    $exceldata[$row][]                                          = "";
                    $exceldata[$row][]                                          = "";
                    $exceldata[$row][]                                          = "";
                    $exceldata[$row][]                                          = "";
                        $row++;
                        
                        $budgetAPP = 0;
                        $totalTimeAPP = 0;
                        $totalBudgetUPP = 0;
                        $totalTimeUPP = 0;
                        $totalBudgetLPP = 0;
                        $totalTimeLPP = 0;
                    
                    if (is_array($employees)) {                        
                        foreach ($employees as $employee) {
                            $table = ($reportOn == "approved") ? "ApprovedTime" : "TimeSheet";
                            $where = ($reportOn == "approved") ? "AND ts.status2 = '2' " : "";

                            $budget = q("SELECT user_budget FROM Project_User WHERE user_id = '".$employee[0]."' AND project_id = '".$project_id."' AND company_id = '$company[0]' ");

                            $rate = q("SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$employee[0]."' ".
                                        "AND pu.company_id = '".$company[0]."' AND pu.project_id = '".$project_id."' AND ur.companyid = '".$company[0]."' AND ur.userid = '".$employee[0]."'");

                            //$tariff_id                                          = q("SELECT user_tariff FROM Project_User WHERE user_id = '".$employee[0]."' AND project_id = '".$project_id."' AND company_id = '$company[0]' ");  
                            //$rate                                               = q("SELECT tariff".$tariff_id." FROM Employee WHERE id = '".$employee[0]."'");
                            $used                                                   = 0;
                            
                            if($area_id > 0){
                                $where = " AND area_id = '$area_id'";
                            }
                            $time                                                   = q("SELECT SUM(time_spent * rate) FROM TimeSheet WHERE user_id = '".$employee[0]."' AND project_id = '".$project_id."' 
                                                                                                AND company_id = '$company[0]' $where ");                            
                            if( !$time  ){ 
                                $used = 0;
                            }else{
                                $used += ($time);
                            }
                            
                            // If there is budget used
                            if($used > 0){
                            
                                $displayString                                          .= "<tr>";
                                //  Employee Name
                                $displayString                                      .= "<td>".$employee[1].", ".$employee[2]."</td>";
                                //  Total Budget Allocated/Person
                                $displayString                                      .= "<td class='rightdata'>".number_format((double)$budget, 2, ".", "")."</td>";
                                //  Total Time Allocated/Person
                                $displayString                                      .= "<td class='rightdata'>".number_format((double)($budget / $rate), 2, ".", "")."</td>";
                                //  Total Budget Used/Person
                                $total                                                   += number_format((double)($used), 2, ".", "");
                                $displayString                                      .= "<td class='rightdata'>".number_format((double)($used), 2, ".", "")."</td>";
                                //  Total Time Used/Person
                                $displayString                                      .= "<td class='rightdata'>".number_format((double)($used / $rate), 2, ".", "")."</td>";
                                //  Total Budget Left/Person
                                if(($budget - $used) >= 0)
                                    $displayString                                      .= "<td class='rightdata'>".number_format((double)($budget - $used), 2, ".", "")."</td>";
                                else
                                    $displayString                                      .= "<td class='rightdata' style='color:#FF5900'>".number_format((double)($budget - $used), 2, ".", "")."</td>";
                                //  Total Time Left/Person
                                if(($budget - $used) >= 0)
                                    $displayString                                      .= "<td class='rightdata'>".number_format((double)(($budget - $used) / $rate), 2, ".", "")."</td>";
                                else
                                    $displayString                                      .= "<td class='rightdata' style='color:#FF5900'>".number_format((double)(($budget - $used) / $rate), 2, ".", "")."</td>";
                                $displayString                                          .= "</tr>";
                                
                                $exceldata[$row][]                            = $employee[1].", ".$employee[2];
                                $exceldata[$row][]                            = $budget;
                                $exceldata[$row][]                            = ($budget / $rate);
                                $exceldata[$row][]                            = $used;
                                $exceldata[$row][]                            = ($used / $rate);
                                $exceldata[$row][]                            = ($budget - $used);
                                $exceldata[$row][]                            = (($budget - $used) / $rate);
                                    $row++;
                                    
                                $budgetAPP += $budget;
                                $totalTimeAPP += ($budget / $rate);
                                $totalBudgetUPP += $used;
                                $totalTimeUPP += ($used/$rate);
                                $totalBudgetLPP += ($budget - $used);
                                $totalTimeLPP += (($budget - $used) / $rate);
                            }
                        } // END Foreach Employee            
                    }  
                    
                    if($totalBudgetUPP > 0){
                        $displayString                                              .= "<tr>";
                        $displayString                                                  .= "<td class='on-table-total'>Total:</td>";
                        $displayString                                                  .= "<td class='on-table-total'>".number_format((double)$budgetAPP,2,".","")."</td>";
                        $displayString                                                  .= "<td class='on-table-total'>".number_format((double)$totalTimeAPP,2,".","")."</td>";
                        $displayString                                                  .= "<td class='on-table-total'>".number_format((double)$totalBudgetUPP,2,".","")."</td>";
                        $displayString                                                  .= "<td class='on-table-total'>".number_format((double)$totalTimeUPP,2,".","")."</td>";
                        $displayString                                                  .= "<td class='on-table-total'>".number_format((double)$totalBudgetLPP,2,".","")."</td>";
                        $displayString                                                  .= "<td class='on-table-total'>".number_format((double)$totalTimeLPP,2,".","")."</td>";
                        $displayString                                              .= "</tr>";  
                        
                        $exceldata[$row][]                            = "Total: ";
                        $exceldata[$row][]                            = $budgetAPP;
                        $exceldata[$row][]                            = $totalTimeAPP;
                        $exceldata[$row][]                            = $totalBudgetUPP;
                        $exceldata[$row][]                            = $totalTimeUPP;
                        $exceldata[$row][]                            = $totalBudgetLPP;
                        $exceldata[$row][]                            = $totalTimeLPP;
                        $row++;
                        
                        // Total Viewed
                        $totalUsedViewed += $totalBudgetUPP;
                        
                        $displayString                                              .= "<tr><td style='background-color:#242220' colspan='100%'>&nbsp;</td></tr>";
                    }
                }
            } // END Foreach Comapy
        }else{
            // (2) - Selected Company (All Areas) OR Selected Company (Selected Area) 
            $employees                                                  = q("SELECT e.id, e.lstname, e.frstname, pu.user_budget FROM (Employee as e 
                                                                                        INNER JOIN Project_User AS pu ON e.id = pu.user_id) 
                                                                                        WHERE pu.project_id = '$project_id' 
                                                                                        AND pu.company_id = '$company_id' AND e.deleted = '0'
                                                                                        ORDER BY pu.manager DESC, e.lstname, e.frstname ASC");
            if (is_array($employees)) {
                foreach ($employees as $employee) {                    
                    $budget                                             = q("SELECT user_budget FROM Project_User WHERE user_id = '".$employee[0]."' AND project_id = '".$project_id."' AND company_id = '$company_id' ");

                    $rate = q("SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$employee[0]."' ".
                                "AND pu.company_id = '".$company[0]."' AND pu.project_id = '".$project_id."' AND ur.companyid = '".$company[0]."' AND ur.userid = '".$employee[0]."'");

                    //$tariff_id                                          = q("SELECT user_tariff FROM Project_User WHERE user_id = '".$employee[0]."' AND project_id = '".$project_id."' AND company_id = '$company[0]' ");  
                    //$rate                                               = q("SELECT tariff".$tariff_id." FROM Employee WHERE id = '".$employee[0]."'");

                    $used                                                   = 0;
                
                     if($area_id > 0){
                        $where = " AND area_id = '$area_id'";
                    }
                    $time                                                   = q("SELECT SUM(time_spent * rate) FROM TimeSheet WHERE user_id = '".$employee[0]."' AND project_id = '".$project_id."' 
                                                                                        AND company_id = '$company_id' $where ");
                    if( !$time ){
                        $used = 0;
                    }else{
                        $used += ($time);
                    }
                    
                    // If there is budget used
                    if($used > 0){
                    
                        $displayString                                          .= "<tr>";
                        //  Employee Name
                        $displayString                                      .= "<td>".$employee[1].", ".$employee[2]."</td>";
                        //  Total Budget Allocated/Person
                        $displayString                                      .= "<td class='rightdata'>".number_format((double)$budget, 2, ".", "")."</td>";
                        //  Total Time Allocated/Person
                        $displayString                                      .= "<td class='rightdata'>".number_format((double)($budget / $rate), 2, ".", "")."</td>";
                        //  Total Budget Used/Person
                        $total                                                  += number_format((double)($used), 2, ".", "");
                        $displayString                                      .= "<td class='rightdata'>".number_format((double)($used), 2, ".", "")."</td>";
                        //  Total Time Used/Person
                        $displayString                                      .= "<td class='rightdata'>".number_format((double)($used / $rate), 2, ".", "")."</td>";
                        //  Total Budget Left/Person
                        if(($budget - $used) >= 0)
                            $displayString                                      .= "<td class='rightdata'>".number_format((double)$budget - $used, 2, ".", "")."</td>";
                        else
                            $displayString                                      .= "<td class='rightdata' style='color:#FF5900'>".number_format((double)($budget - $used), 2, ".", "")."</td>";
                        //  Total Time Left/Person
                        if(($budget - $used) >= 0)
                            $displayString                                      .= "<td class='rightdata'>".number_format((double)(($budget - $used) / $rate), 2, ".", "")."</td>";
                        else
                            $displayString                                      .= "<td class='rightdata' style='color:#FF5900'>".number_format((double)(($budget - $used) / $rate), 2, ".", "")."</td>";
                        $displayString                                          .= "</tr>";
                        
                        $exceldata[$row][]                            = $employee[1].", ".$employee[2];
                        $exceldata[$row][]                            = $budget;
                        $exceldata[$row][]                            = ($budget / $rate);
                        $exceldata[$row][]                            = $used;
                        $exceldata[$row][]                            = ($used / $rate);
                        $exceldata[$row][]                            = ($budget - $used);
                        $exceldata[$row][]                            = (($budget - $used) / $rate);
                            $row++;
                            
                        $budgetAPP += $budget;
                        $totalTimeAPP += ($budget / $rate);
                        $totalBudgetUPP += $used;
                        $totalTimeUPP += ($used/$rate);
                        $totalBudgetLPP += ($budget - $used);
                        $totalTimeLPP += (($budget - $used) / $rate);
                    }
                } // END Foreach  Employee              
            }      
            $displayString                                              .= "<tr>";
            $displayString                                                  .= "<td class='on-table-total'>Total:</td>";
            $displayString                                                  .= "<td class='on-table-total'>".number_format((double)$budgetAPP,2,".","")."</td>";
            $displayString                                                  .= "<td class='on-table-total'>".number_format((double)$totalTimeAPP,2,".","")."</td>";
            $displayString                                                  .= "<td class='on-table-total'>".number_format((double)$totalBudgetUPP,2,".","")."</td>";
            $displayString                                                  .= "<td class='on-table-total'>".number_format((double)$totalTimeUPP,2,".","")."</td>";
            $displayString                                                  .= "<td class='on-table-total'>".number_format((double)$totalBudgetLPP,2,".","")."</td>";
            $displayString                                                  .= "<td class='on-table-total'>".number_format((double)$totalTimeLPP,2,".","")."</td>";
            $displayString                                              .= "</tr>"; 
            
            $exceldata[$row][]                            = "Total: ";
            $exceldata[$row][]                            = $budgetAPP;
            $exceldata[$row][]                            = $totalTimeAPP;
            $exceldata[$row][]                            = $totalBudgetUPP;
            $exceldata[$row][]                            = $totalTimeUPP;
            $exceldata[$row][]                            = $totalBudgetLPP;
            $exceldata[$row][]                            = $totalTimeLPP;
            $row++;
            
            // Total Viewed
            $totalUsedViewed += $totalBudgetUPP;
            
            $displayString                                              .= "<tr><td style='background-color:#242220' colspan='100%'>&nbsp;</td></tr>";
        }
        //  Total Used in current Viewed data
        $displayString                                              .= "<tr>
                                                                                        <td class='rightdata' colspan='3'>Total Used <i>(".$_SESSION["currency"].")</i>:</td>
                                                                                        <td class='rightdata'>".number_format((double)$totalUsedViewed, 2, ".", "")."</td>
                                                                                        <td class='rightdata' colspan='3'></td>
                                                                                </tr>";
        //  Total Used in NOT Viewed data
        if(($timeUsedAll - $totalUsedViewed ) != 0){
            $displayString                                              .= "<tr>
                                                                                            <td class='rightdata' colspan='3'>Total Used (Other Areas) <i>(".$_SESSION["currency"].")</i>:</td>
                                                                                            <td class='rightdata'>".number_format((double)($timeUsedAll - $totalUsedViewed) , 2, ".", "")."</td>
                                                                                            <td class='rightdata' colspan='3'></td>
                                                                                    </tr>";
        }
                                                                                
        //  Total Project Budget
        $displayString                                              .= "<tr>
                                                                                        <td class='on-table-total' colspan='3'>Total Project Budget (Consulting) <i>(".$_SESSION["currency"].")</i>:</td>
                                                                                        <td class='on-table-total'>".number_format((double)$project_budget, 2, ".", "")."</td>
                                                                                        <td class='on-table-total' colspan='3'></td>
                                                                                </tr>";
        //  Total Budget Left
        $displayString                                              .= "<tr>
                                                                                        <td class='on-table-total' colspan='3'>Total Budget Left (Consulting) <i>(".$_SESSION["currency"].")</i>:</td>
                                                                                        <td class='on-table-total'>".number_format((double)($project_budget - $timeUsedAll), 2, ".", "")."</td>
                                                                                        <td class='on-table-total' colspan='3'></td>
                                                                                </tr>";
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //  Create Table Headers
        $headers                                                        = "<tr>
                                                                                        <th>Employee Name</th>
                                                                                        <th>Total Budget Allocated/Person <i>(".$_SESSION["currency"].")</i></th>
                                                                                        <th>Total Time<br/>Allocated/Person</th>
                                                                                        <th>Total Budget<br/>Used/Person <i>(".$_SESSION["currency"].")</i></th>
                                                                                        <th>Total Time<br/>Used/Person</th>
                                                                                        <th>Total Budget<br/>Left/Person <i>(".$_SESSION["currency"].")</i></th>
                                                                                        <th>Total Time<br/>Left/Person</th>
                                                                                    </tr>";
                
            $exceldata[$row][]                                     = $colorClass."";
            $exceldata[$row][]                                     = $colorClass."";
            $exceldata[$row][]                                     = $colorClass."Total Used (".$_SESSION["currency"]."):";
            $exceldata[$row][]                                     = $colorClass.number_format((double)$totalUsedViewed, 2, ".", "");
            $exceldata[$row][]                                     = $colorClass."";
            $exceldata[$row][]                                     = $colorClass."";
            $exceldata[$row][]                                     = $colorClass."";
                $row++;
            
            if(($timeUsedAll - $totalUsedViewed ) != 0){
                $exceldata[$row][]                                     = $colorClass."";
                $exceldata[$row][]                                     = $colorClass."";
                $exceldata[$row][]                                     = $colorClass."Total Used (Other Areas) (".$_SESSION["currency"]."):";
                $exceldata[$row][]                                     = $colorClass.number_format((double)$timeUsedAll - $totalUsedViewed , 2, ".", "");
                $exceldata[$row][]                                     = $colorClass."";
                $exceldata[$row][]                                     = $colorClass."";
                $exceldata[$row][]                                     = $colorClass."";
                    $row++;
            }
            
            $exceldata[$row][]                                     = $colorClass."";
            $exceldata[$row][]                                     = $colorClass."";
            $exceldata[$row][]                                     = $colorClass."Total Project Budget (Consulting) (".$_SESSION["currency"]."):";
            $exceldata[$row][]                                     = $colorClass.number_format((double)$project_budget, 2, ".", "");
            $exceldata[$row][]                                     = $colorClass."";
            $exceldata[$row][]                                     = $colorClass."";
            $exceldata[$row][]                                     = $colorClass."";
                $row++; 
                
            $exceldata[$row][]                                     = $colorClass."";
            $exceldata[$row][]                                     = $colorClass."";
            $exceldata[$row][]                                     = $colorClass."Total Budget Left (Consulting) (".$_SESSION["currency"]."):";
            $exceldata[$row][]                                     = $colorClass.number_format((double)($project_budget - $timeUsedAll), 2, ".", "");
            $exceldata[$row][]                                     = $colorClass."";
            $exceldata[$row][]                                     = $colorClass."";
            $exceldata[$row][]                                     = $colorClass."";
             $row++;   

        if ($displayString != "") {
            $displayStringHead                                      = "<tr><td class='on-table-clear' colspan='7'><a>Shared Project Time Breakdown Report</a></td></tr>
                                                                                    <tr><td class='on-table-clear' colspan='7'>Shared Project: ".$project_name."</td></tr>";
            if($company_name != ""){
                $displayStringHead                                  .= "<tr><td class='on-table-clear' colspan='7'>Company: ".$company_name."</td></tr>";
            }else{
                $displayStringHead                                  .= "<tr><td class='on-table-clear' colspan='7'>Company: All Companies</td></tr>";
            }
            
            if($area_name != ""){
                $displayStringHead                                  .= "<tr><td class='on-table-clear' colspan='7'>Area: ".$area_name."</td></tr>";
            }
            $allData                                                        = $displayString;
            $displayString                                              = $displayStringHead.$headers.$allData;
            $generated                                                   = "1";
        }
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("3", "reports");
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    function check()
    {
        var valid                                                       = 1;

        //  Check That Project Is Selected
        if (document.forms["report_summary"].project.value == "null")
        {
            ShowLayer("projectErrorDiv", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("projectErrorDiv", "none");

        if (valid == 1)
        {
            document.forms["report_summary"].save.value                 = 1;
            document.forms["report_summary"].submit();
        }
    }
    
    //  Sajax
    <?php sajax_show_javascript(); ?>
    
    ///////////////////////////
    //  Sajax
    function ClearOptions(OptionList) {
        for (x = OptionList.length; x >= 0; x--) {
            OptionList[x] = null;
        }
    }
    
    // Set Company
    function setCompanies(data) {
        var c                                                           = 0;
        if(!data)
            ShowLayer("companyDiv", "none");
        
        document.forms["report_summary"].company.options[(c++)]                         = new Option("All Companies", "null");
        
        for (var i in data)
          eval("document.forms['report_summary'].company.options["+(c++)+"] = new Option('" + data[i][1] + "', '" + data[i][0] + "');");
    }
    
    // Get Companies
    function getCompanies(select) {
        ClearOptions(document.report_summary.company);
        if (select.value != "null")
        {
            ShowLayer("companyDiv", "block");
        }else{
            ShowLayer("companyDiv", "none");
        }
        x_getCompanies(select.value, setCompanies);
        
    }
    
    // Set Areas
    function setAreas(data) {
        var c                                                           = 0;
        if(!data)
            ShowLayer("areaDiv", "none");
        
        document.forms["report_summary"].area.options[(c++)]                         = new Option("All Areas", "null");
        
        for (var i in data)
          eval("document.forms['report_summary'].area.options["+(c++)+"] = new Option('" + data[i][1] + "', '" + data[i][0] + "');");
    }
    
    // Get Areas
    function getAreas(select) {
        ClearOptions(document.report_summary.area);
        if (select.value != "null")
        {
            ShowLayer("areaDiv", "block");
        }else{
            ShowLayer("areaDiv", "none");
        }
        x_getAreas(select.value, setAreas);
        
    }
</script>
<?php
	$projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p 
                            INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) 
                            WHERE cot.company_id = '".$_SESSION["company_id"]."' 
                            AND p.p_type = 'SP' 
                            AND p.completed = '0' 
                            AND p.deleted = '0' ORDER BY UPPER(p.name)");

?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="report_summary">
                    <div id="" style="<?php if ($generated == "1") echo "display: none;"; else echo "display: block"; ?>">
                            <table width="100%">
                                <tr>
                                    <td class="centerdata">
                                        <h6>Shared Project Consulting Breakdown Report</h6>
                                    </td>
                                </tr>
                            </table>
                            <br/>
                        
                        <div id="projectDiv" style="display: block;">
                            <table width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Report On:
                                    </td>
                                    <td width="50%">
                                        <select class="on-field" method="post" name="reportOn">
                                            <option value="normal">Actual Time</option>
                                            <option value="approved">Approved Time</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr><td colspan="100%"><br/></td></tr>
                                <tr>
                                    <td class="on-description" width="50%">
                                        Select Shared Project:
                                    </td>
                                    <td width="50%">
                                        <select class="on-field" method="post" id="project" name="project" onChange="getCompanies(project); getAreas(project)">
                                            <option value="null">--  Select A Shared Project  --</option>
                                            <?php
                                                if (is_array($projects))
                                                    foreach ($projects as $project)
                                                        //if ($_POST["project"] == $project[0])
                                                           // echo "<option value='".$project[0]."' selected>".$project[1]."</option>";
                                                        //else
                                                            echo "<option value='".$project[0]."'>".$project[1]."</option>";
                                            ?>
                                        </select>
                                        <div id="projectErrorDiv" style="display: none;"><font class="on-validate-error">* Shared Project must be selected</font></div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                                
                        <div id="companyDiv" style="display: none;">
                            <table width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Select Company:
                                    </td>
                                    <td width="50%">
                                        <select class="on-field" method="post" name="company">
                                            <option value="null">--  Select A Company  --</option>
                                            <?php
                                                if (is_array($companies))
                                                    foreach ($companies as $company)
                                                        //if ($_POST["project"] == $project[0])
                                                           // echo "<option value='".$project[0]."' selected>".$project[1]."</option>";
                                                        //else
                                                            echo "<option value='".$company[0]."'>".$company[1]."</option>";
                                            ?>
                                        </select>
                                        <!--<div id="companyDiv" style="display: none;"><font class="on-validate-error">* Company must be selected</font></div>-->
                                    </td>
                                </tr>
                            </table>
                        </div>
                                
                        <div id="areaDiv" style="display: none;">
                            <table width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Select Area:
                                    </td>
                                    <td width="50%">
                                        <select class="on-field" method="post" name="area">
                                            <option value="null">--  Select Area  --</option>
                                            <?php
                                                if (is_array($areas))
                                                    foreach ($areas as $area)
                                                        //if ($_POST["project"] == $project[0])
                                                           // echo "<option value='".$project[0]."' selected>".$project[1]."</option>";
                                                        //else
                                                            echo "<option value='".$area[0]."'>".$area[1]."</option>";
                                            ?>
                                        </select>
                                        <!--<div id="areaDiv" style="display: none;"><font class="on-validate-error">* Area must be selected</font></div>-->
                                    </td>
                                </tr>
                            </table>
                        </div>
                        
                        <br/>
                        <input name="btnGenerate" onClick="check();" type="button" value="Generate Report">
                        <input method="post" name="save" type="hidden" value="0" />
                    </div>
                </form>
                <div id="report" style="<?php if ($generated == "1") echo "display: block;"; else echo "display: none"; ?>">
                    <?php
                        echo "<div class='on-20px'><table class='on-table-center on-table'>";
                            echo "".$displayString;
                             echo  "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        echo "</table></div>";
                        echo "<br/>";

                        ///////////////////////////
                        //  Set Export Information
                        $_SESSION["fileName"] = "Shared Project Time Breakdown Report";
                        $_SESSION["fileData"] = array_merge($excelheadings, $exceldata);
                        ///////////////////////////

                        echo "<input name='btnExport' onClick=\"location.href='_export.php'\" type='button' value='Export to Excel' />";
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
