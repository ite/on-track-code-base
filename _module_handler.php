<?php
    session_start();
    
    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    //  Check If Company Has Access To Module
    $module_code = $_GET["module"];
    $what = $_GET["what"];

    include("_db.php");
    include("graphics.php");
    include("modules/$module_code.php");
    include("include/sajax.php");

    ///////////////////////////
    //  Sajax
    function getCompanies($id)      {
       return q("SELECT DISTINCT(c.id), c.name FROM (Company AS c INNER JOIN companiesOnTeam AS cot ON c.id = cot.company_id INNER JOIN Project AS p ON p.id =  cot.project_id) WHERE cot.project_id = '$id' ORDER BY c.name"); 
    }

    $sajax_request_type = "GET";
    sajax_init();    
    sajax_export("getCompanies");    
    sajax_handle_client_request();
    ///////////////////////////

    //$access = q("SELECT mu.id FROM (Module_Users AS mu INNER JOIN Modules AS m) WHERE mu.company_id = '".$_SESSION["company_id"]."' AND m.code = '$module_code'");

    //if ($access == "")
    //    header("Location: home.php");

    $module = new Module();
    
    // GLOBAL VARIABLES __________________________________________________
    $isSharedProject = 0;
    $isSPM = hasAccess("SPM_MANAGE");
    //  _________________________________________________________________

    //  Get Menu Option Value
    if ($module_code == "approval")
        $menu_option = "9";
    else
        $menu_option = "0";
	
	if ($module_code == "approval") {
    	if (!hasAccess("APPROVAL_MANAGE"))
            header("Location: noaccess.php");
    }

    //  Get PHP Script
    $module -> getPHPScript();

    //  Print Header
    print_header();
    //  Print Menu
    print_menus($menu_option, $module_code);

    if ($what == "list")
        $module -> getList();
    else if ($what == "display")
        $module -> getDisplay();
    else if ($what == "report")
        $module -> getReport();

    //  Print Footer
    print_footer();
?>
