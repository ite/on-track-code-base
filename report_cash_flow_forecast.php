<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("fusion/FusionCharts.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");
        
    if (!hasAccess("REP_CASH_FLOW"))
        header("Location: noaccess.php");

    //  Set Report Generated Status
    $generated                                                          = "0";

	function getAnchorAlpha($arr)   {
        $cc = 0;
        if (!is_array($arr)) return 100;
        foreach ($arr as $d)    {
            if ($d[0] !== "NULL")   $cc++;
            if ($cc > 52)   return 0;
        }
        return 100;
    }

	function getLabelCount($arr)	{
		return (count($arr) > 30) ? ceil(count($arr) / 30.0) : 1;
	}

    function createXMLString($date_from, $date_to, $consulting_array, $expenses_array, $combination_array)
    {
		$display_number_of_labels = getLabelCount($consulting_array);
		$al = getAnchorAlpha($consulting_array);

        $xmlString                                                      = "";

        $xmlString                                                      = "<chart caption='Cash Flow Forecast Report' subcaption='' xAxisName='Date' ".
                                                                            "numberPrefix='". $_SESSION["currency"]."' showValues='0' labelDisplay='rotate' ".
                                                                            "labelStep = '$display_number_of_labels' chartTopMargin='4' chartBottomMargin='10' chartLeftMargin='4' chartRightMargin='30' captionPadding='4' ".fusionChart().">";

        //  Create Categories
        $plus                                                           = 0;
        $date                                                           = $date_from;

        $xmlString                                                      .= "<categories>";
        while ($date <= $date_to)
        {
            $xmlString                                                  .= "<category label='$date' />";

            $bank                                                       = q("SELECT amount FROM StartBalance WHERE date = '$date' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."'");

            if ($bank != 0)
                $xmlString                                              .= "<vLine color='000000' thickness='2' alpha='50' dashed='1' />";

            $plus++;
            $date                                                       = getDates($date_from, $plus);
        }
        $xmlString                                                      .= "</categories>";

        //  Create Dataset - Consulting
        $plus                                                           = 0;
        $date                                                           = $date_from;

        $xmlString                                                      .= "<dataset color='0033FF' seriesName='Consulting'>";
        while ($date <= $date_to)
        {
            if ($consulting_array[$date] < 0)
                $xmlString                                              .= "<set color='FF0000' value='".$consulting_array[$date]."' anchorAlpha = '$al'/>";
            else
                $xmlString                                              .= "<set color='0033FF' value='".$consulting_array[$date]."' anchorAlpha = '$al'/>";

            $plus++;
            $date                                                       = getDates($date_from, $plus);
        }
        $xmlString                                                      .= "</dataset>";
        //  Create Dataset - Expenses
        $plus                                                           = 0;
        $date                                                           = $date_from;

        $xmlString                                                      .= "<dataset color='FF9933' seriesName='Expenses'>";
        while ($date <= $date_to)
        {
            if ($expenses_array[$date] < 0)
                $xmlString                                              .= "<set color='FF0000' value='".$expenses_array[$date]."' anchorAlpha = '$al'/>";
            else
                $xmlString                                              .= "<set color='FF9933' value='".$expenses_array[$date]."' anchorAlpha = '$al'/>";

            $plus++;
            $date                                                       = getDates($date_from, $plus);
        }
        $xmlString                                                      .= "</dataset>";
        //  Create Dataset - Combination
        $plus                                                           = 0;
        $date                                                           = $date_from;

        $xmlString                                                      .= "<dataset color='33FF00' seriesName='Combination'>";
        while ($date <= $date_to)
        {
            if ($combination_array[$date] < 0)
                $xmlString                                              .= "<set color='FF0000' value='".$combination_array[$date]."' anchorAlpha = '$al'/>";
            else
                $xmlString                                              .= "<set color='33FF00' value='".$combination_array[$date]."' anchorAlpha = '$al'/>";

            $plus++;
            $date                                                       = getDates($date_from, $plus);
        }
        $xmlString                                                      .= "</dataset>";

        $xmlString                                                      .= "<trendLines>";
        $xmlString                                                      .= "<line startValue='0' color='000000' thickness='2' />";
        $xmlString                                                      .= "</trendLines>";
        $xmlString                                                      .= "</chart>";

        return $xmlString;
    }

    function createTable($date_from, $date_to, $start_balance_array, $invoices_consulting_array, $invoices_expense_array, $budget_assigned_array, $budget_expense_array, $consulting_array, $expenses_array, $combination_array)
    {
        $tableString                                                    = "";

        $plus                                                           = 0;
        $date                                                           = $date_from;

        while ($date <= $date_to)
        {
            $tableString                                                .= "<tr>";
            $tableString                                                .= "<td style='white-space:nowrap;' class='rightdata'>".$date."</td>";

            //  Start Balance
            if ($start_balance_array[$date] >= 0)
                $tableString                                            .= "<td class='rightdata'>".$start_balance_array[$date].
                                                                            "</td>";
            else if ($start_balance_array[$date] < 0)
                $tableString                                            .= "<td class='rightdata'>".$start_balance_array[$date].
                                                                            "</td>";
            else
                $tableString                                            .= "<td class='rightdata'>".$start_balance_array[$date]."</td>";

            //  Consulting
            $tableString                                                .= "<td class='rightdata'>".$invoices_consulting_array[$date]."</td>";
            //  Expenses
            $tableString                                                .= "<td class='rightdata'>".$invoices_expense_array[$date]."</td>";
            //  Budget Assigned
            $tableString                                                .= "<td class='rightdata'>".$budget_assigned_array[$date]."</td>";
            //  Budget Expenses
            $tableString                                                .= "<td class='rightdata'>".$budget_expense_array[$date]."</td>";
            //  Cash Flow - Consulting
            if ($consulting_array[$date] >= 0)
                $tableString                                            .= "<td class='rightdata'>".$consulting_array[$date]."</td>";
            else if ($consulting_array[$date] < 0)
                $tableString                                            .= "<td class='rightdata' style='color:#FF0000'>".$consulting_array[$date]."</td>";
            //  Cash Flow - Expenses
            if ($expenses_array[$date] >= 0)
                $tableString                                            .= "<td class='rightdata'>".$expenses_array[$date]."</td>";
            else if ($expenses_array[$date] < 0)
                $tableString                                            .= "<td class='rightdata' style='color:#FF0000'>".$expenses_array[$date]."</td>";
            //  Cash Flow - Combination
            if ($combination_array[$date] >= 0)
                $tableString                                            .= "<td class='rightdata'>".$combination_array[$date].
                                                                            "</td>";
            else if ($combination_array[$date] < 0)
                $tableString                                            .= "<td class='rightdata' style='color:#FF0000'>".$combination_array[$date].
                                                                            "</td>";

            $tableString                                                .= "</tr>";

            $plus++;
            $date                                                       = getDates($date_from, $plus);
        }

        return $tableString;
    }

    //  Create Display Information Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")
    {
        $errorMessage                                                   = "";

        $date_from                                                      = addslashes(strip_tags($_POST["date_from"]));
        $date_to                                                        = addslashes(strip_tags($_POST["date_to"]));
        $displayString                                                  = "";

        ///////////////////////////
        //  Create Arrays
        $start_balance_array                                            = array();
        $invoices_consulting_array                                      = array();
        $invoices_expense_array                                         = array();
        $budget_assigned_array                                          = array();
        $budget_expense_array                                           = array();
        $consulting_array                                               = array();
        $expenses_array                                                 = array();
        $combination_array                                              = array();

        $plus                                                           = 0;
        $date                                                           = $date_from;
        $balance1                                                       = 0;
        $balance2                                                       = 0;
        $balance3                                                       = 0;

        //  Calculate Balances @ Start Date
        $tempDate                                                       = q("SELECT id, date FROM ExpenseStatus WHERE date <= '$date' AND ".
                                                                            "company_id = '".$_SESSION["company_id"]."' ORDER BY date DESC");
        $tempDate                                                       = $tempDate[0][1];

        //  Expense Balance @ Start Date
        if ($tempDate != 0)
        {
            $balance2                                                   = q("SELECT amount FROM ExpenseStatus WHERE date = '$tempDate' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."'");
            $invoiced_expenses                                          = q("SELECT SUM(li.expenses + (li.expenses * (li.vat / 100))) FROM (LoadInvoices AS li ".
                                                                            "INNER JOIN Project AS p ON p.id = li.project_id) WHERE li.due_date >= '$tempDate' ".
                                                                            "AND li.due_date < '$date' AND p.company_id = '".$_SESSION["company_id"]."'");
            $budget_expenses                                            = q("SELECT SUM(amount + (amount * (vat / 100))) FROM ExpenseBudget ".
                                                                            "WHERE due_date >= '$tempDate' AND due_date < '$date' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."'");
            $invoiced_diverse_income                                    = q("SELECT SUM(li.diverse_income + (li.diverse_income * (li.vat / 100))) ".
                                                                            "FROM (LoadInvoices AS li INNER JOIN Project AS p ON p.id = li.project_id) ".
                                                                            "WHERE li.due_date >= '$tempDate' AND li.due_date < '$date' ".
                                                                            "AND p.company_id = '".$_SESSION["company_id"]."'");
            $balance2                                                   += ($invoiced_expenses - ($budget_expenses + $invoiced_diverse_income));
        }
        else
        {
            $invoiced_expenses                                          = q("SELECT SUM(li.expenses + (li.expenses * (li.vat / 100))) FROM (LoadInvoices AS li ".
                                                                            "INNER JOIN Project AS p ON p.id = li.project_id) WHERE li.due_date < '$date' ".
                                                                            "AND p.company_id = '".$_SESSION["company_id"]."'");
            $budget_expenses                                            = q("SELECT SUM(amount + (amount * (vat / 100))) FROM ExpenseBudget WHERE due_date < '$date' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."'");
            $invoiced_diverse_income                                    = q("SELECT SUM(li.diverse_income + (li.diverse_income * (li.vat / 100))) ".
                                                                            "FROM (LoadInvoices AS li INNER JOIN Project AS p ON p.id = li.project_id) ".
                                                                            "WHERE li.due_date < '$date' AND p.company_id = '".$_SESSION["company_id"]."'");
            $balance2                                                   += ($invoiced_expenses - ($budget_expenses + $invoiced_diverse_income));
        }

        $tempDate                                                       = q("SELECT id, date FROM StartBalance WHERE date < '$date' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."' ORDER BY date DESC");
        $tempDate                                                       = $tempDate[0][1];

        //  Consulting Balance @ Start Date
        if ($tempDate != 0)
        {
            $balance1                                                   = q("SELECT amount FROM StartBalance WHERE date = '$tempDate' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."'");
            $invoiced_consulting                                        = q("SELECT SUM(li.consulting + (li.consulting * (li.vat / 100))) ".
                                                                            "FROM (LoadInvoices AS li INNER JOIN Project AS p ON p.id = li.project_id) ".
                                                                            "WHERE li.due_date >= '$tempDate' AND li.due_date < '$date' ".
                                                                            "AND p.company_id = '".$_SESSION["company_id"]."'");
            $invoiced_diverse_income                                    = q("SELECT SUM(li.diverse_income + (li.diverse_income * (li.vat / 100))) ".
                                                                            "FROM (LoadInvoices AS li INNER JOIN Project AS p ON p.id = li.project_id) ".
                                                                            "WHERE li.due_date >= '$tempDate' AND li.due_date < '$date' ".
                                                                            "AND p.company_id = '".$_SESSION["company_id"]."'");
            $expenses                                                   = q("SELECT SUM(amount) FROM Budget WHERE due_date >= '$tempDate' AND due_date < '$date' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."'");

            $balance1                                                   += ($invoiced_consulting + $invoiced_diverse_income) - $expenses;
            $balance3                                                   = $balance1 + $balance2;
        }
        else
        {
            $invoiced_consulting                                        = q("SELECT SUM(li.consulting + (li.consulting * (li.vat / 100))) ".
                                                                            "FROM (LoadInvoices AS li INNER JOIN Project AS p ON p.id = li.project_id) ".
                                                                            "WHERE li.due_date < '$date' AND p.company_id = '".$_SESSION["company_id"]."'");
            $invoiced_diverse_income                                    = q("SELECT SUM(li.diverse_income + (li.diverse_income * (li.vat / 100))) ".
                                                                            "FROM (LoadInvoices AS li INNER JOIN Project AS p ON p.id = li.project_id) ".
                                                                            "WHERE li.due_date < '$date' AND p.company_id = '".$_SESSION["company_id"]."'");
            $expenses                                                   = q("SELECT SUM(amount) FROM Budget WHERE due_date < '$date' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."'");

            $balance1                                                   += ($invoiced_consulting + $invoiced_diverse_income) - $expenses;
            $balance3                                                   = $balance1 + $balance2;
        }

        while ($date <= $date_to)
        {
            $start_balance                                              = q("SELECT amount FROM StartBalance WHERE date = '$date' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."'");

            if ($start_balance != 0)
                $balance1                                               = $start_balance;

            $invoiced_consulting                                        = q("SELECT SUM(li.consulting + (li.consulting * (li.vat / 100))) ".
                                                                            "FROM (LoadInvoices AS li INNER JOIN Project AS p ON p.id = li.project_id) ".
                                                                            "WHERE li.due_date = '$date' AND p.company_id = '".$_SESSION["company_id"]."'");
            $invoiced_diverse_income                                    = q("SELECT SUM(li.diverse_income + (li.diverse_income * (li.vat / 100))) ".
                                                                            "FROM (LoadInvoices AS li INNER JOIN Project AS p ON p.id = li.project_id) ".
                                                                            "WHERE li.due_date = '$date' AND p.company_id = '".$_SESSION["company_id"]."'");
            $invoiced_expenses                                          = q("SELECT SUM(li.expenses + (li.expenses * (li.vat / 100))) FROM (LoadInvoices AS li ".
                                                                            "INNER JOIN Project AS p ON p.id = li.project_id) WHERE li.due_date = '$date' ".
                                                                            "AND p.company_id = '".$_SESSION["company_id"]."'");
            $expenses                                                   = q("SELECT SUM(amount) FROM Budget WHERE due_date = '$date' AND company_id = '".$_SESSION["company_id"]."'");
            $budget_expenses                                            = q("SELECT SUM(amount + (amount * (vat / 100))) FROM ExpenseBudget WHERE due_date = '$date' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."'");

            $balance1                                                   += ($invoiced_consulting + $invoiced_diverse_income) - $expenses;
            $balance2                                                   += ($invoiced_expenses - ($budget_expenses + $invoiced_diverse_income));
            $balance3                                                   = $balance1 + $balance2;

            if ($start_balance != 0)
                $start_balance_array[$date]                             = number_format($start_balance, 2, ".", "");
            else
                $start_balance_array[$date]                             = "-";

            if (($invoiced_consulting + $invoiced_diverse_income) > 0)
                $invoices_consulting_array[$date]                      = number_format(($invoiced_consulting + $invoiced_diverse_income), 2, ".", "");
            else
                $invoices_consulting_array[$date]                      = "-";

            if (($invoiced_expenses - $invoiced_diverse_income) > 0)
                $invoices_expense_array[$date]                          = number_format(($invoiced_expenses - $invoiced_diverse_income), 2, ".", "");
            else
                $invoices_expense_array[$date]                          = "-";

            if ($expenses > 0)
                $budget_assigned_array[$date]                           = number_format($expenses, 2, ".", "");
            else
                $budget_assigned_array[$date]                           = "-";

            if ($budget_expenses > 0)
                //$budget_expense_array[$date]                            = number_format(($budget_expenses + $invoiced_diverse_income), 2, ".", "");
                $budget_expense_array[$date]                            = number_format($budget_expenses, 2, ".", "");
            else
                $budget_expense_array[$date]                            = "-";

            $consulting_array[$date]                                    = number_format($balance1, 2, ".", "");
            $expenses_array[$date]                                      = number_format($balance2, 2, ".", "");
            $combination_array[$date]                                   = number_format($balance3, 2, ".", "");

            $plus++;
            $date                                                       = getDates($date_from, $plus);
        }

        $graph                                                          = createXMLString($date_from, $date_to, $consulting_array, $expenses_array, $combination_array);
        $displayString                                                  = renderChart("fusion/MSLine.swf", "", $graph, "multigraph", 940, 400, false, false);

        ///////////////////////////
        //  Column Headings
        $headers                                                        = "<tr>
                                                                                    <td class='on-table-clear' colspan='2'></td>
                                                                                    <td class='on-table-clear' colspan='2'><h6>LoadedInvoices</h6></td>
                                                                                    <td class='on-table-clear' colspan='2'><h6>Running Cost</h6></td>
                                                                                    <td class='on-table-clear' colspan='3'><h6>Cash Flow</h6></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th>Expected Date</th>
                                                                                    <th>Start Balance</th>
                                                                                    <th>Consulting<i>(".$_SESSION["currency"].")</i></th>
                                                                                    <th>Expenses<i>(".$_SESSION["currency"].")</i></th>
                                                                                    <th>Running Cost<i>(".$_SESSION["currency"].")</i></th>
                                                                                    <th>Expenses<i>(".$_SESSION["currency"].")</i></th>
                                                                                    <th>Consulting<i>(".$_SESSION["currency"].")</i></th>
                                                                                    <th>Expenses<i>(".$_SESSION["currency"].")</i></th>
                                                                                    <th>Combination<i>(".$_SESSION["currency"].")</i></th>
                                                                                </tr>";

        //  Column Data
        $display                                                        = createTable($date_from, $date_to, $start_balance_array, $invoices_consulting_array, $invoices_expense_array, $budget_assigned_array, $budget_expense_array, $consulting_array, $expenses_array, $combination_array);

        if ($display != "")
            $display                                                    = $headers.$display;

        $displayString                                                  = "<tr>
                                                                                        <td class='on-table-clear' colspan='9'><a>Cash Flow Forecast Report</a></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan='9'><br/>".$displayString."<br/></td>
                                                                                    </tr>".$display;

        $displayString                                                  .= "<tr><td class='on-table-total' class='on-table-total' colspan='6'>Grand Totals<i>(".$_SESSION["currency"].")</i>:</td>";

        if ($balance1 >= 0)
            $displayString                                              .= "<td class='on-table-total' class='on-table-total'>".
                                                                                number_format($balance1, 2, ".", "")."</td>";
        else
            $displayString                                              .= "<td class='on-table-total' class='on-table-total'>".
                                                                                number_format($balance1, 2, ".", "")."</td>";

        if ($balance2 >= 0)
            $displayString                                              .= "<td class='on-table-total' class='on-table-total'>".
                                                                                number_format($balance2, 2, ".", "")."</td>";
        else
            $displayString                                              .= "<td class='on-table-total' class='on-table-total'>".
                                                                                number_format($balance2, 2, ".", "")."</td>";

        if ($balance3 >= 0)
            $displayString                                              .= "<td class='on-table-total' class='on-table-total'>".
                                                                                number_format($balance3, 2, ".", "")."</td>";
        else
            $displayString                                              .= "<td class='on-table-total' class='on-table-total'>".
                                                                                number_format($balance3, 2, ".", "")."</td>";
        $displayString                                                  .= "</tr>";

        if ($displayString != "")
            $generated                                                  = "1";
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("5", "reports");

    if ($errorMessage != "")
    {
        echo "<p align='center' style='padding:0px;'><strong><font class='on-validate-error'>$errorMessage</font></strong></p>";
        echo "<br/>";
    }
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    jQuery(function(){
	jQuery('#date_from').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_from').val(),
		current: jQuery('#date_from').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_from').val()))
			_date = new Date();
			else _date = jQuery('#date_from').val();
			jQuery('#date_from').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_from').val(formated);
			jQuery('#date_from').DatePickerHide();
		}
	});
    })

    jQuery(function(){
	jQuery('#date_to').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_to').val(),
		current: jQuery('#date_to').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_to').val()))
			_date = new Date();
			else _date = jQuery('#date_to').val();
			jQuery('#date_to').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_to').val(formated);
			jQuery('#date_to').DatePickerHide();
		}
	});
    })

    function check()
    {
        var valid                                                       = 1;

        //  Check That Date From Is Entered
        if (document.forms["report_cash_flow_forecast"].date_from.value == "")
        {
            ShowLayer("dateFrom", "block");
            valid                                                       = 0;
        }
        //  Check That Entered Date From Is Valid
        else
        {
            ShowLayer("dateFrom", "none");

            if (!validation("date", document.forms["report_cash_flow_forecast"].date_from.value))
            {
                ShowLayer("dateFromDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("dateFromDiv", "none");
        }

        //  Check That Date To Is Entered
        if (document.forms["report_cash_flow_forecast"].date_to.value == "")
        {
            ShowLayer("dateTo", "block");
            valid                                                       = 0;
        }
        //  Check That Entered Date To Is Valid
        else
        {
            ShowLayer("dateTo", "none");

            if (!validation("date", document.forms["report_cash_flow_forecast"].date_to.value))
            {
                ShowLayer("dateToDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("dateToDiv", "none");
        }

        if (valid == 1)
        {
            document.forms["report_cash_flow_forecast"].save.value      = 1;
            document.forms["report_cash_flow_forecast"].submit();
        }
    }
</script>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="report_cash_flow_forecast">
                    <div id="" style="<?php if ($generated == "1") echo "display: none;"; else echo "display: block"; ?>">
                        <table width="100%">
                            <tr>
                                <td class="centerdata">
                                    <h6>Cash Flow Forecast Report</h6>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Date From:
                                </td>
                                <td width="50%">
                                    <input class="on-field-date" id="date_from" name="date_from" type="text" style="text-align:right;" value="<?php
/*
                                        $from                           = $today;

                                        $date                           = q("SELECT MAX(date) FROM StartBalance WHERE company_id = '".$_SESSION["company_id"]."'");

                                        if ($date)
                                            $from                       = $date;

                                        echo "".$from;
*/
                                        if ($_POST["date_from"] != "") echo "".$_POST["date_from"]; else echo "".getDates($today, -15); ?>">
                                    <div id="dateFrom" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                    <div id="dateFromDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Date To:
                                </td>
                                <td width="50%">
                                    <input class="on-field-date" id="date_to" name="date_to" type="text" style="text-align:right;" value="<?php
                                        if ($_POST["date_to"] != "") echo "".$_POST["date_to"]; else echo "".getDates($today, 35); ?>">
                                    <div id="dateTo" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                    <div id="dateToDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <input name="btnGenerate" onClick="check();" type="button" value="Generate Report">
                        <input method="post" name="save" type="hidden" value="0" />
                    </div>
                </form>
                <div id="report" style="<?php if ($generated == "1") echo "display: block;"; else echo "display: none"; ?>">
                    <?php
                        echo "<div class='on-20px'><table class='on-table-center on-table'>";
                            echo "".$displayString;
                            echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        echo "</table></div>";
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
