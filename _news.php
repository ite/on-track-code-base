<table style='padding:4px 4px 4px 4px;margin:4px 4px 4px 4px;' width='100%'>
<tr>
<td width='100%'>
<h1>
On-Track - News Page
</h1>
</td>
</tr>
<tr>
<td width='100%'>
<h2>
Latest News/Updates on On-Track
</h2>
</td>
</tr>
<tr>
<td style='border-bottom: 1px solid orange'>
<br/>
<h3>
2011-10-14		qwer
</h3>
<br/>
</h4>
<p>
&nbsp;qwer
</p>
<br/>
</td>
</tr>
<tr>
<td style='border-bottom: 1px solid orange'>
<br/>
<h3>
2011-09-19		On-Track slow
</h3>
<br/>
</h4>
<p>
&nbsp;Please note that we experienced problems with the ISP hosting On-Track this morning.   We\'re monitoring the situation closely and attempting to resolve the problem asap! Feedback welcome!
</p>
<br/>
</td>
</tr>
<tr>
<td style='border-bottom: 1px solid orange'>
<br/>
<h3>
2011-09-07		On-Track Menu Released
</h3>
<br/>
</h4>
<p>
&nbsp;New Menu Structure Released. Let us know what you think!
</p>
<br/>
</td>
</tr>
<tr>
<td style='border-bottom: 1px solid orange'>
<br/>
<h3>
2011-08-24		On-Track Menu structure Upgrade
</h3>
<br/>
</h4>
<p>
&nbsp;Please note that the development team is working on a menu structure upgrade to improve navigation speed.  Expected launch date: Last week of August 2011.
</p>
<br/>
</td>
</tr>
<tr>
<td style='border-bottom: 1px solid orange'>
<br/>
<h3>
2011-07-21		On-Track Release
</h3>
<br/>
</h4>
<p>
&nbsp;The new version of On-Track is now active. If you have trouble booking, press Ctrl+F5 and try to book again. Please let us know what you think.
</p>
<br/>
</td>
</tr>
<tr>
<td style='border-bottom: 1px solid orange'>
<br/>
<h3>
2011-05-03		Notice: Different URL displaying
</h3>
<br/>
</h4>
<p>
&nbsp;On-Track has been temporarily moved to another server, therefore the URL will display differently. On-Track can still be accessed from:<br/>&nbsp;ontrack.integrityengineering.co.za
</p>
<br/>
</td>
</tr>
<tr>
<td style='border-bottom: 1px solid orange'>
<br/>
<h3>
2010-08-31		Data Recovered
</h3>
<br/>
</h4>
<p>
&nbsp;The data of 2010-08-26 has been recovered as promised.
</p>
<br/>
</td>
</tr>
<tr>
<td style='border-bottom: 1px solid orange'>
<br/>
<h3>
2010-08-30		Database Management Update
</h3>
<br/>
</h4>
<p>
&nbsp;Please note that, due to a problem at our internet service provider, we do not currently have any data entries for the 26th August 2010.  We\'re still in the process of trying to recover the data and we would appreciate your patience.  Please do not attempt to rebook time for the date mentioned since there is a good chance that we will be able to recover the data.
</p>
<br/>
</td>
</tr>
<tr>
<td style='border-bottom: 1px solid orange'>
<br/>
<h3>
2010-08-27		Database Management
</h3>
<br/>
</h4>
<p>
&nbsp;Please note that we are currently busy with data management, no entries are allowed for 2010-08-26. Please check back in 24 Hours.
</p>
<br/>
</td>
</tr>
<tr>
<td style='border-bottom: 1px solid orange'>
<br/>
<h3>
2010-07-02		Server Issues
</h3>
<br/>
</h4>
<p>
&nbsp;Please note that the data of the 30th June has been lost. We will issue a statement soon. Please rebook your information for this day.
</p>
<br/>
</td>
</tr>
<tr>
<td style='border-bottom: 1px solid orange'>
<br/>
<h3>
2010-07-01		Server Issues
</h3>
<br/>
</h4>
<p>
&nbsp;Dear all,<br/>&nbsp;<br/>&nbsp;Our web host has been experiencing some issues with the database. The have been recovering the data throughout the night. It seems like one days data has gone missing and we are busy with the process of trying to recover this data. Please bear with us in this regard.
</p>
<br/>
</td>
</tr>
<tr>
<td style='border-bottom: 1px solid orange'>
<br/>
<h3>
2009-09-18		IE Patch
</h3>
<br/>
</h4>
<p>
&nbsp;We have been experiencing malware infiltrating systems via an Internet Explorer vulnerability. Please download and install the following patch to rectify the problem or consider migrating to Firefox.<br/>&nbsp;<br/>&nbsp;WinXP: <a href='downloads/XP_IE7_update.exe'>Windows
</p>
<br/>
</td>
</tr>
<tr>
<td style='border-bottom: 1px solid orange'>
<br/>
<h3>
2009-07-15		Shared Resource Booking
</h3>
<br/>
</h4>
<p>
&nbsp;We have added shared resource booking. Please notify us at support@integrityengineering.co.za if you want to add a shared resource. You can book by going to Bookings -> Book Shared Resource.
</p>
<br/>
</td>
</tr>
<tr>
<td style='border-bottom: 1px solid orange'>
<br/>
<h3>
2009-07-10		Documentation
</h3>
<br/>
</h4>
<p>
&nbsp;You can look at the general On-Track documentation by clicking on the Documentation link in the bottom left hand corner of your On-Track screen.
</p>
<br/>
</td>
</tr>
<tr>
<td style='border-bottom: 1px solid orange'>
<br/>
<h3>
2009-06-12		New Logo
</h3>
<br/>
</h4>
<p>
&nbsp;Hope all of you enjoy our new logo. 
</p>
<br/>
</td>
</tr>
<tr>
<td style='border-bottom: 1px solid orange'>
<br/>
<h3>
2009-03-24		Mobile Functionality
</h3>
<br/>
</h4>
<p>
&nbsp;You can now access On-Track mobile by going to http://ontrack.integrityengineering.co.za from your mobile phone.
</p>
<br/>
</td>
</tr>
<tr>
<td style='border-bottom: 1px solid orange'>
<br/>
<h3>
2009-03-05		New functionality
</h3>
<br/>
</h4>
<p>
&nbsp;SLA - Notify the project manager as soon as a certain time limit on a project has been exceeded. Notifications need to be enabled for this.
</p>
<br/>
</h4>
<p>
&nbsp;Book Leave - different types
</p>
<br/>
</td>
</tr>
<tr>
<td style='border-bottom: 1px solid orange'>
<br/>
<h3>
2009-03-05		Projects
</h3>
<br/>
</h4>
<p>
&nbsp;Can add active/proposal state to projects
</p>
<br/>
</td>
</tr>
<tr>
<td style='border-bottom: 1px solid orange'>
<br/>
<h3>
2009-03-05		New Reports
</h3>
<br/>
<h4>
&nbsp;Project Reports - Project Summary (Matrix) Report
</h4>
<p>
&nbsp;Shows the hours that an employee has spent per activity type and summarizes for all employees per project/all projects.
</p>
<br/>
<h4>
&nbsp;Project Reports - Progress Report
</h4>
<p>
&nbsp;Shows the progress on a project/all projects graphically by using invoice information as well as time information. Will also indicate if a project budget has been exceeded and by what amount it has been exceeded.
</p>
<br/>
<h4>
&nbsp;Project Reports - Work in Process (Consulting) Report
</h4>
<p>
&nbsp;Shows the work in process on a pie chart as well as in table format.
</p>
<br/>
<h4>
&nbsp;Bussiness Reports - Invoiced vs. Cost to Company Report
</h4>
<p>
&nbsp;Shows the total hours invoiced at an employees rate vs. the cost to company of the employee per hour.
</p>
<br/>
<h4>
&nbsp;Bussiness Reports - Time Comparison Report
</h4>
<p>
&nbsp;Visually compares time booked per hour/week/month of all employees.
</p>
<br/>
</td>
</tr>
<tr>
<td style='border-bottom: 1px solid orange'>
<br/>
<h3>
2009-03-05		Admin
</h3>
<br/>
<h4>
&nbsp;Notifications - These need to be set by the user before being active
</h4>
<p>
&nbsp;- Budget Created/Updated<br/>&nbsp;- Invoice Created/Updated<br/>&nbsp;- Expense Created/Updated<br/>&nbsp;- Leave Booked<br/>&nbsp;- Employee(s) Behind Schedule<br/>&nbsp;- Service Level Agreement(s)
</p>
<br/>
<h4>
&nbsp;Time/Expense Management</h4>
<p>
&nbsp;You can delete wrong time/expense entries here
</p>
<br/>
</td>
</tr>
</table>