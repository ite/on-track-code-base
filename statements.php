<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!$_SESSION["eula_agree"] === true)
        header("Location: logout.php");

    if (!hasAccess("STATEMENT_MAN"))
        header("Location: noaccess.php");

    $link = addslashes(strip_tags($_GET["link"]));

    //  Print Header
    print_header();
    //  Print Menu
    if ($_GET["menu"] == "")
        print_menus("0", "home");
    else
        print_menus($_GET["menu"], "home");

    define(ERROR_TOL,5);	//percentage value
    define(ERROR_MAR,1000);	//R value
    define(MAX_SUGGEST,30);	//R value
    define(DEBUG, 0);           //Set to 1 to enable debugging. Set to 0 to hide the debugging

?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script type="text/javascript" src="include/date.js"></script>
<script language="JavaScript">
    function is_array(input){return typeof(input) == "object" && (input instanceof Array);}

    jQuery(function()    {

        //Updateing the Processed and Unprocessed light
        ///////////////////////////////////////////////
        function updateLight(o)	{
            return setLight(o,checkLight(o));
        }
        function checkLight(o)	{	//true = green, false = red;
            var sum = 0;
            var budgetCheck = false;
            o.children().each(function(i, child)  {
                if (jQuery(this).children().eq(5).children().eq(0).val() == "U")	{
                    if (jQuery(this).children().eq(4).html() == "BUDGET")	budgetCheck = true;	//budget items only need to be linked to be green
                    sum += parseFloat(jQuery(this).children().eq(2).html());
                }
            });
            if (budgetCheck)	return true;
            statement_amount = parseFloat(o.parent().parent().prev().prev().html());
            if (Math.round(sum,2) == Math.round(statement_amount,2))	return true;//the sum of the linked values need to match the statement value (for INVOICE or EXPENSE) to be green
            return false;
        }
        function setLight(o, state)	{
            light = o.parent().parent().prev().prev().prev().prev().prev().children().eq(0);
            if (state)
                light.attr('src','images/icons/yes.png').attr('title','Processed');
            else
                light.attr('src','images/icons/no.png').attr('title','Unprocessed');
            return state;
        }
        ///////////////////////////////////////////////

        //Link or Unlinked or M button clicked
        ///////////////////////////////////////////////
        var M_button_clicked_holder;
        jQuery(".link").click(function()	{
            var button = jQuery(this);
            var statement_id = jQuery(this).next().val();
            var element_id = jQuery(this).next().next().val();
            var type = jQuery(this).next().next().next().val();
            if (jQuery(this).attr("value") == "U")
                jQuery.post("_ajax.php", {func: "linkStatement", act: 'DELETE', statement_id: statement_id, element_id: element_id, type: type}, function(data){
                    button.val("L");
                    button.attr('src','images/icons/unlinked.png');
                    button.attr('title','Unlinked');
                    var state = updateLight(button.parent().parent().parent());
                });
            else if (jQuery(this).attr("value") == "L")
                jQuery.post("_ajax.php", {func: "linkStatement", act: 'ADD', statement_id: statement_id, element_id: element_id, type: type}, function(data) {
                    data = json_decode(data);
                    var last = button.parent().parent();
                    last.children().eq(0).html(data[0][0]);	//due_date
                    last.children().eq(2).html(parseFloat(data[0][1]).toFixed(2));	//amount
                    var diff = Math.abs(Math.abs(parseFloat(data[0][2])) - Math.abs(parseFloat(data[0][1]))).toFixed(2);	//diff
                    last.children().eq(3).html(diff);	//diff
                    button.val("U");
                    button.attr('src','images/icons/linked.png');
                    button.attr('title','Linked');
                    var state = updateLight(button.parent().parent().parent());
                    if (type == "INVOICE" && state)
                        jQuery.post("_ajax.php",{func:"updateInvoices", statement_id: statement_id, type: type}, function(data) {
                            alert("Updated all invoice due_dates and marked as paid");
                        });
                });
            else if (jQuery(this).attr("value") == "M")	{
                M_button_clicked_holder = jQuery(this);
                //reset form
                var typeM = jQuery("#typeM").val("0");
                jQuery("#searchMData tbody>tr").not(':first').remove();    //remove current entries
                var last = jQuery("#searchMData tbody>tr:last");
                last.children().eq(0).html("[date]");
                last.children().eq(1).html("[type]");
                last.children().eq(2).html("[description]");
                last.children().eq(3).html("[amount]");
                last.children().eq(4).children().eq(0).val(0);

                jQuery("#mask").css({"width":jQuery(document).width(),"height":jQuery(document).height()});
                jQuery('#mask').fadeIn("fast").fadeTo("fast",0.8);   //transition effect
                var id = "#modal_manualLink";
                //Set the popup window to center
                jQuery(id).css('top',  jQuery(window).scrollTop()+(jQuery(window).height()/2)-(jQuery(id).height()/1.1));
                jQuery(id).css('left', (jQuery(window).width()/2)-(jQuery(id).width()));
                jQuery(id).fadeIn("fast");   //transition effect
                var amountMS = jQuery(this).parent().parent().parent().parent().parent().prev().prev().html();
                var descrMS = jQuery(this).parent().parent().parent().parent().parent().prev().prev().prev().html();
                var dateMS = jQuery(this).parent().parent().parent().parent().parent().prev().prev().prev().prev().html();
                jQuery("#amountMS").html(amountMS);
                jQuery("#descrMS").html(descrMS);
                jQuery("#dateMS").html(dateMS);
            }
        });
        ///////////////////////////////////////////////

        //Type selectors in modal changed
        ///////////////////////////////////////////////
        jQuery("#typeM").change(function()   {
            var table = jQuery("#searchMData");
            var typeM = jQuery("#typeM").val();
            var amountM = jQuery("#amountMS").html();
            var dateM = jQuery("#dateM").val();
            var type = jQuery.trim(jQuery("#typeM option[value='"+jQuery("#typeM").val()+"']").text());
            var msg = "";
            if (amountM > 0)	{//credit must link to INVOICE OR BUDGET
                if (type == "EXPENSE")
                    msg = "You cannot link a credit to an EXPENSE";
            }
            else	{//debit must link to EXPENSE OR BUDGET
                if (type == "INVOICE")
                    msg = "You cannot link a debit to an INVOICE";
            }
            if (msg != "")	{
                jQuery("#typeM").val("0");
                alert(msg);
            }
            else	{
                if (type == "BUDGET")	
                    jQuery("#typeDateM_layer").show();
                else
                    jQuery("#typeDateM_layer").hide();
                jQuery("#typeFilterM").val("");
                if (typeM != "")	{
                    jQuery("#typeDateM").val("0");
                    var month = new Date().format("Ym");
                    jQuery("#typeDateM option").each(function()	{
                        if (jQuery(this).val() == month)
                            jQuery(this).attr("selected","true");
                    });
                    var typeDateM = jQuery("#typeDateM").val();
                    jQuery("#searchMData tbody>tr").not(':first').remove();    //remove current entries
                    jQuery.post("_ajax.php", {func: "searchLink", typeM: typeM, typeDateM: typeDateM}, function(data)    {
                        data = json_decode(data);
                        var bool = false;
                        var last = jQuery("#searchMData tbody>tr:last");
                        jQuery.each(data,function(i, v)  {
                            var n = last.clone(true);
                            n.children().eq(0).html(v[0]);
                            n.children().eq(1).html(v[1]);
                            n.children().eq(2).html(v[2]);
                            n.children().eq(3).html(v[3]);
                            n.children().eq(4).children().eq(0).val(v[6]);
                            n.children().eq(4).children().eq(1).attr('checked',false);
                            n.insertBefore(last);
                            bool = true;
                        });
                        if (bool)	last.remove();
                    });
                }
            }
        });

        jQuery("#typeDateM").change(function()   {      
            var table = jQuery("#searchMData");
            var typeM = jQuery("#typeM").val();
            var amountM = jQuery("#amountM").val();
            var dateM = jQuery("#dateM").val();
            var typeDateM = jQuery("#typeDateM").val();
            var type = jQuery.trim(jQuery("#typeM option[value='"+jQuery("#typeM").val()+"']").text());
            if (type == "BUDGET" && typeDateM != "0")
                jQuery("#searchMData tbody>tr").not(':first').remove();    //remove current entries
            jQuery.post("_ajax.php", {func: "searchLink", typeM: typeM, typeDateM: typeDateM}, function(data)    {
                data = json_decode(data);
                var bool = false;
                var last = jQuery("#searchMData tbody>tr:last");
                jQuery.each(data,function(i, v)  {
                    var n = last.clone(true);
                    n.children().eq(0).html(v[0]);
                    n.children().eq(1).html(v[1]);
                    n.children().eq(2).html(v[2]);
                    n.children().eq(3).html(v[3]);
                    n.children().eq(4).children().eq(0).val(v[6]);
                    n.insertBefore(last);
                    bool = true;
                });
                if (bool)	last.remove();
            });
        });
        jQuery("#typeFilterM").keyup(function()   {      
            var table = jQuery("#searchMData");
            var typeM = jQuery("#typeM").val();
            var amountM = jQuery("#amountM").val();
            var dateM = jQuery("#dateM").val();
            var typeDateM = jQuery("#typeDateM").val();
            var typeFilterM = jQuery("#typeFilterM").val();
            var type = jQuery.trim(jQuery("#typeM option[value='"+jQuery("#typeM").val()+"']").text());
            if (typeFilterM != "")
                jQuery("#searchMData tbody>tr").not(':first').remove();    //remove current entries
            jQuery.post("_ajax.php", {func: "searchLink", typeM: typeM, typeDateM: typeDateM, typeFilterM: typeFilterM}, function(data)    {
                data = json_decode(data);
                var bool = false;
                var last = jQuery("#searchMData tbody>tr:last");
                if (is_array(data))	{
                    jQuery.each(data,function(i, v)  {
                        var n = last.clone(true);
                        n.children().eq(0).html(v[0]);
                        n.children().eq(1).html(v[1]);
                        n.children().eq(2).html(v[2]);
                        n.children().eq(3).html(v[3]);
                        n.children().eq(4).children().eq(0).val(v[6]);
                        n.children().eq(4).children().eq(1).attr('checked', false);
                        n.insertBefore(last);
                        bool = true;
                    });
                }
                else {
                    var n = last.clone(true);
                    n.children().eq(0).html("[date]");
                    n.children().eq(1).html("[type]");
                    n.children().eq(2).html("[description]");
                    n.children().eq(3).html("[amount]");
                    n.children().eq(4).children().eq(0).val("0");
                    n.children().eq(4).children().eq(1).attr('checked', false);
                    n.insertBefore(last);
                    bool = true;
                }
                if (bool)	last.remove();
            });
        });
        ///////////////////////////////////////////////

        //Link on modal clicked
        ///////////////////////////////////////////////
        jQuery("#linkItems").click(function()   {      
            var statement_id = jQuery("#dateMS").children().eq(0).val();
            var type = jQuery("#typeM").val();
            type = jQuery.trim(jQuery("#typeM option[value='"+type+"']").text());
            var last = jQuery("#searchMData tbody");
            var count = 0;
            jQuery.each(last.children(),function(i, v)  {
                if (jQuery(this).children().eq(4).children().eq(1).attr('checked'))	{//if ontrack element selected to be linked
                    count++;
                }
            });
            var flag = "";
            if (type == "")                     flag = "You need to select a type";
            if (type == "BUDGET" && count > 1)	flag = "You can only select 1 budget item to be linked to a bank statement entry";
            if (type == "EXPENSE" && count > 1)	flag = "The sum of the selected items need to be equal to the bank statement entry.  Please correct your expense entries before linking.";
            if (flag == "")
                jQuery.each(last.children(),function(i, v)  {
                    if (jQuery(this).children().eq(4).children().eq(1).attr('checked'))	{//if ontrack element selected to be linked
                        var element_id = jQuery(this).children().eq(4).children().eq(0).val();
                        var row = jQuery(this);
                        jQuery.post("_ajax.php",{func:"linkStatement", act: 'ADD', statement_id: statement_id, element_id: element_id, type: type}, function(data) {
                            data = json_decode(data);
                            var last = M_button_clicked_holder.parent().parent();
                            var n = last.clone(true);
                            n.children().eq(0).html(data[0][0]);	//due_date
                            n.children().eq(1).html(row.children().eq(2).html());	//descr
                            n.children().eq(2).html(parseFloat(data[0][1]).toFixed(2));	//amount
                            var diff = Math.abs(last.parent().parent().parent().prev().html()-row.children().eq(3).html());
                            n.children().eq(3).html(diff.toFixed(2));	//diff
                            n.children().eq(4).html(row.children().eq(1).html());	//type
                            n.children().eq(5).children().eq(1).val(last.parent().parent().parent().prev().prev().prev().children().eq(0).val());//statment_id
                            n.children().eq(5).children().eq(2).val(row.children().eq(4).children().eq(0).val());	//element_id
                            n.children().eq(5).children().eq(3).val(row.children().eq(1).html());	//type
                            var button = n.children().eq(5).children().eq(0);
                            button.val("U");
                            button.attr('src','images/icons/linked.png')
                            button.attr('title','Linked')
                            n.insertBefore(last);
                            var state = updateLight(M_button_clicked_holder.parent().parent().parent());
                            if (type == "INVOICE" && state)
                                jQuery.post("_ajax.php",{func:"updateInvoices", statement_id: statement_id, type: type}, function(data) {
                                    alert("Updated all invoice due_dates and marked as paid");
                                });
                        });
                        jQuery("#typeM").val("0");
                        jQuery("#typeDateM").val("0");
                        jQuery("#typeFilterM").val("");
                        jQuery("#typeDateM_layer").hide();
                        jQuery("#mask").hide();
                        jQuery('.on-drop-menu').hide();
                        jQuery('.on-contact-modal').hide();
                    }
                });
            else
                alert(flag);
        });
        ///////////////////////////////////////////////

        //Cancel on modal clicked
        ///////////////////////////////////////////////
        jQuery("#cancelManualLink").click(function()   {      
            jQuery("#typeM").val("0");
            jQuery("#typeDateM").val("0");
            jQuery("#typeFilterM").val("");
            jQuery("#typeDateM_layer").hide();
            jQuery("#mask").hide();
            jQuery('.on-drop-menu').hide();
            jQuery('.on-contact-modal').hide();
        });
        ///////////////////////////////////////////////
        
        //  Close Modal
        jQuery("#mask").click(function()  {
            jQuery("#typeM").val("0");
            jQuery("#typeDateM").val("0");
            jQuery("#typeFilterM").val("");
            jQuery("#typeDateM_layer").hide();
            jQuery("#mask").hide();
            jQuery('.on-drop-menu').hide();
            jQuery('.on-contact-modal').hide();
        });

        jQuery(function(){
            jQuery(".pbar").each(function(i, v)  {
                var color;
                var val = parseFloat(jQuery(this).attr('name'));
                jQuery(jQuery(this)).progressbar({ "value": val});
                if (val < 50)           color = "Red";
                else if (val < 90)      color = "Orange";
                else if (val <= 100)	color = "Lime";
                else if (val <= 125)	color = "Orange";
                else                    color = "Red";
                jQuery("#" + this.id + " > div").css({ 'background': color });
            });
        });
    });

</script>
<?php
        function getPercentage($statementID, $statementValue)    {
            $total = 0;
            $elementTypes = q("SELECT id, name FROM ElementTypes WHERE company_id = '".$_SESSION["company_id"]."'");

            if (is_array($elementTypes))        {
                foreach ($elementTypes as $eT)  {
                    switch ($et[1])   {
                        case "INVOICE":
                            $value = "((consulting+expenses)+(consulting+expenses)*vat/100)";  //change to statement amount
                            $sum = q("SELECT SUM($value) FROM (LoadInvoices AS li INNER JOIN StatementLink AS sl ON li.id = sl.element_id AND sl.elementType = '".$et[0]."') ".
                                        "WHERE li.company_id = '".$_SESSION["company_id"]."' AND sl.statement_id = '".$statementID."'");
                            $total += $sum;
                            break;
                        case "EXPENSE":
                            $value = "((amount)+(amount)*vat/100)";
                            $sum = q("SELECT SUM($value) FROM (ExpenseBudget AS eb INNER JOIN StatementLink AS sl ON eb.id = sl.element_id AND sl.elementType = '".$et[0]."') ".
                                        "WHERE eb.company_id = '".$_SESSION["company_id"]."' AND sl.statement_id = '".$statementID."'");
                            $total += $sum;
                            break;
                        case "BUDGET":
                            $value = "(planned)";
                            $sum = q("SELECT SUM($value) FROM (Budget AS b INNER JOIN StatementLink AS sl ON b.id = sl.element_id AND sl.elementType = '".$et[0]."') ".
                                        "WHERE b.company_id = '".$_SESSION["company_id"]."' AND sl.statement_id = '".$statementID."'");
                            $total += $sum;
                            break;
                        default:
                            break;
                    }
                }
            }

            $percentage = ($statementValue != 0) ? number_format(($total / $statementValue) * 100, 0) : 0;

            return $percentage;
        }
	function cmpABS($a,$b)	{
            if ($a[3] == $b[3]) {				//check difference
                if ($a[0] == $b[0]) {return 0;}		//check date
                    return ($a[0] < $b[0]) ? -1 : 1;}	//check date
            return ($a[3] < $b[3]) ? -1 : 1;		//check difference
	}
	function getItems($amount,$date,$descr,$error)	{
            global $link;

            $descr = addslashes($descr);
            $error = ($amount * $error / 100);
            if ($error < 0)
            if ($error < -ERROR_MAR)        $error = -ERROR_MAR;
            if ($error > ERROR_MAR)         $error = ERROR_MAR;
            $upper = $amount + $error;
            $lower = $amount - $error;

            if ($amount > 0)	{	//Credit
                if ($link == "INVOICE")     {
                    $value = "((consulting+expenses)+(consulting+expenses)*vat/100)";
                    $arr = q("SELECT due_date, name, ROUND($value,2), ROUND(ABS($amount - $value),2) AS A, 'INVOICE', 'UNLINKED', L.id FROM LoadInvoices AS L ".
                            "LEFT JOIN StatementLink AS SL ON L.id = SL.element_id AND SL.elementType = (SELECT id FROM ElementTypes WHERE company_id = '".$_SESSION["company_id"]."' AND name = 'INVOICE') ".
                            "WHERE L.company_id = '".$_SESSION['company_id']."' AND paid != 1 ".
                            "AND $value > $lower AND $value < $upper ".
                            "AND SL.id IS NULL ".
                            "ORDER BY A, due_date");
                } else if ($link == "BUDGET")       {
                    $dateFrom = date('Ym',strtotime("-3 month"));
                    $dateTo   = date('Ym',strtotime("+1 month"));
                    $value = "(planned)";
                    $arr = q("SELECT B.due_date,CONCAT(DATE_FORMAT(CONCAT(B.year, '-', B.month, '-01'),'%b'),' - ',C.name,' - ',E.name),
                            -(ROUND($value,2)), ROUND(ABS(-$amount - $value),2) AS A,'BUDGET','UNLINKED',B.id 
                            FROM Budget AS B 
                            INNER JOIN Elements AS E ON B.element_id = E.id 
                            INNER JOIN Category AS C ON E.parent_id = C.id 
                            INNER JOIN Keywords AS K ON K.parent_id = E.id 
                            WHERE	(B.company_id = '".$_SESSION['company_id']."' AND CONCAT(B.year, B.month) 
                                            BETWEEN '$dateFrom' AND '$dateTo' 
                                            AND -$value < $lower AND -$value > $upper
                                    ) 
                                    OR (B.company_id = '".$_SESSION['company_id']."' AND CONCAT(B.year, B.month) 
                                            BETWEEN '$dateFrom' AND '$dateTo' 
                                            AND UPPER('$descr') LIKE UPPER(CONCAT('%', K.keyword, '%'))
                                    )
                            ORDER BY B.year DESC, B.month DESC");
                } else if ($link == "ALL")      {
                    $value = "((consulting+expenses)+(consulting+expenses)*vat/100)";
                    $exp = q("SELECT due_date, name, ROUND($value,2), ROUND(ABS($amount - $value),2) AS A, 'INVOICE', 'UNLINKED', L.id FROM LoadInvoices AS L ".
                            "LEFT JOIN StatementLink AS SL ON L.id = SL.element_id AND SL.elementType = (SELECT id FROM ElementTypes WHERE company_id = '".$_SESSION["company_id"]."' AND name = 'INVOICE') ".
                            "WHERE L.company_id = '".$_SESSION['company_id']."' AND paid != 1 ".
                            "AND $value > $lower AND $value < $upper ".
                            "AND SL.id IS NULL ".
                            "ORDER BY A, due_date");

                    $dateFrom = date('Ym',strtotime("-3 month"));
                    $dateTo   = date('Ym',strtotime("+1 month"));
                    $value = "(planned)";
                    $budget = q("SELECT B.due_date,CONCAT(DATE_FORMAT(CONCAT(B.year, '-', B.month, '-01'),'%b'),' - ',C.name,' - ',E.name),
                            -(ROUND($value,2)), ROUND(ABS(-$amount - $value),2) AS A,'BUDGET','UNLINKED',B.id 
                            FROM Budget AS B 
                            INNER JOIN Elements AS E ON B.element_id = E.id 
                            INNER JOIN Category AS C ON E.parent_id = C.id 
                            INNER JOIN Keywords AS K ON K.parent_id = E.id 
                            WHERE	(B.company_id = '".$_SESSION['company_id']."' AND CONCAT(B.year, B.month) 
                                            BETWEEN '$dateFrom' AND '$dateTo' 
                                            AND -$value < $lower AND -$value > $upper
                                    ) 
                                    OR (B.company_id = '".$_SESSION['company_id']."' AND CONCAT(B.year, B.month) 
                                            BETWEEN '$dateFrom' AND '$dateTo' 
                                            AND UPPER('$descr') LIKE UPPER(CONCAT('%', K.keyword, '%'))
                                    )
                            ORDER BY B.year DESC, B.month DESC");

                    if (is_array($exp) && is_array($budget))	{
                    	$arr = array_merge($exp,$budget);
                    	usort($arr,"cmpABS");
                    }
                    else $arr = (is_array($exp)) ? $exp : $budget;
                }

                $list = (is_array($arr)) ? $arr : array();
            }
            else if ($amount < 0)	{	//Debit
                if ($link == "EXPENSE")     {
                    $value = "((amount)+(amount)*vat/100)";
                    $arr = q("SELECT due_date, descr, -(ROUND($value,2)), ROUND(ABS(-$amount - $value),2) AS A, 'EXPENSE', 'UNLINKED', E.id FROM ExpenseBudget AS E ".
                            "LEFT JOIN StatementLink AS SL ON E.id = SL.element_id AND SL.elementType = (SELECT id FROM ElementTypes WHERE company_id = '".$_SESSION["company_id"]."' AND name = 'EXPENSE') ".
                            "WHERE E.company_id = '".$_SESSION['company_id']."' AND paid != 1 ".
                            "AND -$value < $lower AND -$value > $upper ".
                            "AND SL.id IS NULL ".
                            "ORDER BY A, due_date");
                }
                else if ($link == "BUDGET") {
                    $dateFrom = date('Ym',strtotime("-3 month"));
                    $dateTo   = date('Ym',strtotime("+1 month"));
                    $value = "(planned)";
                    $arr = q("SELECT B.due_date,CONCAT(DATE_FORMAT(CONCAT(B.year, '-', B.month, '-01'),'%b'),' - ',C.name,' - ',E.name),
                            -(ROUND($value,2)), ROUND(ABS(-$amount - $value),2) AS A,'BUDGET','UNLINKED',B.id 
                            FROM Budget AS B 
                            INNER JOIN Elements AS E ON B.element_id = E.id 
                            INNER JOIN Category AS C ON E.parent_id = C.id 
                            INNER JOIN Keywords AS K ON K.parent_id = E.id 
                            WHERE	(B.company_id = '".$_SESSION['company_id']."' AND CONCAT(B.year, B.month) 
                                            BETWEEN '$dateFrom' AND '$dateTo' 
                                            AND -$value < $lower AND -$value > $upper
                                    ) 
                                    OR (B.company_id = '".$_SESSION['company_id']."' AND CONCAT(B.year, B.month) 
                                            BETWEEN '$dateFrom' AND '$dateTo' 
                                            AND UPPER('$descr') LIKE UPPER(CONCAT('%', K.keyword, '%'))
                                    )
                            ORDER BY B.year DESC, B.month DESC");

                    if (is_array($arr))
                        if (count($arr) > 0)  {
                            $list_to_drop = array();
                            if (count($arr) > 1)  {
                                for($i = 1; $i < count($arr); ++$i)
                                    if ($arr[$i-1][6] == $arr[$i][6])
                                        unset($arr[$i-1]);
                            }
                        }
                } else if ($link == "ALL")      {
                    $value = "((amount)+(amount)*vat/100)";
                    $exp = q("SELECT due_date, descr, -(ROUND($value,2)), ROUND(ABS(-$amount - $value),2) AS A, 'EXPENSE', 'UNLINKED', E.id FROM ExpenseBudget AS E ".
                            "LEFT JOIN StatementLink AS SL ON E.id = SL.element_id AND SL.elementType = (SELECT id FROM ElementTypes WHERE company_id = '".$_SESSION["company_id"]."' AND name = 'EXPENSE') ".
                            "WHERE E.company_id = '".$_SESSION['company_id']."' AND paid != 1 ".
                            "AND -$value < $lower AND -$value > $upper ".
                            "AND SL.id IS NULL ".
                            "ORDER BY A, due_date");

                    $dateFrom = date('Ym',strtotime("-3 month"));
                    $dateTo   = date('Ym',strtotime("+1 month"));
                    $value = "(planned)";
                    $budget = q("SELECT B.due_date,CONCAT(DATE_FORMAT(CONCAT(B.year, '-', B.month, '-01'),'%b'),' - ',C.name,' - ',E.name),
                            -(ROUND($value,2)), ROUND(ABS(-$amount - $value),2) AS A,'BUDGET','UNLINKED',B.id 
                            FROM Budget AS B 
                            INNER JOIN Elements AS E ON B.element_id = E.id 
                            INNER JOIN Category AS C ON E.parent_id = C.id 
                            INNER JOIN Keywords AS K ON K.parent_id = E.id 
                            WHERE	(B.company_id = '".$_SESSION['company_id']."' AND CONCAT(B.year, B.month) 
                                            BETWEEN '$dateFrom' AND '$dateTo' 
                                            AND -$value < $lower AND -$value > $upper
                                    ) 
                                    OR (B.company_id = '".$_SESSION['company_id']."' AND CONCAT(B.year, B.month) 
                                            BETWEEN '$dateFrom' AND '$dateTo' 
                                            AND UPPER('$descr') LIKE UPPER(CONCAT('%', K.keyword, '%'))
                                    )
                            ORDER BY B.year DESC, B.month DESC");

                    if (is_array($budget))
                        if (count($budget) > 0)  {
                            $list_to_drop = array();
                            if (count($budget) > 1)  {
                                for($i = 1; $i < count($budget); ++$i)
                                    if ($budget[$i-1][6] == $budget[$i][6])
                                        unset($budget[$i-1]);
                            }
                        }

                    if (is_array($exp) && is_array($budget))	{
                    	$arr = array_merge($exp,$budget);
                    	usort($arr,"cmpABS");
                    }
                    else $arr = (is_array($exp)) ? $exp : $budget;
                }

                $list = (is_array($arr)) ? $arr : array();
            }
            if (is_array($list))
                array_splice($list, MAX_SUGGEST);
            return $list;
	}
	function getLinkedItems($s,$amount)	{
            $trans = q("SELECT sl.element_id, e.name FROM StatementLink AS sl INNER JOIN ElementTypes AS e ON sl.elementType = e.id WHERE sl.statement_id = '$s' ".
                        "AND sl.company_id = '".$_SESSION['company_id']."'");
            if (is_array($trans))
		foreach($trans as $tt)	{
			if ($tt[1] == "INVOICE")	{
                            $value = "((consulting+expenses)+(consulting+expenses)*vat/100)";
                            $l[] = q("SELECT due_date, name, ROUND($value,2), ROUND(ABS($amount - $value),2) AS A, 'INVOICE', 'LINKED', L.id FROM LoadInvoices AS L WHERE id = '".$tt[0]."'");
			}
			else if ($tt[1] == "EXPENSE")	{
                            $value = "((amount)+(amount)*vat/100)";
                            $l[] = q("SELECT due_date, descr, -(ROUND($value,2)), ROUND(ABS(-$amount - $value),2) AS A, 'EXPENSE', 'LINKED', E.id FROM ExpenseBudget AS E WHERE id = '".$tt[0]."'");
			}
			else if ($tt[1] == "BUDGET")	{
                            $value = "(planned)";
                            $l[] = q("SELECT B.due_date, CONCAT(DATE_FORMAT(CONCAT(B.year, '-', B.month, '-01'),'%b'),' - ',C.name,' - ',E.name), ".
					"-(ROUND($value,2)), ROUND(ABS(-$amount - $value),2) AS A, 'BUDGET', 'LINKED', B.id ".
					"FROM Budget AS B ".
					"INNER JOIN Elements AS E ON B.element_id = E.id ".
					"INNER JOIN Category AS C ON E.parent_id = C.id ".
					"WHERE B.id = '".$tt[0]."'");
			}
		}
            if (!is_array($l))	return;			//empty array
            if (count($l) == 1)	return $l[0];	//only 1 element in array
            $list = $l[0];
            for($i=1; $i<count($l);++$i)
                if (is_array($list))
                    if (is_array($l[$i]))
                        $list = array_merge($list,$l[$i]);
            return $list;						//merged all rows into 1 array
	}

	echo "<a>";
	//Check to see if there are inter account transfers we haven't matched yet
	$trans = q("SELECT s.id,s.account,s.description_new,s.amount,s.date FROM Statement AS s LEFT JOIN StatementLink AS sl ON s.id = sl.statement_id ".
				"WHERE s.company_id = '".$_SESSION['company_id']."' AND s.amount != 0 AND sl.id IS NULL ORDER BY date,amount");
	if (DEBUG)	{
	echo "<table style='border:solid 1px;'>";
	echo "<tr><td>ID</td><td>Acc</td><td>Desc</td><td>Amount</td><td>Date><td><--></td><td>ID</td><td>Acc</td><td>Desc</td><td>Amount</td><td>Date</td></tr>";
	}
	if (is_array($trans))
	foreach($trans as $a)	{
		foreach($trans as $b)	{
			if (!(strpos($a[2], $b[1]) === false) && $a[3] == -$b[3])	{	//match found, nou add link
				if (DEBUG)	{
				echo "<tr><td style='padding-right:10px;'>".$a[0]."</td><td style='padding-right:10px;'>".$a[1]."</td>
					<td style='padding-right:10px;'>".$a[2]."</td><td style='padding-right:10px;'>".$a[3]."</td><td style='padding-right:10px;'>".$a[4]."</td>
					<td style='padding-right:10px;'><--></td>
					<td style='padding-right:10px;'>".$b[0]."</td><td style='padding-right:10px;'>".$b[1]."</td><td style='padding-right:10px;'>".$b[2]."</td>
					<td style='padding-right:10px;'>".$b[3]."</td><td><td style='padding-right:10px;'>".$b[4]."</td><td></tr>";
				//echo ">>".$a[0]." = ".$b[0]." | ".$a[1]." = ".$b[1]." | ".$a[2]." = ".$b[2]." | ".$a[3]." = ".$b[3]."<br>";
				}
				$data[] = array($a,$b);
			}
		}
	}
	//Process matches array to only link 1 transfer to a statement. Will be looking at closest date
	$X = array();
	$prev = array(array(),array());
	for($i=0; $i < count($data); ++$i)	{
//		echo ">>".$prev[0][0]." | ".$data[$i][0][0]." <br>";
		if ($prev[0][0] == $data[$i][0][0])	{//we have more than 1 transaction
			$dateB = $data[$i][0][4];
			$date1 = $prev[1][4];
			$date2 = $data[$i][1][4];
			if (abs(strtotime($dateB) - strtotime($date1)) > abs(strtotime($dateB) - strtotime($date2)))	{
				array_pop($X);
				$X[] = $data[$i];
			}
		}
		else
			$X[] = $data[$i];
		$prev = $data[$i];
//		echo "<br>iteration: $i<br>";
	}
	$data = $X;
	
	if (DEBUG)	{
	echo "</table><br>";
	echo "<table style='border:solid 1px;'>";
	echo "<tr><td>ID</td><td>Acc</td><td>Desc</td><td>Amount</td><td>Date><td><--></td><td>ID</td><td>Acc</td><td>Desc</td><td>Amount</td><td>Date</td></tr>";
	}
	for($i=0; $i < count($data); ++$i)	{
		if (in_array(array($data[$i][1],$data[$i][0]),$data))	{
			$a = $data[$i][0];
			$b = $data[$i][1];
			if (DEBUG)	{
			echo "<tr><td style='padding-right:10px;'>".$a[0]."</td><td style='padding-right:10px;'>".$a[1]."</td>
				<td style='padding-right:10px;'>".$a[2]."</td><td style='padding-right:10px;'>".$a[3]."</td><td style='padding-right:10px;'>".$a[4]."</td>
				<td style='padding-right:10px;'><--></td>
				<td style='padding-right:10px;'>".$b[0]."</td><td style='padding-right:10px;'>".$b[1]."</td><td style='padding-right:10px;'>".$b[2]."</td>
				<td style='padding-right:10px;'>".$b[3]."</td><td><td style='padding-right:10px;'>".$b[4]."</td><td></tr>";
			}	
			$statement_id = $data[$i][0][0];
			$element_id = $data[$i][1][0];
			$exist = q("SELECT id,id FROM StatementLink WHERE company_id = '".$_SESSION["company_id"]."' AND statement_id = $statement_id AND 
				element_id=$element_id AND elementType=(SELECT id FROM ElementTypes WHERE name='TRANSFER' AND company_id='".$_SESSION["company_id"]."')");
			if (!is_array($exist))	{
				$insert = q("INSERT INTO StatementLink (company_id,statement_id,element_id,elementType) VALUES ".
					"('".$_SESSION["company_id"]."',$statement_id,$element_id,
					(SELECT id FROM ElementTypes WHERE name = 'TRANSFER' AND company_id = '".$_SESSION["company_id"]."'))");
			}
		}
	}
	if (DEBUG)	{
	echo "</table>";
	}

	//GET transactions
	//	all none linked and non-zero amounts
	$trans = q("SELECT s.date,s.description,s.amount,s.id FROM Statement AS s LEFT JOIN StatementLink AS sl ON s.id = sl.statement_id ".
				"WHERE s.company_id = '".$_SESSION['company_id']."' AND s.amount != 0 AND sl.id IS NULL ORDER BY date,amount");
	$display = "<table class='on-table'><tr><td class='on-table-clear' colspan='8'><a>Bank Statements<a></td></tr>";
	$display .= "<tr><th></th><th>Date</th><th>Description</th><th>Amount</th>".
		"<th style='background-color:#36332a'><font style='size:17pt; color:orange'>Matches in Ontrack</font><br/>".
		"<table><tr>
				<td style='background-color:#36332a;min-width:45px;max-width:45px'>Due_Date</td>
				<td style='background-color:#36332a;min-width:120px;max-width:120px'>Description</td>
				<td class='rightdata' style='background-color:#36332a;min-width:40px;max-width:40px'>Amount</td>
				<td class='rightdata' style='background-color:#36332a;min-width:35px;max-width:35px'>Diff</td>
				<td style='background-color:#36332a;min-width:35px'>Type</td>
				<td style='background-color:#36332a;min-width:15px'>Link / Unlink</td>
			</tr></table>
		</th></tr>";
if (is_array($trans))
foreach($trans as $tt)	{
	$display .= "<tr>
					<td>
						<input type='image' src='images/icons/no.png' title='Correct' />
					</td>
					<td style='white-space:nowrap;padding-left:0;'>
						<input type='hidden' value='".$tt[3]."'/>".$tt[0]."</td><td>".$tt[1]."</td>
					<td class='rightdata' style='white-space:nowrap;border-right:1px solid #36332a;'>".$tt[2]."</td>
					<td>";
	$linkedList = getLinkedItems($tt[3],$tt[2]);
	$list = getItems($tt[2],$tt[0],$tt[1],ERROR_TOL);
	if (is_array($linkedList) && is_array($list))	
		$list = array_merge($linkedList,$list);
	else if (is_array($linkedList))
		$list = $linkedList;
	if (is_array($list))	{
		$display .= "<table>";
		foreach($list as $r)	{
			$diaplay .= "<tr>";
				$display .= "<td class='noborder' style='white-space:nowrap;min-width:45px;max-width:45px'>".$r[0]."</td>";
				$display .= "<td class='noborder' style='min-width:120px;max-width:120px'>".$r[1]."</td>";
				$display .= "<td class='noborder rightdata' style='white-space:nowrap;min-width:40px;max-width:40px;color:white;'>".$r[2]."</td>";
				$display .= "<td class='noborder rightdata' style='white-space:nowrap;min-width:35px;max-width:35px'>".$r[3]."</td>";
				$display .= "<td class='noborder' style='white-space:nowrap;min-width:35px;max-width:35px'>".$r[4]."</td>";
				if ($r[5] == "UNLINKED")
					$display.="<td class='noborder' style='min-width:15px;max-width:15px'>
						<input class='link' type='image' id='link' style='color:blue' src='images/icons/unlinked.png' title='Unlinked' value='L'/>
						<input type='hidden' id='statement_id' value='".$tt[3]."'/>
						<input type='hidden' id='element_id' value='".$r[6]."'/>
						<input type='hidden' id='type' value='".$r[4]."'/>
					</td>";
				else
					$display.="<td class='noborder' style='min-width:15px;max-width:15px'>
						<input class='link' type='image' id='link' style='color:red' src='images/icons/linked.png' title='Linked' value='U'/>
						<input type='hidden' id='statement_id' value='".$tt[3]."'/>
						<input type='hidden' id='element_id' value='".$r[6]."'/>
						<input type='hidden' id='type' value='".$r[4]."'/>
					</td>";
			$display .= "</tr>";
		}
			$diaplay .= "<tr>";
				$display .= "<td class='noborder' style='white-space:nowrap;min-width:45px;max-width:45px'></td>";
				$display .= "<td class='noborder' style='min-width:120px;max-width:120px'></td>";
				$display .= "<td class='noborder rightdata' style='white-space:nowrap;min-width:40px;max-width:40px;color:white;'></td>";
				$display .= "<td class='noborder rightdata' style='white-space:nowrap;min-width:35px;max-width:35px'></td>";
				$display .= "<td class='noborder' style='white-space:nowrap;min-width:35px;max-width:35px'></td>";
				$display.="<td class='noborder' style='min-width:15px;max-width:15px'>
					<input class='link' type='image' id='link' style='color:green' src='images/icons/manual.png' title='Manual' value='M'/>
					<input type='hidden' id='statement_id' value=''/>
					<input type='hidden' id='element_id' value=''/>
					<input type='hidden' id='type' value=''/>
				</td>";
			$display .= "</tr>";
		$display .= "</table>";
	}
	else	{//no match found. add manual linking option
		$display .= "<table>";
			$diaplay .= "<tr>";
				$display .= "<td class='noborder' style='white-space:nowrap;min-width:45px;max-width:45px'></td>";
				$display .= "<td class='noborder' style='min-width:120px;max-width:120px'></td>";
				$display .= "<td class='noborder rightdata' style='white-space:nowrap;min-width:40px;max-width:40px;color:white;'></td>";
				$display .= "<td class='noborder rightdata' style='white-space:nowrap;min-width:35px;max-width:35px'></td>";
				$display .= "<td class='noborder' style='white-space:nowrap;min-width:35px;max-width:35px'></td>";
				$display.="<td class='noborder' style='min-width:15px;max-width:15px'>
					<input class='link' type='image' id='link' style='color:green' src='images/icons/manual.png' title='Manual' value='M'/>
					<input type='hidden' id='statement_id' value=''/>
					<input type='hidden' id='element_id' value=''/>
					<input type='hidden' id='type' value=''/>
				</td>";
			$display .= "</tr>";
		$display .= "</table>";
	}
	$display .= "</td></tr>";
}
	$display .= "</table>";
	echo $display;
	echo '</a>';
?>
	<!--  BEGIN: MANUAL LINKING MODAL  -->
	<div id="modal_manualLink" name="modal_manualLink" class="on-contact-modal" style='margin-left:auto;margin-right:auto;text-align:center;'>
		<table style='width:100%'>
			<tr>
				<td colspan="100%" class='centerdata'><a><b>Manual Linking</b></a><br><br></td>
			</tr>
			<tr>
				<td><a><span id='dateMS'></span></a></td>
				<td class="centerdata"><a><span id='descrMS'></span></a></td>
				<td class="rightdata"><a><span id='amountMS'></span></a></td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td colspan='100%' ><hr></hr></td></tr>
			<tr><td>&nbsp;</td></tr>
                </table>
                <div id="linkedItems" name="linkedItems" class="">
                    <table style='width:100%'>
                        <tr>
                            <td class='noborder'><a>[date]</a></td>
                            <td class='noborder'><a>[type]</a></td>
                            <td class='noborder'><a>[desciption]</a></td>
                            <td class='noborder'><a>[amount]</a></td>
                            <td class='noborder'>
                                <input id="hiddenM" name="hiddenM" type="hidden" value="" />
                                <input id="checkM" name="checkM" type="checkbox" value="checkbox" />
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <br/>
                    <table style='width:100%'>
                        <tr>
                            <td class="centerdata">
                                <input id="linkItems" name="linkItems" type="button" value="Link" />
                                &nbsp;&nbsp;&nbsp;
                                <input id="cancelManualLink" name="cancelManualLink" type="button" value="Cancel" />
                            </td>
                        </tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td colspan='100%' ><hr></hr></td></tr>
                        <tr><td>&nbsp;</td></tr>
                    </table>
                </div>
		<table style='width:100%'>
			<tr>
				<td colspan='100%' class="centerdata"><a>Ontrack Element to Link:</a></td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td colspan='100%' class="centerdata"><a style='margin-top:5px;'>Transaction Type</a>&nbsp;
				<?php
					echo "<select id='typeM' name='typeM' value=''>";
						echo "<option value='0'></option>";
						//TODO: change elementypes to have company and active
						$p = q("SELECT id, name FROM ElementTypes WHERE company_id = '".$_SESSION["company_id"]."' AND name != 'TRANSFER' ORDER BY name");
						if (is_array($p))
						foreach ($p as $r)
							if ($r[0] == $value)
								echo "<option value='$r[0]' selected >$r[1]&nbsp;&nbsp;</option>";
							else
								echo "<option value='$r[0]'>$r[1]&nbsp;&nbsp;</option>";
					echo "</select>";
				?>
				&nbsp;
			  	<?php

					$first_day_of_month = date('Y-m',time()) . '-01 00:00:01';
					$t = strtotime($first_day_of_month);
					for($i = -5; $i <= 3; ++$i)	{
						$dates_[$i][] = date('Ym',strtotime("$i months",$t));
						$dates_[$i][] = date('Y M',strtotime("$i months",$t));
					}
					echo "<span id='typeDateM_layer' style='display:none'>";
					echo "<select id='typeDateM' name='typeDateM' value=''>";
						echo "<option value='0'></option>";
						if (is_array($dates_))
						foreach ($dates_ as $r)
							echo "<option value='$r[0]'>$r[1]&nbsp;&nbsp;</option>";
					echo "</select>";
					echo "</span>";
				?>
				&nbsp;
			  	<?php
					echo "<input type='text' id='typeFilterM' name='typeFilterM' value=''>";
				?>
				</td>
			</tr>
			<tr><td>&nbsp;</td></tr>
		</table>
		<div style="height:300px;overflow:scroll;overflow-x:hidden;">
		<table id='searchMData'>
			<tr>
				<td class='noborder'><a>[date]</a></td>
				<td class='noborder'><a>[type]</a></td>
				<td class='noborder'><a>[desciption]</a></td>
				<td class='noborder'><a>[amount]</a></td>
				<td class='noborder'>
					<input id="hiddenM" name="hiddenM" type="hidden" value="" />
					<input id="checkM" name="checkM" type="checkbox" value="checkbox" />
				</td>
			</tr>
		</table>
		</div>
	</div>
	<!--  END: MANUAL LINKING MODAL  -->

<?php
    //  Print Footer
    print_footer();
?>
