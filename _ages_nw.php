<?php
    include("_db.php");
?>
<html>
    <head>
        <title>
            On-Track - Script...
        </title>
    </head>
    <body>
        <h1 align="center">
            On-Track - Script...
        </h1>
        <?php
            $date = date("Y-m-d H:i:s");
            $user = "admin";

			//  DHSPS&L Geotech Investigation Vryburg&Huhudi => NWDHS Taung Ext 6 Geotech
			$update = q("UPDATE TimeSheet SET project_id = '3746' WHERE project_id = '5555'");
			$update = q("UPDATE ApprovedTime SET project_id = '3746' WHERE project_id = '5555'");
			$update = q("UPDATE ExpenseSheet SET project_id = '3746' WHERE project_id = '5555'");
			$update = q("UPDATE ApprovedExpense SET project_id = '3746' WHERE project_id = '5555'");
			$update = q("UPDATE Project SET completed = '1' WHERE id = '5555'");

			//Stephan Pretorius: R1300
			$userID = q("SELECT id FROM Employee WHERE email = 'spretorius@ages-group.com'");
			$update = q("UPDATE TimeSheet SET rate = '1300.00' WHERE project_id = '3746' AND user_id = '".$userID."'");
			$update = q("UPDATE ApprovedTime SET rate = '1300.00' WHERE project_id = '3746' AND user_id = '".$userID."'");
			//Stephan Potgieter: R1100
			$userID = q("SELECT id FROM Employee WHERE email = 'spotgieter@ages-group.com'");
			$update = q("UPDATE TimeSheet SET rate = '1100.00' WHERE project_id = '3746' AND user_id = '".$userID."'");
			$update = q("UPDATE ApprovedTime SET rate = '1100.00' WHERE project_id = '3746' AND user_id = '".$userID."'");
			//Johannes Potgieter: R500
			$userID = q("SELECT id FROM Employee WHERE email = 'jmpotgieter@ages-group.com'");
			$update = q("UPDATE TimeSheet SET rate = '500.00' WHERE project_id = '3746' AND user_id = '".$userID."'");
			$update = q("UPDATE ApprovedTime SET rate = '500.00' WHERE project_id = '3746' AND user_id = '".$userID."'");
			//Willem Meintjes: R550
			$userID = q("SELECT id FROM Employee WHERE email = 'wmeintjes@ages-group.com'");
			$update = q("UPDATE TimeSheet SET rate = '550.00' WHERE project_id = '3746' AND user_id = '".$userID."'");
			$update = q("UPDATE ApprovedTime SET rate = '550.00' WHERE project_id = '3746' AND user_id = '".$userID."'");
			//Michael Pyburn: R500
			$userID = q("SELECT id FROM Employee WHERE email = 'mpyburn@ages-group.com'");
			$update = q("UPDATE TimeSheet SET rate = '500.00' WHERE project_id = '3746' AND user_id = '".$userID."'");
			$update = q("UPDATE ApprovedTime SET rate = '500.00' WHERE project_id = '3746' AND user_id = '".$userID."'");
			//Nico van Zyl: R550
			$userID = q("SELECT id FROM Employee WHERE email = 'nvanzyl@ages-group.com'");
			$update = q("UPDATE TimeSheet SET rate = '550.00' WHERE project_id = '3746' AND user_id = '".$userID."'");
			$update = q("UPDATE ApprovedTime SET rate = '550.00' WHERE project_id = '3746' AND user_id = '".$userID."'");
			//Johan Smit: R650
			$userID = q("SELECT id FROM Employee WHERE email = 'jsmit@ages-group.com'");
			$update = q("UPDATE TimeSheet SET rate = '650.00' WHERE project_id = '3746' AND user_id = '".$userID."'");
			$update = q("UPDATE ApprovedTime SET rate = '650.00' WHERE project_id = '3746' AND user_id = '".$userID."'");
			//Mari La Grange: R450
			$userID = q("SELECT id FROM Employee WHERE email = 'mlagrange@ages-group.com'");
			$update = q("UPDATE TimeSheet SET rate = '450.00' WHERE project_id = '3746' AND user_id = '".$userID."'");
			$update = q("UPDATE ApprovedTime SET rate = '450.00' WHERE project_id = '3746' AND user_id = '".$userID."'");
			//Hanneke Roux: R500
			$userID = q("SELECT id FROM Employee WHERE email = 'hroux@ages-group.com'");
			$update = q("UPDATE TimeSheet SET rate = '500.00' WHERE project_id = '3746' AND user_id = '".$userID."'");
			$update = q("UPDATE ApprovedTime SET rate = '500.00' WHERE project_id = '3746' AND user_id = '".$userID."'");
			//Eduan Hattingh: R450
			$userID = q("SELECT id FROM Employee WHERE email = 'ehattingh@ages-group.com'");
			$update = q("UPDATE TimeSheet SET rate = '450.00' WHERE project_id = '3746' AND user_id = '".$userID."'");
			$update = q("UPDATE ApprovedTime SET rate = '450.00' WHERE project_id = '3746' AND user_id = '".$userID."'");


			//  ESKOM Resources Mining Evaluation Tender => ESKOM Contract Management (Geology)
			$update = q("UPDATE TimeSheet SET project_id = '5600' WHERE project_id = '5302'");
			$update = q("UPDATE ApprovedTime SET project_id = '5600' WHERE project_id = '5302'");
			$update = q("UPDATE ExpenseSheet SET project_id = '5600' WHERE project_id = '5302'");
			$update = q("UPDATE ApprovedExpense SET project_id = '5600' WHERE project_id = '5302'");
			$update = q("UPDATE Project SET completed = '1' WHERE id = '5302'");

			//Willem Meintjes: R550
			$userID = q("SELECT id FROM Employee WHERE email = 'wmeintjes@ages-group.com'");
			$update = q("UPDATE TimeSheet SET rate = '550.00' WHERE project_id = '5600' AND user_id = '".$userID."'");
			$update = q("UPDATE ApprovedTime SET rate = '550.00' WHERE project_id = '5600' AND user_id = '".$userID."'");
			//Johannes Potgieter: R500
			$userID = q("SELECT id FROM Employee WHERE email = 'jmpotgieter@ages-group.com'");
			$update = q("UPDATE TimeSheet SET rate = '500.00' WHERE project_id = '5600' AND user_id = '".$userID."'");
			$update = q("UPDATE ApprovedTime SET rate = '500.00' WHERE project_id = '5600' AND user_id = '".$userID."'");
			//Stephan Pretorius: R1300
			$userID = q("SELECT id FROM Employee WHERE email = 'spretorius@ages-group.com'");
			$update = q("UPDATE TimeSheet SET rate = '1300.00' WHERE project_id = '5600' AND user_id = '".$userID."'");
			$update = q("UPDATE ApprovedTime SET rate = '1300.00' WHERE project_id = '5600' AND user_id = '".$userID."'");
			//Stephan Potgieter: R1100
			$userID = q("SELECT id FROM Employee WHERE email = 'spotgieter@ages-group.com'");
			$update = q("UPDATE TimeSheet SET rate = '1100.00' WHERE project_id = '5600' AND user_id = '".$userID."'");
			$update = q("UPDATE ApprovedTime SET rate = '1100.00' WHERE project_id = '5600' AND user_id = '".$userID."'");
			//Johan Smit: R650
			$userID = q("SELECT id FROM Employee WHERE email = 'jsmit@ages-group.com'");
			$update = q("UPDATE TimeSheet SET rate = '650.00' WHERE project_id = '5600' AND user_id = '".$userID."'");
			$update = q("UPDATE ApprovedTime SET rate = '650.00' WHERE project_id = '5600' AND user_id = '".$userID."'");


            echo "<p align='center'>Script completed successfully...</p>";
        ?>
        <form action="index.php" method="post">
            <center><input type="submit" value="Return"/></center>
        </form>
    </body>
</html>
