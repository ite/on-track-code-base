<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");
        
    if (!hasAccess("REP_BUDGET"))
        header("Location: noaccess.php");
        
    // Class for table background color
    $colorClass = "OnTrack_#_";

    //  Set Report Generated Status
    $generated = "0";

    //  Create Display Information Function
    if (isset($_POST["save"]) && $_POST["save"] === "1") {
        $errorMessage = "";

        $year = $_POST["year"];
        $month = $_POST["month"];
        $displayString = "";
        
        $excelheadings[$row][] = "Report: Running Cost";
        $excelheadings[$row][] = "";
        $excelheadings[$row][] = "";
        $excelheadings[$row][] = "";
        $excelheadings[$row][] = "";
        $excelheadings[$row][] = "";
            $row++;
            
        $exceldata[$row][] = $colorClass."Category";
        $exceldata[$row][] = $colorClass."Element";
        $exceldata[$row][] = $colorClass."Planned";
        $exceldata[$row][] = $colorClass."Actual";
        $exceldata[$row][] = $colorClass."Paid";
        $exceldata[$row][] = $colorClass."Date Payable";
            $row++;
        
        $headers = "<tr>".
                    "<th>Category</td>".
                    "<th>Element</td>".
                    "<th>Planned<br/>".
                        "Amount <i>(".$_SESSION["currency"].")</i></th>".
                    "<th>Actual<br/>".
                        "Amount <i>(".$_SESSION["currency"].")</i></th>".
                    "<th>Paid</td>".
                    "<th>Date Payable</td></tr>";

        $grand_total_planned = q("SELECT SUM(planned) FROM Budget WHERE year = '".$year."' AND month = '".$month."' AND company_id = '".$_SESSION["company_id"]."'");
        $grand_total_amount = q("SELECT SUM(amount) FROM Budget WHERE year = '".$year."' AND month = '".$month."' AND company_id = '".$_SESSION["company_id"]."'");
        $grand_total_actual = q("SELECT SUM(amount) FROM Budget WHERE year = '".$year."' AND month = '".$month."' AND company_id = '".$_SESSION["company_id"]."' ".
                                "AND actual = '1'");

        if ($grand_total_planned == "")
            $grand_total_planned = "-";

        if ($grand_total_amount == "")
            $grand_total_amount = "-";

        if ($grand_total_actual == "")
            $grand_total_actual = "-";

        $categories = q("SELECT DISTINCT(c.id), c.name FROM (Category AS c INNER JOIN Elements AS e ON c.id = e.parent_id) WHERE e.status = '1' ".
                        "AND c.company_id = '".$_SESSION["company_id"]."' AND e.id IN (SELECT element_id FROM Budget WHERE year = '".$year."' AND month = '".$month."' ".
                        "AND company_id = '".$_SESSION["company_id"]."') ORDER BY c.name");

        if (is_array($categories)) {
            $excel_info; 
            $row_counter = 0;
            foreach ($categories as $category) {
               
                $elements = q("SELECT id, name, status FROM Elements WHERE parent_id = '".$category[0]."' AND status = '1' ".
                                "AND id IN (SELECT element_id FROM Budget WHERE year = '".$year."' AND month = '".$month."' ".
                                "AND company_id = '".$_SESSION["company_id"]."') ORDER BY name");

                $displayString .= "<tr>";
                $displayString .= "<td>";
                $displayString .= "".$category[1]."";
                $displayString .= "</td>";
                $excel_info[$row_counter ][0] = $category[1];       // 1
                
                if (is_array($elements)) {
                    $displayString .= "<td>";
                    $arrElem = array();
                    foreach ($elements as $element) {
                        $displayString .= "".$element[1]."</a><br/>";
                        $arrElem[] = $element[1];       // 2 - Element
                    }
                    $excel_info[$row_counter ][1] = $arrElem;
                    $displayString .= "</td>";
                    
                    //  Planned Amount
                    $displayString .= "<td class='rightdata'>";
                    $arrPlanned = array();
                    foreach ($elements as $element) {
                        $amount = q("SELECT planned FROM Budget WHERE year = '$year' AND month = '$month' AND element_id = '".$element[0]."'");

                        if ($amount){
                            $displayString .= "".$_SESSION["currency"]." ".number_format($amount, 2, ".", "")."<br/>";
                            //$excel_info[$row_counter ][2] .= $_SESSION["currency"]." ".number_format($amount, 2, ".", "");      // 3
                            $arrPlanned[] = $_SESSION["currency"]." ".number_format($amount, 2, ".", ""); 
                        }else{
                            $displayString .= "-<br/>";
                            //$excel_info[$row_counter ][2] .= "-";       // 3
                            $arrPlanned[] = "-";  
                        }
                    }
                    $excel_info[$row_counter ][2] = $arrPlanned;
                    $displayString .= "</td>";
                    //  Actual Amount
                    $displayString .= "<td class='rightdata'>";
                    $arrActual = array();
                    foreach ($elements as $element) {
                        $amount = q("SELECT amount FROM Budget WHERE year = '$year' AND month = '$month' AND element_id = '".$element[0]."'");

                        if ($amount){
                            $displayString .= "".$_SESSION["currency"]." ".number_format($amount, 2, ".", "")."<br/>";
                            //$excel_info[$row_counter ][3] .= $_SESSION["currency"]." ".number_format($amount, 2, ".", "");      // 4 - Actual
                            $arrActual[] = $_SESSION["currency"]." ".number_format($amount, 2, ".", "");
                        }else{
                            //$excel_info[$row_counter ][3] .= "-";       // 4 - Actual
                            $arrActual[] = "-";  
                        }
                    }
                    $excel_info[$row_counter ][3] = $arrActual;
                    $displayString .= "</td>";
                    //  Paid
                    $displayString .= "<td class='rightdata'>";
                    $arrPaid = array();
                    foreach ($elements as $element) {
                        $bool = q("SELECT actual FROM Budget WHERE year = '$year' AND month = '$month' AND element_id = '".$element[0]."'");

                        if ($bool == "1"){
                            $displayString .= "Yes<br/>";
                            //$excel_info[$row_counter ][4] .= "Yes";     // 5
                            $arrPaid [] = "Yes";  
                        }else{
                            $displayString .= "No<br/>";
                            //$excel_info[$row_counter ][4] .= "No";      // 5
                            $arrPaid [] = "No";  
                        }
                    }
                    $excel_info[$row_counter ][4] = $arrPaid;
                    $displayString .= "</td>";
                    //  Date
                    $displayString .= "<td class='rightdata'>";
                    $arrDateP = array();
                    foreach ($elements as $element) {
                        $due_date = q("SELECT due_date FROM Budget WHERE year = '$year' AND month = '$month' AND element_id = '".$element[0]."'");

                        if ($due_date){
                            $displayString .= "".$due_date."<br/>";
                            //$excel_info[$row_counter ][5] .= $due_date;     // 6
                            $arrDateP[] = $due_date; 
                        }else{
                            $displayString .= "-<br/>";
                            //$excel_info[$row_counter ][5] .= "-";       // 6
                            $arrDateP[] = "-"; 
                        }
                    }
                    $excel_info[$row_counter ][5]  = $arrDateP;
                    $displayString .= "</td>";                    
                }
                else {
                    $displayString .= "<td>";
                    $displayString .= "-";
                    $displayString .= "</td>";
                }
                $displayString .= "</tr>"; 
                $row_counter++;
            }
            
            $rowNum = $row-2;
           
            for($i = 0; $i<$row_counter; $i++){
                $exceldata[$row][] = $excel_info[$rowNum][0];
                $exceldata[$row][] = $excel_info[$rowNum][1];
                $exceldata[$row][] = $excel_info[$rowNum][2];
                $exceldata[$row][] = $excel_info[$rowNum][3];
                $exceldata[$row][] = $excel_info[$rowNum][4];
                $exceldata[$row][] = $excel_info[$rowNum][5];
                    $rowNum++;
                    $row++;
            }
        }

        if ($displayString != "") {
            $displayString = $headers.$displayString."<tr>".
                                "<td class='on-table-total' colspan='2'>Grand Total <i>(".
                                    $_SESSION["currency"].")</i>:</td>".
                                "<td class='on-table-total'>".$_SESSION["currency"]." ".number_format($grand_total_planned, 2, ".", "")."</td>".
                                "<td class='on-table-total'>".$_SESSION["currency"]." ".number_format($grand_total_amount, 2, ".", "")."</td>".
                                "<td class='on-table-total'></td>".
                                "<td class='on-table-total'>".$_SESSION["currency"]." ".number_format($grand_total_actual, 2, ".", "")."</td></tr>";
                                
            $exceldata[$row][] = "";
            $exceldata[$row][] = "Grand Total (".$_SESSION["currency"]."):";
            $exceldata[$row][] = $_SESSION["currency"]." ".number_format($grand_total_planned, 2, ".", "");
            $exceldata[$row][] = $_SESSION["currency"]." ".number_format($grand_total_amount, 2, ".", "");
            $exceldata[$row][] = $_SESSION["currency"]." ".number_format($grand_total_actual, 2, ".", "");
            $exceldata[$row][] = "";

            $generated = "1";
        }
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("5", "reports");

    if ($errorMessage != "")
    {
        echo "<p align='center' style='padding:0px;'><strong><font color='#999999'>$errorMessage</font></strong></p>";
        echo "<br/>";
    }
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    function check()
    {
        var valid                                                       = 1;

        //  Check That Year Is Selected
        if (document.forms["report_budget"].year.value == "null")
        {
            ShowLayer("yearDiv", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("yearDiv", "none");

        //  Check That Month Is Selected
        if (document.forms["report_budget"].month.value == "null")
        {
            ShowLayer("monthDiv", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("monthDiv", "none");

        if (valid == 1)
        {
            document.forms["report_budget"].save.value                  = 1;
            document.forms["report_budget"].submit();
        }
    }
</script>
<?php
    //  nYears                                                          = Number of Years
    $nYears                                                             = q("SELECT COUNT(DISTINCT(year)) FROM Budget WHERE company_id = '".$_SESSION["company_id"]."'");
    $years                                                              = q("SELECT DISTINCT(year) FROM Budget WHERE company_id = '".$_SESSION["company_id"]."' ".
                                                                            "ORDER BY year DESC");
    //  nMonths                                                         = Number of Months
    $nMonths                                                            = q("SELECT COUNT(DISTINCT(month)) FROM Budget WHERE company_id = '".$_SESSION["company_id"]."'");
    $months                                                             = q("SELECT DISTINCT(month) FROM Budget WHERE company_id = '".$_SESSION["company_id"]."' ".
                                                                            "ORDER BY month");
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="report_budget">
                    <div id="" style="<?php if ($generated == "1") echo "display: none;"; else echo "display: block"; ?>">
                        <table width="100%">
                            <tr>
                                <td class="centerdata">
                                    <h6>Running Cost Report</h6>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Year:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="year">
                                        <option value="null">--  Select Year  --</option>
                                        <?php
                                            if ($nYears > 1)
                                                foreach ($years as $year)
                                                    if ($_POST["year"] == $year[0])
                                                        echo "<option value='".$year[0]."' selected>".$year[0]."</option>";
                                                    else
                                                        echo "<option value='".$year[0]."'>".$year[0]."</option>";
                                            else if ($nYears == 1)
                                                if ($_POST["year"] == $years)
                                                    echo "<option value='".$years."' selected>".$years."".
                                                        "</option>";
                                                else
                                                    echo "<option value='".$years."'>".$years."</option>";
                                        ?>
                                    </select>
                                    <div id="yearDiv" style="display: none;"><font class="on-validate-error">* Year must be selected</font></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Month:
                                </td>
                                <td align="left" width="50%">
                                    <select class="on-field" method="post" name="month">
                                        <option value="null">--  Select Month  --</option>
                                        <?php
                                            if ($nMonths > 1)
                                                foreach ($months as $month)
                                                    if ($_POST["month"] == $month[0])
                                                        echo "<option value='".$month[0]."' selected>".getMonthString($month[0])."</option>";
                                                    else
                                                        echo "<option value='".$month[0]."'>".getMonthString($month[0])."</option>";
                                            else if ($nMonths == 1)
                                                if ($_POST["month"] == $months)
                                                    echo "<option value='".$months."' selected>".getMonthString($months)."".
                                                        "</option>";
                                                else
                                                    echo "<option value='".$months."'>".getMonthString($months)."</option>";
                                        ?>
                                    </select>
                                    <div id="monthDiv" style="display: none;"><font class="on-validate-error">* Month must be selected</font></div>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <input name="btnGenerate" onClick="check();" type="button" value="Generate Report">
                        <input method="post" name="save" type="hidden" value="0" />
                    </div>
                </form>
                <div id="report" style="<?php if ($generated == "1") echo "display: block;"; else echo "display: none"; ?>">
                    <?php
                        $report_heading = "<tr>
                                                        <td align='center' colspan='6'><a>Running Cost Report</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td align='center' colspan='6'>Company Name: ".$_SESSION["company_name"]."</td>
                                                    </tr>";

                        echo "<div class='on-20px'><table class='on-table-center on-table'>";
                            echo "".$report_heading.$displayString;
                            echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        echo "</table></div>";
                        echo "<br/>";

                        ///////////////////////////
                        //  Set Export Information
                        $_SESSION["fileName"] = "Running_Cost_Report";
                        $_SESSION["fileData"] = array_merge($excelheadings, $exceldata);
                        ///////////////////////////

                        echo "<input name='btnExport' onClick=\"location.href='_export.php'\" type='button' value='Export to Excel' />";
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>