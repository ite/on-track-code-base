<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        $company_id                                                     = null;

    if (isset($_POST["company"]) && is_numeric($_POST["company"]))
    {
        $_SESSION["company_id"]                                         = $_POST["company"];
        $_SESSION["company_name"]                                       = q("SELECT name FROM Company WHERE id = '".$_POST["company"]."'");
        $_SESSION["currency"]                                           = q("SELECT currency FROM Company WHERE id = '".$_POST["company"]."'");

        header("Location: book_time.php");
    }
    else if (isset($_POST["company"]) && !is_numeric($_POST["company"]))
    {
        unset($_SESSION["company_id"]);
        unset($_SESSION["company_name"]);
        unset($_SESSION["currency"]);
    }

    if ($_SESSION["logged_in"] === true)
    {
        $count                                                          = q("SELECT COUNT(c.id) FROM (Company as c INNER JOIN Company_Users as cu ".
                                                                            "ON c.id = cu.company_id) WHERE cu.user_id = '".$_SESSION["user_id"]."' ".
                                                                            "AND c.locked = '0'");
        //$count                                                          = q("SELECT COUNT(id) FROM Company_Users WHERE user_id = '".$_SESSION["user_id"]."'");

        if ($count == 0)
            header("Location: locked.php");
        else if ($count == 1)
        {
            $_SESSION["company_id"]                                     = q("SELECT company_id FROM Company_Users WHERE user_id = '".$_SESSION["user_id"]."'");
            $_SESSION["company_name"]                                   = q("SELECT name FROM Company WHERE id = '".$_SESSION["company_id"]."'");
            $_SESSION["currency"]                                       = q("SELECT currency FROM Company WHERE id = '".$_SESSION["company_id"]."'");

            header("Location: book_time.php");
        }
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0", "home");
?>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="left" valign="top">
                <form action="" method="post" name="home">
                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="left" valign="top">
                                <a>
                                    Select Company:&nbsp;
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top">
                                &nbsp;
                                <select method="post" name="company" onChange="submit();" tabindex="1">
                                    <option value="null">--  Select Company  --&nbsp;&nbsp;</option>
                                    <?php
                                        //  Get Company Info
                                        $company                        = q("SELECT c.id, c.name FROM (Company as c INNER JOIN Company_Users as cu ".
                                                                            "ON c.id = cu.company_id) WHERE cu.user_id = '".$_SESSION["user_id"]."' ".
                                                                            "AND c.locked = '0' ORDER BY c.name");

                                        //  Choose Company To Work On
                                        foreach ($company as $c)
                                        {
                                            if ($c[0] == $_SESSION["company_id"])
                                                echo "<option value='".$c[0]."' selected>".$c[1]."&nbsp;&nbsp;</option>";
                                            else
                                                echo "<option value='".$c[0]."'>".$c[1]."&nbsp;&nbsp;</option>";
                                        }
                                    ?>
                                </select>
                            </td>
                        </tr>
                    </table>
                </form>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>