<?php
    session_start();

    $errorMessage                                                       = "";

    include("_db.php");

    if ($_SESSION["logged_in"] === true)
        header("Location: home.php");

    if (isset($_POST["txtUsername"]) && isset($_POST["txtPassword"]))
    {
	if (isset($_SESSION["attempt"]))
            sleep($_SESSION["attempt"]);

	$username                                                       = addslashes($_POST["txtUsername"]);
        $password                                                       = addslashes($_POST["txtPassword"]);
        $salt                                                           = q("SELECT salt FROM Employee WHERE email = '$username'");

	$password                                                       = generateHash($password, $salt);
	$login                                                          = q("SELECT id, email, usertype, locked FROM Employee WHERE email = '$username' ".
                                                                            "AND password = '$password'");

        if ($login[0][0])
        {
            if ($login[0][3] === "0")
            {
                //  Set Cookie Details
                setcookie("username", $username);

                //  Username & Password Match
                $_SESSION["logged_in"]                                  = true;
                $_SESSION["user_id"]                                    = $login[0][0];
                $_SESSION["email"]                                      = $login[0][1];
                $_SESSION["usertype"]                                   = $login[0][2];
    
                unset($_SESSION["attempt"]);
    
                $time                                                   = date("H:i:s");
    
                $logs                                                   = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time) ".
                                                                            "VALUES ('".$_SESSION["emial"]." logged in successfully', 'Select', 'Employee', ".
                                                                            "'".$_SESSION["emial"]."', '$today', '$time')");
    
                header("Location: home.php");
            }
            else
                $errorMessage                                           = "Sorry, your account has been locked";
	}
        else
        {
            if (isset($_SESSION["logged_in"]))
                $_SESSION["logged_in"]                                  = false;

            $errorMessage                                               = "Sorry, wrong username / password";

            if (!isset($_SESSION["attempt"]))
                $_SESSION["attempt"]                                    = 1;
            else
                $_SESSION["attempt"]                                    = $_SESSION["attempt"] * 2;
        }
    }

    if ($errorMessage != "")
        echo "<p align='center' style='padding:0px;'><strong><font color='#999999'>$errorMessage</font></strong></p>";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>
            On Track - Login Page
        </title>
        <link href="include/style.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <table align="center" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td valign="top">
                    <!--  Login Form  -->
                    <form action="" method="post" name="login">
                        <table align="center" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td align="center">
                                    <img src="header.jpg"/>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <a style="color:#000000;">
                                        Username:
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <input name="txtUsername" type="text" tabindex="1" value="<?php if (isset($_COOKIE["username"])) echo "".$_COOKIE["username"]; ?>">
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <a style="color:#000000;">
                                        Password:
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <input name="txtPassword" type="password" tabindex="2">
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <input name="btnLogin" tabindex="3" type="submit" value="Login">
                                </td>
                            </tr>
                        </table>
                    </form>
                </td>
            </tr>
        </table>
    </body>
</html>