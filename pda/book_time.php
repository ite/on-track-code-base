<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if ($_SESSION["logged_in"] === true && !($_SESSION["usertype"] > "0" && $_SESSION["usertype"] < "5"))
        header("Location: home.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    $activity_id                                                        = null;

    if (isset($_POST["activity"]))
        $activity_type                                                  = $_POST["activity_type"];

    //  Insert Booked Time Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")
    {
        $errorMessage                                                   = "";

        //  Get Information
        $project_id                                                     = $_POST["project"];
        $activity_id                                                    = $_POST["activity"];
        $description                                                    = addslashes(strip_tags($_POST["description"]));
        $date                                                           = addslashes(strip_tags($_POST["book_date"]));
        $time_spent                                                     = $_POST["time_spent"];

        $tariff_id                                                      = q("SELECT user_tariff FROM Project_User WHERE user_id = '".$_SESSION["user_id"]."' ".
                                                                            "AND project_id = '$project_id'");
        $rate                                                           = q("SELECT tariff".$tariff_id." FROM Employee WHERE id = '".$_SESSION["user_id"]."'");
        $time                                                           = date("H:i:s");

        if ($date <= $today)
        {
            $insert                                                     = q("INSERT INTO TimeSheet (date, user_id, project_id, activity_id, descr, rate, time_spent, ".
                                                                            "on_date, on_time) VALUES ('$date', '".$_SESSION["user_id"]."', '$project_id', ".
                                                                            "'$activity_id', '$description', '$rate', '$time_spent', '$today', '$time')");

            if ($insert)
            {
                $project_name                                           = q("SELECT name FROM Project WHERE id = '$project_id'");

                $logs                                                   = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('".$_SESSION["email"]." booked time on ".$project_name."', 'Insert', 'TimeSheet', ".
                                                                            "'".$_SESSION["email"]."', '$today', '$time', '".$_SESSION["company_id"]."')");

                header("Location: book_time.php");
            }
        }
        else
            $errorMessage                                               = "Time Cannot Be Booked In Advance";
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus();

    if ($errorMessage != "")
    {
        echo "<p align='center' style='padding:0px;'><strong><font color='#999999'>$errorMessage</font></strong></p>";
        echo "<br/>";
    }

    //  nProjects                                                       = Number of Contacts
    $nProjects                                                          = q("SELECT COUNT(p.id) FROM (Project AS p INNER JOIN Project_User AS pu ".
                                                                            "ON p.id = pu.project_id) WHERE pu.user_id = '".$_SESSION["user_id"]."' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."' AND p.completed = '0'");
    $projects                                                           = q("SELECT p.id, p.name FROM (Project AS p INNER JOIN Project_User AS pu ".
                                                                            "ON p.id = pu.project_id) WHERE pu.user_id = '".$_SESSION["user_id"]."' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."' AND p.completed = '0' ORDER BY p.name");

    //  nActivityTypes                                                  = Number of Activity Types
    $nActivityTypes                                                     = q("SELECT COUNT(id) FROM ActivityTypes WHERE company_id = '".$_SESSION["company_id"]."' ".
                                                                            "AND active = '1'");
    $activityTypes                                                      = q("SELECT id, type FROM ActivityTypes WHERE company_id = '".$_SESSION["company_id"]."' ".
                                                                            "AND active = '1' ORDER BY type");
?>
<script language="JavaScript">
    //  Internet Browser
    var type                                                            = "MO";

    if (window.opera)
        type                                                            = "OP";
    else if (document.all)
        type                                                            = "IE";	//IE4+
    else if (!document.all && document.getElementById)
        type                                                            = "MO";
    else
        type                                                            = "IE";

    function ShowLayer(id, action)
    {
        if (type == "IE")
            eval("document.all['" + id + "'].style.display='" + action + "'");
        else if(type == "MO" || type == "OP")
            eval("document.getElementById('" + id + "').style.display='" + action + "'");
    }

    function validation(type, value)
    {
        var currency                                                    = /^[-+]?[\d]+(\.\d{1,2})?$/;
        var date                                                        = /^[0-9]{4}-(((0[13578]|(10|12))-(0[1-9]|[1-2][0-9]|3[0-1]))|(02-(0[1-9]|[1-2][0-9]))|((0[469]|11)-(0[1-9]|[1-2][0-9]|30)))$/;
        var email                                                       = /^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/;
        var number                                                      = /^[\d]{1,9}$/;
        var percentage                                                  = /^((100(\.0{1,2})?)|([0-9]{0,2}(\.[0-9]{1,2})?))%?$/;
        var phone                                                       = /^[\d]{10,11}$/;

        if (type == "currency")
            return currency.test(value);
        else if (type == "date")
            return date.test(value);
        else if (type == "email")
            return email.test(value);
        else if (type == "number")
            return number.test(value);
        else if (type == "percentage")
            return percentage.test(value);
        else if (type == "phone")
            return phone.test(value);
    }

    function testShow(select)
    {
        if (select.value != "null")
            ShowLayer("getInfo", "block");
        else
            ShowLayer("getInfo", "none");
    }

    function check()
    {
        var valid                                                       = 1;

        //  Check That Activity Is Selected
        if (document.forms["book_time"].activity.value == "null")
        {
            ShowLayer("activityDiv", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("activityDiv", "none");

        //  Check That Description Is Not Longer Than 150 Characters
        if (document.forms["book_time"].description.value.length > 150)
        {
            ShowLayer("descriptionDiv", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("descriptionDiv", "none");

        //  Check That Date Is Entered
        if (document.forms["book_time"].book_date.value == "")
        {
            ShowLayer("bookDate", "block");
            valid                                                       = 0;
        }
        //  Check That Entered Date Is Valid
        else
        {
            ShowLayer("bookDate", "none");

            if (!validation("date", document.forms["book_time"].book_date.value))
            {
                ShowLayer("bookDateDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("bookDateDiv", "none");
        }

        //  Check That Time Spent Is Selected
        if (document.forms["book_time"].time_spent.value == "null")
        {
            ShowLayer("timeSpent", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("timeSpent", "none");

        if (valid == 1)
        {
            document.forms["book_time"].save.value                      = 1;
            document.forms["book_time"].submit();
        }
    }
</script>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="left" valign="top">
                <form action="" method="post" name="book_time">
                    <table cellpadding="0" cellspacing="2">
                        <tr>
                            <td align="left">
                                <strong>
                                    Project Name:&nbsp;
                                </strong>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <select method="post" name="project" onChange="testShow(project);" tabindex="1">
                                    <option value="null">--  Select A Project  --&nbsp;&nbsp;</option>
                                    <?php
                                        if ($nProjects > 1)
                                            foreach ($projects as $project)
                                                if ($_POST["project"] == $project[0])
                                                    echo "<option value='".$project[0]."' selected>".$project[1]."&nbsp;&nbsp;</option>";
                                                else
                                                    echo "<option value='".$project[0]."'>".$project[1]."&nbsp;&nbsp;</option>";
                                        else if ($nProjects == 1)
                                            if ($_POST["project"] == $projects[0][0])
                                                echo "<option value='".$projects[0][0]."' selected>".$projects[0][1]."&nbsp;&nbsp;</option>";
                                            else
                                                echo "<option value='".$projects[0][0]."'>".$projects[0][1]."&nbsp;&nbsp;</option>";
                                    ?>
                                </select>
                            </td>
                        </tr>
                    </table>
                    <div id="getInfo" style="<?php if ($activity_type == null || $activity_type == "null") echo "display: none;"; else echo "display: block;"; ?>">
                        <table cellpadding="0" cellspacing="2">
                            <tr>
                                <td align="left">
                                    <strong>
                                        Activity Type:&nbsp;
                                    </strong>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <select method="post" name="activity_type" onChange="submit();" tabindex="2">
                                        <option value="null">--  Select Activity Type  --&nbsp;&nbsp;</option>
                                        <?php
                                            if ($nActivityTypes > 1)
                                                foreach ($activityTypes as $activityType)
                                                    if ($_POST["activity_type"] == $activityType[0])
                                                        echo "<option value='".$activityType[0]."' selected>".$activityType[1]."&nbsp;&nbsp;</option>";
                                                    else
                                                        echo "<option value='".$activityType[0]."'>".$activityType[1]."&nbsp;&nbsp;</option>";
                                            else if ($nActivityTypes == 1)
                                                if ($_POST["activity_type"] == $activityTypes[0][0])
                                                    echo "<option value='".$activityTypes[0][0]."' selected>".$activityTypes[0][1]."&nbsp;&nbsp;</option>";
                                                else
                                                    echo "<option value='".$activityTypes[0][0]."'>".$activityTypes[0][1]."&nbsp;&nbsp;</option>";
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <strong>
                                        Activity:&nbsp;
                                    </strong>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <select method="post" name="activity" tabindex="3">
                                        <option value="null">--  Select Activity  --&nbsp;&nbsp;</option>
                                        <?php
                                            if ($activity_type != "null")
                                            {
                                                //  nActivities         = Number of Activities
                                                $nActivities            = q("SELECT COUNT(id) FROM Activities WHERE parent_id = '$activity_type'");
                                                $activities             = q("SELECT id, name FROM Activities WHERE parent_id = '$activity_type' ORDER BY name");

                                                if ($nActivities > 1)
                                                    foreach ($activities as $activity)
                                                        if ($_POST["activity"] == $activity[0])
                                                            echo "<option value='".$activity[0]."' selected>".$activity[1]."&nbsp;&nbsp;</option>";
                                                        else
                                                            echo "<option value='".$activity[0]."'>".$activity[1]."&nbsp;&nbsp;</option>";
                                                else if ($nActivities == 1)
                                                    if ($_POST["activity"] == $activities[0][0])
                                                        echo "<option value='".$activities[0][0]."' selected>".$activities[0][1]."&nbsp;&nbsp;</option>";
                                                    else
                                                        echo "<option value='".$activities[0][0]."'>".$activities[0][1]."&nbsp;&nbsp;</option>";
                                            }
                                        ?>
                                    </select>
                                    <div id="activityDiv" style="display: none;"><font color="#FF5900">* Activity must be selected</font></div>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <strong>
                                        Desciption:&nbsp;
                                    </strong>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <input name="description" type="text" value="<?php echo "".$_POST["description"]; ?>" tabindex="4">
                                    <div id="descriptionDiv" style="display: none;"><font color="#FF5900">* Description may not be longer that 150 characters</font></div>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <strong>
                                        Date:&nbsp;
                                    </strong>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <input name="book_date" tabindex="5" type="text" style="text-align:right;" value="<?php 
                                        if ($_POST["book_date"] != "") echo "".$_POST["book_date"]; else echo "".$today; ?>">
                                    <div id="bookDate" style="display: none;"><font color="#FF5900">* Date must be entered</font></div>
                                    <div id="bookDateDiv" style="display: none;"><font color="#FF5900">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <strong>
                                        Time Spent:&nbsp;
                                    </strong>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <select method="post" name="time_spent" tabindex="6">
                                        <option value="null">&nbsp;&nbsp;</option>
                                        <option value="0.25">0.25&nbsp;</option>
                                        <?php
                                            for ($i = 0.5; $i <= 12; $i += 0.5)
                                            {
                                                if (strpos("".$i, ".") == 0)
                                                    echo "<option value='".$i."'>".$i.".0&nbsp;</option>";
                                                else
                                                    echo "<option value='".$i."'>".$i."&nbsp;</option>";
                                            }
                                        ?>
                                    </select>
                                    <div id="timeSpent" style="display: none;"><font color="#FF5900">* Time spent must be selected</font></div>
                                </td>
                            </tr>
                        </table>
                        <input name="btnAdd" onClick="check();" tabindex="7" type="button" value="Book Time">
                    </div>
                    <br/>
                    <input method="post" name="save" type="hidden" value="0" />
                </form>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>