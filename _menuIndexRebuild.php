<?php
    include("_db.php");
    include("_dates.php");
    include("_functions.php");
?>
<html>
    <head>
        <title>
            On-Track - Recovery Script...
        </title>
    </head>
    <body>
        <h1 align="center">
            Install - Menu Index Rebuild
        </h1>
        <?php
            // Menu Index
            $drop = q("DROP TABLE IF EXISTS MenuIndex");
            $create = q("CREATE TABLE MenuIndex (
                    id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
                    company_id SMALLINT(5) UNSIGNED NOT NULL, INDEX(company_id),
                    user_id SMALLINT(5) UNSIGNED NOT NULL, INDEX(user_id),
                    menu_id MEDIUMINT(8) UNSIGNED NOT NULL, INDEX(menu_id)
            )");

            $start = my_microtime();$timer = 1;	//Init Timer
            $menus = getMenus_();
            $users = q("SELECT cu.company_id, cu.user_id, c.name, e.email FROM Company_Users AS cu INNER JOIN Company AS c ON c.id = cu.company_id ".
                                    "INNER JOIN Employee AS e ON e.id = cu.user_id ORDER BY c.name, e.email");
            $drop = q("DELETE FROM MenuIndex");
            foreach($users as $u)	{
                    $lookup = addMenuIndex($menus, $u[0], $u[1], $u[3]);
                    //echo number_format(round(my_microtime()-$start,3),3)."s - "."$u[2] - $u[3] - $lookup<br/>\n";	//Timer output
            }
            echo "<br/>\nDONE<br/>\nTotal time: ".number_format(round(my_microtime()-$start,3),3)."s<br/>\n";	//Timer output
        ?>
        <form action="index.php" method="post">
            <center><input type="submit" value="Return"/></center>
        </form>
    </body>
</html>
