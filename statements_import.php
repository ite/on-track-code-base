<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!$_SESSION["eula_agree"] === true)
        header("Location: logout.php");
        
	if (!hasAccess("STATEMENT_MAN"))
		header("Location: noaccess.php");

    //  Print Header
    print_header();
    //  Print Menu
	if ($_GET["menu"] == "")
		print_menus("0", "home");
	else
		print_menus($_GET["menu"], "home");

	include("include/OFXParser.php");
	define("NUM_FILES",1);
	define("MAX_SIZE",5);	//in MB
	$max_size = MAX_SIZE * 1024 * 1024;
	$allowedExtensions = array("csv"); //ofx removed

	//line_offset starts at 0
	//starting column index = 0
	//currently used for CSV only
	$conf = array(
		"ABSA" => array(
			"CHEQUE" => array(
				"line_offset"	=> 1,
				"date_col"		=> 0,
				"amount_col"	=> 2,
				"descr_col"		=> 1,
				"descr_col2"	=> -1
			),
			"SAVINGS" => array(
				"line_offset"	=> 1,
				"date_col"		=> 0,
				"amount_col"	=> 2,
				"descr_col"		=> 1,
				"descr_col2"	=> -1
			),
			"INVESTMENT" => array(
				"line_offset"	=> 1,
				"date_col"		=> 0,
				"amount_col"	=> 2,
				"descr_col"		=> 1,
				"descr_col2"	=> -1
			)
		),
		"FNB" => array(
			"CHEQUE" => array(
				"line_offset"	=> 7,
				"date_col"		=> 0,
				"amount_col"	=> 1,
				"descr_col"		=> 3,
				"descr_col2"	=> -1
			),
			"SAVINGS" => array(
				"line_offset"	=> 7,
				"date_col"		=> 0,
				"amount_col"	=> 1,
				"descr_col"		=> 3,
				"descr_col2"	=> -1
			),
			"INVESTMENT" => array(
				"line_offset"	=> 7,
				"date_col"		=> 0,
				"amount_col"	=> 1,
				"descr_col"		=> 3,
				"descr_col2"	=> -1
			)
		),
		"NETBANK" => array(
			"CHEQUE" => array(
				"line_offset"	=> 4,
				"date_col"		=> 0,
				"amount_col"	=> 2,
				"descr_col"		=> 1,
				"descr_col2"	=> -1
			),
			"SAVINGS" => array(
				"line_offset"	=> 4,
				"date_col"		=> 0,
				"amount_col"	=> 2,
				"descr_col"		=> 1,
				"descr_col2"	=> -1
			),
			"INVESTMENT" => array(
				"line_offset"	=> 4,
				"date_col"		=> 0,
				"amount_col"	=> 3,
				"descr_col"		=> 2,
				"descr_col2"	=> -1
			)
		),
		"STANDARD" => array(
			"CHEQUE" => array(
				"line_offset"	=> 3,
				"date_col"		=> 1,
				"amount_col"	=> 3,
				"descr_col"		=> 4,
				"descr_col2"	=> 5
			),
			"SAVINGS" => array(
				"line_offset"	=> 3,
				"date_col"		=> 1,
				"amount_col"	=> 3,
				"descr_col"		=> 4,
				"descr_col2"	=> 5
			),
			"INVESTMENT" => array(
				"line_offset"	=> 3,
				"date_col"		=> 1,
				"amount_col"	=> 3,
				"descr_col"		=> 4,
				"descr_col2"	=> 5
			)
		)
	);

	function isAllowedExtension($fileName,$list) {
		return in_array(end(explode(".", $fileName)), $list);
	}

	function p($s)	{if (is_array($s))	foreach($s as $r) foreach($r as $d)	echo ">>$d<br>\n";	else echo ">>$s<br>\n";}

	//Not in use anymore, ofx not standard enough between the banks.
	/*
	function fixDate($d)	{
		if (substr_count($d, '-') == 0)
			return substr($d,0,4)."-".substr($d,4,2)."-".substr($d,6,2);
		return $d;
	}

	function parseOFX(&$ofx,$path)	{
		$ofx->loadFromFile($path);
		$bank = $ofx->getBank();
		$acc = $ofx->getAccount();
		$trans = $ofx->getMoviments();
		foreach($trans as $tt)	{
			$exists = q("SELECT id,company_id FROM Statement WHERE company_id = '".$_SESSION['company_id']."' AND 
						account = '$acc' AND amount = '".$tt[TRNAMT]."' AND date = '".fixDate($tt[DTPOSTED])."' AND description = '".$tt[NAME]."'");
			if (!is_array($exists))	{
				$balance = q("SELECT balance FROM Statement WHERE account = '$acc' AND date <= '".fixDate($tt[DTPOSTED])."' ".
							"ORDER BY date DESC, id DESC LIMIT 1");
				$balance = number_format(($balance+(double)$tt[TRNAMT]), 2, ".", "");
				$insert = q("INSERT INTO Statement (company_id,bank,account,TRNTYPE,FITID,CHECKNUM,amount,balance,date,description) ".
					"VALUES ('".$_SESSION['company_id']."','$bank','$acc','".$tt[TRNTYPE]."','".$tt[FITID]."','".$tt[CHECKNUM]."','".
					$tt[TRNAMT]."','$balance','".fixDate($tt[DTPOSTED])."','".$tt[NAME]."')");
				$balance = q("UPDATE Statement SET balance = CAST(balance AS DECIMAL(12,2)) + (".$tt[TRNAMT].") WHERE account = '$acc' AND ".
						"date > '".fixDate($tt[DTPOSTED])."' ORDER BY date, id");//update all balances of entries after the current one for this account
			}
		}
	}
	*/

	function cmpTransactions($a,$b)  {
		if ($a[DATE] == $b[DATE]) {
			if ($a[AMOUNT] == $b[AMOUNT]) {
				if ($a[DESCR] == $b[DESCR])
					return 0;
				return ($a[DESCR] < $b[DESCR]) ? -1 : 1;
			}
			return ($a[AMOUNT] < $b[AMOUNT]) ? -1 : 1;
		}
		return ($a[DATE] < $b[DATE]) ? -1 : 1;
	}
	function parseCSV($csv,$account_id)	{
		global $conf;
		$data = q("SELECT d1.name, d2.name,a.account FROM Account AS a INNER JOIN dropdowns AS d1 ON a.bank_id = d1.id 
					INNER JOIN dropdowns AS d2 ON a.type_id = d2.id WHERE a.id = '$account_id'");
		$bank = $data[0][0];
		$accType = $data[0][1];
		$acc = $data[0][2];
		$c = 0;
		while (($data = fgetcsv($csv, 400, ",")) !== FALSE) {
//			echo "<br>".$data;
			if ($c++ >= $conf[$bank][$accType]["line_offset"])	{	//skip lines at the top which aren't statement entries
				$date = date("Y-m-d", strtotime($data[$conf[$bank][$accType]["date_col"]]));
				$amount = number_format((double)$data[$conf[$bank][$accType]["amount_col"]],2,".","");
				$descr = addslashes($data[$conf[$bank][$accType]["descr_col"]]);
				if ($conf[$bank][$accType]["descr_col2"] != -1)
					$descr .= " ".addslashes($data[$conf[$bank][$accType]["descr_col2"]]);
				if (!(strtoupper($descr) == "BROUGHT FORWARD" || strtoupper($descr) == "CARRIED FORWARD" 
					|| (double)$amount == 0.0 || $date == "1970-01-01"))
						$trans[] = array("DATE" => $date,"AMOUNT" => $amount,"DESCR" => $descr, "COUNT" => 1);
			}
		}
		//sort transaction array
		if (is_array($trans))
			usort($trans,"cmpTransactions");
		if (is_array($trans))
		if (count($trans) > 0)	{
			$list_to_drop = array();
			if (count($trans) > 1)	{
				for($i = 1; $i < count($trans); ++$i)	{
					if ($trans[$i-1][DATE] == $trans[$i][DATE])
						if ($trans[$i-1][AMOUNT] == $trans[$i][AMOUNT])
							if ($trans[$i-1][DESCR] == $trans[$i][DESCR])	{
								$trans[$i][COUNT] = $trans[$i-1][COUNT]+1;
								$list_to_drop[] = $i-1;
							}
				}
				for($i = 0; $i < count($trans); ++$i)
					if (!in_array($i,$list_to_drop)) 
						$newarr[] = $trans[$i];
				$trans = $newarr;
			}

	//		echo "==========================================================================<br>";
			foreach($trans as $t)	{
//				echo "Date: $t[DATE], Amount: $t[AMOUNT], Description: $t[DESCR]<br>";
				$exists = q("SELECT id,id FROM Statement WHERE company_id = '".$_SESSION['company_id']."' AND 
							account = '$acc' AND amount = '".$t[AMOUNT]."' AND date = '".$t[DATE]."' AND description = '".$t[DESCR]."'");

				if (!is_array($exists))	$exists = array();
				if (count($exists) < $t[COUNT])	{
					for($i = count($exists); $i < $t[COUNT]; ++$i)	{
//						echo "++".$i."<br>";
						$balance = q("SELECT balance FROM Statement WHERE account = '$acc' AND date <= '".$t[DATE]."' ".
							"ORDER BY date DESC, id DESC LIMIT 1");
						$balance = number_format(($balance+(double)$t[AMOUNT]), 2, ".", "");
						$insert = q("INSERT INTO Statement (company_id,bank,account,amount,balance,date,description,description_new) ".
							"VALUES ('".$_SESSION['company_id']."','$bank','$acc','".$t[AMOUNT]."','$balance','".$t[DATE]."','".$t[DESCR]."','".$t[DESCR]."')");
						$balance = q("UPDATE Statement SET balance = CAST(balance AS DECIMAL(12,2))+(".$t[AMOUNT].") WHERE account = '$acc' ".
							"AND date > '".$t[DATE]."' ORDER BY date, id");
					}
				}
			}
		}
	}

	echo '<a>';

	$c = 0;
	$accounts = $_POST["accounts"];
	$files = $_FILES['files'];
	if (isset($files))	{
//		$ofx = new OFXParser();
		foreach ($files["error"] as $key => $error) {
			if (isset($files["name"][$key]) && $files["name"][$key] != "")	{
				echo "Attemping Import of file: '".$files["name"][$key]."' ----> ";
				if ($error == UPLOAD_ERR_OK) {
					if(isAllowedExtension($files['name'][$key],$allowedExtensions)) {
						$tmp_name = $files["tmp_name"][$key];
						$name = $files["name"][$key];
						$r = rand(100000,999999);	//add some randomness for unique filename
						$path = "data/".$r.$files["name"][$key];
						if (move_uploaded_file($tmp_name, $path))	{//move the file from temp dir, don't leave a copy there
							if (($csv = fopen($path, "r")) !== FALSE) {
								parseCSV($csv,$accounts[$c++]);
								fclose($csv);
							}
							//parseOFX($ofx,$path);
							unlink($path);	//delete the file we saved
							echo "SUCCESS<br>";
						}	else	echo "FAILED - Could not move file from /tmp/ <br>";
					}	else	echo "FAILED - Extension not supported. Supported extensions include: ".implode(", ",$allowedExtensions)."<br>";
				}	else	echo "FAILED - Upload process failed<br>";
			}
		}
	}
?>
<script type="text/JavaScript">
	jQuery(function()    {
		jQuery(".addRow").click(function()   {
			var l = jQuery(this).parent().parent();
			l.clone(true).insertAfter(l);
		});
	});
</script>

<form enctype="multipart/form-data" action="" method="POST">
<table class='on-table'>
	<tr><td class='on-table-clear' colspan='2'><a>Bank Statement Imports<a></td></tr>
	<input type="hidden" name="MAX_FILE_SIZE" value="<?php echo $max_size;?>" />
	<tr><td colspan='3'>Choose files to be uploaded for import.<br>Note only CSV files are allowd.
			Files must be smaller than <font style='color:orange'><?php echo MAX_SIZE;?>MB</font></td></tr>
	<tr>
		<td colspan='2' class='centerdata'>
			<table id="data">
<?php	for($i = 0; $i < NUM_FILES; ++$i)	{
				$accounts = q("SELECT id, name FROM Account WHERE company_id ='".$_SESSION["company_id"]."' ");
?>
				<tr>
					<td>
						<select name='accounts[]' id='accounts[]'>
						<option value='null' selected> -- Select Account -- </option>
<?php
						if(is_array($accounts))
							foreach($accounts as $a)
								echo "<option value='$a[0]'>$a[1]</option>";
?>
						</select>
					</td>
					<td colspan='2' class='noborder' style='padding:3px;'>
						<input name="files[]" type="file" /><input type='button' class='addRow' value='+' />
					</td>
				</tr>
<?php	} ?>
			</table>
		
		</td></tr>
	<tr><td colspan='2' class='centerdata'><input type="submit" value="Import" /></td></tr>
</table>
</form>

<?php
	/*
	if ($handle = opendir('data/')) {
		while (false !== ($file = readdir($handle))) {
			if ($file != "." && $file != ".." && strpos($file,".ofx") != false) {
				parseOFX($ofx,"data/$file");
			}
		}
		closedir($handle);
	}
	 */
//	print_r($ofx->getMoviments());
//	print_r($ofx->getCredits());
//	print_r($ofx->getDebits());
//	print_r($ofx->getByDate(06, 20, 2011));
//	print_r($ofx->filter('MEMO', 'DOC', true, true)); // all moviments that have DOC in its description, NOT case sensitive
	echo '</a>';
?>
<?php
    //  Print Footer
    print_footer();
?>
