<?php
    include("_db.php");
?>
<html>
    <head>
        <title>
            On-Track - Update Script...
        </title>
    </head>
    <body>
        <h1 align="center">
            On-Track - Update Script...
        </h1>
        <?php
            $date = date("Y-m-d H:i:s");
            $user = "admin";

			$cID = q("SELECT id FROM Company WHERE name = 'integrITy Engineering'");
			$pID1 = q("SELECT id FROM Project WHERE company_id = '".$cID."' AND name = 'Air Quality Monitoring system'");
			$pID2 = q("SELECT id FROM Project WHERE company_id = '".$cID."' AND name = 'Flowsentric Dust Monitoring'");

			$update = q("UPDATE ApprovedExpense SET project_id = '".$pID1."' WHERE project_id = '".$pID2."'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE project_id = '".$pID2."'");
			$update = q("UPDATE auditlogexpense SET project_id = '".$pID1."' WHERE project_id = '".$pID2."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE project_id = '".$pID2."'");
			$update = q("UPDATE ExpenseSheet SET project_id = '".$pID1."' WHERE project_id = '".$pID2."'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE project_id = '".$pID2."'");

			$cID = q("SELECT id FROM Company WHERE name = 'AGES NW'");
			$uID1 = q("SELECT id FROM Employee WHERE email = 'dmuller@ages-group.com'");
			$uID2 = q("SELECT id FROM Employee WHERE email = 'jmpotgieter@ages-group.com'");
			$uID3 = q("SELECT id FROM Employee WHERE email = 'dstoltz@ages-group.com'");
			$uID4 = q("SELECT id FROM Employee WHERE email = 'ppretorius@ages-group.com'");
			$uID5 = q("SELECT id FROM Employee WHERE email = 'spretorius@ages-group.com'");

			//Tlokwe Dolomite: AD HOC - Social Awareness
			//Expenses
			$pID1 = q("SELECT id FROM Project WHERE company_id = '".$cID."' AND name = 'Tlokwe Dolomite: AD HOC - Social Awareness'");
			$pID2 = q("SELECT id FROM Project WHERE company_id = '".$cID."' AND name = 'Tlokwe Dolomite: 2.5 (A1.3) Regional Integration (Municipality Liaison)'");

			$esID = q("SELECT id FROM ExpenseSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-05-22' AND descr LIKE '%Social Awareness Workshop Catering%'");
			$aesID = q("SELECT id FROM ApprovedExpense WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-05-22' AND descr LIKE '%Social Awareness Workshop Catering%'");
			$update = q("UPDATE ExpenseSheet SET project_id = '".$pID1."' WHERE id = '".$esID."'");
			$update = q("UPDATE ApprovedExpense SET project_id = '".$pID1."' WHERE id = '".$aesID."'");
			$update = q("UPDATE auditlogexpense SET project_id = '".$pID1."' WHERE id_approved = '".$aesID."'");

			$esID = q("SELECT id FROM ExpenseSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-05-25' AND descr LIKE '%Engineering Workshop Catering%'");
			$aesID = q("SELECT id FROM ApprovedExpense WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-05-25' AND descr LIKE '%Engineering Workshop Catering%'");
			$update = q("UPDATE ExpenseSheet SET project_id = '".$pID1."' WHERE id = '".$esID."'");
			$update = q("UPDATE ApprovedExpense SET project_id = '".$pID1."' WHERE id = '".$aesID."'");
			$update = q("UPDATE auditlogexpense SET project_id = '".$pID1."' WHERE id_approved = '".$aesID."'");

			//Time
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-05-11' AND descr LIKE '%Research on Dolomite and Public Participation Policy.%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-05-15' AND descr LIKE '%Ad hoc legal committee at Willem Coetzee%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-05-15' AND descr LIKE '%Kenneth Kaunda Regional Dolomite meeting%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-05-21' AND descr LIKE '%Workshop admin%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-05-22' AND descr LIKE '%Social Awareness  Workshop%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-05-25' AND descr LIKE '%Engineering Workshop%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-05-29' AND descr LIKE '%Editing%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-04' AND descr LIKE '%Correspondence%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-04' AND descr LIKE '%Social Map for high risk area%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-06' AND descr LIKE '%Housing/planning notice%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-06' AND descr LIKE '%Finalise Social Map%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-06' AND descr LIKE '%Communication%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-07' AND descr LIKE '%Communication%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-07' AND descr LIKE '%Meeting with Tlokwe to finalise press release%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-08' AND descr LIKE '%Communication%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-08' AND descr LIKE '%Press conference preparation%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-11' AND descr LIKE '%Communication%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-12' AND descr LIKE '%Contracts%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-12' AND descr LIKE '%Drilling Liaison officer and supervisor%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-12' AND descr LIKE '%Meeting with X20 and Liaison officers%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-13' AND descr LIKE '%Communication%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-13' AND descr LIKE '%Site visit%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-13' AND descr LIKE '%Research and preparation for radio interview%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-13' AND descr LIKE '%Info session with principal and educators at Resolofetso school%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-14' AND descr LIKE '%Communication%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-14' AND descr LIKE '%Site visit%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-14' AND descr LIKE '%Radio Aggenang interview%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-15' AND descr LIKE '%Communication%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-15' AND descr LIKE '%Site visit%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-15' AND descr LIKE '%Follow up of complaints/concerns%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-18' AND descr LIKE '%CommunicationEditingProofreading%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-18' AND descr LIKE '%Site visit%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-18' AND descr LIKE '%Liaison with principal of Boitsoko school for drilling permission%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-18' AND descr LIKE '%Project Planning%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-18' AND descr LIKE '%Follow up on complaints/concerns%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-19' AND descr LIKE '%Communication%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-19' AND descr LIKE '%Site visit%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-19' AND descr LIKE '%Information session with Community members at Community hall%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-20' AND descr LIKE '%Flyer, ward Councillor, emails%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-21' AND descr LIKE '%Meeting with ward counsillors%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-26' AND descr LIKE '%Emails%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-27' AND descr LIKE '%Time Sheets%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-27' AND descr LIKE '%Answering of dolomite queries, emails, calls to ward counsillors%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-27' AND descr LIKE '%Meeting with Ward Counsillor%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-28' AND descr LIKE '%Visit to drilling site, liaison with MM for housing, training of Liaison officers%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-28' AND descr LIKE '%emails, queries, meeting arrangements%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-07-02' AND descr LIKE '%Meeting with Councillor%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");


			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-05-11' AND descr LIKE '%Research on Dolomite and Public Participation Policy.%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-05-15' AND descr LIKE '%Ad hoc legal committee at Willem Coetzee%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-05-15' AND descr LIKE '%Kenneth Kaunda Regional Dolomite meeting%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-05-21' AND descr LIKE '%Workshop admin%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-05-22' AND descr LIKE '%Social Awareness  Workshop%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-05-25' AND descr LIKE '%Engineering Workshop%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-05-29' AND descr LIKE '%Editing%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-06-04' AND descr LIKE '%Correspondence%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-06-04' AND descr LIKE '%Social Map for high risk area%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-06-06' AND descr LIKE '%Housing/planning notice%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-06-06' AND descr LIKE '%Finalise Social Map%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-06-06' AND descr LIKE '%Communication%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-06-07' AND descr LIKE '%Communication%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-06-07' AND descr LIKE '%Meeting with Tlokwe to finalise press release%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-06-08' AND descr LIKE '%Communication%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-06-08' AND descr LIKE '%Press conference preparation%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-06-11' AND descr LIKE '%Communication%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-06-12' AND descr LIKE '%Contracts%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-06-12' AND descr LIKE '%Drilling Liaison officer and supervisor%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-06-12' AND descr LIKE '%Meeting with X20 and Liaison officers%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-06-13' AND descr LIKE '%Communication%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-06-13' AND descr LIKE '%Site visit%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-06-13' AND descr LIKE '%Research and preparation for radio interview%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-06-13' AND descr LIKE '%Info session with principal and educators at Resolofetso school%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-06-14' AND descr LIKE '%Communication%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-06-14' AND descr LIKE '%Site visit%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-06-14' AND descr LIKE '%Radio Aggenang interview%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-06-15' AND descr LIKE '%Communication%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-06-15' AND descr LIKE '%Site visit%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-06-15' AND descr LIKE '%Follow up of complaints/concerns%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-06-18' AND descr LIKE '%CommunicationEditingProofreading%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-06-18' AND descr LIKE '%Site visit%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-06-18' AND descr LIKE '%Liaison with principal of Boitsoko school for drilling permission%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-06-18' AND descr LIKE '%Project Planning%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-06-18' AND descr LIKE '%Follow up on complaints/concerns%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-06-19' AND descr LIKE '%Communication%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-06-19' AND descr LIKE '%Site visit%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-06-19' AND descr LIKE '%Information session with Community members at Community hall%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-06-20' AND descr LIKE '%Flyer, ward Councillor, emails%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-06-21' AND descr LIKE '%Meeting with ward counsillors%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-06-26' AND descr LIKE '%Emails%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-06-27' AND descr LIKE '%Time Sheets%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-06-27' AND descr LIKE '%Answering of dolomite queries, emails, calls to ward counsillors%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-06-27' AND descr LIKE '%Meeting with Ward Counsillor%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-06-28' AND descr LIKE '%Visit to drilling site, liaison with MM for housing, training of Liaison officers%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-06-28' AND descr LIKE '%emails, queries, meeting arrangements%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-07-02' AND descr LIKE '%Meeting with Councillor%'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");

			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-07-20' AND descr LIKE '%On-Track management: time and expense sheets.%'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID2."' AND date = '2012-07-20' AND descr LIKE '%On-Track management: time and expense sheets.%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");

			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID3."' AND date = '2012-04-25' AND descr LIKE '%Liaison with TCC, Stephan Potgieter Presentation%'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID3."' AND date = '2012-04-25' AND descr LIKE '%Liaison with TCC, Stephan Potgieter Presentation%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");

			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID3."' AND date = '2012-05-15' AND descr LIKE '%%'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID3."' AND date = '2012-05-15' AND descr LIKE '%%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");

			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID3."' AND date = '2012-06-25' AND descr LIKE '%Research of neighbouring municipalities and map interpretation%'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID3."' AND date = '2012-06-25' AND descr LIKE '%Research of neighbouring municipalities and map interpretation%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");

			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID3."' AND date = '2012-06-26' AND descr LIKE '%Compiling contact information for neighbouring municipalities%'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID3."' AND date = '2012-06-26' AND descr LIKE '%Compiling contact information for neighbouring municipalities%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");

			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID3."' AND date = '2012-07-04' AND descr LIKE '%Phoning Municipalities in North West affected by dolomite to get town Planner info%'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID3."' AND date = '2012-07-04' AND descr LIKE '%Phoning Municipalities in North West affected by dolomite to get town Planner info%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");

			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID3."' AND date = '2012-07-05' AND descr LIKE '%Contacting neighbouring municipalities to get town planner contacts %'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID3."' AND date = '2012-07-05' AND descr LIKE '%Contacting neighbouring municipalities to get town planner contacts %'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");




			//Tlokwe Dolomite: Social Awareness Strategy Planning
			$pID1 = q("SELECT id FROM Project WHERE company_id = '".$cID."' AND name = 'Tlokwe Dolomite: Social Awareness Strategy Planning'");
			$pID2 = q("SELECT id FROM Project WHERE company_id = '".$cID."' AND name = 'Tlokwe Dolomite: 2.5 (A1.3) Regional Integration (Municipality Liaison)'");
			$pID3 = q("SELECT id FROM Project WHERE company_id = '".$cID."' AND name = 'Tlokwe Dolomite: AD HOC - Social Awareness'");

			//dmuller@ages-group.com -> Tlokwe Dolomite: 2.5 (A1.3) Regional Integration (Municipality Liaison)
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-14' AND descr LIKE '%Research%'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-14' AND descr LIKE '%Research%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");

			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-15' AND descr LIKE '%Research%'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-15' AND descr LIKE '%Research%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");

			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-17' AND descr LIKE '%Research%'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-17' AND descr LIKE '%Research%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");

			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-20' AND descr LIKE '%Social Awareness Strategy%'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-20' AND descr LIKE '%Social Awareness Strategy%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");

			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-21' AND descr LIKE '%Social Awareness Strategy%'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-21' AND descr LIKE '%Social Awareness Strategy%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");

			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-22' AND descr LIKE '%Social Awareness Strategy%'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-22' AND descr LIKE '%Social Awareness Strategy%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");

			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-23' AND descr LIKE '%Social Awareness Strategy%'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-23' AND descr LIKE '%Social Awareness Strategy%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");

			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-25' AND descr LIKE '%Social awareness Strategy%'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID2."' AND user_id = '".$uID1."' AND date = '2012-06-25' AND descr LIKE '%Social awareness Strategy%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");

			//dmuller@ages-group.com -> Tlokwe Dolomite: AD HOC - Social Awareness
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID3."' AND user_id = '".$uID1."' AND date = '2012-07-03' AND descr LIKE '%Stakeholder analysis%'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID3."' AND user_id = '".$uID1."' AND date = '2012-07-03' AND descr LIKE '%Stakeholder analysis%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");

			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID3."' AND user_id = '".$uID1."' AND date = '2012-07-08' AND descr LIKE '%Social Awareness Strategy%'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID3."' AND user_id = '".$uID1."' AND date = '2012-07-08' AND descr LIKE '%Social Awareness Strategy%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");

			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID3."' AND user_id = '".$uID1."' AND date = '2012-07-09' AND descr LIKE '%Template and technical editing%'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID3."' AND user_id = '".$uID1."' AND date = '2012-07-09' AND descr LIKE '%Template and technical editing%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");

			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID3."' AND user_id = '".$uID1."' AND date = '2012-07-10' AND descr LIKE '%Phase 1 report, costing%'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID3."' AND user_id = '".$uID1."' AND date = '2012-07-10' AND descr LIKE '%Phase 1 report, costing%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");

			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID3."' AND user_id = '".$uID1."' AND date = '2012-07-10' AND descr LIKE '%Stakeholder analysis review with Dawid, project proposal%'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID3."' AND user_id = '".$uID1."' AND date = '2012-07-10' AND descr LIKE '%Stakeholder analysis review with Dawid, project proposal%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");

			//ppretorius@ages-group.com -> Tlokwe Dolomite: AD HOC - Social Awareness
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID3."' AND user_id = '".$uID4."' AND date = '2012-03-13' AND descr LIKE '%Dolomite awareness proposal%'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID3."' AND user_id = '".$uID4."' AND date = '2012-03-13' AND descr LIKE '%Dolomite awareness proposal%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");

			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID3."' AND user_id = '".$uID4."' AND date = '2012-03-14' AND descr LIKE '%Dolomite awareness proposal%'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID3."' AND user_id = '".$uID4."' AND date = '2012-03-14' AND descr LIKE '%Dolomite awareness proposal%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");

			//spretorius@ages-group.com -> Tlokwe Dolomite: AD HOC - Social Awareness
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID3."' AND user_id = '".$uID5."' AND date = '2012-02-07' AND descr LIKE '%Strategic planning of social awareness.%'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID3."' AND user_id = '".$uID5."' AND date = '2012-02-07' AND descr LIKE '%Strategic planning of social awareness.%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");

			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID3."' AND user_id = '".$uID5."' AND date = '2012-03-06' AND descr LIKE '%Social awareness program development.%'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID3."' AND user_id = '".$uID5."' AND date = '2012-03-06' AND descr LIKE '%Social awareness program development.%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");

			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID3."' AND user_id = '".$uID5."' AND date = '2012-04-25' AND descr LIKE '%Social awareness program development.%'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID3."' AND user_id = '".$uID5."' AND date = '2012-04-25' AND descr LIKE '%Social awareness program development.%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");

			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID3."' AND user_id = '".$uID5."' AND date = '2012-05-02' AND descr LIKE '%Social awareness planning input.%'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID3."' AND user_id = '".$uID5."' AND date = '2012-05-02' AND descr LIKE '%Social awareness planning input.%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");

			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID3."' AND user_id = '".$uID5."' AND date = '2012-05-07' AND descr LIKE '%Team establishment and workshop arrangements.%'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID3."' AND user_id = '".$uID5."' AND date = '2012-05-07' AND descr LIKE '%Team establishment and workshop arrangements.%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");

			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID3."' AND user_id = '".$uID5."' AND date = '2012-05-10' AND descr LIKE '%Tasks on social awareness project development.%'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID3."' AND user_id = '".$uID5."' AND date = '2012-05-10' AND descr LIKE '%Tasks on social awareness project development.%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");

			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID3."' AND user_id = '".$uID5."' AND date = '2012-05-31' AND descr LIKE '%Development of a social awareness program.%'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID3."' AND user_id = '".$uID5."' AND date = '2012-05-31' AND descr LIKE '%Development of a social awareness program.%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");

			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID3."' AND user_id = '".$uID5."' AND date = '2012-06-04' AND descr LIKE '%Project development - program and action definition - strategy report framework development. (3 - 4 June 2012).%'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID3."' AND user_id = '".$uID5."' AND date = '2012-06-04' AND descr LIKE '%Project development - program and action definition - strategy report framework development. (3 - 4 June 2012).%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");

			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID3."' AND user_id = '".$uID5."' AND date = '2012-06-05' AND descr LIKE '%Project development - program and action definition - strategy report framework development. (3 - 4 June 2012).%'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID3."' AND user_id = '".$uID5."' AND date = '2012-06-05' AND descr LIKE '%Project development - program and action definition - strategy report framework development. (3 - 4 June 2012).%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");

			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID3."' AND user_id = '".$uID5."' AND date = '2012-06-06' AND descr LIKE '%Flyer, social work documents development, press release development and social awareness program development.%'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID3."' AND user_id = '".$uID5."' AND date = '2012-06-06' AND descr LIKE '%Flyer, social work documents development, press release development and social awareness program development.%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");

			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID3."' AND user_id = '".$uID5."' AND date = '2012-06-10' AND descr LIKE '%Final social awareness document development.%'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID3."' AND user_id = '".$uID5."' AND date = '2012-06-10' AND descr LIKE '%Final social awareness document development.%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");

			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID3."' AND user_id = '".$uID5."' AND date = '2012-06-16' AND descr LIKE '%Social awareness strategy development.%'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID3."' AND user_id = '".$uID5."' AND date = '2012-06-16' AND descr LIKE '%Social awareness strategy development.%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");

			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID3."' AND user_id = '".$uID5."' AND date = '2012-06-20' AND descr LIKE '%Social awareness strategy development.%'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID3."' AND user_id = '".$uID5."' AND date = '2012-06-20' AND descr LIKE '%Social awareness strategy development.%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");

			//dstoltz@ages-group.com -> Tlokwe Dolomite: AD HOC - Social Awareness
			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID3."' AND user_id = '".$uID3."' AND date = '2012-05-25' AND descr LIKE '%Writing Social awareness legal framework%'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID3."' AND user_id = '".$uID3."' AND date = '2012-05-25' AND descr LIKE '%Writing Social awareness legal framework%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");

			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID3."' AND user_id = '".$uID3."' AND date = '2012-06-28' AND descr LIKE '%Liaison with D Muller on the future Social Strategy%'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID3."' AND user_id = '".$uID3."' AND date = '2012-06-28' AND descr LIKE '%Liaison with D Muller on the future Social Strategy%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");

			$tsID = q("SELECT id FROM TimeSheet WHERE project_id = '".$pID3."' AND user_id = '".$uID3."' AND date = '2012-07-10' AND descr LIKE '%Adding to stakeholder analysis and liaison with Desiree on filing%'");
			$atsID = q("SELECT id FROM ApprovedTime WHERE project_id = '".$pID3."' AND user_id = '".$uID3."' AND date = '2012-07-10' AND descr LIKE '%Adding to stakeholder analysis and liaison with Desiree on filing%'");
			$update = q("UPDATE TimeSheet SET project_id = '".$pID1."' WHERE id = '".$tsID."'");
			$update = q("UPDATE ApprovedTime SET project_id = '".$pID1."' WHERE id = '".$atsID."'");
			$update = q("UPDATE auditlogtime SET project_id = '".$pID1."' WHERE id_approved = '".$atsID."'");



            echo "<p align='center'>Update script completed successfully...</p>";
        ?>
        <form action="index.php" method="post">
            <center><input type="submit" value="Return"/></center>
        </form>
    </body>
</html>
