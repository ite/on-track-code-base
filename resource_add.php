<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!hasAccess("RESOURCE_MAN"))
        header("Location: noaccess.php");

    //  Insert Resource Function
    if (isset($_POST["save"]) && $_POST["save"] === "1") {
        $errorMessage                                                   = "";

        //  Get Information
        $resource                                                       = addslashes(strip_tags($_POST["resource"]));

        //  Check If Resource Exists In Database
        $exist                                                          = q("SELECT id,resource FROM Resources WHERE resource = '$resource'");

        if (!is_array($exist)) {
            $insert                                                     = q("INSERT INTO Resources (resource) VALUES ('$resource')");

            if ($insert) {
                $resource_id                                            = q("SELECT id FROM Resources WHERE resource = '$resource'");

                $insert                                                 = q("INSERT INTO ResourceAssign (resource_id, company_id) VALUES ('$resource_id', '".$_SESSION["company_id"]."')");

                $time                                                   = date("H:i:s");

                $logs                                                   = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time) ".
                                                                            "VALUES ('$resource created', 'Insert', 'Resources', '".$_SESSION["email"]."', ".
                                                                            "'$today', '$time')");

                header("Location: resources.php");
            }
        }
        else    {
            $errorMessage = "Resource Already Exists";
        }
    }

    if ($errorMessage != "")
        echo "<p align='center' style='padding:0px;'><strong><font class='on-validate-error'>$errorMessage</font></strong></p>";

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("1", "resources");
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    function check()
    {
        var valid                                                       = 1;

        //  Check That Project Type Is Entered
        if (document.forms["resource_add"].resource.value == "")
        {
            ShowLayer("resourceName", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("resourceName", "none");

        if (valid == 1)
        {
            document.forms["resource_add"].save.value                   = 1;
            document.forms["resource_add"].submit();
        }
    }
</script>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="resource_add">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>
                                    Add New Resource
                                </h6>
                            </td>
                        </tr>
                        <tr>
                            <td class="centerdata">
                                <br/>
                            </td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                    Resource:
                            </td>
                            <td width="50%">
                                <input class="on-field" name="resource" tabindex="1" type="text">
                                <div id="resourceName" style="display: none;"><font class="on-validate-error">* Type must be entered</font></div>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <input name="btnAdd" onClick="check();" tabindex="2" type="button" value="Add Resource">
                    <input method="post" name="save" type="hidden" value="0" />
                </form>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
