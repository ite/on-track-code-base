<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("include/sajax.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");
        
    if (!hasAccess("REP_SP_SUMMARY_EX"))
        header("Location: noaccess.php");
        
    // Class for table background color
    $colorClass = "OnTrack_#_";            

     // Sajax
    function getAreas($id){
        return q("SELECT ar.id, ar.name FROM (areas AS ar INNER JOIN Project as p ON ar.pID = p.id) WHERE ar.active = '1' AND p.p_type='SP' AND ar.pID='".$id."' ORDER BY ar.name");
    } 

    $sajax_request_type                                             = "GET";
    sajax_init();
    sajax_export("getAreas");
    sajax_handle_client_request();
    ///////////////////////////

    //  Set Report Generated Status
    $generated                                                          = "0";

    //  Create Display Information Function
    if (isset($_POST["save"]) && $_POST["save"] === "1") {
        
        $errorMessage = "";

        $reportOn = $_POST["reportOn"];

        //$project_type_id = $_POST["projectType"];
        $project_id = $_POST["project"];
        $area_id = $_POST["area"];
        //$report_type = $_POST["reportType"];
        $date_from = addslashes(strip_tags($_POST["date_from"]));
        $date_to = addslashes(strip_tags($_POST["date_to"]));
        $displayString = "";
        
        $where = "";
        $areaHeader = "";
        $areaWhere = "";
        $areaExcelHead = "";
        $hasArea = 0;
        $excelTH = array();
        
        // Excel data
        $myData = array();
        $myDataRow = 0;
        
        $columnCounter  = 0;
        $row = 0;

        if ($project_type_id != "null" && is_numeric($project_type_id))
            $where = "AND p.type_id = '$project_type_id' ";
            
        if($area_id == "all"){
            $areaHeader = "<td class='on-table-clear' colspan='100%'>Area: All Areas</td>";
            $areaExcelHead = "Area: All Areas";
            $hasArea = 1;
        }else if($area_id != "null"){
            $area_name = q("SELECT name FROM areas WHERE id = '".$area_id."' ");
            $areaHeader = "<td class='on-table-clear' colspan='100%'>Area: ".$area_name."</td>";
            $areaWhere = " AND es.area_id = '".$area_id."' ";
            $areaExcelHead = "Area: ".$area_name;
            $hasArea = 1;
        }

        ///////////////////////////
        //  Get Info From Tables
        ///////////////////////////
        
        $headers = "<tr><th style='vertical-align:bottom'>Company Name</th>";
        $excelTH[0] = "Company Name";
        
        $companies = q("SELECT c.id, c.name FROM (Company AS c 
                                    INNER JOIN companiesOnTeam AS cot ON cot.company_id = c.id) 
                                    WHERE cot.project_id = $project_id");   // 3644 - ITE & ITE Voiced - Shared Project 11
                                    
        //$areaCheck = q("SELECT ar.name FROM (Areas AS ar INNER JOIN companiesOnTeam AS cot ON cot.project_id = ar.pID) WHERE cot.company_id = '".$_SESSION["company_id"]."' AND cot.project_id = '$project_id' ");
        //print_r($areaCheck);

        $table = ($reportOn == "approved") ? "ApprovedExpense" : "ExpenseSheet";
        $where = ($reportOn == "approved") ? "AND es.status2 = '2' " : "";

        if (is_array($companies)){
            foreach ($companies as $company){
                $a = q("SELECT DISTINCT(at.id), at.type FROM (ActivityTypes AS at 
                                        INNER JOIN Activities AS a ON a.parent_id = at.id
                                        INNER JOIN $table AS es ON es.activity_id = a.id) 
                                        WHERE es.project_id = '".$project_id."'
                                        AND es.date >= '".$date_from."' 
                                        AND es.date <= '".$date_to."' $where");
                if(is_array($a)){
                    if(is_array($actTypes))
                        $actTypes = array_merge($actTypes,$a);
                    else{
                        $actTypes = $a;
                    }
                }
            }
            
            $totalPerCompany = 0;
           
            if(is_array($actTypes)){
                asort($actTypes);                                                // Sort Array
                $actTypes = filterQueryData($actTypes,2,1);     // Remove duplicates - 2 Cols, Order by Col 1
                
                if (count($actTypes) > 1) {
                    foreach($actTypes as $actType){
                        
                        $name = "";
                        $name_length = strlen($actType[1]);
                        $excelTH[] = $actType[1];
                        for ($i = 0; $i < $name_length; $i++){
                            $name .= "<br/>".substr($actType[1], $i, 1)."";
                        }
                        $headers .= "<th class='on-table-light' style='vertical-align:text-top;'>".$name."</th>";
                        $columnCounter ++;  // each ActType
                    }
                }else{
                    foreach($actTypes as $actType){
                        $headers .= "<th class='on-table-light'>".$actType[1]."</th>";
                        $excelTH[] = $actType[1];
                        $columnCounter ++;  // each ActType
                    }
                }
                
                $totalPerActivity  = array();
                $actColomnCount = 0;
                
                foreach ($companies as $company){
                   $displayString .= "<tr><td>$company[1]</td>";
                   $myData[$myDataRow][0] = $company[1];
                   
                   $totalPerCompany = 0;
                   $curCol = 0;
                    foreach($actTypes as $actType){
                            $time = q("SELECT SUM(es.expense) FROM ($table AS es 
                                                INNER JOIN Activities AS a ON a.id = es.activity_id)
                                                WHERE a.parent_id = '".$actType[0]."' 
                                                AND es.company_id = '".$company[0]."'
                                                AND es.project_id = '".$project_id."'
                                                AND es.date >= '".$date_from."' 
                                                AND es.date <= '".$date_to."' ".$areaWhere." $where");

                            $totalPerCompany += $time;
                            $displayString .= "<td class='rightdata'>".number_format($time, 2, ".", "")."</td>";
                            $myData[$myDataRow][] = $time;
                            
                            $totalPerActivity[$curCol][0] += $time;
                            $curCol++;
                           $actColomnCount = $curCol;
                        
                    }
                    $displayString .= "<td class='on-table-total'>".number_format($totalPerCompany, 2, ".", "")."</td></tr>";
                    $myData[$myDataRow][] = $totalPerCompany;
                    $myDataRow++;
                }
                $hasValues = 1;
            }
            $columnCounter ++;  // company(1)
        }

        if ($report_type == "time"){
            $headers .= "<th style='vertical-align:bottom'>Total</th></tr>";
            $excelTH[] = "Total";
            $columnCounter ++;  // total(2)
        }else{
            $headers .= "<th style='vertical-align:bottom'>Total <i>(".$_SESSION["currency"].")</i></th></tr>";
            $excelTH[] = "Total (".$_SESSION["currency"].")";
            $columnCounter ++;  // total(2)
        } 
        
        if($hasValues){
            $grandTotal = 0;
            $displayString .= "<tr><td class='on-table-total'>Total:</td>";
            $myData[$myDataRow][] = "Total: ";
            
            for($i=0; $i<$actColomnCount;$i++){
                    $displayString .= "<td class='on-table-total'>".number_format($totalPerActivity[$i][0], 2, ".", "")."</td>";
                    $myData[$myDataRow][] = $totalPerActivity[$i][0];
                    $grandTotal  += $totalPerActivity[$i][0];
            }
            $displayString .= "<td class='on-table-total'><a>".number_format($grandTotal, 2, ".", "")."</a></td></tr>";
             $myData[$myDataRow][] = $grandTotal;
             $myDataRow++;
        }else{
            $displayString .= "<tr><td class='on-table-total'>No data to be displayed</td><td class='on-table-total'></td>";
        }

        for($i=0; $i<$columnCounter ; $i++){          // Print Blank Headers
            $excelheadings[0][] = "";
        }
            
        $project_name = q("SELECT name From Project WHERE id = '$project_id'");
        
        if($displayString != ""){
            $displayString                                                  = "<tr>
                                                                                        <td class='on-table-clear' colspan='100%'><a>Shared Project Summary Report (Expenses): ".$date_from." - ".$date_to."</a></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class='on-table-clear' colspan='100%'>Shared Project: ".$project_name."</td>
                                                                                        </tr>".
                                                                                        $areaHeader.$headers.$displayString;
                                                                                        
            /////////// -- EXCEL DATA EXPORT -- /////////// 
            $exceldata[$row][] = "Shared Project Summary Report (Expenses): ".$date_from." - ".$date_to." " ;
             for($i=0; $i < $columnCounter-1; $i++){
                $exceldata[$row][] = "";
            }
            $row++;
                
            $exceldata[$row][] = "Shared Project: ".$project_name;       
            for($i=0; $i < $columnCounter-1; $i++){
                $exceldata[$row][] = "";
            }
            $row++;
            
            if($hasArea){
                $exceldata[$row][] = $areaExcelHead;        
                for($i=0; $i < $columnCounter-1; $i++){
                    $exceldata[$row][] = "";
                }
                $row++;
            }
            
            for($i=0; $i < $columnCounter; $i++){
                    $exceldata[$row][] = $colorClass.$excelTH[$i];
                }
            
        }

        if ($displayString != "")
            $generated                                                  = "1";
    }
    
    
    //  Print Header
    print_header();
    //  Print Menu
    print_menus("3", "reports");
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    jQuery(function(){
	jQuery('#date_from').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_from').val(),
		current: jQuery('#date_from').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_from').val()))
			_date = new Date();
			else _date = jQuery('#date_from').val();
			jQuery('#date_from').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_from').val(formated);
			jQuery('#date_from').DatePickerHide();
		}
	});
    })

    jQuery(function(){
	jQuery('#date_to').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_to').val(),
		current: jQuery('#date_to').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_to').val()))
			_date = new Date();
			else _date = jQuery('#date_to').val();
			jQuery('#date_to').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_to').val(formated);
			jQuery('#date_to').DatePickerHide();
		}
	});
    })
    
    //  Sajax
    <?php sajax_show_javascript(); ?>

    //  Sajax
    function ClearOptions(OptionList) {
        for (x = OptionList.length; x >= 0; x--)
        {
            OptionList[x]                                               = null;
        }
    }
    
    //Set Areas
    function setAreas(data) {
        var c                                                           = 0;
        if(!data)
            ShowLayer("areaDiv", "none");

        document.forms["report_summary"].area.options[(c++)]                         = new Option("All Areas", "all");

        for (var i in data)
            eval("document.forms['report_summary'].area.options["+(c++)+"] = new Option('" + data[i][1] + "', '" + data[i][0] + "');");
    }
    
    // Get Areas
    function getAreas(select) {
        ClearOptions(document.report_summary.area);
            ShowLayer("areaDiv", "block");
            x_getAreas(select.value, setAreas);
    }

    function check()
    {
        var valid                                                       = 1;

        //  Check That Employee Is Selected
        if (document.forms["report_summary"].project.value == "null")
        {
            ShowLayer("projectDiv", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("projectDiv", "none");

        //  Check That Date From Is Entered
        if (document.forms["report_summary"].date_from.value == "")
        {
            ShowLayer("dateFrom", "block");
            valid                                                       = 0;
        }
        //  Check That Entered Date From Is Valid
        else
        {
            ShowLayer("dateFrom", "none");

            if (!validation("date", document.forms["report_summary"].date_from.value))
            {
                ShowLayer("dateFromDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("dateFromDiv", "none");
        }

        //  Check That Date To Is Entered
        if (document.forms["report_summary"].date_to.value == "")
        {
            ShowLayer("dateTo", "block");
            valid                                                       = 0;
        }
        //  Check That Entered Date To Is Valid
        else
        {
            ShowLayer("dateTo", "none");

            if (!validation("date", document.forms["report_summary"].date_to.value))
            {
                ShowLayer("dateToDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("dateToDiv", "none");
        }

        if (valid == 1)
        {
            document.forms["report_summary"].save.value                 = 1;
            document.forms["report_summary"].submit();
        }
    }

</script>
<?php
    $projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p 
                            INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) 
                            WHERE cot.company_id = '".$_SESSION["company_id"]."' 
                            AND p.completed = '0' 
                            AND p.deleted = '0' 
                                AND p.p_type='SP' 
                            ORDER BY UPPER(p.name)");
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="report_summary">
                    <div id="" style="<?php if ($generated == "1") echo "display: none;"; else echo "display: block"; ?>">
                        <table width="100%">
                            <tr>
                                <td class="centerdata">
                                    <h6>Shared Project Summary Report (Expense Based)</h6>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Report On:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="reportOn">
                                        <option value="normal">Actual Time</option>
                                        <option value="approved">Approved Time</option>
                                    </select>
                                </td>
                            </tr>
                            <tr><td colspan="100%"><br/></td></tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Shared Project:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" id="project" name="project" onChange="getAreas(project)">
                                        <option value="null">--  Select A Shared Project  --</option>
                                        <?php
                                            if (is_array($projects))
                                                foreach ($projects as $project)
                                                    echo "<option value='".$project[0]."'>".$project[1]."</option>";
                                        ?>
                                    </select>
                                    <div id="projectDiv" style="display: none;"><font class="on-validate-error">* Shared Project must be selected</font></div>
                                </td>
                            </tr>
                        </table>
                        
                        <div id="areaDiv" style="display: none;">
                            <table width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Select Area:
                                    </td>
                                    <td width="50%">
                                        <select class="on-field" method="post" name="area">
                                            <?php
                                                if (is_array($areas))
                                                    foreach ($areas as $area)
                                                        if ($_POST["area"] == $area[0])
                                                            echo "<option value='".$area[0]."' selected>".$area[1]."</option>";
                                                        else
                                                            echo "<option value='".$area[0]."'>".$area[1]."</option>";
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Date From:
                                </td>
                                <td width="50%">
                                    <input class="on-field-date" id="date_from" name="date_from" type="text" style="text-align:right;" value="<?php
                                        if ($_POST["date_from"] != "") echo "".$_POST["date_from"]; else echo "".getMonthStart($today); ?>">
                                    <div id="dateFrom" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                    <div id="dateFromDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Date To:
                                </td>
                                <td width="50%">
                                    <input class="on-field-date" id="date_to" name="date_to" type="text" style="text-align:right;" value="<?php
                                        if ($_POST["date_to"] != "") echo "".$_POST["date_to"]; else echo "".getMonthEnd($today); ?>">
                                    <div id="dateTo" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                    <div id="dateToDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                </td>
                            </tr>
                        </table>
                        
                        <br/>
                        <input name="btnGenerate" onClick="check();" type="button" value="Generate Report">
                        <input method="post" name="save" type="hidden" value="0" />
                    </div>
                </form>
                <div id="report" style="<?php if ($generated == "1") echo "display: block;"; else echo "display: none"; ?>">
                    <?php
                        echo "<div class='on-20px'><table class='on-table-center on-table'>";
                            echo "".$displayString;
                             echo  "<tfoot>
                                            <tr>
                                                <td colspan='100%'></td>
                                            </tr>
                                        </tfoot>";
                        echo "</table></div>";
                        echo "<br/>";

                        ///////////////////////////
                        $exceldata = array_merge($exceldata, $myData);
                        //  Set Export Information
                        $_SESSION["fileName"] = "Report SP Summary (Time)";
                        $_SESSION["fileData"] = array_merge($excelheadings, $exceldata);
                        ///////////////////////////
                        
                        echo "<input name='btnExport' onClick=\"location.href='_export.php'\" type='button' value='Export to Excel' />";
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
