<?php
	session_start();

	include("_db.php");
	include("graphics.php");

	if (!$_SESSION["logged_in"] === true)
		header("Location: login.php");

	if (!isset($_SESSION["company_id"]))
		header("Location: home.php");

	if (!hasAccess("REP_OVER_BUD_PROJ_CONS"))
		header("Location: noaccess.php");

	// Class for table background color
	$colorClass = "OnTrack_#_";

	//  Print Header
	print_header();
	//  Print Menu
	print_menus("3", "reports");

	$reportOn = "normal";

	if (isset($_POST["reportOn"]))	$reportOn = addslashes(strip_tags($_POST["reportOn"]));

	$projectTypes = q("SELECT id, type FROM ProjectTypes WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY type");
	$projectTypes[] = array("0","Shared Projects");
	$projectTypes = sortArrayByColumn($projectTypes, 1);
?>
<table width="100%">
	<tr height="380px">
		<td class="centerdata" valign="top">
			<form action="" method="post" name="report_active_projects">
				<table width="100%">
					<tr>
						<td class="centerdata">
							<h6>Over Budget Projects</h6>
						</td>
					</tr>
				</table>
				<br/>
				<table width="100%">
					<tr>
						<td class="on-description" width="50%">
							Report On:
						</td>
						<td width="50%">
							<select class="on-field" method="post" name="reportOn">
								<option value="normal" <?php echo ($reportOn == "normal") ? "selected" : ""; ?>>Actual Time</option>
								<option value="approved" <?php echo ($reportOn == "approved") ? "selected" : ""; ?>>Approved Time</option>
							</select>
						</td>
					</tr>
					<tr>
						<td class="on-description" width="50%">
							Select Project Type:
						</td>
						<td width="50%">
							<select class="on-field" method="post" name="projectType">
								<option value="null">All Project Types</option>
								<?php
									if (is_array($projectTypes))
										foreach ($projectTypes as $projectType)
											if ($_POST["projectType"] == $projectType[0])
												echo "<option value='".$projectType[0]."' selected>".$projectType[1]."</option>";
											else
												echo "<option value='".$projectType[0]."'>".$projectType[1]."</option>";
								?>
							</select>
						</td>
					</tr>
			</table>
			<input name="btnGenerate" type="submit" value="Generate Report">
				<br/>
				<?php
					///////////////////////////
					//  Get Information
					///////////////////////////
					if (isset($_POST["projectType"]) && $_POST["projectType"] != "null")    {
						// Specific Project type
						$projects = q("SELECT p.id, p.name, pt.type, cot.consulting FROM (Project AS p INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id LEFT JOIN ProjectTypes AS pt ON pt.id = p.type_id) ".
										"WHERE p.completed = '0' AND p.deleted = '0' AND cot.status = '1' AND cot.deleted = '0' AND cot.company_id = '".$_SESSION["company_id"]."' ".
										"AND p.type_id = '".$_POST["projectType"]."' ORDER BY UPPER(p.name)");
					} else  {
						// All Project Types
						$projects = q("SELECT p.id, p.name, pt.type, cot.consulting FROM (Project AS p INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id LEFT JOIN ProjectTypes AS pt ON pt.id = p.type_id) ".
										"WHERE p.completed = '0' AND p.deleted = '0' AND cot.status = '1' AND cot.deleted = '0' AND cot.company_id = '".$_SESSION["company_id"]."' ".
										"ORDER BY UPPER(p.name)");
					}

					///////////////////////////
					//  Create Information String
					///////////////////////////
					//  Table Headers
					$string = "";

					$excelheadings[$row][] = "Report: Over Budget Projects";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$row++;

					if ($_POST["projectType"] == "0")
						$projectTypeHead = "Shared Projects";
					else if ($_POST["projectType"] != "" && $_POST["projectType"] != "null")
						$projectTypeHead = q("SELECT type FROM ProjectTypes WHERE id = ".$_POST["projectType"]."");
					else
						$projectTypeHead = "All Project Types";

					$excelheadings[$row][] = "Project Type: $projectTypeHead";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$row++;

					$excelheadings[$row][] = "Please Note This Report Excludes VAT";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$row++;

					$row = 0;

					$exceldata[$row][] = $colorClass."Project Name";
					$exceldata[$row][] = $colorClass."Project Type";
					$exceldata[$row][] = $colorClass."Total Budget (".$_SESSION["currency"].")";
					$exceldata[$row][] = $colorClass."Total Booked (".$_SESSION["currency"].")";
					$exceldata[$row][] = $colorClass."Total Over<br/>Budget (".$_SESSION["currency"].")";
					$row++;

					$headers = "";

					$headers .=   "<tr>
										<td class='on-table-clear' colspan='100%'><a>Report: Over Budger Projects</a></td>
									</tr>
									<tr>
										<td class='on-table-clear' colspan='100%'>Project Types: $projectTypeHead</td>
									</tr>
									<tr>
										<td class='on-table-clear' colspan='100%'>Please Note This Report Excludes VAT</td>
									</tr>";
					$headers .= "<tr>
									<th>Project Name</th>
									<th>Project Type</th>
									<th>Total Budget <i>(".$_SESSION["currency"].")</i></th>
									<th>Total Booked <i>(".$_SESSION["currency"].")</i></th>
									<th>Total Over<br/>Budget <i>(".$_SESSION["currency"].")</i></th>
								</tr>";

					$table = ($reportOn == "normal") ? "TimeSheet" : "ApprovedTime";

					$where = "";

					if ($table != "TimeSheet")	{
						if ($_POST["projectType"] == "0")
							$where .= "AND (status = '2' OR status2 = '2') ";
						else
							$where .= "AND status = '2' ";
					}

					//  Table Information
					if (is_array($projects))    {
						foreach ($projects as $p)       {
							$totalBudget = $p[3];

							if (is_numeric($totalBudget) && $totalBudget != "") {
								$sumTime = q("SELECT SUM(rate * time_spent) FROM ".$table." WHERE project_id = '".$p[0]."' AND company_id = '".$_SESSION["company_id"]."' ".$where."");

								if (number_format($totalBudget, 2, ".", "") <= number_format($sumTime, 2, ".", ""))       {
									$string .= "<tr>
													<td align='left'>".$p[1]."</td>
													<td class='left'>".$p[2]."</td>
													<td class='rightdata'>".number_format($totalBudget, 2, ".", "")."</td>
													<td class='rightdata'>".number_format($sumTime, 2, ".", "")."</td>
													<td class='rightdata'>".number_format(($totalBudget - $sumTime), 2, ".", "")."</td>
												</tr>";

									$exceldata[$row][] = $p[1];
									$exceldata[$row][] = $p[2];
									$exceldata[$row][] = number_format($totalBudget, 2, ".", "");
									$exceldata[$row][] = number_format($sumTime, 2, ".", "");
									$exceldata[$row][] = number_format(($totalBudget - $sumTime), 2, ".", "");
									$row++;
								}
							}
						}
					}

					///////////////////////////
					//  Display Information
					if ($string != "")      $string = $headers.$string;
					else                    $string .= $headers."<tr><td align='center' colspan='5'>No Projects To Display</td></tr>";

					if ($string != "")      {
						echo "<div class='on-20px'><table class='on-table-center on-table'>";
							echo "".$string;
							echo  "<tfoot>
										<tr>
											<td colspan='100%'></td>
										</tr>
									</tfoot>";
						echo "</table></div>";
						echo "<br/>";

						///////////////////////////
						//  Set Export Information
						$_SESSION["fileName"] = "Over_Budget_Projects_Report";
						if ($excelheadings && $exceldata)
							$_SESSION["fileData"] = array_merge($excelheadings, $exceldata);
						///////////////////////////

						echo "<input name='btnExport' onClick=\"location.href='_export.php'\" type='button' value='Export to Excel' />";
					}
					///////////////////////////
				?>
			</form>
		</td>
	</tr>
	<tr>
		<td align="center">
			<br/>
		</td>
	</tr>
</table>
<?php
//  Print Footer
print_footer();
?>