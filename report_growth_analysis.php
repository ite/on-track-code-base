<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("fusion/FusionCharts.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");
        
    if (!hasAccess("REP_GROWTH_ANALYSIS"))
        header("Location: noaccess.php");

    //  Set Report Generated Status
    $generated                                                          = "0";
     // Excel export Variables
     
    $excelheadings;
    $exceldata;
    $row = 0;
    
    // Class for table background color
    $colorClass = "OnTrack_#_";
    
    function createDataSet($dataSetName, $date_from, $months)
    {
        $dataSetString                                                  = "";

        if ($dataSetName == "Invoiced")
        {
            for ($i = 0; $i < $months; $i++)
            {
                $invoiced                                               = 0;
                $date                                                   = getNextMonthStart($date_from, $i);

                $invoiced                                               = q("SELECT SUM(li.consulting + li.diverse_income) FROM (LoadInvoices AS li ".
                                                                            "INNER JOIN Project AS p ON li.project_id = p.id) ".
                                                                            "WHERE p.company_id = '".$_SESSION["company_id"]."' ".
                                                                            "AND li.create_date >= '$date' AND li.create_date <= '".getMonthEnd($date)."'");

                if ($invoiced != 0)
                    $dataSetString                                      .= "<set value='".$invoiced."' />";
                else
                    $dataSetString                                      .= "<set value='' />";
            }
        }
        else if ($dataSetName == "Paid Invoices")
        {
            for ($i = 0; $i < $months; $i++)
            {
                $paid_invoices                                          = 0;
                $date                                                   = getNextMonthStart($date_from, $i);

                $paid_invoices                                          = q("SELECT SUM(li.consulting + li.diverse_income) FROM (LoadInvoices AS li ".
                                                                            "INNER JOIN Project AS p ON li.project_id = p.id) ".
                                                                            "WHERE p.company_id = '".$_SESSION["company_id"]."' AND li.paid = '1' ".
                                                                            "AND li.due_date >= '$date' AND li.due_date <= '".getMonthEnd($date)."'");

                if ($paid_invoices != 0)
                    $dataSetString                                      .= "<set value='".$paid_invoices."' />";
                else
                    $dataSetString                                      .= "<set value='' />";
            }
        }
        else if ($dataSetName == "Expenses")
        {
            for ($i = 0; $i < $months; $i++)
            {
                $date                                                   = getNextMonthStart($date_from, $i);
                $year                                                   = substr($date, 0, strpos($date, "-"));
                $month                                                  = substr($date, (strpos($date, "-") + 1), 2);

                $expenses                                               = q("SELECT SUM(amount) FROM Budget WHERE year = '$year' AND month = '$month' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."'");

                if (!$expenses)
                    $expenses                                           = 0;

                if ($expenses != 0)
                    $dataSetString                                      .= "<set value='".$expenses."' />";
                else
                    $dataSetString                                      .= "<set value='' />";
            }
        }
        else
        {
            for ($i = 0; $i < $months; $i++)
            {
                //  Calculate Netto Income
                $netto_income                                           = 0;
                $date                                                   = getNextMonthStart($date_from, $i);

                $netto_income                                           = q("SELECT SUM(li.consulting + li.diverse_income) FROM (LoadInvoices AS li ".
                                                                            "INNER JOIN Project AS p ON li.project_id = p.id) ".
                                                                            "WHERE p.company_id = '".$_SESSION["company_id"]."' ".
                                                                            "AND li.due_date >= '$date' AND li.due_date <= '".getMonthEnd($date)."'");

                //  Calculate Expenses
                $date                                                   = getNextMonthStart($date_from, $i);
                $year                                                   = substr($date, 0, strpos($date, "-"));
                $month                                                  = substr($date, (strpos($date, "-") + 1), 2);

                $expenses                                               = q("SELECT SUM(amount) FROM Budget WHERE year = '$year' AND month = '$month' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."'");

                if (!$expenses)
                    $expenses                                           = 0;

                //  Calculate Growth Trend
                if ($expenses > 0)
                {
                    $trend                                              = number_format(($netto_income / $expenses), 2, ".", "");

                    $dataSetString                                      .= "<set value='".$trend."' />";
                }
                else
                    $dataSetString                                      .= "<set value='' />";
            }
        }

        return $dataSetString;
    }

    function createXMLString($date_from, $months)
    {
        $xmlString                                                      = "";

        $xmlString                                                      = "<chart palette='1' caption='Growth Analysis Report' shownames='1' showvalues='0' ".
                                                                            "numberPrefix='".$_SESSION["currency"]."' sYAxisValuesDecimals='2' connectNullData='0' ".
                                                                            "numDivLines='4' formatNumberScale='0' ".fusionChart().">";

        //  Create Categories
        $xmlString                                                      .= "<categories>";
        for ($i = 0; $i < $months; $i++)
            $xmlString                                                  .= "<category label='".getNextMonth($date_from, $i)."'></category>";
        $xmlString                                                      .= "</categories>";

        //  Create Dataset - Invoiced
        $xmlString                                                      .= "<dataset seriesName='Invoiced (Consulting)' color='99FF33' showValues='0'>";
        $xmlString                                                      .= createDataSet("Invoiced", $date_from, $months);
        $xmlString                                                      .= "</dataset>";
        //  Create Dataset - Paid Invoices
        $xmlString                                                      .= "<dataset seriesName='Company Income' color='33FF00' showValues='0'>";
        $xmlString                                                      .= createDataSet("Paid Invoices", $date_from, $months);
        $xmlString                                                      .= "</dataset>";
        //  Create Dataset - Expenses
        $xmlString                                                      .= "<dataset seriesName='Company Running Cost' color='FF0033' showValues='0'>";
        $xmlString                                                      .= createDataSet("Expenses", $date_from, $months);
        $xmlString                                                      .= "</dataset>";
        //  Create Dataset - Growth Trend
        $xmlString                                                      .= "<dataset seriesName='Growth Trend' color='0033FF' showValues='0' parentYAxis='S'>";
        $xmlString                                                      .= createDataSet("Growth Trend", $date_from, $months);
        $xmlString                                                      .= "</dataset>";

        $xmlString                                                      .= "</chart>";

        return $xmlString;
    }

    //  Create Display Information Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")
    {
        $errorMessage                                                   = "";

        $date_from                                                      = addslashes(strip_tags($_POST["date_from"]));
        $date_to                                                        = addslashes(strip_tags($_POST["date_to"]));
        $displayString                                                  = "";

        $date_from                                                      = getMonthStart($date_from);
        $date_to                                                        = getMonthEnd($date_to);
        $months                                                         = getMonthPeriod($date_from, $date_to);

        $graph                                                          = createXMLString($date_from, $months);
        $displayString                                                  = renderChart("fusion/MSColumn3DLineDY.swf", "", $graph, "multigraph", 800, 400, false, false);

        //  Column Headings
        $headers                                                        = "<tr>
                                                                                    <th>Month</th>
                                                                                    <th>Invoiced (Consulting) <i>(".$_SESSION["currency"].")</i></th>
                                                                                    <th>Company Income <i>(".$_SESSION["currency"].")</i></th>
                                                                                    <th>Company Running Cost <i>(".$_SESSION["currency"].")</i></th>
                                                                                    <th>Growth Trend</th>
                                                                                </tr>";
        $excelheadings[$row][]                                 = "";
        $excelheadings[$row][]                                 = "";
        $excelheadings[$row][]                                 = "";
        $excelheadings[$row][]                                 = "";
        $excelheadings[$row][]                                 = "";
        
        $exceldata[$row][]                                         = "Report: Growth Analysis";
        $exceldata[$row][]                                         = "";
        $exceldata[$row][]                                         = "";
        $exceldata[$row][]                                         = "";
        $exceldata[$row][]                                         = "";
            $row++;
        
        $exceldata[$row][]                                         = "Company Name: ".$_SESSION["company_name"];
        $exceldata[$row][]                                         = "";
        $exceldata[$row][]                                         = "";
        $exceldata[$row][]                                         = "";
        $exceldata[$row][]                                         = "";
            $row++;
            
        $exceldata[$row][]                                         = "Time Period: ".$date_from." - ".$date_to;
        $exceldata[$row][]                                         = "";
        $exceldata[$row][]                                         = "";
        $exceldata[$row][]                                         = "";
        $exceldata[$row][]                                         = "";
            $row++;
        
        $exceldata[$row][]                                         = $colorClass."Month";
        $exceldata[$row][]                                         = $colorClass."Invoiced (Consulting) (".$_SESSION["currency"].")";
        $exceldata[$row][]                                         = $colorClass."Company Income (".$_SESSION["currency"].")";
        $exceldata[$row][]                                         = $colorClass."Company Running Cost (".$_SESSION["currency"].")";
        $exceldata[$row][]                                         = $colorClass."Growth Trend";
            $row++;


        $display                                                        = "";

        //  Column Data
        for ($i = 0; $i < $months; $i++)    {
            $display                                                    .= "<tr>";
            //  Months
            $display                                                    .= "<td>".getNextMonth($date_from, $i)."</td>";
            $exceldata[$row][]                                      = getNextMonth($date_from, $i);  //  1/5

            //  Invoiced
            $invoiced                                                   = 0;
            $date                                                       = getNextMonthStart($date_from, $i);

            $invoiced                                                   = q("SELECT SUM(li.consulting + li.diverse_income) FROM (LoadInvoices AS li ".
                                                                            "INNER JOIN Project AS p ON li.project_id = p.id) ".
                                                                            "WHERE p.company_id = '".$_SESSION["company_id"]."' ".
                                                                            "AND li.create_date >= '$date' AND li.create_date <= '".getMonthEnd($date)."'");

            if ($invoiced != 0){
                $display                                                .= "<td class='rightdata'>".$_SESSION["currency"]." ".number_format($invoiced, 2, ".", "")."</td>";
                $exceldata[$row][]                                  = $_SESSION["currency"]." ".number_format($invoiced, 2, ".", "");  //  2/5
            }else{
                $display                                                .= "<td class='rightdata'>-</td>";
                $exceldata[$row][]                                  = "-";  //  2/5
            }

            //  Paid Invoices
            $netto_income                                               = 0;

            $netto_income                                               = q("SELECT SUM(li.consulting + li.diverse_income) FROM (LoadInvoices AS li ".
                                                                            "INNER JOIN Project AS p ON li.project_id = p.id) ".
                                                                            "WHERE p.company_id = '".$_SESSION["company_id"]."' AND li.paid = '1' ".
                                                                            "AND li.due_date >= '$date' AND li.due_date <= '".getMonthEnd($date)."'");

            if ($netto_income != 0){
                $display                                                .= "<td class='rightdata'>".$_SESSION["currency"]." ".number_format($netto_income, 2, ".", "")."</td>";
                $exceldata[$row][]                                  = $_SESSION["currency"]." ".number_format($netto_income, 2, ".", "");  //  3/5
            }else{
                $display                                                .= "<td class='rightdata'>-</td>";
                $exceldata[$row][]                                  = "-";  //  3/5
            }

            //  Expenses
            $year                                                       = substr($date, 0, strpos($date, "-"));
            $month                                                      = substr($date, (strpos($date, "-") + 1), 2);

            $expenses                                                   = q("SELECT SUM(amount) FROM Budget WHERE year = '$year' AND month = '$month' AND company_id = '".$_SESSION["company_id"]."'");

            if (!$expenses)
                $expenses                                               = 0;

            if ($expenses != 0) {
                $display                                                .= "<td class='rightdata'>".$_SESSION["currency"]." ".number_format($expenses, 2, ".", "")."</td>";
                $exceldata[$row][]                                  = $_SESSION["currency"]." ".number_format($expenses, 2, ".", "");  //  4/5
            }else{
                $display                                                .= "<td class='rightdata'>-</td>";
                $exceldata[$row][]                                  = "-";  //  4/5
            }

            //  Growth Trend
            if ($expenses > 0)  {
                $trend                                                   = number_format(($netto_income / $expenses), 2, ".", "");
                $display                                                .= "<td class='rightdata'>".number_format($trend, 2, ".", "")."</td>";
                $exceldata[$row][]                                  = number_format($trend, 2, ".", "");  //  5/5
            }else{
                $display                                                .= "<td class='rightdata'>-</td>";
                $exceldata[$row][]                                  = "-";  //  5/5
            }
            $display                                                    .= "</tr>";
            $row++;
        }

        if ($display != "")
            $display                                                    = $headers.$display;

        $displayString                                                  = "<tr>
                                                                                        <td class='on-table-clear' colspan='100%'><a>Growth Analysis Report</a></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class='on-table-clear' colspan='100%'>Company Name: ".$_SESSION["company_name"]."</td>
                                                                                    </tr>
                                                                                    <td colspan='5'><br/>".$displayString."<br/></td>".$display;

        if ($displayString != "")
            $generated                                                  = "1";
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("5", "reports");

    if ($errorMessage != "")
    {
        echo "<p align='center' style='padding:0px;'><strong><font color='#999999'>$errorMessage</font></strong></p>";
        echo "<br/>";
    }
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    jQuery(function(){
	jQuery('#date_from').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_from').val(),
		current: jQuery('#date_from').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_from').val()))
			_date = new Date();
			else _date = jQuery('#date_from').val();
			jQuery('#date_from').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_from').val(formated);
			jQuery('#date_from').DatePickerHide();
		}
	});
    })

    jQuery(function(){
	jQuery('#date_to').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_to').val(),
		current: jQuery('#date_to').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_to').val()))
			_date = new Date();
			else _date = jQuery('#date_to').val();
			jQuery('#date_to').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_to').val(formated);
			jQuery('#date_to').DatePickerHide();
		}
	});
    })

    function check()
    {
        var valid                                                       = 1;

        //  Check That Date From Is Entered
        if (document.forms["report_growth_analysis"].date_from.value == "")
        {
            ShowLayer("dateFrom", "block");
            valid                                                       = 0;
        }
        //  Check That Entered Date From Is Valid
        else
        {
            ShowLayer("dateFrom", "none");

            if (!validation("date", document.forms["report_growth_analysis"].date_from.value))
            {
                ShowLayer("dateFromDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("dateFromDiv", "none");
        }

        //  Check That Date To Is Entered
        if (document.forms["report_growth_analysis"].date_to.value == "")
        {
            ShowLayer("dateTo", "block");
            valid                                                       = 0;
        }
        //  Check That Entered Date To Is Valid
        else
        {
            ShowLayer("dateTo", "none");

            if (!validation("date", document.forms["report_growth_analysis"].date_to.value))
            {
                ShowLayer("dateToDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("dateToDiv", "none");
        }

        if (valid == 1)
        {
            document.forms["report_growth_analysis"].save.value         = 1;
            document.forms["report_growth_analysis"].submit();
        }
    }
</script>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="report_growth_analysis">
                    <div id="" style="<?php if ($generated == "1") echo "display: none;"; else echo "display: block"; ?>">
                        <table width="100%">
                            <tr>
                                <td class="centerdata">
                                    <h6>Growth Analysis Report</h6>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Date From:
                                </td>
                                <td width="50%">
                                    <input class="on-field-date" id="date_from" name="date_from" type="text" style="text-align:right;" value="<?php
                                        if ($_POST["date_from"] != "") echo "".$_POST["date_from"]; else echo "".getMonthStart($today); ?>">
                                    <div id="dateFrom" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                    <div id="dateFromDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Date To:
                                </td>
                                <td width="50%">
                                    <input class="on-field-date" id="date_to" name="date_to" type="text" style="text-align:right;" value="<?php
                                        if ($_POST["date_to"] != "") echo "".$_POST["date_to"]; else echo "".getMonthEnd($today); ?>">
                                    <div id="dateTo" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                    <div id="dateToDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <input name="btnGenerate" onClick="check();" type="button" value="Generate Report">
                        <input method="post" name="save" type="hidden" value="0" />
                    </div>
                </form>
                <div id="report" style="<?php if ($generated == "1") echo "display: block;"; else echo "display: none"; ?>">
                    <?php
                        echo "<table class='on-table-center on-table'>";
                            echo "".$report_heading.$displayString;
                            echo  "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        echo "</table><br>";
                        
                        ///////////////////////////
                        //  Set Export Information
                        $_SESSION["fileName"] = "Growth Analysis Report";
                        $_SESSION["fileData"] = array_merge($excelheadings, $exceldata);
                        ///////////////////////////

                        echo "<input name='btnExport' onClick=\"location.href='_export.php'\" type='button' value='Export to Excel' />";
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>