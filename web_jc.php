<?php
    include("_db.php");
    include("_dates.php");
?>
<html>
    <head>
        <title>
            On-Track - Recovery Script...
        </title>
    </head>
    <body>
        <h1 align="center">
            Install
        </h1>
        <?php
 
	//Roles to which users can be assigned
	$drop = q("DROP TABLE IF EXISTS roles");
	$create = q("CREATE TABLE roles ( ".
		"id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,".
		"role VARCHAR(12),".
		"descr VARCHAR(255),".
		"active TINYINT(3) DEFAULT 1)");

	//Actions defined to be assigned to roles
	$drop = q("DROP TABLE IF EXISTS actions");
	$create = q("CREATE TABLE actions ( ".
		"id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,".
		"action VARCHAR(20),".
		"descr VARCHAR(255),".
		"active TINYINT(3) DEFAULT 1)");

	$insert = q("INSERT INTO roles(role,descr) VALUES ('MAN','Manager')");
	$insert = q("INSERT INTO roles(role,descr) VALUES ('SPM','Shared Project Manager')");
	$insert = q("INSERT INTO roles(role,descr) VALUES ('FM','Financial Manager')");
	$insert = q("INSERT INTO roles(role,descr) VALUES ('PM','Project Manager')");
	$insert = q("INSERT INTO roles(role,descr) VALUES ('CL','Client')");
	$insert = q("INSERT INTO roles(role,descr) VALUES ('ADM','Admin')");
	$insert = q("INSERT INTO roles(role,descr) VALUES ('TM','Type Manager')");
	$insert = q("INSERT INTO roles(role,descr) VALUES ('CA','Company Administrator')");

        $a[] = "'CHNG_PW','Change Password'";
        $a[] = "'EULA','End-User Licence Agreement'";
	$a[] = "'TIME_MANAGE','Manage own time'";	//1
	$a[] = "'EXPENSE_MANAGE_OWN','Manage own expenses'";	//1
	$a[] = "'LEAVE_MANAGE_OWN','Manage own leave'";	//1
	$a[] = "'SHAREDRES_MANAGE_OWN','Manage own shared resources'";	//1
	$a[] = "'SHAREDRES_MANAGE','Manage shared resources'";	//1
	$a[] = "'LOGS_VIEW_OWN','View own logs'";	//1
	$a[] = "'EMP_EDIT','Edit current employees. Cannot view cost to company or tariffs'";	//MAN, PM
	$a[] = "'EMP_ADD_REMOVE','Add and remove employees'";									//MAN
	$a[] = "'EMP_CTC_MANAGE','Manage employee cost to company'";							//MAN, FM
	$a[] = "'EMP_TARIFF_MANAGE','Manage employee tariffs'";								//MAN, FM
	$a[] = "'PROJ_MANAGE','Manage projects'";		//1										//PM
	$a[] = "'VEH_MANAGE','Manage vehicles'";		//1										//PM
	$a[] = "'SLA_MANAGE','Manage SLA'";				//1									//PM
	$a[] = "'ATYPE_MANAGE','Manage areas, activityTypes and Activities'";	//1									
	$a[] = "'AREA_MANAGE','Manage project types'";	//1									
	$a[] = "'PROJTYPE_MANAGE','Manage project types'";	//1									
	$a[] = "'PRINTING_MANAGE','Manage printing'";		//1											
	$a[] = "'BUDGET_CAT_MANAGE','Manage budget categories'";	//1								//FM
	$a[] = "'BUDGET_MANAGE','Manage budget'";					//1							//FM
	$a[] = "'BUDGET_VIEW','View budget'";						//1							//FM,PM
	$a[] = "'INVOICE_MANAGE','Manage invoices'";				//1							
	$a[] = "'INVOICE_VIEW','View and edit date and paid fields on invoices'";	//1			//NEW
	$a[] = "'EXPENSE_MANAGE','Manage expenses'";				//1							
	$a[] = "'EXPENSE_VIEW','View and edit date and paid fields on expenses'";	//1			//NEW
	$a[] = "'LOGS_VIEW_ALL','View all logs'";				//1								//NEW
	$a[] = "'APPROVAL_MANAGE','Approval module'";			//1									
	$a[] = "'SPM_MANAGE','Shared Project module'";			//1									
	$a[] = "'LEAVE_MANAGE_ALL','Manage all leave'";			//1							//NEW
	$a[] = "'RA_MANAGEMENT','Role and Action Management'";
    
    $a[] = "'REP_TIME_EX','Report time and expense'";
    $a[] = "'REP_ACTIVE_PROJ','Report active projects'";
    $a[] = "'REP_ANALYSIS','Report analysis'";
    $a[] = "'REP_BUDGET','Report budget'";
    $a[] = "'REP_BUDGET_PERIOD','Report budget period'";
    $a[] = "'REP_CASH_FLOW','Report cash flow forecast'";
    $a[] = "'REP_COMPANY_BREAK','Report company breakdown'";
    $a[] = "'REP_INVOICE_TRACK','Report invoice tracking'";
    $a[] = "'REP_COMPANY_SUMM','Report company summary'";
    $a[] = "'REP_EX_SHEET','Report expense sheet'";
    $a[] = "'REP_GANTT','Report gantt'";
    $a[] = "'REP_GROWTH_ANALYSIS','Report growth analysis'";
    $a[] = "'REP_INCOME_FORECAST','Report income forecast'";
    $a[] = "'REP_IN_FORE_INVOICE','Report income forecast invoice'";
    $a[] = "'REP_IN_FORE_TIME','Report income forecast time'";
    $a[] = "'REP_PROJ_BREAK','Report project breakdown'";
    $a[] = "'REP_IN_VS_CTC','Report invoiced vs cost to company'";
    $a[] = "'REP_LEAVE','Report leave'";
    $a[] = "'REP_LOADED_INV','Report loaded invoices'";
    $a[] = "'REP_PROGRESS','Report progress'";
    $a[] = "'REP_PROJECT','Report project'";
    $a[] = "'REP_PROJ_INFO','Report project info'";
    $a[] = "'REP_PROJ_LIST','Report project list'";
    $a[] = "'REP_PROJ_TYPE_COM','Report project type comparison'";
    $a[] = "'REP_RUNNING_PROF','Report running profit'";
    $a[] = "'REP_SUMMARY','Report summary'";
    $a[] = "'REP_SUMMARY_EX','Report summary expenses'";
    $a[] = "'REP_SUMMARY_TIME','Report summary time'";
    $a[] = "'REP_TIME_COM',' Report time comparison'";
    $a[] = "'REP_TIME_PROJ','Report time project'";
    $a[] = "'REP_TIME_SHEET','Report time sheet'";
    $a[] = "'REP_WORK_IN_PR','Report work in process'";
    $a[] = "'REP_WORK_IN_PR_M','Report work in process months'";

    // Shared Project Reports
    $a[] = "'REP_SP_TIME_SHEET','Report shared project time sheet'";
    $a[] = "'REP_SP_EX_SHEET','Report shared project expense sheet'";
    $a[] = "'REP_SP_SUMMARY_TIME','Report shared project summary time'";
    $a[] = "'REP_SP_SUMMARY_EX','Report shared project summary expense'";
    $a[] = "'REP_SP_SUMMARY','Report shared project summary'";
    $a[] = "'REP_SP_BREAK','Report shared project breakdown'";
    $a[] = "'REP_SP_INV','Report shared project invoices'";
    
	//TODO: add reports (1 each) and notifications (1 each)
	//TODO: employee delete functoin should be subject to 30 creation minimum
	//TODO: company assighment

	foreach($a as $d)
		$insert = q("INSERT INTO actions(action,descr) VALUES ($d)");

	//BRIDGE roles -> actions
	$drop = q("DROP TABLE IF EXISTS role_action");
	$create = q("CREATE TABLE role_action ( ".
		"id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,".
		"companyid SMALLINT UNSIGNED NOT NULL,".
		"roleid SMALLINT UNSIGNED NOT NULL,".
		"actionid SMALLINT UNSIGNED NOT NULL)");

	//add all actions to admin for now
	$cc = "ADM";
	foreach($a as $d)
		$insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES (1,(SELECT id FROM roles WHERE role = '$cc'),(SELECT id FROM actions WHERE action = '".substr($d,1,strpos($d,",")-2)."'))");

	$cc = "CL";
	$a = array('TIME_MANAGE','EXPENSE_MANAGE_OWN','LEAVE_MANAGE_OWN','SHAREDRES_MANAGE_OWN','LOGS_VIEW_OWN');
	foreach($a as $d)
		$insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES (1,(SELECT id FROM roles WHERE role = '$cc'),(SELECT id FROM actions WHERE action = '$d'))");

	$cc = "MAN";
	$a = array('RA_MANAGEMENT','LEAVE_MANAGE_ALL','LOGS_VIEW_ALL','EMP_ADD_REMOVE','EMP_CTC_MANAGE','EMP_TARIFF_MANAGE');
	foreach($a as $d)
		$insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES (1,(SELECT id FROM roles WHERE role = '$cc'),(SELECT id FROM actions WHERE action = '$d'))");

	$cc = "PM";
	$a = array('PROJ_MANAGE','EMP_EDIT','VEH_MANAGE','PRINTING_MANAGE','SLA_MANAGE','APPROVAL_MANAGE','INVOICE_VIEW','EXPENSE_VIEW','EMP_TARIFF_MANAGE');
	foreach($a as $d)
		$insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES (1,(SELECT id FROM roles WHERE role = '$cc'),(SELECT id FROM actions WHERE action = '$d'))");

	$cc = "FM";
	$a = array('BUDGET_CAT_MANAGE','BUDGET_MANAGE','INVOICE_MANAGE','EXPENSE_MANAGE','EMP_ADD_REMOVE','EMP_CTC_MANAGE','EMP_TARIFF_MANAGE');
	foreach($a as $d)
		$insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES (1,(SELECT id FROM roles WHERE role = '$cc'),(SELECT id FROM actions WHERE action = '$d'))");

	$cc = "SPM";
	$a = array('SPM_MANAGE','REP_SP_TIME_SHEET','REP_SP_EX_SHEET','REP_SP_SUMMARY_TIME','REP_SP_SUMMARY_EX','REP_SP_BREAK','REP_SP_SUMMARY','REP_SP_INV');
	foreach($a as $d)
		$insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES (1,(SELECT id FROM roles WHERE role = '$cc'),(SELECT id FROM actions WHERE action = '$d'))");

	$cc = "TM";
	$a = array('AREA_MANAGE', 'ATYPE_MANAGE','PROJTYPE_MANAGE');
	foreach($a as $d)
		$insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES (1,(SELECT id FROM roles WHERE role = '$cc'),(SELECT id FROM actions WHERE action = '$d'))");

	$cc = "CA";
	$a = array('SHAREDRES_MANAGE','VEH_MANAGE','RA_MANAGEMENT');
	foreach($a as $d)
		$insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES (1,(SELECT id FROM roles WHERE role = '$cc'),(SELECT id FROM actions WHERE action = '$d'))");


	//TODO: add actions to roles here after defined in meeting

	//BRIDGE users -> roles
	$drop = q("DROP TABLE IF EXISTS user_role");
	$create = q("CREATE TABLE user_role ( ".
		"id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,".
		"companyid SMALLINT UNSIGNED NOT NULL,".
		"userid SMALLINT UNSIGNED NOT NULL,".
		"roleid SMALLINT UNSIGNED NOT NULL,".
		"dateupdated CHAR(20),".
		"byuser VARCHAR(60))");

	$insert = q("INSERT INTO user_role(companyid,userid,roleid) VALUES (1,(SELECT id FROM Employee WHERE email = 'admin'),(SELECT id FROM roles WHERE role = 'ADM'))");
	
	$insert = q("INSERT INTO user_role(companyid,userid,roleid) VALUES (1,(SELECT id FROM Employee WHERE email = 'jc@ite.co.za'),(SELECT id FROM roles WHERE role = 'CL'))");
	$insert = q("INSERT INTO user_role(companyid,userid,roleid) VALUES (1,(SELECT id FROM Employee WHERE email = 'jc@ite.co.za'),(SELECT id FROM roles WHERE role = 'PM'))");
	$insert = q("INSERT INTO user_role(companyid,userid,roleid) VALUES (1,(SELECT id FROM Employee WHERE email = 'jc@ite.co.za'),(SELECT id FROM roles WHERE role = 'FM'))");
	$insert = q("INSERT INTO user_role(companyid,userid,roleid) VALUES (1,(SELECT id FROM Employee WHERE email = 'jc@ite.co.za'),(SELECT id FROM roles WHERE role = 'SPM'))");
	$insert = q("INSERT INTO user_role(companyid,userid,roleid) VALUES (1,(SELECT id FROM Employee WHERE email = 'jc@ite.co.za'),(SELECT id FROM roles WHERE role = 'MAN'))");
	$insert = q("INSERT INTO user_role(companyid,userid,roleid) VALUES (1,(SELECT id FROM Employee WHERE email = 'jc@ite.co.za'),(SELECT id FROM roles WHERE role = 'TM'))");
	$insert = q("INSERT INTO user_role(companyid,userid,roleid) VALUES (1,(SELECT id FROM Employee WHERE email = 'jc@ite.co.za'),(SELECT id FROM roles WHERE role = 'CA'))");

	$insert = q("INSERT INTO user_role(companyid,userid,roleid) VALUES (1,(SELECT id FROM Employee WHERE email = 'mkruger@ite.co.za'),(SELECT id FROM roles WHERE role = 'CL'))");
	$insert = q("INSERT INTO user_role(companyid,userid,roleid) VALUES (1,(SELECT id FROM Employee WHERE email = 'mkruger@ite.co.za'),(SELECT id FROM roles WHERE role = 'PM'))");
	$insert = q("INSERT INTO user_role(companyid,userid,roleid) VALUES (1,(SELECT id FROM Employee WHERE email = 'mkruger@ite.co.za'),(SELECT id FROM roles WHERE role = 'FM'))");
	$insert = q("INSERT INTO user_role(companyid,userid,roleid) VALUES (1,(SELECT id FROM Employee WHERE email = 'mkruger@ite.co.za'),(SELECT id FROM roles WHERE role = 'SPM'))");
	$insert = q("INSERT INTO user_role(companyid,userid,roleid) VALUES (1,(SELECT id FROM Employee WHERE email = 'mkruger@ite.co.za'),(SELECT id FROM roles WHERE role = 'MAN'))");
	$insert = q("INSERT INTO user_role(companyid,userid,roleid) VALUES (1,(SELECT id FROM Employee WHERE email = 'mkruger@ite.co.za'),(SELECT id FROM roles WHERE role = 'TM'))");
	$insert = q("INSERT INTO user_role(companyid,userid,roleid) VALUES (1,(SELECT id FROM Employee WHERE email = 'mkruger@ite.co.za'),(SELECT id FROM roles WHERE role = 'CA'))");
	
	$insert = q("INSERT INTO user_role(companyid,userid,roleid) VALUES (1,(SELECT id FROM Employee WHERE email = 'efaul@integrityengineering.co.za'),(SELECT id FROM roles WHERE role = 'CL'))");
	$insert = q("INSERT INTO user_role(companyid,userid,roleid) VALUES (1,(SELECT id FROM Employee WHERE email = 'efaul@integrityengineering.co.za'),(SELECT id FROM roles WHERE role = 'PM'))");
	$insert = q("INSERT INTO user_role(companyid,userid,roleid) VALUES (1,(SELECT id FROM Employee WHERE email = 'efaul@integrityengineering.co.za'),(SELECT id FROM roles WHERE role = 'FM'))");
	$insert = q("INSERT INTO user_role(companyid,userid,roleid) VALUES (1,(SELECT id FROM Employee WHERE email = 'efaul@integrityengineering.co.za'),(SELECT id FROM roles WHERE role = 'SPM'))");
	$insert = q("INSERT INTO user_role(companyid,userid,roleid) VALUES (1,(SELECT id FROM Employee WHERE email = 'efaul@integrityengineering.co.za'),(SELECT id FROM roles WHERE role = 'MAN'))");
	$insert = q("INSERT INTO user_role(companyid,userid,roleid) VALUES (1,(SELECT id FROM Employee WHERE email = 'efaul@integrityengineering.co.za'),(SELECT id FROM roles WHERE role = 'TM'))");
	$insert = q("INSERT INTO user_role(companyid,userid,roleid) VALUES (1,(SELECT id FROM Employee WHERE email = 'efaul@integrityengineering.co.za'),(SELECT id FROM roles WHERE role = 'CA'))");

	$insert = q("INSERT INTO user_role(companyid,userid,roleid) VALUES (1,(SELECT id FROM Employee WHERE email = 'jpdb@integrityengineering.co.za'),(SELECT id FROM roles WHERE role = 'CL'))");
	$insert = q("INSERT INTO user_role(companyid,userid,roleid) VALUES (1,(SELECT id FROM Employee WHERE email = 'jpdb@integrityengineering.co.za'),(SELECT id FROM roles WHERE role = 'PM'))");
	$insert = q("INSERT INTO user_role(companyid,userid,roleid) VALUES (1,(SELECT id FROM Employee WHERE email = 'jpdb@integrityengineering.co.za'),(SELECT id FROM roles WHERE role = 'FM'))");
	$insert = q("INSERT INTO user_role(companyid,userid,roleid) VALUES (1,(SELECT id FROM Employee WHERE email = 'jpdb@integrityengineering.co.za'),(SELECT id FROM roles WHERE role = 'SPM'))");
	$insert = q("INSERT INTO user_role(companyid,userid,roleid) VALUES (1,(SELECT id FROM Employee WHERE email = 'jpdb@integrityengineering.co.za'),(SELECT id FROM roles WHERE role = 'MAN'))");
	$insert = q("INSERT INTO user_role(companyid,userid,roleid) VALUES (1,(SELECT id FROM Employee WHERE email = 'jpdb@integrityengineering.co.za'),(SELECT id FROM roles WHERE role = 'TM'))");
	$insert = q("INSERT INTO user_role(companyid,userid,roleid) VALUES (1,(SELECT id FROM Employee WHERE email = 'jpdb@integrityengineering.co.za'),(SELECT id FROM roles WHERE role = 'CA'))");

   /*
    $insert = q("ALTER TABLE TimeSheet ADD COLUMN `area_id` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 AFTER `project_id`");
	$insert = q("ALTER TABLE ApprovedTime ADD COLUMN `area_id` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 AFTER `project_id`");
    $insert = q("ALTER TABLE ExpenseSheet ADD COLUMN `area_id` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 AFTER `project_id`");
    $insert = q("ALTER TABLE ApprovedExpense ADD COLUMN `area_id` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 AFTER `project_id`");
    
    $insert = q("ALTER TABLE auditlogtime ADD COLUMN `area_id` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 AFTER `project_id`");
    $insert = q("ALTER TABLE auditlogexpense ADD COLUMN `area_id` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 AFTER `project_id`");
 
    $insert = q("ALTER TABLE `ExpenseSheet` ADD COLUMN `activity_id` SMALLINT(5) UNSIGNED AFTER `area_id`;");
    $insert = q("ALTER TABLE `ApprovedExpense` ADD COLUMN `activity_id` SMALLINT(5) UNSIGNED AFTER `area_id`;");   
    */
    
        ?>
        <form action="index.php" method="post">
            <center><input type="submit" value="Return"/></center>
        </form>
    </body>
</html>
