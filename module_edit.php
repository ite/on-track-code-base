<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if ($_SESSION["logged_in"] === true && !($_SESSION["usertype"] === "0"))
        header("Location: home.php");

    //  Update Module Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")
    {
        $errorMessage                                                   = "";

        //  Get Information
        $id                                                             = $_GET["id"];
        $module_name                                                    = addslashes(strip_tags($_POST["module_name"]));
        $module_code                                                    = addslashes(strip_tags($_POST["module_code"]));

        //  Check If Company Exists In Database
        $exist                                                          = q("SELECT id FROM Modules WHERE name = '$module_name' AND id != '$id'");

        if (!$exist)
        {
            $update                                                     = q("UPDATE Modules SET name = '$module_name', code = '$module_code' WHERE id = '$id'");

            if ($update)
            {
                $time                                                   = date("H:i:s");

                $logs                                                   = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time) ".
                                                                            "VALUES ('$module_name updated', 'Update', 'Modules', '".$_SESSION["email"]."', ".
                                                                            "'$today', '$time')");

                header("Location: modules.php");
            }
        }
        else
            $errorMessage                                               = "Module Already Exists";
    }

    if ($errorMessage != "")
        echo "<p align='center' style='padding:0px;'><strong><font class='on-validate-error'>$errorMessage</font></strong></p>";

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0", "modules");
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    var cal                                                             = new CodeThatCalendar(caldef1);

    function check()
    {
        var valid                                                       = 1;

        //  Check That Module Name Is Entered
        if (document.forms["module_edit"].module_name.value == "")
        {
            ShowLayer("moduleName", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("moduleName", "none");

        //  Check That Module Code Is Entered
        if (document.forms["module_edit"].module_code.value == "")
        {
            ShowLayer("moduleCode", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("moduleCode", "none");

        if (valid == 1)
        {
            document.forms["module_edit"].save.value                    = 1;
            document.forms["module_edit"].submit();
        }
    }
</script>
<?php
    $id                                                                 = $_GET["id"];
    $module_info                                                        = q("SELECT name, code FROM Modules WHERE id = '$id'");
?>
    <table width="100%">
        <tr>
            <td class="centerdata" valign="top">
                <form action="" method="post" name="module_edit">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Edit Module</h6>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                Module Name:
                            </td>
                            <td width="50%">
                                <input class="on-field" name="module_name" tabindex="1" type="text" value="<?php echo $module_info[0][0]; ?>">
                                <div id="moduleName" style="display: none;"><font class="on-validate-error">* Name must be entered</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                Module Code:
                            </td>
                            <td width="50%">
                                <input class="on-field" name="module_code" tabindex="1" type="text" value="<?php echo $module_info[0][1]; ?>">
                                <div id="moduleCode" style="display: none;"><font class="on-validate-error">* Code must be entered</font></div>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <input name="btnUpdate" onClick="check();" tabindex="4" type="button" value="Update Module">
                    <input method="post" name="save" type="hidden" value="0" />
                </form>
            </td>
        </tr>
        <tr>
            <td class="centerdata">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>