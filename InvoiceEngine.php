<?php
/*
*  File:        InvoiceEngine.php
*  Updated On:  2011-10-19
*  Author:      integrITyEngineering
*/


class InvoiceEngine {
	private $MACHINE_ID = "";
	private $UN = "";
	private $PW = "";
		 
	public function InvoiceEngine($machine_id,$un,$pw)	{
		$this->MACHINE_ID = $machine_id;
		$this->UN = $un;
		$this->PW = $pw;
	}

	public function process($action, $domain, $id = "", $data = "")	{
		$url = "https://".$this->MACHINE_ID.".blinksale.com/$domain";
		if ($id != "")	$url .= "/$id";
		$userpwd = $this->UN.":".$this->PW;
		$header = array("Accept: application/vnd.blinksale+xml", "Content-Type: application/vnd.blinksale+xml");

		$ch = curl_init();
		switch ($action) {
			case "SELECT":		//SELECT
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_USERPWD, $userpwd);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $header); 
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				break;
			case "INSERT":	//INSERT
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_USERPWD, $userpwd);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $header); 
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
				curl_setopt($ch ,CURLOPT_RETURNTRANSFER, true);
				break;
			case "UPDATE":		//UPDATE
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_USERPWD, $userpwd);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $header); 
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				break;
			case "DELETE":	//DELETE
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_USERPWD, $userpwd);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $header); 
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				break;
		}
		$result = curl_exec($ch);
		curl_close($ch);
		return $result;
	}

	///////////////////
	//  Clients
	///////////////////
	public function client_new($name, $address1 = "", $address2 = "", $city = "", $state = "", $zip = "", $country = "SA", 
			$url = "", $phone = "", $fax = "", $tax_id_name = "", $tax_id = "") {
		$xml = "client";
		$fields = array("name", "address1", "address2", "city", "state", "zip", "country", "url", "phone", "fax", "tax_id_name", "tax_id");

		//construct xml
		$data = "<?xml version='1.0' encoding='UTF-8'?>\n<$xml xmlns='http://www.blinksale.com/api'>\n";
		foreach($fields as $f)	//creates tags from fields above ex: <name>someinfo</name>
			eval("\$data .= \"\t<$f>\$".$f."</$f>\n\";");
		$data .= "</$xml>";

		//process
		$result = $this->process("INSERT",$xml."s", 0, $data);
		
		$res = new SimpleXMLElement($result);
		$list = (string)substr($res['uri'], strrpos($res['uri'], "/") +1);
		return $list;
	}

	public function client_update($id, $name, $address1 = "", $address2 = "", $city = "", $state = "", $zip = "", $country = "SA", 
			$url = "", $phone = "", $fax = "", $tax_id_name = "", $tax_id = "") {
		$xml = "client";
		$fields = array("name", "address1", "address2", "city", "state", "zip", "country", "url", "phone", "fax", "tax_id_name", "tax_id");

		//construct xml
		$data = "<?xml version='1.0' encoding='UTF-8'?>\n<$xml xmlns='http://www.blinksale.com/api'>\n";
		foreach($fields as $f)	//creates tags from fields above ex: <name>someinfo</name>
			eval("\$data .= \"\t<$f>\$".$f."</$f>\n\";");
		$data .= "</$xml>";

		//process
		$result = $this->process("UPDATE",$xml."s", $id, $data);
		
		$res = new SimpleXMLElement($result);
		$list = (string)substr($res['uri'], strrpos($res['uri'], "/") +1);
		return $list;
	}

	public function client_get($id = 0)  {
		$xml = "client";
		$xml_sub = $xml;
		$field = "name";
		if ($id == 0)	$result = $this->process("SELECT", $xml."s");
		else			$result = $this->process("SELECT", $xml."s", $id);
//		return $result;
		$res = new SimpleXMLElement($result);
		foreach ($res->$xml_sub as $node)	{
			$list[$c][0]	= (string)substr($node['uri'], strrpos($node['uri'], "/") +1);
			$list[$c++][1]	= (string)$node->$field;
		}
		return $list;
	}

	public function client_delete($id) {
		$xml = "client";
		$result = $this->process("DELETE", $xml."s", $id);
		return $result;
	}

	///////////////////
	//  People
	///////////////////
	public function people_new($client_id, $first_name, $last_name, $email, $phone_office = "", $phone_mobile = "") {
		$xml = "person";
		$fields = array("first_name", "last_name", "email", "phone_office", "phone_mobile");

		//construct xml
		$data = "<?xml version='1.0' encoding='UTF-8'?>\n<$xml xmlns='http://www.blinksale.com/api'>\n";
		foreach($fields as $f)	//creates tags from fields above ex: <name>someinfo</name>
			eval("\$data .= \"\t<$f>\$".$f."</$f>\n\";");
		$data .= "</$xml>";

		//process
		$result = $this->process("INSERT", "clients/$client_id/people", 0, $data);
		
		$res = new SimpleXMLElement($result);
		$list = (string)substr($res['uri'], strrpos($res['uri'], "/") +1);
		return $list;
	}

	public function people_update($client_id, $id, $first_name, $last_name = "", $email = "", $phone_office = "", $phone_mobile = "") {
		$xml = "person";
		$fields = array("first_name", "last_name", "email", "phone_office", "phone_mobile");

		//construct xml
		$data = "<?xml version='1.0' encoding='UTF-8'?>\n<$xml xmlns='http://www.blinksale.com/api'>\n";
		foreach($fields as $f)	//creates tags from fields above ex: <name>someinfo</name>
			eval("\$data .= \"\t<$f>\$".$f."</$f>\n\";");
		$data .= "</$xml>";

		//process
		$result = $this->process("UPDATE", "clients/$client_id/people", $id, $data);
		
		$res = new SimpleXMLElement($result);
		$list = (string)substr($res['uri'], strrpos($res['uri'], "/") +1);
		return $list;
	}

	public function people_get($client_id, $id = 0)  {
		$xml = "people";
		$xml_sub = "person";
		$field = "email";
		if ($id == 0)	$result = $this->process("SELECT", "clients/$client_id/".$xml);
		else			$result = $this->process("SELECT", "clients/$client_id/".$xml, $id);

		$res = new SimpleXMLElement($result);
		foreach ($res->$xml_sub as $node)	{
			$list[$c][0]	= (string)substr($node['uri'], strrpos($node['uri'], "/") +1);
			$list[$c++][1]	= (string)$node->$field;
		}
		return $list;
	}

	public function people_delete($client_id, $id) {
		$xml = "people";
		$result = $this->process("DELETE", "clients/$client_id/".$xml, $id);
		return $result;
	}

	///////////////////
	//  Invoice
	///////////////////
	public function invoice_new($client_id, $number, $date, $terms, $currency, $tax, $lines) {
		$client = "http://".$this->MACHINE_ID.".blinksale.com/clients/".$client_id;
		$xml = "invoice";
		$fields = array("client", "number", "date", "terms", "currency", "tax", "draft");
		$line_fields = array("name", "quantity", "units", "unit_price", "taxed");

		//construct xml
		$data = "<?xml version='1.0' encoding='UTF-8'?>\n<$xml xmlns='http://www.blinksale.com/api'>\n";
		foreach($fields as $f)	//creates tags from fields above ex: <name>someinfo</name>
			eval("\$data .= \"\t<$f>\$".$f."</$f>\n\";");
		$data .= "\t<lines>\n";
		foreach($lines as $line)	{
			$data .= "\t\t<line>\n";
			foreach($line_fields as $l)
				eval("\$data .= \"\t\t\t<$l>".$line[$l]."</$l>\n\";");
			$data .= "\t\t</line>\n";
		}
		$data .= "\t</lines>\n";
		$data .= "</$xml>";

		//process
		$result = $this->process("INSERT",$xml."s", 0, $data);

		$res = new SimpleXMLElement($result);
		$list = (string)substr($res['uri'], strrpos($res['uri'], "/") +1);
		return $list;
	}

	public function invoice_update($client_id, $id, $number = "", $date = "", $terms = "", $currency = "", $tax = "", $lines = "") {
		$client = "http://".$this->MACHINE_ID.".blinksale.com/clients/".$client_id;
		$xml = "invoice";
		$fields = array("client", "number", "date", "terms", "currency", "tax", "status");
		$line_fields = array("name", "quantity", "units", "unit_price", "taxed");

		//construct xml
		$data = "<?xml version='1.0' encoding='UTF-8'?>\n<$xml xmlns='http://www.blinksale.com/api'>\n";
		foreach($fields as $f)	{//creates tags from fields above ex: <name>someinfo</name>
			$cmd = "if (\$".$f." != \"\") {\$data .= \"<$f>\$".$f."</$f>\";}";
			eval($cmd);
		}
		if ($lines != "")	{
			$data .= "\t<lines>\n";
			foreach($lines as $line)	{
				$data .= "\t\t<line>\n";
				foreach($line_fields as $l)
					eval("\$data .= \"\t\t\t<$l>".$line[$l]."</$l>\n\";");
				$data .= "\t\t</line>\n";
			}
			$data .= "\t</lines>\n";
		}
		$data .= "</$xml>";

		//process
		$result = $this->process("UPDATE",$xml."s", $id, $data);
		return $result;
	}

	public function invoice_get($id = 0)  {
		$xml = "invoice";
		$xml_sub = $xml;
		$field = "number";
		if ($id == 0)	$result = $this->process("SELECT", $xml."s");
		else			$result = $this->process("SELECT", $xml."s", $id);
		return $result;
		$res = new SimpleXMLElement($result);
		foreach ($res->$xml_sub as $node)	{
			$list[$c][0]	= (string)substr($node['uri'], strrpos($node['uri'], "/") +1);
			$list[$c++][1]	= (string)$node->$field;
		}
		return $list;
	}

	public function invoice_delete($id) {
		$xml = "invoice";
		$result = $this->process("DELETE", $xml."s", $id);
		return $result;
	}

	///////////////////
	//  Payments
	///////////////////
	public function payment_new($invoice_id, $amount, $date, $payment_method = "", $payment_reference = "") {
		$xml = "payment";
		$fields = array("amount", "date", "payment_method", "payment_reference");

		//construct xml
		$data = "<?xml version='1.0' encoding='UTF-8'?>\n<$xml xmlns='http://www.blinksale.com/api'>\n";
		foreach($fields as $f)	//creates tags from fields above ex: <name>someinfo</name>
			eval("\$data .= \"\t<$f>\$".$f."</$f>\n\";");
		$data .= "</$xml>";

		//process
		$result = $this->process("INSERT", "invoices/$invoice_id/payments", 0, $data);
		return $result;
	}

	public function payment_get($invoice_id)  {
		$xml = "payment";
		$xml_sub = $xml;
		$field = "amount";
		if ($id == 0)	$result = $this->process("SELECT", "invoices/$invoice_id/".$xml."s");
		else			$result = $this->process("SELECT", "invoices/$invoice_id/".$xml."s", $id);
	
		$res = new SimpleXMLElement($result);
		foreach ($res->$xml_sub as $node)	{
			$list[$c][0]	= (string)substr($node['uri'], strrpos($node['uri'], "/") +1);
			$list[$c++][1]	= (string)$node->$field;
		}
		return $list;
	}

	///////////////////
	//  Deliveries
	///////////////////
	public function delivery_new($invoice_id, $client_id, $people_id, $body = "") {	//$people_id may be array of ints or just an int
		$xml = "delivery";
		if (!is_array($people_id))	$people_ids[] = $people_id;	else $people_ids = $people_id;//make array if it isn't

		//construct xml
		$data = "<?xml version='1.0' encoding='UTF-8'?>\n<$xml xmlns='http://www.blinksale.com/api'>\n";
		if ($body != "")	$data .= "<body>$body</body>";
		foreach($people_ids as $person_id)
			$data .= "<recipient>https://".$this->MACHINE_ID.".blinksale.com/clients/$client_id/people/$person_id</recipient>";
		$data .= "</$xml>";

		//process
		$result = $this->process("INSERT", "invoices/$invoice_id/deliveries", 0, $data);
		return $result;
	}

	public function delivery_get($invoice_id)  {
		$xml = "delivery";
		$xml_sub = $xml;
		$field = "";
		if ($id == 0)	$result = $this->process("SELECT", "invoices/$invoice_id/deliveries");
		else			$result = $this->process("SELECT", "invoices/$invoice_id/deliveries", $id);

		$res = new SimpleXMLElement($result);
		foreach ($res->$xml_sub as $node)	{
			$list[$c][0]	= (string)substr($node['uri'], strrpos($node['uri'], "/") +1);
			$list[$c++][1]	= (string)$node->recipient['email'];
		}
		return $list;
	}
}
?>
