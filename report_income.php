<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("fusion/FusionCharts.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");
        
    if (!hasAccess("REP_PROJ_TYPE_COM"))
        header("Location: noaccess.php");

    //  Set Report Generated Status
    $generated = "0";

    function getMaxLimit($date_from, $date_to, $total, $view, $projectTypes, $financialTarget, $accordingTo)  {
        $max = 0;

        $totalPT = 0;
        $totalFT = $financialTarget;

        if (is_array($projectTypes)) {
            foreach ($projectTypes as $projectType) {
                for ($i = 0; $i < $total; $i++) {
                    $invoiced = q("SELECT SUM(li.consulting + li.diverse_income + (li.consulting * (li.vat / 100)) + (li.diverse_income * (li.vat / 100))) ".
                                    "FROM (LoadInvoices AS li INNER JOIN Project AS p ON li.project_id = p.id) ".
                                    "WHERE p.type_id = '".$projectType[0]."' AND li.$accordingTo >= '".getNextMonthStart($date_from, $i)."' ".
                                    "AND li.$accordingTo < '".getNextMonthStart($date_from, ($i + 1))."' ".
                                    "AND li.company_id = '".$_SESSION["company_id"]."'");

                    if ($invoiced == "")
                        $invoiced = 0;

                    if ($max < $invoiced)
                        $max = $invoiced;
                }
            }
        }

        if ($max < $totalFT)
            $max = $totalFT;

        $max = ceil($max / 100000) * 100000;

        return $max;
    }

    function createDataSet($date_from, $date_to, $total, $view, $projectTypes, $financialTarget, $accordingTo) {
        $dataSetString = "";

        if (is_array($projectTypes)) {
            foreach ($projectTypes as $projectType) {
                $dataSetString	.= "<dataset seriesName='".$projectType[1]."'>";

                for ($i = 0; $i < $total; $i++) {
                    $invoiced = q("SELECT SUM(li.consulting + li.diverse_income + (li.consulting * (li.vat / 100)) + (li.diverse_income * (li.vat / 100))) ".
                                    "FROM (LoadInvoices AS li INNER JOIN Project AS p ON li.project_id = p.id) ".
                                    "WHERE p.type_id = '".$projectType[0]."' AND li.$accordingTo >= '".getNextMonthStart($date_from, $i)."' ".
                                    "AND li.$accordingTo < '".getNextMonthStart($date_from, ($i + 1))."' ".
                                    "AND li.company_id = '".$_SESSION["company_id"]."'");

                    if ($invoiced == "")
                        $invoiced = 0;

                    $dataSetString .= "<set value='".number_format($invoiced, 2, ".", "")."'/>";
                }

                $dataSetString .= "</dataset>";
            }
        }

        if ($financialTarget != "")     {
            $dataSetString .= "<dataset seriesName='Financial Target' showValues='1' color='D2D2D2' parentYAxis='S' includeInLegend='0'>";
                for ($i = 0; $i < $total; $i++) {
                    $dataSetString .= "<set value='".number_format(($financialTarget), 2, ".", "")."'/>";
                }
            $dataSetString .= "</dataset>";
        }

        return $dataSetString;
    }

    function createXMLString($date_from, $date_to, $total, $view, $projectTypes, $financialTarget, $accordingTo) {
        $xmlString = "";

        $maxLimit = getMaxLimit($date_from, $date_to, $total, $view, $projectTypes, $financialTarget, $accordingTo);

        $xmlString .= "<chart caption='Cumulative Income (Consulting %2B Diverse Income) Report' shownames='1' showvalues='0' numberPrefix='".$_SESSION["currency"]."' decimals='2' ".
                        "overlapColumns='0' formatNumberScale='0' formatNumber='0' labelDisplay='rotate' PYAxisMinValue='0' PYAxisMaxValue='".$maxLimit."' SYAxisMinValue='0' SYAxisMaxValue='".$maxLimit."' ".
                        "showSecondaryLimits='0' showDivLineSecondaryValue='0' ".fusionChart().">";
        //  Create Categories
        $xmlString .= "<categories>";

        for ($i = 0; $i < $total; $i++)	{
            $xmlString .= "<category label='".getNextMonth($date_from, $i)."' />";
        }

        $xmlString .= "</categories>";

        //  Create Dataset - Per Employee
        $xmlString .= createDataSet($date_from, $date_to, $total, $view, $projectTypes, $financialTarget, $accordingTo);

        $xmlString .= "</chart>";

        return $xmlString;
    }

    //  Create Display Information Function
    if (isset($_POST["save"]) && $_POST["save"] === "1") {
        $errorMessage = "";

        $projectType = $_POST["projectType"];
        $financialTarget = addslashes(strip_tags($_POST["financialTarget"]));
        $accordingTo = addslashes(strip_tags($_POST["accordingTo"]));
        $date_from = addslashes(strip_tags($_POST["date_from"]));
        $date_to = addslashes(strip_tags($_POST["date_to"]));
        $displayString = "";
        $tableString = "";

        $date_from = getMonthStart($date_from);
        $date_to = getMonthEnd($date_to);

        $total = getMonthPeriod($date_from, $date_to);

        if ($financialTarget != "")     $financialTarget = $financialTarget / $total;

        if ($projectType == "all")      {
            $projectTypes = q("SELECT id, type FROM ProjectTypes WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY type");
            $projectTypes[] = array("0","Shared Projects");
            $projectTypes = sortArrayByColumn($projectTypes, 1);
        }
        else if ($projectType == "0")
            $projectTypes = array("0","Shared Projects");
        else
            $projectTypes = q("SELECT id, type FROM ProjectTypes WHERE company_id = '".$_SESSION["company_id"]."' AND id = '".$projectType."' ORDER BY type");
        
//        if($projectType == 0)
//            $projectTypes = q("SELECT id, type FROM ProjectTypes WHERE company_id = '0' ORDER BY type");

        $graph = createXMLString($date_from, $date_to, $total, $view, $projectTypes, $financialTarget, $accordingTo);
        $displayString = renderChart("fusion/StackedColumn3DLineDY.swf", "", $graph, "multigraph", 800, 400, false, false);

        if ($displayString != "") {
            $displayString = "<tr><td class='on-table-clear' colspan='".($total + 1)."'><a>Cumulative Income (Consulting + Diverse Income) Report</a>".
                                "</td></tr><td colspan='".($total + 1)."'><br/>".$displayString."<br/></td></tr>".$tableString;

            $generated = "1";
        }
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("5", "reports");

    if ($errorMessage != "")
    {
        echo "<p align='center' style='padding:0px;'><strong><font class='on-validate-error'>$errorMessage</font></strong></p>";
        echo "<br/>";
    }
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    jQuery(function(){
	jQuery('#date_from').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_from').val(),
		current: jQuery('#date_from').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_from').val()))
			_date = new Date();
			else _date = jQuery('#date_from').val();
			jQuery('#date_from').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_from').val(formated);
			jQuery('#date_from').DatePickerHide();
		}
	});
    })

    jQuery(function(){
	jQuery('#date_to').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_to').val(),
		current: jQuery('#date_to').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_to').val()))
			_date = new Date();
			else _date = jQuery('#date_to').val();
			jQuery('#date_to').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_to').val(formated);
			jQuery('#date_to').DatePickerHide();
		}
	});
    })

    function check()
    {
        var valid                                                       = 1;

        //  Check That Project Type Is Selected
        if (document.forms["report_time_comparison"].projectType.value == "null")
        {
            ShowLayer("projectTypeDiv", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("projectTypeDiv", "none");

        //  Check That Financial Target Amount Is Valid & Entered
        if (!validation("currency", document.forms["report_time_comparison"].financialTarget.value))      {
            ShowLayer("financialTargetDiv", "block");
            valid = 0;
        }
        else
            ShowLayer("financialTargetDiv", "none");

        //  Check That Date From Is Entered
        if (document.forms["report_time_comparison"].date_from.value == "")
        {
            ShowLayer("dateFrom", "block");
            valid                                                       = 0;
        }
        //  Check That Entered Date From Is Valid
        else
        {
            ShowLayer("dateFrom", "none");

            if (!validation("date", document.forms["report_time_comparison"].date_from.value))
            {
                ShowLayer("dateFromDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("dateFromDiv", "none");
        }

        //  Check That Date To Is Entered
        if (document.forms["report_time_comparison"].date_to.value == "")
        {
            ShowLayer("dateTo", "block");
            valid                                                       = 0;
        }
        //  Check That Entered Date To Is Valid
        else
        {
            ShowLayer("dateTo", "none");

            if (!validation("date", document.forms["report_time_comparison"].date_to.value))
            {
                ShowLayer("dateToDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("dateToDiv", "none");
        }

        if (valid == 1)
        {
            document.forms["report_time_comparison"].save.value         = 1;
            document.forms["report_time_comparison"].submit();
        }
    }
</script>
<?php
    $projectTypes = q("SELECT id, type FROM ProjectTypes WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY type");
    $projectTypes[] = array("0","Shared Projects");
    $projectTypes = sortArrayByColumn($projectTypes, 1);
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="report_time_comparison">
                    <div id="" style="<?php if ($generated == "1") echo "display: none;"; else echo "display: block"; ?>">
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td class="centerdata">
                                    <h6>Cumulative Income Report</h6>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Project Type:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="projectType">
                                        <option value="all">All Project Types</option>
                                        <?php
                                            if (is_array($projectTypes))
                                                foreach ($projectTypes as $projectType)
                                                    if ($_POST["projectType"] == $projectType[0])
                                                        echo "<option value='".$projectType[0]."' selected>".$projectType[1]."</option>";
                                                    else
                                                        echo "<option value='".$projectType[0]."'>".$projectType[1]."</option>";
                                        ?>
                                    </select>
                                    <div id="projectTypeDiv" style="display: none;"><font class="on-validate-error">* Project type must be selected</font></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Financial Target for Selected Period:
                                </td>
                                <td width="50%">
                                    <input class="on-field" id="financialTarget" name="financialTarget" type="text" style="text-align:right;" value="<?php echo "".$_POST["financialTarget"]; ?>">
                                    <div id="financialTargetDiv" style="display: none;"><font class="on-validate-error">* Entered amount should be numeric, eg. 123.45</font></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    According To:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="accordingTo">
                                        <option value="create_date">Date Created</option>
                                        <option value="due_date">Due Date</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Date From:
                                </td>
                                <td width="50%">
                                    <input class="on-field-date" id="date_from" name="date_from" type="text" style="text-align:right;" value="<?php
                                        if ($_POST["date_from"] != "") echo "".$_POST["date_from"]; else echo "".getMonthStart($today); ?>">
                                    <div id="dateFrom" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                    <div id="dateFromDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Date To:
                                </td>
                                <td width="50%">
                                    <input class="on-field-date" id="date_to" name="date_to" type="text" style="text-align:right;" value="<?php
                                        if ($_POST["date_to"] != "") echo "".$_POST["date_to"]; else echo "".getMonthEnd($today); ?>">
                                    <div id="dateTo" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                    <div id="dateToDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <input name="btnGenerate" onClick="check();" type="button" value="Generate Report">
                        <input method="post" name="save" type="hidden" value="0" />
                    </div>
                </form>
                <div id="report" style="<?php if ($generated == "1") echo "display: block;"; else echo "display: none"; ?>">
                    <?php
                        echo "<table class='on-table-center on-table'>";
                            echo "".$displayString;
                            echo  "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        echo "</table>";
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
