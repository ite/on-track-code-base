<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("include/sajax.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    if (!hasAccess("REP_SUMMARY"))
        header("Location: noaccess.php");

    // Class for table background color
    $colorClass = "OnTrack_#_";

    ///////////////////////////
    //  Sajax
    function getProjects($status, $type) {
        if ($status == "all")   $status = " AND p.completed IN (0,1)";
        else                    $status = " AND p.completed = '".$status."'";

        if ($type == "null")
            $projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p
                            INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id)
                            WHERE cot.company_id = '".$_SESSION["company_id"]."'
                            AND p.deleted = '0' $status ORDER BY UPPER(p.name)");
        else
            $projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p
                            INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id)
                            WHERE cot.company_id = '".$_SESSION["company_id"]."'
                            AND p.deleted = '0'
                            AND p.type_id='$type' $status ORDER BY UPPER(p.name)");

        return $projects;
    }

    $sajax_request_type                                                 = "GET";
    sajax_init();
    sajax_export("getProjects");
    sajax_handle_client_request();
    ///////////////////////////

    //  Set Report Generated Status
    $generated                                                          = "0";

    function createRow($employee_id, $project_id, $project_Type, $date_from, $date_to, $where,$table1,$table2,$where1,$where2)
    {
        GLOBAL $myData;
        GLOBAL $row;

        $rowString = "";
        $total = 0;

        if ($project_id == "all") {
            if ($project_Type == "both") {
                $consulting = q("SELECT SUM(ts.time_spent * ts.rate) FROM ($table1 AS ts INNER JOIN Project AS p ".
                                "ON ts.project_id = p.id) WHERE ts.user_id = '$employee_id' ".
                                "AND ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                "AND ts.company_id = '".$_SESSION["company_id"]."' AND p.company_id = '".$_SESSION["company_id"]."' AND ts.projectType = 'CP' $where $where1");
                $expenses = q("SELECT SUM(es.expense) FROM ($table2 AS es INNER JOIN Project AS p ".
                                "ON es.project_id = p.id) WHERE es.user_id = '$employee_id' ".
                                "AND es.date >= '$date_from' AND es.date <= '$date_to' ".
                                "AND es.company_id = '".$_SESSION["company_id"]."' AND p.company_id = '".$_SESSION["company_id"]."' AND es.projectType = 'CP' $where $where2");
            }
            else if ($project_Type == "invoicable") {
                $consulting = q("SELECT SUM(ts.time_spent * ts.rate) FROM ($table1 AS ts INNER JOIN Project AS p ".
                                "ON ts.project_id = p.id INNER JOIN companiesOnTeam AS cot ON p.id = cot.project_id) WHERE ts.user_id = '$employee_id' ".
                                "AND ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                "AND ts.company_id = '".$_SESSION["company_id"]."' AND p.company_id = '".$_SESSION["company_id"]."' AND cot.total_budget > 0 AND ts.projectType = 'CP' $where $where1");
                $expenses = q("SELECT SUM(es.expense) FROM ($table2 AS es INNER JOIN Project AS p ".
                                "ON es.project_id = p.id INNER JOIN companiesOnTeam AS cot ON p.id = cot.project_id) WHERE es.user_id = '$employee_id' ".
                                "AND es.date >= '$date_from' AND es.date <= '$date_to' ".
                                "AND es.company_id = '".$_SESSION["company_id"]."' AND p.company_id = '".$_SESSION["company_id"]."' AND cot.total_budget > 0 AND es.projectType = 'CP' $where $where2");
            }
            else {
                $consulting = q("SELECT SUM(ts.time_spent * ts.rate) FROM ($table1 AS ts INNER JOIN Project AS p ".
                                "ON ts.project_id = p.id INNER JOIN companiesOnTeam AS cot ON p.id = cot.project_id) WHERE ts.user_id = '$employee_id' ".
                                "AND ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                "AND ts.company_id = '".$_SESSION["company_id"]."' AND p.company_id = '".$_SESSION["company_id"]."' AND cot.total_budget = '' AND ts.projectType = 'CP' $where $where1");
                $expenses = q("SELECT SUM(es.expense) FROM ($table2 AS es INNER JOIN Project AS p ".
                                "ON es.project_id = p.id INNER JOIN companiesOnTeam AS cot ON p.id = cot.project_id) WHERE es.user_id = '$employee_id' ".
                                "AND es.date >= '$date_from' AND es.date <= '$date_to' ".
                                "AND es.company_id = '".$_SESSION["company_id"]."' AND p.company_id = '".$_SESSION["company_id"]."' AND cot.total_budget = '' AND es.projectType = 'CP' $where $where2");
            }
        }
        else {
            $consulting = q("SELECT SUM(ts.time_spent * ts.rate) FROM ($table1 AS ts INNER JOIN Project AS p ".
                            "ON ts.project_id = p.id) WHERE ts.user_id = '$employee_id' ".
                            "AND ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                            "AND ts.company_id = '".$_SESSION["company_id"]."' AND p.id = '$project_id' $where $where1");
            $expenses = q("SELECT SUM(es.expense) FROM ($table2 AS es INNER JOIN Project AS p ".
                            "ON es.project_id = p.id) WHERE es.user_id = '$employee_id' ".
                            "AND es.date >= '$date_from' AND es.date <= '$date_to' ".
                            "AND es.company_id = '".$_SESSION["company_id"]."' AND p.id = '$project_id' $where $where2");
        }

        $total = $consulting + $expenses;

        if ($consulting){
            $rowString .= "<td class='rightdata'>".number_format($consulting, 2, ".", "")."</td>";
            $myData[$row][1] = number_format($consulting, 2, ".", "");
        }else{
            $rowString .= "<td class='rightdata'>-</td>";
            $myData[$row][1] = "-";
        }

        if ($expenses){
            $rowString .= "<td class='rightdata'>".number_format($expenses, 2, ".", "")."</td>";
            $myData[$row][2] = number_format($expenses, 2, ".", "");
        }else{
            $rowString .= "<td class='rightdata'>-</td>";
            $myData[$row][2] = "-";
        }

        if ($total != 0){
            $rowString .= "<td class='rightdata'>".number_format($total, 2, ".", "")."</td>";
            $myData[$row][3] = number_format($total, 2, ".", "");
        }else{
            $rowString .= "<td class='rightdata'>-</td>";
            $myData[$row][3] = "";
        }

        return $rowString;
    }

    //  Create Display Information Function
    if (isset($_POST["save"]) && $_POST["save"] === "1") {
        $errorMessage = "";

        $reportOn = $_POST["reportOn"];
        $projectStatus = $_POST["projectStatus"];

        $project_type_id = $_POST["projectTypeID"];
        $project_id = $_POST["project"];
        $date_from = addslashes(strip_tags($_POST["date_from"]));
        $date_to = addslashes(strip_tags($_POST["date_to"]));
        $displayString = "";
        $where = "";

        if ($project_type_id != "null" && is_numeric($project_type_id))
            $where .= "AND p.type_id = '$project_type_id' ";

        $table1 = ($reportOn == "approved") ? "ApprovedTime" : "TimeSheet";
        $where1 = ($reportOn == "approved") ? "AND ts.status = '2' " : "";
        $table2 = ($reportOn == "approved") ? "ApprovedExpense" : "ExpenseSheet";
        $where2 = ($reportOn == "approved") ? "AND es.status = '2' " : "";

        if ($projectStatus == "all")    $projectStatus = " AND p.completed IN (0,1) ";
        else                            $projectStatus = " AND p.completed = '".$projectStatus."' ";

        $where .= $projectStatus;

        ///////////////////////////
        //  Get Info From Tables
        ///////////////////////////
        if ($project_id == "all") {
            $project_name = "All Projects (Shared Projects not included)";
            $project_Type = $_POST["projectType"];
        }
        else {
            $project_name = q("SELECT name From Project WHERE id = '$project_id'");
            $project_Type = "";
        }

        $row                                                                    = 0;
        $myData;

        $excelheadings[$row][] = "";
        $excelheadings[$row][] = "";
        $excelheadings[$row][] = "";
        $excelheadings[$row][] = "";
            $row++;

        $exceldata[$row][] = "Summary Report: ".$date_from."  -  ".$date_to;
        $exceldata[$row][] = "";
        $exceldata[$row][] = "";
        $exceldata[$row][] = "";
            $row++;

        $exceldata[$row][] = "Project: ".$project_name;
        $exceldata[$row][] = "";
        $exceldata[$row][] = "";
        $exceldata[$row][] = "";
            $row++;

        $exceldata[$row][] = $colorClass."Employee Name";
        $exceldata[$row][] = $colorClass."Consulting";
        $exceldata[$row][] = $colorClass."Expenses";
        $exceldata[$row][] = $colorClass."Total";
            $row++;

		if ($project_id == "all")
                    $employees = q("SELECT DISTINCT(e.id), e.frstname, e.lstname FROM (Project AS p INNER JOIN Project_User AS pu ON p.id = pu.project_id ".
                                    "INNER JOIN Employee AS e ON pu.user_id = e.id) ".
                                    "WHERE pu.company_id = '".$_SESSION["company_id"]."' $projectStatus ".
                                    "ORDER BY e.lstname, frstname");
		else
                    $employees = q("SELECT DISTINCT(e.id), e.frstname, e.lstname FROM (Project AS p INNER JOIN Project_User AS pu ON p.id = pu.project_id ".
                                    "INNER JOIN Employee AS e ON pu.user_id = e.id) ".
                                    "WHERE pu.company_id = '".$_SESSION["company_id"]."' $projectStatus ".
                                    "AND pu.project_id = '$project_id' ".
                                    "ORDER BY e.lstname, frstname");


        ///////////////////////////
        //  Create Table Headers
        $headers = "<tr>
                                <th>Employee Name</th>
                                <th>Consulting</th>
                                <th>Expenses</th>
                                <th>Total<i>(".$_SESSION["currency"].")</i></th>
                            </tr>";

        if (is_array($employees))
            foreach ($employees as $employee){
                $exceldata[$row][0] = $employee[1]." ".$employee[2];
                $displayString .= "<tr><td>".$employee[1]." ".$employee[2]."</td>".
                                    createRow($employee[0], $project_id, $project_Type, $date_from, $date_to, $where,$table1,$table2,$where1,$where2)."</tr>";
                $exceldata[$row][1] = $myData[$row][1];
                $exceldata[$row][2] = $myData[$row][2];
                $exceldata[$row][3] = $myData[$row][3];
                $row++;
            }


        if ($project_id == "all") {
            if ($project_Type == "both") {
                $total_consulting = q("SELECT SUM(ts.time_spent * ts.rate) FROM ($table1 AS ts INNER JOIN Project AS p ".
                                        "ON ts.project_id = p.id) WHERE ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                        "AND ts.company_id = '".$_SESSION["company_id"]."' AND p.company_id = '".$_SESSION["company_id"]."' AND ts.projectType = 'CP' $where $where1");
                $total_expenses = q("SELECT SUM(es.expense) FROM ($table2 AS es INNER JOIN Project AS p ".
                                        "ON es.project_id = p.id) WHERE es.date >= '$date_from' AND es.date <= '$date_to' ".
                                        "AND es.company_id = '".$_SESSION["company_id"]."' AND p.company_id = '".$_SESSION["company_id"]."' AND es.projectType = 'CP' $where $where2");
            }
            else if ($project_Type == "invoicable") {
                $total_consulting = q("SELECT SUM(ts.time_spent * ts.rate) FROM ($table1 AS ts INNER JOIN Project AS p ".
                                        "ON ts.project_id = p.id INNER JOIN companiesOnTeam AS cot ON p.id = cot.project_id) WHERE ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                        "AND ts.company_id = '".$_SESSION["company_id"]."' AND p.company_id = '".$_SESSION["company_id"]."' AND cot.total_budget > 0 AND ts.projectType = 'CP' $where $where1");
                $total_expenses = q("SELECT SUM(es.expense) FROM ($table2 AS es INNER JOIN Project AS p ".
                                        "ON es.project_id = p.id INNER JOIN companiesOnTeam AS cot ON p.id = cot.project_id) WHERE es.date >= '$date_from' AND es.date <= '$date_to' ".
                                        "AND es.company_id = '".$_SESSION["company_id"]."' AND p.company_id = '".$_SESSION["company_id"]."' AND cot.total_budget > 0 AND es.projectType = 'CP' $where $where2");
            }
            else {
                $total_consulting = q("SELECT SUM(ts.time_spent * ts.rate) FROM ($table1 AS ts INNER JOIN Project AS p ".
                                        "ON ts.project_id = p.id INNER JOIN companiesOnTeam AS cot ON p.id = cot.project_id) WHERE ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                        "AND ts.company_id = '".$_SESSION["company_id"]."' AND p.company_id = '".$_SESSION["company_id"]."' AND cot.total_budget = '' AND ts.projectType = 'CP' $where $where1");
                $total_expenses = q("SELECT SUM(es.expense) FROM ($table2 AS es INNER JOIN Project AS p ".
                                        "ON es.project_id = p.id INNER JOIN companiesOnTeam AS cot ON p.id = cot.project_id) WHERE es.date >= '$date_from' AND es.date <= '$date_to' ".
                                        "AND es.company_id = '".$_SESSION["company_id"]."' AND p.company_id = '".$_SESSION["company_id"]."' AND cot.total_budget = '' AND es.projectType = 'CP' $where $where2");
            }
        }
        else
        {
            $total_consulting = q("SELECT SUM(ts.time_spent * ts.rate) FROM ($table1 AS ts INNER JOIN Project AS p ".
                                    "ON ts.project_id = p.id) WHERE ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                    "AND ts.company_id = '".$_SESSION["company_id"]."' AND p.id = '$project_id' $where $where1");
            $total_expenses = q("SELECT SUM(es.expense) FROM ($table2 AS es INNER JOIN Project AS p ".
                                    "ON es.project_id = p.id) WHERE es.date >= '$date_from' AND es.date <= '$date_to' ".
                                    "AND es.company_id = '".$_SESSION["company_id"]."' AND p.id = '$project_id' $where $where2");
        }

        $displayString .= "<tr><td class='on-table-total'>Grand Total <i>(".$_SESSION["currency"].")</i>:</td>";

        $exceldata[$row][0] = "Grand Total(".$_SESSION["currency"].")";         // 1

        if ($total_consulting){
            $displayString .= "<td class='on-table-total'>".number_format($total_consulting, 2, ".", "")."</td>";
            $exceldata[$row][1] = number_format($total_consulting, 2, ".", "");
        }else{
            $displayString .= "<td class='on-table-total'>"."-</td>";
            $exceldata[$row][1] ="-";
        }

        if ($total_expenses){
            $displayString .= "<td class='on-table-total'>".number_format($total_expenses, 2, ".", "")."</td>";
            $exceldata[$row][2] = number_format($total_expenses, 2, ".", "");
        }else{
            $displayString .= "<td class='on-table-total'>". "-</td>";
            $exceldata[$row][2] = "-";
        }

        if (($total_consulting + $total_expenses) != 0){
            $displayString .= "<td class='on-table-total'>".
                                number_format(($total_consulting + $total_expenses), 2, ".", "")."</td></tr>";
            $exceldata[$row][3] = number_format(($total_consulting + $total_expenses), 2, ".", "");
        }else{
            $displayString .= "<td class='on-table-total'>"."-</td></tr>";
            $exceldata[$row][3] = "-";
        }

        $displayString = "<tr>
                                        <td class='on-table-clear' colspan='4' ><a>Summary Report: ".$date_from." - ".$date_to."</a></td>
                                    </tr>
                                    <tr>
                                        <td class='on-table-clear' colspan='4'>Project: ".$project_name."</td>
                                    </tr>".$headers.$displayString;

        if ($displayString != "")
            $generated                                                  = "1";
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("3", "reports");
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    jQuery(function(){
	jQuery('#date_from').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_from').val(),
		current: jQuery('#date_from').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_from').val()))
			_date = new Date();
			else _date = jQuery('#date_from').val();
			jQuery('#date_from').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_from').val(formated);
			jQuery('#date_from').DatePickerHide();
		}
	});
    })

    jQuery(function(){
	jQuery('#date_to').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_to').val(),
		current: jQuery('#date_to').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_to').val()))
			_date = new Date();
			else _date = jQuery('#date_to').val();
			jQuery('#date_to').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_to').val(formated);
			jQuery('#date_to').DatePickerHide();
		}
	});
    })

    function check()
    {
        var valid                                                       = 1;

        //  Check That Employee Is Selected
        if (document.forms["report_summary"].project.value == "null")
        {
            ShowLayer("projectDiv", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("projectDiv", "none");

        //  Check That Date From Is Entered
        if (document.forms["report_summary"].date_from.value == "")
        {
            ShowLayer("dateFrom", "block");
            valid                                                       = 0;
        }
        //  Check That Entered Date From Is Valid
        else
        {
            ShowLayer("dateFrom", "none");

            if (!validation("date", document.forms["report_summary"].date_from.value))
            {
                ShowLayer("dateFromDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("dateFromDiv", "none");
        }

        //  Check That Date To Is Entered
        if (document.forms["report_summary"].date_to.value == "")
        {
            ShowLayer("dateTo", "block");
            valid                                                       = 0;
        }
        //  Check That Entered Date To Is Valid
        else
        {
            ShowLayer("dateTo", "none");

            if (!validation("date", document.forms["report_summary"].date_to.value))
            {
                ShowLayer("dateToDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("dateToDiv", "none");
        }

        if (valid == 1)
        {
            document.forms["report_summary"].save.value                 = 1;
            document.forms["report_summary"].submit();
        }
    }

    function displayCheck(select)
    {
        if (select.value == "all")
            ShowLayer("projectTypeDiv", "block");
        else
            ShowLayer("projectTypeDiv", "none");
    }

    ///////////////////////////
    //  Sajax
    <?php sajax_show_javascript(); ?>

    function ClearOptions(OptionList) {
        for (x = OptionList.length; x >= 0; x--)
        {
            OptionList[x]                                               = null;
        }
    }

    function setProjects(data) {
        var c                                                           = 0;

        document.forms["report_summary"].project.options[(c++)] = new Option("--  Select A Project  --", "null");
        document.forms["report_summary"].project.options[(c++)] = new Option("All Projects", "all");

        for (var i in data)
            eval("document.forms['report_summary'].project.options["+(c++)+"] = new Option('" + data[i][1] + "', '" + data[i][0] + " ');");
    }

    function getProjects(select1,select2) {
        var status = eval("document.report_summary."+select1+".value;");
        var type = eval("document.report_summary."+select2+".value;");;

        ClearOptions(document.report_summary.project);
        x_getProjects(status, type, setProjects);
    }
    ///////////////////////////
</script>
<?php
	$n[0][0] = "0";	$n[0][1] = "Shared Projects";
    $projectTypes = q("SELECT id, type FROM ProjectTypes WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY type");
	if (!is_array($projectTypes))	$projectTypes = $n;
	else							$projectTypes = array_merge($n,$projectTypes);
    $projectTypes = sortArrayByColumn($projectTypes, 1);

    $projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) ".
		"WHERE cot.company_id = '".$_SESSION["company_id"]."' AND p.completed = '0' AND p.deleted = '0' ORDER BY UPPER(p.name)");
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="report_summary">
                    <div id="" style="<?php if ($generated == "1") echo "display: none;"; else echo "display: block"; ?>">
                        <table width="100%">
                            <tr>
                                <td class="centerdata">
                                    <h6>Summary Report</h6>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Report On:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="reportOn">
                                        <option value="normal">Actual Time</option>
                                        <option value="approved">Approved Time</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Project Status:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="projectStatus" onChange="getProjects('projectStatus','projectTypeID');">
                                        <option value="all">Completed & Uncompleted</option>
                                        <option value="0" selected>Uncompleted</option>
                                        <option value="1">Completed</option>
                                    </select>
                                </td>
                            </tr>
                            <tr><td colspan="100%"><br/></td></tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Project Type:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="projectTypeID" onChange="getProjects('projectStatus','projectTypeID');">
                                        <option value="null">All Project Types</option>
                                        <?php
                                            if (is_array($projectTypes))
                                                foreach ($projectTypes as $projectType)
                                                    if ($_POST["projectType"] == $projectType[0])
                                                        echo "<option value='".$projectType[0]."' selected>".$projectType[1]."</option>";
                                                    else
                                                        echo "<option value='".$projectType[0]."'>".$projectType[1]."</option>";
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Project:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" id="project" name="project" onChange="displayCheck(project);">
                                        <option value="null">--  Select A Project  --</option>
                                        <option value="all">All Projects</option>
                                        <?php
                                            if (is_array($projects))
                                                foreach ($projects as $project)
                                                    if (isset($_POST["project"]) && $_POST["project"] == $project[0])
                                                        echo "<option value='".$project[0]."' selected>".$project[1]."</option>";
                                                    else
                                                        echo "<option value='".$project[0]."'>".$project[1]."</option>";
                                        ?>
                                    </select>
                                    <div id="projectDiv" style="display: none;"><font class="on-validate-error">* Project must be selected</font></div>
                                </td>
                            </tr>
                        </table>
                        <div id="projectTypeDiv" style="display: none;">
                            <table cellpadding="0" cellspacing="2" width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Project Type:
                                    </td>
                                    <td class="on-description-left" width="50%">
                                        <input method="post" name="projectType" type="radio" value="both" checked><a>Invoicable & Non-Invoicable</a><br/>
                                        <input method="post" name="projectType" type="radio" value="invoicable"><a>Invoicable</a><br/>
                                        <input method="post" name="projectType" type="radio" value="non_invoicable"><a>Non-Invoicable</a><br/>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Date From:
                                </td>
                                <td width="50%">
                                    <input class="on-field-date" id="date_from" name="date_from" type="text" style="text-align:right;" value="<?php
                                        if ($_POST["date_from"] != "") echo "".$_POST["date_from"]; else echo "".getMonthStart($today); ?>">
                                    <div id="dateFrom" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                    <div id="dateFromDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Date To:
                                </td>
                                <td width="50%">
                                    <input class="on-field-date" id="date_to" name="date_to" type="text" style="text-align:right;" value="<?php
                                        if ($_POST["date_to"] != "") echo "".$_POST["date_to"]; else echo "".getMonthEnd($today); ?>">
                                    <div id="dateTo" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                    <div id="dateToDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <input name="btnGenerate" onClick="check();" type="button" value="Generate Report">
                        <input method="post" name="save" type="hidden" value="0" />
                    </div>
                </form>
                <div id="report" style="<?php if ($generated == "1") echo "display: block;"; else echo "display: none"; ?>">
                    <?php
                        echo "<table class='on-table-center on-table'>";
                            echo "".$displayString;
                             echo  "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        echo "</table>";
                        echo "<br/>";

                        ///////////////////////////
                        //  Set Export Information
                        $_SESSION["fileName"] = "Report Summary";
                        $_SESSION["fileData"] = array_merge($excelheadings, $exceldata);
                        ///////////////////////////

                        echo "<input name='btnExport' onClick=\"location.href='_export.php'\" type='button' value='Export to Excel' />";
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
