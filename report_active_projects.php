<?php
	session_start();

	include("_db.php");
	include("graphics.php");
	include("include/sajax.php");

	if (!$_SESSION["logged_in"] === true)
		header("Location: login.php");

	if (!isset($_SESSION["company_id"]))
		header("Location: home.php");

	if (!hasAccess("REP_ACTIVE_PROJ"))
		header("Location: noaccess.php");

	// Class for table background color
	$colorClass = "OnTrack_#_";

	///////////////////////////
	//  Sajax
	function getProjects($invoiceType,$projectType) {
		$where = "";

		if ($projectType != "null") $where .= " AND p.type_id = '".$projectType."'";
		if ($invoiceType == "invoiceable")	$where .= " AND cot.total_budget > 0 AND cot.total_budget != '' AND p.due_date != ''";
		if ($invoiceType == "non_invoiceable")	$where .= " AND (cot.total_budget = '' OR p.due_date = '')";

		$projects = q("SELECT DISTINCT(p.id), p.name
								FROM (Project AS p
									INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id)
								WHERE p.completed = '0'
									AND p.deleted = '0'
									$where
									AND cot.company_id = '".$_SESSION["company_id"]."'
								ORDER BY UPPER(p.name)");

		return $projects;
	}

	$sajax_request_type = "GET";
	sajax_init();
	sajax_export("getProjects");
	sajax_handle_client_request();
	///////////////////////////

	//  Print Header
	print_header();
	//  Print Menu
	print_menus("3", "reports");
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
	//  Sajax
	<?php sajax_show_javascript(); ?>

	function check()
	{
		var valid                                                       = 1;
		if (valid == 1) {
			document.forms["report_active_projects"].save.value                         = 1;
			document.forms["report_active_projects"].submit();
		}
	}

	///////////////////////// -- Sajax -- ///////////////////////////
	function ClearOptions(OptionList) {
		for (x = OptionList.length; x >= 0; x--)
		{
			OptionList[x]                                               = null;
		}
	}

	// Set Projects
	function setProjects(data) {
			var c                                                           = 0;
			document.report_active_projects.project.options[(c++)]                         = new Option("All Projects", "null");
			for (var i in data)
				eval("document.report_active_projects.project.options["+(c++)+"] = new Option('" + data[i][1] + "', '" + data[i][0] + "');");
	}

	//Get Projects
	function getProjects(invoiceType, projectType) {
		ClearOptions(document.report_active_projects.project);

		var invoiceTypeValue = "";

		for (var i = 0; i < invoiceType.length; i++) {
		  if (invoiceType[i].checked) {
				invoiceTypeValue = invoiceType[i].value;
		  }
		}

		x_getProjects(invoiceTypeValue,projectType.value, setProjects);
	}
	///////////////////////////////////////////////////////////////////
</script>
<?php
	$projectTypes = q("SELECT id, type FROM ProjectTypes WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY type");
	$projectTypes[] = array("0","Shared Projects");
	$projectTypes = sortArrayByColumn($projectTypes, 1);

	$users = q("SELECT e.id, CONCAT(e.lstname, ', ', e.frstname) FROM (Employee AS e
				INNER JOIN Company_Users AS cu ON e.id = cu.user_id)
				WHERE cu.company_id = '".$_SESSION["company_id"]."'
				AND e.email != 'admin' ORDER BY e.lstname, frstname");

	$reportOn = "normal";
	$invoiceable = "invoiceable";

	if (isset($_POST["reportOn"]))	$reportOn = addslashes(strip_tags($_POST["reportOn"]));
	if (isset($_POST["invoiceable"]))	$invoiceable = addslashes(strip_tags($_POST["invoiceable"]));

	$where = "";

	if (isset($_POST["projectType"]) && $_POST["projectType"] != "null") $where .= " AND p.type_id = '".$projectType."'";
	if ($invoiceable == "invoiceable")	$where .= " AND cot.total_budget > 0 AND cot.total_budget != '' AND p.due_date != ''";
	if ($invoiceable == "non_invoiceable")	$where .= " AND (cot.total_budget = '' OR p.due_date = '')";

	$projects = q("SELECT DISTINCT(p.id), p.name
							FROM (Project AS p
								INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id)
							WHERE p.completed = 0
								AND p.deleted = '0'
								$where
								AND cot.company_id = '".$_SESSION["company_id"]."'
							ORDER BY UPPER(p.name)");

	$where = "";
?>
<table width="100%">
	<tr height="380px">
		<td class="centerdata" valign="top">
			<form action="" method="post" name="report_active_projects">
				<table width="100%">
					<tr>
						<td class="centerdata">
							<h6>Project List Report</h6>
						</td>
					</tr>
				</table>
				<br/>
				<table width="100%">
					<tr>
						<td class="on-description" width="50%">
							Report On:
						</td>
						<td width="50%">
							<select class="on-field" method="post" name="reportOn">
								<option value="normal" <?php echo ($reportOn == "normal") ? "selected" : ""; ?>>Actual Time/Expenses</option>
								<option value="approved" <?php echo ($reportOn == "approved") ? "selected" : ""; ?>>Approved Time/Expenses</option>
							</select>
						</td>
					</tr>
					<tr>
						<td class="on-description" width="50%">
							Project Invoiceable Type:
						</td>
						<td class="on-description-left" width="50%">
							<input method="post" name="invoiceable" onChange="getProjects(invoiceable,projectType);" type="radio" value="both" <?php echo ($invoiceable == "both") ? "checked" : ""; ?>><a>Invoiceable/Active & Non-Invoicable Projects</a><br/>
							<input method="post" name="invoiceable" onChange="getProjects(invoiceable,projectType);" type="radio" value="invoiceable" <?php echo ($invoiceable == "invoiceable") ? "checked" : ""; ?>><a>Invoiceable/Active Projects</a><br/>
							<input method="post" name="invoiceable" onChange="getProjects(invoiceable,projectType);" type="radio" value="non_invoiceable" <?php echo ($invoiceable == "non_invoiceable") ? "checked" : ""; ?>><a>Non-Invoicable Projects</a><br/>
						</td>
					</tr>
				   <tr>
						<td class="on-description" width="50%">
							Select Project Type:
						</td>
						<td width="50%">
							<select class="on-field" method="post" name="projectType" onChange="getProjects(invoiceable,projectType);">
								<option value="null">All Project Types</option>
								<?php
									if (is_array($projectTypes))
										foreach ($projectTypes as $projectType)
											if ($_POST["projectType"] == $projectType[0])
												echo "<option value='".$projectType[0]."' selected>".$projectType[1]."</option>";
											else
												echo "<option value='".$projectType[0]."'>".$projectType[1]."</option>";
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td class="on-description" width="50%">
							Select User:
						</td>
						<td width="50%">
							<select class="on-field" method="post" name="user">
								<option value="null">All Users</option>
								<?php
									if (is_array($users))
										foreach ($users as $user)
											if (isset($_POST["user"]) && $_POST["user"] == $project[0])
												echo "<option value='".$user[0]."' selected>".$user[1]."</option>";
											else
												echo "<option value='".$user[0]."'>".$user[1]."</option>";
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td class="on-description" width="50%">
							Select Project:
						</td>
						<td width="50%">
							<select class="on-field" method="post" id="project" name="project">
								<option value="null">All Projects</option>
								<?php
									if (is_array($projects))
										foreach ($projects as $project)
											if (isset($_POST["project"]) && $_POST["project"] == $project[0])
												echo "<option value='".$project[0]."' selected>".$project[1]."</option>";
											else
												echo "<option value='".$project[0]."'>".$project[1]."</option>";
								?>
							</select>
						</td>
					</tr>
				</table>
				<input name="btnGenerate" onClick="check();" type="button" value="Generate Report">
				<input method="post" name="save" type="hidden" value="0" />
				<br/>
				<?php
					///////////////////////////
					//  Get Information
					///////////////////////////

					$where = "";
					$select = "";

					///// -- User -- /////
					if (isset($_POST["user"]) && $_POST["user"] != "null") {
						$where .= " AND pu.user_id = '".$_POST["user"]."' ";
						$select .= "INNER JOIN Project_User AS pu ON p.id = pu.project_id";
					}
					///// -- Project -- /////
					if (isset($_POST["project"]) && $_POST["project"] != "null") {
						$where .= " AND p.id = ".$_POST["project"]." ";
					}
					if (isset($_POST["projectType"]) && $_POST["projectType"] != "null") {   // Specific Project type
						$where .= " AND p.type_id = '".$_POST["projectType"]."'";
					}

					if ($invoiceable == "invoiceable")	$where .= " AND cot.total_budget > 0 AND cot.total_budget != '' AND p.due_date != ''";
					if ($invoiceable == "non_invoiceable")	$where .= " AND (cot.total_budget = '' OR p.due_date = '')";

					$projects = q("SELECT p.id, p.name, pt.type, p.due_date, cot.total_budget, cot.consulting, cot.expenses
											FROM (Project AS p
												INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
												LEFT JOIN ProjectTypes AS pt ON pt.id = p.type_id $select)
											WHERE p.completed = 0
												AND p.deleted = '0'
												$where
												AND cot.company_id = '".$_SESSION["company_id"]."'
											ORDER BY p.due_date, p.name");

					///////////////////////////
					//  Create Information String
					///////////////////////////
					//  Table Headers
					$string = "";
					$excelheadings[$row][] = "Report: Project List";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
						$row++;

					if($_POST['projectType'] == "null")
						$projectTypeHead = "All Types";
					else if($_POST['projectType'] == "0")
						$projectTypeHead = "Shared Projects";
					else if($_POST['projectType'] != "")
						$projectTypeHead = q("SELECT type FROM ProjectTypes WHERE id = ".$_POST['projectType']."");

					if($_POST['project'] == "null")
						$projectHead = "All Projects";
					else if($_POST['project'] != "")
						$projectHead = q("SELECT name FROM Project WHERE id = ".$_POST['project']."");

					if($_POST['user'] == "null")
						$username = "All Users";
					else if($_POST['user'] != "")
						$username = q("SELECT CONCAT(frstname, ' ', lstname) FROM Employee WHERE id = ".$_POST['user']."");

					$excelheadings[$row][] = "Project Type: $projectTypeHead";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
						$row++;
					$excelheadings[$row][] = "Project: $projectHead";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
						$row++;
					$excelheadings[$row][] = "User: $username";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
					$excelheadings[$row][] = "";
						$row++;

					$exceldata[$row][] = $colorClass."Project Name";
					$exceldata[$row][] = $colorClass."Project Type";
					$exceldata[$row][] = $colorClass."Due Date";
					$exceldata[$row][] = $colorClass."Total Budget";
					$exceldata[$row][] = $colorClass."Consulting Budget";
					$exceldata[$row][] = $colorClass."Booked Consulting Budget";
					$exceldata[$row][] = $colorClass."Remaining Consulting Budget";
					$exceldata[$row][] = $colorClass."Expense Budget";
					$exceldata[$row][] = $colorClass."Booked Expense Budget";
					$exceldata[$row][] = $colorClass."Remaining Expense Budget";
					$exceldata[$row][] = $colorClass."Total Remaining Budget";
						$row++;

					$headers = "";

					if($_POST['projectType'] || $_POST['project']){
					$headers                                        .=   "<tr>
																					<td class='on-table-clear' colspan='100%'><a>Project List</a></td>
																				</tr>
																				<tr>
																					<td class='on-table-clear' colspan='100%'>Project Types: $projectTypeHead</td>
																				</tr>
																				<tr>
																					<td class='on-table-clear' colspan='100%'>Project: $projectHead</td>
																				</tr>
																				<tr>
																					<td class='on-table-clear' colspan='100%'>User: $username</td>
																				</tr>";
					}
					$headers
																			.= "<tr>
																					<th>Project Name</th>
																					<th>Project Type</th>
																					<th>Due Date</th>
																					<th>Total Budget <i>(".$_SESSION["currency"].")</i></th>
																					<th>Consulting Budget <i>(".$_SESSION["currency"].")</i></th>
																					<th>Booked Consulting Budget <i>(".$_SESSION["currency"].")</i></th>
																					<th>Remaining Consulting Budget <i>(".$_SESSION["currency"].")</i></th>
																					<th>Expense Budget <i>(".$_SESSION["currency"].")</i></th>
																					<th>Booked Expense Budget <i>(".$_SESSION["currency"].")</i></th>
																					<th>Remaining Expense Budget <i>(".$_SESSION["currency"].")</i></th>
																					<th>Total Remaining Budget <i>(".$_SESSION["currency"].")</i></th>
																				</tr>";

					//  Table Information
					if (is_array($projects)) {
						$totaled_total_budget = 0;
						$totaled_consulting_budget = 0;
						$totaled_booked_consulting_budget = 0;
						$totaled_remaining_consulting_budget = 0;
						$totaled_expense_budget = 0;
						$totaled_booked_expense_budget = 0;
						$totaled_remaining_expense_budget = 0;
						$totaled_remaining_total_consulting_budget = 0;

						$timeTable = ($reportOn == "normal") ? "TimeSheet" : "ApprovedTime";
						$expenseTable = ($reportOn == "normal") ? "ExpenseSheet" : "ApprovedExpense";
						$timeWhere = ($reportOn == "normal") ? "" : " AND status = '2'";
						$expenseWhere = ($reportOn == "normal") ? "" : " AND status = '2'";

						foreach ($projects as $project) {

							// - Additional Budget Calculations - //

							$consulting_budget = (is_numeric($project[5]) ? number_format($project[5], 2, ".", "") : "0.00");
							$booked_consulting_budget = number_format(q("SELECT SUM(time_spent * rate) FROM ".$timeTable." WHERE project_id = '".$project[0]."' ".$timeWhere), 2, '.', '');
							$remaining_consulting_budget =  number_format($consulting_budget - $booked_consulting_budget, 2, '.', '');

							$expense_budget = (is_numeric($project[6]) ? number_format($project[6], 2, ".", "") : "0.00");
							$booked_expense_budget = number_format(q("SELECT SUM(expense) FROM ".$expenseTable." WHERE project_id = '".$project[0]."' ".$expenseWhere), 2, '.', '');
							$remaining_expense_budget = number_format($expense_budget - $booked_expense_budget, 2, '.', '');

							$total_budget = (is_numeric($project[4]) ? number_format($project[4], 2, ".", "") : "0.00");
							//$booked_budget = number_format(q("SELECT SUM(time_spent * rate) FROM timesheet WHERE project_id = '".$project[0]."' "), 2, '.', '');
							$remaining_total_budget = number_format($total_budget - ($booked_consulting_budget + $booked_expense_budget), 2, '.', '');

							if($project[2] == "")
								$project[2] = "Shared Project";

							$string .= "<tr>
								<td align='do-not-wrap left'>".$project[1]."</td>
								<td class='do-not-wrap left'>".$project[2]."</td>
								<td class='do-not-wrap rightdata'>".$project[3]."</td>

								<td class='do-not-wrap rightdata'>".$total_budget."</td>

								<td class='do-not-wrap rightdata'>".$consulting_budget."</td>
								<td class='do-not-wrap rightdata'>".$booked_consulting_budget."</td>
								<td class='do-not-wrap rightdata'>".$remaining_consulting_budget."</td>

								<td class='do-not-wrap rightdata'>".$expense_budget."</td>
								<td class='do-not-wrap rightdata'>".$booked_expense_budget."</td>
								<td class='do-not-wrap rightdata'>".$remaining_expense_budget."</td>

								<td class='do-not-wrap rightdata'>".$remaining_total_budget."</td>

							</tr>";

							$totaled_total_budget  += $total_budget;
							$totaled_consulting_budget  += $consulting_budget;
							$totaled_booked_consulting_budget  += $booked_consulting_budget;
							$totaled_remaining_consulting_budget  += $remaining_consulting_budget;
							$totaled_expense_budget  += $expense_budget;
							$totaled_booked_expense_budget  += $booked_expense_budget;
							$totaled_remaining_expense_budget  += $remaining_expense_budget;
							$totaled_remaining_total_consulting_budget  += $remaining_total_budget;

							$exceldata[$row][] = $project[1];
							$exceldata[$row][] = $project[2];
							$exceldata[$row][] = $project[3];
							$exceldata[$row][] = $total_budget;
							$exceldata[$row][] = $consulting_budget;
							$exceldata[$row][] = $booked_consulting_budget;
							$exceldata[$row][] = $remaining_consulting_budget;
							$exceldata[$row][] = $expense_budget;
							$exceldata[$row][] = $booked_expense_budget;
							$exceldata[$row][] = $remaining_expense_budget;
							$exceldata[$row][] = $remaining_total_budget;

							$row++;
						}

						$string .= "<tr>
											<td class='on-table-total'></td>
											<td class='on-table-total'></td>
											<td class='on-table-total'>Total:</td>
											<td class='on-table-total'>".number_format($totaled_total_budget, 2, ".", "")."</td>
											<td class='on-table-total'>".number_format($totaled_consulting_budget, 2, ".", "")."</td>
											<td class='on-table-total'>".number_format($totaled_booked_consulting_budget, 2, ".", "")."</td>
											<td class='on-table-total'>".number_format($totaled_remaining_consulting_budget, 2, ".", "")."</td>
											<td class='on-table-total'>".number_format($totaled_expense_budget, 2, ".", "")."</td>
											<td class='on-table-total'>".number_format($totaled_booked_expense_budget, 2, ".", "")."</td>
											<td class='on-table-total'>".number_format($totaled_remaining_expense_budget, 2, ".", "")."</td>
											<td class='on-table-total'>".number_format($totaled_remaining_total_consulting_budget, 2, ".", "")."</td>
										</tr>";

						$exceldata[$row][] = "";
						$exceldata[$row][] = "";
						$exceldata[$row][] = "Total:";
						$exceldata[$row][] = $totaled_total_budget;
						$exceldata[$row][] = $totaled_consulting_budget;
						$exceldata[$row][] = $totaled_booked_consulting_budget;
						$exceldata[$row][] = $totaled_remaining_consulting_budget;
						$exceldata[$row][] = $totaled_expense_budget;
						$exceldata[$row][] = $totaled_booked_expense_budget;
						$exceldata[$row][] = $totaled_remaining_expense_budget;
						$exceldata[$row][] = $totaled_remaining_total_consulting_budget;

						$row++;
					}

					///////////////////////////
					//  Display Information
					if ($string != "")
					{
						$string                                     = $headers.$string;
					}
					else
						$string                                     .= $headers."<tr>
																			<td align='center' colspan='5'>No Projects To Display</td></tr>";

					if ($string != "")
					{
						echo "<div class='on-20px'><table class='on-table-center on-table'>";
							echo "".$string;
							echo  "<tfoot>
										<tr>
											<td colspan='100%'></td>
										</tr>
									</tfoot>";
						echo "</table></div>";
						echo "<br/>";

						///////////////////////////
						//  Set Export Information
						$_SESSION["fileName"] = "Active_Projects_Report";
						if($excelheadings && $exceldata)
							$_SESSION["fileData"] = array_merge($excelheadings, $exceldata);
						///////////////////////////

						echo "<input name='btnExport' onClick=\"location.href='_export.php'\" type='button' value='Export to Excel' />";
					}
					///////////////////////////
				?>
			</form>
		</td>
	</tr>
	<tr>
		<td align="center">
			<br/>
		</td>
	</tr>
</table>
<?php
    //  Print Footer
    print_footer();
?>