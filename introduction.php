<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!$_SESSION["eula_agree"] === true)
        header("Location: logout.php");
        
    if (seen($_SESSION["user_id"], "intro") == 1)
        header("Location: intro_tutorial.php");
        
    //q("UPDATE seen SET tutorial = '0', intro = '0';");

    //  Print Header
    print_header();
    //  Print Menu
	if ($_GET["menu"] == "")
		print_menus("0", "home");
	else
		print_menus($_GET["menu"], "home");

	if ($_GET["menu"] == "") {
?>
<script language="JavaScript">
    jQuery(function(){
        // Timer - 40 sec
        jQuery("#myButton").oneTime(40000,function(){
            jQuery('#myButton').val("Continue");
        });
        
        // Button Value Check
        jQuery("#myButton").click(function() {
            if(jQuery(this).val() == "Continue"){
                 jQuery.post("_ajax.php", {func: 'setSeen', video: 'intro'}, function(data){
                    data = json_decode(data);
                    location.href='intro_tutorial.php';  
                });
            }else{
                location.href='intro_tutorial.php';
            }
        });
    });
</script>
<table width="100%">
    <tr>
        <td class='centerdata' valign="top">
            <form action="" method="post" name="home">
                <table width="100%">
                    <tr>
                        <td class="centerdata">
                            <h6>On-Track Introduction</h6>
                        </td>
                    </tr>
                    <tr>
                        <td class="centerdata">
                            <br/>
                        </td>
                    </tr>
                </table>
                <table width="100%">
                    <div id="flashContent">
                        <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="700" height="350" id="introvideo" align="middle">
                            <param name="movie" value="flashvideo/introvideo.swf" />
                            <param name="quality" value="high" />
                            <param name="bgcolor" value="#ffffff" />
                            <param name="play" value="true" />
                            <param name="loop" value="true" />
                            <param name="wmode" value="transparent" />
                            <param name="scale" value="showall" />
                            <param name="menu" value="true" />
                            <param name="devicefont" value="false" />
                            <param name="salign" value="" />
                            <param name="allowScriptAccess" value="sameDomain" />
                            <!--[if !IE]>-->
                            <object type="application/x-shockwave-flash" data="flashvideo/introvideo.swf" width="700" height="350">
                                <param name="movie" value="flashvideo/introvideo.swf" />
                                <param name="quality" value="high" />
                                <param name="bgcolor" value="#ffffff" />
                                <param name="play" value="true" />
                                <param name="loop" value="true" />
                                <param name="wmode" value="transparent" />
                                <param name="scale" value="showall" />
                                <param name="menu" value="true" />
                                <param name="devicefont" value="false" />
                                <param name="salign" value="" />
                                <param name="allowScriptAccess" value="sameDomain" />
                                <!--<![endif]-->
                                <a href="http://www.adobe.com/go/getflash">
                                    <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
                                </a>
                            <!--[if !IE]>-->
                            </object>
                            <!--<![endif]-->
                        </object>
                    </div>
                </table>
                
                <table width="100%">
                    <tr>
                        <td class="centerdata">
                            <br>
                            <input type='button' value='Skip Video' id="myButton">
                        </td>
                    </tr>
                </table>
                
            </form>
        </td>
    </tr>
</table>
<?php
    }
    //  Print Footer
    print_footer();
?>
