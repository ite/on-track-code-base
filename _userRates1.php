<?php
    session_start();

    include("_db.php");

    $scriptStart = my_microtime();
?>
<html>
    <head>
        <title>
            On-Track - Recovery Script...
        </title>
    </head>
    <body>
        <h1 align="center">
            Install - Rates/Company
        </h1>
        <?php
            $alter = q("ALTER TABLE Project_User ADD COLUMN rateID SMALLINT UNSIGNED DEFAULT 0 AFTER user_tariff");
            $alter = q("ALTER TABLE Project_User ADD COLUMN rate VARCHAR(10) DEFAULT '' AFTER user_tariff");

            $alter = q("ALTER TABLE Project_User CHANGE COLUMN rate rate VARCHAR(10) DEFAULT ''");
            $alter = q("ALTER TABLE Project_User CHANGE COLUMN rateID rateID SMALLINT UNSIGNED DEFAULT 0");

            $update = q("UPDATE Project_User SET rateID = 0");
            $update = q("UPDATE Project_User SET rate = ''");

            //  Rates Of Employees Per Company
            $drop = q("DROP TABLE IF EXISTS user_rates");
            $create = q("CREATE TABLE user_rates ( ".
                        "id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,".
                        "companyid SMALLINT UNSIGNED NOT NULL,".
                        "userid SMALLINT UNSIGNED NOT NULL,".
                        "rate VARCHAR(10) NOT NULL,".
                        "active TINYINT(3) DEFAULT 1)");

            //  Insert All Rates Of Employee To Company
            $info = q("SELECT c.id,c.name,e.id,e.email,e.tariff1,e.tariff2,e.tariff3,e.tariff4,e.tariff5 ".
                        "FROM (Company_Users AS cu INNER JOIN Company AS c ON cu.company_id = c.id INNER JOIN Employee AS e ON cu.user_id = e.id) ".
                        "WHERE e.email != 'admin' ".
                        "ORDER BY c.id,e.id");

            //cid [0], cname [1], uid [2], uemail [3], utariff1 [4], utariff2 [5], utariff3 [6], utariff4 [7], utariff5 [8]

            if (is_array($info))   {
                foreach ($info as $i)      {
                    $cID = $i[0];
                    $uID = $i[2];

                    for ($a = 4; $a <= 8; $a++) {
                        $rate = $i[$a];

                        $rate = number_format((double)$rate, 2, ".", "");

                        if (is_numeric($rate) && $rate > 0)   {
                            if (!exist("user_rates", "companyid = '".$cID."' AND userid = '".$uID."' AND rate = '".$rate."'"))
                                $insert = q("INSERT INTO user_rates(companyid,userid,rate) VALUES (".$cID.",".$uID.",'".$rate."')");
                        }
                    }
                }
            }

            echo "Total Time  [".number_format(round(my_microtime()-$scriptStart,3),3)."s]<br/>";

            echo "<p align='center'>Rates/Company script done executing</p>";
        ?>
        <form action="index.php" method="post">
            <center><input type="submit" value="Return"/></center>
        </form>
    </body>
</html>
