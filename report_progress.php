<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("fusion/FusionCharts.php");
    include("include/sajax.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    if (!hasAccess("REP_PROGRESS"))
        header("Location: noaccess.php");

    ///////////////////////////
    //  Sajax
    function getProjects($status, $type) {
        if ($status == "all")   $status = " AND p.completed IN (0,1)";
        else                    $status = " AND p.completed = '".$status."'";

        if ($type == "null")
            $projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p
                            INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id)
                            WHERE cot.company_id = '".$_SESSION["company_id"]."'
                            AND p.deleted = '0' $status ORDER BY UPPER(p.name)");
        else
            $projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p
                            INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id)
                            WHERE cot.company_id = '".$_SESSION["company_id"]."'
                            AND p.deleted = '0'
                            AND p.type_id='$type' $status ORDER BY UPPER(p.name)");

        return $projects;
    }

    $sajax_request_type = "GET";
    sajax_init();
    sajax_export("getProjects");
    sajax_handle_client_request();
    ///////////////////////////

    //  Set Report Generated Status
    $generated = "0";
    $exceldata = array();

    function createXMLString($a, $b, $c, $color) {
        //  $a -> Upper Limit, $b -> Value, $c -> Trendpoints
        $xmlString                                                      = "";

        $xmlString                                                      = "<chart editMode='0' showBorder='0' upperLimit='".$a."' ".
                                                                            "lowerLimit='0' numberPrefix='".$_SESSION["currency"]."' gaugeRoundRadius='5' ".
                                                                            "chartBottomMargin='30' ticksBelowGauge='0' placeTicksInside='0' showGaugeLabels='0' ".
                                                                            "pointerOnTop='1' pointerRadius='14' chartLeftMargin='25' chartRightMargin='30' ".
                                                                            "majorTMColor='868F9B' majorTMHeight='10' majorTMThickness='2' pointerBgAlpha='0' ".
                                                                            "pointerBorderThickness='2' majorTMNumber='0' minorTMNumber='0' showToolTip='0' decimals='0' ".fusionChart().">";
        $xmlString                                                      .= "<colorRange>";
        $xmlString                                                      .= "<color minValue='0' maxValue='100' code='$color'/>";
        $xmlString                                                      .= "</colorRange>";
        $xmlString                                                      .= "<value>".$b."</value>";
        $xmlString                                                      .= "<trendpoints>";
        $xmlString                                                      .= "<point value='".$c."' fontcolor='FF4400' useMarker='0' dashed='1' dashLen='1' ".
                                                                            "dashGap='3' markerRadius='5' color='8BBA00' alpha='100' thickness='2'/>";
        $xmlString                                                      .= "</trendpoints>";
        $xmlString                                                      .= "<annotations>";
        $xmlString                                                      .= "<annotationGroup id='Grp1' showBelow='1'>";
        $xmlString                                                      .= "<annotation type='rectangle' x='2' y='2' toX='445' toY='95' radius='10' fillColor='D6E0F6' ".
                                                                            "fillAngle='90' borderColor='868F9B' borderThickness='2'/>";
        $xmlString                                                      .= "</annotationGroup>";
        $xmlString                                                      .= "</annotations>";
        $xmlString                                                      .= "<styles>";
        $xmlString                                                      .= "<definition>";
        $xmlString                                                      .= "<style name='ValueFont' type='Font' bgColor='333333' size='10' color='FFFFFF'/>";
        $xmlString                                                      .= "<style name='RectShadow' type='Shadow' strength='3'/>";
        $xmlString                                                      .= "</definition>";
        $xmlString                                                      .= "<application>";
        $xmlString                                                      .= "<apply toObject='VALUE' styles='valueFont'/>";
        $xmlString                                                      .= "<apply toObject='Grp1' styles='RectShadow'/>";
        $xmlString                                                      .= "</application>";
        $xmlString                                                      .= "</styles>";
        $xmlString                                                      .= "</chart>";

        return $xmlString;
    }

    function createDisplay($project_name, $consulting, $expenses, $total_time, $total_expense, $expense_planning, $total_consulting, $total_expenses) {
        //  Set Table Border & Background Details
        $top                                                            = "border-top: 1px solid #0A0F15;";
        $left                                                           = "border-left: 1px solid #0A0F15;";
        $right                                                          = "border-right: 1px solid #0A0F15;";
        $bottom                                                         = "border-bottom: 1px solid #0A0F15;";
        $background                                                     = "background-color:#3E638F;";

        if ($consulting == "")
            $consulting                                                 = 0;

        if ($expenses == "")
            $expenses                                                   = 0;

        if ($total_time == "")
            $total_time                                                 = 0;

        if ($total_expense == "")
            $total_expense                                              = 0;

        if ($expense_planning == "")
            $expense_planning                                           = 0;

        if ($total_consulting == "")
            $total_consulting                                           = 0;

        if ($total_expenses == "")
            $total_expenses                                             = 0;

        $display                                                        = "<tr>
                                                                                    <td class='on-table-clear' colspan='4'><a>Project Progress Report<a></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='on-table-clear' colspan='4'>Project Name: ".$project_name."</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan=4'><i>Please Note That VAT Is Excluded</i></td>
                                                                                </tr>
                                                                                <td colspan='4'><br/>";

        $color                                                          = "#0033FF";

        //  Upper Limit - Consulting -> Time Based
        if ($total_time_based > $consulting)
            $upperLimit                                                 = $total_time;
        else
            $upperLimit                                                 = $consulting;

        $value                                                          = $total_time;
        $trendPoint                                                     = $consulting;

        $graph                                                          = createXMLString($upperLimit, $value, $trendPoint, $color);

        $display                                                        .= "Consulting (Booked)<br/><br/>";
        $display                                                        .= "".renderChart("fusion/HLinearGauge.swf", "", $graph, "multigraph1", 450, 100, false, false);

        //  Upper Limit - Consulting -> Invoice Based
        if ($total_consulting > $consulting)
            $upperLimit                                                 = $total_consulting;
        else
            $upperLimit                                                 = $consulting;

        $value                                                          = $total_consulting;
        $trendPoint                                                     = $consulting;

        $graph                                                          = createXMLString($upperLimit, $value, $trendPoint, $color);

        $display                                                        .= "<br/>Consulting (Invoiced)<br/><br/>";
        $display                                                        .= "".renderChart("fusion/HLinearGauge.swf", "", $graph, "multigraph2", 450, 100, false, false);

        $color                                                          = "#FF0033";

        //  Upper Limit - Expenses -> Time Based
        if (($total_expense + $expense_planning) > $expenses)
            $upperLimit                                                 = $total_expense + $expense_planning;
        else
            $upperLimit                                                 = $expenses;

        $value                                                          = $total_expense + $expense_planning;
        $trendPoint                                                     = $expenses;

        $graph                                                          = createXMLString($upperLimit, $value, $trendPoint, $color);

        $display                                                        .= "<br/>Expenses (Booked)<br/><br/>";
        $display                                                        .= "".renderChart("fusion/HLinearGauge.swf", "", $graph, "multigraph3", 450, 100, false, false);

        //  Upper Limit - Expenses -> Invoice Based
        if ($total_expenses > $expenses)
            $upperLimit                                                 = $total_expenses;
        else
            $upperLimit                                                 = $expenses;

        $value                                                          = $total_expenses;
        $trendPoint                                                     = $expenses;

        $graph                                                          = createXMLString($upperLimit, $value, $trendPoint, $color);

        $display                                                        .= "<br/>Expenses (Invoiced)<br/><br/>";
        $display                                                        .= "".renderChart("fusion/HLinearGauge.swf", "", $graph, "multigraph4", 450, 100, false, false);

        $color                                                          = "#33FF00";

        //  Upper Limit - Consulting + Expenses -> Time Based
        if (($total_time + $total_expense + $expense_planning) > ($consulting + $expenses))
            $upperLimit                                                 = $total_time + $total_expense + $expense_planning;
        else
            $upperLimit                                                 = $consulting + $expenses;

        $value                                                          = $total_time + $total_expense + $expense_planning;
        $trendPoint                                                     = $consulting + $expenses;

        $graph                                                          = createXMLString($upperLimit, $value, $trendPoint, $color);

        $display                                                        .= "<br/>Consulting + Expenses (Booked)<br/><br/>";
        $display                                                        .= "".renderChart("fusion/HLinearGauge.swf", "", $graph, "multigraph5", 450, 100, false, false);

        //  Upper Limit - Consulting + Expenses -> Invoice Based
        if (($total_consulting + $total_expenses) > ($consulting + $expenses))
            $upperLimit                                                 = $total_consulting + $total_expenses;
        else
            $upperLimit                                                 = $consulting + $expenses;

        $value                                                          = $total_consulting + $total_expenses;
        $trendPoint                                                     = $consulting + $expenses;

        $graph                                                          = createXMLString($upperLimit, $value, $trendPoint, $color);

        $display                                                        .= "<br/>Consulting + Expenses (Invoiced)<br/><br/>";
        $display                                                        .= "".renderChart("fusion/HLinearGauge.swf", "", $graph, "multigraph6", 450, 100, false, false);

        //  Create Table
        $display                                                        .= "<br/><br/></td></tr>
                                                                                    <tr>
                                                                                        <th class='centerdata'>Based On</th>
                                                                                        <th align='center'>Total Budget <i>(".$_SESSION["currency"].")</i></th>
                                                                                        <th align='center'>Total Time <i>(".$_SESSION["currency"].")</i></th>
                                                                                        <th align='center'>Total Invoiced <i>(".$_SESSION["currency"].")</i></th>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Consulting</td>
                                                                                        <td class='rightdata'>".number_format($consulting, 2, ".", "")."</td>
                                                                                        <td class='rightdata'>".number_format($total_time, 2, ".", "")."</td>
                                                                                        <td class='rightdata'>".number_format($total_consulting, 2, ".", "")."</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Expenses</td>
                                                                                        <td class='rightdata'>".number_format($expenses, 2, ".", "")."</td>
                                                                                        <td class='rightdata'>".number_format($total_expense + $expense_planning, 2, ".", "")."</td>
                                                                                        <td class='rightdata'>".number_format(($total_expenses), 2, ".", "")."</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Combination</td>
                                                                                        <td class='rightdata'>".number_format(($consulting + $expenses), 2, ".", "")."</td>
                                                                                        <td class='rightdata'>".number_format(($total_time + $total_expense + $expense_planning), 2, ".", "")."</td>
                                                                                        <td class='rightdata'>".number_format(($total_consulting + $total_expenses), 2, ".", "")."</td>
                                                                                    </tr>";

        $row = 0;

        global $exceldata;

        $exceldata[$row][] = "Based On";
        $exceldata[$row][] = "Total Budget (".$_SESSION["currency"].")";
        $exceldata[$row][] = "Total Time (".$_SESSION["currency"].")";
        $exceldata[$row++][] = "Total Invoiced (".$_SESSION["currency"].")";

        $exceldata[$row][] = "Consulting";
        $exceldata[$row][] = number_format($consulting, 2, ".", "");
        $exceldata[$row][] = number_format($total_time, 2, ".", "");
        $exceldata[$row++][] = number_format($total_consulting, 2, ".", "");

        $exceldata[$row][] = "Expenses";
        $exceldata[$row][] = number_format($expenses, 2, ".", "");
        $exceldata[$row][] = number_format($total_expense + $expense_planning, 2, ".", "");
        $exceldata[$row++][] = number_format(($total_expenses), 2, ".", "");

        $exceldata[$row][] = "Combination";
        $exceldata[$row][] = number_format(($consulting + $expenses), 2, ".", "");
        $exceldata[$row][] = number_format(($total_time + $total_expense + $expense_planning), 2, ".", "");
        $exceldata[$row++][] = number_format(($total_consulting + $total_expenses), 2, ".", "");

        return $display;
    }

    //  Create Display Information Function
    if (isset($_POST["save"]) && $_POST["save"] === "1") {
        $errorMessage                                                   = "";

        $reportOn = $_POST["reportOn"];
        $projectStatus = $_POST["projectStatus"];

        $project_type_id = $_POST["projectType"];
        $project_id = $_POST["project"];
        $displayString = "";
        $where = "";

        if ($project_type_id != "null" && is_numeric($project_type_id)) {
            $where .= "AND p.type_id = '$project_type_id' ";
        }

        if ($projectStatus == "all")    $projectStatus = " AND p.completed IN (0,1) ";
        else                            $projectStatus = " AND p.completed = '".$projectStatus."' ";

        if ($project_id == "all") {
            $table1 = ($reportOn == "approved") ? "ApprovedTime" : "TimeSheet";
            $where1 = ($reportOn == "approved") ? "AND ts.status = '2' " : "";

            $table2 = ($reportOn == "approved") ? "ApprovedExpense" : "ExpenseSheet";
            $where2 = ($reportOn == "approved") ? "AND es.status = '2' " : "";

            if ($_POST["projectType2"] == "active") {
                //  Get Info
                $project_info = q("SELECT SUM(cot.consulting),SUM(cot.expenses) FROM (Project as p INNER JOIN companiesOnTeam as cot ".
                                        "ON p.id = cot.project_id) WHERE cot.total_budget > 0 AND cot.company_id = '".$_SESSION["company_id"]."' $projectStatus $where");

                //  Get Info - Time Based
                $total_time = q("SELECT SUM(ts.rate * ts.time_spent) FROM ($table1 AS ts INNER JOIN Project AS p ".
					"ON p.id = ts.project_id INNER JOIN companiesOnTeam AS cot ON p.id = cot.project_id) WHERE cot.total_budget > 0 AND p.due_date != '' ".
					"AND ts.company_id = '".$_SESSION["company_id"]."' $projectStatus $where $where1");
                $total_expense = q("SELECT SUM(es.expense) FROM ($table2 AS es INNER JOIN Project AS p ".
					"ON p.id = es.project_id INNER JOIN companiesOnTeam AS cot ON p.id = cot.project_id) WHERE cot.total_budget > 0 AND p.due_date != '' ".
					"AND es.company_id = '".$_SESSION["company_id"]."' $projectStatus $where $where2");
                $expense_planning = q("SELECT SUM(eb.amount) FROM (ExpenseBudget AS eb INNER JOIN Project AS p ".
					"ON p.id = eb.project_id INNER JOIN companiesOnTeam AS cot ON p.id = cot.project_id) WHERE cot.total_budget > 0 AND p.due_date != '' ".
					"AND cot.company_id = '".$_SESSION["company_id"]."' AND eb.paid = '1' $projectStatus $where");
                //  Get Info - Invoice Based
                $total_consulting = q("SELECT SUM(li.consulting) FROM (LoadInvoices AS li INNER JOIN Project AS p ".
					"ON p.id = li.project_id INNER JOIN companiesOnTeam AS cot ON p.id = cot.project_id) WHERE cot.total_budget > 0 AND p.due_date != '' ".
					"AND cot.company_id = '".$_SESSION["company_id"]."' $projectStatus $where");
                $total_expenses = q("SELECT SUM(li.expenses) FROM (LoadInvoices AS li INNER JOIN Project AS p ".
					"ON p.id = li.project_id INNER JOIN companiesOnTeam AS cot ON p.id = cot.project_id) WHERE cot.total_budget > 0 AND p.due_date != '' ".
					"AND cot.company_id = '".$_SESSION["company_id"]."' $projectStatus $where");

                $displayString = createDisplay("All Active Projects", $project_info[0][0], $project_info[0][1], $total_time, $total_expense, $expense_planning, $total_consulting, $total_expenses);
            }
            else {
                //  Get Info
                $project_info = q("SELECT SUM(cot.consulting),SUM(cot.expenses) FROM (Project as p INNER JOIN companiesOnTeam AS cot ON p.id = cot.project_id) ".
					"WHERE cot.total_budget = '' AND cot.company_id = '".$_SESSION["company_id"]."' $projectStatus $where");

                //  Get Info - Time Based
                $total_time = q("SELECT SUM(ts.rate * ts.time_spent) FROM ($table1 AS ts INNER JOIN Project AS p ".
					"ON p.id = ts.project_id INNER JOIN companiesOnTeam AS cot ON p.id = cot.project_id) WHERE cot.total_budget = '' ".
					"AND cot.company_id = '".$_SESSION["company_id"]."' $projectStatus $where $where1");
                $total_expense = q("SELECT SUM(es.expense) FROM ($table2 AS es INNER JOIN Project AS p ".
					"ON p.id = es.project_id INNER JOIN companiesOnTeam AS cot ON p.id = cot.project_id) WHERE cot.total_budget = '' ".
					"AND cot.company_id = '".$_SESSION["company_id"]."' $projectStatus $where $where2");
                $expense_planning = q("SELECT SUM(eb.amount) FROM (ExpenseBudget AS eb INNER JOIN Project AS p ".
					"ON p.id = eb.project_id INNER JOIN companiesOnTeam AS cot ON p.id = cot.project_id) WHERE cot.total_budget = '' ".
					"AND cot.company_id = '".$_SESSION["company_id"]."' AND eb.paid = '1' $projectStatus $where");
                //  Get Info - Invoice Based
                $total_consulting = q("SELECT SUM(li.consulting) FROM (LoadInvoices AS li INNER JOIN Project AS p ".
					"ON p.id = li.project_id INNER JOIN companiesOnTeam AS cot ON p.id = cot.project_id) WHERE cot.total_budget = '' ".
					"AND cot.company_id = '".$_SESSION["company_id"]."' $projectStatus $where");
                $total_expenses = q("SELECT SUM(li.expenses) FROM (LoadInvoices AS li INNER JOIN Project AS p ".
					"ON p.id = li.project_id INNER JOIN companiesOnTeam AS cot ON p.id = cot.project_id) WHERE cot.total_budget = '' ".
					"AND cot.company_id = '".$_SESSION["company_id"]."' $projectStatus $where");

                $displayString = createDisplay("All Non-Invoicable Projects", $project_info[0][0], $project_info[0][1], $total_time, $total_expense, $expense_planning, $total_consulting, $total_expenses);
            }
        }
        else {
            $table1 = ($reportOn == "approved") ? "ApprovedTime" : "TimeSheet";
            $where1 = ($reportOn == "approved") ? "AND status = '2' " : "";

            $table2 = ($reportOn == "approved") ? "ApprovedExpense" : "ExpenseSheet";
            $where2 = ($reportOn == "approved") ? "AND status = '2' " : "";

            //  Get Info
            $project_info = q("SELECT p.name, cot.consulting, cot.expenses FROM Project AS p INNER JOIN companiesOnTeam AS cot ON p.id = cot.project_id ".
				"WHERE p.id = '$project_id' AND cot.company_id = '".$_SESSION["company_id"]."' $projectStatus");

            //  Get Info - Time Based
            $total_time = q("SELECT SUM(rate * time_spent) FROM $table1 WHERE project_id = '$project_id' AND company_id = '".$_SESSION["company_id"]."' $where1");
            $total_expense = q("SELECT SUM(expense) FROM $table2 WHERE project_id = '$project_id' AND company_id = '".$_SESSION["company_id"]."' $where2");
            $expense_planning = q("SELECT SUM(amount) FROM ExpenseBudget WHERE paid = '1' AND project_id = '$project_id' AND company_id = '".$_SESSION["company_id"]."'");

            //  Get Info - Invoice Based
            $total_consulting = q("SELECT SUM(consulting) FROM LoadInvoices WHERE project_id = '$project_id' AND company_id = '".$_SESSION["company_id"]."'");
            $total_expenses = q("SELECT SUM(expenses) FROM LoadInvoices WHERE project_id = '$project_id' AND company_id = '".$_SESSION["company_id"]."'");

            $displayString = createDisplay($project_info[0][0], $project_info[0][1], $project_info[0][2], $total_time, $total_expense, $expense_planning, $total_consulting, $total_expenses);
        }

        if ($displayString != "")
            $generated                                                  = "1";
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("3", "reports");

    if ($errorMessage != "") {
        echo "<p align='center' style='padding:0px;'><strong><font color='#999999'>$errorMessage</font></strong></p>";
        echo "<br/>";
    }
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    function check()
    {
        var valid                                                       = 1;

        //  Check That Project Is Selected
        if (document.forms["report_progress"].project.value == "null")
        {
            ShowLayer("projectDiv", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("projectDiv", "none");

        if (valid == 1)
        {
            document.forms["report_progress"].save.value                = 1;
            document.forms["report_progress"].submit();
        }
    }

    function displayCheck(select)
    {
        if (select.value == "all")
            ShowLayer("allProjectsDiv", "block");
        else
            ShowLayer("allProjectsDiv", "none");
    }

    ///////////////////////////
    //  Sajax
    <?php sajax_show_javascript(); ?>

    function ClearOptions(OptionList) {
        for (x = OptionList.length; x >= 0; x--)
        {
            OptionList[x]                                               = null;
        }
    }

    function setProjects(data) {
        var c                                                           = 0;

        document.forms["report_progress"].project.options[(c++)] = new Option("--  Select A Project  --", "null");
        document.forms["report_progress"].project.options[(c++)] = new Option("All Projects", "all");

        for (var i in data)
            eval("document.forms['report_progress'].project.options["+(c++)+"] = new Option('" + data[i][1] + "', '" + data[i][0] + " ');");
    }

    function getProjects(select1,select2) {
        var status = eval("document.report_progress."+select1+".value;");
        var type = eval("document.report_progress."+select2+".value;");;

        ClearOptions(document.report_progress.project);
        x_getProjects(status, type, setProjects);
    }
    ///////////////////////////
</script>
<?php
    $projectTypes= q("SELECT id, type FROM ProjectTypes WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY type");
    $projectTypes[] = array("0","Shared Projects");
    $projectTypes = sortArrayByColumn($projectTypes, 1);

    $projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) ".
		                "WHERE cot.company_id = '".$_SESSION["company_id"]."' AND p.completed = '0' AND p.deleted = '0' ORDER BY UPPER(p.name)");

?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="report_progress">
                    <div id="" style="<?php if ($generated == "1") echo "display: none;"; else echo "display: block"; ?>">
                        <table width="100%">
                            <tr>
                                <td class="centerdata">
                                    <h6>Project Progress Report</h6>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Report On:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="reportOn">
                                        <option value="normal">Actual Time</option>
                                        <option value="approved">Approved Time</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Project Status:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="projectStatus" onChange="getProjects('projectStatus','projectType');">
                                        <option value="all">Completed & Uncompleted</option>
                                        <option value="0" selected>Uncompleted</option>
                                        <option value="1">Completed</option>
                                    </select>
                                </td>
                            </tr>
                            <tr><td colspan="100%"><br/></td></tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Project Type:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="projectType" onChange="getProjects('projectStatus','projectType');">
                                        <option value="all">All Project Types</option>
                                        <?php
                                            if (is_array($projectTypes))
                                                foreach ($projectTypes as $projectType)
                                                    if ($_POST["projectType"] == $projectType[0])
                                                        echo "<option value='".$projectType[0]."' selected>".$projectType[1]."</option>";
                                                    else
                                                        echo "<option value='".$projectType[0]."'>".$projectType[1]."</option>";
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Project:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" id="project" name="project" onChange="displayCheck(project);">
                                        <option value="null">--  Select A Project  --</option>
                                        <option value="all">All Projects</option>
                                        <?php
                                            if (is_array($projects))
                                                foreach ($projects as $project)
                                                    if (isset($_POST["project"]) && $_POST["project"] == $project[0])
                                                        echo "<option value='".$project[0]."' selected>".$project[1]."</option>";
                                                    else
                                                        echo "<option value='".$project[0]."'>".$project[1]."</option>";
                                        ?>
                                    </select>
                                    <div id="projectDiv" style="display: none;"><font class="on-validate-error">* Project must be selected</font></div>
                                </td>
                            </tr>
                        </table>
                        <div id="allProjectsDiv" style="display: none;">
                            <table cellpadding="0" cellspacing="2" width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Project Type:
                                    </td>
                                    <td class="on-description-left" width="50%">
                                        <input method="post" name="projectType2" type="radio" value="active" checked><a>Active Projects</a><br/>
                                        <input method="post" name="projectType2" type="radio" value="non_invoicable"><a>Non-Invoicable Projects</a><br/>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <br/>
                        <input name="btnGenerate" onClick="check();" type="button" value="Generate Report">
                        <input method="post" name="save" type="hidden" value="0" />
                    </div>
                </form>
                <div id="report" style="<?php if ($generated == "1") echo "display: block;"; else echo "display: none"; ?>">
                    <?php
                        echo "<table class='on-table-center on-table'>";
                            echo "".$displayString;
                            echo  "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        echo "</table>";

                        ///////////////////////////
                        //  Set Export Information
                        $_SESSION["fileName"] = "Project Progress Report";
                        $_SESSION["fileData"] = $exceldata;
                        ///////////////////////////

                        echo "<input name='btnExport' onClick=\"location.href='_export.php'\" type='button' value='Export to Excel' />";
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
