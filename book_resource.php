<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!$_SESSION["eula_agree"] === true)
        header("Location: logout.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

	if (!hasAccess("SHAREDRES_BOOK"))
            header("Location: noaccess.php");


    //  nResources                                                      = Number of Resources
    $nResources                                                         = q("SELECT COUNT(rs.id) FROM (Resources AS rs INNER JOIN ResourceAssign AS ra ".
                                                                            "ON rs.id = ra.resource_id) WHERE ra.company_id = '".$_SESSION["company_id"]."'");
    $resources                                                          = q("SELECT rs.id, rs.resource FROM (Resources AS rs INNER JOIN ResourceAssign AS ra ".
                                                                            "ON rs.id = ra.resource_id) WHERE ra.company_id = '".$_SESSION["company_id"]."' ".
                                                                            "ORDER BY rs.resource");

    if ($nResources == 0)
        header("Location: home.php");

    ///////////////////////////
    //  Book Resource Items
    $resource_id                                                        = "null";
    $date                                                               = $today;

    if ($_GET["resoure"] != "")
        $resource_id                                                    = $_GET["resoure"];

    if ($_GET["date"] != "")
        $date                                                           = $_GET["date"];

    if (isset($_POST["resource"]) && is_numeric($_POST["resource"]))
        $resource_id                                                    = $_POST["resource"];

    if (isset($_POST["book_date"]))
        $date                                                           = $_POST["book_date"];

    //  Insert Booked Time Function
    if (isset($_POST["save"]) && $_POST["save"] === "1") {
        $errorMessage                                                   = "";

        //  Get Information
        $resource_id                                                    = $_POST["resource"];


        if ($resource_id != "null") {
            $description                                                = addslashes(strip_tags($_POST["description"]));
            $time                                                       = date("H:i:s");

            $array                                                      = split("_", $_POST["checkbox"]);

            if ($arr[0][0])
                $array                                                  = array_slice($array, 0, count($array) - 1);

            if ($array[0][0]) {
                foreach ($array as $a) {
                    $book_date                                          = substr($a, 0, strpos($a, "*"));
                    $book_time                                          = substr($a, (strpos($a, "*") + 1), strlen($a));

                    //  Make Booking for Resource
                    if (submitDateCheck($book_date))   {
                    if ($_POST[$a]) {
                        $insert                                         = q("INSERT INTO ResourceSheet (date, time, user_id, company_id, resource_id, descr, on_date, ".
                                                                            "on_time) VALUES ('$book_date', '$book_time', '".$_SESSION["user_id"]."', ".
                                                                            "'".$_SESSION["company_id"]."', '$resource_id', '$description', '$today', '$time')");

                        if ($insert) {
                            $time                                       = date("H:i:s");
                            $resource                                   = q("SELECT resource FROM Resources WHERE id = '$resource_id'");

                            $logs                                       = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('".$_SESSION["email"]." booked  ".$resource."', 'Insert', 'ResourceSheet', ".
                                                                            "'".$_SESSION["email"]."', '$today', '$time', '".$_SESSION["company_id"]."')");

                            header("Location: book_resource.php?resoure=$resource_id&date=$date");
                        }
                    }
                    }
                }
            }
        }
        else
            $errorMessage                                               = "Resource Must Be Selected";
    }

    //  User Deletes Own Entry
    if (isset($_POST["deleteBox"]) && $_POST["deleteBox"] === "1") {
	$id                                                             = $_POST["idBox"];
        $date                                                           = substr($id, 0, strpos($id, "*"));
        $time                                                           = substr($id, strpos($id, "*") + 1);

        $delete                                                         = q("DELETE FROM ResourceSheet WHERE date = '$date' AND time = '$time'");

        if ($delete) {
            $time                                                       = date("H:i:s");

            $logs                                                       = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('".$_SESSION["email"]." deleted Resource Booking', 'Delete', ".
                                                                            "'ResourceSheet', '".$_SESSION["email"]."', '$today', '$time', '".$_SESSION["company_id"]."')");

            $errorMessage                                               = "Booking Deleted Successfully...";
        }
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0", "bookings", "book_resource");

    if ($errorMessage != "")
    {
        if ($errorMessage == "Please note that we are currently busy with data management, no entries are allowed for 2010-08-26. Please check back in 24 Hours.")
            echo "<p style='text-align:center; padding:0px; width:100%;'><strong><font class='on-validate-error'>$errorMessage</font></strong></p>";
        else
            echo "<p style='text-align:center; padding:0px; width:100%;'><strong><font color='green'>$errorMessage</font></strong></p>";
        echo "<br/>";
    }

    //  Days                                                            = Week Days Array
    $days                                                               = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    jQuery(function(){
	jQuery('#book_date').DatePicker({
		format:'Y-m-d',
		date: jQuery('#book_date').val(),
		current: jQuery('#book_date').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#book_date').val()))
			_date = new Date();
			else _date = jQuery('#book_date').val();
			jQuery('#book_date').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#book_date').val(formated);
			jQuery('#book_date').DatePickerHide();
		}
	});
    })

    function check1() {
        var valid                                                       = 1;

        //  Check That Resource Is Selected
        if (document.forms["book_resource"].resource.value == "null") {
            ShowLayer("resourceDiv", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("resourceDiv", "none");

        //  Check That Date Is Entered
        if (document.forms["book_resource"].book_date.value == "") {
            ShowLayer("bookDateDiv", "none");
            ShowLayer("bookDate", "block");
            valid                                                       = 0;
        }
        //  Check That Entered Date Is Valid
        else {
            ShowLayer("bookDate", "none");

            if (!validation("date", document.forms["book_resource"].book_date.value)) {
                ShowLayer("bookDateDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("bookDateDiv", "none");
        }

        if (valid == 1)
            document.forms["book_resource"].submit();
    }

    function check2() {
        var valid                                                       = 1;

        //  Check That Date Is Entered
        if (document.forms["book_resource"].description.value == "") {
            ShowLayer("descriptionDiv", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("descriptionDiv", "none");

        if (valid == 1)
        {
            document.forms["book_resource"].save.value                  = 1;
            document.forms["book_resource"].submit();
        }
    }

    function deleted(id) {
        document.forms["book_resource"].idBox.value                     = id;
        document.forms["book_resource"].deleteBox.value                 = 1;
        document.forms["book_resource"].submit();
    }
</script>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata">
                <form action="" method="post" name="book_resource">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>
                                    Book Shared Resource
                                </h6>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                    Resource Name:
                            </td>
                            <td width="50%">
                                <select class="on-field" method="post" name="resource" onChange="check1();">
                                    <option value="null">--  Select A Resource  --&nbsp;&nbsp;</option>
                                    <?php
                                        if ($nResources > 1)
                                            foreach ($resources as $resoure)
                                                if ($resource_id == $resoure[0])
                                                    echo "<option value='".$resoure[0]."' selected>".$resoure[1]."&nbsp;&nbsp;</option>";
                                                else
                                                    echo "<option value='".$resoure[0]."'>".$resoure[1]."&nbsp;&nbsp;</option>";
                                        else if ($nResources == 1)
                                            if ($resource_id == $resources[0][0])
                                                echo "<option value='".$resources[0][0]."' selected>".$resources[0][1]."&nbsp;&nbsp;</option>";
                                            else
                                                echo "<option value='".$resources[0][0]."'>".$resources[0][1]."&nbsp;&nbsp;</option>";
                                    ?>
                                </select>
                                <div id="resourceDiv" style="display: none;"><font class="on-validate-error">* Resource must be selected</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                    Booking Date:
                            </td>
                            <td width="50%">
                                <input class="on-field-date" id="book_date" name="book_date" type="text" value="<?php
                                    if ($_POST["book_date"] != "") echo "".$_POST["book_date"]; else echo "".$today; ?>" onChange="check1();">
                                <div id="bookDate" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                <div id="bookDateDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                    Description:
                            </td>
                            <td width="50%">
                                <input class="on-field" name="description" type="text" value="<?php echo "".$_POST["description"]; ?>">
                                <div id="descriptionDiv" style="display: none;"><font class="on-validate-error">* Description must be entered (max 150 characters)</font></div>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <div class="on-20px">
                    <table class="on-table-center on-table">
                        <!--  Table Headings   -->
                        <tr>
                            <th width="150px">Time</th>
                            <?php
                                $tmpdate   = getWeekStart($date);
                                $i               = 0;

                                foreach ($days as $day) {
                                    echo "<th width='160'>"
                                                .$day."<br/>".getDates($tmpdate, $i++)."
                                            </th>";
                                }
                            ?>
                        </tr>
                        <!--  Table Information   -->
                        <?php
                            $start_time = date("H:i", strtotime("07:00"));
                            $checkbox  = "";

                            while ($start_time < "19:00") {
                                $tmpdate                            = getWeekStart($date);
                                $i                                  = 0;

                                echo "<tr>
                                            <td>".$start_time." - ".date("H:i", (strtotime($start_time) + 1800))."</td>";

                                    foreach ($days as $day) {
                                        $checkbox                   .= "".getDates($tmpdate, $i)."*".$start_time."_";
                                        $checked                    = q("SELECT id FROM ResourceSheet WHERE time = '".$start_time."' ".
                                                                            "AND date = '".getDates($tmpdate, $i)."' AND resource_id = '".$resource_id."'");

                                        echo "<td class='centerdata'>";
                                            if ($checked){
                                                $userInfo           = q("SELECT rs.company_id, e.frstname, e.lstname, rs.descr, e.id FROM (Employee AS e ".
                                                                        "INNER JOIN ResourceSheet AS rs ON e.id = rs.user_id) WHERE rs.time = '".$start_time."' ".
                                                                        "AND rs.date = '".getDates($tmpdate, $i)."' AND rs.resource_id = '".$resource_id."'");
                                                $company_name       = q("SELECT name FROM Company WHERE id = '".$userInfo[0][0]."'");

                                                if ($_SESSION["user_id"] == $userInfo[0][4])
                                                    echo "<b>$company_name</b><br/>
                                                        <i>".$userInfo[0][1]." ".$userInfo[0][2]."</i><br/>
                                                        ".$userInfo[0][3]."
                                                        <input onClick=\"deleted('".getDates($tmpdate, $i)."*".$start_time."');\" type='button' 
                                                            style='padding: 0px 2px; margin:0; font-weight:bold; color:#ff0000;' value='X'/>";
                                                else
                                                    echo "<b>$company_name</b><br/><i>".$userInfo[0][1]." ".$userInfo[0][2]."</i><br/>".$userInfo[0][3]."";
                                            }
                                            else
                                                echo "<input name='".getDates($tmpdate, $i)."*".$start_time."' type='checkbox' value='1'>";
                                        echo "</td>";
                                        $i++;
                                    }

                                    $start_time = date("H:i", (strtotime($start_time) + 1800));
                                echo "</tr>";
                            }
                            echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        ?>
                    </table>
                    </div>
                    <?php
                        echo "<input name='checkbox' type='hidden' value='".$checkbox."'>";
                    ?>
                    <input name="btnAdd" onClick="check2();" type="button" value="Book Resource">
                    <br/>
                    <input method="post" name="save" type="hidden" value="0" />
                    <input name="idBox" type="hidden" value="">
                    <input name="deleteBox" type="hidden" value="">
                </form>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
