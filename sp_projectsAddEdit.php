<?php
    //TODO:  check if record exists
    //TODO:  check if update necessary
    session_start();

    include("_db.php");
    include("graphics.php");
    include("_functions.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

	if (!hasAccess("SPM_MANAGE"))
		header("Location: noaccess.php");

    function getSPTeam($spID) {
        unset($team);

        $r = 0;

        if ($spID != "")  {
            $data = q("SELECT spc.id,spc.project_id,spc.company_id,c.name,spc.total_budget,spc.consulting,spc.expenses,spc.status,spc.teamManage,spc.invoiceCycle ".
                        "FROM (companiesOnTeam AS spc INNER JOIN Company AS c ON spc.company_id = c.id) ".
                        "WHERE spc.project_id = '".$spID."' AND spc.deleted = '0' ORDER BY c.name");

            if (is_array($data))    {
                foreach ($data as $d)   {
                    $team[$r] = $d;

                    if ($d[8] == "1")   {
                        /*
                        $users = q("SELECT spu.id,spu.project_id,spu.company_id,spu.user_id,CONCAT(e.lstname, ', ', e.frstname, ' [', e.email, ']') AS user,spu.manager,".
                                    "spu.user_budget,spu.user_tariff FROM (Project_User AS spu INNER JOIN Employee AS e ON e.id = spu.user_id) ".
                                    "WHERE spu.company_id = '".$d[2]."' AND spu.project_id = '".$spID."' AND spu.deleted = '0' ".
                                    "ORDER BY e.lstname, e.frstname, e.email");
                        */
                        $users = q("SELECT spu.id,spu.project_id,spu.company_id,spu.user_id,CONCAT(e.lstname, ', ', e.frstname, ' [', e.email, ']') AS user,spu.manager,".
                                    "spu.user_budget,spu.rateID FROM (Project_User AS spu INNER JOIN Employee AS e ON e.id = spu.user_id) ".
                                    "WHERE spu.company_id = '".$d[2]."' AND spu.project_id = '".$spID."' AND spu.deleted = '0' ".
                                    "ORDER BY e.lstname, e.frstname, e.email");

                        if (is_array($users))
                            $team[$r][] = $users;
                    }

                    $r++;
                }
            }
        }
        else    {
            //  Companies
            $teamTblCntr = (isset($_POST["teamTblCntr"])) ? addslashes(strip_tags($_POST["teamTblCntr"])) : 0;

            if ($teamTblCntr >= 1)  {
                for ($i = 0; $i < $teamTblCntr; $i++)  {
                    if (addslashes(strip_tags($_POST["company_id".$i])) != "")   {
                        unset($data);

                        $data[] = (isset($_POST["hiddenCompanyID".$i])) ? addslashes(strip_tags($_POST["hiddenCompanyID".$i])) : "";
                        $data[] = (isset($_POST["spID".$i])) ? addslashes(strip_tags($_POST["spID".$i])) : "";
                        $data[] = (isset($_POST["company_id".$i])) ? addslashes(strip_tags($_POST["company_id".$i])) : "";
                        $data[] = (isset($_POST["company_id".$i]) && is_numeric($_POST["company_id".$i])) ? q("SELECT name FROM Company WHERE id = '".addslashes(strip_tags($_POST["company_id".$i]))."'") : "";
                        $data[] = (isset($_POST["total_budget".$i]) && is_numeric($_POST["total_budget".$i])) ? number_format(addslashes(strip_tags($_POST["total_budget".$i])), 2, ".", "") : "";
                        $data[] = (isset($_POST["consultingVal".$i]) && is_numeric($_POST["consultingVal".$i])) ? number_format(addslashes(strip_tags($_POST["consultingVal".$i])), 2, ".", "") : "";
                        $data[] = (isset($_POST["expensesVal".$i]) && is_numeric($_POST["expensesVal".$i])) ? number_format(addslashes(strip_tags($_POST["expensesVal".$i])), 2, ".", "") : "";
                        $data[] = (isset($_POST["status".$i])) ? addslashes(strip_tags($_POST["status".$i])) : "";
                        $data[] = (isset($_POST["teamManage".$i])) ? addslashes(strip_tags($_POST["teamManage".$i])) : "";
                        $data[] = (isset($_POST["request".$i])) ? addslashes(strip_tags($_POST["request".$i])) : "";
                        $data[] = (isset($_POST["invoiceCycle".$i])) ? addslashes(strip_tags($_POST["invoiceCycle".$i])) : "";

                        $memberTblCntr = (isset($_POST["member".$i."TblCntr"])) ? addslashes(strip_tags($_POST["member".$i."TblCntr"])) : 0;

                        if ($memberTblCntr >= 1)  {
                            unset($members);

                            for ($j = 0; $j < $memberTblCntr; $j++)  {
                                $members[$j][] = (isset($_POST["hiddenUserID".$data[2]."_".$j])) ? addslashes(strip_tags($_POST["hiddenUserID".$data[2]."_".$j])) : "";
                                $members[$j][] = (isset($_POST["spID".$data[2]."_".$j])) ? addslashes(strip_tags($_POST["spID".$data[2]."_".$j])) : "";
                                $members[$j][] = (isset($_POST["company_id".$data[2]."_".$j])) ? addslashes(strip_tags($_POST["company_id".$data[2]."_".$j])) : "";
                                $members[$j][] = (isset($_POST["userID".$data[2]."_".$j])) ? addslashes(strip_tags($_POST["userID".$data[2]."_".$j])) : "";
                                $members[$j][] = (isset($_POST["userID".$data[2]."_".$j]) && is_numeric($_POST["userID".$data[2]."_".$j])) ? q("SELECT CONCAT(lstname, ', ', frstname, ' [', email, ']') FROM Employee WHERE id = '".addslashes(strip_tags($_POST["userID".$data[2]."_".$j]))."'") : "";
                                $members[$j][] = (isset($_POST["manager".$data[2]."_".$j])) ? addslashes(strip_tags($_POST["manager".$data[2]."_".$j])) : "";
                                $members[$j][] = (isset($_POST["budget".$data[2]."_".$j]) && is_numeric($_POST["budget".$data[2]."_".$j])) ? number_format(addslashes(strip_tags($_POST["budget".$data[2]."_".$j])), 2, ".", "") : "";
                                $members[$j][] = (isset($_POST["rate".$data[2]."_".$j])) ? addslashes(strip_tags($_POST["rate".$data[2]."_".$j])) : "";
                            }

                            $data[] = $members;
                        }

                        if (is_array($data))
                            $team[] = $data;
                    }
                }
            }

        }

        if (!is_array($team))   {
            $team[$r] = array("","",$_SESSION["company_id"],q("SELECT name FROM Company WHERE id = '".$_SESSION["company_id"]."'"),"","","","1","1",
                                q("SELECT id FROM dropdowns WHERE categoryID = (SELECT id FROM categories WHERE code = 'invoiceCycle') AND active = 1 AND deflt = 1"));
        }

        return $team;
    }

    function getSPInfo($id) {
        unset($info);

        if ($id != "")  {
            $data = q("SELECT p.id,p.company_id,p.name,p.number,p.descr,p.prefix,p.reg_date,p.manager_id,c.total_budget,c.consulting,c.expenses,c.vat,p.due_date,c.invoiceCycle,p.travelRate ".
                        "FROM Project AS p INNER JOIN companiesOnTeam AS c ON p.id = c.project_id WHERE p.id = '".$id."' AND c.company_id = 0");

            if (is_array($data))
                $info = $data[0];
        }
        else    {
            global $today;

            $info[] = (isset($_POST["id"])) ? addslashes(strip_tags($_POST["id"])) : "";
            $info[] = addslashes(strip_tags($_SESSION["company_id"]));
            $info[] = (isset($_POST["name"])) ? addslashes(strip_tags($_POST["name"])) : "";
            $info[] = (isset($_POST["number"])) ? addslashes(strip_tags($_POST["number"])) : "";
            $info[] = (isset($_POST["descr"])) ? addslashes(strip_tags($_POST["descr"])) : "";
            $info[] = (isset($_POST["prefix"])) ? addslashes(strip_tags($_POST["prefix"])) : "";
            $info[] = (isset($_POST["reg_date"])) ? addslashes(strip_tags($_POST["reg_date"])) : $today;
            $info[] = (isset($_POST["manager"])) ? $_POST["manager"] : "";
            $info[] = (isset($_POST["total_budget_"]) && is_numeric($_POST["total_budget_"])) ? number_format(addslashes(strip_tags($_POST["total_budget_"])), 2, ".", "") : "";
            $info[] = (isset($_POST["consultingVal_"]) && is_numeric($_POST["consultingVal_"])) ? number_format(addslashes(strip_tags($_POST["consultingVal_"])), 2, ".", "") : "";
            $info[] = (isset($_POST["expensesVal_"]) && is_numeric($_POST["expensesVal_"])) ? number_format(addslashes(strip_tags($_POST["expensesVal_"])), 2, ".", "") : "";
            $info[] = (isset($_POST["vat"])) ? addslashes(strip_tags($_POST["vat"])) : addslashes(strip_tags($_SESSION["VAT"]));
            $info[] = (isset($_POST["due_date"])) ? addslashes(strip_tags($_POST["due_date"])) : "";
            $info[] = (isset($_POST["invoiceCycle"])) ? $_POST["invoiceCycle"] : "";
            $info[] = (isset($_POST["travelRate"])) ? $_POST["travelRate"] : "";
        }

        return $info;
    }

    if (isset($_POST["save"]) && $_POST["save"] === "1")    {
        $errorMessage = "";

        $fields = array("id","company_id","name","number","descr","prefix","reg_date","manager_id","total_budget","consulting","expenses","vat","due_date","invoiceCycle","travelRate");
        $projectInfo = getSPInfo("");
        $date = date("Y-m-d H:i:s");
        $user = $_SESSION["email"];

        if ($projectInfo[0] == "")  {                       //  DO INSERT
            $query = "INSERT INTO Project (";
            $values = "('";

            for ($i = 1; $i < count($fields); $i++)    {
                if ($i < 7 || $i > 13 || $i == 12)   {
                    $query .= $fields[$i].",";
                    $values .= $projectInfo[$i]."','";
                }
            }

            $query .= "p_type,dateCreated,createdBy,dateUpdated,updatedBy) VALUES ";
            $values .= "SP','".$date."','".$user."','".$date."','".$user."')";

            $query .= $values;

            $insert = q($query);

            //COT
            $pid_ = q("SELECT id FROM Project WHERE company_id = '".$projectInfo[1]."' AND name = '".$projectInfo[2]."'");
            $query = "INSERT INTO companiesOnTeam (";
            $values = "('";

            for ($i = 1; $i < count($fields); $i++)    {
                if (!($i < 8 || $i > 13 || $i == 12))   {
                    $query .= $fields[$i].",";
                    $values .= $projectInfo[$i]."','";
                }
            }

            $query .= "company_id,project_id,dateCreated,createdBy,dateUpdated,updatedBy) VALUES ";
            $values .= "0','$pid_','".$date."','".$user."','".$date."','".$user."')";

            $query .= $values;

            $insert = q($query);

            if ($insert)   {
                $logs = q("INSERT INTO Logs (what,access,on_table,by_user,on_date,on_time,company_id) VALUES ".
                            "('".$projectInfo[2]." created','Insert','Project','".$user."','".date("Y-m-d")."','".date("H:i:s")."','".$projectInfo[1]."')");
            }
        }
        else if (is_numeric($projectInfo[0]))    {          //  DO UPDATE
            $query = "UPDATE Project SET ";

            for ($i = 1; $i < count($fields); $i++)    {
                if ($i < 5 || $i > 11  || $i == 10) 	{
                    $query .= $fields[$i]." = '".$projectInfo[$i]."',";
                }
            }

            $query .= "dateUpdated = '".$date."',updatedBy = '".$user."' WHERE id = '".$projectInfo[0]."'";

            $update = q($query);

            //COT
            $query = "UPDATE companiesOnTeam SET ";

            for ($i = 1; $i < count($fields); $i++)    {
                if (!($i < 6 || $i > 11 || $i == 10))	{
                    $query .= $fields[$i]." = '".$projectInfo[$i]."',";
                }
            }

            $query .= "dateUpdated = '".$date."',updatedBy = '".$user."' WHERE project_id = '".$projectInfo[0]."' AND company_id = 0";

            $update = q($query);

            if ($update)   {
                $logs = q("INSERT INTO Logs (what,access,on_table,by_user,on_date,on_time,company_id) VALUES ".
                            "('".$projectInfo[2]." updated','Update','Project','".$user."','".date("Y-m-d")."','".date("H:i:s")."','".$projectInfo[1]."')");
            }
        }

        if (is_numeric($projectInfo[0]) && $projectInfo[0] != "")
            $spID = $projectInfo[0];
        else
            $spID = q("SELECT id FROM Project WHERE company_id = '".$projectInfo[1]."' AND name = '".$projectInfo[2]."'");

        $projectTeam = getSPTeam("");

        if (is_array($projectTeam)) {
            foreach ($projectTeam as $pt)   {
                if ($pt[0] == "")  {                        //  DO INSERT
                    $action = "Insert";
                    $msg = $pt[3]." added to project team for ".$projectInfo[2];
                    $query = "INSERT INTO companiesOnTeam (project_id,company_id,total_budget,consulting,expenses,status,teamManage,invoiceCycle,dateCreated,createdBy,dateUpdated,".
                                "updatedBy) VALUES ";
                    $values = "('".$spID."','".$pt[2]."','".$pt[4]."','".$pt[5]."','".$pt[6]."','".$pt[7]."','".$pt[8]."','".$pt[10]."','".$date."','".$user."',".
                                "'".$date."','".$user."')";

                    $query .= $values;
                }
                else if (is_numeric($pt[0]))    {           //  DO UPDATE
                    $action = "Update";

                    $oldTotalBudget = q("SELECT total_budget FROM companiesOnTeam WHERE id = '".$pt[0]."'");
                    $overBudget = q("SELECT overBudget FROM companiesOnTeam WHERE id = '".$pt[0]."'");

                    if ($overBudget != "0")     {
                        if (number_format($oldTotalBudget, 2, ".", "") != number_format($pt[4], 2, ".", ""))
                            $overBudget = 0;
                    }

                    $msg = $pt[3]." details updated for ".$projectInfo[2];
                    $query = "UPDATE companiesOnTeam SET project_id = '".$spID."',company_id = '".$pt[2]."',total_budget = '".$pt[4]."',consulting = '".$pt[5]."',".
                                "expenses = '".$pt[6]."',status = '".$pt[7]."',teamManage = '".$pt[8]."',invoiceCycle = '".$pt[10]."',dateUpdated = '".$date."',".
                                "updatedBy = '".$user."',overBudget = '".$overBudget."' WHERE id = '".$pt[0]."'";
                }

                $runQuery = q($query);

                if ($runQuery)   {
                    //if ($pt[9] == "1" && is_numeric($pt[0]))    $update = q("UPDATE companiesOnTeam SET locked = '0' WHERE id  = '".$pt[0]."'");

                    //if (($action == "Insert" && $pt[7] == "0") || $pt[9] == "1")
                    if (($action == "Insert" && $pt[7] == "0"))
                        sp_Invite($spID, $projectInfo[2], $pt[2]);

                    $logs = q("INSERT INTO Logs (what,access,on_table,by_user,on_date,on_time,company_id) VALUES ".
                                "('".$msg." updated','".$action."','companiesOnTeam','".$user."','".date("Y-m-d")."','".date("H:i:s")."','".$projectInfo[1]."')");
                }

                if (is_array($pt[11]))  {
                    foreach ($pt[11] as $u) {
                        if ($u[0] == "")  {                 //  DO INSERT
                            $action = "Insert";
                            $msg = $u[3]." added to project team for ".$projectInfo[2];

                            $manager = ($u[5] == "") ? 0 : 1;

                            //$query = "INSERT INTO Project_User (project_id,company_id,user_id,manager,user_budget,user_tariff,dateCreated,createdBy,dateUpdated,updatedBy) VALUES ";
                            $query = "INSERT INTO Project_User (project_id,company_id,user_id,manager,user_budget,rateID,dateCreated,createdBy,dateUpdated,updatedBy) VALUES ";
                            $values = "('".$spID."','".$u[2]."','".$u[3]."','".$manager."','".$u[6]."','".$u[7]."','".$date."','".$user."','".$date."','".$user."')";

                            $query .= $values;
                        }
                        else if (is_numeric($u[0]))    {    //  DO UPDATE
                            $action = "Update";
                            $msg = $u[3]." details updated ".$projectInfo[2];

                            $manager = ($u[5] == "") ? 0 : 1;

                            //$query = "UPDATE Project_User SET project_id = '".$spID."',company_id = '".$u[2]."',user_id = '".$u[3]."',manager = '".$manager."',".
                            //            "user_budget = '".$u[6]."',user_tariff = '".$u[7]."',dateUpdated = '".$date."',updatedBy = '".$user."' WHERE id  = '".$u[0]."'";
                            $query = "UPDATE Project_User SET project_id = '".$spID."',company_id = '".$u[2]."',user_id = '".$u[3]."',manager = '".$manager."',".
                                        "user_budget = '".$u[6]."',rateID = '".$u[7]."',dateUpdated = '".$date."',updatedBy = '".$user."' WHERE id  = '".$u[0]."'";
                        }

                        $runQuery = q($query);

                        if ($runQuery)   {
                            $logs = q("INSERT INTO Logs (what,access,on_table,by_user,on_date,on_time,company_id) VALUES ".
                                        "('".$msg." updated','".$action."','Project_User','".$user."','".date("Y-m-d")."','".date("H:i:s")."','".$projectInfo[1]."')");
                        }
                    }
                }
            }
        }

        //  Deleting SPM's From Project
        $update = q("UPDATE Project_User SET spm_manager = '0' WHERE project_id = '".$spID."' AND spm_manager = '1'");  //  All have to be set to manager=0 anyway, skip the loop below then
//        $spm_managers = q("SELECT user_id FROM Project_User WHERE project_id = '".$spID."' AND spm_manager = '1'");

//        if (is_array($spm_managers))   {
//            foreach ($spm_managers as $spm) {
//                if (!in_array($spm[0], $projectInfo[4]))
//                    $delete = q("DELETE FROM Project_User WHERE project_id = '".$spID."' AND user_id = '".$spm[0]."' AND spm_manager = '1'");
//            }
//        }

        //  Inserting SPM's On Project
        if (is_array($projectInfo[4])) {
            foreach ($projectInfo[4] as $spm)  {
                $update = q("UPDATE Project_User SET spm_manager = '1' WHERE project_id = '".$spID."' AND user_id = '".$spm."' AND company_id = '".$_SESSION["company_id"]."'");
//                $exist = q("SELECT * FROM Project_User WHERE project_id = '".$spID."' AND user_id = '".$spm."' AND spm_manager = '1'");

//                if (!is_array($exist))
//                    $insert = q("INSERT INTO Project_User (project_id,company_id,user_id,spm_manager,dateCreated,createdBy,dateUpdated,updatedBy) VALUES ".
//                                "('".$spID."','".addslashes(strip_tags($_SESSION["company_id"]))."','".$spm."','1','".$date."','".$user."','".$date."','".$user."')");
            }
        }


        header("Location: sp_projects.php");
    }
    else
        $projectInfo = getSPInfo($_POST["id"]);

    $index = 1;
    $currency = "<i>(".$_SESSION["currency"].")</i>";

    if ($errorMessage != "")    {
        if ($errorMessage == "Please note that we are currently busy with data management, no entries are allowed for 2010-08-26. Please check back in 24 Hours.")
            echo "<p align='center' style='padding:0px;'><strong style='font-size:12px;'><font class='on-validate-error'>$errorMessage</font></strong></p>";
        else
            echo "<p align='center' style='padding:0px;'><strong><font class='on-validate-error'>$errorMessage</font></strong></p>";
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus();
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<link rel="stylesheet" type="text/css" href="include/jquery.autocomplete.css" />
<script type='text/javascript' src='include/jquery.autocomplete.js'></script>
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script type="Text/JavaScript">
	function is_array(input){
		return typeof(input) == "object" && (input instanceof Array);
	}

	var hiddenField = "";
	var companyvalue;
	var usersList;

	jQuery(function()    {
		//  Calendar(s)
		jQuery("#reg_date").DatePicker( {
			format:"Y-m-d",
			date: jQuery("#reg_date").val(),
			current: jQuery("#reg_date").val(),
			starts: 1,
			position: "right",
			onBeforeShow: function()    {
				var _date = (!validation("date", jQuery("#reg_date").val())) ? new Date() : jQuery("#reg_date").val();
				jQuery("#reg_date").DatePickerSetDate(_date, true);
			},
			onChange: function(formated, dates) {
				jQuery("#reg_date").val(formated);
				jQuery("#reg_date").DatePickerHide();
			}
		});

		jQuery("#due_date").DatePicker( {
			format:"Y-m-d",
			date: jQuery("#due_date").val(),
			current: jQuery("#due_date").val(),
			starts: 1,
			position: "right",
			onBeforeShow: function()    {
				var _date = (!validation("date", jQuery("#due_date").val())) ? new Date() : jQuery("#due_date").val();
				jQuery("#due_date").DatePickerSetDate(_date, true);
			},
			onChange: function(formated, dates) {
				jQuery("#due_date").val(formated);
				jQuery("#due_date").DatePickerHide();
			}
		});

		//////////////////////////////
		//  Autocomplete Functions
		function findValue(li)  {
			if (li == null) return alert("No match!");

			var sValue = li.extra[0];
			var tValue = li.selectValue;
		}

		function selectItem(li) {
			findValue(li);
		}

		function setVariables(field, fieldValue, fieldClass)    {
			if (field == "companyName")   {
				var companies = jQuery("#hiddenCompanies").val();

				jQuery.post("_ajax.php", {func: "setSessionVariable", sessionName: "companies", sessionValue: companies}, function(data){});
			}
			else if (fieldClass == "getUser")  {
				companyValue = fieldValue;
				usersList = jQuery("#hiddenUsers"+fieldValue).val();

				jQuery.post("_ajax.php", {func: "setSessionVariable", sessionName: "companyValue#usersList", sessionValue: companyValue+"#"+usersList}, function(data){});
			}
		}

		jQuery(".autocomplete, .getUser").focus(function() {
			if (jQuery(this).attr("id") == "companyName")   {
				setVariables("companyName", jQuery("#hiddenCompanies").val(), "");
			}
			else if (jQuery(this).hasClass("getUser"))  {
				setVariables(jQuery(this).attr("id"), jQuery(this).attr("name"), "getUser");
			}
		});

		<?php
			jQAutoComplete("#", "companyName", "get_companies");
			jQAutoComplete(".", "getUser", "get_users");
		?>
		//////////////////////////////

		//////////////////////////////
		//  Team Creation
		jQuery(".addCompanyBtn, .addUserBtn").click(function()    {
			var inputField = "#"+jQuery(this).attr("name");
			var inputValue = jQuery(inputField).val();
			var tbl = "#"+jQuery(this).attr("id");

			jQuery(inputField).val("");

			var alreadyInList = false;

			if (inputField == "#companyName")	{
				jQuery("table"+tbl+" tbody>tr").not(":first").not(":last").each(function(i,v){
					if (jQuery(this).children().eq(0).children().eq(3).text() == inputValue)	alreadyInList = true;
				});
			} else	{
				jQuery("table"+tbl+" tbody>tr").not(":first").not(":last").each(function(i,v){
					if (jQuery(this).children().eq(0).children().eq(4).text() == inputValue)	alreadyInList = true;
				});
			}

			if (inputValue != "" && !alreadyInList)   {
				var query = (inputField == "#companyName") ? "SELECT id FROM Company WHERE name = '" + addslashes(inputValue) + "'"
															: "SELECT id FROM Employee WHERE CONCAT(lstname, ', ', frstname, ' [', email, ']') = '" + addslashes(inputValue) + "'";

				jQuery.post("_ajax.php", {func: "query_direct", query: query}, function(data)	{
					data = json_decode(data);

					var check = data;

					if (check != 0 && validation("number", check))   {
						var field;

						var hiddenTbl = (inputField == "#companyName") ? "#hidden_teamTbl" : "#hidden_memberTbl";
						var tblCntr = tbl+"Cntr";
						var tblDiv = tbl.replace("Tbl", "Div");
						var tblLstRow = tbl+"LstRow";

						if (inputField != "#companyName")
							var company_id = jQuery(inputField).attr("name");

						var counter = jQuery(tblCntr).val();
						var lstRow = jQuery(hiddenTbl + " tbody>tr:last");

						var columns = (inputField == "#companyName") ? 10 : 5;
						var n = lstRow.clone(true);

						for (var i = 0; i < columns; i++)   {
							n.children().eq(i).children().each(function(j, v) {
								if (n.children().eq(i).children().eq(j).attr("id") != "")   {
									field = n.children().eq(i).children().eq(j).attr("id");

									field = (inputField == "#companyName") ? field + counter : field + company_id + "_" + counter;

									n.children().eq(i).children().eq(j).attr("id", field);
									n.children().eq(i).children().eq(j).attr("name", field)
								}
							});
						}

						n.insertBefore(jQuery(tblLstRow));

						var spID = jQuery("#spID").val();
						var hiddenValue = (inputField == "#companyName") ? jQuery("#hiddenCompanies").val() : jQuery("#hiddenUsers"+company_id).val();

						if (inputField == "#companyName")   {
							n.children().eq(0).children().eq(0).attr("value", "");
							n.children().eq(0).children().eq(1).attr("value", spID);
							n.children().eq(0).children().eq(2).attr("value", data);

							n.children().eq(0).children().eq(3).text(inputValue);

							hiddenValue = hiddenValue + data + "_";

							jQuery("#hiddenCompanies").val(hiddenValue);

							jQuery("#invoiceCycle option").each(function(i, v) {
								field = "#"+n.children().eq(8).children().eq(0).attr("id");

								jQuery(field).append(jQuery("<option></option>").attr("value",jQuery(this).val()).text(jQuery(this).text()));

								if (jQuery(this).attr("selected"))   jQuery(field+" option:last").attr("selected", "selected");
							});
						}
						else    {
							n.children().eq(0).children().eq(0).attr("value", "");
							n.children().eq(0).children().eq(1).attr("value", spID);
							n.children().eq(0).children().eq(2).attr("value", company_id);
							n.children().eq(0).children().eq(3).attr("value", data);

							n.children().eq(0).children().eq(4).text(inputValue);

							hiddenValue = hiddenValue + data + "_";

							jQuery("#hiddenUsers"+company_id).val(hiddenValue);

							jQuery.post("_ajax.php", {func: "get_user_rates", userID: data, companyID: company_id}, function(data)	{
								data = json_decode(data);

								field = "#"+n.children().eq(3).children().eq(0).attr("id");

								jQuery.each(data,function(i, v)	{
									jQuery(field).append(jQuery("<option></option>").attr("value",v[0]).text(v[1]));
								});
							});

							jQuery.post("_ajax.php", {func: "check_pm_status", userID: data, companyID: company_id}, function(data)	{
								data = json_decode(data);

								field = "#"+n.children().eq(1).children().eq(0).attr("id");

								if (data != 1)  jQuery(field).attr("disabled", "true");
							});
						}

						if (inputField == "#companyName")
							setVariables("companyName", jQuery("#hiddenCompanies").val(), "");
						else
							setVariables("", company_id, "getUser");

						jQuery(inputField).val("").focus();

						counter++;

						if (counter > 0)    jQuery(tblDiv).show();

						jQuery(tblCntr).val(counter);

						jQuery(inputField)[0].autocompleter.flushCache();
					}
				});
			}
		});

		jQuery(".showUsers").click(function()    {
			var divName = "#show"+jQuery(this).attr("id");

			if (jQuery(this).children().eq(1).text() == "Hide User(s) For "+jQuery(this).attr("name"))    {
				var imgSrc = "images/icons/expand.ico";
				var title = "Expand";
				var display = "Show User(s) For "+jQuery(this).attr("name");
			}
			else    {
				var imgSrc = "images/icons/collapse.ico";
				var title = "Collapse";
				var display = "Hide User(s) For "+jQuery(this).attr("name");
			}

			jQuery(this).children().eq(0).attr("src", imgSrc);
			jQuery(this).children().eq(0).attr("title", title);
			jQuery(this).children().eq(1).text(display);

			jQuery(divName).toggle();
		});

		jQuery(".resendInvite").click(function()    {
			jQuery(this).attr("disabled", "disabled");

			var projectID = jQuery("#id").val();
			var projectName = jQuery("#name").val();
			var companyID = jQuery("#company_id"+(jQuery(this).attr("id").replace("request", ""))).val();

			jQuery.post("_ajax.php", {func: "resendInvite", projectID: projectID, projectName: projectName, companyID: companyID}, function(data)	{});
		});

		jQuery(".deleteCompanyBtn").click(function()    {
			var value = jQuery(this).parent().parent().children().eq(0).children().eq(2).attr("value");
			var teamManage = jQuery(this).parent().parent().children().eq(7).children().eq(1).attr("value");

			var hiddenValue = jQuery("#hiddenCompanies").val();

			hiddenValue = hiddenValue.replace(value+"_", "");

			jQuery("#hiddenCompanies").val(hiddenValue);

			if (teamManage == "1")      {
				var divName = jQuery(this).parent().parent().children().eq(7).children().eq(1).attr("id");

				divName = "#showUsers"+divName.replace("teamManage", "");

				jQuery(divName).parent().parent().remove();
			}

			jQuery(this).parent().parent().remove();

			var query = "DELETE FROM companiesOnTeam WHERE id = '" + jQuery(this).attr("id") + "'";

			jQuery.post("_ajax.php", {func: "query_direct", query: query}, function(data)	{});
		});

		jQuery(".deleteUserBtn").click(function()    {
			var company_id = jQuery(this).parent().parent().children().eq(0).children().eq(2).attr("value");
			var value = jQuery(this).parent().parent().children().eq(0).children().eq(3).attr("value");

			var hiddenValue = jQuery("#hiddenUsers"+company_id).val();

			hiddenValue = hiddenValue.replace(value+"_", "");

			jQuery("#hiddenUsers"+company_id).val(hiddenValue);

			jQuery(this).parent().parent().remove();

			var query = "DELETE FROM Project_User WHERE id = '" + jQuery(this).attr("id") + "'";

			jQuery.post("_ajax.php", {func: "query_direct", query: query}, function(data)	{});
		});
		//////////////////////////////

		//////////////////////////////
		//  Main SP Budget
		jQuery("#total_budget_, #consultingPer_, #expensesPer_").change(function()    {
			var total_budget = jQuery("#total_budget_").val();
			var consultingPer = jQuery("#consultingPer_").val();
			var expensesPer = jQuery("#expensesPer_").val();

			//  Consulting
			if (consultingPer != "" && total_budget != "" && validation("percentage", consultingPer) && validation("currency", total_budget))  {
				var value = total_budget * (consultingPer / 100);

				jQuery("#consultingVal_").val(value);
			}

			//  Expenses
			if (expensesPer != "" && total_budget != "" && validation("percentage", expensesPer) && validation("currency", total_budget))  {
				var value = total_budget * (expensesPer / 100);

				jQuery("#expensesVal_").val(value);
			}
		});

		jQuery("#total_budget_, #consultingVal_, #expensesVal_").change(function()    {
			var total_budget = jQuery("#total_budget_").val();
			var consultingVal = jQuery("#consultingVal_").val();
			var expensesVal = jQuery("#expensesVal_").val();

			//  Consulting
			if (consultingVal != "" && total_budget != "" && validation("currency", consultingVal) && validation("currency", total_budget))  {
				var value =  (consultingVal / total_budget) * 100;

				jQuery("#consultingPer_").val(value);
			}

			//  Expenses
			if (expensesVal != "" && total_budget != "" && validation("currency", expensesVal) && validation("currency", total_budget))  {
				var value = (expensesVal / total_budget) * 100;

				jQuery("#expensesPer_").val(value);
			}
		});

		jQuery("#consultingVal_, #expensesVal_").change(function()    {
			var total_budget = jQuery("#total_budget_").val();
			var consultingVal = jQuery("#consultingVal_").val();
			var expensesVal = jQuery("#expensesVal_").val();

			//  Consulting
			if (consultingVal != "" && expensesVal != "" && validation("currency", consultingVal) && validation("currency", expensesVal))  {
				total_budget = parseFloat(consultingVal) + parseFloat(expensesVal);

				jQuery("#total_budget_").val(total_budget.toFixed(2));

				//  Consulting
				if (consultingVal != "" && total_budget != "" && validation("currency", consultingVal) && validation("currency", total_budget))  {
					var value =  (consultingVal / total_budget) * 100;

					jQuery("#consultingPer_").val(value.toFixed(2));
				}

				//  Expenses
				if (expensesVal != "" && total_budget != "" && validation("currency", expensesVal) && validation("currency", total_budget))  {
					var value = (expensesVal / total_budget) * 100;

					jQuery("#expensesPer_").val(value.toFixed(2));
				}
			}
		});
		//////////////////////////////

		//////////////////////////////
		//  Add/Update Button Click
		jQuery("#btnAddEdit").click(function()    {
			var valid = true;
			var test;

			//  Check That Project Does Not Exist
			var id = jQuery("#id").val();
			var name = jQuery("#name").val();
			var company_id = jQuery("#company_id").val();

			var query = (id == "") ? "SELECT * FROM Project WHERE name = '"+addslashes(name)+"' AND company_id = '"+company_id+"'"
								: "SELECT * FROM Project WHERE name = '"+addslashes(name)+"' AND company_id = '"+company_id+"' AND id != '"+id+"'";

			jQuery.post("_ajax.php", {func: "query_direct", query: query}, function(data)	{
				data = json_decode(data);

				test = !is_array(data);

				if (!test)  jQuery("#nameDuplicate").show();
				else        jQuery("#nameDuplicate").hide();

				valid &= test;

				var fields = new Array("name","reg_date","total_budget_","consultingVal_","expensesVal_","vat","due_date");

				for (var i = 0; i < fields.length; i++)  {
					var field = fields[i];
					var cls = jQuery("#"+field).attr("class");

					cls = cls.replace(/ project/gi, "");
					cls = (cls != "") ? cls.split(" ") : "";

					if (cls != "")  {
						for (var j = 0; j < cls.length; j++)  {
							var divName = (cls[j] == "entered") ? "#"+field+"Empty" : "#"+field+"Div";
							test = validation(cls[j], jQuery("#"+field).val());

							if (!test)  jQuery(divName).show();
							else        jQuery(divName).hide();

							valid &= test;
						}
					}
				}

				var spmError = true;
				var spmCount = 0;

				jQuery.each(jQuery("input[id='manager[]']"),function(index, value) {
					if (jQuery(value).attr("checked"))  spmCount++;
				});

				spmError &= (spmCount >= 1);

				if (!spmError)  jQuery("#SPMDiv").show();
				else            jQuery("#SPMDiv").hide();

				valid &= spmError;

				//  Check Budgets
				test = checkTotalBudget();

				if (!test)  jQuery("#total_budget_Used").show();
				else        jQuery("#total_budget_Used").hide();

				valid &= test;

				test = checkConsultingBudget();

				if (!test)  jQuery("#total_consulting_Used").show();
				else        jQuery("#total_consulting_Used").hide();

				valid &= test;

				test = checkExpenseBudget();

				if (!test)  jQuery("#total_expenses_Used").show();
				else        jQuery("#total_expenses_Used").hide();

				valid &= test;

				test = checkCompanyBudget("_");

				if (!test)  jQuery("#total_budget_Over").show();
				else        jQuery("#total_budget_Over").hide();

				valid &= test;

				var companies = jQuery("#teamTblCntr").val();

				for (var j = 0; j < companies; j++) {
					test = checkCompanyBudget(j);

					if (!test)  jQuery("#total_budget_Over_"+j).show();
					else        jQuery("#total_budget_Over_"+j).hide();

					valid &= test;
				}

				//  Check That Each Company Has Total Budget, Consulting Budget & Expense Budget && That It Is Numeric
				for (var j = 0; j < companies; j++) {
					var fields = new Array("total_budget","consultingVal","expensesVal");

					for (var i = 0; i < fields.length; i++)  {
						var field = fields[i];
						var cls = jQuery("#"+field+j).attr("class");

						cls = (cls != "") ? cls.split(" ") : "";

						if (cls != "")  {
							for (var k = 0; k < cls.length; k++)  {
								var divName = (cls[k] == "entered") ? "#"+field+"Empty"+j : "#"+field+"Div"+j;
								test = validation(cls[k], jQuery("#"+field+j).val());

								if (!test)  jQuery(divName).show();
								else        jQuery(divName).hide();

								valid &= test;
							}
						}
					}
				}

				//  Check That User Budgets Not More Than Company Consulting Budget
				for (var j = 0; j < companies; j++) {
					var users = jQuery("#member"+j+"TblCntr").val();

					if (jQuery("#teamManage"+j).val() == "1" && users >= 1)  {
						var companyID = jQuery("#company_id"+j).parent().children().eq(2).val();
						var consultingBudget = parseFloat(jQuery("#consultingVal"+j).val());
						var userBudgets = 0;

						for (var k = 0; k < users; k++) {
							var userBudget = jQuery("#budget"+companyID+"_"+k).val();

							if (userBudget != "" && validation("currency", userBudget)) userBudgets += parseFloat(userBudget);
						}

						if (consultingBudget != "" && validation("currency", consultingBudget) && userBudgets != "" && validation("currency", userBudgets))     {
							test = (consultingBudget >= userBudgets);

							if (!test)  jQuery("#consultingOver_"+j).show();
							else        jQuery("#consultingOver_"+j).hide();

							valid &= test;
						}
					}
				}

				//  Check That Company Has At Least 1 Project Manager Selected
				var pmError = true;

				for (var j = 0; j < companies; j++) {
					var users = jQuery("#member"+j+"TblCntr").val();

					if (jQuery("#teamManage"+j).val() == "1" && users >= 1)  {
						var pmCount = 0;
						var companyID = jQuery("#company_id"+j).parent().children().eq(2).val();
						var companyName = jQuery("#company_id"+j).parent().children().eq(3).text();

						for (var k = 0; k < users; k++) {
							if (jQuery("#manager"+companyID+"_"+k).attr("checked"))  pmCount++;
						}

						if (!(pmCount >= 1))    {
							jQuery("#showUsers"+j).show();

							var imgSrc = "images/icons/collapse.ico";
							var title = "Collapse";
							var display = "Hide User(s) For "+companyName;
						}
						else    {
							jQuery("#showUsers"+j).hide();

							var imgSrc = "images/icons/expand.ico";
							var title = "Expand";
							var display = "Show User(s) For "+companyName;
						}

						jQuery("#showUsers"+j).parent().children().eq(0).children().eq(0).attr("src", imgSrc);
						jQuery("#showUsers"+j).parent().children().eq(0).children().eq(0).attr("title", title);
						jQuery("#showUsers"+j).parent().children().eq(0).children().eq(1).text(display);

						pmError &= (pmCount >= 1);
					}
				}

				if (!pmError)   jQuery("#teamPMDiv").show();
				else            jQuery("#teamPMDiv").hide();

				valid &= pmError;

				if (valid) {
					jQuery("#save").val("1");
					jQuery("#sp_projectsAddEdit").submit();
				}
			});
		});
		//////////////////////////////

		jQuery("input[id='manager[]']").click(function()   {
			if (jQuery(this).attr("checked"))   {
				var found = false;
				var field;
				var user_id = jQuery(this).val();
				var company_id = jQuery("#company_id").val();
				var companies = jQuery("#teamTblCntr").val();
				var position = 0;

				for (var i = 0; i < companies; i++) {
					field = "#company_id"+i;

					if (jQuery(field).val() == company_id)
						position = i;
				}

				var userList = jQuery("#hiddenUsers"+company_id).val();

				if (userList.lastIndexOf("_") != -1)
					userList = userList.substr(0, userList.lastIndexOf("_"));

				userList = userList.split("_");

				for (var i = 0; i < userList.length; i++) {
					if (userList[i] == user_id)
						found = true;
				}

				if (!found) {
					var query = "SELECT CONCAT(lstname, ', ', frstname, ' [', email, ']') FROM Employee WHERE id = '" + user_id + "'";

					jQuery.post("_ajax.php", {func: "query_direct", query: query}, function(data)	{
						var tbl = "#member"+position+"Tbl";

						var hiddenTbl = "#hidden_memberTbl";
						var tblCntr = tbl+"Cntr";
						var tblDiv = tbl.replace("Tbl", "Div");
						var tblLstRow = tbl+"LstRow";

						var company_id = jQuery("#company_id").val();

						var counter = jQuery(tblCntr).val();
						var lstRow = jQuery(hiddenTbl + " tbody>tr:last");

						var columns = 5;
						var n = lstRow.clone(true);

						for (var i = 0; i < columns; i++)   {
							n.children().eq(i).children().each(function(j, v) {
								if (n.children().eq(i).children().eq(j).attr("id") != "")   {
									field = n.children().eq(i).children().eq(j).attr("id");

									field = field + company_id + "_" + counter;

									n.children().eq(i).children().eq(j).attr("id", field);
									n.children().eq(i).children().eq(j).attr("name", field)
								}
							});
						}

						n.insertBefore(jQuery(tblLstRow));

						var spID = jQuery("#spID").val();
						var hiddenValue = jQuery("#hiddenUsers"+company_id).val();

						n.children().eq(0).children().eq(0).attr("value", "");
						n.children().eq(0).children().eq(1).attr("value", spID);
						n.children().eq(0).children().eq(2).attr("value", company_id);
						n.children().eq(0).children().eq(3).attr("value", user_id);

						n.children().eq(0).children().eq(4).text(data.replace(/\"/gi, ""));

						hiddenValue = hiddenValue + user_id + "_";

						jQuery("#hiddenUsers"+company_id).val(hiddenValue);

						jQuery.post("_ajax.php", {func: "get_user_rates", userID: user_id, companyID: company_id}, function(data)	{
							data = json_decode(data);

							field = "#"+n.children().eq(3).children().eq(0).attr("id");

							jQuery.each(data,function(i, v)	{
								jQuery(field).append(jQuery("<option></option>").attr("value",v[0]).text(v[1]));
							});
						});

						counter++;

						if (counter > 0)    jQuery(tblDiv).show();

						jQuery(tblCntr).val(counter);
					});
				}
			}
		});
	});

	//////////////////////////////
	//  Company SP Budget
	function percentageChange(field)    {
		var tmpField = field.name.substring(0, 5);
		var inputNumber;

		if (tmpField == "total")
			inputNumber = field.name.replace("total_budget", "");
		else if (tmpField == "consu")
			inputNumber = field.name.replace("consultingPer", "");
		else if (tmpField == "expen")
			inputNumber = field.name.replace("expensesPer", "");

		var total_budget = eval("document.forms['sp_projectsAddEdit'].total_budget"+inputNumber+".value;");
		var consultingPer = eval("document.forms['sp_projectsAddEdit'].consultingPer"+inputNumber+".value;");
		var expensesPer = eval("document.forms['sp_projectsAddEdit'].expensesPer"+inputNumber+".value;");
		var remaining = total_budget;

		//  Consulting
		if (consultingPer != "" && total_budget != "" && validation("percentage", consultingPer) && validation("currency", total_budget))  {
			var value = total_budget * (consultingPer / 100);

			eval("document.forms['sp_projectsAddEdit'].consultingVal"+inputNumber+".value = "+value.toFixed(2)+";");

			remaining -= value;
		}

		//  Expenses
		if (expensesPer != "" && total_budget != "" && validation("percentage", expensesPer) && validation("currency", total_budget))  {
			var value = total_budget * (expensesPer / 100);

			eval("document.forms['sp_projectsAddEdit'].expensesVal"+inputNumber+".value = "+value.toFixed(2)+";");

			remaining -= value;
		}

		if (total_budget != "")   eval("document.forms['sp_projectsAddEdit'].total_budget"+inputNumber+".value = '"+parseFloat(total_budget).toFixed(2)+"';");
		if (consultingPer != "")   eval("document.forms['sp_projectsAddEdit'].consultingPer"+inputNumber+".value = '"+parseFloat(consultingPer).toFixed(0)+"';");
		if (expensesPer != "")   eval("document.forms['sp_projectsAddEdit'].expensesPer"+inputNumber+".value = '"+parseFloat(expensesPer).toFixed(0)+"';");

		if (validation("currency", parseFloat((remaining / total_budget) * 100).toFixed(0)))
			eval("document.forms['sp_projectsAddEdit'].budgetRemain"+inputNumber+".value = '"+parseFloat((remaining / total_budget) * 100).toFixed(0)+"%';");
	}

	function currencyChange(field)  {
		var tmpField = field.name.substring(0, 5);
		var inputNumber;

		if (tmpField == "total")
			inputNumber = field.name.replace("total_budget", "");
		else if (tmpField == "consu")
			inputNumber = field.name.replace("consultingVal", "");
		else if (tmpField == "expen")
			inputNumber = field.name.replace("expensesVal", "");

		var total_budget = eval("document.forms['sp_projectsAddEdit'].total_budget"+inputNumber+".value;");
		var consultingVal = eval("document.forms['sp_projectsAddEdit'].consultingVal"+inputNumber+".value;");
		var expensesVal = eval("document.forms['sp_projectsAddEdit'].expensesVal"+inputNumber+".value;");
		var remaining = total_budget;

		//  Consulting
		if (consultingVal != "" && total_budget != "" && validation("currency", consultingVal) && validation("currency", total_budget))  {
			var value = (total_budget > 0) ? (consultingVal / total_budget) * 100 : 0;

			eval("document.forms['sp_projectsAddEdit'].consultingPer"+inputNumber+".value = "+value.toFixed(2)+";");

			remaining -= consultingVal;
		}

		//  Expenses
		if (expensesVal != "" && total_budget != "" && validation("currency", expensesVal) && validation("currency", total_budget))  {
			var value = (total_budget > 0) ? (expensesVal / total_budget) * 100 : 0;

			eval("document.forms['sp_projectsAddEdit'].expensesPer"+inputNumber+".value = "+value.toFixed(2)+";");

			remaining -= expensesVal;
		}

		if (total_budget != "")   eval("document.forms['sp_projectsAddEdit'].total_budget"+inputNumber+".value = '"+parseFloat(total_budget).toFixed(2)+"';");
		if (consultingVal != "")   eval("document.forms['sp_projectsAddEdit'].consultingVal"+inputNumber+".value = '"+parseFloat(consultingVal).toFixed(2)+"';");
		if (expensesVal != "")   eval("document.forms['sp_projectsAddEdit'].expensesVal"+inputNumber+".value = '"+parseFloat(expensesVal).toFixed(2)+"';");

		if (validation("currency", parseFloat((remaining / total_budget) * 100).toFixed(0)))
			eval("document.forms['sp_projectsAddEdit'].budgetRemain"+inputNumber+".value = '"+parseFloat((remaining / total_budget) * 100).toFixed(0)+"%';");
	}

	function checkCompanyBudget(field)  {
		var budget = eval("document.forms['sp_projectsAddEdit'].total_budget"+field+".value;");
		var consulting = eval("document.forms['sp_projectsAddEdit'].consultingVal"+field+".value;");
		var expenses = eval("document.forms['sp_projectsAddEdit'].expensesVal"+field+".value;");
		var total = parseFloat(0);

		if (budget != "" && validation("currency", budget))  {
			budget = parseFloat(budget);

			if (consulting != "" && validation("currency", consulting)) total += parseFloat(consulting);
			if (expenses != "" && validation("currency", expenses))     total += parseFloat(expenses);

			return (total <= budget) ? true : false;
		}

		return true;
	}

	function checkTotalBudget()  {
		var total_budget = eval("document.forms['sp_projectsAddEdit'].total_budget_.value;");

		if (total_budget != "" && validation("currency", total_budget))  {
			var companies = eval("document.forms['sp_projectsAddEdit'].teamTblCntr.value;");

			var total_company_budget = parseFloat(0);

			for (var i = 0; i < companies; i++) {
				var budget = eval("document.forms['sp_projectsAddEdit'].total_budget"+i+".value;");

				if (budget != "" && validation("currency", budget))  {
					total_company_budget += parseFloat(budget);
				}
			}

			return (total_company_budget <= total_budget) ? true : false;
		}

		return true;
	}

	function checkConsultingBudget()  {
		var total_budget = eval("document.forms['sp_projectsAddEdit'].consultingVal_.value;");

		if (total_budget != "" && validation("currency", total_budget))  {
			var companies = eval("document.forms['sp_projectsAddEdit'].teamTblCntr.value;");

			var total_company_budget = parseFloat(0);

			for (var i = 0; i < companies; i++) {
				var budget = eval("document.forms['sp_projectsAddEdit'].consultingVal"+i+".value;");

				if (budget != "" && validation("currency", budget))  {
					total_company_budget += parseFloat(budget);
				}
			}

			return (total_company_budget <= total_budget) ? true : false;
		}

		return true;
	}

	function checkExpenseBudget()  {
		var total_budget = eval("document.forms['sp_projectsAddEdit'].expensesVal_.value;");

		if (total_budget != "" && validation("currency", total_budget))  {
			var companies = eval("document.forms['sp_projectsAddEdit'].teamTblCntr.value;");

			var total_company_budget = parseFloat(0);

			for (var i = 0; i < companies; i++) {
				var budget = eval("document.forms['sp_projectsAddEdit'].expensesVal"+i+".value;");

				if (budget != "" && validation("currency", budget))  {
					total_company_budget += parseFloat(budget);
				}
			}

			return (total_company_budget <= total_budget) ? true : false;
		}

		return true;
	}
	//////////////////////////////
</script>
<table width="100%">
    <tr height="380px">
        <td class="centerdata">
            <form id="sp_projectsAddEdit" name="sp_projectsAddEdit" action="" method="post">
                <table width="100%">
                    <tr>
                        <td class="centerdata">
                            <h6>
                                <?php # ($projectInfo[0] == "") ? "Add" : "Update"; ?> Shared Project
                            </h6>
                        </td>
                        <td width="25%"></td>
                    </tr>
                </table>
                <br/>
                <table class="on-table-center" width="70%">
                    <input id="id" name="id" type="hidden" value="<?php echo stripslashes($projectInfo[0]); ?>">
                    <input id="company_id" name="company_id" type="hidden" value="<?php echo stripslashes($projectInfo[1]); ?>">
                    <tr>
                        <td class="on-description" width="25%">
                            Shared Project Name:
                        </td>
                        <td width="50%">
                            <input id="name" name="name" type="text" class="on-field required project" tabindex="<?php echo $index++; ?>" size="50" value="<?php echo stripslashes($projectInfo[2]); ?>">
                            <div id="nameDiv" name="nameDiv" class="error"><font class="on-validate-error">* Project name must be entered</font></div>
                            <div id="nameDuplicate" name="nameDuplicate" class="error"><font class="on-validate-error">* Project name already in use</font></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="on-description" width="25%">
                            Shared Project Number:
                        </td>
                        <td width="50%">
                            <input id="number" name="number" type="text" class="on-field" tabindex="<?php echo $index++; ?>" size="50" value="<?php echo stripslashes($projectInfo[3]); ?>">
                        </td>
                    </tr>
                    <tr>
                        <td class="on-description" width="25%">
                            Shared Project Description:
                        </td>
                        <td width="50%">
                            <input id="descr" name="descr" type="text" class="on-field" tabindex="<?php echo $index++; ?>" size="50" value="<?php echo stripslashes($projectInfo[4]); ?>">
                        </td>
                    </tr>
                    <tr>
                        <td class="on-description" width="25%">
                            Shared Project Invoice Prefix:
                        </td>
                        <td width="50%">
                            <input id="prefix" name="prefix" type="text" class="on-field" tabindex="<?php echo $index++; ?>" size="50" value="<?php echo stripslashes($projectInfo[5]); ?>">
                        </td>
                    </tr>
                    <tr>
                        <td class="on-description" width="25%">
                            Registration Date:
                        </td>
                        <td width="50%">
                            <input id="reg_date" name="reg_date" type="text" class="on-field-date entered date" tabindex="<?php echo $index++; ?>" value="<?php echo $projectInfo[6]; ?>">
                            <div id="reg_dateEmpty" name="reg_dateEmpty" class="error"><font class="on-validate-error">* Registration date must be entered</font></div>
                            <div id="reg_dateDiv" name="reg_dateDiv" class="error"><font class="on-validate-error">* Registration date not valid, eg.<?php echo "".date("Y-m-d"); ?></font></div>
                            <br/>
                        </td>
                    </tr>
                    <tr>
                        <td class="on-description" width="25%">
                            Shared Project Manager:
                        </td>
                        <td width="50%">
                            <?php
                                $sharedManagers = q("SELECT DISTINCT(e.id), e.lstname, e.frstname ".
                                                    "FROM (((Employee AS e INNER JOIN user_role AS ur ON ur.userid = e.id) ".
                                                    "INNER JOIN role_action AS ra ON ur.roleid = ra.roleid) INNER JOIN actions AS a ON a.id = ra.actionid) ".
                                                    "WHERE ur.companyid = '".$_SESSION["company_id"]."' AND a.action = 'SPM_MANAGE' ".
                                                    "AND e.email != 'admin' AND e.deleted = '0'");

                                if (is_array($sharedManagers))  {
                                    foreach ($sharedManagers as $manager)   {
                                        if (is_numeric($projectInfo[0]))
                                            $checked = (is_array(q("SELECT * FROM Project_User WHERE project_id = '".$projectInfo[0]."' AND user_id = '".$manager[0]."' AND spm_manager = '1'"))) ? "checked" : "";
                                        else
                                            $checked = "";

                                        echo "&nbsp;&nbsp;<input id='manager[]' name='manager[]' type='checkbox' tabindex='".$index++."' $checked value='".$manager[0]."'><a>".$manager[1].", ".$manager[2]."</a><br/>";
                                    }
                                }
                            ?>
                            <div id="SPMDiv" name="SPMDiv" class="error"><font class="on-validate-error">* Please ensure that at least 1 shared project manager is selected</font></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="on-description" width="25%">
                            Total Project Budget <?php echo $currency; ?>:
                        </td>
                        <td width="50%">
                            <input id="total_budget_" name="total_budget_" type="text" class="on-field currency" tabindex="<?php echo $index++; ?>" value="<?php echo stripslashes($projectInfo[7]); ?>">
                            <font class="on-description-left">* excl
                                <div id="total_budget_Div" name="total_budget_Div" class="error"><font class="on-validate-error">* Entered amount must be numeric, eg. 123.45</font></div>
                                <div id="total_budget_Empty" name="total_budget_Empty" class="error"><font class="on-validate-error">* Total project budget must be entered</font></div>
                                <div id="total_budget_Used" name="total_budget_Used" class="error"><font class="on-validate-error">* Budget assigned to companies more than total budget</font></div>
                                <div id="total_budget_Over" name="total_budget_Over" class="error"><font class="on-validate-error">* Consulting and expenses more than total budget</font></div>
                        </td>
                    </tr>
                    <tr>
                        <td width="25%">
                        </td>
                        <td width="50%">
                            <input class="on-field-heading" type="text" value="<?php echo $_SESSION["currency"]." Value"; ?>">
                            <input class="on-field-heading2" type="text" value="% Value">
                        </td>
                    </tr>
                    <tr>
                        <td class="on-description" width="25%">
                            Consulting Budget <?php echo $currency; ?>:
                        </td>
                        <td style="text-align:left;" width="50%">
                            <input id="consultingVal_" name="consultingVal_" type="text" class="on-field currency" tabindex="<?php echo $index++; ?>" value="<?php echo stripslashes($projectInfo[8]); ?>">
                            <input id="consultingPer_" name="consultingPer_" type="text" class="on-field-date percentage" tabindex="<?php echo $index++; ?>" value="<?php echo (is_numeric($projectInfo[7]) && is_numeric($projectInfo[8])) ? number_format((stripslashes($projectInfo[8]) / stripslashes($projectInfo[7])) * 100, 0) : ""; ?>">
                            <font class="on-description-left">* excl
                            <div id="consultingVal_Div" name="consultingVal_Div" class="error"><font class="on-validate-error">* Entered amount must be numeric, eg. 123.45</font></div>
                            <div id="consultingVal_Empty" name="consultingVal_Empty" class="error"><font class="on-validate-error">* Consulting budget must be entered</font></div>
                            <div id="total_consulting_Used" name="total_consulting_Used" class="error"><font class="on-validate-error">* Total consulting budget assigned to companies more than consulting budget</font></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="on-description" width="25%">
                            Expense Budget <?php echo $currency; ?>:
                        </td>
                        <td style="text-align:left;" width="50%">
                            <input id="expensesVal_" name="expensesVal_" type="text" class="currency on-field" tabindex="<?php echo $index++; ?>" value="<?php echo stripslashes($projectInfo[9]); ?>">
                            <input id="expensesPer_" name="expensesPer_" type="text" class="percentage on-field-date" tabindex="<?php echo $index++; ?>" value="<?php echo (is_numeric($projectInfo[7]) && is_numeric($projectInfo[9])) ? number_format((stripslashes($projectInfo[9]) / stripslashes($projectInfo[7])) * 100, 0) : ""; ?>">
                            <font class="on-description-left">* excl
                            <div id="expensesVal_Div" name="expensesVal_Div" class="error"><font class="on-validate-error">* Entered amount must be numeric, eg. 123.45</font></div>
                            <div id="expensesVal_Empty" name="expensesVal_Empty" class="error"><font class="on-validate-error">* Expenses budget must be entered</font></div>
                            <div id="total_expenses_Used" name="total_expenses_Used" class="error"><font class="on-validate-error">* Total expense budget assigned to companies more than expense budget</font></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="on-description" width="25%">
                            VAT <i>(%)</i>:
                        </td>
                        <td style="text-align:left;" width="50%">
                            <input id="vat" name="vat" type="text" class="percentage on-field-date" tabindex="<?php echo $index++; ?>" value="<?php echo stripslashes($projectInfo[10]); ?>">
                            <div id="vatDiv" id="vatDiv" class="error"><font class="on-validate-error">* Entered amount must be a percentage, eg. 100%</font></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="on-description" width="25%">
                            Due Date:
                        </td>
                        <td width="50%">
                            <input id="due_date" name="due_date" type="text" class="on-field-date date2" tabindex="<?php echo $index++; ?>" value="<?php echo stripslashes($projectInfo[11]); ?>">
                            <div id="due_dateDiv" name="due_dateDiv" class="error"><font class="on-validate-error">* Due date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="on-description" width="25%">
                            Invoice Cycle:
                        </td>
                        <td style="text-align:left;" width="50%">
                            <select class="on-field" id="invoiceCycle" name="invoiceCycle" tabindex="<?php echo $index++; ?>">
                                <?php
                                    $invoiceCycles = q("SELECT id, name, deflt FROM dropdowns WHERE categoryID = (SELECT id FROM categories WHERE code = 'invoiceCycle') ".
                                                        "AND active = 1 ORDER BY ranking");

                                    if (is_array($invoiceCycles))  {
                                        foreach ($invoiceCycles as $v)   {
                                            $selected = (stripslashes($projectInfo[12]) == $v[0]) ? "selected" : (($v[2] === "1" && stripslashes($projectInfo[12]) == "0") ? "selected" : "");

                                            echo "<option value='".$v[0]."' $selected>".$v[1]."</option>";
                                        }
                                    }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="on-description" width="25%">
                            Travelling Rate:
                        </td>
                        <td style="text-align:left;" width="50%">
                            <select class="on-field" id="travelRate" name="travelRate" tabindex="<?php echo $index++; ?>">
                                <?php
                                    $travelRates = q("SELECT id, name, deflt FROM dropdowns WHERE categoryID = (SELECT id FROM categories WHERE code = 'travelRate') ".
                                                        "AND active = 1 ORDER BY ranking");

                                    if (is_array($travelRates))  {
                                        foreach ($travelRates as $v)   {
                                            $selected = (stripslashes($projectInfo[13]) == $v[0]) ? "selected" : (($v[2] === "1" && stripslashes($projectInfo[13]) == "0") ? "selected" : "");

                                            echo "<option value='".$v[0]."' $selected>".$v[1]."</option>";
                                        }
                                    }
                                ?>
                            </select>
                        </td>
                    </tr>
                </table>
                <br/>
                <?php
                    $projectTeam = getSPTeam($projectInfo[0]);
                ?>
                <table width="100%">
                    <tr>
                        <td class="centerdata">
                            <h6>Team Definition</h6>
                            <br/><br/>
                        </td>
                        <td width="25%"></td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td class="on-description">
                            Please start typing company name:
                        </td>
                        <td width="180px">
                            <table>
                                <tr>
                                    <td>
                                        <input id="companyName" name="companyName" type="text" class="on-field autocomplete" value="" />
                                    <td>
                                    <td>
                                        <img style="margin-top:6px" id="teamTbl" name="companyName" class="addCompanyBtn" src="images/icons/add.ico" title="Add" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="centerdata" colspan="2">
                            <br/>
                            <div id="teamPMDiv" name="teamPMDiv" class="error"><font class="on-validate-error">* Please ensure that all companies have at least 1 project manager</font></div>
                            <div id="teamDiv" name="teamDiv" class="<?php echo (count($projectTeam) > 0) ? "" : "hidden"; ?>">
                                <div class="on-20px">
                                <table id="teamTbl" name="teamTbl" class="on-table-center on-table">
                                    <tr>
                                        <th rowspan="2">Company</th>
                                        <th rowspan="2">Total Budget</th>
                                        <th colspan="2">Consulting</th>
                                        <th colspan="2">Expenses</th>
                                        <th rowspan="2">% Remaining<br/>of Total Budget</th>
                                        <th rowspan="2">Status</th>
                                        <th rowspan="2">Invoice Cycle</th>
                                        <th rowspan="2"></th>
                                    </tr>
                                    <tr>
                                        <th>Value <?php echo $currency; ?></th>
                                        <th>%</th>
                                        <th>Value <?php echo $currency; ?></th>
                                        <th>%</th>
                                    </tr>
                                    <?php
                                        $counter = 0;

                                        if (is_array($projectTeam)) {
                                            $companies = "";

                                            foreach ($projectTeam as $pt) {
                                                $users = "";
                                                $userCounter = 0;
                                                $companies .= stripslashes($pt[2])."_";

                                                echo "<tr>";
                                                    echo "<td>";
                                                        echo "<input id='hiddenCompanyID".$counter."' name='hiddenCompanyID".$counter."' type='hidden' value='".stripslashes($pt[0])."' />";
                                                        echo "<input id='spID".$counter."' name='spID".$counter."' type='hidden' value='".stripslashes($pt[1])."' />";
                                                        echo "<input id='company_id".$counter."' name='company_id".$counter."' type='hidden' value='".stripslashes($pt[2])."' />";
                                                        echo "<a>".stripslashes($pt[3])."</a>";
                                                    echo "</td>";

                                                    //  Numeric Values
                                                    $total_budget = (is_numeric($pt[4])) ? number_format(stripslashes($pt[4]), 2, ".", "") : "";
                                                    $consultingVal = (is_numeric($pt[5])) ? number_format(stripslashes($pt[5]), 2, ".", "") : "";
                                                    $expensesVal = (is_numeric($pt[6])) ? number_format(stripslashes($pt[6]), 2, ".", "") : "";

                                                    $consultingPer = (is_numeric($total_budget) && is_numeric($consultingVal)) ? number_format((($consultingVal / $total_budget) * 100), 0) : "";
                                                    $expensesPer = (is_numeric($total_budget) && is_numeric($expensesVal)) ? number_format((($expensesVal / $total_budget) * 100), 0) : "";

                                                    $remaining = (is_numeric($total_budget)) ? $total_budget : "";

                                                    $remaining = ($remaining != "" && is_numeric($consultingVal)) ? $remaining - $consultingVal : $remaining;
                                                    $remaining = ($remaining != "" && is_numeric($expensesVal)) ? $remaining - $expensesVal : $remaining;

                                                    if (is_numeric($remaining))
                                                        $remaining = number_format((($remaining / $total_budget) * 100), 0)."%";

                                                    echo "<td>";
                                                        echo "<input id='total_budget".$counter."' name='total_budget".$counter."' type='text' class='currency' onChange='currencyChange(this);' tabindex='".$index++."' value='".$total_budget."' />";
                                                        echo "<div id='total_budget_Over_".$counter."' name='total_budget_Over_".$counter."' class='error'><font class='on-validate-error'>* Consulting and expenses more than total budget</font></div>";
                                                        echo "<div id='total_budgetDiv".$counter."' name='total_budgetDiv".$counter."' class='error'><font class='on-validate-error'>* Entered amount must be numeric, eg. 123.45</font></div>";
                                                        echo "<div id='total_budgetEmpty".$counter."' name='total_budgetEmpty".$counter."' class='error'><font class='on-validate-error'>* Total budget must be entered</font></div>";
                                                    echo "</td>";
                                                    echo "<td>";
                                                        echo "<input id='consultingVal".$counter."' name='consultingVal".$counter."' type='text' class='currency' onChange='currencyChange(this);' tabindex='".$index++."' value='".$consultingVal."' />";
                                                        echo "<div id='consultingOver_".$counter."' name='consultingOver_".$counter."' class='error'><font class='on-validate-error'>* User budget more than consulting budget</font></div>";
                                                        echo "<div id='consultingValDiv".$counter."' name='consultingValDiv".$counter."' class='error'><font class='on-validate-error'>* Entered amount must be numeric, eg. 123.45</font></div>";
                                                        echo "<div id='consultingValEmpty".$counter."' name='consultingValEmpty".$counter."' class='error'><font class='on-validate-error'>* Consulting budget must be entered</font></div>";
                                                    echo "</td>";
                                                    echo "<td><input id='consultingPer".$counter."' name='consultingPer".$counter."' type='text' class='percentage on-field-perc' onChange='percentageChange(this);' tabindex='".$index++."' value='".$consultingPer."' /></td>";
                                                    echo "<td>";
                                                        echo "<input id='expensesVal".$counter."' name='expensesVal".$counter."' type='text' class='currency' onChange='currencyChange(this);' tabindex='".$index++."' value='".$expensesVal."' />";
                                                        echo "<div id='expensesValDiv".$counter."' name='expensesValDiv".$counter."' class='error'><font class='on-validate-error'>* Entered amount must be numeric, eg. 123.45</font></div>";
                                                        echo "<div id='expensesValEmpty".$counter."' name='expensesValEmpty".$counter."' class='error'><font class='on-validate-error'>* Expense budget must be entered</font></div>";
                                                    echo "</td>";
                                                    echo "<td><input id='expensesPer".$counter."' name='expensesPer".$counter."' type='text' class='percentage on-field-perc' onChange='percentageChange(this);' tabindex='".$index++."' value='".$expensesPer."' /></td>";
                                                    echo "<td class='centerdata'><input id='budgetRemain".$counter."' name='budgetRemain".$counter."' type='text' class='percentage on-field-perc' readonly value='".$remaining."' /></td>";
                                                    echo "<td style='white-space:nowrap;' class='centerdata'>";
                                                        echo "<input id='status".$counter."' name='status".$counter."' type='hidden' value='".stripslashes($pt[7])."' />";
                                                        echo "<input id='teamManage".$counter."' name='teamManage".$counter."' type='hidden' value='".stripslashes($pt[8])."' />";

                                                        $img = (stripslashes($pt[7]) == 1) ? "approved" : ((stripslashes($pt[7]) == 2) ? "declined" : "pending");
                                                        $title = (stripslashes($pt[7]) == 1) ? "Company participating in shared project" : ((stripslashes($pt[7]) == 2) ? "Company not participating in shared project" : "Pending");

                                                        echo "<img id='companyStatus".$counter."' name='companyStatus".$counter."' src='images/icons/".$img.".ico' title='".$title."' />";

                                                        $img = (stripslashes($pt[8]) == 1) ? "approved" : ((stripslashes($pt[8]) == 2) ? "declined" : "pending");
                                                        $title = (stripslashes($pt[8]) == 1) ? "Team managed by Shared Project Manager" : ((stripslashes($pt[8]) == 2) ? "Team managed by Company Project Manager" : "Pending");

                                                        echo "<img id='teamStatus".$counter."' name='teamStatus".$counter."' src='images/icons/".$img.".ico' title='".$title."' />";

                                                        //$readonly = (stripslashes($pt[7]) == 1 && stripslashes($pt[8]) == 1) ? "readonly" : "";

                                                        echo "<input id='request".$counter."' name='request".$counter."' type='button' class='resendInvite' title='Resend Request' value='Resend Request' />";
                                                        //echo "<input id='request".$counter."' name='request".$counter."' type='checkbox' $readonly title='Resend Request' value='1' />";
                                                    echo "</td>";
                                                    echo "<td>";
                                                        echo "<select id='invoiceCycle".$counter."' name='invoiceCycle".$counter."' tabindex='".$index++."'>";
                                                            if (is_array($invoiceCycles))  {
                                                                foreach ($invoiceCycles as $v)   {
                                                                    $selected = (stripslashes($pt[9]) == $v[0]) ? "selected" : (($v[2] === "1" && stripslashes($pt[9]) == "0") ? "selected" : "");

                                                                    echo "<option value='".$v[0]."' $selected>".$v[1]."</option>";
                                                                }
                                                            }
                                                        echo "</select>";
                                                    echo "</td>";
                                                    echo "<td><img id='".stripslashes($pt[0])."' name='".stripslashes($pt[0])."' src='images/icons/delete.ico' class='deleteCompanyBtn' title='Delete' /></td>";
                                                echo "</tr>";

                                                if (stripslashes($pt[8]) == 1)   {
                                                    echo "<tr>";
                                                        echo "<td class='on-table-clear' colspan='10'>";
                                                            echo "<a id='Users".$counter."' name='".stripslashes($pt[3])."' class='showUsers'><img src='images/icons/collapse.ico' title='Collapse' />";
                                                            echo "<i>Hide User(s) For ".stripslashes($pt[3])."</i></a>";
                                                            echo "<div id='showUsers".$counter."' name='showUsers".$counter."'>";
                                                                $cls = (!is_array($pt[10])) ? "hidden" : "";

                                                                echo "<br>";
                                                                echo "<table width='100%'>";
                                                                    echo "<tr>";
                                                                        echo "<td style='border:0' class='noborder rightdata on-table-clear' width='22%'>";
                                                                            echo "Please start typing user name:";
                                                                        echo "</td>";
                                                                        echo "<td style='border:0' class='noborder on-table-clear' width='79%'>";
                                                                            echo "<input id='username".$counter."' name='".stripslashes($pt[2])."' class='getUser' type='text' value='' />";
                                                                            echo "&nbsp;<img id='member".$counter."Tbl' name='username".$counter."' class='addUserBtn' src='images/icons/add.ico' title='Add' />";
                                                                        echo "</td>";
                                                                    echo "</tr>";
                                                                echo "</table>";

                                                                echo "<div id='member".$counter."Div' name='member".$counter."Div' class='".$cls."'>";
                                                                    echo "<table class='on-table' id='member".$counter."Tbl' name='member".$counter."Tbl'>";
                                                                        echo "<tr>";
                                                                            echo "<th style='width:500px'>User Details</th>";
                                                                            echo "<th>PM</th>";
                                                                            echo "<th>Budget</th>";
                                                                            echo "<th style='width:80px'>Rate</th>";
                                                                            echo "<th></th>";
                                                                        echo "</tr>";

                                                                        if (is_array($pt[10]))  {
                                                                            foreach ($pt[10] as $u) {
                                                                                $users .= stripslashes($u[3])."_";

                                                                                echo "<tr>";
                                                                                    echo "<td>";
                                                                                        echo "<input id='hiddenUserID".stripslashes($pt[2])."_".$userCounter."' name='hiddenUserID".stripslashes($pt[2])."_".$userCounter."' type='hidden' value='".stripslashes($u[0])."' />";
                                                                                        echo "<input id='spID".stripslashes($pt[2])."_".$userCounter."' name='spID".stripslashes($pt[2])."_".$userCounter."' type='hidden' value='".stripslashes($u[1])."' />";
                                                                                        echo "<input id='company_id".stripslashes($pt[2])."_".$userCounter."' name='company_id".stripslashes($pt[2])."_".$userCounter."' type='hidden' value='".stripslashes($u[2])."' />";
                                                                                        echo "<input id='userID".stripslashes($pt[2])."_".$userCounter."' name='userID".stripslashes($pt[2])."_".$userCounter."' type='hidden' value='".stripslashes($u[3])."' />";
                                                                                        echo "<a>".stripslashes($u[4])."</a>";
                                                                                    echo "</td>";

                                                                                    $checked = (stripslashes($u[5])) ? "checked" : "";

                                                                                    $checkPM = q("SELECT ur.id,r.id FROM (user_role AS ur INNER JOIN roles AS r ON ur.roleid = r.id)
                                                                                                    WHERE r.role = 'PM' AND ur.userid='".stripslashes($u[3])."' AND ur.companyid = '".stripslashes($u[2])."'".
                                                                                                        "AND r.companyid = '".$_SESSION["company_id"]."'");
                                                                                    $disabled = ($checkPM[0][0] != 0) ? "" : "disabled";

                                                                                    echo "<td><input id='manager".stripslashes($pt[2])."_".$userCounter."' name='manager".stripslashes($pt[2])."_".$userCounter."' type='checkbox' tabindex='".$index."' $checked $disabled value='1' /></td>";
                                                                                    echo "<td><input id='budget".stripslashes($pt[2])."_".$userCounter."' name='budget".stripslashes($pt[2])."_".$userCounter."' type='text' class='currency' tabindex=".$index."' value='".stripslashes($u[6])."' /></td>";
                                                                                    echo "<td>";
                                                                                        echo "<select id='rate".stripslashes($pt[2])."_".$userCounter."' name='rate".stripslashes($pt[2])."_".$userCounter."' tabindex='".$index."'>";
                                                                                            echo "<option value='0'>0.00</option>";
                                                                                            //$rates = q("SELECT tariff1, tariff2, tariff3, tariff4, tariff5 FROM Employee WHERE id = '".$u[3]."'");
                                                                                            $rates = q("SELECT id, rate FROM user_rates WHERE userid = '".$u[3]."' AND companyid = '".$u[2]."' AND active = 1");
                                                                                            $rates = sortArrayByColumn($rates, 1);
                                                                                            $data = $rates;

                                                                                            /*
                                                                                            if (is_array($rates))   {
                                                                                                $r = 0;

                                                                                                for ($i = 1; $i <= 5; $i++) {
                                                                                                    if ($rates[0][$i - 1] > 0) {
                                                                                                        $data[$r][0] = $i;
                                                                                                        $data[$r++][1] = $rates[0][$i - 1];
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                            */

                                                                                            if (is_array($data))  {
                                                                                                foreach ($data as $d)   {
                                                                                                    $selected = (stripslashes($u[7]) == $d[0]) ? "selected" : "";

                                                                                                    echo "<option value='".$d[0]."' $selected>".$d[1]."</option>";
                                                                                                }
                                                                                            }
                                                                                        echo "</select>";
                                                                                    echo "</td>";
                                                                                    echo "<td><img id='".stripslashes($u[0])."' name='".stripslashes($u[0])."' src='images/icons/delete.ico' class='deleteUserBtn' title='Delete' /></td>";
                                                                                echo "</tr>";

                                                                                $userCounter++;
                                                                            }
                                                                        }
                                                                        echo "<tr id='member".$counter."TblLstRow' name='member".$counter."TblLstRow'></tr>";
                                                                    echo "</table>";
                                                                    echo "<input id='member".$counter."TblCntr' name='member".$counter."TblCntr' type='hidden' value='".$userCounter."' />";
                                                                    echo "<input id='hiddenUsers".stripslashes($pt[2])."' name='hiddenUsers".stripslashes($pt[2])."' type='hidden' value='".$users."' />";
                                                                echo "</div>";
                                                            echo "</div>";
                                                        echo "</td>";
                                                    echo "</tr>";
                                                }
                                                $counter++;
                                            }
                                        }
                                    ?>
                                    <tr id="teamTblLstRow" name="teamTblLstRow"></tr>
                                    <tfoot><tr><td colspan='100%'></td></tr></tfoot>
                                </table>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
                <div class="hidden">
                    <table id="hidden_teamTbl" name="hidden_teamTbl">
                        <tr>
                            <td>
                                <input id="hiddenCompanyID" name="hiddenCompanyID" type="hidden" value="" />
                                <input id="spID" name="spID" type="hidden" value="" />
                                <input id="company_id" name="company_id" type="hidden" value="" />
                                <a></a>
                            </td>
                            <td>
                                <input id="total_budget" name="total_budget" type="text" class="currency" onChange="currencyChange(this);" tabindex="<?php echo $index; ?>" value="" />
                                <div id="total_budget_Over_" name="total_budget_Over_" class="error"><font class="on-validate-error">* Consulting and expenses more than total budget</font></div>
                                <div id="total_budgetDiv" name="total_budgetDiv" class="error"><font class="on-validate-error">* Entered amount must be numeric, eg. 123.45</font></div>
                                <div id="total_budgetEmpty" name="total_budgetEmpty" class="error"><font class="on-validate-error">* Total budget must be entered</font></div>
                            </td>
                            <td>
                                <input id="consultingVal" name="consultingVal" type="text" class="currency" onChange="currencyChange(this);" tabindex="<?php echo $index; ?>" value="" />
                                <div id="consultingOver_" name="consultingOver_" class="error"><font class="on-validate-error">* User budget more than consulting budget</font></div>
                                <div id="consultingValDiv" name="consultingValDiv" class="error"><font class="on-validate-error">* Entered amount must be numeric, eg. 123.45</font></div>
                                <div id="consultingValEmpty" name="consultingValEmpty" class="error"><font class="on-validate-error">* Total budget must be entered</font></div>
                            </td>
                            <td><input id="consultingPer" name="consultingPer" type="text" class="percentage on-field-perc" onChange="percentageChange(this);" tabindex="<?php echo $index; ?>" value="" /></td>
                            <td>
                                <input id="expensesVal" name="expensesVal" type="text" class="currency" onChange="currencyChange(this);"tabindex="<?php echo $index; ?>" value="" />
                                <div id="expensesValDiv" name="expensesValDiv" class="error"><font class="on-validate-error">* Entered amount must be numeric, eg. 123.45</font></div>
                                <div id="expensesValEmpty" name="expensesValEmpty" class="error"><font class="on-validate-error">* Total budget must be entered</font></div>
                            </td>
                            <td><input id="expensesPer" name="expensesPer" type="text" class="percentage on-field-perc" onChange="percentageChange(this);" tabindex="<?php echo $index; ?>" value="" /></td>
                            <td class="centerdata"><input id="budgetRemain" name="budgetRemain" type="text" class="percentage on-field-perc" readonly value="" /></td>
                            <td style="white-space:nowrap;">
                                <input id="status" name="status" type="hidden" value="0" />
                                <input id="teamManage" name="teamManage" type="hidden" value="0" />
                                <img id="companyStatus" name="companyStatus" src="images/icons/pending.ico" title="Company Pending" />
                                <img id="teamStatus" name="teamStatus" src="images/icons/pending.ico" title="Team Pending" />
                                <input id="request" name="request" type="button" class="resendInvite" readonly disabled title="Resend Request" tabindex="<?php echo $index; ?>" value="Resend Request" />
                                <!--<input id="request" name="request" type="checkbox" readonly title="Resend Request" tabindex="<?php //echo $index; ?>" value="1" />-->
                            </td>
                            <td><select id="invoiceCycle" name="invoiceCycle" tabindex="<?php echo $index; ?>"></select></td>
                            <td><img id="" name="" src="images/icons/delete.ico" class="deleteCompanyBtn" title="Delete" /></td>
                        </tr>
                    </table>
                    <table id="hidden_memberTbl" name="hidden_memberTbl">
                        <tr>
                            <td>
                                <input id="hiddenUserID" name="hiddenUserID" type="hidden" value="" />
                                <input id="spID" name="spID" type="hidden" value="" />
                                <input id="company_id" name="company_id" type="hidden" value="" />
                                <input id="userID" name="userID" type="hidden" value="" />
                                <a></a>
                            </td>
                            <td><input id="manager" name="manager" type="checkbox" tabindex="<?php echo $index; ?>" value="1" /></td>
                            <td><input id="budget" name="budget" type="text" class="currency" tabindex="<?php echo $index; ?>" value="" /></td>
                            <td><select id="rate" name="rate" tabindex="<?php echo $index; ?>"></select></td>
                            <td><img id="" name="" src="images/icons/delete.ico" class="deleteUserBtn" title="Delete" /></td>
                        </tr>
                    </table>
                </div>
                <br/>
                <input id="btnAddEdit" name="btnAddEdit" tabindex="<?php echo $index++; ?>" type="button" value="<?php echo ($projectInfo[0] == "") ? "Add" : "Update"; ?>">
                <input id="save" name="save" type="hidden" value="0" />
                <input id="teamTblCntr" name="teamTblCntr" type="hidden" value="<?php echo $counter; ?>" />
                <input id="hiddenCompanies" name="hiddenCompanies" type="hidden" value="<?php echo $companies; ?>" />
            </form>
        </td>
    </tr>
    <tr>
        <td>
            <br/>
        </td>
    </tr>
</table>
<?php
    //  Print Footer
    print_footer();
?>
