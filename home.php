<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    // Mobile Detection
    function mobile_detection ()    {
        if (isset($_SERVER['HTTP_X_WAP_PROFILE']) || isset($_SERVER['HTTP_PROFILE']))   return 1;
        if (isset ($_SERVER['HTTP_ACCEPT']))    {
            $accept = strtolower($_SERVER['HTTP_ACCEPT']);
            if (strpos($accept, 'wap') !== false)   return 1;
        }
        if (isset ($_SERVER['HTTP_USER_AGENT']))    {
            if (strpos ($_SERVER['HTTP_USER_AGENT'], 'Mobile') !== false)    return 1;
            if (strpos ($_SERVER['HTTP_USER_AGENT'], 'Opera Mini') !== false)   return 1;
        }
        return 0;
    }

    // if (mobile_detection())     {
    //     echo "<meta http-equiv='Refresh' content='0; url=http://live.on-track.ws/mobile'>";
    // }
    ////////////////////////////////////////

    if (!$_SESSION["eula_agree"] === true)
        header("Location: logout.php");

	if ($_GET["menu"] == "") {
		if (!isset($_SESSION["company_id"]))
                    $company_id                                                     = null;

		if (isset($_POST["company"]) && is_numeric($_POST["company"])) {
			$_SESSION["company_id"] = $_POST["company"];
			$_SESSION["company_name"] = q("SELECT name FROM Company WHERE id = '".$_POST["company"]."'");
			$_SESSION["currency"] = q("SELECT currency FROM Company WHERE id = '".$_POST["company"]."'");
			$_SESSION["VAT"] = q("SELECT vat FROM Company WHERE id = '".$_POST["company"]."'");

			$actions = q("SELECT a.`action` FROM (((roles AS r INNER JOIN role_action AS ra ON r.id = ra.roleid) INNER JOIN user_role AS ur ".
					"ON r.id = ur.roleid) INNER JOIN actions AS a ON ra.actionid = a.id) ".
					"WHERE ur.companyid = '".$_SESSION["company_id"]."' AND ra.companyid = '".$_SESSION["company_id"]."' AND ur.userid = '".$_SESSION['user_id']."' ".
                                            "AND r.companyid = '".$_SESSION["company_id"]."'");

			if ($_SESSION["email"] == "admin")
                            $actions = q("SELECT `action` FROM actions");

                        if (is_array($actions))
                            foreach ($actions as $a)
                                $actionArr[] = $a[0];

                        $_SESSION["actions"] = $actionArr;

                        $modules = q("SELECT m.code, m.name FROM (Modules AS m INNER JOIN Module_Users AS mu ON m.id = mu.module_id) WHERE mu.company_id = '".$_SESSION["company_id"]."' AND m.code = 'approval'");

                        if (!is_array($modules))        $_SESSION["approvalModule"] = 0;
                        else                            $_SESSION["approvalModule"] = 1;

                        $landingPage = q("SELECT landingPage FROM user_defaults WHERE companyID = '".$_SESSION["company_id"]."' AND userID = '".$_SESSION["user_id"]."'");
                        $activityType = q("SELECT activityType FROM user_defaults WHERE companyID = '".$_SESSION["company_id"]."' AND userID = '".$_SESSION["user_id"]."'");
                        $activity = q("SELECT activity FROM user_defaults WHERE companyID = '".$_SESSION["company_id"]."' AND userID = '".$_SESSION["user_id"]."'");
                        $vehicle = q("SELECT vehicle FROM user_defaults WHERE companyID = '".$_SESSION["company_id"]."' AND userID = '".$_SESSION["user_id"]."'");

						$_SESSION["default_activityType"] = $activityType;
						$_SESSION["default_activity"] = $activity;
						$_SESSION["default_vehicle"] = $vehicle;

                        $landingPage = ($landingPage == "dashboard.php") ? "bookings.php" : $landingPage;

			header("Location: $landingPage");
		}
		else if (isset($_POST["company"]) && !is_numeric($_POST["company"])) {
			unset($_SESSION["company_id"]);
			unset($_SESSION["company_name"]);
			unset($_SESSION["currency"]);
			unset($_SESSION["VAT"]);
			unset($_SESSION["default_activityType"]);
			unset($_SESSION["default_activity"]);
		}

		if ($_SESSION["logged_in"] === true)
		{
			$count = q("SELECT COUNT(c.id) FROM (Company as c INNER JOIN Company_Users as cu ON c.id = cu.company_id) WHERE cu.user_id = '".$_SESSION["user_id"]."' AND c.locked = '0'");
			//$count = q("SELECT COUNT(id) FROM Company_Users WHERE user_id = '".$_SESSION["user_id"]."'");

			if ($count == 0)
				header("Location: locked.php");
			else if ($count == 1)   {
				$_SESSION["company_id"] = q("SELECT c.id FROM (Company as c INNER JOIN Company_Users as cu ON c.id = cu.company_id) WHERE cu.user_id = '".$_SESSION["user_id"]."' AND c.locked = '0'");
				$_SESSION["company_name"] = q("SELECT name FROM Company WHERE id = '".$_SESSION["company_id"]."'");
				$_SESSION["currency"] = q("SELECT currency FROM Company WHERE id = '".$_SESSION["company_id"]."'");
				$_SESSION["VAT"] = q("SELECT vat FROM Company WHERE id = '".$_SESSION["company_id"]."'");

				$actions = q("SELECT a.`action` FROM (((roles AS r INNER JOIN role_action AS ra ON r.id = ra.roleid) INNER JOIN user_role AS ur ON r.id = ur.roleid) ".
                                                "INNER JOIN actions AS a ON ra.actionid = a.id) WHERE ur.companyid = '".$_SESSION["company_id"]."' AND ur.userid = '".$_SESSION['user_id']."' ".
                                                "AND r.companyid = '".$_SESSION["company_id"]."'");

                                if ($_SESSION["email"] == "admin")
                                    $actions = q("SELECT `action` FROM actions");

                                if (is_array($actions))
                                    foreach ($actions as $a)
                                        $actionArr[] = $a[0];

                                $_SESSION["actions"] = $actionArr;

                                $modules = q("SELECT m.code, m.name FROM (Modules AS m INNER JOIN Module_Users AS mu ON m.id = mu.module_id) WHERE mu.company_id = '".$_SESSION["company_id"]."' AND m.code = 'approval'");

                                if (!is_array($modules))        $_SESSION["approvalModule"] = 0;
                                else                            $_SESSION["approvalModule"] = 1;

                                $landingPage = q("SELECT landingPage FROM user_defaults WHERE companyID = '".$_SESSION["company_id"]."' AND userID = '".$_SESSION["user_id"]."'");
                                $activityType = q("SELECT activityType FROM user_defaults WHERE companyID = '".$_SESSION["company_id"]."' AND userID = '".$_SESSION["user_id"]."'");
                                $activity = q("SELECT activity FROM user_defaults WHERE companyID = '".$_SESSION["company_id"]."' AND userID = '".$_SESSION["user_id"]."'");
								$vehicle = q("SELECT vehicle FROM user_defaults WHERE companyID = '".$_SESSION["company_id"]."' AND userID = '".$_SESSION["user_id"]."'");

								$_SESSION["default_activityType"] = $activityType;
								$_SESSION["default_activity"] = $activity;
								$_SESSION["default_vehicle"] = $vehicle;

                                $landingPage = ($landingPage == "dashboard.php") ? "bookings.php" : $landingPage;

				if ($_SESSION["user_id"] != "93" && $_SESSION["user_id"] != "1")
					header("Location: $landingPage");
				else if ($_SESSION["user_id"] == "93")
					header("Location: invoice_load.php");
			}
		}
	}

    //  Print Header
    print_header();
    //  Print Menu
	if ($_GET["menu"] == "")
		print_menus("0", "home");
	else
		print_menus($_GET["menu"], "home");

	if ($_GET["menu"] == "") {
?>
    <table width="100%">
        <tr>
            <td align="center" valign="top">
                <form action="" method="post" name="home">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Company Selection</h6>
                            </td>
                        </tr>
                        <tr>
                            <td class="centerdata">
                                <br/>
                            </td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                Select Company:
                            </td>
                            <td width="50%">
                                <select class="on-field" method="post" name="company" onChange="submit();" tabindex="1">
                                    <option value="null">--  Select Company  --</option>
                                    <?php
                                        //  Get Company Info
                                        $companies = q("SELECT c.id, c.name FROM (Company as c INNER JOIN Company_Users as cu ON c.id = cu.company_id) WHERE cu.user_id = '".$_SESSION["user_id"]."' AND c.locked = '0' ".
                                                        "ORDER BY c.name");

                                        //  Choose Company To Work On
					if (is_array($companies))
					    foreach ($companies as $company) {
						if ($company[0] == $_SESSION["company_id"])     echo "<option value='".$company[0]."' selected>".$company[1]."</option>";
						else                                            echo "<option value='".$company[0]."'>".$company[1]."</option>";
					    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                    </table>
                </form>
            </td>
        </tr>
    </table>
<?php
    }

    //  Print Footer
    print_footer();
?>
