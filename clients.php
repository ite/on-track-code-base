<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    if (!hasAccess("CLIENT_MANAGE"))
		header("Location: noaccess.php");

    $display_link                                                       = "clients.php?alphabet";

    //  Set Alphabet Letter
    $alphabet                                                           = "";
    $alphabet                                                           = $_GET["alphabet"];

    //  If Delete Client Was Clicked
    if (isset($_POST["deleteBox"]) && $_POST["deleteBox"] === "1")
    {
        $id                                                             = $_POST["idBox"];
        $delete                                                         = $_POST["delete"];
        $client_info                                                  = q("SELECT companyName FROM Client WHERE id = '$id'");
        $setStatus                                                      = q("UPDATE Client SET deleted = '$delete' WHERE id = '$id'");

        if ($setStatus) {
            $time                                                       = date("H:i:s");

            $logs                                                       = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('".$client_info." deleted', 'Update', 'Client', '".$_SESSION["email"]."', ".
                                                                            "'$today', '$time', '".$_SESSION["company_id"]."')");

            $errorMessage                                               = "Client Deleted/Undeleted Successfully...";
        }
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0", "clients", "clients");

    if ($errorMessage != "")
    {
        echo "<div style='width:100%; text-align:center'><strong><font class='on-validate-error'>$errorMessage</font></strong></div>";
        echo "<br/>";
    }
?>
<script language="JavaScript">
    function deleted(id, del) {
        document.forms["clients"].idBox.value                          = id;
        document.forms["clients"].delete.value                         = del;
        document.forms["clients"].deleteBox.value                      = 1;
        document.forms["clients"].submit();
    }
</script>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata">
                <form action="" method="post" name="clients">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>
                                   Client List
                                </h6>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <br/>
                            </td>
                        </tr>
                    </table>
                    <?php
                        echo "<br/>";
                        echo "| <a href='$display_link='>View All Clients</a> |";
                        echo "<br/><br/>";
                        echo "<a href='$display_link=A'>A</a> | <a href='$display_link=B'>B</a> | ".
                            "<a href='$display_link=C'>C</a> | <a href='$display_link=D'>D</a> | ".
                            "<a href='$display_link=E'>E</a> | <a href='$display_link=F'>F</a> | ".
                            "<a href='$display_link=G'>G</a> | <a href='$display_link=H'>H</a> | ".
                            "<a href='$display_link=I'>I</a> | <a href='$display_link=J'>J</a> | ".
                            "<a href='$display_link=K'>K</a> | <a href='$display_link=L'>L</a> | ".
                            "<a href='$display_link=M'>M</a> | <a href='$display_link=N'>N</a> | ".
                            "<a href='$display_link=O'>O</a> | <a href='$display_link=P'>P</a> | ".
                            "<a href='$display_link=Q'>Q</a> | <a href='$display_link=R'>R</a> | ".
                            "<a href='$display_link=S'>S</a> | <a href='$display_link=T'>T</a> | ".
                            "<a href='$display_link=U'>U</a> | <a href='$display_link=V'>V</a> | ".
                            "<a href='$display_link=W'>W</a> | <a href='$display_link=X'>X</a> | ".
                            "<a href='$display_link=Y'>Y</a> | <a href='$display_link=Z'>Z</a>";
                    ?>
                    <br/><br/>
                    <input name="btnAdd" onClick="location.href='client_add.php';" tabindex="1" type="button" value="Add New Client">
                    <br/>
                    <br/>
                    <!--<input name="btnEdit" onClick="location.href='client_edit.php';" tabindex="2" type="button" value="Edit Client(s)">
                    <br/><br/>-->
                    <table class="on-table-center on-table" width="40%">
                        <!--  Table Headings   -->
                        <tr>
                            <th width="40%">
                                Client/Company Name
                            </th>
                            <th>
                                Contacts
                            </th>
                            <th>
                                Delete/Undelete
                            </th>
                        </tr>
                        <!--  Table Information   -->
                        <?php
                                //  nClients                           = Number of Clients
                                $nClients                              = q("SELECT COUNT(id) FROM Client WHERE companyName LIKE '$alphabet%' 
                                                                                    AND company_id = '".$_SESSION["company_id"]."'");
                                $clients                               = q("SELECT id, companyName, deleted FROM Client WHERE companyName LIKE '$alphabet%' 
                                                                                    AND company_id = '".$_SESSION["company_id"]."' ORDER BY companyName");

                            if ($nClients > 1)
                            {
                                foreach ($clients as $clients)
                                {
                                    echo "<tr>";
                                        echo "<td><a href='client_edit.php?id=".$clients[0]."'>".$clients[1]."</a></td>";
                                        echo "<td><a href='contacts.php?id=".$clients[0]."'><input type='button' value='View Contacts'></a></td>";
                                                //  Delete/Undelete Button
                                      echo "<td align='center'>";
                                                if ($clients[2] === "0")
                                                    echo "<input name='btnDelete' onClick=\"deleted(".$clients[0].", '1');\" type='button' value='Delete Client     '>";
                                                else
                                                    echo "<input name='btnDelete' onClick=\"deleted(".$clients[0].", '0');\" type='button' value='Undelete Client'>";
                                            echo "</td>";
                                    echo "</tr>";
                                }
                            }
                            else if ($nClients == 1)
                            {
                                echo "<tr>";
                                    echo "<td><a href='client_edit.php?id=".$clients[0][0]."'>".$clients[0][1]."</a></td>";
                                    echo "<td><a href='contacts.php?id=".$clients[0][0]."'><input type='button' value='View Contacts'></a></td>";
                                        //  Delete/Undelete Button
                                        echo "<td class='centerdata'>";
                                            if ($clients[0][2] === "0")
                                                echo "<input name='btnDelete' onClick=\"deleted(".$clients[0][0].", '1');\" type='button' value='Delete Client    '>";
                                            else
                                                echo "<input name='btnDelete' onClick=\"deleted(".$clients[0][0].", '0');\" type='button' value='Undelete Client'>";
                                        echo "</td>";
                                echo "</tr>";
                            }
                            else
                            {
                                echo "<tr>
                                            <td class='centerdata' colspan='100%'>";
                                        if ($alphabet == "")
                                            echo "No clients available";
                                        else
                                            echo "No clients available under $alphabet";
                                    echo "</td>";
                                echo "</tr>";
                            }
                            echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        ?>
                    </table>
                    <br/>
                    <!--<input name="btnEdit" onClick="location.href='client_edit.php';" tabindex="3" type="button" value="Edit Client(s)">
                    <br/><br/>-->
                    <input name="btnAdd" onClick="location.href='client_add.php';" tabindex="4" type="button" value="Add New Client">
                    <input name="idBox" type="hidden" value="">
                    <input name="delete" type="hidden" value="">
                    <input name="deleteBox" type="hidden" value="">
                </form>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
