<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("include/sajax.php");
    include("fusion/FusionCharts.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");
    
     if (!hasAccess("REP_GANTT"))
        header("Location: noaccess.php");

    ///////////////////////////
    //  Sajax
    function getActivityTypes($id) {
        return q("SELECT id, type FROM ActivityTypes WHERE pID='$id' ORDER BY type");
    }
    
    function getActivities($id) {
        return q("SELECT id, name FROM Activities WHERE parent_id = '$id' ORDER BY name");
    }

    function getProjects($id) {
		if ($id == "null")
			$projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) ".
			"WHERE cot.company_id = '".$_SESSION["company_id"]."' AND p.completed = '0' AND p.deleted = '0' ORDER BY UPPER(p.name)");
		else
			$projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) ".
			"WHERE cot.company_id = '".$_SESSION["company_id"]."' AND p.completed = '0' AND p.deleted = '0' AND p.type_id='$id' ORDER BY UPPER(p.name)");
		return $projects;
	}

    $sajax_request_type = "GET";
    sajax_init();
    sajax_export("getActivities");
    sajax_export("getActivityTypes");
    sajax_export("getProjects");
    sajax_handle_client_request();
    ///////////////////////////

    //  Set Report Generated Status
    $generated = "0";
	$rows = 1;

	function createChartXML($categories, $processes, $where, $date_from, $date_to, $view, $period, $duration, $durationUnit)	{
		global $rows;
		$xmlStr = "";

		$xmlStr .= "<chart dateFormat='yyyy/mm/dd' outputDateFormat='yyyy/mm/dd hh:mn:ss' ganttPaneDuration='".$duration."' ganttPaneDurationUnit='".$durationUnit."' ".fusionChart().">";
			$xmlStr .= "<categories fontSize='10' isBold='1'>";

			foreach ($categories as $category)	{
				$xmlStr .= "<category start='".str_replace("-","/",$category[0])."' end='".str_replace("-","/",$category[1])."' name='".$category[2]."' />";
			}

			$xmlStr .= "</categories>";
			$xmlStr .= "<processes headerText='Activity Types and Activities' headeralign='left' align='left'>";

			foreach ($processes as $process)	{
				$rows++;
				$xmlStr .= "<process name='".$process[0]."' id='".$process[0]." - ".$process[1]."' isBold='1' />";

				$subProcesses = q("SELECT DISTINCT(a.name), a.id ".
									"FROM ((((TimeSheet AS ts) LEFT JOIN Activities AS a ON ts.activity_id = a.id) ".
										"INNER JOIN ActivityTypes AS at ON a.parent_id = at.id) INNER JOIN Project AS p ON ts.project_id = p.id)".
									"WHERE $where AND at.company_id = '".$_SESSION["company_id"]."'AND at.id = '".$process[1]."' ORDER BY a.name");

				if (is_array($subProcesses))	{
					foreach ($subProcesses as $subProcess)	{
						$rows++;
						$xmlStr .= "<process name='".$subProcess[0]."' id='".$subProcess[0]." - ".$subProcess[1]."' />";
					}
				}
			}

			$xmlStr .= "</processes>";
			$xmlStr .= "<tasks color='4567aa'>";

			foreach ($processes as $process)	{
				$subProcesses = q("SELECT DISTINCT(a.name), a.id ".
									"FROM ((((TimeSheet AS ts) LEFT JOIN Activities AS a ON ts.activity_id = a.id) ".
										"INNER JOIN ActivityTypes AS at ON a.parent_id = at.id) INNER JOIN Project AS p ON ts.project_id = p.id)".
									"WHERE $where AND at.company_id = '".$_SESSION["company_id"]."'AND at.id = '".$process[1]."' ORDER BY a.name");
/*
				$dateMinMax = q("SELECT MIN(date), MAX(date) FROM TimeSheet WHERE date BETWEEN '".$date_from."' AND '".$date_to."' ".
									"AND activity_id IN (SELECT id FROM Activities WHERE parent_id = '".$process[1]."')");
*/
				$dateMinMax = q("SELECT MIN(ts.date), MAX(ts.date) ".
								"FROM ((((TimeSheet AS ts) LEFT JOIN Activities AS a ON ts.activity_id = a.id) ".
									"INNER JOIN ActivityTypes AS at ON a.parent_id = at.id) INNER JOIN Project AS p ON ts.project_id = p.id)".
								"WHERE $where AND at.company_id = '".$_SESSION["company_id"]."'AND at.id = '".$process[1]."' ORDER BY a.name");

				$xmlStr .= "<task processId='".$process[0]." - ".$process[1]."' showAsGroup='1' label='".$process[0]."' showLabel='1' ".
								"start='".str_replace("-","/",$dateMinMax[0][0])." 00:00:00' end='".str_replace("-","/",$dateMinMax[0][1])." 23:59:59' />";

				if (is_array($subProcesses))	{
					foreach ($subProcesses as $subProcess)	{
/*
						$bookings = q("SELECT date, sum(time_spent) FROM TimeSheet WHERE date BETWEEN '".$date_from."' AND '".$date_to."' ".
										"AND activity_id = '".$subProcess[1]."' GROUP BY date;");
*/
						$bookings = q("SELECT ts.date, SUM(ts.time_spent) ".
										"FROM ((((TimeSheet AS ts) LEFT JOIN Activities AS a ON ts.activity_id = a.id) ".
											"INNER JOIN ActivityTypes AS at ON a.parent_id = at.id) INNER JOIN Project AS p ON ts.project_id = p.id)".
										"WHERE $where AND at.company_id = '".$_SESSION["company_id"]."'AND a.id = '".$subProcess[1]."' GROUP BY ts.date");

						if (is_array($bookings))	{
							foreach ($bookings as $booking)	{
								$startTime = date("Y-m-d H:i:s", strtotime($booking[0]." 08:00:00"));
								$minutes = ($booking[1] * 60);
								$endTime = date("Y-m-d H:i:s", strtotime("+$minutes minutes", strtotime($startTime)));

								$xmlStr .= "<task processId='".$subProcess[0]." - ".$subProcess[1]."' name='".$subProcess[0]." - ".$booking[1]." hrs' ".
											"start='".str_replace("-","/",$startTime). "' end='".str_replace("-","/",$endTime)."' />";
							}
						}
					}
				}
			}

			$xmlStr .= "</tasks>";
		$xmlStr .= "</chart>";

		return $xmlStr;
	}

    //  Create Display Information Function
    if (isset($_POST["save"]) && $_POST["save"] === "1") {
        $errorMessage = "";

        $project_type_id  = $_POST["projectType"];
        $project_id = $_POST["project"];
        $activity_type_id = $_POST["activityType"];
        $activity_id = $_POST["activity"];
        $date_type = $_POST["dateType"];
        $view = $_POST["view"];

        //  Get Dates
        if ($date_type == "currentwk") {
            $date_from = currentWeekStart();
            $date_to = $today;
        }
        else if ($date_type == "previouswk") {
            $date_from = getDates(currentWeekStart(), -7);
            $date_to = getDates(currentWeekStart(), -1);
        }
        else if ($date_type == "currentmnth") {
            $date_from = getMonthStart($today);
            $date_to = $today;
        }
        else if ($date_type == "previousmnth") {
            $date_from = getPreviousMonthStart();
            $date_to = getMonthEnd(getPreviousMonthStart());
        }
        else if ($date_type == "projectLifetime")      {
            $date_fromT = q("SELECT MIN(date) FROM TimeSheet WHERE project_id = '".$project_id."'");
            $date_fromE = q("SELECT MIN(date) FROM ExpenseSheet WHERE project_id = '".$project_id."'");
            $date_from = ($date_fromT <= $date_fromE) ? $date_fromT : $date_fromE;
            $date_to = $today;
        }
        else if ($date_type == "custom") {
            $date_from = getMonthStart(addslashes(strip_tags($_POST["date_from"])));
            $date_to = getMonthEnd(addslashes(strip_tags($_POST["date_to"])));
        }

        if ($view == "day")
            $period = getDays($date_from, $date_to);
        else if ($view == "week")
            $period = getDays($date_from, $date_to) / 7;
        else
            $period = getMonthPeriod($date_from, $date_to);

		unset($categories);

        if ($view == "day")	{
			$duration = 7;
			$durationUnit = "d";

            for ($i = 0; $i < $period; $i++) {
				$start = getDates($date_from, $i)." 00:00:00";
				$end = getDates($date_from, $i)." 23:59:59";
				$display = getDay($start)."&#xA;".str_replace("-","/",$start);

				$categories[$i][0] = $start;
				$categories[$i][1] = $end;
				$categories[$i][2] = $display;
            }
        }
        else if ($view == "week")	{
			$duration = 7;
			$durationUnit = "d";

            $i = 0;

            for ($i; $i < ($period - 1); $i++)	{
				$start = getDates($date_from, ($i * 7))." 00:00:00";
				$end = getDates($date_from, ($i * 7) + 6)." 23:59:59";
				$display = "Week ".getWeek($start)."&#xA;".str_replace("-","/",$start)." - ".str_replace("-","/",$end);

				$categories[$i][0] = $start;
				$categories[$i][1] = $end;
				$categories[$i][2] = $display;
            }

            if (getDates($date_from, ($i * 7)) < $date_to)	{
				$start = getDates($date_from, ($i * 7))." 00:00:00";
				$end = $date_to." 23:59:59";
				$display = "Week ".getWeek($start)."&#xA;".str_replace("-","/",$start)." - ".str_replace("-","/",$end);

				$categories[$i][0] = $start;
				$categories[$i][1] = $end;
				$categories[$i][2] = $display;
            }
        }
        else	{
			$duration = 3;
			$durationUnit = "m";

            for ($i = 0; $i < $period; $i++)	{
				$start = getNextMonthStart($date_from, $i)." 00:00:00";
				$end = getMonthEnd($start)." 23:59:59";
				$monthStr = getNextMonth($date_from, $i);

				$categories[$i][0] = $start;
				$categories[$i][1] = $end;
				$categories[$i][2] = $monthStr;
            }
        }

        //  Timestamp
        if ($date_from === $date_to)
            $timestamp = "Time Period: ".$date_from;
        else
            $timestamp = "Time Period: ".$date_from." - ".$date_to;

		$where = "ts.date >= '$date_from' AND ts.date <= '$date_to' ";

        if (!($project_id != "null" && is_numeric($project_id))) {
            if ($project_type_id != "null" && is_numeric($project_type_id))
                $where .= "AND p.type_id = '$project_type_id' ";
        }
        else
            $where .= "AND ts.project_id = '$project_id' ";

        if (!($activity_id != "null" && is_numeric($activity_id))) {
            if ($activity_type_id != "null" && is_numeric($activity_type_id))
                $where .= "AND at.id = '$activity_type_id' ";
        }
        else
            $where .= "AND ts.activity_id = '$activity_id' ";

		$activityTypes = q("SELECT DISTINCT(at.type), at.id ".
							"FROM ((((TimeSheet AS ts) LEFT JOIN Activities AS a ON ts.activity_id = a.id) ".
								"INNER JOIN ActivityTypes AS at ON a.parent_id = at.id) INNER JOIN Project AS p ON ts.project_id = p.id)".
							"WHERE $where AND at.company_id = '".$_SESSION["company_id"]."' ORDER BY at.type");

		if ($activityTypes)	{
			$graph = createChartXML($categories, $activityTypes, $where, $date_from, $date_to, $view, $period, $duration, $durationUnit);
			$displayString = renderChart("fusion/Gantt.swf", "", $graph, "gantt", (800 * 3), ($rows * 25), false, false);
		}

        if ($displayString != "")
            $generated = "1";
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("3", "reports");
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    jQuery(function(){
	jQuery('#date_from').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_from').val(),
		current: jQuery('#date_from').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_from').val()))
			_date = new Date();
			else _date = jQuery('#date_from').val();
			jQuery('#date_from').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_from').val(formated);
			jQuery('#date_from').DatePickerHide();
		}
	});
    })

    jQuery(function(){
	jQuery('#date_to').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_to').val(),
		current: jQuery('#date_to').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_to').val()))
			_date = new Date();
			else _date = jQuery('#date_to').val();
			jQuery('#date_to').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_to').val(formated);
			jQuery('#date_to').DatePickerHide();
		}
	});


        jQuery("#project").change(function()    {
            if (jQuery(this).val() != "null")   {
                jQuery("#dateType option[value='projectLifetime']").removeAttr("disabled");
            } else      {
                jQuery("#dateType option[value='projectLifetime']").attr("disabled","disabled");
                jQuery("#dateType option[value='currentwk']").attr("selected", "selected");
            }
        });
    });

    //  Sajax
    <?php sajax_show_javascript(); ?>

    function check()
    {
        var valid                                                       = 1;

        if (document.forms["report"].dateType.value == "custom") {
            //  Check That Date From Is Entered
            if (document.forms["report"].date_from.value == "") {
                ShowLayer("dateFrom", "block");
                valid                                                   = 0;
            }
            //  Check That Entered Date From Is Valid
            else {
                ShowLayer("dateFrom", "none");

                if (!validation("date", document.forms["report"].date_from.value)) {
                    ShowLayer("dateFromDiv", "block");
                    valid                                               = 0;
                }
                else
                    ShowLayer("dateFromDiv", "none");
            }

            //  Check That Date To Is Entered
            if (document.forms["report"].date_to.value == "") {
                ShowLayer("dateTo", "block");
                valid                                                   = 0;
            }
            //  Check That Entered Date To Is Valid
            else {
                ShowLayer("dateTo", "none");

                if (!validation("date", document.forms["report"].date_to.value)) {
                    ShowLayer("dateToDiv", "block");
                    valid                                               = 0;
                }
                else
                    ShowLayer("dateToDiv", "none");
            }
        }

        if (valid == 1) {
            document.forms["report"].save.value                         = 1;
            document.forms["report"].submit();
        }
    }

    function dateSelection() {
        if (document.forms["report"].dateType.value == "custom")
            ShowLayer("datesDiv", "block");
        else
            ShowLayer("datesDiv", "none");
    }

    ///////////////////////////
    //  Sajax
    function ClearOptions(OptionList) {
        for (x = OptionList.length; x >= 0; x--)
        {
            OptionList[x]                                               = null;
        }
    }
    
    //Set ActivityTypes
    function setActivityTypes(data) {
        var c                                                           = 0;
        document.report.activityType.options[(c++)]                         = new Option("All Activity Types", "null");
        for (var i in data)
            eval("document.report.activityType.options["+(c++)+"] = new Option('" + data[i][1] + "', '" + data[i][0] + "');");
    }

    function setActivities(data) {
        var c                                                           = 0;

        document.report.activity.options[(c++)]                         = new Option("All Activities", "null");

        for (var i in data)
            eval("document.report.activity.options["+(c++)+"] = new Option('" + data[i][1] + "', '" + data[i][0] + "');");
    }

    function setProjects(data) {
        var c                                                           = 0;

        document.report.project.options[(c++)]                         = new Option("All Projects", "null");

        for (var i in data)
            eval("document.report.project.options["+(c++)+"] = new Option('" + data[i][1] + "', '" + data[i][0] + "');");
    }

    function getProject(select) {
        ClearOptions(document.report.project);

        x_getProjects(select.value, setProjects);
    }
    
    //Get ActivityTypes
    function getActivityTypes(select) {
        ClearOptions(document.report.activityType);
        if (select.value != "null")
        {   
            ShowLayer("activityTypeDiv", "block");
            x_getActivityTypes(select.value, setActivityTypes);
        }
        else{
            ShowLayer("activityTypeDiv", "none");
        }
    }

    function getData(select) {
        ClearOptions(document.report.activity);

        if (select.value != "null")
        {
            ShowLayer("activityDiv", "block");
            x_getActivities(select.value, setActivities);
        }
        else
            ShowLayer("activityDiv", "none");
    }
    ///////////////////////////
</script>
<?php
    $projectTypes = q("SELECT id, type FROM ProjectTypes WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY type");
    $projectTypes[] = array("0","Shared Projects");
    $projectTypes = sortArrayByColumn($projectTypes, 1);
    
    $projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) ".
		"WHERE cot.company_id = '".$_SESSION["company_id"]."' AND p.completed = '0' AND p.deleted = '0' ORDER BY UPPER(p.name)");
    $activityTypes = q("SELECT id, type FROM ActivityTypes WHERE company_id = '".$_SESSION["company_id"]."' AND active = '1' ORDER BY type");
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
				<?php
					if ($generated != "1")	{
				?>
					<form action="" method="post" name="report">
						<div id="" style="<?php if ($generated == "1") echo "display: none;"; else echo "display: block"; ?>">
							<table width="100%">
								<tr>
									<td class="centerdata">
										<h6>Gantt Report</h6>
									</td>
								</tr>
							</table>
							<br/>
							<table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Project Type:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="projectType" onChange="getProject(projectType);">
                                        <option value="null">All Project Types</option>
                                        <?php
                                            if (is_array($projectTypes))
                                                foreach ($projectTypes as $projectType)
                                                    if ($_POST["projectType"] == $projectType[0])
                                                        echo "<option value='".$projectType[0]."' selected>".$projectType[1]."</option>";
                                                    else
                                                        echo "<option value='".$projectType[0]."'>".$projectType[1]."</option>";
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Project:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" id="project" name="project" onChange="getActivityTypes(project)">
                                        <option value="null">All Projects</option>
                                        <?php
                                            if (is_array($projects))
                                                foreach ($projects as $project)
                                                    if (isset($_POST["project"]) && $_POST["project"] == $project[0])
                                                        echo "<option value='".$project[0]."' selected>".$project[1]."</option>";
                                                    else
                                                        echo "<option value='".$project[0]."'>".$project[1]."</option>";
                                        ?>
                                    </select>
                                </td>
                            </tr>
                        </table>
                        
                        <div id="activityTypeDiv" style="display: none;">
                            <table width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Activity Type:
                                    </td>
                                    <td width="50%">
                                        <select class="on-field" method="post" name="activityType" onChange="getData(this);">
                                            <option value="null">All Activity Types</option>
                                            <?php
                                                if (is_array($activityTypes))
                                                    foreach ($activityTypes as $activityType)
                                                        if ($_POST["activityType"] == $activityType[0])
                                                            echo "<option value='".$activityType[0]."' selected>".$activityType[1]."</option>";
                                                        else
                                                            echo "<option value='".$activityType[0]."'>".$activityType[1]."</option>";
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                            
                            <div id="activityDiv" style="display: none;">
								<table width="100%">
									<tr>
										<td class="on-description" width="50%">
												Activity:
										</td>
										<td width="50%">
											<select class="on-field" method="post" name="activity">
												<option value="null">All Activities</option>
											</select>
										</td>
									</tr>
								</table>
							</div>
							<table width="100%">
								<tr>
									<td class="on-description" width="50%">
                                        View Per:
									</td>
									<td class="on-description-left" width="50%">
										<input method="post" name="view" type="radio" value="day" checked><a>Day</a><br/>
										<input method="post" name="view" type="radio" value="week"><a>Week</a><br/>
										<input method="post" name="view" type="radio" value="month"><a>Month</a><br/>
									</td>
								</tr>
								<tr>
									<td class="on-description" width="50%">
                                        Date Type(s):
									</td>
									<td width="50%">
										<select class="on-field" method="post" id="dateType" name="dateType" onChange="dateSelection();">
											<option value="currentwk">Current Week</option>
											<option value="previouswk">Previous Week</option>
											<option value="currentmnth">Current Month</option>
											<option value="previousmnth">Previous Month</option>
                                                                                        <option value="projectLifetime" disabled>Project Lifetime</option>
											<option value="custom">Custom Dates</option>
										</select>
									</td>
								</tr>
							</table>
							<div id="datesDiv" style="display: none;">
								<table cellpadding="0" cellspacing="2" width="100%">
									<tr>
										<td class="on-description" valign="top" width="50%">
                                            Date From:
										</td>
										<td width="50%">
											<input class="on-field-date" id="date_from" name="date_from" type="text" style="text-align:right;" value="<?php
												if ($_POST["date_from"] != "") echo "".$_POST["date_from"]; else echo "".getMonthStart($today); ?>">
											<div id="dateFrom" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
											<div id="dateFromDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font>
											</div>
										</td>
									</tr>
									<tr>
										<td class="on-description" valign="top" width="50%">
                                            Date To:
										</td>
										<td width="50%">
											<input class="on-field-date" id="date_to" name="date_to" type="text" style="text-align:right;" value="<?php
												if ($_POST["date_to"] != "") echo "".$_POST["date_to"]; else echo "".getMonthEnd($today); ?>">
											<div id="dateTo" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
											<div id="dateToDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
										</td>
									</tr>
								</table>
							</div>
							<br/>
							<input name="btnGenerate" onClick="check();" type="button" value="Generate Report">
							<input method="post" name="save" type="hidden" value="0" />
						</div>
					</form>
				<?php
					}
					else	{
                        echo "<div class='on-20px'><table class='on-table-center on-table'>";
                            echo "".$displayString;
                            echo  "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        echo "</table></div>";
					}
				?>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
