<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

	if (!hasAccess("PROJ_MANAGE"))
		header("Location: noaccess.php");

    $errorMessage                                                       = "";

    //  Get Project Information
    function updateInfo()   {
        $id                                                             = $_GET["id"];
        $project_name                                                   = addslashes(strip_tags($_POST["project_name"]));
        $registration_date                                              = addslashes(strip_tags($_POST["registration_date"]));
        $submitted_by                                                   = $_POST["submitted_by"];
        $project_manager                                                = $_POST["project_manager"];

        if ($project_manager == "null")
            $project_manager                                            = "0";

        $project_type                                                   = $_POST["project_type"];
        $total_budget                                                   = addslashes(strip_tags($_POST["total_budget"]));

        if ($total_budget != "")
            $total_budget                                               = number_format($total_budget, 2, ".", "");

        $consulting_budget                                              = addslashes(strip_tags($_POST["consulting_budget"]));

        if ($consulting_budget != "")
            $consulting_budget                                          = number_format($consulting_budget, 2, ".", "");

        $expense_budget                                                 = addslashes(strip_tags($_POST["expense_budget"]));

        if ($expense_budget != "")
            $expense_budget                                             = number_format($expense_budget, 2, ".", "");

        $diverse_income_budget                                          = addslashes(strip_tags($_POST["diverse_income_budget"]));

        if ($diverse_income_budget != "")
            $diverse_income_budget                                      = number_format($diverse_income_budget, 2, ".", "");

        $vat                                                            = addslashes(strip_tags($_POST["vat"]));

        if ($vat == "")
            $vat                                                        = 0;

        $buffer                                                         = addslashes(strip_tags($_POST["buffer"]));

        if ($buffer == "")
            $buffer                                                     = 0;

        $due_date                                                       = addslashes(strip_tags($_POST["due_date"]));
        $completed                                                      = $_POST["completed"];

        if (!$completed)
            $completed                                                  = 0;

        $status                                                         = $_POST["status"];

        //  Check If Project Exists In Database
        $exist                                                          = q("SELECT id FROM Project WHERE name = '$project_name' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."' AND id != '$id'");

        if (submitDateCheck($registration_date))   {
        if (!$exist)
        {
			$d_ = date("Y-m-d H:i:s");
            $update                                                     = q("UPDATE Project SET name = '$project_name', reg_date = '$registration_date', ".
                                                                            "submitted_by_id = '$submitted_by', manager_id = '$project_manager', ".
																			"type_id = '$project_type', due_date = '$due_date', completed = '$completed', status = '$status', ".
																		   	" dateUpdated = '$d_', updatedBy = '".$_SESSION["email"]."' WHERE id = '$id'");
            $update2                                                     = q("UPDATE companiesOnTeam SET  ".
                                                                            "total_budget = '$total_budget', consulting = '$consulting_budget', expenses = '$expense_budget', ".
																			"diverse_income = '$diverse_income_budget', vat = '$vat', buffer = '$buffer', dateUpdated = '$d_', ".
																			"updatedBy = '".$_SESSION["email"]."' WHERE project_id = '$id'");
        }
        else
            $errorMessage                                               = "Project Already Exists";
        }

        if ($update)
            return true;
        else
            return false;
    }

    //  Project Manager Selected
    if (isset($_POST["project_manager"]))
        $update                                                         = updateInfo();

    //  Create Project Team Button Pressed
    if (isset($_POST["btnCreate"]))
    {
        $update                                                         = updateInfo();

        if (!isset($_SESSION["project_id"]))
            $_SESSION["project_id"]                                     = $_GET["id"];

        if (!isset($_SESSION["location"]))
            $_SESSION["location"]                                       = "Location: project_edit.php?id=".$_GET["id"];

        $_SESSION["projectInfo"]                                        = $_POST["project_manager"];

        if ($_POST["project_manager"] != "null")
            header("Location: project_team.php");
        else
            $errorMessage                                               = "Before Creating Team, Please Select Project Manager";
    }

    //  Update Project Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")
    {
        $id                                                             = $_GET["id"];
        $project_name                                                   = addslashes(strip_tags($_POST["project_name"]));
        if (submitDateCheck(addslashes(strip_tags($_POST["registration_date"]))))   {
        $update                                                         = updateInfo();

        if ($update)
        {
            //  Insert/Update Project Manager Details to Team
            $project_manager                                            = $_POST["project_manager"];
            $project_manager_tariff                                     = $_POST["project_manager_tariff"];

	    if ($project_manager_tariff == 0)
		$project_manager_tariff					= 1;

            if ($project_manager != "null")
            {
                $employee_info                                          = q("SELECT frstname, lstname FROM Employee WHERE id = '$project_manager'");
                $budget                                                 = addslashes(strip_tags($_POST["project_manager_budget"]));

                if ($budget == "Assign Budget" || $budget == "" || $budget == "0" || $budget == "0.00")
                    $budget                                             = null;
                else
                    $budget                                             = number_format($budget, 2, ".", "");

                $exist                                                  = q("SELECT id FROM Project_User WHERE project_id = '$id' AND manager = '1'");

                if (!$exist)
                {
                    $insert                                             = q("INSERT INTO Project_User (user_id, user_tariff, user_budget, project_id, manager) ".
                                                                            "VALUES ('$project_manager', '$project_manager_tariff', '$budget', '$id', '1')");

                    if ($insert)
                    {
                        $time                                           = date("H:i:s");

                        $logs                                           = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('".$employee_info[0][0]." ".$employee_info[0][1]." added to project team', ".
                                                                            "'Insert', 'Project_User', '".$_SESSION["email"]."', '$today', '$time', ".
                                                                            "'".$_SESSION["company_id"]."')");
                    }
                }
                else
                {
                    $update                                             = q("UPDATE Project_User SET user_id = '$project_manager', ".
                                                                            "user_tariff = '$project_manager_tariff',  user_budget = '$budget' WHERE project_id = '$id' ".
                                                                            "AND manager = '1'");

                    if ($update)
                    {
                        $time                                           = date("H:i:s");

                        $logs                                           = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('".$employee_info[0][0]." ".$employee_info[0][1]." information changed at team', ".
                                                                            "'Update', 'Project_User', '".$_SESSION["email"]."', '$today', '$time', ".
                                                                            "'".$_SESSION["company_id"]."')");
                    }
                }
            }
            //  If There Is A Manager - Remove
            else
            {
                $delete                                                 = q("DELETE FROM Project_User WHERE project_id = '$id' AND manager = '1'");

                if ($delete)
                {
                    $time                                               = date("H:i:s");

                    $logs                                               = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('Project manager removed', 'Delete', 'Project_User', '".$_SESSION["email"]."', ".
                                                                            "'$today', '$time', '".$_SESSION["company_id"]."')");
                }
            }

            $members                                                    = q("SELECT e.id, e.lstname, e.frstname FROM (Employee as e INNER JOIN Project_User as pu ".
                                                                            "ON e.id = pu.user_id) WHERE pu.project_id = '$id' AND pu.manager = '0' ".
                                                                            "ORDER BY e.lstname, e.frstname");

            if (is_array($members)) {
                foreach ($members as $member) {
                    //  Check If Member Is Project Manager
                    $is_manager                                         = q("SELECT * FROM Project_User WHERE project_id = '$id' ".
                                                                            "AND user_id = '$member[0]' AND manager = '1'");

                    if (!is_array($is_manager)) {
                        $budget                                         = addslashes(strip_tags($_POST["team_member_budget".$member[0]]));

                        if ($budget == "Assign Budget" || $budget == "" || $budget == "0" || $budget == "0.00")
                            $budget                                     = null;
                        else
                            $budget                                     = number_format($budget, 2, ".", "");

                        $update                                         = q("UPDATE Project_User SET user_budget = '$budget' WHERE user_id = '$member[0]' ".
                                                                            "AND project_id = '$id'");
                    }
                    else
                        $delete                                         = q("DELETE FROM Project_User WHERE project_id = '$id' ".
                                                                            "AND user_id = '$member[0]' AND manager = '0'");
                }
            }

            $time                                                       = date("H:i:s");

            $logs                                                       = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('".$project_name." updated', 'Update', 'Project', '".$_SESSION["email"]."', ".
                                                                            "'$today', '$time', '".$_SESSION["company_id"]."')");

            header("Location: projects.php");
        }
        }
    }

    if ($errorMessage != "")
    {
        if ($errorMessage == "Please note that we are currently busy with data management, no entries are allowed for 2010-08-26. Please check back in 24 Hours.")
            echo "<p class='centerdata' style='padding:0px;'><strong style='font-size:12px;'><font class='on-validate-error'>$errorMessage</font></strong></p>";
        else
            echo "<p class='centerdata' style='padding:0px;'><strong><font class='on-validate-error'>$errorMessage</font></strong></p>";
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0", "projects");
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    jQuery(function(){
	jQuery('#registration_date').DatePicker({
		format:'Y-m-d',
		date: jQuery('#registration_date').val(),
		current: jQuery('#registration_date').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#registration_date').val()))
			_date = new Date();
			else _date = jQuery('#registration_date').val();
			jQuery('#registration_date').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#registration_date').val(formated);
			jQuery('#registration_date').DatePickerHide();
		}
	});
    })

    jQuery(function(){
	jQuery('#due_date').DatePicker({
		format:'Y-m-d',
		date: jQuery('#due_date').val(),
		current: jQuery('#due_date').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#due_date').val()))
			_date = new Date();
			else _date = jQuery('#due_date').val();
			jQuery('#registration_date').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#due_date').val(formated);
			jQuery('#due_date').DatePickerHide();
		}
	});
    })

    //  Internet Browser
    var type                                                            = "MO";

    if (window.opera)
        type                                                            = "OP";
    else if (document.all)
        type                                                            = "IE";	//IE4+
    else if (!document.all && document.getElementById)
        type                                                            = "MO";
    else
        type                                                            = "IE";

    var valid                                                           = 1;

    function check()
    {
        //  Check That Project Name Is Entered
        if (document.forms["project_edit"].project_name.value == "")
        {
            ShowLayer("projectName", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("projectName", "none");

        // Check That Registration Date Is Entered
        if (document.forms["project_edit"].registration_date.value == "")
        {
            ShowLayer("registrationDate", "block");
            valid                                                       = 0;
        }
        //  Check That Entered Registration Date Is Valid
        else
        {
            ShowLayer("registrationDate", "none");

            if (!validation("date", document.forms["project_edit"].registration_date.value))
            {
                ShowLayer("registrationDateDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("registrationDateDiv", "none");
        }

        //  Check That Total Budget Is Valid, If Entered
        if (document.forms["project_edit"].total_budget.value != "")
        {
            if (!validation("currency", document.forms["project_edit"].total_budget.value))
            {
                ShowLayer("totalBudget", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("totalBudget", "none");
        }
        else
            ShowLayer("totalBudget", "none");

        //  Check That Consulting Budget Is Valid, If Entered
        if (document.forms["project_edit"].consulting_budget.value != "")
        {
            if (!validation("currency", document.forms["project_edit"].consulting_budget.value))
            {
                ShowLayer("consultingBudget", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("consultingBudget", "none");
        }
        else
            ShowLayer("consultingBudget", "none");

        //  Check That Expense Budget Is Valid, If Entered
        if (document.forms["project_edit"].expense_budget.value != "")
        {
            if (!validation("currency", document.forms["project_edit"].expense_budget.value))
            {
                ShowLayer("expenseBudget", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("expenseBudget", "none");
        }
        else
            ShowLayer("expenseBudget", "none");

        //  Check That Diverse Income Budget Is Valid, If Entered
        if (document.forms["project_edit"].diverse_income_budget.value != "")
        {
            if (!validation("currency", document.forms["project_edit"].diverse_income_budget.value))
            {
                ShowLayer("diverseIncomeBudget", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("diverseIncomeBudget", "none");
        }
        else
            ShowLayer("diverseIncomeBudget", "none");

        //  Check That Vat Is Valid, If Entered
        if (document.forms["project_edit"].vat.value != "")
        {
            if (!validation("percentage", document.forms["project_edit"].vat.value))
            {
                ShowLayer("VAT", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("VAT", "none");
        }
        else
            ShowLayer("VAT", "none");

        //  Check That Buffer Is Valid, If Entered
        if (document.forms["project_edit"].buffer.value != "")
        {
            if (!validation("percentage", document.forms["project_edit"].buffer.value))
            {
                ShowLayer("BUFFER", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("BUFFER", "none");
        }
        else
            ShowLayer("BUFFER", "none");

        //  Check That Due Date Is Valid, If Entered
        if (document.forms["project_edit"].due_date.value != "")
        {
            if (!validation("date", document.forms["project_edit"].due_date.value))
            {
                ShowLayer("dueDateDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("dueDateDiv", "none");
        }
        else
            ShowLayer("dueDateDiv", "none");

        if (valid == 1)
        {
            document.forms["project_edit"].save.value                   = 1;
            document.forms["project_edit"].submit();
        }
    }

    var tempValue                                                       = "";

    function clearBox(box)
    {
        tempValue                                                       = box.value;
        box.value                                                       = "";
    }

    function test_budget(box, div)
    {
        //  Test Entered Value
        if (box.value != "")
        {
            if (!validation("currency", box.value))
            {
                ShowLayer(div, "block");
                valid                                                   = 0;
            }
            else
            {
                ShowLayer(div, "none");
                valid                                                   = 1;
            }
        }
        else
        {
            box.value                                                   = tempValue;
            tempValue                                                   = "";
            ShowLayer(div, "none");
        }
    }

    function projectStatus(type)
    {
        if (type == "proposal")
        {
            ShowLayer("viewActive1", "none");
            ShowLayer("viewActive2", "none");
        }
        else
        {
            ShowLayer("viewActive1", "block");
            ShowLayer("viewActive2", "block");
        }
    }
</script>
<?php
    $id                                                                 = $_GET["id"];
    $project_info                                                       = q("SELECT p.name, p.reg_date, p.submitted_by_id, p.manager_id, p.type_id, c.total_budget, c.consulting, ".
																			"c.expenses, c.diverse_income, c.vat, c.buffer, p.due_date, p.completed, p.status FROM Project AS p INNER JOIN ".
																			"companiesOnTeam AS c ON p.id = c.project_id WHERE p.id = '$id'");

    //  nEmployees                                                      = Number of Employees
    $nEmployees                                                         = q("SELECT COUNT(e.id) FROM (Employee AS e INNER JOIN Company_Users ".
                                                                            "AS cu ON e.id = cu.user_id) WHERE cu.company_id = '".$_SESSION["company_id"]."' ".
                                                                            "AND e.email != 'admin' AND e.deleted = '0'");
    $employees                                                          = q("SELECT e.id, e.lstname, e.frstname FROM (Employee AS e INNER JOIN Company_Users ".
                                                                            "AS cu ON e.id = cu.user_id) WHERE cu.company_id = '".$_SESSION["company_id"]."' ".
                                                                            "AND e.email != 'admin' AND e.deleted = '0' ORDER BY e.lstname, frstname");

    //  nManagers                                                       = Number of Managers & Project Managers
    $nManagers                                                          = q("SELECT COUNT(DISTINCT(e.id)) FROM (((Employee AS e INNER JOIN user_role AS ur ON ur.userid = e.id) ".
                                                                            "INNER JOIN role_action AS ra ON ur.roleid = ra.roleid) INNER JOIN actions AS a ON a.id = ra.actionid) ".
                                                                            "WHERE ur.companyid = '".$_SESSION["company_id"]."' AND a.action = 'PROJ_MANAGE' ".
                                                                            "AND e.email != 'admin' AND e.deleted = '0'");
    $managers                                                           = q("SELECT DISTINCT(e.id), e.lstname, e.frstname FROM (((Employee AS e INNER JOIN user_role AS ur ON ur.userid = e.id) ".
                                                                            "INNER JOIN role_action AS ra ON ur.roleid = ra.roleid) INNER JOIN actions AS a ON a.id = ra.actionid) ".
                                                                            "WHERE ur.companyid = '".$_SESSION["company_id"]."' AND a.action = 'PROJ_MANAGE' ".
                                                                            "AND e.email != 'admin' AND e.deleted = '0'");

    //  nProjectTypes                                                   = Number of Project Types
    $nProjectTypes                                                      = q("SELECT COUNT(id) FROM ProjectTypes WHERE company_id = '".$_SESSION["company_id"]."'");
    $projectTypes                                                       = q("SELECT id, type FROM ProjectTypes WHERE company_id = '".$_SESSION["company_id"]."' ".
                                                                            "ORDER BY type");

    //  Check If Project Team Exists
    $nMembers                                                           = q("SELECT COUNT(e.id) FROM (Employee as e INNER JOIN Project_User as pu ON e.id = pu.user_id) ".
                                                                            "WHERE pu.project_id = '$id' AND pu.manager = '0' AND e.deleted = '0'");
    $members                                                            = q("SELECT e.id, e.lstname, e.frstname, pu.user_budget FROM (Employee as e ".
                                                                            "INNER JOIN Project_User as pu ON e.id = pu.user_id) WHERE pu.project_id = '$id' ".
                                                                            "AND pu.manager = '0' AND e.deleted = '0' ORDER BY lstname, frstname");
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="project_edit">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>
                                    Edit Project
                                </h6>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <table cellpadding="0" cellspacing="2" width="100%">
                        <tr>
                            <td align="right" class="on-description" width="50%">
                                Proposal State
                                <input method="post" name="status" onClick="projectStatus('proposal');" tabindex="1" type="radio" value="proposal" <?php
                                    if ($project_info[0][13] == "proposal") echo "checked"; ?>>&nbsp;
                            </td>
                            <td class="on-description-left" width="50%">
                               <input method="post" name="status" onClick="projectStatus('active');" tabindex="1" type="radio" value="active" <?php
                                    if ($project_info[0][13] == "active") echo "checked"; ?>>
                                Active State
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                    Project Name:
                            </td>
                            <td width="50%">
                                <input class="on-field" name="project_name" tabindex="1" size="50" type="text" value="<?php echo $project_info[0][0]; ?>">
                                <div id="projectName" style="display: none;"><font class="on-validate-error">* Project name must be entered</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="on-description" width="50%">
                                    Registration Date:
                            </td>
                            <td width="50%">
                                <input class="on-field"  id="registration_date" name="registration_date" tabindex="2" type="text" style="text-align:right;" value="<?php echo $project_info[0][1]; ?>">
                                <div id="registrationDate" style="display: none;"><font class="on-validate-error">* Registration date must be entered</font></div>
                                <div id="registrationDateDiv" style="display: none;"><font class="on-validate-error">* Registration date not valid, eg.
                                    <?php echo "".date("Y-m-d"); ?></font></div>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="on-description" width="50%">
                                    Plan Submitted By:
                            </td>
                            <td width="50%">
                                <select class="on-field" method="post" name="submitted_by" tabindex="3">
                                    <option value="null">--  Select Employee --&nbsp;&nbsp;</option>
                                    <?php
                                        if ($nEmployees > 1)
                                        {
                                            foreach ($employees as $employee)
                                                if ($project_info[0][2] == $employee[0])
                                                    echo "<option value='".$employee[0]."' selected>".$employee[1].", ".$employee[2]."&nbsp;&nbsp;</option>";
                                                else
                                                    echo "<option value='".$employee[0]."'>".$employee[1].", ".$employee[2]."&nbsp;&nbsp;</option>";
                                        }
                                        else if ($nEmployees == 1)
                                            if ($project_info[0][2] == $employees[0][0])
                                                echo "<option value='".$employees[0][0]."' selected>".$employees[0][1].", ".$employees[0][2]."&nbsp;&nbsp;</option>";
                                            else
                                                echo "<option value='".$employees[0][0]."'>".$employees[0][1].", ".$employees[0][2]."&nbsp;&nbsp;</option>";
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="on-description" width="50%">
                                    Project Manager:
                            </td>
                            <td width="50%">
                                <?php
                                    echo "<select class='on-field' method='post' name='project_manager' onChange='submit();' tabindex='4'>";
                                        echo "<option value='null'>--  Select Project Manager --&nbsp;&nbsp;</option>";
                                        if ($nManagers > 1)
                                        {
                                            foreach ($managers as $manager)
                                                if ($project_info[0][3] == $manager[0])
                                                    echo "<option value='".$manager[0]."' selected>".$manager[1].", ".$manager[2]."&nbsp;&nbsp;</option>";
                                                else
                                                    echo "<option value='".$manager[0]."'>".$manager[1].", ".$manager[2]."&nbsp;&nbsp;</option>";
                                        }
                                        else if ($nManagers == 1)
                                            if ($project_info[0][3] == $managers[0][0])
                                                echo "<option value='".$managers[0][0]."' selected>".$managers[0][1].", ".$managers[0][2]."&nbsp;&nbsp;</option>";
                                            else
                                                echo "<option value='".$managers[0][0]."'>".$managers[0][1].", ".$managers[0][2]."&nbsp;&nbsp;</option>";
                                    echo "</select>";
                                ?>
                            </td>
                        </tr>
                        <?php
                            if ($projectInfo[3] != "null")
                            {
                                $tariffs                                = q("SELECT tariff1, tariff2, tariff3, tariff4, tariff5 FROM Employee ".
                                                                            "WHERE id = '".$project_info[0][3]."'");
                                $tariff_id                              = q("SELECT user_tariff FROM Project_User WHERE user_id = '".$project_info[0][3]."' ".
                                                                            "AND project_id = '$id'");

                                echo "<tr>";
                                    echo "<td class='on-description' width='50%'>";
                                            echo "Project Manager Tariff:";
                                    echo "</td>";
                                    echo "<td align='left' width='50%'>";
                                        echo "<select class='on-field' method='post' name='project_manager_tariff' tabindex='5'>";
                                            if ($tariffs[0][0] > 0)
                                                if ($tariff_id == 1)
                                                    echo "<option value='1' selected>".$tariffs[0][0]."&nbsp;&nbsp;</option>";
                                                else
                                                    echo "<option value='1'>".$tariffs[0][0]."&nbsp;&nbsp;</option>";

                                            if ($tariffs[0][1] > 0)
                                                if ($tariff_id == 2)
                                                    echo "<option value='2' selected>".$tariffs[0][1]."&nbsp;&nbsp;</option>";
                                                else
                                                    echo "<option value='2'>".$tariffs[0][1]."&nbsp;&nbsp;</option>";

                                            if ($tariffs[0][2] > 0)
                                                if ($tariff_id == 3)
                                                    echo "<option value='3' selected>".$tariffs[0][2]."&nbsp;&nbsp;</option>";
                                                else
                                                    echo "<option value='3'>".$tariffs[0][2]."&nbsp;&nbsp;</option>";

                                            if ($tariffs[0][3] > 0)
                                                if ($tariff_id == 4)
                                                    echo "<option value='4' selected>".$tariffs[0][3]."&nbsp;&nbsp;</option>";
                                                else
                                                    echo "<option value='4'>".$tariffs[0][3]."&nbsp;&nbsp;</option>";

                                            if ($tariffs[0][4] > 0)
                                                if ($tariff_id == 5)
                                                    echo "<option value='5' selected>".$tariffs[0][4]."&nbsp;&nbsp;</option>";
                                                else
                                                    echo "<option value='5'>".$tariffs[0][4]."&nbsp;&nbsp;</option>";
                                        echo "</select>";
                                    echo "</td>";
                                echo "</tr>";
                            }
                        ?>
                        <tr>
                            <td align="right" class="on-description" width="50%">
                                    Project Type:
                            </td>
                            <td width="50%">
                                <?php
                                    echo "<select class='on-field' method='post' name='project_type' tabindex='6'";
                                        echo "<option value='null'>--  Select Project Type --&nbsp;&nbsp;</option>";
                                        if ($nProjectTypes > 1)
                                        {
                                            foreach ($projectTypes as $projectType)
                                                if ($project_info[0][4] == $projectType[0])
                                                    echo "<option value='".$projectType[0]."' selected>".$projectType[1]."&nbsp;&nbsp;</option>";
                                                else
                                                    echo "<option value='".$projectType[0]."'>".$projectType[1]."&nbsp;&nbsp;</option>";
                                        }
                                        else if ($nProjectTypes == 1)
                                            if ($project_info[0][4] == $projectTypes[0][0])
                                                echo "<option value='".$projectTypes[0][0]."' selected>".$projectTypes[0][1]."&nbsp;&nbsp;</option>";
                                            else
                                                echo "<option value='".$projectTypes[0][0]."'>".$projectTypes[0][1]."&nbsp;&nbsp;</option>";
                                    echo "</select>";
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="centerdata" colspan="2"><br/>
                                <input name="btnCreate" type="submit" value="Create/Update Project Team"><br/><br/>
                            </td>
                        </tr>
                    </table>
                    <div id="viewActive1" style="<?php if ($project_info[0][13] == "proposal") echo "display: none;"; else echo "display: block;";?>">
                        <table cellpadding="0" cellspacing="2" width="100%">
                            <tr>
                                <td align="right" class="on-description" width="50%">
                                        Total Project Budget <i>(R)</i>:
                                </td>
                                <td width="50%">
                                    <input class="on-field"  name="total_budget" style="text-align:right;" tabindex="7" type="text" value="<?php if ($project_info[0][5] > 0)
                                        echo $project_info[0][5];?>"><font class="on-description-left">* excl</font>
                                    <div id="totalBudget" style="display: none;"><font class="on-validate-error">* Entered amount must be numeric, eg. 123.45</font></div>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="on-description" width="50%">
                                        Consulting Budget <i>(R)</i>:
                                </td>
                                <td width="50%">
                                    <input class="on-field"  name="consulting_budget" style="text-align:right;" tabindex="8" type="text" value="<?php if ($project_info[0][6] > 0)
                                        echo $project_info[0][6]; ?>"><font class="on-description-left">* excl</font>
                                    <div id="consultingBudget" style="display: none;"><font class="on-validate-error">* Entered amount must be numeric, eg. 123.45</font></div>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="on-description" width="50%">
                                        Expense Budget <i>(R)</i>:
                                </td>
                                <td width="50%">
                                    <input class="on-field"  name="expense_budget" style="text-align:right;" tabindex="9" type="text" value="<?php if ($project_info[0][7] > 0)
                                        echo $project_info[0][7]; ?>"><font class="on-description-left">* excl</font>
                                    <div id="expenseBudget" style="display: none;"><font class="on-validate-error">* Entered amount must be numeric, eg. 123.45</font></div>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="on-description" width="50%">
                                        Diverse Income Budget <i>(R)</i>:
                                </td>
                                <td width="50%">
                                    <input class="on-field"  name="diverse_income_budget" style="text-align:right;" tabindex="10" type="text" value="<?php if ($project_info[0][8] > 0)
                                        echo $project_info[0][8]; ?>"><font class="on-description-left">* excl</font>
                                    <div id="diverseIncomeBudget" style="display: none;"><font class="on-validate-error">* Entered amount must be numeric, eg. 123.45</font></div>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="on-description" width="50%">
                                        VAT <i>(%)</i>:
                                </td>
                                <td width="50%">
                                    <input class="on-field-date"  name="vat" style="text-align:right;" tabindex="11" type="text" value="<?php if ($project_info[0][9] > 0)
                                        echo $project_info[0][9];?>">
                                    <div id="VAT" style="display: none;"><font class="on-validate-error">* Entered amount must be a percentage, eg. 100%</font></div>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="on-description" width="50%">
                                        Buffer <i>(%)</i>:
                                </td>
                                <td width="50%">
                                    <input class="on-field-date"  name="buffer" style="text-align:right;" tabindex="12" type="text" value="<?php if ($project_info[0][10] > 0)
                                        echo $project_info[0][10];?>">
                                    <div id="BUFFER" style="display: none;"><font class="on-validate-error">* Entered amount must be a percentage, eg. 100%</font></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <?php
                        if ($project_info[0][3] != "null" || $nMembers > 0)
                        {
                            echo "<table cellpadding='0' cellspacing='2' width='100%'>";
                                echo "<tr>";
                                    echo "<td class='centerdata' colspan='2' width='100%'>";
                                        echo "<br/><br/><h6>";
                                            echo "Project Team Budget Assignment";
                                        echo "</h6><br/>";
                                    echo "</td>";
                                echo "</tr>";

                                //  Project Team - Manager
                                if ($project_info[0][3] != "null")
                                {
                                    $project_manager_info               = q("SELECT lstname, frstname FROM Employee WHERE id = '".$project_info[0][3]."'");
                                    $project_manager_budget             = q("SELECT user_budget FROM Project_User WHERE user_id = '".$project_info[0][3]."' ".
                                                                            "AND project_id = '$id'");

                                    if ($project_manager_budget == "" || $project_manager_budget == 0)
                                        $project_manager_budget         = "Assign Budget";

                                    echo "<tr>";
                                        echo "<td class='on-description' width='50%'>";
                                            echo "".$project_manager_info[0][0].", ".$project_manager_info[0][1]." <i>(PM)</i>&nbsp;";
                                        echo "</td>";
                                        echo "<td align='left' width='50%'>";
                                            echo "<input class='on-field'  name='project_manager_budget' tabindex='13' onBlur=\"test_budget(project_manager_budget, 'projectManagerBudget');\" ".
                                            "onFocus=\"clearBox(project_manager_budget);\" style='text-align:right;' type='text' value='".$project_manager_budget."'>".
                                            "<div id='projectManagerBudget' style='display: none;'><font class='on-validate-error'>* Entered amount must be numeric, eg. 123.45".
                                            "</font></div>";
                                        echo "</td>";
                                    echo "</tr>";
                                }

                                //  Project Team - Team
                                if ($nMembers > 1)
                                {
                                    foreach ($members as $member)
                                    {
                                        echo "<tr>";
                                            echo "<td class='on-description' width='50%'>";
                                                echo "".$member[1].", ".$member[2]."&nbsp;";
                                            echo "</td>";
                                            echo "<td align='left' width='50%'>";
                                                $budget                 = $member[3];

                                                if ($budget == "" || $budget == 0)
                                                    $budget             = "Assign Budget";

                                                echo "<input class='on-field'  name='team_member_budget".$member[0]."' tabindex='14' ".
                                                    "onBlur=\"test_budget(team_member_budget".$member[0].", 'teamMemberBudget".$member[0]."');\" ".
                                                    "onFocus=\"clearBox(team_member_budget".$member[0].");\" style='text-align:right;' type='text' ".
                                                    "value='".$budget."'><div id='teamMemberBudget".$member[0]."' style='display: none;'>".
                                                    "<font class='on-validate-error'>* Entered amount must be numeric, eg. 123.45</font></div>";
                                            echo "</td>";
                                        echo "</tr>";
                                    }
                                }
                                else if ($nMembers == 1)
                                {
                                    echo "<tr>";
                                        echo "<td align='right' valign='top' style='padding-top:4px;' width='50%'>";
                                            echo "".$members[0][1].", ".$members[0][2]."&nbsp;";
                                        echo "</td>";
                                        echo "<td align='left' width='50%'>";
                                            $budget                     = $members[0][3];

                                            if ($budget == "" || $budget == 0)
                                                $budget                 = "Assign Budget";

                                            echo "<input class='on-field'  name='team_member_budget".$members[0][0]."' tabindex='14' ".
                                                "onBlur=\"test_budget(team_member_budget".$members[0][0].", 'teamMemberBudget".$members[0][0]."');\" ".
                                                "onFocus=\"clearBox(team_member_budget".$members[0][0].");\" style='text-align:right;' type='text' ".
                                                "value='".$budget."'><div id='teamMemberBudget".$members[0][0]."' style='display: none;'>".
                                                "<font class='on-validate-error'>* Entered amount must be numeric, eg. 123.45</font></div>";
                                        echo "</td>";
                                    echo "</tr>";
                                }
                            echo "</table>";
                        }
                    ?>
                    <div id="viewActive2" style="<?php if ($project_info[0][13] == "proposal") echo "display: none;"; else echo "display: block;";?>">
                        <table cellpadding="0" cellspacing="2" width="100%">
                            <tr>
                                <td align="right" class="on-description" width="50%">
                                        Due Date:
                                </td>
                                <td width="50%">
                                    <input class="on-field-date" id="due_date" name="due_date" tabindex="15" type="text" style="text-align:right;" value="<?php if ($project_info[0][11] > 0)
                                        echo $project_info[0][11];?>">
                                    <div id="dueDateDiv" style="display: none;"><font class="on-validate-error">* Due date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="on-description" width="50%">
                                        Completed:
                                </td>
                                <td class="on-description-left" tabindex="16" width="50%">
                                    <input  name="completed" type="checkbox" value="1" <?php if ($project_info[0][12] == "1") echo "checked"; ?>>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <br/>
                    <input name="btnAdd" onClick="check();" tabindex="17" type="button" value="Update Project">
                    <input method="post" name="save" type="hidden" value="0" />
                </form>
            </td>
        </tr>
        <tr>
            <td class="centerdata">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
