<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

	if (!hasAccess("VEH_MANAGE"))
		header("Location: noaccess.php");

    //  Get Vehicles Information
    //  nVehicles                                                       = Number of Vehicles
    $nVehicles                                                          = q("SELECT COUNT(id) FROM Vehicle WHERE company_id = '".$_SESSION["company_id"]."' AND deleted = '0'");
    $vehicles                                                           = q("SELECT id, type, rate FROM Vehicle WHERE company_id = '".$_SESSION["company_id"]."' AND deleted = '0' ORDER BY type");

    //  Update Vehicle Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")
    {
        if ($nVehicles > 1)
        {   
            foreach ($vehicles as $vehicle)
            {
                $vehicle_name                                           = addslashes(strip_tags($_POST["vehicle_name$vehicle[0]"]));
                $vehicle_rate                                           = addslashes(strip_tags($_POST["vehicle_rate$vehicle[0]"]));

                $vehicle_rate                                           = number_format($vehicle_rate, 2, ".", "");
                
                if (is_numeric($vehicle_rate))
                    $update                                             = q("UPDATE Vehicle SET type = '$vehicle_name', rate = '$vehicle_rate' WHERE id = '$vehicle[0]'");
                else
                    $errorMessage                                       = "Entered Amounts Must Be Numeric, eg. 123.45";
            }
        }
        else if ($nVehicles == 1)
        {    
            foreach ($vehicles as $vehicle)
            {
                $vehicle_name                                               = addslashes(strip_tags($_POST["vehicle_name$vehicle[0]"]));
                $vehicle_rate                                               = addslashes(strip_tags($_POST["vehicle_rate$vehicle[0]"]));

                $vehicle_rate                                               = number_format($vehicle_rate, 2, ".", "");

                if (is_numeric($vehicle_rate))
                    $update                                                 = q("UPDATE Vehicle SET type = '$vehicle_name', rate = '$vehicle_rate' ".
                                                                                        "WHERE id = '$vehicle[0]'");
                else
                    $errorMessage                                           = "Entered Amounts Must Be Numeric, eg. 123.45";
            }
        }

        if ($errorMessage == "")
        {    
            $time                                                       = date("H:i:s");

            $logs                                                       = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                                    "VALUES ('Vehicles edited', 'Update', 'Vehicle', '".$_SESSION["email"]."', '$today', ".
                                                                                    "'$time', '".$_SESSION["company_id"]."')");

            $errorMessage                                               = "Vehicles Updated Successfully";

            header("Location: vehicles.php");
        }
    }

    if ($errorMessage != "")
        echo "<div style='width:100%; text-align:center'><strong><font class='on-validate-error'>$errorMessage</font></strong></div>";

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0", "vehicles");
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    function check()
    {
        var valid                                                       = 1;

        if (valid == 1)
        {
            document.forms["vehicle_edit"].save.value                   = 1;
            document.forms["vehicle_edit"].submit();
        }
    }
</script>
<?php
    //  nVehicles                                                       = Number of Vehicles
    $nVehicles                                                          = q("SELECT COUNT(id) FROM Vehicle WHERE company_id = '".$_SESSION["company_id"]."' ".
                                                                            "AND deleted = '0'");
    $vehicles                                                           = q("SELECT id, type, rate FROM Vehicle WHERE company_id = '".$_SESSION["company_id"]."' ".
                                                                            "AND deleted = '0' ORDER BY type");
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata">
                <form action="" method="post" name="vehicle_edit">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>
                                    Edit Vehicles
                                </h6>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <br/>
                            </td>
                        </tr>
                    </table>
                    <table class="on-table-center on-table" width="48%">
                        <tr>
                            <th width="50%">
                                Vehicle Name
                            </th>
                            <th width="50%">
                                Rate/Km
                            </th>
                        </tr>
                        <?php
                            if ($nVehicles > 1)
                            {
                                foreach ($vehicles as $vehicle)
                                {
                                    echo "<tr>";
                                        echo "<td class='rightdata'>";
                                            echo "<input name='vehicle_name".$vehicle[0]."' tabindex='1' size='60' type='text' value='".$vehicle[1]."'>";
                                        echo "</td>";
                                        echo "<td>";
                                            echo "<input name='vehicle_rate".$vehicle[0]."' style='text-align:right' tabindex='1' type='text' ".
                                                "value='".number_format($vehicle[2], 2, ".", "")."'>";
                                        echo "</td>";
                                    echo "</tr>";
                                }
                            }
                            else if ($nVehicles == 1)
                            {
                                echo "<tr>";
                                    echo "<td>";
                                        echo "<input name='vehicle_name".$vehicles[0][0]."' tabindex='1' size='60' type='text' value='".$vehicles[0][1]."'>";
                                    echo "</td>";
                                    echo "<td class='rightdata'>";
                                        echo "<input name='vehicle_rate".$vehicles[0][0]."' style='text-align:right' tabindex='1' type='text' ".
                                            "value='".number_format($vehicles[0][2], 2, ".", "")."'>";
                                    echo "</td>";
                                echo "</tr>";
                            }
                            else
                            {
                                echo "<tr>";
                                    echo "<td colspan ='100%' class='centerdata'>";
                                        echo "No vehicles";
                                    echo "</td>";
                                echo "</tr>";
                            }
                            echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        ?>
                    </table>
                    <br/>
                    <input name="btnUpdate" onClick="check();" tabindex="2" type="button" value="Update Vehicles">
                    <input method="post" name="save" type="hidden" value="0" />
                </form>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
