<?php
    include("_db.php");
    include("_dates.php");
?>
<html>
    <head>
        <title>
            Bank line for Franszo
        </title>
    </head>
    <body>
		<table border='1' cellpadding='2'>
<?php
	$date		= "2011-08-09";
	$date_to	= date("Y-m-d");

	$inc = 0;

	$accounts_ = q("SELECT account,name FROM Account WHERE company_id = '1'");
	if (!is_array($accounts_))  $accounts[][] = $accounts_;
	else                        $accounts = $accounts_;
	echo "<tr><td>Date</td><td>Total</td>";
	foreach($accounts as $ac)   //data for bank statement line
		echo "<td>$ac[1]</td>";
	echo "</tr>";

	while ($date <= $date_to)	{
		$balance = 0;
		$a = 0;
		foreach($accounts as $ac)  { //data for bank statement line
			$acc[$a++] = q("SELECT balance FROM Statement WHERE account = '$ac[0]' AND date <= '$date' ORDER BY date DESC, FITID DESC LIMIT 1");
			$balance += (double)q("SELECT balance FROM Statement WHERE account = '$ac[0]' AND date <= '$date' ORDER BY date DESC, FITID DESC LIMIT 1");
		}

		$date = getDates($date, 1);
		
		$date_display = getDates($date, -1);
		echo "<tr><td>$date_display</td><td align='right'>".number_format($balance, 2, ".", "")."</td>";
		foreach($acc as $a)	echo "<td align='right'>$a</td>";
		echo "</tr>";

	}


 
?>
	</table>
        <form action="index.php" method="post">
            <center><input type="submit" value="Return"/></center>
        </form>
    </body>
</html>
