<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

	if (!hasAccess("INVOICE_MANAGE"))
		header("Location: noaccess.php");

    //  Set Report Generated Status
    $generated                                                          = "0";

    $project_id                                                         = "null";
    $date_from                                                          = $today;
    $date_to                                                            = $today;

    if (isset($_POST["project"]) && is_numeric($_POST["project"]))
    {
        $project_id                                                     = $_POST["project"];

        $dates                                                          = q("SELECT id, end_date FROM Invoices WHERE project_id = '$project_id' AND company_id = '".$_SESSION["company_id"]."' ORDER BY end_date DESC");

        if ($dates)
            $date_from                                                  = getDates($dates[0][1], 1);
        else
            $date_from                                                  = q("SELECT reg_date FROM Project WHERE id = '$project_id'");

        if ($date_from > $date_to)
            $date_to                                                    = $date_from;
    }

    //  Get Expenses Info
    function getExpenses($project_id, $date_from, $date_to, $total_cost)
    {
        GLOBAL $exceldata;
        GLOBAL $row;

        $expense_based                                                  = "";

        //  $nExpenses                                                  =  Number of Expenses Booked
        $nExpenses                                                      = q("SELECT COUNT(id) FROM ExpenseSheet WHERE project_id = '$project_id' ".
                                                                            "AND date >= '$date_from' AND date <= '$date_to'");
        $expenses                                                       = q("SELECT id, descr, expense FROM ExpenseSheet WHERE project_id = '$project_id' ".
                                                                            "AND date >= '$date_from' AND date <= '$date_to' ORDER BY descr");

        if ($nExpenses > 1)
        {
            $expense_based                                              .= "<tr><td>Expenses</td>";

            $expense_based                                              .= "<td><table width='100%'>";

            foreach ($expenses as $expense)
                $expense_based                                          .= "<tr><td class='noborder'>".$expense[1]."</td></tr>";

            $expense_based                                              .= "</table></td><td><table width='100%'>";

            foreach ($expenses as $expense)
            {
                $total_cost                                             = $total_cost + $expense[2];

                $expense_based                                          .= "<tr><td class='noborder rightdata'>".number_format($expense[2], 2, ".", "")."</td></tr>";
            
                $exceldata[$row][]  = "Expenses";
                $exceldata[$row][]  = $expense[1];
                $exceldata[$row][]  = $expense[2];
                $row++;
            }

            $expense_based                                              .= "</table></td>";
        }
        else if ($nExpenses == 1)
        {
            $total_cost                                                 = $total_cost + $expenses[0][2];
            $expense_based                                              .= "<tr><td>Expenses</td>";
            $expense_based                                              .= "<td>
                                                                                            <table width='100%'>
                                                                                                <tr><td class='noborder'>".$expenses[0][1]."</td></tr>
                                                                                            </table>
                                                                                        </td>
                                                                                        <td>
                                                                                            <table width='100%'>
                                                                                                <tr>
                                                                                                    <td class='noborder rightdata'>".number_format($expenses[0][2], 2, ".", "")."</td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>";
            $exceldata[$row][]  = "Expenses";
            $exceldata[$row][]  = $expenses[0][1];
            $exceldata[$row][]  = $expenses[0][2];
            $row++;
        }

        $expense_based                                                  .= "<tr>
                                                                                            <td class='on-table-total'>&nbsp;</td>
                                                                                            <td class='on-table-total'>&nbsp;</td>
                                                                                            <td class='on-table-total'>
                                                                                                <table width='100%'>
                                                                                                    <tr>
                                                                                                        <td class='on-table-total' style='border:0px'>Total <i>(".$_SESSION["currency"].")</i>:</td>
                                                                                                        <td class='on-table-total' style='border:0px'>".number_format($total_cost, 2, ".", "")."</td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>";
        $exceldata[$row][]  = "";
        $exceldata[$row][]  = "Total (".$_SESSION["currency"]."): ";
        $exceldata[$row][]  = $total_cost;
        $row++;

        return "".$expense_based;
    }

    //  Create Display Information Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")
    {
        $errorMessage                                                   = "";

        $project_id                                                     = $_POST["project"];
        $date_from                                                      = addslashes(strip_tags($_POST["date_from"]));
        $date_to                                                        = addslashes(strip_tags($_POST["date_to"]));
        $displayString                                                  = "";

        //  Check If Invoice Exists In Database
        $exists                                                         = q("SELECT id FROM Invoices WHERE end_date >= '$date_from' AND project_id = '$project_id' AND company_id = '".$_SESSION["company_id"]."'");

        if (!$exists)
        {
            ///////////////////////////
            //  Get Info From Tables
            ///////////////////////////
            $project_name                                               = q("SELECT name From Project WHERE id = '$project_id'");

            $total_cost                                                 = 0;
            $row                                                           = 0;

            //  Create Heading
            $table_headers                                              = "<tr>
                                                                                            <th>Activity Type</th>
                                                                                            <th>Activity</th>
                                                                                            <th>Cost <i>(".$_SESSION["currency"].")</i></th>";
            $excelheadings[0][] = "";
            $excelheadings[0][] = "";
            $excelheadings[0][] = "";
            
            $exceldata[$row][] = "Activity Type";                                   // 1
            $exceldata[$row][] = "Activity";                                           // 2
            $exceldata[$row][] = "Cost (".$_SESSION["currency"].")";     // 3
            $row++;
            
            ///////////////////////////
            //  Create Display
            ///////////////////////////
            //  Time Booked Section
            //  $nActivityTypes                                         = Number of Activities
            $nActivityTypes                                             =q("SELECT COUNT(DISTINCT(at.id)) FROM (ActivityTypes AS at LEFT JOIN Activities AS a ".
                                                                            "ON at.id = a.parent_id LEFT JOIN TimeSheet AS ts ON ts.activity_id = a.id) ".
                                                                            "WHERE ts.project_id = '$project_id' AND ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                                                            "AND at.company_id = '".$_SESSION["company_id"]."'");
            $activityTypes                                              =q("SELECT DISTINCT(at.id), at.type FROM (ActivityTypes AS at LEFT JOIN Activities AS a ".
                                                                            "ON at.id = a.parent_id LEFT JOIN TimeSheet AS ts ON ts.activity_id = a.id) ".
                                                                            "WHERE ts.project_id = '$project_id' AND ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                                                            "AND at.company_id = '".$_SESSION["company_id"]."' ORDER BY at.type");

            if ($nActivityTypes > 1)
            {
                foreach ($activityTypes as $activityType)
                {
                    //  nActivities                                     = Number of Activities per Type
                    $nActivities                                        = q("SELECT COUNT(a.id) FROM (TimeSheet AS ts INNER JOIN Activities AS a ".
                                                                            "ON a.id = ts.activity_id) WHERE a.parent_id = '".$activityType[0]."' ".
                                                                            "AND ts.project_id = '$project_id' AND ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                                                            "GROUP BY a.id");
                    $activities                                         = q("SELECT a.id, a.name, SUM(ts.rate * ts.time_spent) FROM (TimeSheet AS ts INNER JOIN ".
                                                                            "Activities AS a ON a.id = ts.activity_id) WHERE a.parent_id = '".$activityType[0]."' ".
                                                                            "AND ts.project_id = '$project_id' AND ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                                                            "GROUP BY a.id ORDER BY a.name");

                    $time_based                                         .= "<tr><td>".$activityType[1]."</td>";

                    if ($nActivities > 1)
                    {
                        $time_based                                     .= "<td><table width='100%'>";

                        foreach ($activities as $activity)
                            $time_based                                 .= "<tr><td class='noborder'>".$activity[1]."</td></tr>";

                        $time_based                                     .= "</table></td><td><table width='100%'>";

                        foreach ($activities as $activity)
                        {
                            $total_cost                                 = $total_cost + $activity[2];

                            $time_based                                 .= "<tr><td class='noborder rightdata'>".number_format($activity[2], 2, ".", "")."</td></tr>";
                            $exceldata[$row][]                              = $activityType[1];
                            $exceldata[$row][]                              = $activity[1];
                            $exceldata[$row][]                              = $activity[2];
                            $row++;
                       }

                        $time_based                                     .= "</table></td>";
                    }
                    else if ($nActivities == 1)
                    {
                        $total_cost                                     = $total_cost + $activities[0][2];
                        $time_based                                     .= "<td>
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td class='noborder'>".$activities[0][1]."</td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td>
                                                                                        <table width='100%'>
                                                                                            <tr>
                                                                                                <td class='noborder rightdata'>".number_format($activities[0][2], 2, ".", "")."</td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>";
                        $exceldata[$row][]                              = $activityType[1];
                        $exceldata[$row][]                              = $activities[0][1];
                        $exceldata[$row][]                              = $activities[0][2];
                        $row++;
                    }

                    $time_based                                         .= "</tr>";
                }
            }
            else if ($nActivityTypes == 1)
            {
                //  nActivities                                         = Number of Activities per Type
                $nActivities                                            = q("SELECT COUNT(a.name) FROM (TimeSheet AS ts INNER JOIN Activities AS a ".
                                                                            "ON a.id = ts.activity_id) WHERE a.parent_id = '".$activityTypes[0][0]."' ".
                                                                            "AND ts.project_id = '$project_id' AND ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                                                            "GROUP BY a.id");
                $activities                                             = q("SELECT a.id, a.name, SUM(ts.rate * ts.time_spent) FROM (TimeSheet AS ts INNER JOIN ".
                                                                            "Activities AS a ON a.id = ts.activity_id) WHERE a.parent_id = '".$activityTypes[0][0]."' ".
                                                                            "AND ts.project_id = '$project_id' AND ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                                                            "GROUP BY a.id ORDER BY a.name");

                $time_based                                             .= "<tr><td>".$activityTypes[0][1]."</td>";

                if ($nActivities > 1)
                {
                    $time_based                                         .= "<td><table width='100%'>";

                    foreach ($activities as $activity)
                        $time_based                                     .= "<tr><td class='noborder'>".$activity[1]."</td></tr>";

                    $time_based                                         .= "</table></td><td><table width='100%'>";

                    foreach ($activities as $activity)
                    {
                        $total_cost                                     = $total_cost + $activity[2];

                        $time_based                                     .= "<tr><td class='noborder rightdata'>".number_format($activity[2], 2, ".", "")."</td></tr>";
                        
                        $exceldata[$row][]                              = $activityTypes[0][1];
                        $exceldata[$row][]                              = $activity[1];
                        $exceldata[$row][]                              = $activity[2];
                        $row++;
                    }

                    $time_based                                         .= "</table></td>";
                }
                else if ($nActivities == 1)
                {
                    $total_cost                                         = $total_cost + $activities[0][2];
                    $time_based                                         .= "<td>
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td class='noborder'>".$activities[0][1]."</td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td>
                                                                                        <table style='width:100%'>
                                                                                            <tr>
                                                                                                <td class='noborder rightdata'>".number_format($activities[0][2], 2, ".", "")."</td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>";
                                                                                    
                    $exceldata[$row][]                              = $activityTypes[0][1];
                    $exceldata[$row][]                              = $activities[0][1];
                    $exceldata[$row][]                              = $activities[0][2];
                    $row++;
                }
            }

            $expense_based                                              = getExpenses($project_id, $date_from, $date_to, $total_cost);
            $display                                                    = $table_headers.$time_based.$expense_based;

            if ($date_from == $date_to)
                $timestamp                                              = "Time Period: ".$date_from;
            else
                $timestamp                                              = "Time Period: ".$date_from." - ".$date_to;

            if ($display != "")
                $displayString                                          = "<tr><td class='on-table-clear' colspan='100%'><a>Project Report</a></td></tr>
                                                                                     <tr><td class='on-table-clear' colspan='100%'>Project: ".$project_name."</td></tr>
                                                                                     <tr><td class='on-table-clear' colspan='100%'>".$timestamp."</td></tr>".$display;
                $excelheadings[1][] = "Project Report";                                   
                $excelheadings[1][] = "";                                           
                $excelheadings[1][] = "";    

                $excelheadings[2][] = "Project: ".$project_name;                                   
                $excelheadings[2][] = "";                                           
                $excelheadings[2][] = "";
                
                $excelheadings[3][] = $timestamp;                                   
                $excelheadings[3][] = "";                                           
                $excelheadings[3][] = "";

            if ($displayString != "")
            {
                $generated                                              = "1";
                $insert                                                 = q("INSERT INTO Invoices (start_date, end_date, project_id) ".
                                                                            "VALUES ('$date_from', '$date_to', '$project_id')");
            }
        }
        else
            $errorMessage                                               = "Invoice Between Dates Already Exists...";
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("3", "reports");

    if ($errorMessage != "")
    {
        echo "<p align='center' style='padding:0px;'><strong><font color='#999999'>$errorMessage</font></strong></p>";
        echo "<br/>";
    }
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    jQuery(function(){
	jQuery('#date_from').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_from').val(),
		current: jQuery('#date_from').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_from').val()))
			_date = new Date();
			else _date = jQuery('#date_from').val();
			jQuery('#date_from').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_from').val(formated);
			jQuery('#date_from').DatePickerHide();
		}
	});
    })

    jQuery(function(){
	jQuery('#date_to').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_to').val(),
		current: jQuery('#date_to').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_to').val()))
			_date = new Date();
			else _date = jQuery('#date_to').val();
			jQuery('#date_to').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_to').val(formated);
			jQuery('#date_to').DatePickerHide();
		}
	});
    })

    function check()
    {
        var valid                                                       = 1;

        //  Check That Date From Is Entered
        if (document.forms["invoice_create"].date_from.value == "")
        {
            ShowLayer("dateFrom", "block");
            valid                                                       = 0;
        }
        //  Check That Entered Date From Is Valid
        else
        {
            ShowLayer("dateFrom", "none");

            if (!validation("date", document.forms["invoice_create"].date_from.value))
            {
                ShowLayer("dateFromDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("dateFromDiv", "none");
        }

        //  Check That Date To Is Entered
        if (document.forms["invoice_create"].date_to.value == "")
        {
            ShowLayer("dateTo", "block");
            valid                                                       = 0;
        }
        //  Check That Entered Date To Is Valid
        else
        {
            ShowLayer("dateTo", "none");

            if (!validation("date", document.forms["invoice_create"].date_to.value))
            {
                ShowLayer("dateToDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("dateToDiv", "none");
        }

        if (valid == 1)
        {
            document.forms["invoice_create"].save.value                 = 1;
            document.forms["invoice_create"].submit();
        }
    }
</script>
<?php
    //  nProjects                                                       = Number of Contacts
	$nProjects                                                          = q("SELECT COUNT(p.id) FROM Project as p INNER JOIN companiesOnTeam as cot ".
																			"ON p.id = cot.project_id WHERE p.completed = 0 ".
                                                                            "AND cot.company_id = '".$_SESSION["company_id"]."'");
	$projects                                                           = q("SELECT p.id, p.name FROM Project as p INNER JOIN companiesOnTeam as cot ".
	   																		"ON p.id = cot.project_id WHERE p.completed = 0 ".
                                                                            "AND cot.company_id = '".$_SESSION["company_id"]."' ORDER BY p.name");
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="invoice_create">
                    <div id="" style="<?php if ($generated == "1") echo "display: none;"; else echo "display: block"; ?>">
                        <table width="100%">
                            <tr>
                                <td class="centerdata">
                                    <h6>Invoice Info</h6>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Project:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" id="project" name="project" onChange="submit();" tabindex="1">
                                        <option value="null">--  Select A Project  --</option>
                                        <?php
                                            if ($nProjects > 1)
                                                foreach ($projects as $project)
                                                    if ($project_id == $project[0])
                                                        echo "<option value='".$project[0]."' selected>".$project[1]."</option>";
                                                    else
                                                        echo "<option value='".$project[0]."'>".$project[1]."</option>";
                                            else if ($nProjects == 1)
                                                if ($project_id == $projects[0][0])
                                                    echo "<option value='".$projects[0][0]."' selected>".$projects[0][1]."".
                                                        "</option>";
                                                else
                                                    echo "<option value='".$projects[0][0]."'>".$projects[0][1]."</option>";
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Date From:
                                </td>
                                <td width="50%">
                                    <input class="on-field-date" id="date_from" name="date_from" tabindex="2" type="text" style="text-align:right;" value="<?php echo "".$date_from; ?>">
                                    <div id="dateFrom" style="display: none;"><font color="#FF5900">* Date must be entered</font></div>
                                    <div id="dateFromDiv" style="display: none;"><font color="#FF5900">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Date To:
                                </td>
                                <td width="50%">
                                    <input class="on-field-date" id="date_to" name="date_to" tabindex="3" type="text" style="text-align:right;" value="<?php echo "".$date_to; ?>">
                                    <div id="dateTo" style="display: none;"><font color="#FF5900">* Date must be entered</font></div>
                                    <div id="dateToDiv" style="display: none;"><font color="#FF5900">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <input name="btnGenerate" onClick="check();" tabindex="4" type="button" value="Generate Report">
                        <input method="post" name="save" type="hidden" value="0" />
                        <?php
                            if ($project_id != null && is_numeric($project_id))
                            {
                                //  nInvoices                           = Number of Invoices
								$nInvoices                              = q("SELECT COUNT(id) FROM Invoices WHERE project_id = '$project_id' ".
																			"AND company_id = '".$_SESSION["company_id"]."'");
                                $invoices                               = q("SELECT id, start_date, end_date FROM Invoices WHERE project_id = '$project_id' ".
																			"AND company_id = '".$_SESSION["company_id"]."' ORDER BY start_date, end_date");

                                $header                                 = "<tr>
                                                                                    <th>Date From</th>
                                                                                    <th>Date To</th>
                                                                                    <th>Show</th>
                                                                                </tr>";
                                $display                                = "";

                                if ($nInvoices > 1)
                                {
                                    foreach ($invoices as $invoice)
                                    {
                                        $display                        .= "<tr>";
                                        $display                        .= "<td>".$invoice[1]."</td>";
                                        $display                        .= "<td>".$invoice[2]."</td>";
                                        $display                        .= "<td><input name='btnShow' ".
                                                                                "onClick=\"location.href='invoice_display.php'+'?id='+".$invoice[0]."\" type='button' ".
                                                                                "value='Show'></td>";
                                        $display                        .= "</tr>";
                                    }
                                }
                                else if ($nInvoices == 1)
                                {
                                    $display                            .= "<tr>";
                                    $display                            .= "<td>".$invoices[0][1]."</td>";
                                    $display                            .= "<td>".$invoices[0][2]."</td>";
                                    $display                            .= "<td><input name='btnShow' ".
                                                                            "onClick=\"location.href='invoice_display.php'+'?id='+".$invoices[0][0]."\" type='button' ".
                                                                            "value='Show'></td>";
                                    $display                            .= "</tr>";
                                }

                                if ($display != "")
                                {
                                    echo "<br/><br/>";
                                    echo "<table class='on-table-center on-table'>";
                                        echo "".$header.$display;
                                    echo "</table>";
                                }
                            }
                        ?>
                    </div>
                </form>
                <div id="report" style="<?php if ($generated == "1") echo "display: block;"; else echo "display: none"; ?>">
                    <?php
                        echo "<table class='on-table-center on-table'>";
                            echo "".$displayString;
                            echo  "<tfoot>
                                            <tr>
                                                <td colspan='100%'></td>
                                            </tr>
                                        </tfoot>";
                        echo "</table>";
                        echo "<br/>";

                        ///////////////////////////
                        if($exceldata && $excelheadings){
                            //  Set Export Information
                            $_SESSION["fileName"] = "Project Invoice";
                            $_SESSION["fileData"] = array_merge($excelheadings, $exceldata);
                        }
                        ///////////////////////////

                        echo "<input name='btnExport' onClick=\"location.href='_export.php'\" type='button' value='Export to Excel' />";
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
