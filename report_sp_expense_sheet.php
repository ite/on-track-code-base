<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("include/sajax.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");
        
    if (!hasAccess("REP_SP_EX_SHEET"))
        header("Location: noaccess.php");
        
    // Class for table background color
    $colorClass = "OnTrack_#_";

   ///////////////////////////
    //  Sajax
    function getCompanies($id)  {
       return q("SELECT DISTINCT(c.id), c.name FROM (Company AS c INNER JOIN companiesOnTeam AS cot ON c.id = cot.company_id 
                        INNER JOIN Project AS p ON p.id =  cot.project_id) WHERE cot.project_id = '$id' ORDER BY c.name"); 
    }
    function getEmployees($id)  {
        return q("SELECT id, CONCAT(lstname, ', ', frstname) FROM Employee WHERE company_id = '$id'");
    }
    function getAreas($id)      {
        return q("SELECT ar.id, ar.name FROM (areas AS ar INNER JOIN Project as p ON ar.pID = p.id) WHERE ar.active = '1' AND p.p_type='SP' AND ar.pID='".$id."' ORDER BY ar.name");
    } 
    function getActivityTypes($id)      {
        return q("SELECT id, type FROM ActivityTypes WHERE pID = '$id' ORDER BY type");
    }
    function getActivities($id) {
        return q("SELECT id, name FROM Activities WHERE parent_id = '$id' ORDER BY name");
    }

    $sajax_request_type = "GET";
    sajax_init();    
    sajax_export("getCompanies");    
    sajax_export("getEmployees");    
    sajax_export("getAreas");
    sajax_export("getActivityTypes");
    sajax_export("getActivities");
    sajax_handle_client_request();
    ///////////////////////////

    //  Set Report Generated Status
    $generated = "0";
    
    //  Create Display Information Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")        {
        unset($exceldata);

        $errorMessage = "";

        $reportOn = $_POST["reportOn"];

        $employee_id = $_POST["employee"];
        $project_id = $_POST["project"];
        $company_id = $_POST["company"];
        $area_id = $_POST["area"];
        $activity_type_id = $_POST["activityType"];
        $activity_id = $_POST["activity"];
        $expenseType = $_POST["expenseType"];
        $date_type = $_POST["dateType"];

        //  Get Dates
        if ($date_type == "currentwk")  {
            $date_from = currentWeekStart();
            $date_to = $today;
        }
        else if ($date_type == "previouswk")    {
            $date_from = getDates(currentWeekStart(), -7);
            $date_to = getDates(currentWeekStart(), -1);
        }
        else if ($date_type == "currentmnth")   {
            $date_from = getMonthStart($today);
            $date_to = $today;
        }
        else if ($date_type == "previousmnth")  {
            $date_from = getPreviousMonthStart();
            $date_to = getMonthEnd(getPreviousMonthStart());
        }
        else if ($date_type == "projectLifetime")      {
            $date_fromT = q("SELECT MIN(date) FROM TimeSheet WHERE project_id = '".$project_id."'");
            $date_fromE = q("SELECT MIN(date) FROM ExpenseSheet WHERE project_id = '".$project_id."'");
            $date_from = ($date_fromT <= $date_fromE) ? $date_fromT : $date_fromE;
            $date_to = $today;
        }
        else if ($date_type == "custom")        {
            $date_from = addslashes(strip_tags($_POST["date_from"]));
            $date_to = addslashes(strip_tags($_POST["date_to"]));
        }

        //  Timestamp
        $timestamp = ($date_from === $date_to) ? "Time Period: ".$date_from : "Time Period: ".$date_from." - ".$date_to;

        $hasAreas = 0;

        $where = "";

        $table = ($reportOn == "approved") ? "ApprovedExpense" : "ExpenseSheet";
        $where .= ($reportOn == "approved") ? "AND es.status2 = '2' " : "";

        if (($employee_id != "null" && is_numeric($employee_id)))       {
            $empInfo = q("SELECT lstname, frstname FROM Employee WHERE id = '$employee_id'");
            $employeeInfo = $empInfo[0][0].", ".$empInfo[0][1];
            $where .= "AND es.user_id = '$employee_id' ";
        } else  $employeeInfo = "All Employees";

        $projectInfo = q("SELECT name FROM Project WHERE id = '$project_id'");
        $where .= "AND cot.project_id = '$project_id' ";

        if (($area_id != "null" && is_numeric($area_id)))       {
            $areaInfo = q("SELECT name FROM areas WHERE id = '$area_id'");
            $where .= "AND es.area_id = '$area_id' ";
        } else  $areaInfo = "All Areas";

        if (($company_id != "null" && is_numeric($company_id)))       {
            $companyInfo = q("SELECT name FROM Company WHERE id = '$company_id'");
            $where .= "AND cot.company_id = '$company_id' ";
        } else  $companyInfo = "All Companies";

        if (($expenseType != "null" && is_numeric($expenseType)))       {
            $expenseInfo = q("SELECT name FROM dropdowns WHERE id = '$expenseType'");
            $where .= "AND es.expense_type_id = '$expenseType' ";
        } else  $expenseInfo = "All Expense Types";

        //  Create Headings
        $reportHead = array("<a>User: ".$employeeInfo."</a>","<a>Shared Project: ".$projectInfo."</a>","<a>Company: ".$companyInfo."</a>","<a>Area: ".$areaInfo."</a>","<a>Expense Type: ".$expenseInfo."</a>",$timestamp);

        $areas  = q("SELECT es.project_id,es.area_id 
                        FROM ($table AS es
                            INNER JOIN Employee AS e ON es.user_id = e.id 
                            INNER JOIN dropdowns AS dd ON es.expense_type_id = dd.id 
                            LEFT JOIN Activities AS a ON es.activity_id = a.id 
                            LEFT JOIN ActivityTypes AS at ON a.parent_id = at.id 
                            INNER JOIN Project AS p ON es.project_id = p.id 
                            INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id 
                            INNER JOIN Company AS c ON cot.company_id = c.id 
                            LEFT JOIN areas AS ar ON es.area_id = ar.id
                            LEFT JOIN Vehicle AS v ON es.vehicle_id = v.id
                            LEFT JOIN Disbursements AS d ON es.disbursement_id = d.id
                            LEFT JOIN DisbursementTypes AS dt ON dt.id = d.parent_id)
                        WHERE es.date >= '".$date_from."' AND es.date <= '".$date_to."' AND es.projectType = 'SP' AND es.area_id > 0 AND es.company_id = c.id $where");

        if (is_array($areas))    $hasAreas = 1;

        $select = "";

        if ($hasAreas)  {
            $select .= "ar.name,";
            $expenseHeadings = array("Company Name","Employee Name","Expense Type","Expense Info","Area","Activity Type","Activity","Description","Date","Rate","Quantity","Cost <i>(".$_SESSION["currency"].")</i>");
        } else  $expenseHeadings = array("Company Name","Employee Name","Expense Type","Expense Info","Activity Type","Activity","Description","Date","Rate","Quantity","Cost <i>(".$_SESSION["currency"].")</i>");

        //  Get Information - Expense
        $expenseInfo = q("SELECT c.name,CONCAT(e.lstname, ', ', e.frstname),dd.name,IF(dd.name = 'Driving',v.type,IF(dd.name = 'Printing',CONCAT(dt.type,': ',d.name),es.otherDescr)),".$select."at.type,a.name,es.descr,es.date,
                                es.rate, IF(dd.name = 'Driving',es.kilometers,es.total), es.expense 
                        FROM (ExpenseSheet AS es
                            INNER JOIN Employee AS e ON es.user_id = e.id 
                            INNER JOIN dropdowns AS dd ON es.expense_type_id = dd.id 
                            LEFT JOIN Activities AS a ON es.activity_id = a.id 
                            LEFT JOIN ActivityTypes AS at ON a.parent_id = at.id 
                            INNER JOIN Project AS p ON es.project_id = p.id 
                            INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id 
                            INNER JOIN Company AS c ON cot.company_id = c.id 
                            LEFT JOIN areas AS ar ON es.area_id = ar.id
                            LEFT JOIN Vehicle AS v ON es.vehicle_id = v.id
                            LEFT JOIN Disbursements AS d ON es.disbursement_id = d.id
                            LEFT JOIN DisbursementTypes AS dt ON dt.id = d.parent_id)
                            WHERE es.date >= '".$date_from."' AND es.date <= '".$date_to."' AND es.projectType = 'SP' AND es.company_id = c.id $where 
                            ORDER BY c.name, es.date, es.on_date, es.on_time, ar.name");

        if (is_array($expenseInfo))     {
            $columns = count($expenseHeadings) - 1;

            $displayString = "";
            $row = 0;

            //  HTML Display
            foreach ($reportHead as $rh)
                $displayString .= "<tr><td class='on-table-clear' colspan='".($columns + 1)."'>".$rh."</td></tr>";

            //  EXCEL DISPLAY
            foreach ($reportHead as $rh)        {
                $exceldata[$row][] = strip_tags($rh);

                for ($a = 0; $a < $columns; $a++)       $exceldata[$row][] = "";

                $row++;
            }

            $total_cost = 0;

            //  Headings
            //  HTML Display
            $displayString .= "<tr>";

            foreach ($expenseHeadings as $h)    $displayString .= "<th>".$h."</th>";

            $displayString .= "</tr>";

            //  EXCEL DISPLAY
            foreach ($expenseHeadings as $h)    $exceldata[$row][] = $colorClass.strip_tags($h);

            $row++;

            //  Data
            foreach ($expenseInfo as $r)        {
                $col = 0;
                $displayString .= "<tr>";

                foreach ($r as $d)      {
                    if ($expenseHeadings[$col] == "Cost <i>(".$_SESSION["currency"].")</i>")   $total_cost += $d;

                    if (preg_match("/^((\d{2}(([02468][048])|([13579][26]))[\-\/\s]?((((0?[13578])|(1[02]))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-\/\s]?((0?[1-9])|([1-2][0-9])))))|(\d{2}(([02468][1235679])|([13579][01345789]))[\-\/\s]?((((0?[13578])|(1[02]))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-\/\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))(\s(((0?[1-9])|(1[0-2]))\:([0-5][0-9])((\s)|(\:([0-5][0-9])\s))([AM|PM|am|pm]{2,2})))?$/", $d))
                        $displayString .= "<td style='white-space:nowrap'>".$d."</td>";
                    else if (is_numeric($d))
                        $displayString .= "<td style='white-space:nowrap' class='rightdata'>".number_format($d, 2, ".", "")."</td>";
                    else if ($d == "")
                        $displayString .= "<td style='white-space:nowrap'>-</td>";
                    else
                        $displayString .= "<td style='white-space:nowrap'>".$d."</td>";

                    $exceldata[$row][] .= $d;

                    $col++;
                }

                $displayString .= "</tr>";
                $row++;
            }

            $colspan = ($hasAreas) ? 11 : 10;

            $displayString .= "<tr>
                                    <td class='on-table-total' colspan='".$colspan."><a style='color:#FFFFFF;'>Total <i>(".$_SESSION["currency"].")</a></td>
                                    <td class='on-table-total'>".number_format($total_cost, 2, ".", "")."</td>
                                </tr>";

            for ($a = 0; $a < $colspan - 1; $a++)       $exceldata[$row][] = "";

            $exceldata[$row][] .= "Total (".$_SESSION["currency"]."):";
            $exceldata[$row][] .= $total_cost;

            $row++;
        }

        if ($displayString != "")
            $generated                                                  = "1";

    }
   
    //  Print Header
    print_header();
    //  Print Menu
    print_menus("3", "reports");
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    jQuery(function(){
	jQuery('#date_from').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_from').val(),
		current: jQuery('#date_from').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_from').val()))
			_date = new Date();
			else _date = jQuery('#date_from').val();
			jQuery('#date_from').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_from').val(formated);
			jQuery('#date_from').DatePickerHide();
		}
	});

	jQuery('#date_to').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_to').val(),
		current: jQuery('#date_to').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_to').val()))
			_date = new Date();
			else _date = jQuery('#date_to').val();
			jQuery('#date_to').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_to').val(formated);
			jQuery('#date_to').DatePickerHide();
		}
	});

        jQuery("#project").change(function()    {
            if (jQuery(this).val() != "null")   {
                jQuery("#dateType option[value='projectLifetime']").removeAttr("disabled");
            } else      {
                jQuery("#dateType option[value='projectLifetime']").attr("disabled","disabled");
                jQuery("#dateType option[value='currentwk']").attr("selected", "selected");
            }
        });
    });

    //  Sajax
    <?php sajax_show_javascript(); ?>

    function check()
    {
        var valid                                                       = 1;
        
        // Validate Project selection
        if (document.forms["report"].project.value == "null")
        {
            ShowLayer("projectErrorDiv", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("projectErrorDiv", "none");

        if (document.forms["report"].dateType.value == "custom") {
            //  Check That Date From Is Entered
            if (document.forms["report"].date_from.value == "") {
                ShowLayer("dateFrom", "block");
                valid                                                   = 0;
            }
            //  Check That Entered Date From Is Valid
            else {
                ShowLayer("dateFrom", "none");

                if (!validation("date", document.forms["report"].date_from.value)) {
                    ShowLayer("dateFromDiv", "block");
                    valid                                               = 0;
                }
                else
                    ShowLayer("dateFromDiv", "none");
            }

            //  Check That Date To Is Entered
            if (document.forms["report"].date_to.value == "") {
                ShowLayer("dateTo", "block");
                valid                                                   = 0;
            }
            //  Check That Entered Date To Is Valid
            else {
                ShowLayer("dateTo", "none");

                if (!validation("date", document.forms["report"].date_to.value)) {
                    ShowLayer("dateToDiv", "block");
                    valid                                               = 0;
                }
                else
                    ShowLayer("dateToDiv", "none");
            }
        }

        if (valid == 1) {
            document.forms["report"].save.value                         = 1;
            document.forms["report"].submit();
        }
    }
    
    var hasAreas = 1;

    function dateSelection() {
        if (document.forms["report"].dateType.value == "custom")
            ShowLayer("datesDiv", "block");
        else
            ShowLayer("datesDiv", "none");
    }

    ///////////////////////////
    //  Sajax
    function ClearOptions(OptionList) {
        for (x = OptionList.length; x >= 0; x--)
        {
            OptionList[x]                                               = null;
        }
    }
    
    // Set Company
    function setCompanies(data) {
        var c                                                           = 0;
        if(!data)
            ShowLayer("emloyeeDiv", "none");
        
        document.forms["report"].company.options[(c++)]                         = new Option("All Companies", "null");
        
        for (var i in data)
          eval("document.forms['report'].company.options["+(c++)+"] = new Option('" + data[i][1] + "', '" + data[i][0] + "');");
    }

    // Set Employees
    function setEmployees(data) {
        var c                                                           = 0;
            
        document.forms["report"].employee.options[(c++)]                         = new Option("All Employees", "null");
        
        for (var i in data)
          eval("document.forms['report'].employee.options["+(c++)+"] = new Option('" + data[i][1] + "', '" + data[i][0] + "');");
    }
    
    //Set Areas
    function setAreas(data) {
        var c                                                           = 0;
        if(!data){
            ShowLayer("areaDiv", "none");
            hasAreas = 0;
        }else{
            ShowLayer("areaDiv", "block");
            hasAreas = 1;
        }
        
        if(hasAreas)
        document.forms["report"].area.options[(c++)]                         = new Option("All Areas", "null");

        for (var i in data)
            eval("document.forms['report'].area.options["+(c++)+"] = new Option('" + data[i][1] + "', '" + data[i][0] + "');");
    }
    
    // Set Activity Types
    function setActivityTypes(data) {
        var c                                                           = 0;
        if(!data)
            ShowLayer("activityTypeDiv", "none");

        document.forms["report"].activityType.options[(c++)]                         = new Option("All Activity Types", "null");

        for (var i in data)
            eval("document.forms['report'].activityType.options["+(c++)+"] = new Option('" + data[i][1] + "', '" + data[i][0] + "');");
    }
    
    // Set Activities
    function setActivities(data) {
        var c                                                           = 0;
        if(!data)
            ShowLayer("activityDiv", "none");

        document.forms["report"].activity.options[(c++)]                         = new Option("All Activities", "null");

        for (var i in data)
            eval("document.forms['report'].activity.options["+(c++)+"] = new Option('" + data[i][1] + "', '" + data[i][0] + "');");
    }
    
    // Get Companies
    function getCompanies(select) {
        ClearOptions(document.report.company);
        if (select.value != "null")
        {
            ShowLayer("companyDiv", "block");
        }else{
            ShowLayer("companyDiv", "none");
        }
        x_getCompanies(select.value, setCompanies);
        
        if (select.value != "null")
            ShowLayer("dateShowDiv", "block");
        
    }
    
    // Get Employees
    function getEmployees(select) {
        ClearOptions(document.report.employee);
        if (select.value != "null")
        {
            ShowLayer("employeeDiv", "block");
        }else{
            ShowLayer("employeeDiv", "none");
        }
            x_getEmployees(select.value, setEmployees);
    }
    
    // Get Areas
    function getAreas(select) {
        ClearOptions(document.report.area);
            ShowLayer("areaDiv", "block");
            x_getAreas(select.value, setAreas);
    }
    
    // Get Activity Types
    function getActivityTypes(select) {
        ClearOptions(document.report.activityType);
            ShowLayer("activityTypeDiv", "block");
            x_getActivityTypes(select.value, setActivityTypes);
    }
    
     // Get Activities
    function getActivities(select) {
        ClearOptions(document.report.activity);
            ShowLayer("activityDiv", "block");
            x_getActivities(select.value, setActivities);
    }

    function getData(select) {
        ClearOptions(document.report.activity);

        if (select.value != "null")
        {
            ShowLayer("activityDiv", "block");
            x_getActivities(select.value, setActivities);
        }
        else{
            ShowLayer("activityDiv", "none");
        }
    }
    ///////////////////////////
</script>
<?php
    $expenseTypes = q("SELECT id, name FROM dropdowns WHERE categoryID = (SELECT id FROM categories WHERE code = 'expenseType') ORDER BY ranking ASC");

	$projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p 
                            INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) 
                            WHERE cot.company_id = '".$_SESSION["company_id"]."' 
                            AND p.completed = '0' 
                            AND p.deleted = '0' 
                            AND p.p_type='SP' ORDER BY UPPER(p.name)");
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata">
            
                <form action="" method="post" name="report">
                    <div id="" style="<?php if ($generated == "1") echo "display: none;"; else echo "display: block"; ?>">
                        <table width="100%">
                            <tr>
                                <td class="centerdata">
                                    <h6>Shared Project Expense Sheet Report</h6>
                                </td>
                            </tr>
                        </table>
                        <br>   
                        <div id="projectDiv" style="display: block;">
                            <table width="100%">                            
                                <tr>
                                    <td class="on-description" width="50%">
                                        Report On:
                                    </td>
                                    <td width="50%">
                                        <select class="on-field" method="post" name="reportOn">
                                            <option value="normal">Actual Time</option>
                                            <option value="approved">Approved Time</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr><td colspan="100%"><br/></td></tr>
                                <tr>
                                    <td class="on-description" width="50%">
                                        Select Shared Project:
                                    </td>
                                    <td width="50%">
                                        <select class="on-field" method="post" id="project" name="project" onChange="getCompanies(project); getAreas(project); getActivityTypes(project)">
                                            <option value="null">--  Select a Shared Project  --</option>
                                            <?php
                                                if (is_array($projects))
                                                    foreach ($projects as $project)
                                                        echo "<option value='".$project[0]."'>".$project[1]."</option>";
                                            ?>
                                        </select>
                                        <div id="projectErrorDiv" style="display: none;"><font class="on-validate-error">* Shared Project must be selected</font></div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="companyDiv" style="display: none;"> 
                            <table width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Select Company:
                                    </td>
                                    <td width="50%">
                                        <select class="on-field" method="post" name="company" onChange="getEmployees(company)">
                                            <option value="null">--  Select a Company  --</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="areaDiv" style="display: none;">
                            <table width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Select Area:
                                    </td>
                                    <td width="50%">
                                        <select class="on-field" method="post" name="area">
                                            <option value="null">--  Select a Area  --</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="employeeDiv" style="display: none;"> 
                            <table width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Select Employee:
                                    </td>
                                    <td width="50%">
                                        <select class="on-field" method="post" id="employee" name="employee">
                                            <option value="null">--  Select Employee  --</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="activityTypeDiv" style="display: none;"> 
                            <table width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Activity Type:
                                    </td>
                                    <td width="50%">
                                        <select class="on-field" method="post" name="activityType" onChange="getActivities(activityType)">
                                            <option value="null">All Activity Types</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="activityDiv" style="display: none;">
                            <table width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Activity:
                                    </td>
                                    <td width="50%">
                                        <select class="on-field" method="post" name="activity">
                                            <option value="null">All Activities</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="dateShowDiv" style="display: none">
                            <table width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Date Type(s):
                                    </td>
                                    <td width="50%">
                                        <select class="on-field" method="post" id="dateType" name="dateType" onChange="dateSelection();">
                                            <option value="currentwk">Current Week</option>
                                            <option value="previouswk">Previous Week</option>
                                            <option value="currentmnth">Current Month</option>
                                            <option value="previousmnth">Previous Month</option>
                                            <option value="projectLifetime" disabled>Project Lifetime</option>
                                            <option value="custom">Custom Dates</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="datesDiv" style="display: none;">       
                            <table width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Date From:
                                    </td>
                                    <td width="50%">
                                        <input class="on-field-date" id="date_from" name="date_from" type="text" style="text-align:right;" value="<?php
                                            if ($_POST["date_from"] != "") echo "".$_POST["date_from"]; else echo "".getMonthStart($today); ?>">
                                        <div id="dateFrom" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                        <div id="dateFromDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="on-description" width="50%">
                                        Date To:
                                    </td>
                                    <td width="50%">
                                        <input class="on-field-date" id="date_to" name="date_to" type="text" style="text-align:right;" value="<?php
                                            if ($_POST["date_to"] != "") echo "".$_POST["date_to"]; else echo "".getMonthEnd($today); ?>">
                                        <div id="dateTo" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                        <div id="dateToDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <br/>
                        <input name="btnGenerate" onClick="check();" type="button" value="Generate Report">
                        <input method="post" name="save" type="hidden" value="0" />
                    </div>
                </form>
                
                <div id="report" style="<?php if ($generated == "1") echo "display: block;"; else echo "display: none"; ?>">
                    <?php
                        echo "<div class='on-20px'>
                                    <table class='on-table on-table-center'>".$displayString."
                                        <tfoot><tr><td colspan='100%'></td></tr></tfoot>
                                    </table>
                                </div>";
                        ///////////////////////////
                        //  Set Export Information
                        $_SESSION["fileName"] = "Report SP Expense Sheet";
                        $_SESSION["fileData"] = $exceldata;
                        ///////////////////////////

                        echo "<input name='btnExport' onClick=\"location.href='_export.php'\" type='button' value='Export to Excel' />";
                    ?>
                </div>
                
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
