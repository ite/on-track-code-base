<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    if (!hasAccess("BUDGET_CAT_MANAGE"))
            header("Location: noaccess.php");

    $display_link                                                       = "categories.php?alphabet";

    //  Set Alphabet Letter
    $alphabet                                                           = "";
    $alphabet                                                           = $_GET["alphabet"];

    //  Set Text Color
    $active                                                             = "color:#000000;font-weight:normal;";
    $non_active                                                         = "color:#A0A0A0;font-weight:normal;";

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("2", "budget");
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="category_management">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Category Type List</h6>
                            </td>
                        </tr>
                    </table>
                    <?php
                        echo "<br/>";
                        echo "| <a href='$display_link='>View All Caterories</a> |";
                        echo "<br/><br/>";
                        echo "<a href='$display_link=A'>A</a> | <a href='$display_link=B'>B</a> | ".
                            "<a href='$display_link=C'>C</a> | <a href='$display_link=D'>D</a> | ".
                            "<a href='$display_link=E'>E</a> | <a href='$display_link=F'>F</a> | ".
                            "<a href='$display_link=G'>G</a> | <a href='$display_link=H'>H</a> | ".
                            "<a href='$display_link=I'>I</a> | <a href='$display_link=J'>J</a> | ".
                            "<a href='$display_link=K'>K</a> | <a href='$display_link=L'>L</a> | ".
                            "<a href='$display_link=M'>M</a> | <a href='$display_link=N'>N</a> | ".
                            "<a href='$display_link=O'>O</a> | <a href='$display_link=P'>P</a> | ".
                            "<a href='$display_link=Q'>Q</a> | <a href='$display_link=R'>R</a> | ".
                            "<a href='$display_link=S'>S</a> | <a href='$display_link=T'>T</a> | ".
                            "<a href='$display_link=U'>U</a> | <a href='$display_link=V'>V</a> | ".
                            "<a href='$display_link=W'>W</a> | <a href='$display_link=X'>X</a> | ".
                            "<a href='$display_link=Y'>Y</a> | <a href='$display_link=Z'>Z</a>";
                    ?>
                    <br/><br/>
                    <input name="btnAdd" onClick="location.href='category_add.php';" tabindex="1" type="button" value="Add New Category">
                    <br/><br/>
                    <!--  Headings   -->
                    <table class="on-table-center on-table">
                        <tr>
                            <th>Category</th>
                            <th>Element</th>
                        </tr>
                        <!--  Table Information   -->
                        <?php
                            //  nCategories                             = Number of Categories
                            $nCategories                                = q("SELECT COUNT(id) FROM Category WHERE company_id = '".$_SESSION["company_id"]."' ".
                                                                                        "AND name LIKE '$alphabet%'");
                            $categories                                 = q("SELECT id, name FROM Category WHERE company_id = '".$_SESSION["company_id"]."' ".
                                                                                        "AND name LIKE '$alphabet%' ORDER BY name");

                            if ($nCategories > 1)
                            {
                                foreach ($categories as $category)
                                {
                                    //  nElements                       = Number of Elements per Category
                                    $nElements                          = q("SELECT COUNT(id) FROM Elements WHERE parent_id = '".$category[0]."'");
                                    $elements                           = q("SELECT id, name, status FROM Elements WHERE parent_id = '".$category[0]."' ORDER BY name");

                                    echo "<tr>";
                                        echo "<td >";
                                            echo "<a href='category_edit.php?id=".$category[0]."'>".$category[1]."</a>";
                                        echo "</td>";
                                        if ($nElements > 1)
                                        {
                                            echo "<td>";
                                                echo "<table width='100%'>";
                                                    foreach ($elements as $element)
                                                    {
                                                        echo "<tr>";
                                                            echo "<td class='noborder'>";
                                                                if ($element[2] == "0")
                                                                    echo "<a href='keyword_edit.php?id=".$element[0]."' style='".$non_active."'>".$element[1]."</a>";
                                                                else
                                                                    echo "<a href='keyword_edit.php?id=".$element[0]."'>".$element[1]."</a>";
                                                            echo "</td>";
                                                        echo "</tr>";
                                                    }
                                                echo "</table>";
                                            echo "</td>";
                                        }
                                        else if ($nElements == 1)
                                        {
                                            echo "<td>";
                                                echo "<table width='100%'>";
                                                     echo "<tr>";
                                                            echo "<td class='noborder'>";
                                                                if ($elements[0][2] == "0")
                                                                    echo "<a href='keyword_edit.php?id=".$elements[0][0]."' style='".$non_active."'>".$elements[0][1]."</a>";
                                                                else
                                                                    echo "<a href='keyword_edit.php?id=".$elements[0][0]."'>".$elements[0][1]."</a>";
                                                            echo "</td>";
                                                    echo "</tr>";
                                                 echo "</table>";
                                            echo "</td>";
                                        }
                                        else
                                        {
                                            echo "<td>";
                                                echo "-";
                                            echo "</td>";
                                        }
                                    echo "</tr>";
                                }
                            }
                            else if ($nCategories == 1)
                            {
                                //  nElements                           = Number of Elements per Category
                                $nElements                              = q("SELECT COUNT(id) FROM Elements WHERE parent_id = '".$categories[0][0]."'");
                                $elements                               = q("SELECT id, name, status FROM Elements WHERE parent_id = '".$categories[0][0]."' ".
                                                                            "ORDER BY name");

                                echo "<tr>";
                                    echo "<td>";
                                        echo "<a href='category_edit.php?id=".$categories[0][0]."'>".$categories[0][1]."</a>";
                                    echo "</td>";
                                    if ($nElements > 1)
                                    {
                                        echo "<td>";
                                            echo "<table width='100%'>";
                                                foreach ($elements as $element)
                                                {
                                                    echo "<tr>";
                                                        echo "<td>";
                                                            if ($element[2] == "0")
                                                                echo "<a href='keyword_edit.php?id=".$element[0]."' style='".$non_active."'>".$element[1]."</a>";
                                                            else
                                                                echo "<a href='keyword_edit.php?id=".$element[0]."'>".$element[1]."</a>";
                                                        echo "</td>";
                                                    echo "</tr>";
                                                }
                                            echo "</table>";
                                        echo "</td>";
                                    }
                                    else if ($nElements == 1)
                                    {
                                        echo "<td>";
                                            if ($elements[0][2] == "0")
                                                echo "<a href='keyword_edit.php?id=".$elements[0][0]."' style='".$non_active."'>".$elements[0][1]."</a>";
                                            else
                                                echo "<a href='keyword_edit.php?id=".$elements[0][0]."'>".$elements[0][1]."</a>";
                                        echo "</td>";
                                    }
                                    else
                                    {
                                        echo "<td>";
                                            echo "-";
                                        echo "</td>";
                                    }
                                echo "</tr>";
                            }
                            else
                            {
                                echo "<tr>";
                                    echo "<td align='center' colspan='2'>";
                                        if ($alphabet == "")
                                            echo "No categories available";
                                        else
                                            echo "No categories available under $alphabet";
                                    echo "</td>";
                                echo "</tr>";
                            }
                        ?>
                    </table>
                    <br/>
                    <input name="btnAdd" onClick="location.href='category_add.php';" tabindex="1" type="button" value="Add New Category">
                </form>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
