<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("include/sajax.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

   if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    if (!hasAccess("STATEMENT_MAN"))
        header("Location: noaccess.php");
        
	function getLinkedItems($s,$amount)	{
		$trans = q("SELECT sl.element_id, e.name FROM StatementLink AS sl INNER JOIN ElementTypes AS e ON sl.elementType = e.id WHERE sl.statement_id = '$s' ".
					"AND sl.company_id = '".$_SESSION['company_id']."'");
		if (is_array($trans))
		foreach($trans as $tt)	{
			if ($tt[1] == "INVOICE")	{
				$value = "((consulting+expenses)+(consulting+expenses)*vat/100)";
				$l[] = q("SELECT due_date, name, ROUND($value,2), ROUND(ABS($amount - $value),2) AS A, 'INVOICE', 'LINKED', L.id FROM LoadInvoices AS L WHERE id = '".$tt[0]."'");
			}
			else if ($tt[1] == "EXPENSE")	{
				$value = "((amount)+(amount)*vat/100)";
				$l[] = q("SELECT due_date, descr, -(ROUND($value,2)), ROUND(ABS(-$amount - $value),2) AS A, 'EXPENSE', 'LINKED', E.id FROM ExpenseBudget AS E WHERE id = '".$tt[0]."'");
			}
			else if ($tt[1] == "BUDGET")	{
				$value = "(planned)";
				$l[] = q("SELECT B.due_date, CONCAT(DATE_FORMAT(CONCAT(B.year, '-', B.month, '-01'),'%b'),' - ',C.name,' - ',E.name), ".
					"-(ROUND($value,2)), ROUND(ABS(-$amount - $value),2) AS A, 'BUDGET', 'LINKED', B.id ".
					"FROM Budget AS B ".
					"INNER JOIN Elements AS E ON B.element_id = E.id ".
					"INNER JOIN Category AS C ON E.parent_id = C.id ".
					"WHERE B.id = '".$tt[0]."'");
			}
			else if ($tt[1] == "TRANSFER")	{
				$value = "(amount)";
				$l[] = q("SELECT S.date, S.description, $value, 0.00 AS A, 'TRANSFER', 'LINKED', S.id ".
					"FROM Statement AS S WHERE S.id = '".$tt[0]."'");
			}
		}
		if (!is_array($l))	return;			//empty array
		if (count($l) == 1)	return $l[0];	//only 1 element in array
		$list = $l[0];
		for($i=1; $i<count($l);++$i)
			if (is_array($list))
				if (is_array($l[$i]))
					$list = array_merge($list,$l[$i]);
		return $list;						//merged all rows into 1 array
	}

    //  Create Display Information Function
    if (isset($_POST["save"]) && $_POST["save"] === "1") 
    {
        GLOBAL $excelheadings;
        
        $errorMessage = "";
        $date_type = $_POST["dateType"];

        //  Get Dates
        if ($date_type == "currentwk") {
            $date_from = currentWeekStart();
            $date_to = $today;
        }
        else if ($date_type == "previouswk") {
            $date_from = getDates(currentWeekStart(), -7);
            $date_to = getDates(currentWeekStart(), -1);
        }
        else if ($date_type == "currentmnth") {
            $date_from = getMonthStart($today);
            $date_to = $today;
        }
        else if ($date_type == "previousmnth") {
            $date_from = getPreviousMonthStart();
            $date_to = getMonthEnd(getPreviousMonthStart());
        }
        else if ($date_type == "custom") {
            $date_from = addslashes(strip_tags($_POST["date_from"]));
            $date_to = addslashes(strip_tags($_POST["date_to"]));
        }

        //  Timestamp
        if ($date_from === $date_to)
            $timestamp = "Time Period: ".$date_from;
        else
            $timestamp = "Time Period: ".$date_from." - ".$date_to;

        $account_id = $_POST["account"];
        $type_id  = $_POST["type"];

		//SET where clause
        $where = "";
        if($account_id != null && is_numeric($account_id))
            $where .= " AND s.account = '$account_id' ";
        if($type_id != null && is_numeric($type_id))
            $where .= " AND e.id = '$type_id' ";        
        $where .= " AND s.date BETWEEN '$date_from' AND '$date_to' ";
        
		//GET transactions
		//	all linked and non-zero amounts
		$trans = q("SELECT s.date,s.description,s.amount,s.id FROM Statement AS s INNER JOIN StatementLink AS sl ON s.id = sl.statement_id ".
					"INNER JOIN ElementTypes AS e ON sl.elementType = e.id ".
					"WHERE s.company_id = '".$_SESSION['company_id']."' AND s.amount != 0 $where GROUP BY s.id ORDER BY date,amount");

		$displayString = "<table class='on-table'><tr><td class='on-table-clear' colspan='8'><a>Bank Statements<a></td></tr>";
		$displayString .= "<tr><th></th><th>Date</th><th>Description</th><th>Amount</th>".
			"<th style='background-color:#36332a'><font style='size:17pt; color:orange'>Matches in Ontrack</font><br/>".
			"<table><tr>
					<td style='background-color:#36332a;min-width:45px;max-width:45px'>Due_Date</td>
					<td style='background-color:#36332a;min-width:120px;max-width:120px'>Description</td>
					<td class='rightdata' style='background-color:#36332a;min-width:40px;max-width:40px'>Amount</td>
					<td class='rightdata' style='background-color:#36332a;min-width:35px;max-width:35px'>Diff</td>
					<td style='background-color:#36332a;min-width:35px'>Type</td>
					<td style='background-color:#36332a;min-width:15px'>Link / Unlink</td>
				</tr></table>
			</th></tr>";
		if (is_array($trans))
		foreach($trans as $tt)	{
			$displayString .= "<tr>
						<td>
							<input type='image' src='images/icons/yes.png' title='Correct' />
						</td>
						<td style='white-space:nowrap;padding-left:0;'>
							<input type='hidden' value='".$tt[3]."'/>".$tt[0]."</td><td>".$tt[1]."</td>
						<td class='rightdata' style='white-space:nowrap;border-right:1px solid #36332a;'>".$tt[2]."</td>
						<td>";
			$list = getLinkedItems($tt[3],$tt[2]);
			if (is_array($list))	{
				$displayString .= "<table>";
				foreach($list as $r)	{
					$diaplay .= "<tr>";
						$displayString .= "<td class='noborder' style='white-space:nowrap;min-width:45px;max-width:45px'>".$r[0]."</td>";
						$displayString .= "<td class='noborder' style='min-width:120px;max-width:120px'>".$r[1]."</td>";
						$displayString .= "<td class='noborder rightdata' style='white-space:nowrap;min-width:40px;max-width:40px;color:white;'>".$r[2]."</td>";
						$displayString .= "<td class='noborder rightdata' style='white-space:nowrap;min-width:35px;max-width:35px'>".$r[3]."</td>";
						$displayString .= "<td class='noborder' style='white-space:nowrap;min-width:35px;max-width:35px'>".$r[4]."</td>";
						if ($r[5] == "UNLINKED")
							$displayString.="<td class='noborder' style='min-width:15px;max-width:15px'>
								<input class='link' type='image' id='link' style='color:blue' src='images/icons/unlinked.png' title='Unlinked' value='L'/>
								<input type='hidden' id='statement_id' value='".$tt[3]."'/>
								<input type='hidden' id='element_id' value='".$r[6]."'/>
								<input type='hidden' id='type' value='".$r[4]."'/>
							</td>";
						else
							$displayString.="<td class='noborder' style='min-width:15px;max-width:15px'>
								<input class='link' type='image' id='link' style='color:red' src='images/icons/linked.png' title='Linked' value='U'/>
								<input type='hidden' id='statement_id' value='".$tt[3]."'/>
								<input type='hidden' id='element_id' value='".$r[6]."'/>
								<input type='hidden' id='type' value='".$r[4]."'/>
							</td>";
					$displayString .= "</tr>";
				}
				$displayString .= "</table>";
			}
			$displayString .= "</td></tr>";
		}
		$displayString .= "</table>";
        
        if ($displayString != "")
            $generated                                                  = "1";
    }
    
    //  Print Header
    print_header();
    //  Print Menu
	if ($_GET["menu"] == "")
		print_menus("0", "home");
	else
		print_menus($_GET["menu"], "home");

	?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    jQuery(function(){

        //Updateing the Processed and Unprocessed light
        ///////////////////////////////////////////////
        function updateLight(o) {
            return setLight(o,checkLight(o));
        }
        function checkLight(o)  {   //true = green, false = red;
            var sum = 0;
            var budgetCheck = false;
            o.children().each(function(i, child)  {
                if (jQuery(this).children().eq(5).children().eq(0).val() == "U")    {
                    if (jQuery(this).children().eq(4).html() == "BUDGET")   budgetCheck = true; //budget items only need to be linked to be green
                    sum += parseFloat(jQuery(this).children().eq(2).html());
                }
            });
            if (budgetCheck)    return true;
            statement_amount = parseFloat(o.parent().parent().prev().html());
            if (sum == statement_amount)    return true;//the sum of the linked values need to match the statement value (for INVOICE or EXPENSE) to be green
            return false;
        }
        function setLight(o, state) {
            light = o.parent().parent().prev().prev().prev().prev().children().eq(0);
            if (state)
                light.attr('src','images/icons/yes.png').attr('title','Processed');
            else
                light.attr('src','images/icons/no.png').attr('title','Unprocessed');
			return state;
        }
        ///////////////////////////////////////////////

        //Link or Unlinked or M button clicked
        ///////////////////////////////////////////////
        var M_button_clicked_holder;
        jQuery(".link").click(function()    {
            var button = jQuery(this);
            var statement_id = jQuery(this).next().val();
            var element_id = jQuery(this).next().next().val();
            var type = jQuery(this).next().next().next().val();
            if (jQuery(this).attr("value") == "U")
                jQuery.post("_ajax.php", {func: "linkStatement", act: 'DELETE', statement_id: statement_id, element_id: element_id, type: type}, function(data){
                    button.val("L");
                    button.attr('src','images/icons/unlinked.png');
                    button.attr('title','Unlinked');
					var state = updateLight(button.parent().parent().parent());
                });
            else if (jQuery(this).attr("value") == "L")
                jQuery.post("_ajax.php", {func: "linkStatement", act: 'ADD', statement_id: statement_id, element_id: element_id, type: type}, function(data){
                    data = json_decode(data);
                    var last = button.parent().parent();
                    last.children().eq(0).html(data[0][0]); //due_date
                    last.children().eq(2).html(parseFloat(data[0][1]).toFixed(2));  //amount
                    var diff = Math.abs(Math.abs(parseFloat(data[0][2])) - Math.abs(parseFloat(data[0][1]))).toFixed(2);    //diff
                    last.children().eq(3).html(diff);   //diff
                    button.val("U");
                    button.attr('src','images/icons/linked.png');
                    button.attr('title','Linked');
					var state = updateLight(button.parent().parent().parent());
                    if (type == "INVOICE" && state)
                        jQuery.post("_ajax.php",{func:"updateInvoices", statement_id: statement_id, type: type}, function(data) {
                            alert("Updated all invoice due_dates and marked as paid");
                        });
                });
		});




	jQuery('#date_from').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_from').val(),
		current: jQuery('#date_from').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_from').val()))
			_date = new Date();
			else _date = jQuery('#date_from').val();
			jQuery('#date_from').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_from').val(formated);
			jQuery('#date_from').DatePickerHide();
		}
	});
    })

    jQuery(function(){
	jQuery('#date_to').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_to').val(),
		current: jQuery('#date_to').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_to').val()))
			_date = new Date();
			else _date = jQuery('#date_to').val();
			jQuery('#date_to').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_to').val(formated);
			jQuery('#date_to').DatePickerHide();
		}
	});
    })

    //  Sajax
    <?php sajax_show_javascript(); ?>

    function check()
    {
        var valid                                                       = 1;

        if (document.forms["report"].dateType.value == "custom") {
            //  Check That Date From Is Entered
            if (document.forms["report"].date_from.value == "") {
                ShowLayer("dateFrom", "block");
                valid                                                   = 0;
            }
            //  Check That Entered Date From Is Valid
            else {
                ShowLayer("dateFrom", "none");

                if (!validation("date", document.forms["report"].date_from.value)) {
                    ShowLayer("dateFromDiv", "block");
                    valid                                               = 0;
                }
                else
                    ShowLayer("dateFromDiv", "none");
            }

            //  Check That Date To Is Entered
            if (document.forms["report"].date_to.value == "") {
                ShowLayer("dateTo", "block");
                valid                                                   = 0;
            }
            //  Check That Entered Date To Is Valid
            else {
                ShowLayer("dateTo", "none");

                if (!validation("date", document.forms["report"].date_to.value)) {
                    ShowLayer("dateToDiv", "block");
                    valid                                               = 0;
                }
                else
                    ShowLayer("dateToDiv", "none");
            }
        }

        if (valid == 1) {
            document.forms["report"].save.value                         = 1;
            document.forms["report"].submit();
        }
    }

    function dateSelection() {
        if (document.forms["report"].dateType.value == "custom")
            ShowLayer("datesDiv", "block");
        else
            ShowLayer("datesDiv", "none");
    }


</script>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata">
                <form action="" method="post" name="report">
                    <div id="" style="<?php if ($generated == "1") echo "display: none;"; else echo "display: block"; ?>">
                        <table width="100%">
                            <tr>
                                <td class="centerdata">
                                    <h6>Unlink Statements</h6>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Account:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="account">
                                        <option value="null">All</option>
                                        <?php
                                        	$accounts = q("SELECT account,name FROM Account WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY id");
                                            if (is_array($accounts))
                                                foreach ($accounts as $ac)
                                                    if ($_POST["account"] == $ac[0])
                                                        echo "<option value='".$ac[0]."' selected>".$ac[1]."</option>";
                                                    else
                                                        echo "<option value='".$ac[0]."'>".$ac[1]."</option>";
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Transaction Type:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="type">
                                        <option value="null">All</option>
                                        <?php
					$types = q("SELECT id,name FROM ElementTypes WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY name");
                                            if (is_array($types))
                                                foreach ($types as $type)
                                                    if ($_POST["type"] == $type[0])
                                                        echo "<option value='".$type[0]."' selected>".$type[1]."</option>";
                                                    else
                                                        echo "<option value='".$type[0]."'>".$type[1]."</option>";
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Date Type(s):
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="dateType" onChange="dateSelection();">
                                        <option value="currentwk">Current Week</option>
                                        <option value="previouswk">Previous Week</option>
                                        <option value="currentmnth">Current Month</option>
                                        <option value="previousmnth">Previous Month</option>
                                        <option value="custom">Custom Dates</option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                        <div id="datesDiv" style="display: none;">
                            <table width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Date From:
                                    </td>
                                    <td width="50%">
                                        <input class="on-field-date" id="date_from" name="date_from" type="text" style="text-align:right;" value="<?php
                                            if ($_POST["date_from"] != "") echo "".$_POST["date_from"]; else echo "".getMonthStart($today); ?>">
                                        <div id="dateFrom" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                        <div id="dateFromDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="on-description" width="50%">
                                        Date To:
                                    </td>
                                    <td width="50%">
                                        <input class="on-field-date" id="date_to" name="date_to" type="text" style="text-align:right;" value="<?php
                                            if ($_POST["date_to"] != "") echo "".$_POST["date_to"]; else echo "".getMonthEnd($today); ?>">
                                        <div id="dateTo" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                        <div id="dateToDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <br/>
                        <input name="btnGenerate" onClick="check();" type="button" value="Generate Report">
                        <input method="post" name="save" type="hidden" value="0" />
                    </div>
                </form>
                <div id="report" style="<?php if ($generated == "1") echo "display: block;"; else echo "display: none"; ?>">
                    <?php
                        echo "<div class='on-20px'><table class='on-table-center on-table'>";
                            echo "".$displayString;
                            echo  "<tfoot><tr><td colspan='".($columns+1+$hasAreas)."'></td></tr></tfoot>";
                        echo "</table></div>";
                        echo "<br/>";

                        ///////////////////////////
                        //  Set Export Information
//                        $_SESSION["fileName"] = "Time Sheet Report";
//                        $_SESSION["fileData"] = array_merge($excelheadings, $exceldata);
                        ///////////////////////////

//                        echo "<input name='btnExport' onClick=\"location.href='_export.php'\" type='button' value='Export to Excel' />";
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
