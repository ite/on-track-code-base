<?php
/*
*  File:        _export.php
*  Updated On:  2010-05-17
*  Author:      integrITyEngineering
*/
    session_start();

    include("_db.php");
    include("_functions.php");

    $fileName = $_SESSION["fileName"]." ".date("Ymd His");
    $fileData = $_SESSION["fileData"];

    $fileName = str_replace(" ", "_", $fileName);
    $fileName = createExport($fileName, $fileData, "csv");

    // Open File For Download/Save
    header("Content-Type: application/vnd.ms-excel");
    header("Content-Disposition: attachment; filename=$fileName");

    include("data/".$fileName);
?>
