<?php
    session_start();

    include("_db.php");
    include("_rss_script.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

//    if ($_SESSION["logged_in"] === true && !($_SESSION["usertype"] === "0"))
//        header("Location: home.php");

    if (!hasAccess("NEWS_MAN"))
        header("Location: noaccess.php");

    //  Insert New News Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")
    {
        $errorMessage                                                   = "";

        $heading1                                                       = addslashes(strip_tags($_POST["heading1"]));
        //$heading2                                                       = addslashes(strip_tags($_POST["heading2"]));
        //$heading3                                                       = addslashes(strip_tags($_POST["heading3"]));
        $heading2                                                       = "";
        $heading3                                                       = "";
        $description                                                    = addslashes(strip_tags($_POST["description"]));
        $time                                                           = date("H:i:s");

        //  Insert News into Database
        $insert                                                         = q("INSERT INTO RSS (heading1, heading2, heading3, description, date, time) ".
                                                                            "VALUES ('$heading1', '$heading2', '$heading3', '$description', '$today', '$time')");

        if ($insert)
        {
            //  Delete al id's in RSS_Seen table
            $delete                                                     = q("DELETE FROM RSS_Seen");

            createNewsDisplay();

            $logs                                                       = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time) ".
                                                                            "VALUES ('News added', 'Insert', 'RSS', '".$_SESSION["email"]."', ".
                                                                            "'$today', '$time')");

            header("Location: news_add.php");
        }
        else
            $errorMessage                                               = "News Not Added To Database";
    }

    if ($errorMessage != "")
        echo "<p align='center' style='padding:0px;'><strong><font class='on-validate-error'>$errorMessage</font></strong></p>";

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0", "news");
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    var cal                                                             = new CodeThatCalendar(caldef1);

    function check(box)
    {
        var valid                                                       = 1;

        //  Check That Heading 1 Is Entered
        if (document.forms["news_add"].heading1.value == "")
        {
            ShowLayer("headingDiv", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("headingDiv", "none");

        //  Check That Description Is Entered
        if (document.forms["news_add"].description.value == "")
        {
            ShowLayer("descriptionDiv", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("descriptionDiv", "none");

        if (valid == 1)
        {
            document.forms["news_add"].save.value                       = 1;
            document.forms["news_add"].submit();
        }
    }
</script>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="news_add">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Add New News</h6>
                            </td>
                        </tr>
                        <tr>
                            <td class="centerdata">
                                <br/>
                            </td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                Heading:
                            </td>
                            <td width="50%">
                                <input class="on-field" name="heading1" type="text" value="" tabindex="1">
                                <div id="headingDiv" style="display: none;"><font class="on-validate-error">* Heading 1 must be entered</font></div>
                            </td>
                        </tr>
                        <!--
                        <tr>
                            <td class="on-description" width="50%">
                                Heading 2:
                            </td>
                            <td align="left" width="50%">
                                <input class="on-field" name="heading2" type="text" value="" tabindex="2">
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                Heading 3:
                            </td>
                            <td align="left" width="50%">
                                <input class="on-field" name="heading3" type="text" value="" tabindex="3">
                            </td>
                        </tr>
                        -->
                        <tr>
                            <td class="on-description" width="50%">
                                Description:
                            </td>
                            <td align="left" width="50%">
                                <textarea class="on-field" name="description" tabindex="4" rows="5" cols="30"></textarea>
                                <div id="descriptionDiv" style="display: none;"><font class="on-validate-error">* Description must be entered</font></div>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <input name="btnAdd" onClick="check();" tabindex="5" type="button" value="Add News">
                    <input method="post" name="save" type="hidden" value="0" />
                </form>
            </td>
        </tr>
        <tr>
            <td class="centerdata">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
