<?php
	session_start();

	include("_db.php");
	include("graphics.php");
	include("include/sajax.php");

	unset($_SESSION["fileName"]);
	unset($_SESSION["fileData"]);

	if (!$_SESSION["logged_in"] === true)
		header("Location: login.php");

	if (!isset($_SESSION["company_id"]))
		header("Location: home.php");

	if (!hasAccess("REP_TIME_EX"))
		header("Location: noaccess.php");

	///////////////////////////
	//  Sajax
	function getProjects($status, $invoiceable, $type) {
		if ($status == "all")   $status = " AND p.completed IN (0,1)";
		else                    $status = " AND p.completed = '".$status."'";

		if ($invoiceable == "invoiceable")	$invoiceable = " AND cot.total_budget > 0 AND cot.total_budget != '' AND p.due_date != ''";
		else if ($invoiceable == "non_invoiceable")	$invoiceable = " AND (cot.total_budget = '' OR p.due_date = '')";
		else $invoiceable = "";

		if ($type == "null")
			$projects = q("SELECT DISTINCT(p.id),p.p_type, p.name FROM (Project AS p
							INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id)
							WHERE cot.company_id = '".$_SESSION["company_id"]."'
							AND p.deleted = '0' $status $invoiceable ORDER BY UPPER(p.name)");
		else
			$projects = q("SELECT DISTINCT(p.id),p.p_type, p.name FROM (Project AS p
							INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id)
							WHERE cot.company_id = '".$_SESSION["company_id"]."'
							AND p.deleted = '0'
							AND p.type_id='$type' $status $invoiceable ORDER BY UPPER(p.name)");

		return $projects;
	}

	$sajax_request_type = "GET";
	sajax_init();
	sajax_export("getProjects");

	sajax_handle_client_request();
	///////////////////////////

	// Class for table background color
	$colorClass = "OnTrack_#_";

	//  Set Report Generated Status
	$generated = "0";

	//  Create Display Information Function
	if (isset($_POST["save"]) && $_POST["save"] === "1")        {
		$errorMessage = "";
		$downloadList = "";

		$reportOn = $_POST["reportOn"];
		$projectStatus = $_POST["projectStatus"];
		$invoiceable = $_POST["invoiceableStatus"];

		$employee_id = $_POST["employee"];
		$projectString = $_POST["project"];
		$projectType = (substr($projectString,0,3) == "CP_") ? "CP" : "SP";
		$project_type_id = $_POST["projectType"];
		$project_id= substr($projectString,3);
		$area_id = $_POST["area"];
		$activity_type_id = $_POST["activityType"];
		$activity_id = $_POST["activity"];
		$date_type = $_POST["dateType"];

		//  Expense Info
		$expenseType = null;
		$vehicle_id = null;
		$printingType_id = null;
		$printing_id = null;

		//  Get Dates
		if ($date_type == "currentwk")  {
			$date_from = currentWeekStart();
			$date_to = $today;
		}
		else if ($date_type == "previouswk")    {
			$date_from = getDates(currentWeekStart(), -7);
			$date_to = getDates(currentWeekStart(), -1);
		}
		else if ($date_type == "currentmnth")   {
			$date_from = getMonthStart($today);
			$date_to = $today;
		}
		else if ($date_type == "previousmnth")  {
			$date_from = getPreviousMonthStart();
			$date_to = getMonthEnd(getPreviousMonthStart());
		}
		else if ($date_type == "projectLifetime")      {
			$date_fromT = q("SELECT MIN(date) FROM TimeSheet WHERE project_id = '".$project_id."'");
			$date_fromE = q("SELECT MIN(date) FROM ExpenseSheet WHERE project_id = '".$project_id."'");
			$date_from = ($date_fromT <= $date_fromE) ? $date_fromT : $date_fromE;
			$date_to = $today;
		}
		else if ($date_type == "custom")        {
			$date_from = addslashes(strip_tags($_POST["date_from"]));
			$date_to = addslashes(strip_tags($_POST["date_to"]));
		}

		//  Timestamp
		$timestamp = ($date_from === $date_to) ? "Time Period: ".$date_from : "Time Period: ".$date_from." - ".$date_to;

		$total_hours = 0;
		$expense_total = 0;

		$hasAreas = 0;
		$select = "";
		$select_emp_name = "";
		$from = "";
		$from2 = "";

		$table = ($reportOn == "approved") ? "ApprovedTime" : "TimeSheet";
		$where = ($reportOn == "approved") ? "AND ts.status = '2' " : "";

		$table2 = ($reportOn == "approved") ? "ApprovedExpense" : "ExpenseSheet";
		$where2 = ($reportOn == "approved") ? "AND es.status = '2' " : "";

		$showEmployee = 0;
		$empHead = "";
	   //-- Employee -- //
		if($employee_id == "null"){
			$employee_info[0][1]  = "All Employees";
			$employee_info[0][0]  = "";
			$select_emp_name .= " CONCAT(e.lstname, ', ', e.frstname), ";
			$from .= " INNER JOIN Employee AS e ON ts.user_id = e.id ";
			$from2 .= " INNER JOIN Employee AS e ON es.user_id = e.id ";
			$showEmployees = 1;
			$empHead = "Employee";
		}else{
			$employee_info = q("SELECT lstname, frstname FROM Employee WHERE id = '$employee_id'");
			$where .= " AND ts.user_id=$employee_id  ";
			$where2 .= " AND es.user_id = $employee_id ";
		}
		// -- Project Type -- //
		if($project_type_id != "null" && $project_type_id != 0){
			$project_Type_info = q("SELECT type FROM projectTypes WHERE id = '$project_type_id'");
			$where .= " AND p.type_id = '$project_type_id'";
			$where2 .= " AND p.type_id = '$project_type_id'";
		}else{
			$project_Type_info = "All Project Types";
		}
		// -- Project -- //
		if($project_id != "null" && $project_id != 0){
			$project_info = q("SELECT name FROM Project WHERE id = '$project_id'");
			$where .= " AND ts.project_id = $project_id ";
			$where2 .= " AND es.project_id = $project_id ";
		}else{
			$project_info = "All Projects";
		}
		// -- Area -- //
		if($project_id != "null" && $project_id != 0){
			$areasTime = q("SELECT project_id, area_id FROM TimeSheet WHERE company_id = '".$_SESSION["company_id"]."' AND project_id =".$project_id." AND area_id > 0 AND date >= '".$date_from."' AND date <= '".$date_to."'");
			$areasExpense = q("SELECT project_id, area_id FROM ExpenseSheet WHERE company_id = '".$_SESSION["company_id"]."' AND project_id =".$project_id." AND area_id > 0 AND date >= '".$date_from."' AND date <= '".$date_to."'");
		} else {
			$areasTime = q("SELECT project_id, area_id FROM TimeSheet WHERE company_id = '".$_SESSION["company_id"]."'AND area_id > 0 AND date >= '".$date_from."' AND date <= '".$date_to."'");
			$areasExpense = q("SELECT project_id, area_id FROM ExpenseSheet WHERE company_id = '".$_SESSION["company_id"]."' AND area_id > 0 AND date >= '".$date_from."' AND date <= '".$date_to."'");
		}
		if (is_array($areasTime) || is_array($areasExpense))    {
			$hasAreas = 1;
			$select .= "ar.name,";
		} else  {
			$hasAreas = 0;
		}
		if($area_id != "null"){
			$area_info = q("SELECT name FROM areas WHERE id = '$area_id'");
			$where .= " AND ts.area_id = $area_id ";
			$where2 .= " AND es.area_id = $area_id ";
		}else{
			$area_info = "All Areas";
		}
		// -- Activity Type -- //
		if($activity_type_id != "null"){
			$where .= " AND at.id = $activity_type_id ";
			$where2 .= " AND at.id = $activity_type_id ";
		}
		// -- Activity -- //
		if($activity_id != "null"){
			$where .= " AND ts.activity_id = $activity_id ";
			$where2 .= " AND es.activity_id = $activity_id ";
		}

		if ($projectStatus == "all")    $projectStatus = " AND p.completed IN (0,1) ";
		else                            $projectStatus = " AND p.completed = '".$projectStatus."' ";

		if ($invoiceable == "invoiceable")	{
			$where .= " AND cot.total_budget > 0 AND cot.total_budget != '' AND p.due_date != ''";
			$where2 .= " AND cot.total_budget > 0 AND cot.total_budget != '' AND p.due_date != ''";
		} else if ($invoiceable == "non_invoiceable")	{
			$where .= " AND (cot.total_budget = '' OR p.due_date = '')";
			$where2 .= " AND (cot.total_budget = '' OR p.due_date = '')";
		}

		//  Get Information - Time
		$timeInfo = q("SELECT ".$select_emp_name." p.name,p.number,".$select."at.type,a.name,ts.descr,ts.date,ts.rate,ts.time_spent,ts.time_spent * ts.rate
						FROM ($table AS ts
							".$from."
							LEFT JOIN Activities AS a ON ts.activity_id = a.id
							LEFT JOIN ActivityTypes AS at ON a.parent_id = at.id
							INNER JOIN Project AS p ON ts.project_id = p.id
							INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
							LEFT JOIN areas AS ar ON ts.area_id = ar.id )
						WHERE ts.date >= '".$date_from."' AND ts.date <= '".$date_to."' AND ts.company_id = '".$_SESSION["company_id"]."' $projectStatus
						 ".$where." ORDER BY ts.date, ts.on_date, ts.on_time, p.name");

		//  Get Information - Expense
		$expenseInfo = q("SELECT ".$select_emp_name." p.name,p.number,dd.name,IF(dd.name = 'Driving',v.type,IF(dd.name = 'Printing',CONCAT(dt.type,': ',d.name),es.otherDescr)),".$select."at.type,a.name,es.descr,es.date,
								IF(dd.name = 'Driving',es.kilometers,es.total), es.rate, es.expense".(($table2 == "ExpenseSheet") ? ", es.id" : ", es.expensesheet_id")."
							FROM ($table2 AS es
								".$from2."
								INNER JOIN dropdowns AS dd ON es.expense_type_id = dd.id
								LEFT JOIN Activities AS a ON es.activity_id = a.id
								LEFT JOIN ActivityTypes AS at ON a.parent_id = at.id
								INNER JOIN Project AS p ON es.project_id = p.id
								INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
								LEFT JOIN areas AS ar ON es.area_id = ar.id
								LEFT JOIN Vehicle AS v ON es.vehicle_id = v.id
								LEFT JOIN Disbursements AS d ON es.disbursement_id = d.id
								LEFT JOIN DisbursementTypes AS dt ON dt.id = d.parent_id)
							WHERE es.date >= '".$date_from."' AND es.date <= '".$date_to."' AND es.company_id = '".$_SESSION["company_id"]."' $projectStatus
							".$where2." ORDER BY es.date, es.on_date, es.on_time, p.name");
		//  Create Headings
		$reportHead = array("
			<a><font style='color:orange'>Time & Expense</font> Report: ".$employee_info[0][1].", ".$employee_info[0][0]."</a>",
			"Project Type: ".$project_Type_info,
			"Project: ".$project_info,
			"Areas: ".$area_info,
			$timestamp
		);

		if ($hasAreas)  {
			if($showEmployees == 1){
				$timeHeadings = array($empHead,"Project Name","Project #","Area","Activity Type","Activity","Description","Date","Rate","Time Spent","Cost <i>(".$_SESSION["currency"].")</i>");
				$timeHeadings2 = array($empHead,"Project Name","Project #","","","Area","Activity Type","Activity","Description","Date","Rate","Time Spent","Cost <i>(".$_SESSION["currency"].")</i>");
				$expenseHeadings = array($empHead,"Project Name","Project #","Expense Type","Expense Info","Area","Activity Type","Activity","Description","Date","Quantity","Rate","Cost <i>(".$_SESSION["currency"].")</i>");
			}else{
				$timeHeadings = array("Project Name","Project #","Area","Activity Type","Activity","Description","Date","Rate","Time Spent","Cost <i>(".$_SESSION["currency"].")</i>");
				$timeHeadings2 = array("Project Name","Project #","","","Area","Activity Type","Activity","Description","Date","Rate","Time Spent","Cost <i>(".$_SESSION["currency"].")</i>");
				$expenseHeadings = array("Project Name","Project #","Expense Type","Expense Info","Area","Activity Type","Activity","Description","Date","Quantity","Rate","Cost <i>(".$_SESSION["currency"].")</i>");
			}
		} else  {
			if($showEmployees == 1){
				$timeHeadings = array($empHead,"Project Name","Project #","Activity Type","Activity","Description","Date","Rate","Time Spent","Cost <i>(".$_SESSION["currency"].")</i>");
				$timeHeadings2 = array($empHead,"Project Name","Project #","","","Activity Type","Activity","Description","Date","Rate","Time Spent","Cost <i>(".$_SESSION["currency"].")</i>");
				$expenseHeadings = array($empHead,"Project Name","Project #","Expense Type","Expense Info","Activity Type","Activity","Description","Date","Quantity","Rate","Cost <i>(".$_SESSION["currency"].")</i>");
			}else{
				$timeHeadings = array("Project Name","Project #","Activity Type","Activity","Description","Date","Rate","Time Spent","Cost <i>(".$_SESSION["currency"].")</i>");
				$timeHeadings2 = array("Project Name","Project #","","","Activity Type","Activity","Description","Date","Rate","Time Spent","Cost <i>(".$_SESSION["currency"].")</i>");
				$expenseHeadings = array("Project Name","Project #","Expense Type","Expense Info","Activity Type","Activity","Description","Date","Quantity","Rate","Cost <i>(".$_SESSION["currency"].")</i>");
			}
		}

		//  Create Display String
		if (is_array($timeInfo) || is_array($expenseInfo))    {
			$timeColumns = count($timeHeadings) - 1;
			$expenseColumns = count($expenseHeadings) - 1;

			$columns = (!is_array($expenseInfo)) ? $timeColumns : $expenseColumns;
			$hasExpense = (is_array($expenseInfo)) ? 1 : 0;
			$extraCounter = 0;
			if(!$hasExpense)
				$extraCounter  = 2;

			$displayString = "";
			$row = 0;

			//  HTML Display
			foreach ($reportHead as $rh)
				$displayString .= "<tr><td class='on-table-clear' colspan='100%'>".$rh."</td></tr>";

			//  EXCEL DISPLAY
			foreach ($reportHead as $rh)        {
				$exceldata[$row][] = strip_tags($rh);

				for ($a = 0; $a < $columns+$extraCounter; $a++)       $exceldata[$row][] = "";

				$row++;
			}
			$timeCostTotal = 0;
			$expenseCostTotal = 0;

			if (is_array($timeInfo))    {
				$total_hours = 0;
				$total_cost = 0;

				//  Headings
				//  HTML Display
				$displayString .= "<tr>";

				foreach ($timeHeadings as $h)   {
					$colspan = ($h == "Project Name") ? 3 : 1;

					$displayString .= "<th colspan='".$colspan."'>".$h."</th>";
				}

				$displayString .= "</tr>";

				//  EXCEL DISPLAY
				foreach ($timeHeadings2 as $h)
					$exceldata[$row][] = $colorClass.strip_tags($h);

				//for ($a = count($timeHeadings); $a < $columns; $a++)    $exceldata[$row][] = $colorClass."";

				$row++;

				//  Data
				foreach ($timeInfo as $r)       {
					$col = 0;
					$displayString .= "<tr>";

					foreach ($r as $d) {
						$colspan = ($timeHeadings[$col] == "Project Name") ? 3 : 1;

						if ($timeHeadings[$col] == "Time Spent")                                $total_hours += $d;
						if ($timeHeadings[$col] == "Cost <i>(".$_SESSION["currency"].")</i>")   $total_cost += $d;

						if (preg_match("/^((\d{2}(([02468][048])|([13579][26]))[\-\/\s]?((((0?[13578])|(1[02]))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-\/\s]?((0?[1-9])|([1-2][0-9])))))|(\d{2}(([02468][1235679])|([13579][01345789]))[\-\/\s]?((((0?[13578])|(1[02]))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-\/\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))(\s(((0?[1-9])|(1[0-2]))\:([0-5][0-9])((\s)|(\:([0-5][0-9])\s))([AM|PM|am|pm]{2,2})))?$/", $d))
							$displayString .= "<td colspan='".$colspan."' style='white-space:nowrap'>".$d."</td>";
						else if (is_numeric($d))
							$displayString .= "<td colspan='".$colspan."' style='white-space:nowrap' class='rightdata'>".number_format($d, 2, ".", "")."</td>";
						else if ($d == "")
							$displayString .= "<td colspan='".$colspan."' style='white-space:nowrap'>-</td>";
						else
							$displayString .= "<td colspan='".$colspan."' style='white-space:nowrap'>".$d."</td>";

						$exceldata[$row][] .= $d;
						if($timeHeadings[$col] == "Project Name")   {
							$exceldata[$row][] .= "";
							$exceldata[$row][] .= "";
						}
						$col++;
					}

					for ($a = count($timeHeadings) - 1; $a < $columns; $a++)    $exceldata[$row][] = "";

					$displayString .= "</tr>";
					$row++;
				}

				$colspan = ($hasAreas) ? 10 : 9;
				$colspan += $showEmployees;
				$displayString .= "<tr>
										<td class='on-table-total' colspan='".$colspan."><a style='color:#FFFFFF;'>Total <i>(".$_SESSION["currency"].")</a></td>
										<td class='on-table-total'>".number_format($total_hours, 2, ".", "")."</td>
										<td class='on-table-total'>".number_format($total_cost, 2, ".", "")."</td>
									</tr>";
				$timeCostTotal = $total_cost;

				$colspan = ($hasAreas) ? 9 : 8;

				for ($a = 0; $a < $colspan+$showEmployees; $a++)   $exceldata[$row][] = "";

				$exceldata[$row][] .= "Total (".$_SESSION["currency"]."):";
				$exceldata[$row][] .= $total_hours;
				$exceldata[$row][] .= $total_cost;

				for ($a = count($timeHeadings) - 1; $a < $columns; $a++)    $exceldata[$row][] = "";

				$row++;

				if (is_array($expenseInfo))     $displayString .= "<tr><td style='background-color:transparent' colspan='100%'>&nbsp;</td></tr>";
			}

			if (is_array($expenseInfo))    {
				$total_cost = 0;

				//  Headings
				//  HTML Display
				$displayString .= "<tr>";

				foreach ($expenseHeadings as $h)   {
					$displayString .= "<th>".$h."</th>";
				}

				$displayString .= "</tr>";

				//  EXCEL DISPLAY
				foreach ($expenseHeadings as $h)
					$exceldata[$row][] = $colorClass.strip_tags($h);

				$row++;

				//  Data
				foreach ($expenseInfo as $r)       {
					$col = 0;
					$displayString .= "<tr>";

					$columns = count($r) - 1;

					foreach ($r as $d) {
						if (isset($expenseHeadings[$col]))	{
						   if ($expenseHeadings[$col] == "Cost <i>(".$_SESSION["currency"].")</i>")   $total_cost += $d;

							if (preg_match("/^((\d{2}(([02468][048])|([13579][26]))[\-\/\s]?((((0?[13578])|(1[02]))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-\/\s]?((0?[1-9])|([1-2][0-9])))))|(\d{2}(([02468][1235679])|([13579][01345789]))[\-\/\s]?((((0?[13578])|(1[02]))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-\/\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))(\s(((0?[1-9])|(1[0-2]))\:([0-5][0-9])((\s)|(\:([0-5][0-9])\s))([AM|PM|am|pm]{2,2})))?$/", $d))
								$displayString .= "<td style='white-space:nowrap'>".$d."</td>";
							else if (is_numeric($d))
								$displayString .= "<td style='white-space:nowrap' class='rightdata'>".number_format($d, 2, ".", "")."</td>";
							else if ($d == "")
								$displayString .= "<td style='white-space:nowrap'>-</td>";
							else	{
								if ($expenseHeadings[$col] == "Project Name")	{
									$displayString .= "<td style='white-space:nowrap'>";
									if (hasBackingDocuments($r[$columns]))	{
										$displayString .= $d. " <img onClick='window.open(\"serveFile.php?type=files&expense=".$r[$columns]."\")' class='hand' title='Click to download backing document(s)' src='images/icons/download.png' />";
										$downloadList .= $r[$columns].",";
									} else
										$displayString .= $d;
									$displayString .= "</td>";
								}
								else
									$displayString .= "<td style='white-space:nowrap'>".$d."</td>";
							}

							$exceldata[$row][] .= $d;
						}

						$col++;
					}

					$displayString .= "</tr>";
					$row++;
				}

				$colspan = ($hasAreas) ? 11 : 10;
				$colspan += $showEmployees;
				$displayString .= "<tr>
										<td class='on-table-total' colspan='".$colspan."><a style='color:#FFFFFF;'>Total <i>(".$_SESSION["currency"].")</a></td>
										<td class='on-table-total'>".number_format($total_cost, 2, ".", "")."</td>
									</tr>";
				$expenseCostTotal = $total_cost;

				for ($a = 0; $a < $colspan - 1; $a++)   $exceldata[$row][] = "";

				$exceldata[$row][] .= "Total (".$_SESSION["currency"]."):";
				$exceldata[$row][] .= $total_cost;

				$row++;
			}
			if (is_array($timeInfo) || is_array($expenseInfo))    {
				if(is_array($expenseInfo)){
					$colspan = ($hasAreas) ? 11 : 10;
					$colspan += $showEmployees;
				}else if(is_array($timeInfo)){
					$colspan = ($hasAreas) ? 9 : 8;
					$colspan += $showEmployees;
					$gapspan = 2;
				}
				$displayString .= "<tr><td colspan = '100%'>&nbsp;</td></tr>
								<tr>
									<td class='on-table-total' colspan='".$colspan."><a style='color:#FFFFFF;'>Grand Total <i>(".$_SESSION["currency"].")</a></td>";
				if($gapspan > 0)
					$displayString .= "<td class='on-table-total' colspan='".$gapspan."'></td>";
				$displayString .= "<td class='on-table-total'>".number_format($expenseCostTotal+$timeCostTotal, 2, ".", "")."</td>
								</tr>";

				for ($a = 0; $a < $colspan - 1 + $gapspan; $a++)   $exceldata[$row][] = "";

				$exceldata[$row][] .= "Grand Total (".$_SESSION["currency"]."):";
				$exceldata[$row][] .= $expenseCostTotal+$timeCostTotal;

				$row++;
			}
		}

		if ($displayString != "")
			$generated = "1";
	}

	//  Print Header
	print_header();
	//  Print Menu
	print_menus("0", "reports");
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
	// ----- new additions ----- //
	var loadActivityTypes;
	var loadActivities;
	var hasAreas = false;

	function is_array(input)    {
		return typeof(input)=='object'&&(input instanceof Array);
	}

	jQuery(function()    {
		//  Load Areas
		loadAreas = function()  {
			var projectType = jQuery("#project").val().substring(0, 2);
			var projectID = jQuery("#project").val().substring(3);

			jQuery.post("_ajax.php", {func: "get_area_info", projectType: projectType, projectID: projectID}, function(data)	{
				data = json_decode(data);
				jQuery("#area option").remove();

				jQuery("#area").append(jQuery("<option></option>").attr("value","null").text("All Areas"));
				if (is_array(data)) {   // Display area options
					hasAreas = true;
					jQuery("#areaInfo").show();
				}
				else    {               // Do not display area options
					hasAreas = false;
					jQuery("#areaInfo").hide();
				}

				jQuery.each(data,function(i, v)	{
					jQuery("#area").append(jQuery("<option></option>").attr("value",v[0]).text(v[1]));
				});
			});
		};
		//  Load Activity Types
		loadActivityTypes = function()  {
			var projectType = jQuery("#project").val().substring(0, 2);
			var projectID = jQuery("#project").val().substring(3);

			jQuery.post("_ajax.php", {func: "get_activity_types", projectType: projectType, projectID: projectID}, function(data)	{
				data = json_decode(data);

				jQuery("#activityType option").remove();
				jQuery("#activityType").append(jQuery("<option></option>").attr("value","null").text("All Activity Types"));

				jQuery.each(data,function(i, v)	{
					jQuery("#activityType").append(jQuery("<option></option>").attr("value",v[0]).text(v[1]));
				});
			});
		};
		//  Load Activities
		loadActivities = function()  {
			var projectType = jQuery("#project").val().substring(0, 2);
			var activityType = jQuery("#activityType").val();

			jQuery.post("_ajax.php", {func: "get_activities", projectType: projectType, activityType: activityType}, function(data)	{
				data = json_decode(data);

				jQuery("#activity option").remove();
				jQuery("#activity").append(jQuery("<option></option>").attr("value","null").text("All Activities"));

				jQuery.each(data,function(i, v)	{
					jQuery("#activity").append(jQuery("<option></option>").attr("value",v[0]).text(v[1]));
				});
			});
		};
		//////////////////////////////
		//  Project Change - Show/Hide
		jQuery("#project").change(function()    {
			if (jQuery(this).val() != "null")    {
				jQuery("#project").focus();
				loadAreas();
				loadActivityTypes();
				jQuery("#activityTypeInfo").show();
			}
			else    {
				//jQuery("#info").hide();
				jQuery("#project").focus();
			}
		});
		//  Activity Type Change
		jQuery("#activityType").change(function()    {
			jQuery("#activityType").val(jQuery(this).val()).attr("selected", true);
			jQuery("#activityInfo").show();
			loadActivities();
		});
		//////////////////////////////
	});
	// ------------------------ //

	//  Calendar
	jQuery(function(){
	jQuery('#date_from').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_from').val(),
		current: jQuery('#date_from').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_from').val()))
			_date = new Date();
			else _date = jQuery('#date_from').val();
			jQuery('#date_from').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_from').val(formated);
			jQuery('#date_from').DatePickerHide();
		}
	});

		jQuery('#date_to').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_to').val(),
		current: jQuery('#date_to').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_to').val()))
			_date = new Date();
			else _date = jQuery('#date_to').val();
			jQuery('#date_to').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_to').val(formated);
			jQuery('#date_to').DatePickerHide();
		}
	});

		jQuery("#project").change(function()    {
			if (jQuery(this).val() != "null")   {
				jQuery("#dateType option[value='projectLifetime']").removeAttr("disabled");
			} else      {
				jQuery("#dateType option[value='projectLifetime']").attr("disabled","disabled");
				jQuery("#dateType option[value='currentwk']").attr("selected", "selected");
			}
		});
	});

	function check()
	{
		var valid                                                       = 1;

		if (document.forms["report"].dateType.value == "custom")
		{
			//  Check That Date From Is Entered
			if (document.forms["report"].date_from.value == "")
			{
				ShowLayer("dateFrom", "block");
				valid                                                   = 0;
			}
			//  Check That Entered Date From Is Valid
			else
			{
				ShowLayer("dateFrom", "none");

				if (!validation("date", document.forms["report"].date_from.value))
				{
					ShowLayer("dateFromDiv", "block");
					valid                                               = 0;
				}
				else
					ShowLayer("dateFromDiv", "none");
			}

			//  Check That Date To Is Entered
			if (document.forms["report"].date_to.value == "")
			{
				ShowLayer("dateTo", "block");
				valid                                                   = 0;
			}
			//  Check That Entered Date To Is Valid
			else
			{
				ShowLayer("dateTo", "none");

				if (!validation("date", document.forms["report"].date_to.value))
				{
					ShowLayer("dateToDiv", "block");
					valid                                               = 0;
				}
				else
					ShowLayer("dateToDiv", "none");
			}
		}

		if (valid == 1)
		{
			document.forms["report"].save.value                         = 1;
			document.forms["report"].submit();
		}
	}

	function dateSelection()
	{
		if (document.forms["report"].dateType.value == "custom")
			ShowLayer("datesDiv", "block");
		else
			ShowLayer("datesDiv", "none");
	}

	//  Sajax
	<?php sajax_show_javascript(); ?>
	///////////////////////// -- Sajax -- ///////////////////////////
	function ClearOptions(OptionList) {
		for (x = OptionList.length; x >= 0; x--)
		{
			OptionList[x]                                               = null;
		}
	}

	// Set Projects
	function setProjects(data) {
		var c                                                           = 0;
		document.report.project.options[(c++)]                         = new Option("All Projects", "null");
		for (var i in data)
			eval("document.report.project.options["+(c++)+"] = new Option('" + data[i][2] + "', '" + data[i][1] + "_" + data[i][0] + "');");
	}

	//Get Projects
	function getProjects(select1,select2,select3) {
		var status = eval("document.report."+select1+".value;");
		var invoiceable = eval("document.report."+select2+".value;");;
		var type = eval("document.report."+select3+".value;");;

		ClearOptions(document.report.project);
		x_getProjects(status, invoiceable, type, setProjects);
	}
</script>

<?php
    $employees = q("SELECT e.id, e.lstname, e.frstname FROM (Employee AS e INNER JOIN Company_Users AS cu ON e.id = cu.user_id) ".
                                "WHERE cu.company_id = '".$_SESSION["company_id"]."' AND e.email != 'admin' ORDER BY e.lstname, frstname");
    $projectTypes = q("SELECT id, type FROM ProjectTypes WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY type");
    $projectTypes[] = array("0","Shared Projects");
    $projectTypes = sortArrayByColumn($projectTypes, 1);
    $projects = q("SELECT DISTINCT(p.id), p.name, p_type FROM (Project AS p INNER JOIN Project_User AS pu ON p.id = pu.project_id INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) ".
                          "WHERE "/*pu.user_id = '".$_SESSION["user_id"]."' AND "*/ ."cot.company_id = '".$_SESSION["company_id"]."' AND pu.company_id = '".$_SESSION["company_id"]."' "/*AND p.completed = '0' */."AND p.deleted = '0' ".
                          "AND pu.deleted = '0' ORDER BY UPPER(p.name)");
?>
	<table width="100%">
		<tr>
			<td class="centerdata">
				<form action="" method="post" name="report">
					<div id="" style="<?php if ($generated == "1") echo "display: none;"; else echo "display: block"; ?>">
						<table width="100%">
							<tr>
								<td class="centerdata">
									<h6>Time &amp; Expense Sheet Report</h6>
								</td>
							</tr>
						</table>
						<br/>
						<table width="100%">
							<tr>
								<td class="on-description" width="50%">
									Report On:
								</td>
								<td width="50%">
									<select id="reportOn" name="reportOn" method="post" class="on-field" tabindex="<?php echo $index++; ?>">
										<option value="normal">Actual Time/Expenses</option>
										<option value="approved">Approved Time/Expenses</option>
									</select>
								</td>
							</tr>
							<tr>
								<td class="on-description" width="50%">
									Select Project Status:
								</td>
								<td width="50%">
									<select id="projectStatus" name="projectStatus" method="post" class="on-field" tabindex="<?php echo $index++; ?>" onChange="getProjects('projectStatus','invoiceableStatus','projectType');">
										<option value="all" selected>Completed & Uncompleted</option>
										<option value="0">Uncompleted</option>
										<option value="1">Completed</option>
									</select>
								</td>
							</tr>
							<tr>
								<td class="on-description" width="50%">
									Project Invoiceable Type:
								</td>
								<td width="50%">
									<select class="on-field" method="post" name="invoiceableStatus" onChange="getProjects('projectStatus','invoiceableStatus','projectType');">
										<option value="both" selected>All (Invoiceable & Non-Invoicable)</option>
										<option value="invoiceable">Invoiceable</option>
										<option value="non_invoiceable">Non-Invoicable</option>
									</select>
								</td>
							</tr>
							<tr><td colspan="100%"><br/></td></tr>
							<tr>
								<td class="on-description" width="50%">
									Select Employee:
								</td>
								<td width="50%">
									<select id="employee" name="employee" method="post" class="on-field" tabindex="<?php echo $index++; ?>">
										<option value="null">All Employees</option>
										<?php
											if (is_array($employees))
												foreach ($employees as $employee)
													if ($_POST["employee"] == $employee[0])
														echo "<option value='".$employee[0]."' selected>".$employee[1].", ".$employee[2]."</option>";
													else
														echo "<option value='".$employee[0]."'>".$employee[1].", ".$employee[2]."</option>";
										?>
									</select>
								</td>
							</tr>
							<tr>
								<td class="on-description" width="50%">
									Select Project Type:
								</td>
								<td width="50%">
									<select id="projectType" name="projectType" method="post" class="on-field" tabindex="<?php echo $index++; ?>" onChange="getProjects('projectStatus','invoiceableStatus','projectType');">
										<option value="null">All Project Types</option>
										<?php
											if (is_array($projectTypes))
												foreach ($projectTypes as $projectType)
													if ($_POST["projectType"] == $projectType[0])
														echo "<option value='".$projectType[0]."' selected>".$projectType[1]."</option>";
													else
														echo "<option value='".$projectType[0]."'>".$projectType[1]."</option>";
										?>
									</select>
								</td>
							</tr>
							<tr>
						</table>

						<table width="100%">
							<tr>
								<td class="on-description" width="50%">
										Select Project:
								</td>
								<td width="50%">
									<select id="project" name="project" method="post" class="on-field required" tabindex="<?php echo $index++; ?>">
										<option value="null">All Projects</option>
										<?php
											if (is_array($projects))    {
												foreach ($projects as $project) {
													$value = ($project[2] == "CP") ? "CP_".$project[0]: "SP_".$project[0];

													if ($_POST["project"] == $value)    echo "<option value='".$value."' selected>".$project[1]."</option>";
													else                                echo "<option value='".$value."'>".$project[1]."</option>";
												}
											}
										?>
									</select>
									<div id="projectDiv" name="projectDiv" class="error">* Project must be selected</div>
								</td>
							</tr>
						</table>

						<div id="areaInfo" name="areaInfo1" style="display:none;">
							<table width="100%">
								<tr>
									<td class="on-description" width="50%">
											Select Area:
									</td>
									<td width="50%">
											<select id="area" name="area" method="post" class="on-field required" tabindex="<?php echo $index++; ?>">
												<option value="null">All Areas</option>
												<?php
													if (is_array($areas))
														foreach ($areas as $area)
															if ($_POST["area"] == $area[0]) echo "<option value='".$area[0]."' selected>".$area[1]."</option>";
															else                            echo "<option value='".$area[0]."'>".$area[1]."</option>";
												?>
											</select>
											<div id="areaDiv" name="areaDiv" class="error">* Area must be selected</div>
									</td>
								</tr>
							</table>
						</div>
						<div id="activityTypeInfo" name="activityTypeInfo" style="display:none;">
							<table width="100%">
								<tr>
									<td class="on-description" width="50%">
										Select Activity Type:
									</td>
									<td width="50%">
										<select id="activityType" name="activityType" method="post" class="on-field required" tabindex="<?php echo $index++; ?>">
											<option value="null">All Activity Types</option>
										</select>
										<div id="activityTypeDiv" name="activityTypeDiv" class="error">* Activity type must be selected</div>
									</td>
								</tr>
							</table>
						</div>
						<div id="activityInfo" name="activityInfo" style="display:none;">
							<table width="100%">
								<tr>
									<td class="on-description" width="50%">
										Select Activity:
									</td>
									<td width="50%">
										<select id="activity" name="activity" method="post" class="on-field required" tabindex="<?php echo $index++; ?>">
											<option value="null">All Activities</option>
										</select>
										<div id="activityDiv" name="activityDiv" class="error">* Activity must be selected</div>
									</td>
								</tr>
							</table>
						</div>
						<table width="100%">
							<tr>
								<td class="on-description" width="50%">
									Date Type(s):
								</td>
								<td width="50%">
									<select class="on-field" method="post" id="dateType" name="dateType" onChange="dateSelection();">
										<option value="currentwk">Current Week</option>
										<option value="previouswk">Previous Week</option>
										<option value="currentmnth">Current Month</option>
										<option value="previousmnth">Previous Month</option>
										<option value="projectLifetime" disabled>Project Lifetime</option>
										<option value="custom">Custom Dates</option>
									</select>
								</td>
							</tr>
						</table>
						<div id="datesDiv" style="display: none;">
							<table width="100%">
								<tr>
									<td class="on-description" width="50%">
										Date From:
									</td>
									<td width="50%">
										<input class="on-field-date" id="date_from" name="date_from" type="text" style="text-align:right;" value="<?php
											if ($_POST["date_from"] != "") echo "".$_POST["date_from"]; else echo "".getMonthStart($today); ?>">
										<div id="dateFrom" style="display: none;"><font color="#FF5900">* Date must be entered</font></div>
										<div id="dateFromDiv" style="display: none;"><font color="#FF5900">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font>
										</div>
									</td>
								</tr>
								<tr>
									<td class="on-description" width="50%">
										Date To:
									</td>
									<td>
										<input class="on-field-date" id="date_to" name="date_to" type="text" style="text-align:right;" value="<?php
											if ($_POST["date_to"] != "") echo "".$_POST["date_to"]; else echo "".getMonthEnd($today); ?>">
										<div id="dateTo" style="display: none;"><font color="#FF5900">* Date must be entered</font></div>
										<div id="dateToDiv" style="display: none;"><font color="#FF5900">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
									</td>
								</tr>
							</table>
						</div>
						<br/>
						<input name="btnGenerate" onClick="check();" type="button" value="Generate Report">
						<input method="post" name="save" type="hidden" value="0" />
					</div>
				</form>
				<div id="report" style="<?php if ($generated == "1") echo "display: block;"; else echo "display: none"; ?>">
					<?php
						echo "<div class='on-20px'><table class='on-table-center on-table'>";
							echo "<h6>Personal Time &amp; Expense Report</h6>";
							echo "".$displayString;
							echo  "<tfoot>
											<tr>
												<td colspan='100%'></td>
											</tr>
										</tfoot>";
						echo "</table>
								</div>";
						echo "<br/>";

						///////////////////////////
						//  Set Export Information
						$_SESSION["fileName"] = "Time Expense Sheet Report";
						$_SESSION["fileData"] = $exceldata;
						///////////////////////////

						echo "<input name='btnExport' onClick=\"location.href='_export.php'\" type='button' value='Export to Excel' />";

						if ($downloadList != "")	{
							$downloadList = substr($downloadList,0,-1);

							echo "<br/><br/><input onClick='window.open(\"serveFile.php?type=files&expense=".$downloadList ."\")' class='hand' type='button' value='Download backing document(s)' />";
						}
				   ?>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<br/>
			</td>
		</tr>
	</table>
<?php
    //  Print Footer
    print_footer();
?>