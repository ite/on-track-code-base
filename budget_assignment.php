<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("_functions.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    if (!hasAccess("BUDGET_MANAGE") && !hasAccess("BUDGET_VIEW"))
        header("Location: noaccess.php");
    if (hasAccess("BUDGET_VIEW") && !hasAccess("BUDGET_MANAGE"))	{
        $RA_VIEW = 1;
        $readonly = "readonly";
        $disabled = "disabled";
    }

    //  Set Year/Month Variables To Current Year/Month
    $display_year = date("Y");
    $display_month = date("m");

    if (isset($_POST["selectDate"]) && $_POST["selectDate"] != "") {
        $display_year = substr($_POST["selectDate"], 0, strpos($_POST["selectDate"], "-"));
        $display_month = substr($_POST["selectDate"], (strpos($_POST["selectDate"], "-") + 1), 2);
    }

    if (isset($_POST["btnAmend"])) {
        unset($_POST["btnAmend"]);

        $prevDate = getPreviousMonth($display_year, $display_month);
        $year = substr($prevDate, 0, strpos($prevDate, "-"));
        $month = substr($prevDate, (strpos($prevDate, "-") + 1), 2);

        $categories = q("SELECT c.id, c.name, COUNT(DISTINCT(e.id)) FROM (Category AS c INNER JOIN Elements AS e ON c.id = e.parent_id INNER JOIN Budget AS b ON e.id = b.element_id) ".
                        "WHERE (c.company_id = '".$_SESSION["company_id"]."' AND e.status = '1') OR (b.year = '".$year."' AND b.month = '".$month."' ".
                            "AND b.company_id = '".$_SESSION["company_id"]."') GROUP BY c.id ORDER BY c.name");

        if (is_array($categories)) {
            foreach ($categories as $category) {
                $elements = q("SELECT e.id, e.name, e.status, b.planned, b.amount, b.actual, b.due_date FROM (Budget AS b INNER JOIN Elements AS e ON b.element_id = e.id) ".
                                "WHERE e.parent_id = '".$category[0]."' AND b.year = '".$year."' AND b.month = '".$month."' ORDER BY e.name");

                if (is_array($elements)) {
                    foreach ($elements as $element) {
                        //$alreadyUpdated = q("SELECT updated FROM Budget WHERE year = '$display_year' AND month = '$display_month' AND company_id = '".$_SESSION["company_id"]."' AND element_id = '".$element[0]."'");

                        //if ($alreadyUpdated == "0")  {
                            $planned = $element[3];
                            $amount = $element[4];

                            if ($_SESSION["company_id"] != "1" && $_SESSION["company_id"] != "52" && $_SESSION["company_id"] != "56")      $due_date = ($element[6] != "") ? getDates($element[6], 30) : "";
                            else                                                                        $due_date = ($element[6] != "") ? date("Y-m-d", strtotime("+1 month", strtotime($element[6]))) : ""; 

                            if (exist("Budget", "year = '$display_year' AND month = '$display_month' AND company_id = '".$_SESSION["company_id"]."' AND element_id = '".$element[0]."'"))   {
                                if ($_SESSION["company_id"] != "1" && $_SESSION["company_id"] != "52" && $_SESSION["company_id"] != "56")
                                    $update = q("UPDATE Budget SET planned = '".$planned."', amount = '".$amount."', actual = '0', due_date = '".$due_date."' ".
                                                "WHERE year = '$display_year' AND month = '$display_month' AND company_id = '".$_SESSION["company_id"]."' AND element_id = '".$element[0]."'");
                                else
                                    $update = q("UPDATE Budget SET planned = '".$amount."', due_date = '".$due_date."' ".
                                                "WHERE year = '$display_year' AND month = '$display_month' AND company_id = '".$_SESSION["company_id"]."' AND element_id = '".$element[0]."'");
                            } else  {
                                if ($_SESSION["company_id"] != "1" && $_SESSION["company_id"] != "52" && $_SESSION["company_id"] != "56")
                                    $insert = q("INSERT INTO Budget (year, month, element_id, planned, amount, actual, due_date, company_id) VALUES ".
                                                "('$display_year', '$display_month', '".$element[0]."', '$planned', '$amount', '0', '$due_date', '".$_SESSION["company_id"]."')");
                                else
                                    $insert = q("INSERT INTO Budget (year, month, element_id, planned, amount, actual, due_date, company_id) VALUES ".
                                                "('$display_year', '$display_month', '".$element[0]."', '$amount', '', '0', '$due_date', '".$_SESSION["company_id"]."')");
                            }
                        //}
                    }
                }
            }
        }

        header("Location: budget_assignment.php");
    }

    //  Create Insert String
    function getData($id) {
        $selectDate = $_POST["selectDate"];
        $year = substr($selectDate, 0, strpos($selectDate, "-"));
        $month = substr($selectDate, (strpos($selectDate, "-") + 1), 2);

        $planned = addslashes(strip_tags($_POST["planned".$id]));
        $amount = addslashes(strip_tags($_POST["amount".$id]));
        $due_date = addslashes(strip_tags($_POST["due_date".$id]));

        if (isset($_POST["actual".$id]))        $actual = 1;
        else                                    $actual = 0;

        if ($planned != "")     {
            if (substr($planned, 0, 2) == "R ") $planned = number_format(substr($planned, 2, strlen($planned)), 2, ".", "");
            else                                $planned = number_format($planned, 2, ".", "");
        }

        if ($amount != "")      {
            if (substr($amount, 0, 2) == "R ")  $amount = number_format(substr($amount, 2, strlen($amount)), 2, ".", "");
            else                                $amount = number_format($amount, 2, ".", "");
        }

        //  GET OLD VALUES TO COMPARE WITH CURRENT VALUES
//        $oldValues = q("SELECT planned, amount, actual, due_date FROM Budget WHERE year = '$year' AND month = '$month' AND company_id = '".$_SESSION["company_id"]."' AND element_id = '".$id."'");

        //if ($planned == "" || $due_date == "")  {
            //if (exist("Budget", "year = '$year' AND month = '$month' AND company_id = '".$_SESSION["company_id"]."' AND element_id = '".$id."'"))
            //    $delete = q("DELETE FROM Budget WHERE year = '$year' AND month = '$month' AND company_id = '".$_SESSION["company_id"]."' AND element_id = '".$id."'");
        //}

        //if ($planned != "" || $due_date != "")  {
            if (exist("Budget", "year = '$year' AND month = '$month' AND company_id = '".$_SESSION["company_id"]."' AND element_id = '".$id."'"))
                $update = q("UPDATE Budget SET planned = '".$planned."', amount = '".$amount."', actual = '".$actual."', due_date = '".$due_date."' ".
                            "WHERE year = '$year' AND month = '$month' AND company_id = '".$_SESSION["company_id"]."' AND element_id = '".$id."'");
            else
                $insert = q("INSERT INTO Budget (year, month, element_id, planned, amount, actual, due_date, company_id) VALUES ".
                            "('$year', '$month', '".$id."', '$planned', '$amount', '$actual', '$due_date', '".$_SESSION["company_id"]."')");
        //}
/*
        //  CHECK IF VALUES DIFFERED
        if (is_array($oldValues))       {
            $oldPlanned = $oldValues[0][0];
            $oldAmount = $oldValues[0][1];
            $oldActual = $oldValues[0][2];
            $oldDueDate = $oldValues[0][3];

            $updated = 0;

            if (number_format((double)$oldPlanned, 2, ".", "") != number_format((double)$planned, 2, ".", ""))  $updated++;
            if (number_format((double)$oldAmount, 2, ".", "") != number_format((double)$amount, 2, ".", ""))    $updated++;
            if (number_format((double)$oldActual, 2, ".", "") != number_format((double)$actual, 2, ".", ""))    $updated++;
            if ($oldDueDate != $due_date)                                                                       $updated++;

            if ($updated > 0)
                $update = q("UPDATE Budget SET updated = 1 WHERE year = '$year' AND month = '$month' AND company_id = '".$_SESSION["company_id"]."' AND element_id = '".$id."'");
        }
*/
        return;
    }

    //  Save/Update Budget Function
    if (isset($_POST["save"]) && $_POST["save"] === "1") {
        $errorMessage = "";
        $values = "";

        //  Check If Current Month Has Starting Balance
        $selectDate = $_POST["selectDate"];

        $year = substr($selectDate, 0, strpos($selectDate, "-"));
        $month = substr($selectDate, (strpos($selectDate, "-") + 1), 2);

        $start_balance_exists = q("SELECT id, date, amount FROM StartBalance WHERE company_id = '".$_SESSION["company_id"]."' AND date LIKE '".$year."-".$month."-%' ORDER BY date DESC");

        if ($_POST["balance_amount"] != "" && number_format($_POST["balance_amount"], 2, ".", "") != 0) {
            $new_balance = addslashes(strip_tags($_POST["balance_amount"]));
            $new_date = addslashes(strip_tags($_POST["balance_date"]));

            if ($start_balance_exists && ($year."-".$month."-") == (date("Y-m-"))) {
                if ($start_balance_exists[0][1] == $today)
                    $update = q("UPDATE StartBalance SET amount = '".number_format($new_balance, 2, ".", "")."' WHERE id = '".$start_balance_exists[0][0]."'");
                else if ($start_balance_exists[0][1] != $new_date)
                    $update = q("UPDATE StartBalance SET date = '".$new_date."' WHERE id = '".$start_balance_exists[0][0]."'");
                else if (!($start_balance_exists[0][2] == $new_balance))
                    $insert = q("INSERT INTO StartBalance (amount, date, company_id) VALUES ('".number_format($new_balance, 2, ".", "")."', '$today', '".$_SESSION["company_id"]."')");
            }
            else if (!$start_balance_exists)
                $insert = q("INSERT INTO StartBalance (amount, date, company_id) VALUES ('".number_format($new_balance, 2, ".", "")."', '$today', '".$_SESSION["company_id"]."')");
        }

        //  Insert/Update/Delete Budget Values...
        $categories = q("SELECT c.id, c.name, COUNT(DISTINCT(e.id)) FROM (Category AS c INNER JOIN Elements AS e ON c.id = e.parent_id INNER JOIN Budget AS b ON e.id = b.element_id) ".
                        "WHERE (c.company_id = '".$_SESSION["company_id"]."' AND e.status = '1') OR (b.year = '".$year."' AND b.month = '".$month."' ".
                            "AND b.company_id = '".$_SESSION["company_id"]."') GROUP BY c.id ORDER BY c.name");

        if (is_array($categories)) {
            foreach ($categories as $category) {
                $elements = q("SELECT e.id, e.name, e.status, b.planned, b.amount, b.actual, b.due_date FROM (Budget AS b INNER JOIN Elements AS e ON b.element_id = e.id) ".
                                "WHERE e.parent_id = '".$category[0]."' AND b.year = '".$year."' AND b.month = '".$month."' ORDER BY e.name");

                if (is_array($elements)) {
                    foreach ($elements as $element) {
                        $string = getData($element[0]);
                    }
                }
            }
        }

        $errorMessage = "Values Saved/Updated Successfully";
        $time = date("H:i:s");

        $message = addslashes(createMessageReport($year, $month, $_SESSION["company_id"]));
        $notify_id = q("SELECT id FROM Notify WHERE code = 'Budget'");
        $events = q("INSERT INTO Events (date, time, message, notify_id, processed, user_id, company_id) ".
                    "VALUES ('".$today."', '".$time."', '$message', '$notify_id', '0', '".$_SESSION["user_id"]."', '".$_SESSION["company_id"]."')");

        $logs = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                    "VALUES ('Budget values created/updated', 'Insert', 'Budget', '".$_SESSION["email"]."', '$today', '$time', '".$_SESSION["company_id"]."')");

        header("Location: budget_assignment.php");
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("2", "budget");

    if ($errorMessage != "")    {
        if ($errorMessage == "Please note that we are currently busy with data management, no entries are allowed for 2010-08-26. Please check back in 24 Hours.")
            echo "<p align='center' style='padding:0px;'><strong style='font-size:12px;'><font color='#FF5900'>$errorMessage</font></strong></p>";
        else
            echo "<p align='center' style='padding:0px;'><strong><font color='#999999'>$errorMessage</font></strong></p>";
        echo "<br/>";
    }
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    var loadStatementLinks;

    jQuery(function(){
<?php if (!$RA_VIEW)	{?>
	jQuery('#balance_date').DatePicker({
		format:'Y-m-d',
		date: jQuery('#balance_date').val(),
		current: jQuery('#balance_date').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
                    var _date;
                    if (!validation('date',jQuery('#balance_date').val()))
                    _date = new Date();
                    else _date = jQuery('#balance_date').val();
                    jQuery('#balance_date').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
                    jQuery('#balance_date').val(formated);
                    jQuery('#balance_date').DatePickerHide();
		}
	});
<?php	} ?>
    })

    jQuery(function(){
        loadStatementLinks = function(e)   {
            jQuery.post("_ajax.php", {func: "getStatementLinks", elementType: "BUDGET", elementID: e}, function(data)	{
                data = json_decode(data);

                var bool = false;
                jQuery("#statementLinks tbody>tr").not(':first').not(':last').remove();    //remove current entries
                var last = jQuery("#statementLinks tbody>tr:last");

                jQuery.each(data,function(i, v)  {
                    var n = last.clone(true);
                    n.children().eq(0).html(v[0]);
                    n.children().eq(1).html(v[1]);
                    n.children().eq(2).html(parseFloat(v[2]).toFixed(2));
                    n.insertBefore(last);
                    bool = true;
                });

                if (bool)	last.remove();
            });
        };

        //  Open Modal
        jQuery(".openModal").click(function()   {
            var id = "#modal_statementLinks";

            loadStatementLinks(jQuery(this).attr("id"));

            jQuery("#mask").css({"width":jQuery(document).width(),"height":jQuery(document).height()});
            jQuery('#mask').fadeIn("fast").fadeTo("fast",0.8);   //transition effect
            //Set the popup window to center
            jQuery(id).css('top',  jQuery(window).scrollTop()+(jQuery(window).height()/2)-(jQuery(id).height()/1.1));
            jQuery(id).css('left', (jQuery(window).width()/2)-(jQuery(id).width()));
            jQuery(id).fadeIn("fast");   //transition effect
        });

        //  Close Modal
        jQuery(".closeModal").click(function()  {
            jQuery("#mask").hide();
            jQuery('.on-drop-menu').hide();
            jQuery('.on-contact-modal').hide();
        });

        jQuery(".pbar").each(function(i, v)  {
            var color;
            var val = parseFloat(jQuery(this).attr('name'));
            jQuery(jQuery(this)).progressbar({ "value": val});
            if (val < 50)			color = "Red";
            else if (val < 90)		color = "Orange";
            else if (val <= 100)	color = "Lime";
            else if (val <= 125)	color = "Orange";
            else					color = "Red";
            jQuery("#" + this.id + " > div").css({ 'background': color });
        });

    <?php
        $categories = q("SELECT c.id, c.name, COUNT(DISTINCT(e.id)) FROM (Category AS c INNER JOIN Elements AS e ON c.id = e.parent_id INNER JOIN Budget AS b ON e.id = b.element_id) ".
                        "WHERE (c.company_id = '".$_SESSION["company_id"]."' AND e.status = '1') OR (b.year = '".$display_year."' AND b.month = '".$display_month."' ".
                            "AND b.company_id = '".$_SESSION["company_id"]."') GROUP BY c.id ORDER BY c.name");

        if (is_array($categories)) {
            foreach ($categories as $category) {
                $elements = q("SELECT e.id, e.name, e.status, b.planned, b.amount, b.actual, b.due_date FROM (Budget AS b INNER JOIN Elements AS e ON b.element_id = e.id) ".
                                "WHERE e.parent_id = '".$category[0]."' AND b.year = '".$display_year."' AND b.month = '".$display_month."' ORDER BY e.name");

                if (!$RA_VIEW)  {
                    if (is_array($elements)) {
                        foreach ($elements as $element) {
                            jQDate("due_date".$element[0]);
                        }
                    }
                }
            }
        }
    ?>
    })

    function check()    {
        var valid = 1;

        //  Check That Balance Amount Is Valid, If Entered
        if (document.forms["budget_assignment"].balance_amount.value != "")     {
            if (!validation("currency", document.forms["budget_assignment"].balance_amount.value))      {
                ShowLayer("balanceAmount", "block");
                valid = 0;
            }
            else
                ShowLayer("balanceAmount", "none");
        }
        else
            ShowLayer("balanceAmount", "none");

        if (valid == 1) {
            document.forms["budget_assignment"].save.value = 1;
            document.forms["budget_assignment"].submit();
        }
    }

    function calTotal() {
        var idString = document.forms["budget_assignment"].idString.value;
        var plannedTot = 0;
        var amountTot = 0;
        var actualTot = 0;

        idString = idString.split(",");

        for (var i = 0; i < idString.length; i++) {
            var planned = "planned" + idString[i];
            var amount = "amount" + idString[i];
            var actual = "actual" + idString[i];

            var plannedVal = eval("document.forms['budget_assignment']."+planned+".value");
            var amountVal = eval("document.forms['budget_assignment']."+amount+".value");

            if (plannedVal != "")       {
                if (plannedVal.substring(0,2) == "R ")  {
                    plannedVal = (plannedVal.substring(2));
                    plannedVal = parseFloat(plannedVal).toFixed(2);
                }
                else
                    plannedVal = parseFloat(plannedVal).toFixed(2);

                plannedTot += parseFloat(plannedVal);

                eval("document.forms['budget_assignment']."+planned+".value = 'R '+(parseFloat(plannedVal)).toFixed(2);");
            }

            if (amountVal != "")        {
                if (amountVal.substring(0,2) == "R ")   {
                    amountVal = (amountVal.substring(2));
                    amountVal = parseFloat(amountVal).toFixed(2);
                }
                else
                    amountVal = parseFloat(amountVal).toFixed(2);

                amountTot += parseFloat(amountVal);

                if (eval("document.forms['budget_assignment']."+actual+".checked") == true) {
                    actualTot += parseFloat(amountVal);
                }

                eval("document.forms['budget_assignment']."+amount+".value = 'R '+(parseFloat(amountVal)).toFixed(2);");
            }
        }

        document.forms["budget_assignment"].plannedTotal.value = "R "+plannedTot.toFixed(2);
        document.forms["budget_assignment"].amountTotal.value = "R "+amountTot.toFixed(2);
        document.forms["budget_assignment"].actualTotal.value = "R "+actualTot.toFixed(2);
    }
</script>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="budget_assignment">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Running Cost Management</h6>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                Select Month to Edit:
                            </td>
                            <td width="50%">
                                <select class="on-field" method="post" name="selectDate" onChange="submit();" tabindex="1">
                                    <?php
                                        //  Check If Budget Exists For Current Month
                                        $current_year = date("Y");
                                        $current_month = date("m");

                                        $budget_exists = q("SELECT id FROM Budget WHERE year = '".$current_year."' AND month = '".$current_month."' AND company_id = '".$_SESSION["company_id"]."'");

                                        if (!$budget_exists)
                                            if ($display_year == $current_year && $display_month == $current_month)
                                                echo "<option value='".$current_year."-".$current_month."' selected>".getMonthString($current_month)."-".$current_year."</option>";
                                            else
                                                echo "<option value='".$current_year."-".$current_month."'>".getMonthString($current_month)."-".$current_year."</option>";

                                        //  Get Budgets That Exist
                                        $count_records = q("SELECT COUNT(DISTINCT(CONCAT(year, '-', month))) FROM Budget WHERE company_id = '".$_SESSION["company_id"]."'");
                                        $records = q("SELECT DISTINCT(CONCAT(year, '-', month)) FROM Budget WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY CONCAT(year, '-', month) DESC");

                                        if ($count_records > 1) {
                                            foreach ($records as $record)       {
                                                $year = substr($record[0], 0, strpos($record[0], "-"));
                                                $month = substr($record[0], (strpos($record[0], "-") + 1), 2);

                                                if ($display_year == $year && $display_month == $month)
                                                    echo "<option value='".$year."-".$month."' selected>".getMonthString($month)."-".$year."</option>";
                                                else
                                                    echo "<option value='".$year."-".$month."'>".getMonthString($month)."-".$year."</option>";
                                            }
                                        }
                                        else if ($count_records == 1)   {
                                            $year = substr($records, 0, strpos($records, "-"));
                                            $month = substr($records, (strpos($records, "-") + 1), 2);

                                            if ($display_year == $year && $display_month == $month)
                                                echo "<option value='".$year."-".$month."' selected>".getMonthString($month)."-".$year."</option>";
                                            else
                                                echo "<option value='".$year."-".$month."'>".getMonthString($month)."-".$year."</option>";
                                        }
                                    ?>
                                </select>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <table width="100%">
                        <?php
                            $start_balance_info = q("SELECT date, amount FROM StartBalance WHERE date LIKE '".$display_year."-".$display_month. "-%' AND company_id = '".$_SESSION["company_id"]."' ORDER BY date DESC");
                        ?>
                        <tr>
                            <td class="on-description" width="50%">
                                Starting Date:
                            </td>
                            <td width="50%">
                                <input class="on-field-date" id="balance_date" name="balance_date" readonly style="text-align:right;" type="text" value="<?php echo "".$start_balance_info[0][0]; ?>">
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                Starting Balance:
                            </td>
                            <td width="50%">
                                <input class="on-field" name="balance_amount" style="text-align:right;" tabindex="2" type="text" <?php echo $readonly;?> value="<?php
                                    echo "".number_format($start_balance_info[0][1], 2, ".", ""); ?>">
                                <div id="balanceAmount" style="display: none;"><font color="#FF5900">* Entered amount must be numeric, eg. 123.45</font></div>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <!--  Headings   -->
                    <table class="on-table-center on-table">
                        <tr>
                            <th>
                               Category
                            </td>
                            <th>
                               Element
                            </td>
                            <th>
                               Planned<br/>
                               Amount <i>(<?php echo "".$_SESSION["currency"]; ?>)</i>
                            </td>
                            <th>
                               Actual<br/>
                               Amount <i>(<?php echo "".$_SESSION["currency"]; ?>)</i>
                            </td>
                            <th>
                               Paid
                            </td>
                            <th>
                               Date Payable
                            </td>
                            <th></td>
                        </tr>
                        <!--  Table Information   -->
                        <?php
                            //  Check If Budget Exists For Display Year/Month
                            $budget_exists = q("SELECT id FROM Budget WHERE year = '".$display_year."' AND month = '".$display_month."' AND company_id = '".$_SESSION["company_id"]."'");

                            $grand_total_planned = 0;
                            $grand_total_amount = 0;
                            $grand_total_actual = 0;

                            //  Create Index Start Values
                            $planned_index = 3;
                            $amount_index = 4;
                            $actual_index = 5;
                            $date_index = 6;

                            $categories = q("SELECT c.id, c.name, COUNT(DISTINCT(e.id)) FROM (Category AS c INNER JOIN Elements AS e ON c.id = e.parent_id INNER JOIN Budget AS b ON e.id = b.element_id) ".
                                            "WHERE (c.company_id = '".$_SESSION["company_id"]."' AND e.status = '1') GROUP BY c.id ORDER BY c.name");

                            $idString = "";

                            if (is_array($categories))  {
                                foreach ($categories as $category)      {
                                    $elements = q("SELECT e.id, e.name, e.status, b.planned, b.amount, b.actual, b.due_date, b.id FROM (Budget AS b INNER JOIN Elements AS e ON b.element_id = e.id) ".
                                                    "WHERE e.parent_id = '".$category[0]."' AND b.year = '".$display_year."' AND b.month = '".$display_month."' AND e.status = '1' ORDER BY e.name");

                                    echo "<tr>";
                                        echo "<td rowspan='".$category[2]."'>";
                                            if ($RA_VIEW)       echo $category[1];
                                            else                echo "<a href='category_edit.php?id=".$category[0]."'>".$category[1]."</a>";
                                        echo "</td>";
                                        if (is_array($elements))        {
                                            $count = 0;
                                            $last = count($elements) - 1;

                                            foreach ($elements as $element)     {
                                                //  Element Name
                                                $color = ($element[2] == "1") ? "" : "style='font-weight:normal;color:#A0A0A0;'";
                                                echo "<td>";
                                                    echo "<a $color>".$element[1]."</a>";
                                                echo "</td>";
                                                //  Planned Amount
                                                $amount = (is_numeric($element[3])) ? number_format((double)$element[3], 2, ".", "") : "";
                                                $grand_total_planned += $amount;
                                                echo "<td>";
                                                    echo "<input id='planned".$element[0]."' name='planned".$element[0]."' tabindex='".$planned_index."' $readonly type='text' style='text-align:right;' ".
                                                            "value='".$amount."' onBlur='calTotal();' />";
                                                echo "</td>";
                                                $planned_index += 4;
                                                //  Actual Amount
                                                $amount = (is_numeric($element[4])) ? number_format((double)$element[4], 2, ".", "") : "";
                                                $grand_total_amount += $amount;
                                                $secondaryReadonly = ($_SESSION["company_id"] != "1" && $_SESSION["company_id"] != "52" && $_SESSION["company_id"] != "56") ? "" : "readonly";
                                                echo "<td>";
                                                    echo "<input id='amount".$element[0]."' name='amount".$element[0]."' tabindex='".$amount_index."' $readonly $secondaryReadonly type='text' style='text-align:right;' ".
                                                            "value='".$amount."' onBlur='calTotal();' />";
                                                echo "</td>";
                                                $amount_index += 4;
                                                //  Paid
                                                $checked = ($element[5]) ? "checked" : "";
                                                $grand_total_actual += ($element[5]) ? $element[4] : 0;
                                                echo "<td align='center'>";
                                                    if ($_SESSION["company_id"] != "1" && $_SESSION["company_id"] != "52" && $_SESSION["company_id"] != "56")
                                                        echo "<input id='actual".$element[0]."' name='actual".$element[0]."' tabindex='".$actual_index."' $checked $disabled type='checkbox' value='1' onChange='calTotal();' />";
                                                    else        {
                                                        $act = q("SELECT -SUM(S.amount) FROM (Statement AS S INNER JOIN StatementLink AS SL ON S.id = SL.statement_id INNER JOIN Budget AS B ON SL.element_id = B.id) ".
                                                                    "WHERE B.id = '".$element[7]."'");
                                                        $ans = (is_numeric($element[3]) && $element[3] > 0) ? number_format((($act / $element[3]) * 100),0) : number_format(0,0);
                                                        //$ans = (is_numeric($element[3]) && $element[3] > 0) ?number_format((($element[4] / $element[3]) * 100),2) : number_format(0,2);

                                                        echo "<table border='0' cellpadding='0' cellspacing='0' width='80%'>";
                                                            echo "<tr>";
                                                                echo "<td style='border:0;padding-top:2px;margin:0;'><div id='".$element[0]."' name='$ans' class='pbar' style='height:10px;width:40px;'></div></td>";
                                                                echo "<td style='border:0;padding:0;margin:0;' class='rightdata'><div id='".$element[0]."value'>$ans%</div></td>";
                                                            echo "</tr>";
                                                        echo "</table>";
                                                    }
                                                echo "</td>";
                                                $actual_index += 4;
                                                //  Due Date
                                                $due_date = ($element[6] && !$budget_exists) ? getDates($element[6], 30) : $element[6];
                                                echo "<td>";
                                                    echo "<input id='due_date".$element[0]."' name='due_date".$element[0]."' tabindex='".$date_index."' $readonly type='text' style='text-align:right;' value='".$due_date."' />";
                                                echo "</td>";
                                                $date_index += 4;

                                                echo "<td>";
                                                    echo "<input id='".$element[7]."' name='".$element[7]."' class='openModal' type='button' value='...' title='View Linked Statement Items'>";
                                                echo "</td>";

                                                echo "</tr>";

                                                $count++;
                                                
                                                if ($count != $last)
                                                    echo "<tr>";
                                            }
                                        }
                                }
                            }

                            $grand_total_planned = number_format($grand_total_planned, 2, ".", "");
                            $grand_total_amount = number_format($grand_total_amount, 2, ".", "");
                            $grand_total_actual = number_format($grand_total_actual, 2, ".", "");

                            echo "<tr>
                                        <td class='on-table-total' colspan='2'>Grand Total:</td>
                                        <td class='on-table-total'><input name='plannedTotal' tabindex='0' readonly type='text' value='".$_SESSION["currency"]." ".$grand_total_planned."'></td>
                                        <td class='on-table-total'><input name='amountTotal' tabindex='0' readonly type='text' value='".$_SESSION["currency"]." ".$grand_total_amount."'></td>
                                        <td class='on-table-total'><input name='actualTotal' tabindex='0' readonly type='text' value='".$_SESSION["currency"]." ".$$grand_total_actual."'></td>
                                        <td class='on-table-total' colspan='2'></td>
                                    </tr>
                                    <tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        ?>
                    </table>
                    <br/>
<?php	if (!$RA_VIEW)	{	?>
                    <input name="btnAdd" onClick="check();" tabindex="<?php echo "".($date_index + 1);?>" type="button" value="Save/Update Running Cost">
                    <?php
                        $isManager = q("SELECT ur.id,r.id FROM user_role AS ur INNER JOIN roles AS r ON ur.roleid = r.id ".
                                        "WHERE r.role = 'MAN' AND ur.userid = '".$_SESSION["user_id"]."' AND ur.companyid = '".$_SESSION["company_id"]."' AND r.companyid = '".$_SESSION["company_id"]."'");
                        $isManager = ($isManager[0][0] != 0) ? 1 : 0;

                        if ($isManager && ($_SESSION["company_id"] == "1" || $_SESSION["company_id"] == "52" && $_SESSION["company_id"] != "56")) {
                            if (number_format($display_year, 0, ".", "") <= number_format($current_year, 0, ".", "") 
                                && number_format($display_month, 0, ".", "") < number_format($current_month, 0, ".", ""))
                                echo "<br/><br/><input name='btnAmend' tabindex='1' type='submit' disabled value='Amend Running Cost to Previous Month'>";
                            else
                                echo "<br/><br/><input name='btnAmend' tabindex='1' type='submit' value='Amend Running Cost to Previous Month'>";
                        }
                    ?>
                    <input method="post" name="save" type="hidden" value="0" />
                    <input method="post" name="idString" type="hidden" value="<?php echo "".substr($idString, 0, -1); ?>" />
<?php } ?>
                </form>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
    <!--  BEGIN: STATEMENT LINKS MODAL  -->
    <div id="modal_statementLinks" name="modal_statementLinks" class="on-contact-modal" style='margin-left:auto;margin-right:auto;text-align:center;'>
        <table style='width:100%'>
            <tr>
                <td colspan="100%" class='centerdata'><a><b>Linked Statement Entries</b></a><br><br></td>
            </tr>
            <tr><td>&nbsp;</td></tr>
        </table>
        <div style="height:300px;overflow:scroll;overflow-x:hidden;">
            <table id='statementLinks'>
                <tr>
                    <td class='noborder'><strong>Date</strong></td>
                    <td class='noborder'><strong>Description</strong></td>
                    <td class='noborder'><strong>Amount <?php echo "<i>(".$_SESSION["currency"].")</i>"; ?></strong></td>
                </tr>
                <tr>
                    <td class='noborder'><a></a></td>
                    <td class='noborder'><a></a></td>
                    <td class='noborder'><a></a></td>
                </tr>
            </table>
        </div>
        <br/>
        <br/>
        <table style='width:100%'>
            <tr>
                <td class="centerdata">
                    <input id="modal_statementLinksClose" name="modal_statementLinksClose" class="closeModal" type="button" value="Close" />
                </td>
            </tr>
        </table>
    </div>
    <!--  END: STATEMENT LINKS MODAL  -->
<?php
    //  Print Footer
    print_footer();
?>
