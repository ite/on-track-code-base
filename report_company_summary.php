<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");
        
    if (!hasAccess("REP_COMPANY_SUMM"))
        header("Location: noaccess.php");
        
    // Class for table background color
    $colorClass = "OnTrack_#_";

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("6", "reports");
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="report_company_summary">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Company Summary Report</h6>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <?php
                        ///////////////////////////
                        //  Get Information
                        ///////////////////////////
                        //  nCompanies                                  = Number of Companies
                        $companies                                      = q("SELECT id, name, rate FROM Company ORDER BY name");
                        ///////////////////////////
                        //  Create Information String
                        ///////////////////////////
                        //  Table Headers
                        $string                                         = "";
                        $grand_total                                    = 0;
                        
                        $row                                                              = 0;
                        
                        $excelheadings[$row][]                                     = "Report: Company Summary"; 
                        $excelheadings[$row][]                                     = "";
                        $excelheadings[$row][]                                     = "";    
                        $excelheadings[$row][]                                     = "";
                            $row++;
                        
                        $excelheadings[$row][]                                     = $colorClass."Company Name"; 
                        $excelheadings[$row][]                                     = $colorClass."Number of Users";
                        $excelheadings[$row][]                                     = $colorClass."Rate/User (".$_SESSION["currency"].")";    
                        $excelheadings[$row][]                                     = $colorClass."Total (".$_SESSION["currency"].")";
                            $row++;
                        
                        $headers                                        = "<tr>
                                                                                        <th>Company Name</th>
                                                                                        <th>Number of Users</th>
                                                                                        <th>Rate/User <i>(".$_SESSION["currency"].")</i></th>
                                                                                        <th>Total <i>(".$_SESSION["currency"].")</i></th>
                                                                                    </tr>";

                        //  Table Information
                        if (is_array($companies))   {
                            foreach ($companies as $company)    {
                                $nUsers                                 = q("SELECT COUNT(id) FROM Employee WHERE company_id = '".$company[0]."' AND email != 'admin' ".
                                                                            "AND demo_user = '0' AND deleted = '0' AND id IN (SELECT user_id FROM Company_Users ".
                                                                            "WHERE company_id = '".$company[0]."')");

                                $total                                  = $nUsers * $company[2];
                                $grand_total                            += ($nUsers * $company[2]);

                                if ($company[2] != "")
                                    $rate                               = "".number_format($company[2], 2, ".", "");
                                else
                                    $rate                               = "-";

                                if ($total != 0)
                                    $total                              = "".number_format($total, 2, ".", "");
                                else
                                    $total                              = "-";

                                $string                                 .= "<tr>
                                                                                    <td>".$company[1]."</td><td class='centerdata'>".$nUsers."</td>
                                                                                    <td class='rightdata'>".$rate."</td>
                                                                                    <td class='rightdata'>".$total."</td>
                                                                                </tr>";
                                                                            
                                $exceldata[$row][]                                     = $company[1]; 
                                $exceldata[$row][]                                     = $nUsers;
                                $exceldata[$row][]                                     = $rate;    
                                $exceldata[$row][]                                     = $total;
                                    $row++;
                            }
                        }

                        ///////////////////////////
                        //  Display Information
                        if ($string != "")
                        {
                            if ($grand_total != 0)
                                $grand_total                            = "".number_format($grand_total, 2, ".", "");
                            else
                                $grand_total                            = "-";

                            $string                                     = $headers.$string."<tr>
                                                                                    <td class='on-table-total' colspan='3'>Grand Total<i>(".$_SESSION["currency"].")</i>:</td>
                                                                                    <td class='on-table-total'>".$grand_total."</td>
                                                                                </tr>";
                        }
                        
                        $exceldata[$row][]                                     = $colorClass.""; 
                        $exceldata[$row][]                                     = $colorClass.""; 
                        $exceldata[$row][]                                     = $colorClass."Grand Total: (".$_SESSION["currency"].")"; 
                        $exceldata[$row][]                                     = $colorClass.$grand_total ;
                        
                        if ($string != "")
                        {
                            echo "<div class='on-20px'><table class='on-table-center on-table'>";
                                echo "".$string;
                                echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                            echo "</table></div>";
                            echo "<br/>";

                            ///////////////////////////
                            //  Set Export Information
                            $_SESSION["fileName"] = "Company Summary Report";
                            $_SESSION["fileData"] = array_merge($excelheadings, $exceldata);
                            ///////////////////////////

                            echo "<input name='btnExport' onClick=\"location.href='_export.php'\" type='button' value='Export to Excel' />";
                        }
                        ///////////////////////////
                    ?>
                </form>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>