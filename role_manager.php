<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    //if ($_SESSION["logged_in"] === true && !($_SESSION["usertype"] < "3"))
    //    header("Location: home.php");
        
    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    if (!hasAccess("ROLE_MAN"))
        header("Location: noaccess.php");

        if($_POST['role_name'] && $_POST['role_keyword']){
            $roleName = $_POST['role_name'];
            $roleKeyword = $_POST['role_keyword'];

            $exist = q("SELECT * FROM roles WHERE role = '".$roleKeyword."' AND descr = '".$roleName."' AND companyid = '".$_SESSION["company_id"]."'");

            if (!is_array($exist)) {

                $insert = q("INSERT INTO roles (role, descr,active,companyid) VALUES ('".$roleKeyword."', '".$roleName."', '1','".$_SESSION["company_id"]."')");           // Correct Query
            
                // Add a LOG of the addition
                $what = "Role: (".$roleKeyword.") added to the Roles table";
                $byUser = $_SESSION["email"];
                $onDate = date("Y-m-d");
                $onTime = date("h:i:s");
                $compId = $_SESSION["company_id"];
            
                $insert = q("INSERT INTO Logs (what,access,on_table,by_user,on_date,on_time,company_id) VALUES ( '".$what."','Insert','Roles','".$byUser."','".$onDate."','".$onTime."','".$compId."' )");

                header("Location: action_assignment.php");
            }
            else
                $errorMessage = "Role/Role Keyword Already Used";
        }

    if ($errorMessage != "")
        echo "<p align='center' style='padding:0px;'><strong><font class='on-validate-error'>$errorMessage</font></strong></p>";

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0", "vehicles");
?>
<script language="JavaScript" src="include/validation.js"></script>
<script type="text/JavaScript">
    function validate_form( ){
        //roleDescr
        // More fields to be validated
        var description = document.assign_roles.role_name.value;
        var keyword = document.assign_roles.role_keyword.value;
        
        var valid = true;
        
        if ( description == "" ){
            ShowLayer("roleDescr", "block");
            valid = false;
        }else{
            ShowLayer("roleDescr", "none");
        }
        
        if(keyword == ""){
            ShowLayer("roleKeyW", "block");
            valid = false;
        }else{
            ShowLayer("roleKeyW", "none");
        }
        
        //Form submit function
        function submitform()
        {
            document.assign_roles.submit();
        }
        
        // Submit form if valid
        if(valid){
            submitform();   
        }
     }
</script>
<?php
    $query = q("SELECT id FROM roles WHERE companyid = '".$_SESSION["company_id"]."'");
    foreach($query as $r){
        if ($_POST["role_".$r[0].""]){  
            $set_to_inactive = q("UPDATE roles SET active='0' WHERE id='".$r[0]."' AND active='1' ");
            // Add Querys to Delte from other tables as well !!!
            $roleKeyword  = q("SELECT role FROM roles WHERE id='".$r[0]."' ");
            // Add a LOG of the addition
            $what = "Role: (".$roleKeyword.") was set to Inactive";
            $byUser = $_SESSION["email"];
            $onDate = date("Y-m-d");
            $onTime = date("h:i:s");
            $compId = $_SESSION["company_id"];
            
            $insert = q("INSERT INTO Logs (what,access,on_table,by_user,on_date,on_time,company_id) VALUES ( '".$what."','Alter','Roles','".$byUser."','".$onDate."','".$onTime."','".$compId."' )");
        }
    }
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata">
                <form action="" method="post" name="assign_roles">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Role Management</h6>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <br/>
                            </td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                Role Description:
                            </td>
                            <td width="50%">
                                <input class="on-field" name="role_name" size="30" type="text"><br/>
                                <div id="roleDescr" style="display: none;"><font class="on-validate-error">* Role description must be entered</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                Keyword (eg. SPM):
                            </td>
                            <td width="50%">
                                <input class="on-field" name="role_keyword" size="18" type="text">
                                <div id="roleKeyW" style="display: none;"><font class="on-validate-error">* Keyword must be entered</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="centerdata" colspan="2">
                                <br/>
                                <input name="btnAddRole" type="button" onclick="validate_form()" value="Add Role">
                                <br/><br/>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                Roles:
                            </td>
                            <td width="50%">
                               <!-- -->
                               <?php
                                    $query = "SELECT id,descr,role,dflt FROM roles WHERE active='1' AND companyid = '".$_SESSION["company_id"]."'";
                                    $result = q($query);
                                    ?>
                                    <table class="on-lefttable on-table">                                      
                                    <?php
                                    $counter = 0;
                                    $colorCounter = 0;
                                    foreach($result as $r){
                                      if( ($r[1] == "Admin") || ($r[1] == "admin") ){
                                            //Don't dispaly Admin
                                        }else{
                                            $disabled = ($r[3]) ? "disabled" : "";
                                            if($colorCounter == 0){
                                                echo "<tr>
                                                            <td>" . $r[1] . "</td><td><input id='role_".$r[0]."' name='role_".$r[0]."' type='submit' value='Delete' $disabled/></td>
                                                        </tr>";
                                            }else if($colorCounter == 1){
                                                echo "<tr>
                                                            <td>" . $r[1] . "</td><td><input id='role_".$r[0]."' name='role_".$r[0]."' type='submit' value='Delete' $disabled/></td>
                                                        </tr>";
                                            }
                                            $counter++;
                                            $colorCounter ++;
                                            if ($colorCounter == 2) {
                                                $colorCounter = 0;
                                            }
                                        }
                                    }

                                    ?>
                                    </table>
                               <!-- -->
                            </td>
                        </tr>
                    </table>
                    
                </form>
            </td>
        </tr>
        <tr>
            <td>
                <br/>
            </td>
        </tr>
    </table>
    
<?php
    //  Print Footer
    print_footer();
?>
