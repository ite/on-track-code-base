<?php
/*
	Title:                          On-Track Interface
	For:                            ITE
	Site:
	Author:                         DID
 	Version:                        1.0
	Date Created:			2011/03/25
	Last Modified:			2012/01/11
*/
	include("_dates.php");
	include("_menus.php");

	//////////////////////////////////////////////////
	//  Check If Date Is 2010-08-26
	function submitDateCheck($date)	{
		/*
		if ($date == "2010-08-26")  {
			global $errorMessage;
			$errorMessage = "Please note that we are currently busy with data management, no entries are allowed for 2010-08-26. Please check back in 24 Hours.";
			return false;
		}
		*/

		return true;
	}
	//////////////////////////////////////////////////

	//////////////////////////////////////////////////
	//  Print jQuery JavaScript For Dates
	function jQDate($field) {
?>
		jQuery("#<?php echo $field; ?>").DatePicker({
			format:"Y-m-d",
			date: jQuery("#<?php echo $field; ?>").val(),
			current: jQuery("#<?php echo $field; ?>").val(),
			starts: 1,
			position: "right",
			onBeforeShow: function(){
				var _date = new Date();
				jQuery("#<?php echo $field; ?>").DatePickerSetDate(_date, true);
			},
			onChange: function(formated, dates){
				jQuery("#<?php echo $field; ?>").val(formated);
			}
		});
<?php
	}
	//////////////////////////////////////////////////

	//////////////////////////////////////////////////
	//  Print jQuery JavaScript For Autocomplete Fields
	function jQAutoComplete($selector, $field, $function) {
?>
		jQuery("<?php echo $selector.$field; ?>").autocomplete("_ajax.php?func=<?php echo $function; ?>", {
			width:400,
			minChars:3,
			matchSubset:1,
			matchContains:1,
			maxItemsToShow:20,
			cacheLength:10,
			onItemSelect:selectItem
		});
<?php
	}
	//////////////////////////////////////////////////

	//////////////////////////////////////////////////
	//  Print Page Analytics
	function print_analytics() {
		global $analytics;
?>
		<script language="JavaScript" src="analytics/<?php echo $analytics; ?>"></script>
<?php
	}
	//////////////////////////////////////////////////

	function getPage() {
		$page = substr($_SERVER["REQUEST_URI"], strrpos($_SERVER["REQUEST_URI"], "/") + 1);
		$page = str_replace("%20", " ", $page);
		$page = substr($page, 0, strpos($page, ".php") + 4);

		return $page;
	}

	//////////////////////////////////////////////////
	//  HTML - Website
	//////////////////////////////////////////////////
	//  Print Page Header
	function print_header() {
?>
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html>
	<head>
		<title>On-Track</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="shortcut icon" href="images/favicon.png">
		<!--  BEGIN: STYLESHEET  -->
		<link rel="stylesheet" href="include/style.css"/>
		<link rel="stylesheet" media="screen" type="text/css" href="include/jquery-ui-1.8.16.custom.css" />
		<link rel="stylesheet" href="CSSFiles/styles.css"/>
		<!--[if lt IE 8]>
		<style type="text/css">
		.on-top{margin-bottom:-6px;}
		.on-his-pad{margin-top:3px;}
		.on-rss-pad{margin-top:-2px;}
		.on-rss-l a span{width:330px;}
		</style>
		<![endif]-->
		<!--  END: STYLESHEET  -->
		<!--  BEGIN: FUSIONCHARTS  -->
		<script language="JavaScript" src="fusion/FusionCharts.js"></script>
		<!--  END: FUSIONCHARTS  -->
		<!--  BEGIN:  ANALYTICS  -->
		<?php
			print_analytics();
		?>
		<!--  END:  ANALYTICS  -->
		<!--  BEGIN: JQUERRY  -->
		<script language="JavaScript" src="include/jquery.js"></script>
		<script language="JavaScript" src="include/jquery.marquee.js"></script>
		<script language="JavaScript" src="include/jquery.timers-1.2.js"></script>
		<script language="JavaScript" src="include/jquery-ui-1.8.16.custom.min.js"></script>
		<!-- Load the Mootools Framework -->
		<script src="include/mootools-1.2-core.js"></script>
		<script src="include/MenuMatic_0.68.3.js" type="text/javascript" charset="utf-8"></script>
		<!-- Create a MenuMatic Instance -->
		<script type="text/javascript" >
				window.addEvent('domready', function() {
					var myMenu = new MenuMatic({ orientation:'vertical' });
				});
		</script>
		<script type="Text/JavaScript">
			function json_decode(input) {
				//return JSON.parse(input, null);
				return eval( "(" + input + ")" );
			}

			function stripslashes(str)  {
				return (str + "").replace(/\\(.?)/g, function (s, n1) {
					switch (n1) {
					case "\\":
						return "\\";
					case "0":
						return "\u0000";
					case "":
						return "";
					default:
						return n1;
					}
				});
			}

			function addslashes(str)    {
				return (str + "").replace(/[\\"']/g, "\\$&").replace(/\u0000/g, "\\0");
			}

			function is_array(input)    {
				return typeof(input)=='object'&&(input instanceof Array);
			}

			var currentPage = "<?php echo getPage(); ?>";
			var negLoad;
			var settingsLoad;
			var defaultActivityTypeLoad;
			var defaultActivityLoad;
			var defaultVehicleLoad;
			var profileLoad;
			var notificationShow;
			var blogShow;
			var socialShow;
			var successMessage;
			var errorMessage;
			var modalID = "";
			var landingPage = "";
			var defaultActivityTypeID;
			var defaultActivityID;
			var defaultVehicleID;

			jQuery(function()	{
				//  Set header width equal to body table width
				jQuery("#on-headthrough").width(jQuery("#in-body").css("width"));

				//  Set head right content
				//jQuery.post("_ajax.php", {func: "getHeaderInfo"}, function(data)  {
				//    data = json_decode(data);

				//    jQuery("#on-static_User").html(data[0]);
				//    jQuery("#on-static_Company").html(data[1]);
				//    jQuery("#on-static_Project").html(data[2]);
				//});

				//  Check If Password Change Needs To Be Forced
				jQuery.post("_ajax.php", {func: "checkPassword"}, function(data)  {
					data = json_decode(data);

					if (data == 1)      {
						if (jQuery("#mask").is(":visible") == false && jQuery("#maskNotification").is(":visible") == false)     {
							jQuery("#maskNotification").css({"width":jQuery(document).width(),"height":jQuery(document).height()});
							jQuery('#maskNotification').fadeIn("fast").fadeTo("fast",0.8);   //transition effect

							//jQuery("#modal_help").show();
							var id = "#modal_password";
							//Set the popup window to center
							jQuery(id).css('top', (jQuery(window).height()/2-jQuery(id).height()/2) - jQuery(id).height()/3);
							jQuery(id).css('left', jQuery(window).width()/2-jQuery(id).width()/2);
							jQuery(id).fadeIn("fast");   //transition effect
						}
					}
				});

				//  Check If Actions Need To Be Re-Issued
				jQuery.post("_ajax.php", {func: "checkActions"}, function(data)  {});

				//  Negative Value Load
				negLoad = function()   {
					jQuery.each(jQuery("td"),function(index, value) {
						var styleStr = jQuery(value).attr("style");

						if (jQuery(value).text().indexOf("-") == 0)  {

							if (styleStr != "" && styleStr != null)     {
								if (styleStr.indexOf("color:\#FF0000\;") >= 0)  styleStr = styleStr.replace(/color:\#FF0000\;/g, "color:orange;");
								else                                            styleStr = styleStr + "color:orange;";

								jQuery(value).attr("style",styleStr);
							}

							if (""+styleStr == "undefined") {
								jQuery(value).attr("style","color:orange;");
							}
						}
					});
				};
				negLoad();

				//  Settings
				settingsLoad = function()   {
					jQuery.post("_ajax.php", {func: "getSettingItems"}, function(data)	{
						data = json_decode(data);

						var settingsItem = jQuery("#hidden_settings").children().eq(0);

						jQuery("#settings").children().not(":last").remove();
						var lastSettingsItem = jQuery("#settings").children().last();

						jQuery.each(data,function(i, v)	{
							var n = settingsItem.clone(true);

							n.attr("href", v[1]);
							n.attr("title", v[2]);

							if (v[4] == "1")    n.addClass("modal_link");

							n.children().eq(0).text(v[2]);

							n.insertBefore(lastSettingsItem);
						});

						lastSettingsItem.remove();
					});
				};

				defaultActivityTypeLoad = function()   {
					jQuery.post("_ajax.php", {func: "get_activity_types", projectType: "CP", projectID: 0}, function(data)	{
						data = json_decode(data);

						jQuery("#default_activityType option").remove();

						jQuery("#default_activityType").append(jQuery("<option></option>").attr("value","null").text("--  Select Activity Type  --"));

						if (is_array(data))     {
							jQuery.each(data,function(i, v)	{
								jQuery("#default_activityType").append(jQuery("<option></option>").attr("value",v[0]).text(v[1]));
							});
						}

						jQuery("#default_activityType").val(defaultActivityTypeID);

						defaultActivityLoad();
					});
				};

				defaultActivityLoad = function()   {
					jQuery.post("_ajax.php", {func: "get_activities", projectType: "CP", activityType: jQuery("#default_activityType").val()}, function(data)	{
						data = json_decode(data);

						jQuery("#default_activity option").remove();
						jQuery("#default_activity").append(jQuery("<option></option>").attr("value","null").text("--  Select Activity  --"));

						if (is_array(data))     {
							jQuery.each(data,function(i, v)	{
								jQuery("#default_activity").append(jQuery("<option></option>").attr("value",v[0]).text(v[1]));
							});
						}

						jQuery("#default_activity").val(defaultActivityID);
					});
				};

							defaultVehicleLoad = function()   {
								jQuery.post("_ajax.php", {func: "get_vehicle_assign", userid: "<?php echo $_SESSION["user_id"]; ?>"}, function(data)	{
									data = json_decode(data);

									jQuery("#default_vehicle option").remove();

									jQuery("#default_vehicle").append(jQuery("<option></option>").attr("value","null").text("--  Select Vehicle  --"));

									if (is_array(data))     {
										jQuery.each(data,function(i, v)	{
											jQuery("#default_vehicle").append(jQuery("<option></option>").attr("value",v[0]).text(v[1]));
										});
									}

									if (is_array(data) && data.length > 1)	jQuery("#vehicle-row").show();
									else	jQuery("#vehicle-row").hide();

									jQuery("#default_vehicle").val(defaultVehicleID);
								});
							};

				profileLoad = function()   {
					jQuery.post("_ajax.php", {func: "getProfileDetails"}, function(data)	{
						data = json_decode(data);

						jQuery("#landingPage").val(data[0]);

						defaultActivityTypeID = data[1];
						defaultActivityID = data[2];
						defaultVehicleID = data[3];

						jQuery("#current_name").val(data[4]);
						jQuery("#current_surname").val(data[5]);
						jQuery("#current_email").val(data[6]);

						defaultActivityTypeLoad();
						defaultVehicleLoad();
					});
				};

				notificationShow = function()   {
					jQuery("#maskNotification").css({"width":jQuery(document).width(),"height":jQuery(document).height()});
					jQuery('#maskNotification').fadeIn("fast").fadeTo("fast",0.8);   //transition effect

					//jQuery("#modal_help").show();
					var id = "#modal_notification";
					//Set the popup window to center
					jQuery(id).css('top',  jQuery(window).height()/2-jQuery(id).height()/2);
					jQuery(id).css('left', jQuery(window).width()/2-jQuery(id).width()/2);
					jQuery(id).fadeIn("fast");   //transition effect
					jQuery.post("_ajax.php", {func: "setSeen", video: "ctrlF5"}, function(data){
						data = json_decode(data);
					});
				};
				<?php
				if (seen($_SESSION["user_id"], "ctrlF5") != 1)   {
				?>
					if (jQuery("#mask").is(":visible") == false && jQuery("#maskNotification").is(":visible") == false)
						notificationShow();
				<?php
				}
				?>

				blogShow = function()   {
					jQuery("#mask").css({"width":jQuery(document).width(),"height":jQuery(document).height()});
					jQuery('#mask').fadeIn("fast").fadeTo("fast",0.8);   //transition effect

					//jQuery("#modal_help").show();
					var id = "#modal_blog";
					//Set the popup window to center
					jQuery(id).css('top', (jQuery(window).height()/2-jQuery(id).height()/2) - jQuery(id).height()/3);
					jQuery(id).css('left', jQuery(window).width()/2-jQuery(id).width()/2);
					jQuery(id).fadeIn("fast");   //transition effect
					jQuery.post("_ajax.php", {func: "setSeen", video: "blog"}, function(data){
						data = json_decode(data);
					});
				};
				<?php
				if (seen($_SESSION["user_id"], "blog") != 1)   {
				?>
					if (jQuery("#mask").is(":visible") == false && jQuery("#maskNotification").is(":visible") == false)
						blogShow();
				<?php
				}
				?>

				socialShow = function()   {
					jQuery("#maskNotification").css({"width":jQuery(document).width(),"height":jQuery(document).height()});
					jQuery('#maskNotification').fadeIn("fast").fadeTo("fast",0.8);   //transition effect

					//jQuery("#modal_help").show();
					var id = "#modal_social";
					//Set the popup window to center
					jQuery(id).css('top', (jQuery(window).height()/2-jQuery(id).height()/2) - jQuery(id).height()/3);
					jQuery(id).css('left', jQuery(window).width()/2-jQuery(id).width()/2);
					jQuery(id).fadeIn("fast");   //transition effect
				};
				<?php
				if (seen($_SESSION["user_id"], "social") != 1)   {
				?>
					if (jQuery("#mask").is(":visible") == false && jQuery("#maskNotification").is(":visible") == false)
						socialShow();
				<?php
				}
				?>

				//  Successful Modal Alerts
				successMessage = function(message)   {
					if (modalID != "")  jQuery(modalID).hide();

					jQuery("#mask").css({"width":jQuery(document).width(),"height":jQuery(document).height()});
					jQuery('#mask').fadeIn("fast").fadeTo("fast",0.8);   //transition effect

					var id = "#modal_success";
					jQuery("#successMessage").html(message);
					//Set the popup window to center
					jQuery(id).css('top', (jQuery(window).height()/2-jQuery(id).height()/2) - jQuery(id).height()/3);
					jQuery(id).css('left', jQuery(window).width()/2-jQuery(id).width()/2);
					jQuery(id).fadeIn("fast");   //transition effect
				};

				jQuery("#closeSuccess, #closeBlog").click(function()	{
					jQuery("#mask").hide();
					jQuery("#maskNotification").hide();
					jQuery("#sendEmail").show();
					jQuery(".on-profile-modal").hide();
					jQuery('.on-drop-menu').hide();
					jQuery('.on-contact-modal').hide();

					if (landingPage != "")
						window.location.href = landingPage;
				});

				//  Error Modal Alerts
				errorMessage = function(message)   {
					if (modalID != "")  jQuery(modalID).hide();

					jQuery("#mask").css({"width":jQuery(document).width(),"height":jQuery(document).height()});
					jQuery('#mask').fadeIn("fast").fadeTo("fast",0.8);   //transition effect

					var id = "#modal_error";
					jQuery("#errorMessage").html(message);
					//Set the popup window to center
					jQuery(id).css('top', (jQuery(window).height()/2-jQuery(id).height()/2) - jQuery(id).height()/3);
					jQuery(id).css('left', jQuery(window).width()/2-jQuery(id).width()/2);
					jQuery(id).fadeIn("fast");   //transition effect
				};

				jQuery("#closeError").click(function()	{
					jQuery("#modal_error").hide();
					jQuery("#sendEmail").show();

					if (modalID != "")  jQuery(modalID).show();
				});

				jQuery("#closeSocial").click(function()	{
					jQuery.post("_ajax.php", {func: "setSeen", video: "social"}, function(data){
						data = json_decode(data);
					});
					jQuery(".on-profile-modal").hide();
					jQuery('.on-drop-menu').hide();
					jQuery('.on-contact-modal').hide();
					jQuery('#maskNotification').hide();
				});

				jQuery("#default_activityType").change(function()	{
					defaultActivityLoad();
				});

				jQuery("#modal_profile #chngBDBtn").click(function()	{
					modalID = "#modal_profile";

					jQuery(modalID).hide();

					landingPage = jQuery("#landingPage").val();

					var activityType = jQuery("#default_activityType").val();
					var activity = jQuery("#default_activity").val();
					var vehicle = jQuery("#default_vehicle").val();
					var frstname = jQuery("#current_name").val();
					var lstname = jQuery("#current_surname").val();
					var email = jQuery("#current_email").val();

					var doTest = 0;

					if (email != "admin")
						doTest = 1;

					var valid = 1;

					if (doTest) {
						var exp = /^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/;

						valid = exp.test(email);
					}

					if (valid)  {
						jQuery.post("_ajax.php", {func: "updateDetails", landingPage: landingPage, activityType: activityType, activity: activity, vehicle: vehicle, frstname: frstname, lstname: lstname, email: email}, function(data){
							data = json_decode(data);

							if (data[0] == "SUCCESS")       successMessage(data[1]);
							if (data[0] == "ERROR")         errorMessage(data[1]);
						});
					} else      errorMessage("Please enter a valid email address.");
				});

				jQuery("#modal_profile #chngPWBtn").click(function()	{
					modalID = "#modal_profile";

					jQuery(modalID).hide();

					var currentPW = jQuery("#current_password").val();
					var newPW = jQuery("#new_password").val();
					var confirmPW = jQuery("#confirm_password").val();

					if (currentPW != "" && newPW != "" && confirmPW != "")       {
						if (newPW == confirmPW)   {
							if (newPW != "password")    {
								jQuery.post("_ajax.php", {func: "updatePassword", currentPW: currentPW, newPW: newPW}, function(data){
									data = json_decode(data);

									if (data[0] == "SUCCESS")       {
										successMessage(data[1]);

										jQuery("#current_password").val("");
										jQuery("#new_password").val("");
										jQuery("#confirm_password").val("");
									}
									if (data[0] == "ERROR")         errorMessage(data[1]);
								});
							} else      {
								jQuery("#new_password").val("");
								jQuery("#confirm_password").val("");

								errorMessage("Password as 'password' not allowed, please choose another password...");
							}
						} else  errorMessage("New password & confirmation password need to match...");
					} else      errorMessage("Please enter all password fields...");
				});

				jQuery("#modal_password #chngPWBtnFrc").click(function()	{
					modalID = "#modal_password";

					jQuery(modalID).hide();

					var newPW = jQuery("#newPW").val();
					var confirmPW = jQuery("#confirmPW").val();

					if (newPW != "" && confirmPW != "")       {
						if (newPW == confirmPW)   {
							if (newPW != "password")    {
								jQuery.post("_ajax.php", {func: "updatePassword", currentPW: "password", newPW: newPW}, function(data){
									data = json_decode(data);

									if (data[0] == "SUCCESS")       {
										successMessage(data[1]);

										jQuery("#newPW").val("");
										jQuery("#confirmPW").val("");
									}
									if (data[0] == "ERROR")         errorMessage(data[1]);
								});
							} else      {
								jQuery("#newPW").val("");
								jQuery("#confirmPW").val("");

								errorMessage("Password as 'password' not allowed, please choose another password...");
							}
						} else  errorMessage("New password & confirmation password need to match...");
					} else      errorMessage("Please enter all password fields...");
				});

				//  Help Clicked
				jQuery(".on-help").click(function()  {
					jQuery.post("_ajax.php", {func: "getHelp", page: currentPage}, function(data){
						data = json_decode(data);

						if (is_array(data))     {
							jQuery("#help").html("<center><h1>"+data[0][0]+"</h1></center><br/><table><tr><td class='negative' align='justify'>"+data[0][1]+"</td></tr></table>");
							jQuery(".on-help-menu").toggle();
							if (jQuery("#mask").is(":visible") == false)        {
								jQuery('#mask').css({'width':jQuery(document).width(),'height':jQuery(document).height()});  //Set heigth and width to mask to fill up the whole screen
								jQuery('#mask').fadeIn("fast").fadeTo("fast",0.8);   //transition effect
							} else jQuery("#mask").hide();
						}
						else    {
							window.location.href = "mailto:support@integrityengineering.co.za?Subject=On-Track%20Support";
//							jQuery('#mask').css({'width':jQuery(document).width(),'height':jQuery(document).height()});  //Set heigth and width to mask to fill up the whole screen
//							jQuery('#mask').fadeIn("fast").fadeTo("fast",0.8);   //transition effect
//							//jQuery("#modal_help").show();
//							var id = "#modal_help";
//							//Set the popup window to center
//							jQuery(id).css('top',  jQuery(window).height()/2-jQuery(id).height()/2);
//							jQuery(id).css('left', jQuery(window).width()/2-jQuery(id).width()/2);
//							jQuery(id).fadeIn("fast");   //transition effect
						}
					});
				});

				//  Mail Clicked
				jQuery(".mail").click(function()  {
					jQuery('#mask').css({'width':jQuery(document).width(),'height':jQuery(document).height()});  //Set heigth and width to mask to fill up the whole screen
					jQuery('#mask').fadeIn("fast").fadeTo("fast",0.8);   //transition effect
					//jQuery("#modal_help").show();
					var id = "#modal_help";
					//Set the popup window to center
					jQuery(id).css('top',  jQuery(window).height()/2-jQuery(id).height()/2);
					jQuery(id).css('left', jQuery(window).width()/2-jQuery(id).width()/2);
					jQuery(id).fadeIn("fast");   //transition effect
				});

				//  Mail Button Clicked
//				jQuery(".on-but-mail").click(function()  {
//					jQuery("#mask").css({"width":jQuery(document).width(),"height":jQuery(document).height()});
//					jQuery('#mask').fadeIn("fast").fadeTo("fast",0.8);   //transition effect
//					//jQuery("#modal_help").show();
//					var id = "#modal_help";
//					//Set the popup window to center
//					jQuery(id).css('top',  jQuery(window).height()/2-jQuery(id).height()/2);
//					jQuery(id).css('left', jQuery(window).width()/2-jQuery(id).width()/2);
//					jQuery(id).fadeIn("fast");   //transition effect
//				});

				//  Settings Click
				jQuery("#on-con-set").click(function()   {
					settingsLoad();
					jQuery("#mask").css({"width":jQuery(document).width(),"height":jQuery(document).height()});
					jQuery('#mask').fadeIn("fast").fadeTo("fast",0.8);   //transition effect
					jQuery(".on-drop-menu").toggle();
				});

				jQuery(".settings").click(function()   {
					var action = jQuery(this).attr("href");
					var cls = jQuery(this).attr("class");

					if (cls.indexOf("modal_link") == -1)       {
						jQuery("#menus_form").attr("action", action);
						jQuery("#menus_form").submit();
					} else      {
						profileLoad();

						jQuery(".on-profile-modal").hide();
						jQuery('.on-drop-menu').hide();
						jQuery('.on-contact-modal').hide();

						jQuery('#mask').css({'width':jQuery(document).width(),'height':jQuery(document).height()});  //Set heigth and width to mask to fill up the whole screen
						jQuery('#mask').fadeIn("fast").fadeTo("fast",0.8);   //transition effect
						var id = "#"+action;
						//Set the popup window to center
						jQuery(id).css('top',  jQuery(window).height()/2-jQuery(id).height()/2);
						jQuery(id).css('left', jQuery(window).width()/2-jQuery(id).width()/2);
						jQuery(id).fadeIn("fast");   //transition effect
					}
				});

				jQuery("#mask").click(function()	{
					if (jQuery("#modal_success").is(":visible") && landingPage != "")        {
						window.location.href = landingPage;
					} else      {
						jQuery(this).hide();
						jQuery("#sendEmail").show();
						jQuery(".on-help-menu").hide();
						jQuery(".on-profile-modal").hide();
						jQuery('.on-drop-menu').hide();
						jQuery('.on-contact-modal').hide();
					}
				});

				//  Home Navigation
				jQuery(".on-home-nav").click(function()   {
					jQuery("#menus_form").attr("action", "home.php");
					jQuery("#menus_form").submit();
				});

				//  Send Mail
				jQuery("#sendEmail").click(function()   {
					modalID = "#modal_help";
					jQuery("#modal_help").hide();

					<?php
					//global $serverName;

					//if ($serverName == "demo.on-track.ws" || $serverName == "localhost")	{
					?>
					if ((jQuery("#contact_from").val() != "") && (jQuery("#contact_subject").val() != "") && (jQuery("#contact_description").val() != ""))   {
						var from = jQuery("#contact_from").val();
						var subject = jQuery("#contact_subject").val();
						var desc = jQuery("#contact_description").val();

						var exp = /^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/;

						if (exp.test(from))	{
							jQuery.post("_ajax.php", {func: "sendEmail", from: from, subject: subject, desc: desc}, function(data){
								successMessage("Thank you for your feedback.<br><br>You will be contacted via email within the next business day.");
								jQuery("#contact_subject").val("");
								jQuery("#contact_description").val("");
								//jQuery("#modal_help").show();
							});
						} else
							errorMessage("Please enter a valid email address.");
					}
					else{
						errorMessage("Please fill out the entire form");
						//jQuery("#modal_help").show();
					}
					<?php
					/*}	else	{
					?>
					if ((jQuery("#contact_subject").val() != "") && (jQuery("#contact_description").val() != ""))   {
						jQuery.post("_ajax.php", {func: "sendEmail", subject: jQuery("#contact_subject").val(), desc: jQuery("#contact_description").val()}, function(data){
							successMessage("Thank you for your feedback.<br><br>You will be contacted via email within the next business day.");
							jQuery("#contact_subject").val("");
							jQuery("#contact_description").val("");
							//jQuery("#modal_help").show();
						});
					}
					else{
						errorMessage("Please fill out the entire form");
						//jQuery("#modal_help").show();
					}
					<?php
					}*/
					?>
				});

				// Cancel Mail
				jQuery("#cancelEmail").click(function()   {
					jQuery("#mask").hide();
					jQuery("#sendEmail").show();
					jQuery(".on-help-menu").hide();
					jQuery(".on-profile-modal").hide();
					jQuery('.on-drop-menu').hide();
					jQuery('.on-contact-modal').hide();
					jQuery("#contact_subject").val("");
					jQuery("#contact_description").val("");
				});

				//  Menu Item Click
				jQuery(".on-but").click(function()   {
					if (jQuery(this).attr("name") == "modal_help")   {
						window.location.href = "mailto:support@integrityengineering.co.za?Subject=On-Track%20Support";

//						jQuery("#mask").css({"width":jQuery(document).width(),"height":jQuery(document).height()});
//						jQuery('#mask').fadeIn("fast").fadeTo("fast",0.8);   //transition effect
//
//						//jQuery("#modal_help").show();
//						var id = "#modal_help";
//						//Set the popup window to center
//						jQuery(id).css('top',  jQuery(window).height()/2-jQuery(id).height()/2);
//						jQuery(id).css('left', jQuery(window).width()/2-jQuery(id).width()/2);
//						jQuery(id).fadeIn("fast");   //transition effect
					}
				});

				/* Toggel coming soon */
				jQuery('.coming-soon').hover(function() {
				   jQuery("#coming-soon").toggle();
				});

				jQuery('#marqueeNews').marquee({
					speed : 5,
					coefAcc : 2,
					enableCookie : true,
					enableScroll : false,
					stepScroll : 30,
					enableAnimateScroll : true,
					animateScrollDuration : 250,
					animateScrollEasing : 'linear'
				}).mouseover(function () {
					jQuery(this).trigger('stop');
				}).mouseout(function () {
					jQuery(this).trigger('start');
				}).mousemove(function (event) {
					if (jQuery(this).data('drag') == true) {
						this.scrollLeft = jQuery(this).data('scrollX') + (jQuery(this).data('x') - event.clientX);
					}
				}).mousedown(function (event) {
					jQuery(this).data('drag', true).data('x', event.clientX).data('scrollX', this.scrollLeft);
				}).mouseup(function () {
					jQuery(this).data('drag', false);
				});
			});
		</script>
	<!--  END: JQUERRY  -->
	</head>
<?php
	}

	//////////////////////////////////////////////////
	//  Print Page Menus
	function print_menus() {
?>
	<body>
		<div id="mask"></div>
		<div id="maskNotification"></div>
		<div class="on-headthrough" id="on-headthrough"></div>
		<table class="in-header">
			<tr>
				<td rowspan="2" class="in-head-left"><div class="on-nameplate"><div class="on-logo"></div></div></td>
				<td class="in-head-right">
					<div class="on-header">
						<!--  BEGIN: RSS VIEWER
						<div class="on-rss">
							<div class="on-rss-l"><a title="News..."><span><div class="on-rss-pad"><marquee id="rss_feeder" scrollamount="3"></marquee></div></span></a></div><div class="on-rss-r"></div>
						</div>
						END: RSS VIEWER  -->
						<!--  BEGIN: RSS VIEWER  -->
						<div class="on-rss">
							<div class="on-rss-l">
								<a title="News...">
									<span>
										<div id="marqueeNews">
											<div class="on-rss-pad" id="rss_feeder">
											<?php
												$tweet = q("SELECT descr,date FROM TwitterPosts ORDER BY date DESC LIMIT 1");

												echo "<font style='color:#e6e6e6;'>".time_since($tweet[0][1])."</font><font style='color:orange;'> - ".$tweet[0][0]."</font>";
											?>
											</div>
										</div>
									</span>
								</a>
							</div>
							<div class="on-rss-r"></div>
						</div>
						<!--  END: RSS VIEWER  -->
						<div class="on-con">
							<a class="on-con-set" id="on-con-set" name="on-con-set" title="Settings"></a>
							<div class="on-con-sep" id="on-con-sep" name="on-con-sep"></div>
							<a href="logout.php" class="on-con-log" id="on-con-log" name="on-con-log"title="Log Off"></a>
						</div>
						<div class="on-user">
							<div class="on-user-left">
								<div class="on-user-box">
									<div class="on-user-icon"></div>
									<div class="on-user-data" id="on-user-data" name="on-user-data">
										<table class="on-user-vert" id="" name="">
											<tr><td class="on-static">User:</td><td><a id="on-static_User"><?php echo (isset($_SESSION["user"])) ? $_SESSION["user"] : ""; ?></a></td></tr>
											<tr><td class="on-static">Company:</td><td><a id="on-static_Company"><?php echo (isset($_SESSION["company_name"])) ? $_SESSION["company_name"] : "None Selected"; ?></a></td></tr>
											<!--<tr><td class="on-static">Project:</td><td><a id="on-static_Project"></a></td></tr>-->
										</table>
									</div>
								</div>
							</div><div class="on-user-right"></div>
						</div>
					</div>
					<!--  BEGIN: SETTINGS MENU  -->
					<div class="on-drop-menu">
						<ul>
							<div id="settings" name="settings">
								<li class="settings" id="" name="" href="" title=""><a></a></li>
							</div>
							<li class="settings" style="padding:0; margin:0;" id="" name="" href="" title=""><hr></li>
							<li class="settings" id="Intro video" name="Intro video" href="videoIntro.php" title="Intro video"><a>Intro video</a></li>
							<li class="settings" id="Tutorial video" name="Tutorial video" href="videoTut.php" title="Tutorial video"><a>Tutorial video</a></li>
							<div id="hidden_settings" name="hidden_settings" style="display:none;">
								<li class="settings" id="" name="" href="" title=""><a></a></li>
							</div>
						</ul>
					</div>
					<!--  END: SETTINGS MENU  -->
					<!--  BEGIN: CONTACT ITE MODAL  -->
					<div id="modal_help" name="modal_help" class="on-contact-modal">
						<table>
							<tr>
								<td colspan="100%" class='centerdata'><a>Help &amp; Support</a><br><br></td>
							</tr>
							<?php
								//global $serverName;

								//if ($serverName == "demo.on-track.ws" || $serverName == "localhost")	{
							?>
							<tr>
								<td class="on-description">From:</td>
								<td><input id="contact_from" name="contact_from" class="on-field" type="text" /></td>
							</tr>
							<?php
								//}
							?>
							<tr>
								<td class="on-description">Subject:</td>
								<td><input id="contact_subject" name="contact_subject" class="on-field" type="text" /></td>
							</tr>
							<tr>
								<td class="on-description">Desciption:</td>
								<td><textarea id="contact_description" name="contact_description" cols="50" rows="6" class="on-field maxLength" ></textarea></td>
							</tr>
							<tr>
								<td></td>
								<td>
									<input id="sendEmail" name="sendEmail" type="button" value="Send" />
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<input id="cancelEmail" name="cancelEmail" type="button" value="Cancel" />
								</td>
							</tr>
						</table>
					</div>
					<!--  END: CONTACT ITE MODAL  -->
					<!-- BEGIN: ERROR MODAL -->
					<div id="modal_error" name="modal_error" class="on-contact-modal">
						<table>
							<tr>
								<td colspan="100%" class="centerdata">
									<img src="images/logo.png" />
									<br/><br/>
									<strong style="color:orange;font-weight:bold;font-size:18px;font-variant:small-caps;">
										Request Unsuccessful
									</strong>
									<br/><br/>
									<table width="300px">
										<tr>
											<td>
												<center id="errorMessage" name="errorMessage" style="font-weight:bold;font-size:14px;color:#d2d2d2;">
												</center>
											</td>
										</tr>
									</table>
									<br/>
									<input id="closeError" name="closeError" type="button" value="Close" />
								</td>
							</tr>
						</table>
					</div>
					<!--  END: ERROR MODAL  -->
					<!-- BEGIN: SUCCESS MODAL -->
					<div id="modal_success" name="modal_success" class="on-contact-modal">
						<table>
							<tr>
								<td colspan="100%" class="centerdata">
									<img src="images/logo.png" />
									<br/><br/>
									<strong style="color:#028200;font-weight:bold;font-size:18px;font-variant:small-caps;">
										Request Successfully Submitted
									</strong>
									<br/><br/>
									<table width="300px">
										<tr>
											<td>
												<center id="successMessage" name="successMessage" style="font-weight:bold;font-size:14px;color:#d2d2d2;">
												</center>
											</td>
										</tr>
									</table>
									<br/>
									<input id="closeSuccess" name="closeSuccess" type="button" value="Close" />
								</td>
							</tr>
						</table>
					</div>
					<!--  END: SUCCESS MODAL  -->
					<!-- BEGIN: PASSWORD CHANGE MODAL -->
					<div id="modal_password" name="modal_password" class="on-contact-modal">
						<table width="100%">
							<tr>
								<td colspan="100%" class="centerdata">
									<table width="100%">
										<tr>
											<td class="centerdata" colspan="100%">
												<strong style="color:orange;font-weight:bold;font-size:18px;font-variant:small-caps;">
													Change Password
												</strong>
											</td>
										</tr>
										<tr>
											<td class="on-description centerdata" colspan="100%">
												Please note that your password is still 'password'.  This is a security risk. Please change your password now.
												<br/><br/>
												<hr/>
											</td>
										</tr>
										<tr>
											<td class="on-description" width="50%">
												New Password:
											</td>
											<td width="50%">
												<input class="on-field" id="newPW" name="newPW" tabindex="1" type="password" value="">
											</td>
										</tr>
										<tr>
											<td class="on-description" width="50%">
												Confirm Password:
											</td>
											<td width="50%">
												<input class="on-field" id="confirmPW" name="confirmPW" tabindex="1" type="password" value="">
											</td>
										</tr>
									</table>
									<br/>
									<input id="chngPWBtnFrc" name="chngPWBtnFrc" tabindex="1" type="button" value="Change Password">
								</td>
							</tr>
						</table>
					</div>
					<!-- END: PASSWORD CHANGE MODAL -->
					<!-- BEGIN: PROFILE MODAL -->
					<div id="modal_profile" name="modal_profile" class="on-profile-modal">
						<table width="100%">
							<tr>
								<td colspan="100%" class="centerdata">
									<table width="100%">
										<tr>
											<td class="centerdata" colspan="100%">
												<h6>Landing Page</h6><br/>
											</td>
										</tr>
										<tr>
											<td class="on-description" width="50%">
												Default Landing Page:
											</td>
											<td width="50%">
												<select id="landingPage" name="landingPage" method="post" class="on-field required" tabindex="1">
													<option value="book_time.php">Book Time</option>
													<option value="book_expense.php">Book Expenses</option>
													<option value="bookings.php">Book Time &amp; Expenses</option>
										<?php
											$gpsLogbook = q("SELECT gpsLogbook FROM Company WHERE id = '".$_SESSION["company_id"]."'");

											if ($gpsLogbook == "1" && hasAccess("GPS_LOGBOOK_MANAGEMENT"))	{
										?>
													<option value="gpslogbook_trip_management.php">GPS Logbook Trip Management</option>
										<?php
											}
										?>
												</select>
											</td>
										</tr>
									</table>
									<br/><br/>
									<table width="100%">
										<tr>
											<td class="centerdata" colspan="100%">
												<h6>Default Activity Type & Acivity</h6><br/>
											</td>
										</tr>
										<tr>
											<td class="on-description" width="50%">
												Default Activity Type:
											</td>
											<td width="50%">
												<select id="default_activityType" name="default_activityType" method="post" class="on-field" tabindex="1">
												</select>
											</td>
										</tr>
										<tr>
											<td class="on-description" width="50%">
												Default Activity:
											</td>
											<td width="50%">
												<select id="default_activity" name="default_activity" method="post" class="on-field" tabindex="1">
												</select>
											</td>
										</tr>
										<tr id="vehicle-row">
											<td class="on-description" width="50%">
												Default Vehicle:
											</td>
											<td width="50%">
												<select id="default_vehicle" name="default_vehicle" method="post" class="on-field" tabindex="1">
												</select>
											</td>
										</tr>
									</table>
									<br/><br/>
									<table width="100%">
										<tr>
											<td class="centerdata" colspan="100%">
												<h6>Basic Details</h6><br/>
											</td>
										</tr>
										<tr>
											<td class="on-description" width="50%">
												Name:
											</td>
											<td width="50%">
												<input class="on-field" id="current_name" name="current_name" tabindex="1" type="text" value="">
											</td>
										</tr>
										<tr>
											<td class="on-description" width="50%">
												Surname:
											</td>
											<td width="50%">
												<input class="on-field" id="current_surname" name="current_surname" tabindex="1" type="text" value="">
											</td>
										</tr>
										<tr>
											<td class="on-description" width="50%">
												Email:
											</td>
											<td width="50%">
												<input class="on-field" id="current_email" name="current_email" tabindex="1" type="text" value="">
											</td>
										</tr>
									</table>
									<br/>
									<input id="chngBDBtn" name="chngBDBtn" tabindex="1" type="button" value="Update Details">
									<br/><br/>
									<hr>
									<br/>
									<table width="100%">
										<tr>
											<td class="centerdata" colspan="100%">
												<h6>Change Password</h6><br/>
											</td>
										</tr>
										<tr>
											<td class="on-description" width="50%">
												Current Password:
											</td>
											<td width="50%">
												<input class="on-field" id="current_password" name="current_password" tabindex="1" type="password" value="">
											</td>
										</tr>
										<tr>
											<td class="on-description" width="50%">
												New Password:
											</td>
											<td width="50%">
												<input class="on-field" id="new_password" name="new_password" tabindex="1" type="password" value="">
											</td>
										</tr>
										<tr>
											<td class="on-description" width="50%">
												Confirm Password:
											</td>
											<td width="50%">
												<input class="on-field" id="confirm_password" name="confirm_password" tabindex="1" type="password" value="">
											</td>
										</tr>
									</table>
									<br/>
									<input id="chngPWBtn" name="chngPWBtn" tabindex="1" type="button" value="Change Password">
								</td>
							</tr>
						</table>
					</div>
					<!-- END: PROFILE MODAL -->
					<!--  BEGIN: NOTIFICATION MODAL  -->
					<div id="modal_notification" name="modal_notification" class="on-contact-modal">
						<table>
							<tr>
								<td colspan="100%" class="centerdata">
									<strong style="color:orange;font-weight:bold;font-size:22px;font-variant:small-caps;">
										Dear all,<br/><br/>
										Please press Ctrl+F5 as new features have been added.<br/>
										Please check out the news feed for information.
									</strong>
								</td>
							</tr>
						</table>
					</div>
					<!-- END: NOTIFICATION MODAL -->
					<!--  BEGIN: BLOG MODAL  -->
					<div id="modal_blog" name="modal_blog" class="on-contact-modal">
						<table>
							<tr>
								<td colspan="100%" class="centerdata">
									<img src="images/logo.png" />
									<br/><br/>
									<strong style="font-weight:bold;font-size:14px;color:#d2d2d2;">
										Please note that new features were added to On-Track.<br/>
										Check it out here: <a href="http://iteontrack.blogspot.com/" target="_blank">http://iteontrack.blogspot.com/</a>
									</strong>
									<br/><br/>
									<input id="closeBlog" name="closeBlog" type="button" value="Close" />
								</td>
							</tr>
						</table>
					</div>
					<!-- END: BLOG MODAL -->
					<!--  BEGIN: SOCIAL MEDIA MODAL  -->
					<div id="modal_social" name="modal_social" class="on-contact-modal">
						<table>
							<tr>
								<td colspan="100%" class="centerdata">
									<img src="images/logo.png" />
									<br/><br/>
									<strong style="color:orange;font-weight:bold;font-size:22px;font-variant:small-caps;">
										On-Track Goes Social!!
									</strong>
									<br/><br/>
									<table width="300px">
										<tr>
											<td colspan="2">
												<center style="color:orange;font-weight:bold;font-size:18px;font-variant:small-caps;">
													For the latest news & updates,<br/>click on the links below:<br/><br/>
												</center>
											</td>
										</tr>
										<tr>
											<td align="center" width="100px">
												<center>
													<a href="http://twitter.com/#!/ite_ontrack" title="twitter" target="_blank">
														<img src="images/on-but-tw.png" />
													</a>
												</center>
												<center style="color:orange;font-weight:bold;font-size:14px;font-variant:small-caps;">`Follow` us on Twitter!</center>
											</td>
											<td align="center" width="100px">
												<center>
													<a href="https://www.facebook.com/pages/On-Track-Business-Management/274662732552676?ref=ts" title="facebook" target="_blank">
														<img src="images/on-but-fb.png" />
													</a>
												</center>
												<center style="color:orange;font-weight:bold;font-size:14px;font-variant:small-caps;">`Like` us on Facebook!</center>
											</td>
										</tr>
									</table>
									<br/>
									<center><input id="closeSocial" name="closeSocial" type="button" value="Close" /></center>
								</td>
							</tr>
						</table>
					</div>
					<!--  END: SOCIAL MEDIA MODAL  -->
				</td>
			</tr>
			<tr>
				<td class="in-his">
					<ul class="on-his">
						<!--  BEGIN: NAVIGATION/HISTORY  -->
						<div id="navigation" name="navigation" class="on-his-pad-64">
							<li class="on-his-1"><a id="" name="" title=""><span><div class="on-his-pad"></div></span></a></li>
						</div>
						<div id="hidden_navigation" name="hidden_navigation" style="display:none;">
							<li class="on-his-1"><a id="" name="" title=""><span><div class="on-his-pad"></div></span></a></li>
						</div>
						<!--  END: NAVIGATION/HISTORY  -->
						<!--  BEGIN: HELP!  -->
						<div class="on-help"><a class="on-help-64" title="Help!"></a></div>
						<div class="on-help-menu on-help-menu64">
							<div id="help" name="help">
							</div>
						</div>
						<!--  END: HELP!  -->
					</ul>
				</td>
			</tr>
		</table>
		<table class="in-body" id="in-body">
			<tr>
				<td class="in-nav">
					<!--  BEGIN: NAVIGATION MENU  -->
					<div class="on-nav">
						<div class="on-nav-con">
							<div class="on-top"></div>
							<div class="on-mid">
								<form id="menus_form" name="menus_form" action="" method="post">
									<div class="on-con-nav" name="on-con-nav" >
										<a class="on-left-nav" id="on-left-nav" name="on-left-nav" title="Back"><span></span></a>
										<a class="on-home-nav" id="on-home-nav" name="on-home-nav" title="Home"><span></span></a>
										<a class="on-right-nav" id="on-right-nav" name="on-right-nav" title="Forward"><span></span></a>
									</div>
									<?php
										getMenus($_SESSION["company_id"]);
									?>
								</form>
							</div>
							<!--
							<iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.facebook.com%2Fpages%2FOn-Track-Business-Management%2F274662732552676&amp;send=false&amp;layout=button_count&amp;width=450&amp;show_faces=false&amp;action=like&amp;colorscheme=dark&amp;font=tahoma&amp;height=21"
								scrolling="no" frameborder="0" style="padding:10px 37px 37px 37px; border:none; overflow:hidden; width:80px; height:21px; float:left;" allowTransparency="true"></iframe>
							-->
							<div class="on-bot"></div>
						</div>
					</div>
					<!--  END: NAVIGATION MENU  -->
				</td>
				<td class="in-container">
					<div class="on-container">
<?php
	}
	//////////////////////////////////////////////////
	//  Print Page Footer
	function print_footer() {
?>
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="in-foot">
					<!--  BEGIN: FOOTER  -->
					<div class="on-footer">
						<div class="on-emo">
							<div id="coming-soon"><p>coming soon</p></div>
								<a class="on-but-mail on-emost" name="on-but-mail" title="mail" href="mailto:support@integrityengineering.co.za?Subject=On-Track%20Support"></a>
								<a class="on-but-fb on-emost" name="on-but-fb" href="https://www.facebook.com/pages/On-Track-Business-Management/274662732552676?ref=ts" title="facebook" target="_blank"></a>
								<a class="on-but-tw on-emost" name="on-but-tw" href="http://twitter.com/#!/ite_ontrack" title="twitter" target="_blank"></a>
								<a class="on-but-yt on-emost" name="on-but-yt" href="http://www.youtube.com/user/Integrityengeneering" title="youtube" target="_blank"></a>
								<a class="coming-soon on-but-rss on-emost" name="on-but-rss" title="rss"></a>
						</div>
					</div>
					<!--  END: FOOTER  -->
				</td>
			</tr>
		</table>
	</body>
</html>
<?php
	}
	//////////////////////////////////////////////////

	//////////////////////////////////////////////////
	//  HTML - Email
	//////////////////////////////////////////////////
	//  Print Email Header
	function print_email_header($title = "Notification")       {
		global $urlLink;

		$header =
		"<html>\n".
			"<head>\n".
				"<title>On-Track: ".$title."</title>\n".
				"<style>\n".
					"<!--\n".
					"body {\n".
						"width:100%;\n".
						"min-width:1000px;\n".
						"height:100%;\n".
						"margin:0;\n".
						"padding:0;\n".
						"font-size:12px;\n".
						"font-family:Meriad,Verdana,Arial,Helvetica,sans-serif;\n".
					"}\n".
					"a {color:#A56C02;text-decoration:none;}\n".
					"-->\n".
				"</style>\n".
			"</head>\n".
			"<body style='background-image:url(".$urlLink."/CSSFiles/im/bg.jpg) repeat #000;'>\n".
				"<center>\n".
					"<div style='background-image:url(".$urlLink."/CSSFiles/im/statusBG.png);'>\n".
						"<br>\n".
						"<img src ='".$urlLink."/CSSFiles/im/logo.png'/>\n".
						"<div style='text-align:justify;line-height:20px;width:800px;'>\n";

		return $header;
	}
	//////////////////////////////////////////////////
	//  Print Email Footer
	function print_email_footer()       {
		global $urlLink;

		$footer =
						"</div>\n".
					"</div>\n".
					"<div style='height:62px; background-image:url(".$urlLink."/CSSFiles/im/statusFooter.png);'>\n".
						"<br>\n".
					"</div>\n".
				"</center>\n".
			"</body>\n".
		"</html>";

		return $footer;
	}
	//////////////////////////////////////////////////
?>
