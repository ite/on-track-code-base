<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

	if (!hasAccess("BUDGET_CAT_MANAGE"))
		header("Location: noaccess.php");

    //  Update Category And Elements Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")        {
        $errorMessage = "";

        //  Get Information
        $id = $_GET["id"];
        $element_info_old = q("SELECT name FROM Elements WHERE id = '$id'");
        $element_info_new = addslashes(strip_tags($_POST["element_name"]));

        //  nKeywords                                                  = Number of Keywords per Element
        $nKeywords = q("SELECT COUNT(id) FROM Keywords WHERE parent_id = '$id'");
        $keywords = q("SELECT id, keyword, status FROM Keywords WHERE parent_id = '$id' ORDER BY keyword");

        //  Check If Exists In Database
        if ($element_info_old != $element_info_new)   {
            $exist = q("SELECT id FROM Elements WHERE name = '$element_info_new' AND parent_id = '".$id."'");
            $do_update = true;
        }
        else    {
            $exist = false;
            $do_update = false;
        }
        if (!$exist)    {
            //  Update Element If Necessary
            if ($do_update)
                $update = q("UPDATE Elements SET name = '$element_info_new' WHERE id = '$id'");

            //  Keywords
            if ($nKeywords >= 1) {
                foreach ($keywords as $keyword) {
                    $keyword_name_old = $keyword[1];
                    $keyword_name_new = addslashes(strip_tags($_POST["keyword_name_$keyword[0]"]));
                    $keyword_status = $_POST["box_$keyword[0]"];

                    if ($keyword_status == "")
                        $keyword_status = 0;

                    if ($keyword_name_new != "")
                        $update = q("UPDATE Keywords SET keyword = '$keyword_name_new', status = '$keyword_status' WHERE id = '$keyword[0]'");
                }

                //  New Keyword
                $keyword_name = addslashes(strip_tags($_POST["keyword_name_".($nKeywords + 1).""]));

                if ($keyword_name != "")        {
                    $insert = q("INSERT INTO Keywords (parent_id, keyword, status) VALUES ('$id', '$keyword_name', '1')");
                    $getID = q("SELECT id FROM Keywords WHERE parent_id = '$id' AND keyword = '$keyword_name' AND status = '1'");

                    /*$yearMonth = q("SELECT DISTINCT(CONCAT(year, '-', month)), year, month FROM Budget WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY CONCAT(year, '-', month) ASC");

                    if (is_array($yearMonth))       {
                        foreach ($yearMonth as $yM) {
                            $insert = q("INSERT INTO Budget (year, month, element_id, planned, amount, actual, due_date, company_id) ".
                                        "VALUES ('".$yM[1]."', '".$yM[2]."', '".$getID."', '', '', '0', '', '".$_SESSION["company_id"]."')");
                        }
                    }
                    */
                }
            }
            else if ($nKeywords == 1)   {
                $keyword_name_old = $keywords[0][1];
                $keyword_name_new = addslashes(strip_tags($_POST["keyword_name_$keywords[0][0]"]));
                $keyword_status = $_POST["box_$keywords[0][0]"];

                if ($keyword_status == "")
                    $keyword_status = 0;

                if ($keyword_name_new != "")
                    $update = q("UPDATE Keywords SET keyword = '$keyword_name_new', status = '$keyword_status' WHERE id = '$keywords[0][0]'");

                //  New SElement
                $keyword_name = addslashes(strip_tags($_POST["keyword_name_".($nKeywords + 1).""]));

                if ($keyword_name != "")        {
                    $insert = q("INSERT INTO Keywords (parent_id, keyword, status) VALUES ('$id', '$keyword_name', '1')");
                    $getID = q("SELECT id FROM Keywords WHERE parent_id = '$id' AND keyword = '$keyword_name' AND status = '1'");

                    /*
                    $yearMonth = q("SELECT DISTINCT(CONCAT(year, '-', month)), year, month FROM Budget WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY CONCAT(year, '-', month) ASC");

                    if (is_array($yearMonth))       {
                        foreach ($yearMonth as $yM) {
                            $insert = q("INSERT INTO Budget (year, month, element_id, planned, amount, actual, due_date, company_id) ".
                                        "VALUES ('".$yM[1]."', '".$yM[2]."', '".$getID."', '', '', '0', '', '".$_SESSION["company_id"]."')");
                        }
                    }
                    */
                }
            }
            else        {
                for ($i = 1; $i <= 5; $i++)     {
                    $keyword_name = addslashes(strip_tags($_POST["keyword_name_$i"]));

                    if ($keyword_name != "")    {
                        $insert = q("INSERT INTO Keywords (parent_id, keyword, status) VALUES ('$id', '$keyword_name', '1')");
                        $getID = q("SELECT id FROM Keywords WHERE parent_id = '$id' AND keyword = '$keyword_name' AND status = '1'");

                        /*
                        $yearMonth = q("SELECT DISTINCT(CONCAT(year, '-', month)), year, month FROM Budget WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY CONCAT(year, '-', month) ASC");

                        if (is_array($yearMonth))       {
                            foreach ($yearMonth as $yM) {
                                $insert = q("INSERT INTO Budget (year, month, element_id, planned, amount, actual, due_date, company_id) ".
                                            "VALUES ('".$yM[1]."', '".$yM[2]."', '".$getID."', '', '', '0', '', '".$_SESSION["company_id"]."')");
                            }
                        }
                        */
                    }
                }
            }

            if ($errorMessage == "")    {
                $time = date("H:i:s");

                $logs = q("INSERT INTO Logs (what,access,on_table,by_user,on_date,on_time,company_id) ".
                            "VALUES ('$element_info_new and Keyword updated','Update','Elements','".$_SESSION["email"]."','$today','$time','".$_SESSION["company_id"]."')");

                header("Location: categories.php");
            }
        }
        else
            $errorMessage = "Keyword Already Exists";
    }

    if ($errorMessage != "")
        echo "<p align='center' style='padding:0px;'><strong><font color='#999999'>$errorMessage</font></strong></p>";

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("2", "budget");
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    var cal                                                             = new CodeThatCalendar(caldef1);

    function check(box)
    {
        var valid                                                       = 1;

        //  Check That Project Type Is Entered
        if (document.forms["category_edit"].element_name.value == "")
        {
            ShowLayer("categoryName", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("categoryName", "none");

        if (valid == 1)
        {
            document.forms["category_edit"].save.value                  = 1;
            document.forms["category_edit"].submit();
        }
    }
</script>
<?php
    $id = $_GET["id"];
    $element_info = q("SELECT name FROM Elements WHERE id = '$id'");

    //  nElements = Number of Elements per Category
    $nKeywords = q("SELECT COUNT(id) FROM Keywords WHERE parent_id = '$id'");
    $keywords = q("SELECT id, keyword, status FROM Keywords WHERE parent_id = '$id' ORDER BY keyword");

    $index = 2;
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="category_edit">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Edit Keywords</h6>
                            </td>
                        </tr>
                        <tr>
                            <td class="centerdata">
                                <br/>
                            </td>
                        </tr>
                    </table>
                    <table class="on-table-center">
                        <tr>
                            <td class="on-description-left">Element</td>
                            <td class="on-description-left">Keyword</td>
                            <td class="on-description-center">Active</td>
                        </tr>
                        <tr>
                            <td class="rightdata" valign="top" rowspan="100%">
                                <input class="on-field" name="element_name" tabindex="1" type="text" value="<?php echo $element_info; ?>">
                                <div id="categoryName" style="display: none;"><font style="margin-right:45px;" class='on-validate-error'>* Element must be entered</font></div>
                            </td>
                            <?php
                                if (is_array($keywords)) {
                                    $count = 0;

                                    foreach ($keywords as $keyword)     {
                                        ?>
                                        <td class="rightdata">
                                        <?php
                                            echo "<input class='on-field' id='keyword_name_".$keyword[0]."' name='keyword_name_".$keyword[0]."' tabindex='".($index++)."' type='text' value='".$keyword[1]."'>";
                                        ?>
                                        </td>
                                        <td class="centerdata" style="padding-top:8px;">
                                        <?php
                                            if ($keyword[2] == "1")
                                                echo "<input id='box_".$keyword[0]."' name='box_".$keyword[0]."' type='checkbox' value='1' checked>";
                                            else
                                                echo "<input id='box_".$keyword[0]."' name='box_".$keyword[0]."' type='checkbox' value='1'>";
                                        ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <?php
                                    }
                                    ?>
                                        <td class="rightdata">
                                        <?php
                                            echo "<input class='on-field' id='keyword_name_".($nKeywords + 1)."' name='keyword_name_".($nKeywords + 1)."' tabindex='".($index++)."' type='text'>";
                                        ?>
                                        </td>
                                        <td class="centerdata">
                                        </td>
                                    </tr>
                                    <?php
                                } else  {
                                    for ($i = 1; $i <= 5; $i++) {
                                    ?>
                                        <td class="rightdata">
                                        <?php
                                            echo "<input class='on-field' id='keyword_name_".$i."' name='keyword_name_".$i."' tabindex='".($index++)."' type='text'>";
                                        ?>
                                        </td>
                                        <td class="centerdata">
                                        </td>
                                    </tr>
                                    <?php
                                        if ($i < 5)
                                            echo "<tr>";
                                    }
                                }
                            ?>
                    </table>
                    <br/>
                    <input name="btnAdd" onClick="check();" tabindex="<?php echo "".($index++); ?>" type="button" value="Update Keyword">
                    <input method="post" name="save" type="hidden" value="0" />
                </form>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
