<?php
    include("_db.php");
    include("_dates.php");

    function getEasterSunday($date)   {
        global $dates;

        $day = q("SELECT DATE_FORMAT('".$date."', '%W')");

        switch($day)        {
            case "Monday":
                $date = q("SELECT DATE_ADD('".$date."', INTERVAL 6 DAY)");
                break;            
            case "Tuesday":
                $date = q("SELECT DATE_ADD('".$date."', INTERVAL 5 DAY)");
                break;            
            case "Wednesday":
                $date = q("SELECT DATE_ADD('".$date."', INTERVAL 4 DAY)");
                break;            
            case "Thursday":
                $date = q("SELECT DATE_ADD('".$date."', INTERVAL 3 DAY)");
                break;            
            case "Friday":
                $date = q("SELECT DATE_ADD('".$date."', INTERVAL 2 DAY)");
                break;            
            case "Saturday":
                $date = q("SELECT DATE_ADD('".$date."', INTERVAL 1 DAY)");
                break;            
            case "Sunday":
                $date = q("SELECT DATE_ADD('".$date."', INTERVAL 7 DAY)");
                break;            
        }

        $dates[] = q("SELECT DATE_SUB('".$date."', INTERVAL 2 DAY)");
        $dates[] = q("SELECT DATE_ADD('".$date."', INTERVAL 1 DAY)");

        return;
    }
?>
<html>
    <head>
        <title>
            On-Track - Recovery Script...
        </title>
    </head>
    <body>
        <h1 align="center">
            Install - Public Holidays
        </h1>
        <?php
            $drop = q("DROP TABLE IF EXISTS fullMoons");
            $create = q("CREATE TABLE fullMoons ( ".
                        "id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,".
                        "date CHAR(10))");

            $a[] = "2011-04-18";
            $a[] = "2012-04-06";
            $a[] = "2013-03-27";
            $a[] = "2014-04-15";
            $a[] = "2015-04-04";
            $a[] = "2016-03-23";
            $a[] = "2017-04-11";
            $a[] = "2018-03-31";
            $a[] = "2019-04-19";
            $a[] = "2020-04-08";
            $a[] = "2021-03-28";
            $a[] = "2022-04-16";
            $a[] = "2023-04-06";
            $a[] = "2024-03-25";
            $a[] = "2025-04-13";
            $a[] = "2026-04-02";
            $a[] = "2027-03-22";
            $a[] = "2028-04-09";
            $a[] = "2029-03-30";
            $a[] = "2030-04-18";
            $a[] = "2031-04-07";
            $a[] = "2032-03-27";
            $a[] = "2033-04-14";
            $a[] = "2034-04-03";
            $a[] = "2035-03-23";
            $a[] = "2036-04-10";
            $a[] = "2037-03-31";
            $a[] = "2038-04-19";
            $a[] = "2039-04-09";
            $a[] = "2040-03-28";
            $a[] = "2041-04-16";
            $a[] = "2042-04-05";
            $a[] = "2043-03-25";
            $a[] = "2044-04-12";
            $a[] = "2045-04-01";
            $a[] = "2046-03-22";
            $a[] = "2047-04-10";
            $a[] = "2048-03-30";
            $a[] = "2049-04-18";
            $a[] = "2050-04-07";

            sort($a);

            foreach ($a as $d)  $insert = q("INSERT INTO fullMoons(date) VALUES ('".$d."')");

            $drop = q("DROP TABLE IF EXISTS publicHolidays");
            $create = q("CREATE TABLE publicHolidays ( ".
                        "id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,".
                        "date CHAR(10))");

            $year = 2011;

            while ($year <= 2050)       {
                unset($dates);

                $dates[] = $year."-01-01";
                $dates[] = $year."-03-21";
                $dates[] = $year."-04-27";
                $dates[] = $year."-05-01";
                $dates[] = $year."-06-16";
                $dates[] = $year."-08-09";
                $dates[] = $year."-09-24";
                $dates[] = $year."-12-16";
                $dates[] = $year."-12-25";
                $dates[] = $year."-12-26";

                $fullMoons = q("SELECT date, date FROM fullMoons WHERE date LIKE '".$year."%' ORDER BY date");

                if (is_array($fullMoons))       {
                    getEasterSunday($fullMoons[0][0]);
                }

                sort($dates);

                foreach ($dates as $d)  {
                    $day = q("SELECT DATE_FORMAT('".$d."', '%W')");

                    if ($day != "Saturday")     {
                        $date = $d;

                        if ($day == "Sunday")   $date = q("SELECT DATE_ADD('".$date."', INTERVAL 1 DAY)");

                        if (!exist("publicHolidays", "date = '".$date."'"))
                            $insert = q("INSERT INTO publicHolidays(date) VALUES ('".$date."')");
                    }
                }

                $year++;
            }
        ?>
        <form action="index.php" method="post">
            <center><input type="submit" value="Return"/></center>
        </form>
    </body>
</html>
