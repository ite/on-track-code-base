<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("include/sajax.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");
        
    if (!hasAccess("REP_SP_SUMMARY"))
        header("Location: noaccess.php");
        
    // Class for table background color
    $colorClass = "OnTrack_#_";            

    //  Set Report Generated Status
    $generated = "0";
    
    // Excel export Variables
    $row1Head = "";
    $row2Head = "";
    $row3Head = "";
    $row = 0;
    
    GLOBAL $excelheadings;
    
    function printHeadings($val, $level, $cols){
        GLOBAL $excelheadings;
        $excelheadings[$level][] = $val;for($i=1; $i<=$cols; $i++)$excelheadings[$level][] ="";
    }

    //  Create Display Information Function
    if (isset($_POST["save"]) && $_POST["save"] === "1") 
    {
        GLOBAL $excelheadings;
        $errorMessage = "";

        $reportOn = $_POST["reportOn"];

        // POST Values
        $project_id = $_POST["project"];
        $date_type = $_POST["dateType"];
        
        //  Get Dates
        if ($date_type == "currentwk") {    $date_from = currentWeekStart();    $date_to = $today;}
        else if ($date_type == "previouswk") {  $date_from = getDates(currentWeekStart(), -7);  $date_to = getDates(currentWeekStart(), -1);}
        else if ($date_type == "currentmnth") { $date_from = getMonthStart($today); $date_to = $today;}
        else if ($date_type == "previousmnth") { $date_from = getPreviousMonthStart();   $date_to = getMonthEnd(getPreviousMonthStart());}
        else if ($date_type == "projectLifetime") {
            $date_fromT = q("SELECT MIN(date) FROM TimeSheet WHERE project_id = '".$project_id."'");
            $date_fromE = q("SELECT MIN(date) FROM ExpenseSheet WHERE project_id = '".$project_id."'");
            $date_from = ($date_fromT <= $date_fromE) ? $date_fromT : $date_fromE;
            $date_to = $today;
        }
        else if ($date_type == "custom") {$date_from = addslashes(strip_tags($_POST["date_from"])); $date_to = addslashes(strip_tags($_POST["date_to"]));}
        //  Timestamp
        if ($date_from === $date_to)
            $timestamp = "Time Period: ".$date_from;
        else
            $timestamp = "Time Period: ".$date_from." - ".$date_to;

        $table1 = ($reportOn == "approved") ? "ApprovedTime" : "TimeSheet";
        $where1 = ($reportOn == "approved") ? "AND ts.status2 = '2' " : "";

        $table2 = ($reportOn == "approved") ? "ApprovedExpense" : "ExpenseSheet";
        $where2 = ($reportOn == "approved") ? "AND ts.status2 = '2' " : "";

        //////////   Get Data  ////////////////
        $project_name = q("SELECT name FROM Project WHERE id = '".$project_id."' ");
		$sp = q("SELECT p_type,id FROM Project WHERE id = '".$project_id."' ");

                
        $areas= q("SELECT ar.id, ar.name FROM (areas AS ar 
                        INNER JOIN Project AS p ON p.id = ar.pID)
                        WHERE p.id = '".$project_id."' ");
                        
        if ($sp[0][0] == "SP")
	            $activityTypes = q("SELECT at.id, at.type FROM (ActivityTypes AS at) WHERE pID = '".$sp[0][1]."'");
//        else
//	            $activityTypes = q("SELECT at.id, at.type FROM (ActivityTypes AS at) WHERE at.company_id = '".$_SESSION["company_id"]."' ");

                                    
		$companies = q("SELECT c.id, c.name FROM (Project AS p INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id 
							INNER JOIN Company AS c ON cot.company_id = c.id)
							WHERE cot.project_id = '".$project_id."' ORDER BY c.name");
                                
        $whatever = q("SELECT  c.name, ar.name, at.type, ts.time_spent * ts.rate FROM (Project AS p
                                        INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
                                        INNER JOIN Company AS c ON c.id = cot.company_id
                                        INNER JOIN $table1 AS ts ON ts.company_id = cot.company_id
                                        LEFT JOIN areas AS ar ON ar.id = ts.area_id
                                        INNER JOIN Activities AS a ON a.id = ts.activity_id
                                        INNER JOIN ActivityTypes AS at ON at.id = a.parent_id)
                                        WHERE ts.date >= '$date_from' 
                                        AND ts.date <= '$date_to'
                                        AND ts.project_id = $project_id
                                        AND cot.company_id = ".$_SESSION["company_id"]."
                                        AND cot.project_id = $project_id
                                        AND ts.area_id = 0 $where1 
                                        ORDER BY c.name, ar.name, at.type");


        if (is_array($whatever))    {
            $n[0][0] = 0;
            $n[0][1] = "NO AREA";
            $areas = array_merge($n,$areas);
        }

        class O{
            public $area; public $sum;
            public function __construct($area,$sum) {
                $this->area = $area;
                $this->sum = $sum;
            }
        }
		
    if(is_array($companies)){
        if(is_array($activityTypes)){
            for($c = 0; $c < count($companies); ++$c) {
                for($a = 0; $a < count($activityTypes); ++$a) {
                    $time = q("SELECT c.name, ts.area_id, at.type, ts.time_spent * ts.rate FROM (Project AS p
                                        INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
                                        INNER JOIN Company AS c ON c.id = cot.company_id
                                        INNER JOIN $table1 AS ts ON ts.company_id = cot.company_id
                                        LEFT JOIN areas AS ar ON ar.id = ts.area_id
                                        INNER JOIN Activities AS a ON a.id = ts.activity_id
                                        INNER JOIN ActivityTypes AS at ON at.id = a.parent_id)
                                        WHERE ts.date >= '$date_from' 
                                        AND ts.date <= '$date_to'
                                        AND ts.project_id = $project_id
                                        AND at.id = ".$activityTypes[$a][0]."
                                        AND cot.company_id = ".$companies[$c][0]."
                                        AND cot.project_id = $project_id $where1 
                                        ORDER BY c.name, ar.name, at.type");
                                        
                    $expense= q("SELECT c.name, ts.area_id, at.type, ts.expense FROM (Project AS p
                                            INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
                                            INNER JOIN Company AS c ON c.id = cot.company_id
                                            INNER JOIN $table2 AS ts ON ts.company_id = cot.company_id
                                            LEFT JOIN areas AS ar ON ar.id = ts.area_id
                                            INNER JOIN Activities AS a ON a.id = ts.activity_id
                                            INNER JOIN ActivityTypes AS at ON at.id = a.parent_id)
                                            WHERE ts.date >= '$date_from' 
                                            AND ts.date <= '$date_to'
                                            AND ts.project_id = $project_id
                                            AND at.id = ".$activityTypes[$a][0]."
                                            AND cot.company_id = ".$companies[$c][0]."
                                            AND cot.project_id = $project_id $where2 
                                            ORDER BY c.name, ar.name, at.type");
                  
/*              
        echo count($time);
        echo "<table >";
                foreach($time as $t){
              echo "<tr style='background-color:white'>";
                    foreach($t as $x){
                    echo "<td>| $x | </td>";
                    }
                echo "</tr>";
                }
        echo "</table>";
        echo count($expense);
        echo "<table >";
                foreach($expense as $t){
              echo "<tr style='background-color:white'>";
                    foreach($t as $x){
                    echo "<td>| $x | </td>";
                    }
                echo "</tr>";
                }
        echo "</table>";
*/
                    if(is_array($time) || is_array($expense)){
                        ///// -- Time Sheet -- /////
                        $prev = $time[0][1];
                        $sum = $time[0][3];
                        $areaCounter = 0;

                        for($i = 1; $i < count($time); ++$i)    {
                            if ($time[$i][1] != $prev) {
                                $o = new O($prev,$sum);
                                $timeTotal[$c][$prev][$a] = $o;
                                $sum = (double)$time[$i][3];
                            }else{
                                $sum += (double)$time[$i][3];
                            }
                            $prev = $time[$i][1];
                        }
                        $o = new O($prev,$sum);
                        $timeTotal[$c][$prev][$a] = $o;
                        ///// -- Expense Sheet -- /////
                        $prev = $expense[0][1];
                        $sum = $expense[0][3];
                        $areaCounter = 0;

                        for($i = 1; $i < count($expense); ++$i)    {
                            if ($expense[$i][1] != $prev) {
                                $o = new O($prev,$sum);
                                $expenseTotal[$c][$prev][$a] = $o;
                                $sum = (double)$expense[$i][3];
                            }else{
                                $sum += (double)$expense[$i][3];
                            }
                            $prev = $expense[$i][1];
                        }
                        $o = new O($prev,$sum);
                        $expenseTotal[$c][$prev][$a] = $o;
                    }
                }
            }
        }
/*		
		echo count($timeTotal);
		foreach($timeTotal as $tt)	{
        echo "<table>";
                foreach($tt as $t){
              echo "<tr style='background-color:white'>";
                    foreach($t as $x){
                    echo "<td>AREA:$x->area - SUM:$x->sum</td>";
                    }
                echo "</tr>";
                }
        echo "</table>";
		}
        
		echo count($expenseTotal);
		foreach($expenseTotal as $tt)	{
        echo "<table>";
                foreach($tt as $t){
              echo "<tr style='background-color:white'>";
                    foreach($t as $x){
                    echo "<td>AREA:$x->area - SUM:$x->sum</td>";
                    }
                echo "</tr>";
                }
        echo "</table>";
		}
*/
        
        $displayString .= "<tr><td class='on-table-clear' colspan='100%'><a>Project Summary Report</a></td></tr>";
        $displayString .= "<tr><td class='on-table-clear' colspan='100%'>Project: ".$project_name."</td></tr>";
        $displayString .= "<tr><td class='on-table-clear' colspan='100%'>".$timestamp."</td></tr>";
        $displayString .= "<tr>
                    <th>Area</th>
                    <th>Activity Type</th>
                    <th>Consulting</th>
                    <th>Expense</th>
                    <th>Total (".$_SESSION["currency"].")</th>
                </tr>";
        
        ///// -- EXCEL -- /////
        $exceldata[$row][] = $colorClass."Area";
        $exceldata[$row][] = $colorClass."Activity Type";
        $exceldata[$row][] = $colorClass."Consulting";
        $exceldata[$row][] = $colorClass."Expense";
        $exceldata[$row][] = $colorClass."Total (".$_SESSION["currency"].")";
            $row++;
                
        $row1Head = "Project Summary Report";
        $row2Head = "Project: ".$project_name;
        $row3Head = "".$timestamp."";
        
        $grandTime = 0;
        $grandExpense = 0;
        $grandTotal = 0;
            
        for($c = 0; $c < count($companies); ++$c)    {
        
            $totalConsulting = 0;
            $totalExpense = 0;
            $totalSummed = 0;
        
            $currC = $companies[$c][1];     
            if ($c == 0)    
                $dispC = $currC; 
            else {
                if ($prevC == $currC) 
                    $dispC = "";  
                else    
                    $dispC = $currC;
            }
            
            $checkQuery = q("SELECT id, time_spent FROM TimeSheet 
                                         WHERE company_id = ".$companies[$c][0]." 
                                         AND project_id = ".$project_id." 
                                         AND date >= '$date_from' AND date <= '$date_to' ");
            
            if(is_array($checkQuery)){
                    $displayString .= "<tr><td class='on-table-clear' colspan='100%'><a>Company: $dispC</a></td></tr>";
               
                ///// -- EXCEL -- /////
                $exceldata[$row][] = "Company: $dispC";
                $exceldata[$row][] = "";
                $exceldata[$row][] = "";
                $exceldata[$row][] = "";
                $exceldata[$row][] = "";
                    $row++;
           
				$prevAR = "";
                for($ar = 0; $ar < count($areas); ++$ar)    {
                    for($a = 0; $a < count($activityTypes); ++$a)    {
                        $currAR = $areas[$ar][1];
                        if ($c+$ar+$a == 0){
                            $dispAR = $currAR;
                        }else {
                            if ($prevAR == $currAR)
                                $dispAR = "";
                            else
                                $dispAR = $currAR;
                        }
                        $dispA = $activityTypes[$a][1];
                        $dispTime = ($timeTotal[$c][$areas[$ar][0]][$a] != "") ? (double)$timeTotal[$c][$areas[$ar][0]][$a]->sum : 0.0;
                        $dispExpense = ($expenseTotal[$c][$areas[$ar][0]][$a] != "") ? (double)$expenseTotal[$c][$areas[$ar][0]][$a]->sum : 0.0;
                        $dispBalance = ($dispTime + $dispExpense);

                        if($dispBalance > 0){
                            $displayString .= "<tr>
                                        <td>$dispAR</td>
                                        <td>$dispA</td>
                                        <td class='rightdata'>".number_format($dispTime, 2, ".", "")."</td>
                                        <td class='rightdata'>".number_format($dispExpense, 2, ".", "")."</td>
                                        <td class='on-table-total'>".number_format($dispBalance, 2, ".", "")."</td>
                                    </tr>";
                            
                            $totalConsulting += $dispTime;
                            $totalExpense += $dispExpense;
                            $totalSummed += $dispBalance;
                            
                                    ///// -- EXCEL -- /////
                                    $exceldata[$row][] = $dispAR;
                                    $exceldata[$row][] = $dispA;
                                    $exceldata[$row][] = $dispTime;
                                    $exceldata[$row][] = $dispExpense;
                                    $exceldata[$row][] = $dispBalance;
                                        $row++;
                                        
                            $prevAR = $currAR;
                        }
                    }
                }

                $prevC = $currC;      
                
                if($totalSummed > 0){
                    $displayString .= "<tr>
                                                    <td class='on-table-total'>&nbsp;</td>
                                                    <td class='on-table-total'>Total:</td>
                                                    <td class='on-table-total'>".number_format($totalConsulting, 2, ".", "")."</td>
                                                    <td class='on-table-total'>".number_format($totalExpense, 2, ".", "")."</td>
                                                    <td class='on-table-total'>".number_format($totalSummed, 2, ".", "")."</td>
                                                </tr>
                                                <tr><td colspan='100%'></td></tr>"; 
                                                
                                    ///// -- EXCEL -- /////
                                    $exceldata[$row][] = $colorClass."";
                                    $exceldata[$row][] = $colorClass."Total:";
                                    $exceldata[$row][] = $colorClass.$totalConsulting;
                                    $exceldata[$row][] = $colorClass.$totalExpense;
                                    $exceldata[$row][] = $colorClass.$totalSummed;
                                        $row++;
                                        
                    $grandTime += $totalConsulting ;
                    $grandExpense += $totalExpense ;
                    $grandTotal += $totalSummed ;
                }else{
                    $displayString .= "<tr><td class='centerdata' colspan='100%'></td></tr>";
                }
            }
        }
        $displayString .= "<tr>
                                            <td class='on-table-total'>&nbsp;</td>
                                            <td class='on-table-total'>Grand Total:</td>
                                            <td class='on-table-total'>".number_format($grandTime, 2, ".", "")."</td>
                                            <td class='on-table-total'>".number_format($grandExpense, 2, ".", "")."</td>
                                            <td class='on-table-total'>".number_format($grandTotal, 2, ".", "")."</td>
                                    </tr>";
                                    
                             ///// -- EXCEL -- /////
                            $exceldata[$row][] = $colorClass."";
                            $exceldata[$row][] = $colorClass."Grand Total:";
                            $exceldata[$row][] = $colorClass.$grandTime;
                            $exceldata[$row][] = $colorClass.$grandExpense;
                            $exceldata[$row][] = $colorClass.$grandTotal;
                                $row++;
        
    }

        if ($displayString != "")
            $generated                                                  = "1";
    }
    
    printHeadings($row1Head, 0, 4);
    printHeadings($row2Head, 1, 4);
    printHeadings($row3Head, 2, 4);
    
    //print_r($excelheadings);
    //print_r($exceldata);
   
    //  Print Header
    print_header();
    //  Print Menu
    print_menus("3", "reports");
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    jQuery(function(){
	jQuery('#date_from').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_from').val(),
		current: jQuery('#date_from').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_from').val()))
			_date = new Date();
			else _date = jQuery('#date_from').val();
			jQuery('#date_from').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_from').val(formated);
			jQuery('#date_from').DatePickerHide();
		}
	});
    })

    jQuery(function(){
	jQuery('#date_to').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_to').val(),
		current: jQuery('#date_to').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_to').val()))
			_date = new Date();
			else _date = jQuery('#date_to').val();
			jQuery('#date_to').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_to').val(formated);
			jQuery('#date_to').DatePickerHide();
		}
	});

        jQuery("#project").change(function()    {
            if (jQuery(this).val() != "null")   {
                jQuery("#dateType option[value='projectLifetime']").removeAttr("disabled");
            } else      {
                jQuery("#dateType option[value='projectLifetime']").attr("disabled","disabled");
                jQuery("#dateType option[value='currentwk']").attr("selected", "selected");
            }
        });
    })

    //  Sajax
    <?php sajax_show_javascript(); ?>

    function check()
    {
        var valid                                                       = 1;
        
        // Validate Project selection
        if (document.forms["report"].project.value == "null")
        {
            ShowLayer("projectDiv", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("projectDiv", "none");

        if (document.forms["report"].dateType.value == "custom") {
            //  Check That Date From Is Entered
            if (document.forms["report"].date_from.value == "") {
                ShowLayer("dateFrom", "block");
                valid                                                   = 0;
            }
            //  Check That Entered Date From Is Valid
            else {
                ShowLayer("dateFrom", "none");

                if (!validation("date", document.forms["report"].date_from.value)) {
                    ShowLayer("dateFromDiv", "block");
                    valid                                               = 0;
                }
                else
                    ShowLayer("dateFromDiv", "none");
            }

            //  Check That Date To Is Entered
            if (document.forms["report"].date_to.value == "") {
                ShowLayer("dateTo", "block");
                valid                                                   = 0;
            }
            //  Check That Entered Date To Is Valid
            else {
                ShowLayer("dateTo", "none");

                if (!validation("date", document.forms["report"].date_to.value)) {
                    ShowLayer("dateToDiv", "block");
                    valid                                               = 0;
                }
                else
                    ShowLayer("dateToDiv", "none");
            }
        }

        if (valid == 1) {
            document.forms["report"].save.value                         = 1;
            document.forms["report"].submit();
        }
    }
    
    var hasAreas = 1;

    function dateSelection() {
        if (document.forms["report"].dateType.value == "custom")
            ShowLayer("datesDiv", "block");
        else
            ShowLayer("datesDiv", "none");
    }
    
    ///////////////////////////
</script>
<?php
    $projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p 
                            INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) 
                            WHERE cot.company_id = '".$_SESSION["company_id"]."' 
                            AND p.completed = '0' 
                            AND p.deleted = '0' 
                            AND p.p_type='SP' 
                            ORDER BY UPPER(p.name)");
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata">
            
                <form action="" method="post" name="report">
                    <div id="" style="<?php if ($generated == "1") echo "display: none;"; else echo "display: block"; ?>">
                        
                        <table width="100%">
                            <tr>
                                <td class="centerdata">
                                    <h6>Project Summary Report</h6>
                                </td>
                            </tr>
                        </table>
                        <br>   
                         
                       <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Report On:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="reportOn">
                                        <option value="normal">Actual Time</option>
                                        <option value="approved">Approved Time</option>
                                    </select>
                                </td>
                            </tr>
                            <tr><td colspan="100%"><br/></td></tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Shared Project:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" id="project" name="project">
                                        <option value="null">--  Select A Shared Project  --</option>
                                        <?php
                                            if (is_array($projects))
                                                foreach ($projects as $project)
                                                    echo "<option value='".$project[0]."'>".$project[1]."</option>";
                                        ?>
                                    </select>
                                    <div id="projectDiv" style="display: none;"><font class="on-validate-error">* Shared Project must be selected</font></div>
                                </td>
                            </tr>
                        </table>
                        
                        <div id="dateShowDiv" style="display: block">
                            <table width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Date Type(s):
                                    </td>
                                    <td width="50%">
                                        <select class="on-field" method="post" id="dateType" name="dateType" onChange="dateSelection();">
                                            <option value="currentwk">Current Week</option>
                                            <option value="previouswk">Previous Week</option>
                                            <option value="currentmnth">Current Month</option>
                                            <option value="previousmnth">Previous Month</option>
                                            <option value="projectLifetime" disabled>Project Lifetime</option>
                                            <option value="custom">Custom Dates</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                            
                        <div id="datesDiv" style="display: none;">       
                            <table width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Date From:
                                    </td>
                                    <td width="50%">
                                        <input class="on-field-date" id="date_from" name="date_from" type="text" style="text-align:right;" value="<?php
                                            if ($_POST["date_from"] != "") echo "".$_POST["date_from"]; else echo "".getMonthStart($today); ?>">
                                        <div id="dateFrom" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                        <div id="dateFromDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="on-description" width="50%">
                                        Date To:
                                    </td>
                                    <td width="50%">
                                        <input class="on-field-date" id="date_to" name="date_to" type="text" style="text-align:right;" value="<?php
                                            if ($_POST["date_to"] != "") echo "".$_POST["date_to"]; else echo "".getMonthEnd($today); ?>">
                                        <div id="dateTo" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                        <div id="dateToDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        
                        <br/>
                        <input name="btnGenerate" onClick="check();" type="button" value="Generate Report">
                        <input method="post" name="save" type="hidden" value="0" />
                        
                    </div>
                </form>
                
                <div id="report" style="<?php if ($generated == "1") echo "display: block;"; else echo "display: none"; ?>">
                    <?php
                        echo "<div class='on-20px'>
                                    <table class='on-table on-table-center'>".$displayString."
                                        <tfoot><tr><td colspan='100%'></td></tr></tfoot>
                                    </table>
                                </div>";
                        ///////////////////////////
                        //  Set Export Information
                        $_SESSION["fileName"] = "Report SP Time Sheet";
                        $_SESSION["fileData"] = array_merge($excelheadings, $exceldata);
                        ///////////////////////////

                        echo "<input name='btnExport' onClick=\"location.href='_export.php'\" type='button' value='Export to Excel' />";
                    ?>
                </div>
                
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
