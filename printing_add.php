<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

	if (!hasAccess("PRINTING_MANAGE"))
		header("Location: noaccess.php");

    //  Insert Disbursement Type And Sub Disbursement Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")
    {
        $errorMessage                                                   = "";

        //  Get Information
        $disbursement_type_name                                         = addslashes(strip_tags($_POST["disbursement_type_name"]));

        //  Check If Disbursement Type Exists In Database
        $exist                                                          = q("SELECT id FROM DisbursementTypes WHERE type = '$disbursement_type_name' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."'");

        if (!$exist)
        {
            //  Insert Disbursement Type
            $insert                                                     = q("INSERT INTO DisbursementTypes (type, company_id) ".
                                                                            "VALUES ('$disbursement_type_name', '".$_SESSION["company_id"]."')");

            //  Insert Sub Disbursements
            if ($insert)
            {
                $parent_id                                              = q("SELECT id FROM DisbursementTypes WHERE type = '$disbursement_type_name' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."'");

                //  Get Information
                for ($i = 1; $i <= 10; $i++)
                {
                    $disbursement_name                                  = addslashes(strip_tags($_POST["disbursement_name_$i"]));
                    $disbursement_cost                                  = addslashes(strip_tags($_POST["disbursement_cost_$i"]));

                    if ($disbursement_name != "" && $disbursement_cost != "")
                        $insert                                         = q("INSERT INTO Disbursements (parent_id, name, cost) ".
                                                                            "VALUES ('$parent_id', '$disbursement_name', '$disbursement_cost')");
                }
            }

            if ($insert)
            {
                $time                                                   = date("H:i:s");

                $logs                                                   = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('$disbursement_type_name type and printing created', 'Insert', 'Disbursements', ".
                                                                            "'".$_SESSION["email"]."', '$today', '$time', '".$_SESSION["company_id"]."')");

                header("Location: printing_management.php");
            }
        }
        else
            $errorMessage                                               = "Colour Type Already Exists";
    }

    if ($errorMessage != "")
        echo "<p align='center' style='padding:0px;'><strong><font class='on-validate-error'>$errorMessage</font></strong></p>";

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("1", "activity_types");
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    function check(box) {
        var valid                                                       = 1;

        //  Check That Project Type Is Entered
        if (document.forms["printing_add"].disbursement_type_name.value == "") {
            ShowLayer("disbursementTypeName", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("disbursementTypeName", "none");

        for (var i = 1; i <= 10; i++) {
            //  Check That Disbursement Cost Is Valid, If Entered
            if (eval("document.forms['printing_add'].disbursement_name_" + i + ".value") != "")
            {
                if (!validation("currency", eval("document.forms['printing_add'].disbursement_cost_" + i + ".value")))
                {
                    ShowLayer("disbursementName" + i, "block");
                    ShowLayer("disbursementCost" + i, "block");
                    valid                                               = 0;
                }
                else {
                    ShowLayer("disbursementName" + i, "none");
                    ShowLayer("disbursementCost" + i, "none");
                }
            }
            else {
                ShowLayer("disbursementName" + i, "none");
                ShowLayer("disbursementCost" + i, "none");
            }
        }

        if (valid == 1) {
            document.forms["printing_add"].save.value               = 1;
            document.forms["printing_add"].submit();
        }
    }
</script>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="printing_add">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Add New Printing Type</h6>
                            </td>
                        </tr>
                        <tr>
                            <td class="centerdata">
                                <br/>
                            </td>
                        </tr>
                    </table>
                    <table class="on-table-center">
                        <tr>
                            <td class="on-description-left">
                                Printing Type
                            </td>
                            <td class="on-description-left">
                                Colour
                            </td>
                            <td  class="on-description-left">
                                Cost
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input class="on-field" name="disbursement_type_name" tabindex="1" type="text">
                                <div id="disbursementTypeName" style="display: none;"><font class="on-validate-error">* Printing Type must be entered&nbsp;</font></div>
                            </td>
                            <td align="left" valign="top">
                                <?php
                                    for ($i = 1; $i <= 10; $i++)
                                        echo "<input class='on-field' name='disbursement_name_".$i."' tabindex='".($i + 1)."' type='text'><br/>".
                                            "<div id='disbursementName".$i."' style='display: none;'>".
                                            "<font class='on-validate-error'>&nbsp;</font></div>";
                                ?>
                            </td>
                            <td align="left" valign="top">
                                <?php
                                    for ($i = 1; $i <= 10; $i++)
                                        echo "<input class='on-field-date' name='disbursement_cost_".$i."' tabindex='".($i + 1)."' type='text' style='text-align:right;'><br/>".
                                            "<div id='disbursementCost".$i."' style='display: none;'>".
                                            "<font class='on-validate-error'>* Colour Cost must be numeric, eg. 123.45</font></div>";
                                ?>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <input name="btnAdd" onClick="check();" tabindex="12" type="button" value="Add Printing">
                    <input method="post" name="save" type="hidden" value="0" />
                </form>
            </td>
        </tr>
        <tr>
            <td class="centerdata">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
