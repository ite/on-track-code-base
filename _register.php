<?php
    session_start();

    include("_db.php");
    include("_functions.php");

    //////////////////////////////////////////////////
    //  GET SESSION VARIABLE VALUES HERE
    //////////////////////////////////////////////////
    $companyID = addslashes(strip_tags($_SESSION["company_id"]));
    $userID = addslashes(strip_tags($_SESSION["user_id"]));
    $userName = addslashes(strip_tags($_SESSION["email"]));
    //////////////////////////////////////////////////

    if (isset($data))   unset($data);

    $func = (isset($_POST["func"])) ? $_POST["func"] : $_GET["func"];

    switch ($func)	{
        //////////////////////////////////////////////////
        //  RUNNING DIRECT QUERIES
        case "industry":	{
            unset($data);

            $industry = addslashes(strip_tags($_POST["industry"]));

Architecture [PRISM - strip to basics]
ICT [ITE - strip to basics]
Engineering [AES - strip to basics]
Consulting [AGES - strip to basics][Consulting]
Geotechnical [AGES - strip to basics][Geotechnical]
Other


            if (is_null($data))	$data = "";

            echo json_encode($data);
            break;
        }
        //////////////////////////////////////////////////
        //  DEFAULT FUNCTION
        //////////////////////////////////////////////////
        default:	{
            echo json_encode("ERROR: Ajax.php->DEFAULT was reached. Fix function reference");
            break;
        }
    }
?>		
