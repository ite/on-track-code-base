<?php
    include("_db.php");
    include("_dates.php");
    include("_functions.php");

    ////////////////////////////////////////
    //  POPULATE TABLES
    ////////////////////////////////////////
    $insert = q("INSERT INTO Company (name,rate,currency,notifyPeriod,locked) VALUES ('Demo Company','55.00','R',3,'0')");

    $companyID = q("SELECT id FROM Company WHERE name = 'Demo Company'");

    $password = generateHash("demopassword");

    $insert = q("INSERT INTO Employee (lstname,frstname,email,password,salt,company_id,cost,demo_user,deleted,locked,activationDate,usertype) VALUES ".
                    "('User 1','Demo','demo1@on-track.ws','$password','$origsalt',".$companyID.",'250.00',0,0,0,'".date("Y-m-d")."','0'),".
                    "('User 2','Demo','demo2@on-track.ws','$password','$origsalt',".$companyID.",'200.00',0,0,0,'".date("Y-m-d")."','0'),".
                    "('User 3','Demo','demo3@on-track.ws','$password','$origsalt',".$companyID.",'150.00',0,0,0,'".date("Y-m-d")."','0'),".
                    "('User 4','Demo','demo4@on-track.ws','$password','$origsalt',".$companyID.",'100.00',0,0,0,'".date("Y-m-d")."','0')");

    $userID = q("SELECT id FROM Employee WHERE email = 'admin'");
    $userID1 = q("SELECT id FROM Employee WHERE email = 'demo1@on-track.ws'");
    $userID2 = q("SELECT id FROM Employee WHERE email = 'demo2@on-track.ws'");
    $userID3 = q("SELECT id FROM Employee WHERE email = 'demo3@on-track.ws'");
    $userID4 = q("SELECT id FROM Employee WHERE email = 'demo4@on-track.ws'");

    $insert = q("INSERT INTO EULA (user_id) VALUES ('".$userID1."'),('".$userID2."'),('".$userID3."'),('".$userID4."')");

    $insert = q("INSERT INTO roles (companyid,role,descr,active,dflt) SELECT ".$companyID.",role,descr,active,dflt FROM roles WHERE companyid = 0");

	//  Clear Default Role Actions
	$delete = q("DELETE FROM role_action WHERE companyid = '0'");

	//  Insert Default Role Actions
	$roles = array("ADM", "MAN", "SPM", "PM", "FM", "TM", "CL", "CA");

	$actions["ADM"] = array(
											"ACCOUNT_MANAGE",
											"ACTION_ASSIGN",
											"ACTION_MAN",
											"APPROVAL_MANAGE",
											"AREA_MANAGE",
											"ATYPE_MANAGE",
											"BOOKING_MANAGE",
											"BUDGET_CAT_MANAGE",
											"BUDGET_MANAGE",
											"BUDGET_VIEW",
											"CHNG_PW",
											"CLIENT_MANAGE",
											"COMP_ASSIGN",
											"COMP_MAN",
											"EMP_ADD_REMOVE",
											"EMP_CTC_MANAGE",
											"EMP_EDIT",
											"EMP_TARIFF_MANAGE",
											"EULA",
											"EXPENSE_MANAGE",
											"EXPENSE_MANAGE_ALL",
											"EXPENSE_MANAGE_OWN",
											"EXPENSE_VIEW",
											"GPS_LOGBOOK_MANAGEMENT",
											"INVOICE_MANAGE",
											"INVOICE_MANAGE_NEW",
											"INVOICE_PROCESS",
											"INVOICE_VIEW",
											"INVOICE_VIEW_NEW",
											"LEAVE_MANAGE_ALL",
											"LEAVE_MANAGE_OWN",
											"LOCATION_MANAGEMENT",
											"LOGS_VIEW_ALL",
											"LOGS_VIEW_OWN",
											"MODULE_MAN",
											"NEWS_MAN",
											"PAYMENT_CERTIFICATE",
											"PRINTING_MANAGE",
											"PROJTYPE_MANAGE",
											"PROJ_MANAGE",
											"RA_MANAGEMENT",
											"REP_ACTIVE_PROJ",
											"REP_ANALYSIS",
											"REP_BUDGET",
											"REP_BUDGET_PERIOD",
											"REP_CASH_FLOW",
											"REP_CASH_FLOW2",
											"REP_COMPANY_BREAK",
											"REP_COMPANY_SUMM",
											"REP_EX_SHEET",
											"REP_GROWTH_ANALYSIS",
											"REP_INCOME_FORECAST",
											"REP_INVOICE_TRACK",
											"REP_INV_VS_CTC_OWN",
											"REP_INV_VS_CTC_OWN_V2",
											"REP_IN_FORE_INVOICE",
											"REP_IN_FORE_TIME",
											"REP_IN_VS_CTC",
											"REP_IN_VS_CTC_V2",
											"REP_LEAVE",
											"REP_LOADED_INV",
											"REP_LOGBOOK",
											"REP_OVER_BUD_PROJ_CONS",
											"REP_PROGRESS",
											"REP_PROJECT",
											"REP_PROJ_BREAK",
											"REP_PROJ_INFO",
											"REP_PROJ_LIST",
											"REP_PROJ_TYPE_COM",
											"REP_RUNNING_PROF",
											"REP_SP_BREAK",
											"REP_SP_EX_SHEET",
											"REP_SP_INV",
											"REP_SP_SUMMARY",
											"REP_SP_SUMMARY_EX",
											"REP_SP_SUMMARY_TIME",
											"REP_SP_TIME_SHEET",
											"REP_STATEMENT",
											"REP_SUMMARY",
											"REP_SUMMARY_EX",
											"REP_SUMMARY_TIME",
											"REP_TIME_COM",
											"REP_TIME_EX",
											"REP_TIME_SHEET",
											"REP_TYPE_SUM",
											"REP_WORK_IN_PR",
											"REP_WORK_IN_PR_M",
											"RESOURCE_ASSIGN",
											"RESOURCE_MAN",
											"ROLE_ASSIGN",
											"ROLE_MAN",
											"SHAREDRES_BOOK",
											"SHAREDRES_MANAGE",
											"SHAREDRES_MANAGE_OWN",
											"SLA_MANAGE",
											"SPM_MANAGE",
											"STATEMENT_MAN",
											"TIME_MANAGE",
											"TIME_MANAGE_ALL",
											"VEHICLE_ASSIGN",
											"VEH_MANAGE"
										);
	$actions["MAN"] = array(
											"ACCOUNT_MANAGE",
											"ACTION_ASSIGN",
											"ACTION_MAN",
											"APPROVAL_MANAGE",
											"AREA_MANAGE",
											"ATYPE_MANAGE",
											"BOOKING_MANAGE",
											"BUDGET_CAT_MANAGE",
											"BUDGET_MANAGE",
											"BUDGET_VIEW",
											"CHNG_PW",
											"CLIENT_MANAGE",
											"COMP_ASSIGN",
											"COMP_MAN",
											"EMP_ADD_REMOVE",
											"EMP_CTC_MANAGE",
											"EMP_EDIT",
											"EMP_TARIFF_MANAGE",
											"EULA",
											"EXPENSE_MANAGE",
											"EXPENSE_MANAGE_ALL",
											"EXPENSE_MANAGE_OWN",
											"EXPENSE_VIEW",
											"GPS_LOGBOOK_MANAGEMENT",
											"INVOICE_MANAGE",
											"INVOICE_MANAGE_NEW",
											"INVOICE_PROCESS",
											"INVOICE_VIEW",
											"INVOICE_VIEW_NEW",
											"LEAVE_MANAGE_ALL",
											"LEAVE_MANAGE_OWN",
											"LOCATION_MANAGEMENT",
											"LOGS_VIEW_ALL",
											"LOGS_VIEW_OWN",
											"MODULE_MAN",
											"PAYMENT_CERTIFICATE",
											"PRINTING_MANAGE",
											"PROJTYPE_MANAGE",
											"PROJ_MANAGE",
											"RA_MANAGEMENT",
											"REP_ACTIVE_PROJ",
											"REP_ANALYSIS",
											"REP_BUDGET",
											"REP_BUDGET_PERIOD",
											"REP_CASH_FLOW",
											"REP_CASH_FLOW2",
											"REP_COMPANY_BREAK",
											"REP_COMPANY_SUMM",
											"REP_EX_SHEET",
											"REP_GROWTH_ANALYSIS",
											"REP_INCOME_FORECAST",
											"REP_INVOICE_TRACK",
											"REP_INV_VS_CTC_OWN_V2",
											"REP_IN_FORE_INVOICE",
											"REP_IN_FORE_TIME",
											"REP_IN_VS_CTC",
											"REP_IN_VS_CTC_V2",
											"REP_LEAVE",
											"REP_LOADED_INV",
											"REP_LOGBOOK",
											"REP_OVER_BUD_PROJ_CONS",
											"REP_PROGRESS",
											"REP_PROJECT",
											"REP_PROJ_BREAK",
											"REP_PROJ_INFO",
											"REP_PROJ_LIST",
											"REP_PROJ_TYPE_COM",
											"REP_RUNNING_PROF",
											"REP_SP_BREAK",
											"REP_SP_EX_SHEET",
											"REP_SP_INV",
											"REP_SP_SUMMARY",
											"REP_SP_SUMMARY_EX",
											"REP_SP_SUMMARY_TIME",
											"REP_SP_TIME_SHEET",
											"REP_STATEMENT",
											"REP_SUMMARY",
											"REP_SUMMARY_EX",
											"REP_SUMMARY_TIME",
											"REP_TIME_COM",
											"REP_TIME_EX",
											"REP_TIME_SHEET",
											"REP_TYPE_SUM",
											"REP_WORK_IN_PR",
											"REP_WORK_IN_PR_M",
											"RESOURCE_ASSIGN",
											"RESOURCE_MAN",
											"ROLE_ASSIGN",
											"ROLE_MAN",
											"SHAREDRES_MANAGE",
											"SHAREDRES_MANAGE_OWN",
											"STATEMENT_MAN",
											"TIME_MANAGE",
											"TIME_MANAGE_ALL",
											"VEHICLE_ASSIGN",
											"VEH_MANAGE"
										);
	$actions["SPM"] = array(
											"REP_LEAVE",
											"REP_SP_BREAK",
											"REP_SP_EX_SHEET",
											"REP_SP_INV",
											"REP_SP_SUMMARY",
											"REP_SP_SUMMARY_EX",
											"REP_SP_SUMMARY_TIME",
											"REP_SP_TIME_SHEET",
											"SPM_MANAGE"
										);
	$actions["PM"] = array(
											"APPROVAL_MANAGE",
											"AREA_MANAGE",
											"ATYPE_MANAGE",
											"BUDGET_CAT_MANAGE",
											"BUDGET_MANAGE",
											"BUDGET_VIEW",
											"EMP_ADD_REMOVE",
											"EMP_CTC_MANAGE",
											"EMP_EDIT",
											"EMP_TARIFF_MANAGE",
											"EXPENSE_MANAGE",
											"EXPENSE_MANAGE_ALL",
											"EXPENSE_MANAGE_OWN",
											"EXPENSE_VIEW",
											"INVOICE_VIEW",
											"LEAVE_MANAGE_ALL",
											"LOCATION_MANAGEMENT",
											"LOGS_VIEW_ALL",
											"LOGS_VIEW_OWN",
											"PRINTING_MANAGE",
											"PROJTYPE_MANAGE",
											"PROJ_MANAGE",
											"REP_ACTIVE_PROJ",
											"REP_ANALYSIS",
											"REP_CASH_FLOW2",
											"REP_EX_SHEET",
											"REP_IN_VS_CTC_V2",
											"REP_LEAVE",
											"REP_LOGBOOK",
											"REP_OVER_BUD_PROJ_CONS",
											"REP_PROGRESS",
											"REP_PROJECT",
											"REP_PROJ_BREAK",
											"REP_PROJ_INFO",
											"REP_PROJ_LIST",
											"REP_SUMMARY",
											"REP_SUMMARY_EX",
											"REP_SUMMARY_TIME",
											"REP_TIME_COM",
											"REP_TIME_EX",
											"REP_TIME_SHEET",
											"REP_TYPE_SUM",
											"REP_WORK_IN_PR",
											"REP_WORK_IN_PR_M",
											"RESOURCE_ASSIGN",
											"RESOURCE_MAN",
											"ROLE_ASSIGN",
											"ROLE_MAN",
											"SLA_MANAGE",
											"TIME_MANAGE",
											"TIME_MANAGE_ALL",
											"VEHICLE_ASSIGN",
											"VEH_MANAGE"
										);
	$actions["FM"] = array(
											"ACCOUNT_MANAGE",
											"BUDGET_CAT_MANAGE",
											"BUDGET_MANAGE",
											"BUDGET_VIEW",
											"CLIENT_MANAGE",
											"EMP_ADD_REMOVE",
											"EMP_CTC_MANAGE",
											"EMP_EDIT",
											"EMP_TARIFF_MANAGE",
											"EXPENSE_MANAGE",
											"EXPENSE_MANAGE_ALL",
											"EXPENSE_MANAGE_OWN",
											"EXPENSE_VIEW",
											"INVOICE_MANAGE",
											"INVOICE_PROCESS",
											"INVOICE_VIEW",
											"INVOICE_VIEW_NEW",
											"LEAVE_MANAGE_ALL",
											"LEAVE_MANAGE_OWN",
											"LOGS_VIEW_ALL",
											"LOGS_VIEW_OWN",
											"PAYMENT_CERTIFICATE",
											"REP_ANALYSIS",
											"REP_BUDGET",
											"REP_BUDGET_PERIOD",
											"REP_CASH_FLOW",
											"REP_CASH_FLOW2",
											"REP_COMPANY_BREAK",
											"REP_COMPANY_SUMM",
											"REP_EX_SHEET",
											"REP_GROWTH_ANALYSIS",
											"REP_INCOME_FORECAST",
											"REP_INVOICE_TRACK",
											"REP_INV_VS_CTC_OWN",
											"REP_INV_VS_CTC_OWN_V2",
											"REP_IN_FORE_INVOICE",
											"REP_IN_FORE_TIME",
											"REP_IN_VS_CTC",
											"REP_IN_VS_CTC_V2",
											"REP_LEAVE",
											"REP_LOADED_INV",
											"REP_PROGRESS",
											"REP_PROJECT",
											"REP_PROJ_BREAK",
											"REP_PROJ_INFO",
											"REP_PROJ_LIST",
											"REP_PROJ_TYPE_COM",
											"REP_RUNNING_PROF",
											"REP_STATEMENT",
											"REP_SUMMARY",
											"REP_TIME_COM",
											"REP_TIME_SHEET",
											"STATEMENT_MAN"
										);
	$actions["TM"] = array(
											"AREA_MANAGE",
											"ATYPE_MANAGE",
											"PROJTYPE_MANAGE"
										);
	$actions["CL"] = array(
											"BOOKING_MANAGE",
											"CHNG_PW",
											"EULA",
											"EXPENSE_MANAGE_OWN",
											"GPS_LOGBOOK_MANAGEMENT",
											"LEAVE_MANAGE_OWN",
											"LOGS_VIEW_OWN",
											"REP_INV_VS_CTC_OWN_V2",
											"REP_LOGBOOK",
											"SHAREDRES_BOOK",
											"TIME_MANAGE"
										);
	$actions["CA"] = array(
											"AREA_MANAGE",
											"ATYPE_MANAGE",
											"EMP_ADD_REMOVE",
											"EMP_CTC_MANAGE",
											"EMP_EDIT",
											"EMP_TARIFF_MANAGE",
											"INVOICE_MANAGE",
											"INVOICE_VIEW",
											"PRINTING_MANAGE",
											"PROJTYPE_MANAGE",
											"RA_MANAGEMENT",
											"REP_LEAVE",
											"ROLE_ASSIGN",
											"ROLE_MAN",
											"VEHICLE_ASSIGN",
											"VEH_MANAGE"
										);

	foreach ($roles as $role)	{
		foreach ($actions[$role] as $action)	{
			$insert = q("INSERT INTO role_action (companyid,roleid,actionid) VALUES (0,(SELECT id FROM roles WHERE role = '".$role."' AND companyid = 0),(SELECT id FROM actions WHERE action = '".$action."'))");
		}
	}

    $insert = q("INSERT INTO role_action (companyid,roleid,actionid) SELECT ".$companyID.",roleid,actionid FROM role_action WHERE companyid = 0");

    $roles = q("SELECT id,role FROM roles WHERE companyid = 0 ORDER BY id");

    if (is_array($roles))       {
        foreach ($roles as $r)  {
            $oldRoleID = $r[0];
            $newRoleID = q("SELECT id FROM roles WHERE companyid = '".$companyID."' AND role = '".$r[1]."'");

            $update = q("UPDATE role_action SET roleid = '".$newRoleID."' WHERE companyid = '".$companyID."' AND roleid = '".$oldRoleID."'");
        }
    }

    $insert = q("INSERT INTO user_role(companyid,userid,roleid) VALUES ".
                    "('".$companyID."','".$userID1."',(SELECT id FROM roles WHERE role = 'MAN' AND companyid = '".$companyID."')),".
                    "('".$companyID."','".$userID1."',(SELECT id FROM roles WHERE role = 'PM' AND companyid = '".$companyID."')),".
                    "('".$companyID."','".$userID1."',(SELECT id FROM roles WHERE role = 'FM' AND companyid = '".$companyID."')),".
                    "('".$companyID."','".$userID1."',(SELECT id FROM roles WHERE role = 'TM' AND companyid = '".$companyID."')),".
                    "('".$companyID."','".$userID1."',(SELECT id FROM roles WHERE role = 'CL' AND companyid = '".$companyID."')),".
                    "('".$companyID."','".$userID2."',(SELECT id FROM roles WHERE role = 'PM' AND companyid = '".$companyID."')),".
                    "('".$companyID."','".$userID2."',(SELECT id FROM roles WHERE role = 'FM' AND companyid = '".$companyID."')),".
                    "('".$companyID."','".$userID2."',(SELECT id FROM roles WHERE role = 'TM' AND companyid = '".$companyID."')),".
                    "('".$companyID."','".$userID2."',(SELECT id FROM roles WHERE role = 'CL' AND companyid = '".$companyID."')),".
                    "('".$companyID."','".$userID3."',(SELECT id FROM roles WHERE role = 'FM' AND companyid = '".$companyID."')),".
                    "('".$companyID."','".$userID3."',(SELECT id FROM roles WHERE role = 'CL' AND companyid = '".$companyID."')),".
                    "('".$companyID."','".$userID4."',(SELECT id FROM roles WHERE role = 'CL' AND companyid = '".$companyID."'))");

    $insert = q("INSERT INTO Company_Users (user_id,company_id) VALUES ".
                    "('".$userID."','".$companyID."'),".
                    "('".$userID1."','".$companyID."'),".
                    "('".$userID2."','".$companyID."'),".
                    "('".$userID3."','".$companyID."'),".
                    "('".$userID4."','".$companyID."')");

    $insert = q("INSERT INTO user_rates(companyid,userid,rate) VALUES ".
                    "(".$companyID.",".$userID1.",'300.00'),".
                    "(".$companyID.",".$userID1.",'350.00'),".
                    "(".$companyID.",".$userID1.",'400.00'),".
                    "(".$companyID.",".$userID1.",'450.00'),".
                    "(".$companyID.",".$userID2.",'250.00'),".
                    "(".$companyID.",".$userID2.",'300.00'),".
                    "(".$companyID.",".$userID2.",'350.00'),".
                    "(".$companyID.",".$userID2.",'400.00'),".
                    "(".$companyID.",".$userID3.",'200.00'),".
                    "(".$companyID.",".$userID3.",'250.00'),".
                    "(".$companyID.",".$userID3.",'300.00'),".
                    "(".$companyID.",".$userID3.",'350.00'),".
                    "(".$companyID.",".$userID4.",'150.00'),".
                    "(".$companyID.",".$userID4.",'200.00'),".
                    "(".$companyID.",".$userID4.",'250.00'),".
                    "(".$companyID.",".$userID4.",'300.00')");

    $insert = q("INSERT INTO user_leave(companyid,userid) VALUES ".
                    "('".$companyID."','".$userID1."'),".
                    "('".$companyID."','".$userID2."'),".
                    "('".$companyID."','".$userID3."'),".
                    "('".$companyID."','".$userID4."')");

    $insert = q("INSERT INTO seen (user_id,comingSoon,intro,tutorial,ctrlF5,social,blog) VALUES ".
                    "('".$userID1."',1,1,1,1,1,1),".
                    "('".$userID2."',1,1,1,1,1,1),".
                    "('".$userID3."',1,1,1,1,1,1),".
                    "('".$userID4."',1,1,1,1,1,1)");

    $insert = q("INSERT INTO ProjectTypes (type,company_id) VALUES ".
                    "('Development','".$companyID."'),".
                    "('Monitoring','".$companyID."'),".
                    "('Sales','".$companyID."'),".
                    "('Support','".$companyID."')");

    $typeID1 = q("SELECT id FROM ProjectTypes WHERE type = 'Development' AND company_id = '".$companyID."'");
    $typeID2 = q("SELECT id FROM ProjectTypes WHERE type = 'Monitoring' AND company_id = '".$companyID."'");
    $typeID3 = q("SELECT id FROM ProjectTypes WHERE type = 'Sales' AND company_id = '".$companyID."'");
    $typeID4 = q("SELECT id FROM ProjectTypes WHERE type = 'Support' AND company_id = '".$companyID."'");

    $startDate = date("Y-m-d");
    $startDate1 = getPreviousMonthStart(5);
    $startDate2 = getPreviousMonthStart(4);
    $startDate3 = getPreviousMonthStart(3);
    $startDate4 = getPreviousMonthStart(2);
    $startDate5 = getPreviousMonthStart(1);

    $invoiceCycleID = q("SELECT id FROM dropdowns WHERE categoryID = (SELECT id FROM categories WHERE code = 'invoiceCycle') AND name = '1 Month'");
    $travelRateID = q("SELECT id FROM dropdowns WHERE categoryID = (SELECT id FROM categories WHERE code = 'travelRate') AND name = '1.0 x Professional Rate'");

    $insert = q("INSERT INTO Project (name,reg_date,submitted_by_id,type_id,due_date,company_id,completed,status,invoiceCycle,travelRate,deleted,dateCreated,createdBy,dateUpdated,updatedBy) VALUES ".
                    "('Website 1 Development','".$startDate1."',0,".$typeID1.",'".getNextMonthStart($startDate, rand(8,36))."','".$companyID."',0,'active','".$invoiceCycleID."','".$travelRateID."',0,'".$startDate1."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws'),".
                    "('Website 2 Development','".$startDate2."',0,".$typeID1.",'".getNextMonthStart($startDate, rand(8,36))."','".$companyID."',0,'active','".$invoiceCycleID."','".$travelRateID."',0,'".$startDate2."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws'),".
                    "('Website 3 Development','".$startDate3."',0,".$typeID1.",'','".$companyID."',0,'proposal','".$invoiceCycleID."','".$travelRateID."',0,'".$startDate3."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws'),".
                    "('Construction Monitoring','".$startDate4."',0,".$typeID2.",'".getNextMonthStart($startDate, rand(8,36))."','".$companyID."',0,'active','".$invoiceCycleID."','".$travelRateID."',0,'".$startDate4."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws'),".
                    "('Scada Systems Monitoring','".$startDate5."',0,".$typeID2.",'','".$companyID."',0,'proposal','".$invoiceCycleID."','".$travelRateID."',0,'".$startDate5."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws'),".
                    "('Product Sales','".$startDate1."',0,".$typeID3.",'".getNextMonthStart($startDate, rand(8,36))."','".$companyID."',0,'active','".$invoiceCycleID."','".$travelRateID."',0,'".$startDate1."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws'),".
                    "('Construction Support','".$startDate2."',0,".$typeID4.",'".getNextMonthStart($startDate, rand(8,36))."','".$companyID."',0,'active','".$invoiceCycleID."','".$travelRateID."',0,'".$startDate2."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws'),".
                    "('Scada Systems Support','".$startDate3."',0,".$typeID4.",'".getNextMonthStart($startDate, rand(8,36))."','".$companyID."',0,'active','".$invoiceCycleID."','".$travelRateID."',0,'".$startDate3."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws'),".
                    "('Technical Support','".$startDate4."',0,".$typeID4.",'".getNextMonthStart($startDate, rand(8,36))."','".$companyID."',0,'active','".$invoiceCycleID."','".$travelRateID."',0,'".$startDate4."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws'),".
                    "('Website Support','".$startDate5."',0,".$typeID4.",'".getNextMonthStart($startDate, rand(8,36))."','".$companyID."',0,'active','".$invoiceCycleID."','".$travelRateID."',0,'".$startDate5."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws')");

    $projectID1 = q("SELECT id FROM Project WHERE company_id = '".$companyID."' AND name = 'Website 1 Development'");
    $projectID2 = q("SELECT id FROM Project WHERE company_id = '".$companyID."' AND name = 'Website 2 Development'");
    $projectID3 = q("SELECT id FROM Project WHERE company_id = '".$companyID."' AND name = 'Website 3 Development'");
    $projectID4 = q("SELECT id FROM Project WHERE company_id = '".$companyID."' AND name = 'Construction Monitoring'");
    $projectID5 = q("SELECT id FROM Project WHERE company_id = '".$companyID."' AND name = 'Scada Systems Monitoring'");
    $projectID6 = q("SELECT id FROM Project WHERE company_id = '".$companyID."' AND name = 'Product Sales'");
    $projectID7 = q("SELECT id FROM Project WHERE company_id = '".$companyID."' AND name = 'Construction Support'");
    $projectID8 = q("SELECT id FROM Project WHERE company_id = '".$companyID."' AND name = 'Scada Systems Support'");
    $projectID9 = q("SELECT id FROM Project WHERE company_id = '".$companyID."' AND name = 'Technical Support'");
    $projectID10 = q("SELECT id FROM Project WHERE company_id = '".$companyID."' AND name = 'Website Support'");

    $insert = q("INSERT INTO companiesOnTeam (project_id,company_id,total_budget,consulting,expenses,diverse_income,vat,buffer,status,teamManage,locked,invoiceCycle,deleted,overBudget,OBEmailSent,dateCreated,createdBy,dateUpdated,updatedBy) VALUES ".
                    "('".$projectID1."','".$companyID."','100000.00','75000.00','25000.00','5000.00','14','10',1,2,0,0,0,0,0,'".$startDate1."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws'),".
                    "('".$projectID2."','".$companyID."','900000.00','750000.00','250000.00','50000.00','14','10',1,2,0,0,0,0,0,'".$startDate2."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws'),".
                    "('".$projectID3."','".$companyID."','600000.00','500000.00','100000.00','40000.00','14','0',1,2,0,0,0,0,0,'".$startDate3."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws'),".
                    "('".$projectID4."','".$companyID."','2000000.00','1750000.00','250000.00','156000.00','14','10',1,2,0,0,0,0,0,'".$startDate4."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws'),".
                    "('".$projectID5."','".$companyID."','1500000.00','1250000.00','250000.00','140000.00','14','0',1,2,0,0,0,0,0,'".$startDate5."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws'),".
                    "('".$projectID6."','".$companyID."','100000.00','55000.00','45000.00','9000.00','14','10',1,2,0,0,0,0,0,'".$startDate1."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws'),".
                    "('".$projectID7."','".$companyID."','1000000.00','1000000.00','','','14','10',1,2,0,0,0,0,0,'".$startDate2."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws'),".
                    "('".$projectID8."','".$companyID."','100000.00','100000.00','','','14','10',1,2,0,0,0,0,0,'".$startDate3."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws'),".
                    "('".$projectID9."','".$companyID."','250000.00','250000.00','','','14','10',1,2,0,0,0,0,0,'".$startDate4."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws'),".
                    "('".$projectID10."','".$companyID."','10000.00','10000.00','','','14','10',1,2,0,0,0,0,0,'".$startDate5."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws')");

    $userRateID1 = q("SELECT id,id FROM user_rates WHERE companyid = '".$companyID."' AND userid = '".$userID1."'");
    $userRateID2 = q("SELECT id,id FROM user_rates WHERE companyid = '".$companyID."' AND userid = '".$userID2."'");
    $userRateID3 = q("SELECT id,id FROM user_rates WHERE companyid = '".$companyID."' AND userid = '".$userID3."'");
    $userRateID4 = q("SELECT id,id FROM user_rates WHERE companyid = '".$companyID."' AND userid = '".$userID4."'");

    $insert = q("INSERT INTO Project_User (company_id,user_id,rateID,user_budget,project_id,manager,deleted,dateCreated,createdBy,dateUpdated,updatedBy,spm_manager) VALUES ".
                    "('".$companyID."','".$userID1."','".$userRateID1[rand(0,(count($userRateID1) - 1))][0]."','','".$projectID1."',1,0,'".$startDate1."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws',0),".
                    "('".$companyID."','".$userID1."','".$userRateID1[rand(0,(count($userRateID1) - 1))][0]."','','".$projectID2."',1,0,'".$startDate2."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws',0),".
                    "('".$companyID."','".$userID1."','".$userRateID1[rand(0,(count($userRateID1) - 1))][0]."','','".$projectID3."',1,0,'".$startDate3."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws',0),".
                    "('".$companyID."','".$userID1."','".$userRateID1[rand(0,(count($userRateID1) - 1))][0]."','','".$projectID4."',1,0,'".$startDate4."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws',0),".
                    "('".$companyID."','".$userID1."','".$userRateID1[rand(0,(count($userRateID1) - 1))][0]."','','".$projectID5."',1,0,'".$startDate5."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws',0),".
                    "('".$companyID."','".$userID1."','".$userRateID1[rand(0,(count($userRateID1) - 1))][0]."','','".$projectID6."',1,0,'".$startDate1."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws',0),".
                    "('".$companyID."','".$userID1."','".$userRateID1[rand(0,(count($userRateID1) - 1))][0]."','','".$projectID7."',1,0,'".$startDate2."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws',0),".
                    "('".$companyID."','".$userID1."','".$userRateID1[rand(0,(count($userRateID1) - 1))][0]."','','".$projectID8."',1,0,'".$startDate3."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws',0),".
                    "('".$companyID."','".$userID1."','".$userRateID1[rand(0,(count($userRateID1) - 1))][0]."','','".$projectID9."',1,0,'".$startDate4."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws',0),".
                    "('".$companyID."','".$userID1."','".$userRateID1[rand(0,(count($userRateID1) - 1))][0]."','','".$projectID10."',1,0,'".$startDate5."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws',0),".
                    "('".$companyID."','".$userID2."','".$userRateID2[rand(0,(count($userRateID2) - 1))][0]."','','".$projectID1."',".rand(0,1).",0,'".$startDate1."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws',0),".
                    "('".$companyID."','".$userID2."','".$userRateID2[rand(0,(count($userRateID2) - 1))][0]."','','".$projectID2."',".rand(0,1).",0,'".$startDate2."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws',0),".
                    "('".$companyID."','".$userID2."','".$userRateID2[rand(0,(count($userRateID2) - 1))][0]."','','".$projectID3."',".rand(0,1).",0,'".$startDate3."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws',0),".
                    "('".$companyID."','".$userID2."','".$userRateID2[rand(0,(count($userRateID2) - 1))][0]."','','".$projectID4."',".rand(0,1).",0,'".$startDate4."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws',0),".
                    "('".$companyID."','".$userID2."','".$userRateID2[rand(0,(count($userRateID2) - 1))][0]."','','".$projectID5."',".rand(0,1).",0,'".$startDate5."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws',0),".
                    "('".$companyID."','".$userID2."','".$userRateID2[rand(0,(count($userRateID2) - 1))][0]."','','".$projectID6."',".rand(0,1).",0,'".$startDate1."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws',0),".
                    "('".$companyID."','".$userID2."','".$userRateID2[rand(0,(count($userRateID2) - 1))][0]."','','".$projectID7."',".rand(0,1).",0,'".$startDate2."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws',0),".
                    "('".$companyID."','".$userID2."','".$userRateID2[rand(0,(count($userRateID2) - 1))][0]."','','".$projectID8."',".rand(0,1).",0,'".$startDate3."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws',0),".
                    "('".$companyID."','".$userID2."','".$userRateID2[rand(0,(count($userRateID2) - 1))][0]."','','".$projectID9."',".rand(0,1).",0,'".$startDate4."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws',0),".
                    "('".$companyID."','".$userID2."','".$userRateID2[rand(0,(count($userRateID2) - 1))][0]."','','".$projectID10."',".rand(0,1).",0,'".$startDate5."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws',0),".
                    "('".$companyID."','".$userID3."','".$userRateID3[rand(0,(count($userRateID3) - 1))][0]."','','".$projectID1."',0,0,'".$startDate1."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws',0),".
                    "('".$companyID."','".$userID3."','".$userRateID3[rand(0,(count($userRateID3) - 1))][0]."','','".$projectID2."',0,0,'".$startDate2."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws',0),".
                    "('".$companyID."','".$userID3."','".$userRateID3[rand(0,(count($userRateID3) - 1))][0]."','','".$projectID3."',0,0,'".$startDate3."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws',0),".
                    "('".$companyID."','".$userID3."','".$userRateID3[rand(0,(count($userRateID3) - 1))][0]."','','".$projectID4."',0,0,'".$startDate4."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws',0),".
                    "('".$companyID."','".$userID3."','".$userRateID3[rand(0,(count($userRateID3) - 1))][0]."','','".$projectID5."',0,0,'".$startDate5."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws',0),".
                    "('".$companyID."','".$userID3."','".$userRateID3[rand(0,(count($userRateID3) - 1))][0]."','','".$projectID6."',0,0,'".$startDate1."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws',0),".
                    "('".$companyID."','".$userID3."','".$userRateID3[rand(0,(count($userRateID3) - 1))][0]."','','".$projectID7."',0,0,'".$startDate2."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws',0),".
                    "('".$companyID."','".$userID3."','".$userRateID3[rand(0,(count($userRateID3) - 1))][0]."','','".$projectID8."',0,0,'".$startDate3."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws',0),".
                    "('".$companyID."','".$userID3."','".$userRateID3[rand(0,(count($userRateID3) - 1))][0]."','','".$projectID9."',0,0,'".$startDate4."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws',0),".
                    "('".$companyID."','".$userID3."','".$userRateID3[rand(0,(count($userRateID3) - 1))][0]."','','".$projectID10."',0,0,'".$startDate5."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws',0),".
                    "('".$companyID."','".$userID4."','".$userRateID4[rand(0,(count($userRateID4) - 1))][0]."','','".$projectID1."',0,0,'".$startDate1."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws',0),".
                    "('".$companyID."','".$userID4."','".$userRateID4[rand(0,(count($userRateID4) - 1))][0]."','','".$projectID2."',0,0,'".$startDate2."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws',0),".
                    "('".$companyID."','".$userID4."','".$userRateID4[rand(0,(count($userRateID4) - 1))][0]."','','".$projectID3."',0,0,'".$startDate3."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws',0),".
                    "('".$companyID."','".$userID4."','".$userRateID4[rand(0,(count($userRateID4) - 1))][0]."','','".$projectID4."',0,0,'".$startDate4."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws',0),".
                    "('".$companyID."','".$userID4."','".$userRateID4[rand(0,(count($userRateID4) - 1))][0]."','','".$projectID5."',0,0,'".$startDate5."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws',0),".
                    "('".$companyID."','".$userID4."','".$userRateID4[rand(0,(count($userRateID4) - 1))][0]."','','".$projectID6."',0,0,'".$startDate1."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws',0),".
                    "('".$companyID."','".$userID4."','".$userRateID4[rand(0,(count($userRateID4) - 1))][0]."','','".$projectID7."',0,0,'".$startDate2."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws',0),".
                    "('".$companyID."','".$userID4."','".$userRateID4[rand(0,(count($userRateID4) - 1))][0]."','','".$projectID8."',0,0,'".$startDate3."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws',0),".
                    "('".$companyID."','".$userID4."','".$userRateID4[rand(0,(count($userRateID4) - 1))][0]."','','".$projectID9."',0,0,'".$startDate4."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws',0),".
                    "('".$companyID."','".$userID4."','".$userRateID4[rand(0,(count($userRateID4) - 1))][0]."','','".$projectID10."',0,0,'".$startDate5."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws',0)");

    $insert = q("INSERT INTO Vehicle (type,rate,company_id,deleted) VALUES ".
                    "('Toyota Hilux 3.0','4.00','".$companyID."',0),".
                    "('BMW350d','3.80','".$companyID."',0),".
                    "('Volkswagen Polo','3.30','".$companyID."',0)");

    $vehicleID1 = q("SELECT id FROM Vehicle WHERE company_id = '".$companyID."' AND type = 'Toyota Hilux 3.0'");
    $vehicleID2 = q("SELECT id FROM Vehicle WHERE company_id = '".$companyID."' AND type = 'BMW350d'");
    $vehicleID3 = q("SELECT id FROM Vehicle WHERE company_id = '".$companyID."' AND type = 'Volkswagen Polo'");

    $startDate1 = getDates(getPreviousMonthStart(5),rand(0,15));
    $startDate2 = getDates(getPreviousMonthStart(4),rand(0,15));
    $startDate3 = getDates(getPreviousMonthStart(3),rand(0,15));

    $insert = q("INSERT INTO StartBalance (date,amount,company_id) VALUES ".
                    "('$startDate1','".rand(1000000,1500000)."','".$companyID."'),".
                    "('$startDate2','".rand(1500000,2000000)."','".$companyID."'),".
                    "('$startDate3','".rand(1700000,2200000)."','".$companyID."')");

    $insert = q("INSERT INTO Category (name,company_id) VALUES ".
                    "('Debit Orders','".$companyID."'),".
                    "('Other Expenses','".$companyID."'),".
                    "('Project Costs','".$companyID."'),".
                    "('Vehicles','".$companyID."')");

    $categoryID1 = q("SELECT id FROM Category WHERE company_id = '".$companyID."' AND name = 'Debit Orders'");
    $categoryID2 = q("SELECT id FROM Category WHERE company_id = '".$companyID."' AND name = 'Other Expenses'");
    $categoryID3 = q("SELECT id FROM Category WHERE company_id = '".$companyID."' AND name = 'Project Costs'");
    $categoryID4 = q("SELECT id FROM Category WHERE company_id = '".$companyID."' AND name = 'Vehicles'");

    $insert = q("INSERT INTO Elements (parent_id,name,status) VALUES ".
                    "('".$categoryID1."','Line Rentals',0),".
                    "('".$categoryID1."','Medical Aid',0),".
                    "('".$categoryID1."','Vehicle Tracking',0),".
                    "('".$categoryID2."','Office Maintenance',0),".
                    "('".$categoryID2."','Marketing',0),".
                    "('".$categoryID2."','Business Insurance',0),".
                    "('".$categoryID3."','External Consultants',0),".
                    "('".$categoryID3."','Postage & Printing',0),".
                    "('".$categoryID3."','S&T',0),".
                    "('".$categoryID4."','Fuel',0),".
                    "('".$categoryID4."','Maintenance',0),".
                    "('".$categoryID4."','Insurance',0)");

    $elementID1 = q("SELECT id FROM Elements WHERE parent_id = '".$categoryID1."' AND name = 'Line Rentals'");
    $elementID2 = q("SELECT id FROM Elements WHERE parent_id = '".$categoryID1."' AND name = 'Medical Aid'");
    $elementID3 = q("SELECT id FROM Elements WHERE parent_id = '".$categoryID1."' AND name = 'Vehicle Tracking'");
    $elementID4 = q("SELECT id FROM Elements WHERE parent_id = '".$categoryID2."' AND name = 'Office Maintenance'");
    $elementID5 = q("SELECT id FROM Elements WHERE parent_id = '".$categoryID2."' AND name = 'Marketing'");
    $elementID6 = q("SELECT id FROM Elements WHERE parent_id = '".$categoryID2."' AND name = 'Business Insurance'");
    $elementID7 = q("SELECT id FROM Elements WHERE parent_id = '".$categoryID3."' AND name = 'External Consultants'");
    $elementID8 = q("SELECT id FROM Elements WHERE parent_id = '".$categoryID3."' AND name = 'Postage & Printing'");
    $elementID9 = q("SELECT id FROM Elements WHERE parent_id = '".$categoryID3."' AND name = 'S&T'");
    $elementID10 = q("SELECT id FROM Elements WHERE parent_id = '".$categoryID4."' AND name = 'Fuel'");
    $elementID11 = q("SELECT id FROM Elements WHERE parent_id = '".$categoryID4."' AND name = 'Maintenance'");
    $elementID12 = q("SELECT id FROM Elements WHERE parent_id = '".$categoryID4."' AND name = 'Insurance'");

    $insert = q("INSERT INTO Budget (year,month,element_id,planned,amount,actual,due_date,company_id) VALUES ".
                    //  3 Months Back
                    "('".date("Y", strtotime("-3 months"))."','".date("m", strtotime("-3 months"))."',$elementID1,'12000.00','12000.00',1,'".getMonthEnd(date("Y", strtotime("-3 months"))."-".date("m", strtotime("-3 months")))."','".$companyID."'),".
                    "('".date("Y", strtotime("-3 months"))."','".date("m", strtotime("-3 months"))."',$elementID2,'7890.00','7890.00',1,'".getMonthEnd(date("Y", strtotime("-3 months"))."-".date("m", strtotime("-3 months")))."','".$companyID."'),".
                    "('".date("Y", strtotime("-3 months"))."','".date("m", strtotime("-3 months"))."',$elementID3,'980.00','980.00',1,'".getMonthEnd(date("Y", strtotime("-3 months"))."-".date("m", strtotime("-3 months")))."','".$companyID."'),".
                    "('".date("Y", strtotime("-3 months"))."','".date("m", strtotime("-3 months"))."',$elementID4,'2600.00','2600.00',1,'".getMonthEnd(date("Y", strtotime("-3 months"))."-".date("m", strtotime("-3 months")))."','".$companyID."'),".
                    "('".date("Y", strtotime("-3 months"))."','".date("m", strtotime("-3 months"))."',$elementID5,'1200.00','1200.00',1,'".getMonthEnd(date("Y", strtotime("-3 months"))."-".date("m", strtotime("-3 months")))."','".$companyID."'),".
                    "('".date("Y", strtotime("-3 months"))."','".date("m", strtotime("-3 months"))."',$elementID6,'2500.00','2500.00',1,'".getMonthEnd(date("Y", strtotime("-3 months"))."-".date("m", strtotime("-3 months")))."','".$companyID."'),".
                    "('".date("Y", strtotime("-3 months"))."','".date("m", strtotime("-3 months"))."',$elementID7,'45000.00','45000.00',1,'".getMonthEnd(date("Y", strtotime("-3 months"))."-".date("m", strtotime("-3 months")))."','".$companyID."'),".
                    "('".date("Y", strtotime("-3 months"))."','".date("m", strtotime("-3 months"))."',$elementID8,'7800.00','7800.00',1,'".getMonthEnd(date("Y", strtotime("-3 months"))."-".date("m", strtotime("-3 months")))."','".$companyID."'),".
                    "('".date("Y", strtotime("-3 months"))."','".date("m", strtotime("-3 months"))."',$elementID9,'1890.00','1890.00',1,'".getMonthEnd(date("Y", strtotime("-3 months"))."-".date("m", strtotime("-3 months")))."','".$companyID."'),".
                    "('".date("Y", strtotime("-3 months"))."','".date("m", strtotime("-3 months"))."',$elementID10,'2800.00','2800.00',1,'".getMonthEnd(date("Y", strtotime("-3 months"))."-".date("m", strtotime("-3 months")))."','".$companyID."'),".
                    "('".date("Y", strtotime("-3 months"))."','".date("m", strtotime("-3 months"))."',$elementID11,'2600.00','2600.00',1,'".getMonthEnd(date("Y", strtotime("-3 months"))."-".date("m", strtotime("-3 months")))."','".$companyID."'),".
                    "('".date("Y", strtotime("-3 months"))."','".date("m", strtotime("-3 months"))."',$elementID12,'1800.00','1800.00',1,'".getMonthEnd(date("Y", strtotime("-3 months"))."-".date("m", strtotime("-3 months")))."','".$companyID."'),".
                    //  2 Months Back
                    "('".date("Y", strtotime("-2 months"))."','".date("m", strtotime("-2 months"))."',$elementID1,'12750.00','12750.00',1,'".getMonthEnd(date("Y", strtotime("-2 months"))."-".date("m", strtotime("-2 months")))."','".$companyID."'),".
                    "('".date("Y", strtotime("-2 months"))."','".date("m", strtotime("-2 months"))."',$elementID2,'7890.00','7890.00',1,'".getMonthEnd(date("Y", strtotime("-2 months"))."-".date("m", strtotime("-2 months")))."','".$companyID."'),".
                    "('".date("Y", strtotime("-2 months"))."','".date("m", strtotime("-2 months"))."',$elementID3,'980.00','980.00',1,'".getMonthEnd(date("Y", strtotime("-2 months"))."-".date("m", strtotime("-2 months")))."','".$companyID."'),".
                    "('".date("Y", strtotime("-2 months"))."','".date("m", strtotime("-2 months"))."',$elementID4,'2600.00','3200.00',1,'".getMonthEnd(date("Y", strtotime("-2 months"))."-".date("m", strtotime("-2 months")))."','".$companyID."'),".
                    "('".date("Y", strtotime("-2 months"))."','".date("m", strtotime("-2 months"))."',$elementID5,'1200.00','1200.00',1,'".getMonthEnd(date("Y", strtotime("-2 months"))."-".date("m", strtotime("-2 months")))."','".$companyID."'),".
                    "('".date("Y", strtotime("-2 months"))."','".date("m", strtotime("-2 months"))."',$elementID6,'2500.00','2500.00',1,'".getMonthEnd(date("Y", strtotime("-2 months"))."-".date("m", strtotime("-2 months")))."','".$companyID."'),".
                    "('".date("Y", strtotime("-2 months"))."','".date("m", strtotime("-2 months"))."',$elementID7,'35000.00','35000.00',1,'".getMonthEnd(date("Y", strtotime("-2 months"))."-".date("m", strtotime("-2 months")))."','".$companyID."'),".
                    "('".date("Y", strtotime("-2 months"))."','".date("m", strtotime("-2 months"))."',$elementID8,'7800.00','7800.00',1,'".getMonthEnd(date("Y", strtotime("-2 months"))."-".date("m", strtotime("-2 months")))."','".$companyID."'),".
                    "('".date("Y", strtotime("-2 months"))."','".date("m", strtotime("-2 months"))."',$elementID9,'1890.00','1890.00',1,'".getMonthEnd(date("Y", strtotime("-2 months"))."-".date("m", strtotime("-2 months")))."','".$companyID."'),".
                    "('".date("Y", strtotime("-2 months"))."','".date("m", strtotime("-2 months"))."',$elementID10,'3200.00','3200.00',1,'".getMonthEnd(date("Y", strtotime("-2 months"))."-".date("m", strtotime("-2 months")))."','".$companyID."'),".
                    "('".date("Y", strtotime("-2 months"))."','".date("m", strtotime("-2 months"))."',$elementID11,'2600.00','2600.00',1,'".getMonthEnd(date("Y", strtotime("-2 months"))."-".date("m", strtotime("-2 months")))."','".$companyID."'),".
                    "('".date("Y", strtotime("-2 months"))."','".date("m", strtotime("-2 months"))."',$elementID12,'1800.00','1800.00',1,'".getMonthEnd(date("Y", strtotime("-2 months"))."-".date("m", strtotime("-2 months")))."','".$companyID."'),".
                    //  1 Month Back
                    "('".date("Y", strtotime("-1 months"))."','".date("m", strtotime("-1 months"))."',$elementID1,'14500.00','14500.00',1,'".getMonthEnd(date("Y", strtotime("-1 months"))."-".date("m", strtotime("-1 months")))."','".$companyID."'),".
                    "('".date("Y", strtotime("-1 months"))."','".date("m", strtotime("-1 months"))."',$elementID2,'8980.00','8980.00',1,'".getMonthEnd(date("Y", strtotime("-1 months"))."-".date("m", strtotime("-1 months")))."','".$companyID."'),".
                    "('".date("Y", strtotime("-1 months"))."','".date("m", strtotime("-1 months"))."',$elementID3,'980.00','980.00',1,'".getMonthEnd(date("Y", strtotime("-1 months"))."-".date("m", strtotime("-1 months")))."','".$companyID."'),".
                    "('".date("Y", strtotime("-1 months"))."','".date("m", strtotime("-1 months"))."',$elementID4,'3200.00','3200.00',1,'".getMonthEnd(date("Y", strtotime("-1 months"))."-".date("m", strtotime("-1 months")))."','".$companyID."'),".
                    "('".date("Y", strtotime("-1 months"))."','".date("m", strtotime("-1 months"))."',$elementID5,'1200.00','1200.00',1,'".getMonthEnd(date("Y", strtotime("-1 months"))."-".date("m", strtotime("-1 months")))."','".$companyID."'),".
                    "('".date("Y", strtotime("-1 months"))."','".date("m", strtotime("-1 months"))."',$elementID6,'2500.00','2900.00',1,'".getMonthEnd(date("Y", strtotime("-1 months"))."-".date("m", strtotime("-1 months")))."','".$companyID."'),".
                    "('".date("Y", strtotime("-1 months"))."','".date("m", strtotime("-1 months"))."',$elementID7,'45000.00','45000.00',1,'".getMonthEnd(date("Y", strtotime("-1 months"))."-".date("m", strtotime("-1 months")))."','".$companyID."'),".
                    "('".date("Y", strtotime("-1 months"))."','".date("m", strtotime("-1 months"))."',$elementID8,'7800.00','7800.00',1,'".getMonthEnd(date("Y", strtotime("-1 months"))."-".date("m", strtotime("-1 months")))."','".$companyID."'),".
                    "('".date("Y", strtotime("-1 months"))."','".date("m", strtotime("-1 months"))."',$elementID9,'1890.00','1890.00',1,'".getMonthEnd(date("Y", strtotime("-1 months"))."-".date("m", strtotime("-1 months")))."','".$companyID."'),".
                    "('".date("Y", strtotime("-1 months"))."','".date("m", strtotime("-1 months"))."',$elementID10,'3200.00','3200.00',1,'".getMonthEnd(date("Y", strtotime("-1 months"))."-".date("m", strtotime("-1 months")))."','".$companyID."'),".
                    "('".date("Y", strtotime("-1 months"))."','".date("m", strtotime("-1 months"))."',$elementID11,'2600.00','2600.00',1,'".getMonthEnd(date("Y", strtotime("-1 months"))."-".date("m", strtotime("-1 months")))."','".$companyID."'),".
                    "('".date("Y", strtotime("-1 months"))."','".date("m", strtotime("-1 months"))."',$elementID12,'1800.00','1800.00',1,'".getMonthEnd(date("Y", strtotime("-1 months"))."-".date("m", strtotime("-1 months")))."','".$companyID."'),".
                    //  Current Month
                    "('".date("Y")."','".date("m")."',$elementID1,'14500.00','14500.00',0,'".getMonthEnd(date("Y")."-".date("m"))."','".$companyID."'),".
                    "('".date("Y")."','".date("m")."',$elementID2,'8980.00','8980.00',0,'".getMonthEnd(date("Y")."-".date("m"))."','".$companyID."'),".
                    "('".date("Y")."','".date("m")."',$elementID3,'980.00','980.00',0,'".getMonthEnd(date("Y")."-".date("m"))."','".$companyID."'),".
                    "('".date("Y")."','".date("m")."',$elementID4,'3200.00','3200.00',0,'".getMonthEnd(date("Y")."-".date("m"))."','".$companyID."'),".
                    "('".date("Y")."','".date("m")."',$elementID5,'2200.00','2200.00',0,'".getMonthEnd(date("Y")."-".date("m"))."','".$companyID."'),".
                    "('".date("Y")."','".date("m")."',$elementID6,'2900.00','2900.00',0,'".getMonthEnd(date("Y")."-".date("m"))."','".$companyID."'),".
                    "('".date("Y")."','".date("m")."',$elementID7,'37000.00','37000.00',0,'".getMonthEnd(date("Y")."-".date("m"))."','".$companyID."'),".
                    "('".date("Y")."','".date("m")."',$elementID8,'6800.00','6800.00',0,'".getMonthEnd(date("Y")."-".date("m"))."','".$companyID."'),".
                    "('".date("Y")."','".date("m")."',$elementID9,'1890.00','1890.00',0,'".getMonthEnd(date("Y")."-".date("m"))."','".$companyID."'),".
                    "('".date("Y")."','".date("m")."',$elementID10,'3600.00','3600.00',0,'".getMonthEnd(date("Y")."-".date("m"))."','".$companyID."'),".
                    "('".date("Y")."','".date("m")."',$elementID11,'2600.00','2600.00',0,'".getMonthEnd(date("Y")."-".date("m"))."','".$companyID."'),".
                    "('".date("Y")."','".date("m")."',$elementID12,'1800.00','1800.00',0,'".getMonthEnd(date("Y")."-".date("m"))."','".$companyID."')");

    $insert = q("INSERT INTO Module_Users (company_id,module_id) VALUES ".
                    "('".$companyID."',1)");

    $insert = q("INSERT INTO ActivityTypes (type,company_id,active,dateCreated,createdBy,dateUpdated,updatedBy) VALUES ".
                    "('Development','".$companyID."',1,'".getPreviousMonthStart(3)."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws'),".
                    "('Project Management','".$companyID."',1,'".getPreviousMonthStart(3)."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws'),".
                    "('Sales','".$companyID."',1,'".getPreviousMonthStart(3)."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws'),".
                    "('Support','".$companyID."',1,'".getPreviousMonthStart(3)."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws')");

    $activityType1 = q("SELECT id FROM ActivityTypes WHERE company_id = '".$companyID."' AND type = 'Development'");
    $activityType2 = q("SELECT id FROM ActivityTypes WHERE company_id = '".$companyID."' AND type = 'Project Management'");
    $activityType3 = q("SELECT id FROM ActivityTypes WHERE company_id = '".$companyID."' AND type = 'Sales'");
    $activityType4 = q("SELECT id FROM ActivityTypes WHERE company_id = '".$companyID."' AND type = 'Support'");

    $insert = q("INSERT INTO Activities (parent_id,name,active,dateCreated,createdBy,dateUpdated,updatedBy) VALUES ".
                    "('".$activityType1."','Database',1,'".getPreviousMonthStart(3)."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws'),".
                    "('".$activityType1."','Scada',1,'".getPreviousMonthStart(3)."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws'),".
                    "('".$activityType1."','Software',1,'".getPreviousMonthStart(3)."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws'),".
                    "('".$activityType1."','Web',1,'".getPreviousMonthStart(3)."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws'),".
                    "('".$activityType2."','Budget Planning',1,'".getPreviousMonthStart(3)."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws'),".
                    "('".$activityType2."','Correspondence',1,'".getPreviousMonthStart(3)."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws'),".
                    "('".$activityType2."','Meeting',1,'".getPreviousMonthStart(3)."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws'),".
                    "('".$activityType2."','Procurement',1,'".getPreviousMonthStart(3)."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws'),".
                    "('".$activityType2."','Resource Planning',1,'".getPreviousMonthStart(3)."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws'),".
                    "('".$activityType2."','Strategy Planning',1,'".getPreviousMonthStart(3)."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws'),".
                    "('".$activityType2."','Work Breakdown Structure',1,'".getPreviousMonthStart(3)."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws'),".
                    "('".$activityType3."','Private',1,'".getPreviousMonthStart(3)."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws'),".
                    "('".$activityType3."','Support',1,'".getPreviousMonthStart(3)."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws'),".
                    "('".$activityType4."','E-Mail',1,'".getPreviousMonthStart(3)."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws'),".
                    "('".$activityType4."','Network',1,'".getPreviousMonthStart(3)."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws'),".
                    "('".$activityType4."','Scada',1,'".getPreviousMonthStart(3)."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws'),".
                    "('".$activityType4."','Server',1,'".getPreviousMonthStart(3)."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws'),".
                    "('".$activityType4."','Other',1,'".getPreviousMonthStart(3)."','demo2@on-track.ws','".date("Y-m-d")."','demo2@on-track.ws')");

    $activityID1 = q("SELECT id FROM Activities WHERE parent_id = '".$activityType1."' AND name = 'Database'");
    $activityID2 = q("SELECT id FROM Activities WHERE parent_id = '".$activityType1."' AND name = 'Scada'");
    $activityID3 = q("SELECT id FROM Activities WHERE parent_id = '".$activityType1."' AND name = 'Software'");
    $activityID4 = q("SELECT id FROM Activities WHERE parent_id = '".$activityType1."' AND name = 'Web'");
    $activityID5 = q("SELECT id FROM Activities WHERE parent_id = '".$activityType2."' AND name = 'Budget Planning'");
    $activityID6 = q("SELECT id FROM Activities WHERE parent_id = '".$activityType2."' AND name = 'Correspondence'");
    $activityID7 = q("SELECT id FROM Activities WHERE parent_id = '".$activityType2."' AND name = 'Meeting'");
    $activityID8 = q("SELECT id FROM Activities WHERE parent_id = '".$activityType2."' AND name = 'Procurement'");
    $activityID9 = q("SELECT id FROM Activities WHERE parent_id = '".$activityType2."' AND name = 'Resource Planning'");
    $activityID10 = q("SELECT id FROM Activities WHERE parent_id = '".$activityType2."' AND name = 'Strategy Planning'");
    $activityID11 = q("SELECT id FROM Activities WHERE parent_id = '".$activityType2."' AND name = 'Work Breakdown Structure'");
    $activityID12 = q("SELECT id FROM Activities WHERE parent_id = '".$activityType3."' AND name = 'Private'");
    $activityID13 = q("SELECT id FROM Activities WHERE parent_id = '".$activityType3."' AND name = 'Support'");
    $activityID14 = q("SELECT id FROM Activities WHERE parent_id = '".$activityType4."' AND name = 'E-Mail'");
    $activityID15 = q("SELECT id FROM Activities WHERE parent_id = '".$activityType4."' AND name = 'Network'");
    $activityID16 = q("SELECT id FROM Activities WHERE parent_id = '".$activityType4."' AND name = 'Scada'");
    $activityID17 = q("SELECT id FROM Activities WHERE parent_id = '".$activityType4."' AND name = 'Server'");
    $activityID18 = q("SELECT id FROM Activities WHERE parent_id = '".$activityType4."' AND name = 'Other'");

    $insert = q("INSERT INTO DisbursementTypes (type,company_id) VALUES ".
                    "('A3','".$companyID."'),".
                    "('A4','".$companyID."'),".
                    "('A5','".$companyID."')");

    $dibursementType1 = q("SELECT id FROM DisbursementTypes WHERE company_id = '".$companyID."' AND type = 'A3'");
    $dibursementType2 = q("SELECT id FROM DisbursementTypes WHERE company_id = '".$companyID."' AND type = 'A4'");
    $dibursementType3 = q("SELECT id FROM DisbursementTypes WHERE company_id = '".$companyID."' AND type = 'A5'");

    $insert = q("INSERT INTO Disbursements (parent_id,name,cost) VALUES ".
                    "('".$dibursementType1."','Black & White','0.50'),".
                    "('".$dibursementType1."','Colour','1.75'),".
                    "('".$dibursementType2."','Black & White','0.35'),".
                    "('".$dibursementType2."','Colour','1.50'),".
                    "('".$dibursementType3."','Black & White','0.20'),".
                    "('".$dibursementType3."','Colour','1.25')");

    $disbursementID1 = q("SELECT id FROM Disbursements WHERE parent_id = '".$dibursementType1."' AND name = 'Black & White'");
    $disbursementID2 = q("SELECT id FROM Disbursements WHERE parent_id = '".$dibursementType1."' AND name = 'Colour'");
    $disbursementID3 = q("SELECT id FROM Disbursements WHERE parent_id = '".$dibursementType2."' AND name = 'Black & White'");
    $disbursementID4 = q("SELECT id FROM Disbursements WHERE parent_id = '".$dibursementType2."' AND name = 'Colour'");
    $disbursementID5 = q("SELECT id FROM Disbursements WHERE parent_id = '".$dibursementType3."' AND name = 'Black & White'");
    $disbursementID6 = q("SELECT id FROM Disbursements WHERE parent_id = '".$dibursementType3."' AND name = 'Colour'");

    $insert = q("INSERT INTO Resources (resource) VALUES ".
                    "('Demo Company - Boardroom'),".
                    "('Demo Company - Presentation Room')");

    $resourceID1 = q("SELECT id FROM Resources WHERE resource = 'Demo Company - Boardroom'");
    $resourceID2 = q("SELECT id FROM Resources WHERE resource = 'Demo Company - Presentation Room'");

    $insert = q("INSERT INTO ResourceAssign (resource_id,company_id) VALUES ".
                    "('".$resourceID1."','".$companyID."'),".
                    "('".$resourceID2."','".$companyID."')");

    $insert = q("INSERT INTO LoadInvoices (create_date,name,consulting,diverse_income,expenses,vat,due_date,project_id,paid,processed,company_id,area_id) VALUES ".
                    //  3 Months Back
                    "('".date("-Y-m-d", strtotime("-3 months"))."','INV01/".date("Ym", strtotime("-3 months"))."','12500.00','2000.00','1000.00','14','".getMonthEnd(date("Y-m", strtotime("-3 months")))."',$projectID1,1,1,'".$companyID."',0),".
                    "('".date("-Y-m-d", strtotime("-3 months"))."','INV01/".date("Ym", strtotime("-3 months"))."','124500.00','0.00','0.00','14','".getMonthEnd(date("Y-m", strtotime("-3 months")))."',$projectID2,1,1,'".$companyID."',0),".
                    "('".date("-Y-m-d", strtotime("-3 months"))."','INV01/".date("Ym", strtotime("-3 months"))."','250000.00','50000.00','22000.00','14','".getMonthEnd(date("Y-m", strtotime("-3 months")))."',$projectID4,1,1,'".$companyID."',0),".
                    "('".date("-Y-m-d", strtotime("-3 months"))."','INV01/".date("Ym", strtotime("-3 months"))."','15000.00','0.00','0.00','14','".getMonthEnd(date("Y-m", strtotime("-3 months")))."',$projectID6,1,1,'".$companyID."',0),".
                    "('".date("-Y-m-d", strtotime("-3 months"))."','INV01/".date("Ym", strtotime("-3 months"))."','20000.00','0.00','0.00','14','".getMonthEnd(date("Y-m", strtotime("-3 months")))."',$projectID7,1,1,'".$companyID."',0),".
                    "('".date("-Y-m-d", strtotime("-3 months"))."','INV01/".date("Ym", strtotime("-3 months"))."','13500.00','0.00','0.00','14','".getMonthEnd(date("Y-m", strtotime("-3 months")))."',$projectID8,1,1,'".$companyID."',0),".
                    "('".date("-Y-m-d", strtotime("-3 months"))."','INV01/".date("Ym", strtotime("-3 months"))."','52000.00','0.00','0.00','14','".getMonthEnd(date("Y-m", strtotime("-3 months")))."',$projectID9,1,1,'".$companyID."',0),".
                    "('".date("-Y-m-d", strtotime("-3 months"))."','INV01/".date("Ym", strtotime("-3 months"))."','890.00','0.00','0.00','14','".getMonthEnd(date("Y-m", strtotime("-3 months")))."',$projectID10,1,1,'".$companyID."',0),".
                    //  2 Months Back
                    "('".date("-Y-m-d", strtotime("-2 months"))."','INV02/".date("Ym", strtotime("-2 months"))."','16840.00','0.00','0.00','14','".getMonthEnd(date("Y-m", strtotime("-2 months")))."',$projectID1,1,1,'".$companyID."',0),".
                    "('".date("-Y-m-d", strtotime("-2 months"))."','INV02/".date("Ym", strtotime("-2 months"))."','108640.00','25000.00','0.00','14','".getMonthEnd(date("Y-m", strtotime("-2 months")))."',$projectID2,1,1,'".$companyID."',0),".
                    "('".date("-Y-m-d", strtotime("-2 months"))."','INV02/".date("Ym", strtotime("-2 months"))."','218500.00','28650.00','8650.00','14','".getMonthEnd(date("Y-m", strtotime("-2 months")))."',$projectID4,1,1,'".$companyID."',0),".
                    "('".date("-Y-m-d", strtotime("-2 months"))."','INV02/".date("Ym", strtotime("-2 months"))."','14580.00','0.00','0.00','14','".getMonthEnd(date("Y-m", strtotime("-2 months")))."',$projectID7,1,1,'".$companyID."',0),".
                    "('".date("-Y-m-d", strtotime("-2 months"))."','INV02/".date("Ym", strtotime("-2 months"))."','7560.00','0.00','0.00','14','".getMonthEnd(date("Y-m", strtotime("-2 months")))."',$projectID8,1,1,'".$companyID."',0),".
                    "('".date("-Y-m-d", strtotime("-2 months"))."','INV02/".date("Ym", strtotime("-2 months"))."','43500.00','0.00','0.00','14','".getMonthEnd(date("Y-m", strtotime("-2 months")))."',$projectID9,1,1,'".$companyID."',0),".
                    "('".date("-Y-m-d", strtotime("-2 months"))."','INV02/".date("Ym", strtotime("-2 months"))."','985.00','0.00','0.00','14','".getMonthEnd(date("Y-m", strtotime("-2 months")))."',$projectID10,1,1,'".$companyID."',0),".
                    //  1 Month Back
                    "('".date("-Y-m-d", strtotime("-1 months"))."','INV03/".date("Ym", strtotime("-1 months"))."','14750.00','5600.00','2000.00','14','".getMonthEnd(date("Y-m", strtotime("-1 months")))."',$projectID1,1,1,'".$companyID."',0),".
                    "('".date("-Y-m-d", strtotime("-1 months"))."','INV03/".date("Ym", strtotime("-1 months"))."','95680.00','0.00','0.00','14','".getMonthEnd(date("Y-m", strtotime("-1 months")))."',$projectID2,1,1,'".$companyID."',0),".
                    "('".date("-Y-m-d", strtotime("-1 months"))."','INV03/".date("Ym", strtotime("-1 months"))."','198560.00','0.00','0.00','14','".getMonthEnd(date("Y-m", strtotime("-1 months")))."',$projectID4,1,1,'".$companyID."',0),".
                    "('".date("-Y-m-d", strtotime("-1 months"))."','INV03/".date("Ym", strtotime("-1 months"))."','18570.00','4230.00','650.00','14','".getMonthEnd(date("Y-m", strtotime("-1 months")))."',$projectID6,1,1,'".$companyID."',0),".
                    "('".date("-Y-m-d", strtotime("-1 months"))."','INV03/".date("Ym", strtotime("-1 months"))."','9860.00','0.00','0.00','14','".getMonthEnd(date("Y-m", strtotime("-1 months")))."',$projectID8,1,1,'".$companyID."',0),".
                    "('".date("-Y-m-d", strtotime("-1 months"))."','INV03/".date("Ym", strtotime("-1 months"))."','46780.00','0.00','0.00','14','".getMonthEnd(date("Y-m", strtotime("-1 months")))."',$projectID9,1,1,'".$companyID."',0),".
                    //  Current Month
                    "('".date("Y-m-d")."','INV04/".date("Ym")."','8640.00','2800.00','1200.00','14','".getMonthEnd(date("Y-m"))."',$projectID1,1,1,'".$companyID."',0),".
                    "('".date("Y-m-d")."','INV04/".date("Ym")."','88760.00','0.00','0.00','14','".getMonthEnd(date("Y-m"))."',$projectID2,1,1,'".$companyID."',0),".
                    "('".date("Y-m-d")."','INV04/".date("Ym")."','156480.00','.00','.00','14','".getMonthEnd(date("Y-m"))."',$projectID4,1,1,'".$companyID."',0),".
                    "('".date("Y-m-d")."','INV04/".date("Ym")."','42740.00','0.00','0.00','14','".getMonthEnd(date("Y-m"))."',$projectID9,1,1,'".$companyID."',0),".
                    "('".date("Y-m-d")."','INV04/".date("Ym")."','1890.00','0.00','0.00','14','".getMonthEnd(date("Y-m"))."',$projectID10,1,1,'".$companyID."',0)");

    $invoiceID1 = q("SELECT id FROM LoadInvoices WHERE company_id = '".$companyID."' AND name = 'INV01/".date("Ym", strtotime("-3 months"))."' AND project_id = '".$projectID1."'");
    $invoiceID2 = q("SELECT id FROM LoadInvoices WHERE company_id = '".$companyID."' AND name = 'INV01/".date("Ym", strtotime("-3 months"))."' AND project_id = '".$projectID2."'");
    $invoiceID3 = q("SELECT id FROM LoadInvoices WHERE company_id = '".$companyID."' AND name = 'INV01/".date("Ym", strtotime("-3 months"))."' AND project_id = '".$projectID4."'");
    $invoiceID4 = q("SELECT id FROM LoadInvoices WHERE company_id = '".$companyID."' AND name = 'INV01/".date("Ym", strtotime("-3 months"))."' AND project_id = '".$projectID6."'");
    $invoiceID5 = q("SELECT id FROM LoadInvoices WHERE company_id = '".$companyID."' AND name = 'INV01/".date("Ym", strtotime("-3 months"))."' AND project_id = '".$projectID7."'");
    $invoiceID6 = q("SELECT id FROM LoadInvoices WHERE company_id = '".$companyID."' AND name = 'INV01/".date("Ym", strtotime("-3 months"))."' AND project_id = '".$projectID8."'");
    $invoiceID7 = q("SELECT id FROM LoadInvoices WHERE company_id = '".$companyID."' AND name = 'INV01/".date("Ym", strtotime("-3 months"))."' AND project_id = '".$projectID9."'");
    $invoiceID8 = q("SELECT id FROM LoadInvoices WHERE company_id = '".$companyID."' AND name = 'INV01/".date("Ym", strtotime("-3 months"))."' AND project_id = '".$projectID10."'");
    $invoiceID9 = q("SELECT id FROM LoadInvoices WHERE company_id = '".$companyID."' AND name = 'INV02/".date("Ym", strtotime("-2 months"))."' AND project_id = '".$projectID1."'");
    $invoiceID10 = q("SELECT id FROM LoadInvoices WHERE company_id = '".$companyID."' AND name = 'INV02/".date("Ym", strtotime("-2 months"))."' AND project_id = '".$projectID2."'");
    $invoiceID11 = q("SELECT id FROM LoadInvoices WHERE company_id = '".$companyID."' AND name = 'INV02/".date("Ym", strtotime("-2 months"))."' AND project_id = '".$projectID4."'");
    $invoiceID12 = q("SELECT id FROM LoadInvoices WHERE company_id = '".$companyID."' AND name = 'INV02/".date("Ym", strtotime("-2 months"))."' AND project_id = '".$projectID7."'");
    $invoiceID13 = q("SELECT id FROM LoadInvoices WHERE company_id = '".$companyID."' AND name = 'INV02/".date("Ym", strtotime("-2 months"))."' AND project_id = '".$projectID8."'");
    $invoiceID14 = q("SELECT id FROM LoadInvoices WHERE company_id = '".$companyID."' AND name = 'INV02/".date("Ym", strtotime("-2 months"))."' AND project_id = '".$projectID9."'");
    $invoiceID15 = q("SELECT id FROM LoadInvoices WHERE company_id = '".$companyID."' AND name = 'INV02/".date("Ym", strtotime("-2 months"))."' AND project_id = '".$projectID10."'");
    $invoiceID16 = q("SELECT id FROM LoadInvoices WHERE company_id = '".$companyID."' AND name = 'INV03/".date("Ym", strtotime("-1 months"))."' AND project_id = '".$projectID1."'");
    $invoiceID17 = q("SELECT id FROM LoadInvoices WHERE company_id = '".$companyID."' AND name = 'INV03/".date("Ym", strtotime("-1 months"))."' AND project_id = '".$projectID2."'");
    $invoiceID18 = q("SELECT id FROM LoadInvoices WHERE company_id = '".$companyID."' AND name = 'INV03/".date("Ym", strtotime("-1 months"))."' AND project_id = '".$projectID4."'");
    $invoiceID19 = q("SELECT id FROM LoadInvoices WHERE company_id = '".$companyID."' AND name = 'INV03/".date("Ym", strtotime("-1 months"))."' AND project_id = '".$projectID6."'");
    $invoiceID20 = q("SELECT id FROM LoadInvoices WHERE company_id = '".$companyID."' AND name = 'INV03/".date("Ym", strtotime("-1 months"))."' AND project_id = '".$projectID8."'");
    $invoiceID21 = q("SELECT id FROM LoadInvoices WHERE company_id = '".$companyID."' AND name = 'INV03/".date("Ym", strtotime("-1 months"))."' AND project_id = '".$projectID9."'");
    $invoiceID22 = q("SELECT id FROM LoadInvoices WHERE company_id = '".$companyID."' AND name = 'INV04/".date("Ym")."' AND project_id = '".$projectID1."'");
    $invoiceID23 = q("SELECT id FROM LoadInvoices WHERE company_id = '".$companyID."' AND name = 'INV04/".date("Ym")."' AND project_id = '".$projectID2."'");
    $invoiceID24 = q("SELECT id FROM LoadInvoices WHERE company_id = '".$companyID."' AND name = 'INV04/".date("Ym")."' AND project_id = '".$projectID4."'");
    $invoiceID25 = q("SELECT id FROM LoadInvoices WHERE company_id = '".$companyID."' AND name = 'INV04/".date("Ym")."' AND project_id = '".$projectID9."'");
    $invoiceID26 = q("SELECT id FROM LoadInvoices WHERE company_id = '".$companyID."' AND name = 'INV04/".date("Ym")."' AND project_id = '".$projectID10."'");

    $insert = q("INSERT INTO ExpenseBudget (project_id,descr,amount,vat,due_date,invoice_id,company_id,paid) VALUES ".
                    "($projectID1,'EXP01/".date("Ym", strtotime("-3 months"))."','1000.00',14,'".getMonthEnd(date("Y-m", strtotime("-3 months")))."',$invoiceID1,'".$companyID."',1),".
                    "($projectID4,'EXP01/".date("Ym", strtotime("-3 months"))."','28000.00',14,'".getMonthEnd(date("Y-m", strtotime("-3 months")))."',$invoiceID3,'".$companyID."',1),".
                    "($projectID2,'EXP01/".date("Ym", strtotime("-2 months"))."','25000.00',14,'".getMonthEnd(date("Y-m", strtotime("-2 months")))."',$invoiceID10,'".$companyID."',1),".
                    "($projectID3,'EXP01/".date("Ym", strtotime("-2 months"))."','20000.00',14,'".getMonthEnd(date("Y-m", strtotime("-2 months")))."',$invoiceID11,'".$companyID."',1),".
                    "($projectID1,'EXP02/".date("Ym", strtotime("-1 months"))."','3600.00',14,'".getMonthEnd(date("Y-m", strtotime("-1 months")))."',$invoiceID16,'".$companyID."',1),".
                    "($projectID6,'EXP01/".date("Ym", strtotime("-1 months"))."','3580.00',14,'".getMonthEnd(date("Y-m", strtotime("-1 months")))."',$invoiceID19,'".$companyID."',1),".
                    "($projectID1,'EXP03/".date("Ym")."','1600.00',14,'".getMonthEnd(date("Y-m"))."',$invoiceID22,'".$companyID."',1)");

    $firstDate = date("Y-m-d", strtotime("-3 months"));
    $lastDate = date("Y-m-d");
    $date = getWeekStart($firstDate);
    $timeSpent = array("0.25","0.50","0.75","1.00","1.50","2.00","2.50","3.00","3.50","4.00","4.50","5.00","5.50","6.00");//Size:=14

    while ($date <= $lastDate)    {
        if ($date >= $firstDate)    {
            $day = date("D", strtotime($date));

            $timeEntries = "";
            $expenseEntries = "";

            switch ($day)   {
                case "Mon":
                    //Time
                    //Manager
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID1."','".$projectID4."','0','".$activityID7."','Meeting with contruction team',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID1."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID4."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID1."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID1."','".$projectID1."','0','".$activityID7."','Meeting with contruction team',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID1."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID1."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID1."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID1."','".$projectID5."','0','".$activityID6."','Sending follow-up mails to clients and team',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID1."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID5."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID1."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID1."','".$projectID4."','0','".$activityID5."','Altering budget as necessary',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID1."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID4."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID1."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID1."','".$projectID5."','0','".$activityID11."','Altering WBS as necessary',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID1."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID5."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID1."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    //Project Manager
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID2."','".$projectID1."','0','".$activityID7."','Meeting with development team',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID2."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID1."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID2."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID2."','".$projectID4."','0','".$activityID7."','Meeting with development team',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID2."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID4."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID2."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID2."','".$projectID2."','0','".$activityID9."','Assigning new resources to team members',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID2."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID2."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID2."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID2."','".$projectID3."','0','".$activityID9."','Assigning new resources to team members',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID2."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID3."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID2."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID2."','".$projectID8."','0','".$activityID16."','Fixing settings with system',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID2."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID8."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID2."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID2."','".$projectID10."','0','".$activityID17."','Fixing settings on server',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID2."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID10."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID2."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    //Financial Manager
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID3."','".$projectID1."','0','".$activityID7."','Meeting with development team',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID3."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID1."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID3."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID3."','".$projectID1."','0','".$activityID6."','Compiling invoices and following up on payments',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID3."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID1."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID3."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID3."','".$projectID4."','0','".$activityID6."','Compiling invoices and following up on payments',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID3."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID4."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID3."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID3."','".$projectID7."','0','".$activityID6."','Compiling invoices and following up on payments',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID3."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID7."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID3."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    //Client
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID4."','".$projectID1."','0','".$activityID7."','Meeting with development team',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID4."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID1."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID4."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID4."','".$projectID1."','0','".$activityID4."','Doing further development on website',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID4."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID1."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID4."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID4."','".$projectID2."','0','".$activityID3."','Doing further development on website',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID4."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID2."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID4."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID4."','".$projectID3."','0','".$activityID1."','Doing further development on website',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID4."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID3."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID4."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID4."','".$projectID9."','0','".$activityID14."','Helping clients with emailing problems',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID4."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID9."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID4."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID4."','".$projectID10."','0','".$activityID18."','Fixing bugs on website and testing',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID4."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID10."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID4."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";

                    //Expenses
                    //Manager
                    $expenseTypeID = q("SELECT id FROM dropdowns WHERE categoryID = (SELECT id FROM categories WHERE code = 'expenseType') AND name = 'Driving'");
                    $vehicleID = "0";
                    $disbursementID = "0";
                    $expenseEntries    .= "('".$companyID."','CP','".$date."','".$userID1."','".$projectID4."','0','".$activityID7."','".$expenseTypeID."','".$vehicleID2."','".$disbursementID."',".
                                            "'Driving to and from meeting','','52','','','52','0','0','3.80','197.60','".$date."','".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    break;
                case "Tue":
                    //Time
                    //Manager
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID1."','".$projectID4."','0','".$activityID7."','Meeting with contruction team',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID1."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID4."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID1."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID1."','".$projectID5."','0','".$activityID6."','Sending follow-up mails to clients and team',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID1."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID5."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID1."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID1."','".$projectID4."','0','".$activityID5."','Altering budget as necessary',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID1."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID4."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID1."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID1."','".$projectID5."','0','".$activityID11."','Altering WBS as necessary',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID1."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID5."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID1."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    //Project Manager
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID2."','".$projectID4."','0','".$activityID7."','Meeting with contruction team',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID2."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID4."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID2."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID2."','".$projectID2."','0','".$activityID9."','Assigning new resources to team members',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID2."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID2."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID2."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID2."','".$projectID3."','0','".$activityID9."','Assigning new resources to team members',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID2."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID3."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID2."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID2."','".$projectID8."','0','".$activityID16."','Fixing settings with system',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID2."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID8."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID2."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID2."','".$projectID10."','0','".$activityID17."','Fixing settings on server',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID2."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID10."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID2."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    //Client
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID4."','".$projectID1."','0','".$activityID4."','Doing further development on website',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID4."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID1."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID4."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID4."','".$projectID2."','0','".$activityID3."','Doing further development on website',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID4."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID2."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID4."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID4."','".$projectID3."','0','".$activityID1."','Doing further development on website',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID4."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID3."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID4."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID4."','".$projectID9."','0','".$activityID15."','Helping clients with networking problems',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID4."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID9."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID4."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID4."','".$projectID10."','0','".$activityID18."','Fixing bugs on website and testing',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID4."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID10."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID4."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";

                    //Expenses
                    //Project Manager
                    $expenseTypeID = q("SELECT id FROM dropdowns WHERE categoryID = (SELECT id FROM categories WHERE code = 'expenseType') AND name = 'Driving'");
                    $vehicleID = "0";
                    $disbursementID = "0";
                    $expenseEntries    .= "('".$companyID."','CP','".$date."','".$userID2."','".$projectID4."','0','".$activityID7."','".$expenseTypeID."','".$vehicleID3."','".$disbursementID."',".
                                            "'Driving to and from meeting','','52','','','52','0','0','3.30','171.60','".$date."','".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    break;
                case "Wed":
                    //Time
                    //Manager
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID1."','".$projectID4."','0','".$activityID7."','Meeting with contruction team',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID1."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID4."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID1."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID1."','".$projectID5."','0','".$activityID6."','Sending follow-up mails to clients and team',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID1."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID5."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID1."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID1."','".$projectID4."','0','".$activityID5."','Altering budget as necessary',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID1."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID4."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID1."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID1."','".$projectID5."','0','".$activityID11."','Altering WBS as necessary',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID1."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID5."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID1."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    //Project Manager
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID2."','".$projectID4."','0','".$activityID7."','Meeting with contruction team',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID2."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID4."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID2."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID2."','".$projectID2."','0','".$activityID9."','Assigning new resources to team members',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID2."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID2."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID2."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID2."','".$projectID3."','0','".$activityID9."','Assigning new resources to team members',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID2."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID3."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID2."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID2."','".$projectID8."','0','".$activityID16."','Fixing settings with system',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID2."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID8."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID2."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID2."','".$projectID10."','0','".$activityID17."','Fixing settings on server',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID2."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID10."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID2."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    //Financial Manager
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID3."','".$projectID2."','0','".$activityID6."','Compiling invoices and following up on payments',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID3."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID2."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID3."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID3."','".$projectID6."','0','".$activityID6."','Compiling invoices and following up on payments',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID3."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID6."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID3."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID3."','".$projectID8."','0','".$activityID6."','Compiling invoices and following up on payments',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID3."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID8."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID3."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID3."','".$projectID10."','0','".$activityID6."','Compiling invoices and following up on payments',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID3."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID10."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID3."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    //Client
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID4."','".$projectID1."','0','".$activityID4."','Doing further development on website',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID4."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID1."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID4."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID4."','".$projectID2."','0','".$activityID3."','Doing further development on website',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID4."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID2."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID4."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID4."','".$projectID3."','0','".$activityID1."','Doing further development on website',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID4."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID3."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID4."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID4."','".$projectID9."','0','".$activityID15."','Helping clients with networking problems',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID4."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID9."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID4."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID4."','".$projectID10."','0','".$activityID18."','Fixing bugs on website and testing',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID4."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID10."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID4."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";

                    //Expenses
                    //Manager
                    $expenseTypeID = q("SELECT id FROM dropdowns WHERE categoryID = (SELECT id FROM categories WHERE code = 'expenseType') AND name = 'Driving'");
                    $vehicleID = "0";
                    $disbursementID = "0";
                    $expenseEntries    .= "('".$companyID."','CP','".$date."','".$userID1."','".$projectID4."','0','".$activityID7."','".$expenseTypeID."','".$vehicleID2."','".$disbursementID."',".
                                            "'Driving to and from meeting','','52','','','52','0','0','3.80','197.60','".$date."','".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    break;
                case "Thu":
                    //Time
                    //Manager
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID1."','".$projectID4."','0','".$activityID7."','Meeting with contruction team',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID1."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID4."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID1."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID1."','".$projectID5."','0','".$activityID6."','Sending follow-up mails to clients and team',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID1."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID5."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID1."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID1."','".$projectID4."','0','".$activityID5."','Altering budget as necessary',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID1."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID4."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID1."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID1."','".$projectID5."','0','".$activityID11."','Altering WBS as necessary',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID1."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID5."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID1."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    //Project Manager
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID2."','".$projectID4."','0','".$activityID7."','Meeting with contruction team',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID2."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID4."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID2."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID2."','".$projectID2."','0','".$activityID9."','Assigning new resources to team members',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID2."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID2."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID2."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID2."','".$projectID3."','0','".$activityID9."','Assigning new resources to team members',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID2."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID3."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID2."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID2."','".$projectID8."','0','".$activityID16."','Fixing settings with system',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID2."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID8."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID2."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID2."','".$projectID10."','0','".$activityID17."','Fixing settings on server',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID2."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID10."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID2."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    //Client
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID4."','".$projectID1."','0','".$activityID4."','Doing further development on website',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID4."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID1."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID4."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID4."','".$projectID2."','0','".$activityID3."','Doing further development on website',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID4."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID2."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID4."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID4."','".$projectID3."','0','".$activityID1."','Doing further development on website',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID4."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID3."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID4."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID4."','".$projectID9."','0','".$activityID15."','Helping clients with networking problems',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID4."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID9."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID4."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID4."','".$projectID10."','0','".$activityID18."','Fixing bugs on website and testing',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID4."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID10."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID4."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";

                    //Expenses
                    //Project Manager
                    $expenseTypeID = q("SELECT id FROM dropdowns WHERE categoryID = (SELECT id FROM categories WHERE code = 'expenseType') AND name = 'Driving'");
                    $vehicleID = "0";
                    $disbursementID = "0";
                    $expenseEntries    .= "('".$companyID."','CP','".$date."','".$userID2."','".$projectID4."','0','".$activityID7."','".$expenseTypeID."','".$vehicleID3."','".$disbursementID."',".
                                            "'Driving to and from meeting','','52','','','52','0','0','3.30','171.60','".$date."','".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    break;
                case "Fri":
                    //Time
                    //Manager
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID1."','".$projectID4."','0','".$activityID7."','Meeting with contruction team',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID1."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID4."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID1."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID1."','".$projectID5."','0','".$activityID6."','Sending follow-up mails to clients and team',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID1."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID5."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID1."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID1."','".$projectID4."','0','".$activityID5."','Altering budget as necessary',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID1."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID4."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID1."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID1."','".$projectID5."','0','".$activityID11."','Altering WBS as necessary',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID1."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID5."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID1."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    //Project Manager
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID2."','".$projectID4."','0','".$activityID7."','Meeting with contruction team',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID2."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID4."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID2."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID2."','".$projectID2."','0','".$activityID9."','Assigning new resources to team members',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID2."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID2."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID2."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID2."','".$projectID3."','0','".$activityID9."','Assigning new resources to team members',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID2."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID3."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID2."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID2."','".$projectID8."','0','".$activityID16."','Fixing settings with system',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID2."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID8."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID2."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID2."','".$projectID10."','0','".$activityID17."','Fixing settings on server',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID2."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID10."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID2."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    //Financial Manager
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID3."','".$projectID1."','0','".$activityID6."','Compiling invoices and following up on payments',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID3."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID1."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID3."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID3."','".$projectID9."','0','".$activityID6."','Compiling invoices and following up on payments',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID3."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID9."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID3."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    //Client
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID4."','".$projectID1."','0','".$activityID4."','Doing further development on website',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID4."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID1."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID4."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID4."','".$projectID2."','0','".$activityID3."','Doing further development on website',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID4."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID2."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID4."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID4."','".$projectID3."','0','".$activityID1."','Doing further development on website',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID4."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID3."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID4."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID4."','".$projectID9."','0','".$activityID15."','Helping clients with networking problems',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID4."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID9."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID4."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $timeEntries    .= "('".$companyID."','CP','".$date."','".$userID4."','".$projectID10."','0','".$activityID18."','Fixing bugs on website and testing',".
                                            "(SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$userID4."' AND pu.company_id = '".$companyID."' ".
                                                "AND pu.project_id = '".$projectID10."' AND ur.companyid = '".$companyID."' AND ur.userid = '".$userID4."'),'".$timeSpent[rand(3,9)]."','".$date."',".
                                                "'".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";

                    //Expenses
                    //Manager
                    $expenseTypeID = q("SELECT id FROM dropdowns WHERE categoryID = (SELECT id FROM categories WHERE code = 'expenseType') AND name = 'Driving'");
                    $vehicleID = "0";
                    $disbursementID = "0";
                    $expenseEntries    .= "('".$companyID."','CP','".$date."','".$userID1."','".$projectID4."','0','".$activityID7."','".$expenseTypeID."','".$vehicleID2."','".$disbursementID."',".
                                            "'Driving to and from meeting','','52','','','52','0','0','3.80','197.60','".$date."','".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    //Client
                    $expenseTypeID = q("SELECT id FROM dropdowns WHERE categoryID = (SELECT id FROM categories WHERE code = 'expenseType') AND name = 'Printing'");
                    $vehicleID = "0";
                    $disbursementID = "0";
                    $expenseEntries    .= "('".$companyID."','CP','".$date."','".$userID4."','".$projectID1."','0','".$activityID4."','".$expenseTypeID."','".$vehicleID."','".$disbursementID4."',".
                                            "'Printing files for hard-copy archieve','','52','','','52','0','0','3.30','171.60','".$date."','".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    $expenseTypeID = q("SELECT id FROM dropdowns WHERE categoryID = (SELECT id FROM categories WHERE code = 'expenseType') AND name = 'Printing'");
                    $vehicleID = "0";
                    $disbursementID = "0";
                    $expenseEntries    .= "('".$companyID."','CP','".$date."','".$userID4."','".$projectID2."','0','".$activityID4."','".$expenseTypeID."','".$vehicleID."','".$disbursementID4."',".
                                            "'Printing files for hard-copy archieve','','52','','','52','0','0','3.30','171.60','".$date."','".rand(10,18).":".rand(0,59).":".rand(0,59)."'),";
                    break;
                default:
                    break;
            }

            if ($timeEntries != "")     $timeEntries        = substr($timeEntries,0,(strlen($timeEntries) - 1));
            if ($expenseEntries != "")  $expenseEntries     = substr($expenseEntries,0,(strlen($expenseEntries) - 1));

            //  Time
            if ($timeEntries != "")
                $insert = q("INSERT INTO TimeSheet (company_id,projectType,date,user_id,project_id,area_id,activity_id,descr,rate,time_spent,on_date,on_time) VALUES $timeEntries");

            //  Expenses
            if ($expenseEntries != "")
                $insert = q("INSERT INTO ExpenseSheet ".
                                "(company_id,projectType,date,user_id,project_id,area_id,activity_id,expense_type_id,vehicle_id,disbursement_id,descr,otherDescr,kilometers,odoStart,odoFinish,businessKM,privateKM,total,rate,".
                                    "expense,on_date,on_time) VALUES $expenseEntries");
        }

        $date = getDates($date, 1);
    }

    //  Time
    $insert = q("INSERT INTO ApprovedTime (company_id,projectType,date,user_id,project_id,area_id,activity_id,descr,rate,time_spent,on_date,on_time,timesheet_id,status,status2) ".
                "SELECT company_id,projectType,date,user_id,project_id,area_id,activity_id,descr,rate,time_spent,on_date,on_time,id,0,0 FROM TimeSheet ".
                    "WHERE user_id IN ('".$userID1."','".$userID2."','".$userID3."','".$userID4."') ORDER BY id");

    //  Expenses
    $insert = q("INSERT INTO ApprovedExpense (company_id,projectType,date,user_id,project_id,area_id,activity_id,expense_type_id,vehicle_id,disbursement_id,descr,otherDescr,kilometers,odoStart,odoFinish,businessKM,".
                    "privateKM,total,rate,expense,on_date,on_time,expensesheet_id,status,status2) ".
                "SELECT company_id,projectType,date,user_id,project_id,area_id,activity_id,expense_type_id,vehicle_id,disbursement_id,descr,otherDescr,kilometers,odoStart,odoFinish,businessKM,".
                    "privateKM,total,rate,expense,on_date,on_time,id,0,0 FROM ExpenseSheet WHERE user_id IN ('".$userID1."','".$userID2."','".$userID3."','".$userID4."') ORDER BY id");

    $date = date("Y-m-d", strtotime("-1 months"));

    $update = q("UPDATE TimeSheet SET locked = 1 WHERE date < '".$date."'");
    $update = q("UPDATE ExpenseSheet SET locked = 1 WHERE date < '".$date."'");
    $update = q("UPDATE ApprovedTime SET status = 2 WHERE date < '".$date."'");
    $update = q("UPDATE ApprovedExpense SET status = 2 WHERE date < '".$date."'");
    ////////////////////////////////////////

	$drop = q("DROP TABLE IF EXISTS menus");
	$create = q("CREATE TABLE menus (".
				"id MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,".
				"parentID MEDIUMINT UNSIGNED NOT NULL DEFAULT 0, INDEX(parentID),".
				"display TINYINT UNSIGNED NOT NULL DEFAULT 1,".
				"modal TINYINT UNSIGNED NOT NULL DEFAULT 0,".
				"newPage TINYINT UNSIGNED NOT NULL DEFAULT 0,".
				"level TINYINT UNSIGNED NOT NULL DEFAULT 0, INDEX(level),".
				"ranking TINYINT UNSIGNED NOT NULL DEFAULT 1, INDEX(ranking),".
				"menuType VARCHAR(10) NOT NULL DEFAULT 'MAIN',".    //  MAIN || SETTINGS
				"icon VARCHAR(255) NOT NULL DEFAULT '', ".
				"urlLink VARCHAR(255) NOT NULL,".
				"menuDescr VARCHAR(255) NOT NULL)");

	//////////////////////////////////////////////////
	//  MAIN MENU ITEMS
	//////////////////////////////////////////////////
	$menuType = "MAIN";
	$ranking = 0;
	//////////////////////////////////////////////////
	//  LEVEL 0
	$level = 0;

	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (0,".$level.",".$ranking++.",'".$menuType."','bookings.php','Bookings')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (0,".$level.",".$ranking++.",'".$menuType."','','Projects')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (0,".$level.",".$ranking++.",'".$menuType."','','Approval')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (0,".$level.",".$ranking++.",'".$menuType."','','Finances')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (0,".$level.",".$ranking++.",'".$menuType."','','Reports')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (0,".$level.",".$ranking++.",'".$menuType."','','Admin')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (0,".$level.",".$ranking++.",'".$menuType."','','On-Track Admin')");

	//////////////////////////////////////////////////
	//  LEVEL 1
	$level++;

	$parentID = q("SELECT id FROM menus WHERE urlLink = 'bookings.php' AND menuDescr = 'Bookings'");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','book_time.php','Time')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','book_expense.php','Expenses')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','bookings.php','Time/Expenses')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','gpslogbook_trip_management.php','Trip Management')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','book_leave.php','Leave')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','book_resource.php','Shared Resources')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','booking_management.php','Manage Bookings')");
	//$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_inv_vs_ctc_own.php','Report Invoice Potential vs Cost to Company')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_inv_vs_ctc_own_v2.php','Report Invoice Potential vs Cost to Company')");

	$parentID = q("SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Projects'");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','sp_projects.php','Shared Projects')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','projects.php','Company Projects')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','sla.php','SLA')");

	$parentID = q("SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Approval'");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','_module_handler.php?module=approval&what=display&level=SP','Shared Projects')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','_module_handler.php?module=approval&what=display&level=CP','Company Projects')");

	$parentID = q("SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Finances'");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','budget_assignment.php','Running Cost')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','invoice_load.php','Invoices')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','expense_planning.php','Expenses')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','clients.php','Clients')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','blankPage.php','Statements')");

	$parentID = q("SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Reports'");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report.php','Personal Time/Expenses')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_time_sheet.php','Project')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_sp_time_sheet.php','Shared Project')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_cash_flow_forecast.php','Business')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','','Approvals')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_leave.php','Leave')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_logbook.php','Log Book')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_company_breakdown.php','Administrative')");

	$parentID = q("SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Admin'");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','management.php','Time/Expense Management')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','','Vehicles')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','employees.php?userType=all','Employees')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','printing_management.php','Printing')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','locations.php','Locations')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','resources.php','Resources')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','role_manager.php','Role Management')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','role_assignment.php','Role/Action Assignment')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','logs.php','Logs')");

	$parentID = q("SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'On-Track Admin'");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','companies.php','Companies')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','modules.php','Module Management')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','action_manager.php','Action Management')");
	//$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','news_add.php','News')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','company_assignment.php','Privilege Assignment')");

	//////////////////////////////////////////////////
	//  LEVEL 2
	$level++;

	$parentID = q("SELECT id FROM menus WHERE urlLink = 'sp_projects.php' AND menuDescr = 'Shared Projects' AND parentID = (SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Projects' AND level = ".($level - 2).")");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','sp_areas.php','Area and Type Management')");

	$parentID = q("SELECT id FROM menus WHERE urlLink = 'projects.php' AND menuDescr = 'Company Projects' AND parentID = (SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Projects' AND level = ".($level - 2).")");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','projects.php','Manage Projects')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','areas.php','Area and Type Management')");

	$parentID = q("SELECT id FROM menus WHERE urlLink = 'budget_assignment.php' AND menuDescr = 'Running Cost' AND parentID = (SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Finances' AND level = ".($level - 2).")");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','budget_assignment.php','Management')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','categories.php','Definition')");

	$parentID = q("SELECT id FROM menus WHERE urlLink = 'invoice_load.php' AND menuDescr = 'Invoices' AND parentID = (SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Finances' AND level = ".($level - 2).")");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','invoice_load.php','Manage')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','invoice_loadNEW.php','Manage [NEW]')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','invoice_processing.php','Processing')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','invoice_payment_certificate.php','Invoice & Payment Certificate')");

	$parentID = q("SELECT id FROM menus WHERE urlLink = '_module_handler.php?module=approval&what=display&level=SP' AND menuDescr = 'Shared Projects' AND parentID = (SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Approval' AND level = ".($level - 2).")");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','_module_handler.php?module=approval&what=display&level=SP','Approve')");

	$parentID = q("SELECT id FROM menus WHERE urlLink = '_module_handler.php?module=approval&what=display&level=CP' AND menuDescr = 'Company Projects' AND parentID = (SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Approval' AND level = ".($level - 2).")");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','_module_handler.php?module=approval&what=display&level=CP','Approve')");

	$parentID = q("SELECT id FROM menus WHERE urlLink = 'blankPage.php' AND menuDescr = 'Statements' AND parentID = (SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Finances' AND level = ".($level - 2).")");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','statements.php?link=INVOICE','Link')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','statements_unlink.php','Unlink')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','statements_update.php','Edit')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','statements_import.php','Import')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','account_manage.php','Bank Account(s)')");

	$parentID = q("SELECT id FROM menus WHERE urlLink = 'report_time_sheet.php' AND menuDescr = 'Project' AND parentID = (SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Reports' AND level = ".($level - 2).")");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_time_sheet.php','Time Sheet')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_expense_sheet.php','Expense Sheet')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_time_expense.php','Time & Expenses')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_active_projects.php','Project List')");
	//$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_over_budget_projects.php','Projects Over Budget')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_over_budget_projects_consulting.php','Projects Over Budget (Consulting)')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_project_breakdown.php','Consulting Breakdown')");
	//$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_project.php','Per Project')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_project_list.php','User/Project List')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_progress.php','Progress')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_work_in_process.php','Work In Process')");
	//$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_gantt.php','Gantt')");
	//$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','invoice_create.php','Invoice Info')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_summary_time.php','Summaries')");

	$parentID = q("SELECT id FROM menus WHERE urlLink = 'report_sp_time_sheet.php' AND menuDescr = 'Shared Project' AND parentID = (SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Reports' AND level = ".($level - 2).")");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_sp_time_sheet.php','Time Sheet')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_sp_expense_sheet.php','Expense Sheet')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_sp_project_breakdown.php','Consulting Breakdown')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_sp_invoice.php','Invoices')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_sp_summary_time.php','Summaries')");

	$parentID = q("SELECT id FROM menus WHERE urlLink = 'report_cash_flow_forecast.php' AND menuDescr = 'Business' AND parentID = (SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Reports' AND level = ".($level - 2).")");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_cash_flow_forecast.php','Cash Flow Forecast')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_cash_flow_forecast2.php','Cash Flow Forecast V2')");
	//$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_invoiced_vs_cost_to_company.php','Invoice Potential vs Cost to Company')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_invoiced_vs_cost_to_company_v2.php','Invoice Potential vs Cost to Company')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_income_forecast_time.php','Income Forecast')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_growth_analysis.php','Growth Analysis')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_budget.php','Running Cost Report')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_budget_breakdown.php','Running Cost Breakdown')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_analysis.php','Business Analysis')");
	//$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_running_profit.php','Running Profit')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_time_comparison.php','Time Comparison')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_project_type_comparison.php','Project Type Comparison')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_cumulative_income.php','Cumulative Income')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_budget_period.php','Running Cost vs Invoiced')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_loaded_invoices.php','Loaded Invoices')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_invoice_tracking.php','Invoice Tracking')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_statements.php','Linked Statements')");

	$parentID = q("SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Approvals' AND parentID = (SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Reports' AND level = ".($level - 2).")");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','_module_handler.php?module=approval&what=report&level=SP','Shared Projects')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','_module_handler.php?module=approval&what=report&level=CP','Company Projects')");

	$parentID = q("SELECT id FROM menus WHERE urlLink = 'report_company_breakdown.php' AND menuDescr = 'Administrative' AND parentID = (SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Reports' AND level = ".($level - 2).")");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_company_breakdown.php','Company Breakdown')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_company_summary.php','Company Summary')");

	$parentID = q("SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Vehicles' AND parentID = (SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Admin' AND level = ".($level - 2).")");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','vehicles.php','Add/Edit Vehicles')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','vehicle_assignment.php','Vehicle Assignment')");

	$parentID = q("SELECT id FROM menus WHERE urlLink = 'role_assignment.php' AND menuDescr = 'Role/Action Assignment' AND parentID = (SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'Admin' AND level = ".($level - 2).")");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','action_assignment.php','Action Assignment')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','role_assignment.php','Role Assignment')");

	$parentID = q("SELECT id FROM menus WHERE urlLink = 'company_assignment.php' AND menuDescr = 'Privilege Assignment' AND parentID = (SELECT id FROM menus WHERE urlLink = '' AND menuDescr = 'On-Track Admin' AND level = ".($level - 2).")");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','company_assignment.php','Company Assignment')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','resource_assignment.php','Shared Resource Assignment')");

	//////////////////////////////////////////////////
	//  LEVEL 3
	$level++;

	$parentID = q("SELECT id FROM menus WHERE urlLink = 'sp_areas.php' AND menuDescr = 'Area and Type Management' AND parentID = (SELECT id FROM menus WHERE urlLink = 'sp_projects.php' AND menuDescr = 'Shared Projects' AND level = ".($level - 2).")");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','sp_areas.php','Areas')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','sp_activityTypes.php','Activities')");

	$parentID = q("SELECT id FROM menus WHERE urlLink = 'statements.php?link=INVOICE' AND menuDescr = 'Link' AND parentID = (SELECT id FROM menus WHERE urlLink = 'blankPage.php' AND menuDescr = 'Statements' AND level = ".($level - 2).")");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','statements.php?link=BUDGET','Running Cost')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','statements.php?link=INVOICE','Invoices')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','statements.php?link=EXPENSE','Expenses')");

	$parentID = q("SELECT id FROM menus WHERE urlLink = 'areas.php' AND menuDescr = 'Area and Type Management' AND parentID = (SELECT id FROM menus WHERE urlLink = 'projects.php' AND menuDescr = 'Company Projects' AND level = ".($level - 2).")");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','areas.php','Areas')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','activityTypes.php','Activities')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','project_type_management.php','Project Types')");

	$parentID = q("SELECT id FROM menus WHERE urlLink = 'report_sp_summary_time.php' AND menuDescr = 'Summaries' AND parentID = (SELECT id FROM menus WHERE urlLink = 'report_sp_time_sheet.php' AND menuDescr = 'Shared Project' AND level = ".($level - 2).")");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_sp_summary_time.php','Time')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_sp_summary_expenses.php','Expenses')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_sp_summary.php','Time & Expenses')");

	$parentID = q("SELECT id FROM menus WHERE urlLink = 'report_summary_time.php' AND menuDescr = 'Summaries' AND parentID = (SELECT id FROM menus WHERE urlLink = 'report_time_sheet.php' AND menuDescr = 'Project' AND level = ".($level - 2).")");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_summary_time.php','Time')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_summary_expenses.php','Expenses')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_summary.php','Time & Expenses')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_project_info.php','Project Overview')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_summary_type.php','Type Summary')");

	$parentID = q("SELECT id FROM menus WHERE urlLink = 'report_work_in_process.php' AND menuDescr = 'Work In Process' AND parentID = (SELECT id FROM menus WHERE urlLink = 'report_time_sheet.php' AND menuDescr = 'Project' AND level = ".($level - 2).")");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_work_in_process.php','Project Lifetime')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_work_in_process_months.php','Past 6 Months')");

	$parentID = q("SELECT id FROM menus WHERE urlLink = 'report_income_forecast_time.php' AND menuDescr = 'Income Forecast' AND parentID = (SELECT id FROM menus WHERE urlLink = 'report_cash_flow_forecast.php' AND menuDescr = 'Business' AND level = ".($level - 2).")");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_income_forecast_time.php','Time Based')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_income_forecast_invoice.php','Invoice Based')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (".$parentID .",".$level.",".$ranking++.",'".$menuType."','report_income_forecast.php','Summary (Average)')");

	//////////////////////////////////////////////////
	//  SETTINGS MENU ITEMS
	//////////////////////////////////////////////////
	$menuType = "SETTINGS";
	$ranking = 0;
	$level = 0;

	$insert = q("INSERT INTO menus (parentID,modal,level,ranking,menuType,urlLink,menuDescr) VALUES (0,1,".$level.",".$ranking++.",'".$menuType."','modal_profile','Profile Settings')");
	//$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (0,".$level.",".$ranking++.",'".$menuType."','change_password.php','Change Password')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (0,".$level.",".$ranking++.",'".$menuType."','notifications.php','Notifications')");
	//$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (0,".$level.",".$ranking++.",'".$menuType."','','Themes')");
	$insert = q("INSERT INTO menus (parentID,level,ranking,menuType,urlLink,menuDescr) VALUES (0,".$level.",".$ranking++.",'".$menuType."','eula.php','EULA')");
	//////////////////////////////////////////////////

	$drop = q("DROP TABLE IF EXISTS menuAction");
	$create = q("CREATE TABLE menuAction (".
				"id MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,".
				"menuID MEDIUMINT UNSIGNED NOT NULL, INDEX(menuID),".
				"actionID MEDIUMINT UNSIGNED NOT NULL, INDEX(actionID))");

	//////////////////////////////////////////////////
	//  ACCESS FOR EACH MENU ITEM
	//////////////////////////////////////////////////
	$actions = array("TIME_MANAGE","EXPENSE_MANAGE_OWN","BOOKING_MANAGE","LEAVE_MANAGE_OWN","SHAREDRES_BOOK","LOGS_VIEW_OWN","EMP_EDIT","EMP_ADD_REMOVE","TIME_MANAGE_ALL",
						"EXPENSE_MANAGE_ALL","EMP_CTC_MANAGE","EMP_TARIFF_MANAGE","PROJ_MANAGE","VEH_MANAGE","SLA_MANAGE","ATYPE_MANAGE","AREA_MANAGE","PROJTYPE_MANAGE",
						"PRINTING_MANAGE","BUDGET_CAT_MANAGE","BUDGET_MANAGE","BUDGET_VIEW","INVOICE_MANAGE","INVOICE_VIEW","EXPENSE_MANAGE","EXPENSE_VIEW","STATEMENT_MAN",
						"LOGS_VIEW_ALL","APPROVAL_MANAGE","SPM_MANAGE","LEAVE_MANAGE_ALL","RA_MANAGEMENT","REP_TIME_EX","REP_ACTIVE_PROJ","REP_ANALYSIS","REP_STATEMENT",
						"REP_BUDGET","REP_BUDGET_PERIOD","REP_CASH_FLOW","REP_CASH_FLOW2","REP_COMPANY_BREAK","REP_INVOICE_TRACK","REP_COMPANY_SUMM","REP_EX_SHEET","REP_GANTT",
						"REP_GROWTH_ANALYSIS","REP_INCOME_FORECAST","REP_IN_FORE_INVOICE","REP_IN_FORE_TIME","REP_PROJ_BREAK","REP_IN_VS_CTC","REP_LEAVE",
						"REP_LOADED_INV","REP_PROGRESS","REP_TYPE_SUM","REP_PROJ_INFO","REP_PROJ_LIST","REP_PROJ_TYPE_COM","REP_RUNNING_PROF","REP_SUMMARY",
						"REP_SUMMARY_EX","REP_SUMMARY_TIME","REP_TIME_COM","REP_TIME_PROJ","REP_TIME_SHEET","REP_WORK_IN_PR","REP_WORK_IN_PR_M","CHNG_PW","EULA",
						"REP_SP_TIME_SHEET","REP_SP_EX_SHEET","REP_SP_SUMMARY_TIME","REP_SP_SUMMARY_EX","REP_SP_SUMMARY","REP_SP_BREAK","REP_SP_INV",
						"COMP_MAN","RESOURCE_MAN","MODULE_MAN","ACTION_MAN","ROLE_MAN","NEWS_MAN","COMP_ASSIGN","ACTION_ASSIGN","ROLE_ASSIGN","RESOURCE_ASSIGN","INVOICE_MANAGE_NEW","INVOICE_VIEW_NEW",
						"CLIENT_MANAGE","ACCOUNT_MANAGE","INVOICE_PROCESS","REP_INV_VS_CTC_OWN","REP_INV_VS_CTC_OWN_V2","REP_IN_VS_CTC_V2","REP_LOGBOOK","PAYMENT_CERTIFICATE",
						"REP_OVER_BUD_PROJ_CONS","VEHICLE_ASSIGN","LOCATION_MANAGEMENT","GPS_LOGBOOK_MANAGEMENT");

	sort($actions);

	if (is_array($actions))   {
		foreach ($actions as $action)  {
			unset($menuItems);

			switch($action)    {
				case "GPS_LOGBOOK_MANAGEMENT":
					$menuItems = array("0#bookings.php#Bookings","1#gpslogbook_trip_management.php#Trip Management#0#bookings.php#Bookings");
					break;
				case "TIME_MANAGE":
					$menuItems = array("0#bookings.php#Bookings","1#book_time.php#Time#0#bookings.php#Bookings","1#bookings.php#Time/Expenses#0#bookings.php#Bookings");
					break;
				case "REP_INV_VS_CTC_OWN":
					//$menuItems = array("0#bookings.php#Bookings","1#report_inv_vs_ctc_own.php#Report Invoice Potential vs Cost to Company#0#bookings.php#Bookings");
					$menuItems = array("0#bookings.php#Bookings","1#report_inv_vs_ctc_own_v2.php#Report Invoice Potential vs Cost to Company#0#bookings.php#Bookings");
					break;
				case "REP_INV_VS_CTC_OWN_V2":
					$menuItems = array("0#bookings.php#Bookings","1#report_inv_vs_ctc_own_v2.php#Report Invoice Potential vs Cost to Company#0#bookings.php#Bookings");
					break;
				case "EXPENSE_MANAGE_OWN":
					$menuItems = array("0#bookings.php#Bookings","1#book_expense.php#Expenses#0#bookings.php#Bookings","1#bookings.php#Time/Expenses#0#bookings.php#Bookings");
					break;
				case "BOOKING_MANAGE":
					$menuItems = array("0#bookings.php#Bookings","1#booking_management.php#Manage Bookings#0#bookings.php#Bookings");
					break;
				case "TIME_MANAGE_ALL":
				case "EXPENSE_MANAGE_ALL":
					$menuItems = array("0##Admin","1#management.php#Time/Expense Management#0##Admin");
					break;
				case "LEAVE_MANAGE_OWN":
					$menuItems = array("0#bookings.php#Bookings","1#book_leave.php#Leave#0#bookings.php#Bookings");
					break;
				case "SHAREDRES_BOOK":
					$menuItems = array("0#bookings.php#Bookings","1#book_resource.php#Shared Resources#0#bookings.php#Bookings");
					break;
				case "LOGS_VIEW_ALL":
				case "LOGS_VIEW_OWN":
					$menuItems = array("0##Admin","1#logs.php#Logs#0##Admin");
					break;
				case "EMP_EDIT":
					$menuItems = array("0##Admin","1#employees.php?userType=all#Employees#0##Admin");
					break;
				case "EMP_ADD_REMOVE":
				case "EMP_CTC_MANAGE":
				case "EMP_TARIFF_MANAGE":
					$menuItems = array("0##Admin","1#employees.php?userType=all#Employees#0##Admin");
					break;
				case "PROJ_MANAGE":
					$menuItems = array("0##Projects","1#projects.php#Company Projects#0##Projects","2#projects.php#Manage Projects#1#projects.php#Company Projects#0##Projects");
					break;
				case "VEH_MANAGE":
					$menuItems = array("0##Admin","1##Vehicles#0##Admin","2#vehicles.php#Add/Edit Vehicles#1##Vehicles#0##Admin");
					break;
				case "VEHICLE_ASSIGN":
					$menuItems = array("0##Admin","1##Vehicles#0##Admin","2#vehicle_assignment.php#Vehicle Assignment#1##Vehicles#0##Admin");
					break;
				case "RESOURCE_MAN":
					$menuItems = array("0##Admin","1#resources.php#Resources#0##Admin");
					break;
				case "ACTION_ASSIGN":
					$menuItems = array("0##Admin","1#role_assignment.php#Role/Action Assignment#0##Admin",
										"2#action_assignment.php#Action Assignment#1#role_assignment.php#Role/Action Assignment#0##Admin");
					break;
				case "ROLE_ASSIGN":
					$menuItems = array("0##Admin","1#role_assignment.php#Role/Action Assignment#0##Admin",
										"2#role_assignment.php#Role Assignment#1#role_assignment.php#Role/Action Assignment#0##Admin");
					break;
				case "ROLE_MAN":
					$menuItems = array("0##Admin","1#role_manager.php#Role Management#0##Admin");
					break;
				case "SLA_MANAGE":
					$menuItems = array("0##Projects","1#sla.php#SLA#0##Projects");
					break;
				case "ATYPE_MANAGE":
					$menuItems = array("0##Projects","1#projects.php#Company Projects#0##Projects","2#projects.php#Manage Projects#1#projects.php#Company Projects#0##Projects",
										"2#areas.php#Area and Type Management#1#projects.php#Company Projects#0##Projects",
										"3#activityTypes.php#Activities#2#areas.php#Area and Type Management#1#projects.php#Company Projects");
					break;
				case "AREA_MANAGE":
					$menuItems = array("0##Projects","1#projects.php#Company Projects#0##Projects","2#projects.php#Manage Projects#1#projects.php#Company Projects#0##Projects",
										"2#areas.php#Area and Type Management#1#projects.php#Company Projects#0##Projects",
										"3#areas.php#Areas#2#areas.php#Area and Type Management#1#projects.php#Company Projects");
					break;
				case "PROJTYPE_MANAGE":
					$menuItems = array("0##Projects","1#projects.php#Company Projects#0##Projects","2#projects.php#Manage Projects#1#projects.php#Company Projects#0##Projects",
										"2#areas.php#Area and Type Management#1#projects.php#Company Projects#0##Projects",
										"3#project_type_management.php#Project Types#2#areas.php#Area and Type Management#1#projects.php#Company Projects");
					break;
				case "LOCATION_MANAGEMENT":
					$menuItems = array("0##Admin","1#locations.php#Locations#0##Admin");
					break;
				case "PRINTING_MANAGE":
					$menuItems = array("0##Admin","1#printing_management.php#Printing#0##Admin");
					break;
				case "BUDGET_CAT_MANAGE":
					$menuItems = array("0##Finances","1#budget_assignment.php#Running Cost#0##Finances","2#categories.php#Definition#1#budget_assignment.php#Running Cost#0##Finances");
					break;
				case "BUDGET_MANAGE":
					$menuItems = array("0##Finances","1#budget_assignment.php#Running Cost#0##Finances","2#budget_assignment.php#Management#1#budget_assignment.php#Running Cost#0##Finances",
										"0#notifications.php#Notifications");
					break;
				case "BUDGET_VIEW":
					$menuItems = array("0##Finances","1#budget_assignment.php#Running Cost#0##Finances","2#budget_assignment.php#Management#1#budget_assignment.php#Running Cost#0##Finances");
					break;
				case "INVOICE_MANAGE":
					$menuItems = array("0##Finances","1#invoice_load.php#Invoices#0##Finances","2#invoice_load.php#Manage#1#invoice_load.php#Invoices#0##Finances",
										"0##Reports","1#report_time_sheet.php#Project#0##Reports",
										"2#invoice_create.php#Invoice Info#1#report_time_sheet.php#Project#0##Reports","0#notifications.php#Notifications");
					break;
				case "INVOICE_PROCESS":
					$menuItems = array("0##Finances","1#invoice_load.php#Invoices#0##Finances","2#invoice_processing.php#Processing#1#invoice_load.php#Invoices#0##Finances");
					break;
				case "PAYMENT_CERTIFICATE":
					$menuItems = array("0##Finances","1#invoice_load.php#Invoices#0##Finances","2#invoice_payment_certificate.php#Invoice & Payment Certificate#1#invoice_load.php#Invoices#0##Finances");
					break;
				case "INVOICE_VIEW":
					$menuItems = array("0##Finances","1#invoice_load.php#Invoices#0##Finances","2#invoice_load.php#Manage#1#invoice_load.php#Invoices#0##Finances");
					break;
				case "INVOICE_MANAGE_NEW":
					$menuItems = array("0##Finances","1#invoice_load.php#Invoices#0##Finances","2#invoice_loadNEW.php#Manage [NEW]#1#invoice_load.php#Invoices#0##Finances",
										"0##Reports","1#report_time_sheet.php#Project#0##Reports",
										"2#invoice_create.php#Invoice Info#1#report_time_sheet.php#Project#0##Reports","0#notifications.php#Notifications");
					break;
				case "INVOICE_VIEW_NEW":
					$menuItems = array("0##Finances","1#invoice_load.php#Invoices#0##Finances","2#invoice_loadNEW.php#Manage [NEW]#1#invoice_load.php#Invoices#0##Finances");
					break;
				case "CLIENT_MANAGE":
					$menuItems = array("0##Finances","1#clients.php#Clients#0##Finances");
					break;
				case "ACCOUNT_MANAGE":
					$menuItems = array("0##Finances","1#blankPage.php#Statements#0##Finances",
										"2#account_manage.php#Bank Account(s)#1#blankPage.php#Statements#0##Finances");
					break;
				case "STATEMENT_MAN":
					$menuItems = array("0##Finances","1#blankPage.php#Statements#0##Finances",
										"2#statements.php?link=INVOICE#Link#1#blankPage.php#Statements#0##Finances",
										"2#statements_unlink.php#Unlink#1#blankPage.php#Statements#0##Finances",
										"2#statements_update.php#Edit#1#blankPage.php#Statements#0##Finances",
										"2#statements_import.php#Import#1#blankPage.php#Statements#0##Finances",
										"3#statements.php?link=BUDGET#Running Cost#2#statements.php?link=INVOICE#Link#1#blankPage.php#Statements#0##Finances",
										"3#statements.php?link=INVOICE#Invoices#2#statements.php?link=INVOICE#Link#1#blankPage.php#Statements#0##Finances",
										"3#statements.php?link=EXPENSE#Expenses#2#statements.php?link=INVOICE#Link#1#blankPage.php#Statements#0##Finances");
					break;
				case "REP_STATEMENT":
					$menuItems = array("0##Reports","1#report_cash_flow_forecast.php#Business#0##Reports",
										"2#report_statements.php#Linked Statements#1#report_cash_flow_forecast.php#Business#0##Reports");
					break;
				case "EXPENSE_MANAGE":
					$menuItems = array("0##Finances","1#expense_planning.php#Expenses#0##Finances","0#notifications.php#Notifications");
					break;
				case "EXPENSE_VIEW":
					$menuItems = array("0##Finances","1#expense_planning.php#Expenses#0##Finances");
					break;
				case "APPROVAL_MANAGE":
					$menuItems = array("0##Approval","0##Reports","1#_module_handler.php?module=approval&what=display&level=CP#Company Projects#0##Approval",
										"1##Approvals#0##Reports",
										"2#_module_handler.php?module=approval&what=display&level=CP#Approve#1#_module_handler.php?module=approval&what=display&level=CP#Company Projects#0##Approval",
										"2#_module_handler.php?module=approval&what=report&level=CP#Company Projects#1##Approvals#0##Reports",
										"0#notifications.php#Notifications");
					break;
				case "SPM_MANAGE":
					$menuItems = array("0##Projects","0##Approval","0##Reports","1#sp_projects.php#Shared Projects#0##Projects","1#_module_handler.php?module=approval&what=display&level=SP#Shared Projects#0##Approval",
										"1#report_time_sheet.php#Project#0##Reports","1##Approvals#0##Reports",
										"2#sp_areas.php#Area and Type Management#1#sp_projects.php#Shared Projects#0##Projects",
										"2#_module_handler.php?module=approval&what=display&level=SP#Approve#1#_module_handler.php?module=approval&what=display&level=SP#Shared Projects#0##Approval",
										"2#report_project_list.php#User/Project List#1#report_time_sheet.php#Project#0##Reports",
										"2#_module_handler.php?module=approval&what=report&level=SP#Shared Projects#1##Approvals#0##Reports",
										"3#sp_areas.php#Areas#2#sp_areas.php#Area and Type Management#1#sp_projects.php#Shared Projects",
										"3#sp_activityTypes.php#Activities#2#sp_areas.php#Area and Type Management#1#sp_projects.php#Shared Projects");
					break;
				case "REP_TIME_EX":
					$menuItems = array("0##Reports","1#report_time_sheet.php#Project#0##Reports",
										"2#report_time_expense.php#Time & Expenses#1#report_time_sheet.php#Project#0##Reports");
					break;
				case "REP_ACTIVE_PROJ":
					$menuItems = array("0##Reports","1#report_time_sheet.php#Project#0##Reports",
													"2#report_active_projects.php#Project List#1#report_time_sheet.php#Project#0##Reports"/*,
										"2#report_over_budget_projects.php#Projects Over Budget#1#report_time_sheet.php#Project#0##Reports"*/);
					break;
				case "REP_OVER_BUD_PROJ_CONS":
					$menuItems = array("0##Reports","1#report_time_sheet.php#Project#0##Reports",
										"2#report_over_budget_projects_consulting.php#Projects Over Budget (Consulting)#1#report_time_sheet.php#Project#0##Reports");
					break;
				case "REP_BUDGET":
					$menuItems = array("0##Reports","1#report_cash_flow_forecast.php#Business#0##Reports",
										"2#report_budget.php#Running Cost Report#1#report_cash_flow_forecast.php#Business#0##Reports",
										"2#report_budget_breakdown.php#Running Cost Breakdown#1#report_cash_flow_forecast.php#Business#0##Reports",
										"2#report_budget_period.php#Running Cost vs Invoiced#1#report_cash_flow_forecast.php#Business#0##Reports");
					break;
				case "REP_BUDGET_PERIOD":
					$menuItems = array("0##Reports","1#report_cash_flow_forecast.php#Business#0##Reports",
										"2#report_budget_period.php#Running Cost vs Invoiced#1#report_cash_flow_forecast.php#Business#0##Reports");
					break;
				case "REP_CASH_FLOW":
					$menuItems = array("0##Reports","1#report_cash_flow_forecast.php#Business#0##Reports",
										"2#report_cash_flow_forecast.php#Cash Flow Forecast#1#report_cash_flow_forecast.php#Business#0##Reports");
					break;
				case "REP_CASH_FLOW2":
					$menuItems = array("0##Reports","1#report_cash_flow_forecast.php#Business#0##Reports",
										"2#report_cash_flow_forecast2.php#Cash Flow Forecast V2#1#report_cash_flow_forecast.php#Business#0##Reports");
					break;
				case "REP_COMPANY_BREAK":
					$menuItems = array("0##Reports","1#report_company_breakdown.php#Administrative#0##Reports",
										"2#report_company_breakdown.php#Company Breakdown#1#report_company_breakdown.php#Administrative#0##Reports");
					break;
				case "REP_INVOICE_TRACK":
					$menuItems = array("0##Reports","1#report_cash_flow_forecast.php#Business#0##Reports",
										"2#report_invoice_tracking.php#Invoice Tracking#1#report_cash_flow_forecast.php#Business#0##Reports");
					break;
				case "REP_COMPANY_SUMM":
					$menuItems = array("0##Reports","1#report_company_breakdown.php#Administrative#0##Reports",
										"2#report_company_summary.php#Company Summary#1#report_company_breakdown.php#Administrative#0##Reports");
					break;
				case "REP_EX_SHEET":
					$menuItems = array("0##Reports","1#report_time_sheet.php#Project#0##Reports",
										"2#report_expense_sheet.php#Expense Sheet#1#report_time_sheet.php#Project#0##Reports");
					break;
				case "REP_GANTT":
					$menuItems = array("0##Reports","1#report_time_sheet.php#Project#0##Reports",
										"2#report_gantt.php#Gantt#1#report_time_sheet.php#Project#0##Reports");
					break;
				case "REP_GROWTH_ANALYSIS":
					$menuItems = array("0##Reports","1#report_cash_flow_forecast.php#Business#0##Reports",
										"2#report_growth_analysis.php#Growth Analysis#1#report_cash_flow_forecast.php#Business#0##Reports");
					break;
				case "REP_INCOME_FORECAST":
					$menuItems = array("0##Reports","1#report_cash_flow_forecast.php#Business#0##Reports",
										"2#report_income_forecast_time.php#Income Forecast#1#report_cash_flow_forecast.php#Business#0##Reports",
										"3#report_income_forecast.php#Summary (Average)#2#report_income_forecast_time.php#Income Forecast#1#report_cash_flow_forecast.php#Business");
					break;
				case "REP_IN_FORE_INVOICE":
					$menuItems = array("0##Reports","1#report_cash_flow_forecast.php#Business#0##Reports",
										"2#report_income_forecast_time.php#Income Forecast#1#report_cash_flow_forecast.php#Business#0##Reports",
										"3#report_income_forecast_invoice.php#Invoice Based#2#report_income_forecast_time.php#Income Forecast#1#report_cash_flow_forecast.php#Business");
					break;
				case "REP_IN_FORE_TIME":
					$menuItems = array("0##Reports","1#report_cash_flow_forecast.php#Business#0##Reports",
										"2#report_income_forecast_time.php#Income Forecast#1#report_cash_flow_forecast.php#Business#0##Reports",
										"3#report_income_forecast_time.php#Time Based#2#report_income_forecast_time.php#Income Forecast#1#report_cash_flow_forecast.php#Business");
					break;
				case "REP_PROJ_BREAK":
					$menuItems = array("0##Reports","1#report_time_sheet.php#Project#0##Reports",
										"2#report_project_breakdown.php#Consulting Breakdown#1#report_time_sheet.php#Project#0##Reports");
					break;
				case "REP_IN_VS_CTC":
					/*
					$menuItems = array("0##Reports","1#report_cash_flow_forecast.php#Business#0##Reports",
										"2#report_invoiced_vs_cost_to_company.php#Invoice Potential vs Cost to Company#1#report_cash_flow_forecast.php#Business#0##Reports");
					*/
					$menuItems = array("0##Reports","1#report_cash_flow_forecast.php#Business#0##Reports",
										"2#report_invoiced_vs_cost_to_company_v2.php#Invoice Potential vs Cost to Company#1#report_cash_flow_forecast.php#Business#0##Reports");
					break;
				case "REP_IN_VS_CTC_V2":
					$menuItems = array("0##Reports","1#report_cash_flow_forecast.php#Business#0##Reports",
										"2#report_invoiced_vs_cost_to_company_v2.php#Invoice Potential vs Cost to Company#1#report_cash_flow_forecast.php#Business#0##Reports");
					break;
				case "REP_LEAVE":
					$menuItems = array("0##Reports","1#report_leave.php#Leave#0##Reports");
					break;
				case "REP_LOGBOOK":
					$menuItems = array("0##Reports","1#report_logbook.php#Log Book#0##Reports");
					break;
				case "REP_LOADED_INV":
					$menuItems = array("0##Reports","1#report_cash_flow_forecast.php#Business#0##Reports",
										"2#report_loaded_invoices.php#Loaded Invoices#1#report_cash_flow_forecast.php#Business#0##Reports");
					break;
				case "REP_PROGRESS":
					$menuItems = array("0##Reports","1#report_time_sheet.php#Project#0##Reports",
										"2#report_progress.php#Progress#1#report_time_sheet.php#Project#0##Reports");
					break;
				case "REP_TYPE_SUM":
					$menuItems = array("0##Reports","1#report_time_sheet.php#Project#0##Reports",
										"2#report_summary_time.php#Summaries#1#report_time_sheet.php#Project#0##Reports",
										"3#report_summary_type.php#Type Summary#2#report_summary_time.php#Summaries#1#report_time_sheet.php#Project");
					break;
				case "REP_PROJ_INFO":
					$menuItems = array("0##Reports","1#report_time_sheet.php#Project#0##Reports",
										"2#report_summary_time.php#Summaries#1#report_time_sheet.php#Project#0##Reports",
										"3#report_project_info.php#Project Overview#2#report_summary_time.php#Summaries#1#report_time_sheet.php#Project");
					break;
				case "REP_PROJ_LIST":
					$menuItems = array("0##Reports","1#report_time_sheet.php#Project#0##Reports",
										"2#report_project_list.php#User/Project List#1#report_time_sheet.php#Project#0##Reports");
					break;
				case "REP_PROJ_TYPE_COM":
					$menuItems = array("0##Reports","1#report_cash_flow_forecast.php#Business#0##Reports",
										"2#report_project_type_comparison.php#Project Type Comparison#1#report_cash_flow_forecast.php#Business#0##Reports",
										"2#report_cumulative_income.php#Cumulative Income#1#report_cash_flow_forecast.php#Business#0##Reports");
					break;
				case "REP_ANALYSIS":
					$menuItems = array("0##Reports","1#report_cash_flow_forecast.php#Business#0##Reports",
										"2#report_analysis.php#Business Analysis#1#report_cash_flow_forecast.php#Business#0##Reports");
					break;
				case "REP_RUNNING_PROF":
					$menuItems = array("0##Reports","1#report_cash_flow_forecast.php#Business#0##Reports",
										"2#report_running_profit.php#Running Profit#1#report_cash_flow_forecast.php#Business#0##Reports");
					break;
				case "REP_SUMMARY":
					$menuItems = array("0##Reports","1#report_time_sheet.php#Project#0##Reports",
										"2#report_summary_time.php#Summaries#1#report_time_sheet.php#Project#0##Reports",
										"3#report_summary.php#Time & Expenses#2#report_summary_time.php#Summaries#1#report_time_sheet.php#Project");
					break;
				case "REP_SUMMARY_EX":
					$menuItems = array("0##Reports","1#report_time_sheet.php#Project#0##Reports",
										"1#report_summary_time.php#Summaries#1#report_time_sheet.php#Project#0##Reports",
										"3#report_summary_expenses.php#Expenses#2#report_summary_time.php#Summaries#1#report_time_sheet.php#Project");
					break;
				case "REP_SUMMARY_TIME":
					$menuItems = array("0##Reports","1#report_time_sheet.php#Project#0##Reports",
										"2#report_summary_time.php#Summaries#1#report_time_sheet.php#Project#0##Reports",
										"3#report_summary_time.php#Time#2#report_summary_time.php#Summaries#1#report_time_sheet.php#Project");
					break;
				case "REP_TIME_COM":
					$menuItems = array("0##Reports","1#report_cash_flow_forecast.php#Business#0##Reports",
										"2#report_time_comparison.php#Time Comparison#1#report_cash_flow_forecast.php#Business#0##Reports");
					break;
				case "REP_TIME_SHEET":
					$menuItems = array("0##Reports","1#report_time_sheet.php#Project#0##Reports",
										"2#report_time_sheet.php#Time Sheet#1#report_time_sheet.php#Project#0##Reports");
					break;
				case "REP_WORK_IN_PR":
					$menuItems = array("0##Reports","1#report_time_sheet.php#Project#0##Reports",
										"2#report_work_in_process.php#Work In Process#1#report_time_sheet.php#Project#0##Reports",
										"3#report_work_in_process.php#Project Lifetime#2#report_work_in_process.php#Work In Process#1#report_time_sheet.php#Project");
					break;
				case "REP_WORK_IN_PR_M":
					$menuItems = array("0##Reports","1#report_time_sheet.php#Project#0##Reports",
										"2#report_work_in_process.php#Work In Process#1#report_time_sheet.php#Project#0##Reports",
										"3#report_work_in_process_months.php#Past 6 Months#2#report_work_in_process.php#Work In Process#1#report_time_sheet.php#Project");
					break;
				case "CHNG_PW":
					$menuItems = array("0#modal_profile#Profile Settings");
					break;
				case "EULA":
					$menuItems = array("0#eula.php#EULA");
					break;
				case "REP_SP_TIME_SHEET":
					$menuItems = array("0##Reports","1#report_sp_time_sheet.php#Shared Project#0##Reports",
										"2#report_sp_time_sheet.php#Time Sheet#1#report_sp_time_sheet.php#Shared Project#0##Reports");
					break;
				case "REP_SP_EX_SHEET":
					$menuItems = array("0##Reports","1#report_sp_time_sheet.php#Shared Project#0##Reports",
										"2#report_sp_expense_sheet.php#Expense Sheet#1#report_sp_time_sheet.php#Shared Project#0##Reports");
					break;
				case "REP_SP_SUMMARY_TIME":
					$menuItems = array("0##Reports","1#report_sp_time_sheet.php#Shared Project#0##Reports",
										"2#report_sp_summary_time.php#Summaries#1#report_sp_time_sheet.php#Shared Project#0##Reports",
										"3#report_sp_summary_time.php#Time#2#report_sp_summary_time.php#Summaries#1#report_sp_time_sheet.php#Shared Project");
					break;
				case "REP_SP_SUMMARY_EX":
					$menuItems = array("0##Reports","1#report_sp_time_sheet.php#Shared Project#0##Reports",
										"1#report_sp_summary_time.php#Summaries#1#report_sp_time_sheet.php#Shared Project#0##Reports",
										"3#report_sp_summary_expenses.php#Expenses#2#report_sp_summary_time.php#Summaries#1#report_sp_time_sheet.php#Shared Project");
					break;
				case "REP_SP_SUMMARY":
					$menuItems = array("0##Reports","1#report_sp_time_sheet.php#Shared Project#0##Reports",
										"2#report_sp_summary_time.php#Summaries#1#report_sp_time_sheet.php#Shared Project#0##Reports",
										"3#report_sp_summary.php#Time & Expenses#2#report_sp_summary_time.php#Summaries#1#report_sp_time_sheet.php#Shared Project");
					break;
				case "REP_SP_BREAK":
					$menuItems = array("0##Reports","1#report_sp_time_sheet.php#Shared Project#0##Reports",
										"2#report_sp_project_breakdown.php#Consulting Breakdown#1#report_sp_time_sheet.php#Shared Project#0##Reports");
					break;
				case "REP_SP_INV":
					$menuItems = array("0##Reports","1#report_sp_time_sheet.php#Shared Project#0##Reports",
										"2#report_sp_invoice.php#Invoices#1#report_sp_time_sheet.php#Shared Project#0##Reports");
					break;
				case "COMP_MAN":
					$menuItems = array("0##On-Track Admin","1#companies.php#Companies#0##On-Track Admin");
					break;
				case "MODULE_MAN":
					$menuItems = array("0##On-Track Admin","1#modules.php#Module Management#0##On-Track Admin");
					break;
				case "ACTION_MAN":
					$menuItems = array("0##On-Track Admin","1#action_manager.php#Action Management#0##On-Track Admin");
					break;
				//case "NEWS_MAN":
				//    $menuItems = array("0##On-Track Admin","1#news_add.php#News#0##On-Track Admin");
				//    break;
				case "COMP_ASSIGN":
					$menuItems = array("0##On-Track Admin","1#company_assignment.php#Privilege Assignment#0##On-Track Admin",
										"2#company_assignment.php#Company Assignment#1#company_assignment.php#Privilege Assignment#0##On-Track Admin");
					break;
				case "RESOURCE_ASSIGN":
					$menuItems = array("0##On-Track Admin","1#company_assignment.php#Privilege Assignment#0##On-Track Admin",
										"2#resource_assignment.php#Shared Resource Assignment#1#company_assignment.php#Privilege Assignment#0##On-Track Admin");
					break;
				default:
					break;
			}

			if (is_array($menuItems))   {
				foreach ($menuItems as $menuItem)  {
					if ($menuItem != "")   {
						$menuItem = preg_split("/#/", $menuItem);

						$actionID = q("SELECT id FROM actions WHERE action = '".$action."'");

						if ($menuItem[0] == 0)
							$menuID = q("SELECT id FROM menus WHERE level = '".$menuItem[0]."' AND urlLink = '".$menuItem[1]."' ".
											"AND menuDescr = '".$menuItem[2]."'");
						else if ($menuItem[0] > 1)
							$menuID = q("SELECT id FROM menus WHERE level = '".$menuItem[0]."' AND urlLink = '".$menuItem[1]."' ".
										"AND menuDescr = '".$menuItem[2]."' AND parentID = (SELECT id FROM menus WHERE level = ".$menuItem[3]." ".
										"AND urlLink = '".$menuItem[4]."' AND menuDescr = '".$menuItem[5]."' AND parentID = (SELECT id FROM menus ".
											"WHERE level = ".$menuItem[6]." AND urlLink = '".$menuItem[7]."' AND menuDescr = '".$menuItem[8]."'))");
						else
							$menuID = q("SELECT id FROM menus WHERE level = '".$menuItem[0]."' AND urlLink = '".$menuItem[1]."' ".
										"AND menuDescr = '".$menuItem[2]."' AND parentID = (SELECT id FROM menus WHERE level = ".$menuItem[3]." ".
										"AND urlLink = '".$menuItem[4]."' AND menuDescr = '".$menuItem[5]."')");

						$insert = q("INSERT INTO menuAction (menuID, actionID) VALUES (".$menuID.", ".$actionID.")");
					}
				}
			}
		}
	}

	// Menu Index
	$drop = q("DROP TABLE IF EXISTS MenuIndex");
	$create = q("CREATE TABLE MenuIndex (
			id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
			company_id SMALLINT(5) UNSIGNED NOT NULL, INDEX(company_id),
			user_id SMALLINT(5) UNSIGNED NOT NULL, INDEX(user_id),
			menu_id MEDIUMINT(8) UNSIGNED NOT NULL, INDEX(menu_id)
	)");

	$menus = getMenus_();
	$users = q("SELECT cu.company_id, cu.user_id, c.name, e.email FROM Company_Users AS cu INNER JOIN Company AS c ON c.id = cu.company_id ".
						"INNER JOIN Employee AS e ON e.id = cu.user_id ORDER BY c.name, e.email");

	foreach($users as $u)	{
		$lookup = addMenuIndex($menus, $u[0], $u[1], $u[3]);
	}
?>
