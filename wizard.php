                    <!--  BEGIN: ON-TRACK SETUP WIZARD MODAL  -->
                    <div id="modal_wizard" name="modal_wizard" class="on-contact-modal">
                        <table>
                            <tr>
                                <td colspan="100%" class="centerdata">
                                    <img src="images/logo.png" />
                                    <br/><br/>
                                    <strong style="color:orange;font-weight:bold;font-size:22px;font-variant:small-caps;">
                                        On-Track Setup Wizard
                                    </strong>
                                    <br/><br/>
                                    <table width="300px">
                                        <tr>
                                            <td colspan="2">
                                                <center style="color:orange;font-weight:bold;font-size:18px;font-variant:small-caps;">
                                                    For the latest news & updates,<br/>click on the links below:<br/><br/>
                                                </center>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" width="100px">
                                                <center>
                                                    <a href="http://twitter.com/#!/ite_ontrack" title="twitter" target="_blank">
                                                        <img src="images/on-but-tw.png" />
                                                    </a>
                                                </center>
                                                <center style="color:orange;font-weight:bold;font-size:14px;font-variant:small-caps;">`Follow` us on Twitter!</center>
                                            </td>
                                            <td align="center" width="100px">
                                                <center>
                                                    <a href="https://www.facebook.com/pages/On-Track-Business-Management/274662732552676?ref=ts" title="facebook" target="_blank">
                                                        <img src="images/on-but-fb.png" />
                                                    </a>
                                                </center>
                                                <center style="color:orange;font-weight:bold;font-size:14px;font-variant:small-caps;">`Like` us on Facebook!</center>
                                            </td>
                                        </tr>
                                    </table>
                                    <br/>
                                    <center><input id="closeSocial" name="closeSocial" type="button" value="Close" /></center>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <!--  END: ON-TRACK SETUP WIZARD MODAL  -->
