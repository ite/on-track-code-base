<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("include/sajax.php");
    include("_functions.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

	if (!hasAccess("EXPENSE_MANAGE") && !hasAccess("EXPENSE_VIEW"))
		header("Location: noaccess.php");

	if (hasAccess("EXPENSE_VIEW") && !hasAccess("EXPENSE_MANAGE"))
		$RA_VIEW = 1;

    ///////////////////////////
    //  Sajax
    function getInvoices($id) {
        return q("SELECT id, name FROM LoadInvoices WHERE project_id = '$id' ORDER BY name");
    }

    $sajax_request_type = "GET";
    sajax_init();
    sajax_export("getInvoices");
    sajax_handle_client_request();
    ///////////////////////////

    //  Update Expense Information Function
    if (isset($_POST["update"]) && $_POST["update"] === "1") {
        $projectID = $_POST["projectHidden"];
        $invoices = $_POST["invoiceHidden"];

        if ($invoices == "null")
            $expenses = q("SELECT * FROM ExpenseBudget WHERE project_id = '".$projectID."' ORDER BY due_date");
        else
            $expenses = q("SELECT * FROM ExpenseBudget WHERE invoice_id = '".$invoices."' ORDER BY due_date");

        if (is_array($expenses)) {
            foreach($expenses as $expense) {
                $date = addslashes(strip_tags($_POST["date".$expense[0]]));
				$old_amount = q("SELECT amount FROM ExpenseBudget WHERE id = '".$expense[0]."'");
                $old_date = q("SELECT due_date FROM ExpenseBudget WHERE id = '".$expense[0]."'");
                $old_vat = q("SELECT vat FROM ExpenseBudget WHERE id = '".$expense[0]."'");
                $old_expense = ($old_amount+($old_amount*($old_vat/100)));

                if ($date != "") {
                    if ($_POST["box".$expense[0]])
                        $update = q("UPDATE ExpenseBudget SET due_date = '".$date."', paid = '1' WHERE id = '".$expense[0]."'");
                    else
                        $update = q("UPDATE ExpenseBudget SET due_date = '".$date."', paid = '0' WHERE id = '".$expense[0]."'");
					if ($old_date != $date) {
						statusUpdateDate($old_date, $date, "expense", $old_expense, $_SESSION["company_id"]);
					}
                }
            }
        }

        if ($update)
            header("Location: expense_project.php");
    }

    $projects = q("SELECT id, name FROM Project WHERE completed = '0' AND company_id = '".$_SESSION["company_id"]."' ORDER BY name");

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("2_1", "budget");

    if ($errorMessage != "")
    {
        if ($errorMessage == "Please note that we are currently busy with data management, no entries are allowed for 2010-08-26. Please check back in 24 Hours.")
            echo "<p align='center' style='padding:0px;'><strong style='font-size:12px;'><font color='#FF5900'>$errorMessage</font></strong></p>";
        else
            echo "<p align='center' style='padding:0px;'><strong><font color='#999999'>$errorMessage</font></strong></p>";
        echo "<br/>";
    }
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Sajax
    <?php sajax_show_javascript(); ?>

    function updateInfo()
    {
        document.forms["expense_project"].update.value = 1;
        document.forms["expense_project"].submit();
    }

    ///////////////////////////
    //  Sajax
    function ClearOptions(OptionList) {
        for (x = OptionList.length; x >= 0; x--) {
            OptionList[x] = null;
        }
    }

    function setInvoices(data) {
        var c = 0;

        eval("document.expense_project.invoice.options[" + (c++) + "] = new Option('All Invoices', 'null');");

        for (var i in data) {
            eval("document.expense_project.invoice.options[" + (c++) + "] = new Option('" + data[i][1] + "', '" + data[i][0] + "');");
        }
    }

    function getData(select) {
        ClearOptions(document.expense_project.invoice);

        if (select.value != "null")
            x_getInvoices(select.value, setInvoices);
    }
    ///////////////////////////
</script>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="expense_project">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Expenses By Project</h6>
                            </td>
                        </tr>
                        <tr>
                            <td class="centerdata">
                                <br/>
                            </td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                Project Name:
                            </td>
                            <td width="50%">
                                <select class="on-field" method="post" id="project" name="project" onChange="getData(project);" tabindex="1">
                                    <option value="null">--  Select Project --</option>
                                    <?php
                                        if (is_array($projects)) {
                                            foreach ($projects as $project)
                                                if ($_POST["project"] == $project[0])
                                                    echo "<option value='".$project[0]."' selected>".$project[1]."</option>";
                                                else
                                                    echo "<option value='".$project[0]."'>".$project[1]."</option>";
                                        }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                Choose Invoice:
                            </td>
                            <td align="left" width="50%">
                                <select class="on-field" method="post" name="invoice" tabindex="2">
                                    <option value="null">All Invoices</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="centerdata" colspan="2">
                                <input name="btnGo" tabindex="3" type="submit" value="Show Expenses">
                            </td>
                        </tr>
                    </table>
                    <?php
                        if (isset($_POST["btnGo"]) || ($_GET["pid"] != "" && is_numeric($_GET["pid"]))) {
                            $projectID = (isset($_POST["project"])) ? $_POST["project"] : $_GET["pid"];
                            $invoices = $_POST["invoice"];

                            if ($invoices == "null" || $invoices == "")
                                $expenses = q("SELECT id, descr, amount, vat, due_date, invoice_id, paid FROM ExpenseBudget ".
                                                "WHERE project_id = '".$projectID."' ORDER BY due_date");
                            else
                                $expenses = q("SELECT id, descr, amount, vat, due_date, invoice_id, paid FROM ExpenseBudget ".
                                                "WHERE invoice_id = '".$invoices."' ORDER BY due_date");

                            $total_excl = 0;
                            $total_incl = 0;

                            if (is_array($expenses)) {
                                echo "<br/>";
                                echo "<div class='on-20px'><table class='on-table-center on-table'>";
                                    //  Table Headings
                                    echo "<tr>";
                                        echo "<th>Expense Description</td>";
                                        echo "<th>Expense <i>(".$_SESSION["currency"].") (excl)</i></td>";
                                        echo "<th>Expense <i>(".$_SESSION["currency"].") (incl)</i></td>";
                                        echo "<th>Due Date</td>";
                                        echo "<th>Linked Invoice Paid</td>";
                                        echo "<th>Paid</td>";
                                        if (!$RA_VIEW)
										echo "<th>Edit</td>";
                                    echo "</tr>";
                                    //  Table Information
                                    foreach ($expenses as $expense) {
                                        echo "<tr>";
                                            echo "<td>";
											if ($RA_VIEW)
                                                echo "".$expense[1]."";
											else
												if ($expense[6] == "1")
													echo "<a>".$expense[1]."</a>";
												else
													echo "<a href='expense_split.php?project=$projectID&id=".$expense[0]."'>".$expense[1]."</a>";
                                            echo "</td>";
                                            if ($expense[2] != "") {
                                                $total_excl += $expense[2];

                                                echo "<td class='rightdata'>".number_format($expense[2], 2, ".", "")."</td>";
                                            }
                                            else
                                                echo "<td class='rightdata'></td>";
                                            if ($expense[2] != "") {
                                                $incl_vat = $expense[2] + ($expense[2] * ($expense[3] / 100));
                                                $total_incl += $incl_vat;

                                                echo "<td class='rightdata'>".number_format($incl_vat, 2, ".", "")."</td>";
                                            }
                                            else
                                                echo "<td class='rightdata'></td>";
                                            if ($expense[4] != "")
                                                echo "<td class='rightdata'><input name='date".$expense[0]."' ".
                                                    "type='text' style='text-align:right;' value='".$expense[4]."'></td>";
                                            else
                                                echo "<td class='rightdata'></td>";
                                            if ($expense[5] != "0") {
                                                $paid = q("SELECT paid FROM LoadInvoices WHERE id = '".$expense[5]."'");

                                                if ($paid == "1")
                                                    echo "<td style='centerdata'><input name='paid".$expense[0]."' type='checkbox' value='1' ".
                                                        "disabled checked></td>";
                                                else
                                                    echo "<td style='centerdata'><input name='paid".$expense[0]."' type='checkbox' value='1' ".
                                                        "disabled></td>";
                                            }
                                            else
                                                echo "<td style='centerdata'></td>";
                                            if ($expense[6] == "1")
                                                echo "<td style='centerdata'><input name='box".$expense[0]."' type='checkbox' value='1' ".
                                                    "checked></td>";
                                            else
                                                echo "<td style='centerdata'><input name='box".$expense[0]."' type='checkbox' value='1'>".
                                                    "</td>";
											if (!$RA_VIEW)	{
												echo "<td style='centerdata'>";
													echo "<input name='btnEdit' type='button' ".
														"onClick=\"location.href='expense_edit.php?id=".$expense[0]."'\" value='Edit'>";
												echo "</td>";
											}
                                        echo "</tr>";
                                    }

                                    echo "<tr><td class='on-table-total'>Grand Total ".
                                        "<i>(".$_SESSION["currency"].")</i>:</td><td class='on-table-total'>".
                                        number_format($total_excl, 2, ".", "")."</td><td class='on-table-total'>".
                                        number_format($total_incl, 2, ".", "")."</td><td class='on-table-total' colspan='4'>".
                                        "</td></tr>";
                                    echo "<tr>";
                                        echo "<td align='center' colspan='100%><br/><input name='btnUpdate' onClick='updateInfo();' type='button' ".
                                            "value='Update Information'></td>";
                                    echo "</tr>";
                                echo "</table></div>";
                                echo "<input method='post' name='projectHidden' type='hidden' value='$projectID' />";
                                echo "<input method='post' name='invoiceHidden' type='hidden' value='$invoices' />";
                            }
                        }
                    ?>
                    <br/>
                    <input method="post" name="update" type="hidden" value="0" />
                </form>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
