<?php
	session_start();

	include("_db.php");
	include("graphics.php");

	if (!$_SESSION["logged_in"] === true)
		header("Location: login.php");

	if (!isset($_SESSION["company_id"]))
		header("Location: home.php");

	if (!hasAccess("LOCATION_MANAGEMENT"))
		header("Location: noaccess.php");

	$index = 1;

	//  Print Header
	print_header();
	//  Print Menu
	print_menus("1", "locations");
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
	var locations_LOAD;

	jQuery(function()    {
		locations_LOAD = function()  {
			jQuery.post("_ajax.php", {func: "get_locations"}, function(data)	{
				data = json_decode(data);

				jQuery("#tableContent tbody>tr").not(":first").not(":last").remove();
				var lstRow = jQuery("#tableContent tbody>tr:last");

				if (is_array(data))	{
					jQuery.each(data,function(i, v)	{
						var n = lstRow.clone(true);

						n.children().eq(0).children().eq(0).text((v[1] != null) ? v[1] : "").attr("id", (v[0] != null) ? v[0] : "").attr("id", (v[0] != null) ? v[0] : "");
						n.children().eq(1).text((v[2] != null) ? v[2] : "");
						n.children().eq(2).text((v[3] != null) ? v[3] : "");
						n.children().eq(3).text((v[4] != null) ? v[4] : "");
						n.children().eq(4).text((v[5] != null) ? v[5] : "");
						n.children().eq(5).text((v[6] != null) ? v[6] : "");
						n.children().eq(6).text((v[7] != null) ? v[7] : "");

						n.insertBefore(lstRow);
					});

					lstRow.remove();

					jQuery("#informationDiv").show();
				}

				jQuery("#addBtnDiv").show();
			});
		};
		locations_LOAD();

		jQuery(".btnAdd").click(function() {
			jQuery("#id").val("");
			jQuery("#form").attr("action", "locationsAddEdit.php");
			jQuery("#form").submit();
		});

		jQuery(".btnEdit").click(function() {
			jQuery("#id").val(jQuery(this).attr("id"));
			jQuery("#form").attr("action", "locationsAddEdit.php");
			jQuery("#form").submit();
		});
	});
</script>
<table width="100%">
	<tr height="380px">
		<td class="centerdata" valign="top">
			<form id="form" name="form" action="" method="post">
				<table width="100%">
					<tr>
						<td class="centerdata">
							<h6>Location Management
						</td>
					</tr>
				</table>
				<br/>
				<div id="informationDiv" name="informationDiv" class="hidden">
					<div class="on-20px">
					<table class="on-table-center on-table" id="tableContent" name="tableContent">
						<tr>
							<th>Location Description</th>
							<th>Latitude</th>
							<th>Longitude</th>
							<th>Created By</th>
							<th>Date Created</th>
							<th>Updated By</th>
							<th>Date Updated</th>
						</tr>
						<!--  Table Information   -->
						<?php
							echo "<tr>";
								echo "<td style='white-space:nowrap;'>";
									echo "<a id='' name='' class='btnEdit' style='cursor: hand' tabindex='".$index++."'></a>";
								echo "</td>";
								echo "<td></td>";
								echo "<td></td>";
								echo "<td></td>";
								echo "<td></td>";
								echo "<td></td>";
								echo "<td></td>";
							echo "</tr><tfoot><tr><td colspan='100%'></td></tr></tfoot>";
						?>
					</table>
					</div>
					<br/>
				</div>
				<div id="addBtnDiv" name="addBtnDiv" class="hidden">
					<input id="btnAdd" name="btnAdd" class="btnAdd" tabindex="<?php echo $index++; ?>" type="button" value="Add New">
				</div>
				<input id="id" name="id" type="hidden" value="">
			</form>
		</td>
	</tr>
	<tr>
		<td align="center">
			<br/>
		</td>
	</tr>
</table>
<?php
	//  Print Footer
	print_footer();
?>
