<?php
class Module    {
    ///////////////////////////
    //   Module Constructor
    ///////////////////////////
    function Module()       {
    }

    ///////////////////////////
    //   Functions Of Module
    ///////////////////////////
    //  Get PHP Script For Before Headers Are Printed
    function getPHPScript()     {
        include("_functions.php");

        function getJQeury()        {
        ?>
        <link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
        <script type="text/javascript" src="include/datepicker.js"></script>
        <script language="JavaScript" src="include/validation.js"></script>
        <script language="JavaScript">
			var loadDates;
			var loadCompanies;
			var loadProjects;

			function is_array(input)    {
				return typeof(input)=='object'&&(input instanceof Array);
			}

            jQuery(function()        {
            <?php
                $dateFields = array("dateFrom","dateTo");

                foreach ($dateFields as $df)    jQDate($df);
            ?>

                jQuery("#dateType").change(function()        {
                    if (jQuery(this).val() == "custom") jQuery("#datesDiv").show();
                    else                                jQuery("#datesDiv").hide();

					loadDates();
                });

				loadDates = function()	{
					var project = jQuery("#project").val();
					var dateType = jQuery("#dateType").val();

					jQuery.post("_ajax.php", {func: "getDates", project: project, dateType: dateType}, function(data)	{
						data = json_decode(data);

						if (is_array(data))	{
							jQuery("#dateFrom").val(data[0]);
							jQuery("#dateTo").val(data[1]);
						}
					});
				}

				loadCompanies = function()	{
					var project = jQuery("#project").val();

					jQuery.post("_ajax.php", {func: "getApprovalCompanies", project: project}, function(data)	{
						data = json_decode(data);

						jQuery("#company option").remove();

						if (is_array(data))	{
							jQuery("#company").append(jQuery("<option></option>").attr("value","null").text("All Companies"));

							jQuery.each(data,function(i, v)	{
								jQuery("#company").append(jQuery("<option></option>").attr("value",v[0]).text(v[1]));
							});

							jQuery("#company").parent().parent().show();
						} else	{
							jQuery("#company").append(jQuery("<option></option>").attr("value","null").attr("disabled","disabled").text("No available companies"));
							jQuery("#company").parent().parent().hide();
						}
					});
				}

				loadProjects = function()	{
					var report = jQuery("#report").val();
					var level = jQuery("#level").val();
					var entryStatus = jQuery("#entryStatus").val();
					var projectType = jQuery("#projectType").val();
					var mainProjectType = jQuery("#mainProjectType").val();

					jQuery.post("_ajax.php", {func: "getApprovalProjects", report: report,level: level, entryStatus: entryStatus, projectType: projectType, mainProjectType: mainProjectType},
												function(data)	{
						data = json_decode(data);

						jQuery("#project option").remove();

						if (is_array(data))	{
							jQuery("#project").append(jQuery("<option></option>").attr("value","null").text("All Projects"));

							jQuery.each(data,function(i, v)	{
								jQuery("#project").append(jQuery("<option></option>").attr("value",v[0]).text(v[1]));
							});
						} else	{
							jQuery("#project").append(jQuery("<option></option>").attr("value","null").attr("disabled","disabled").text("No available projects"));
						}

						jQuery("#dateType option[value='projectLifetime']").attr("disabled","disabled");
						jQuery("#dateType option[value='currentwk']").attr("selected", "selected");
					});
				}

				jQuery("#entryStatus, #projectType, #mainProjectType").change(function()	{
					loadProjects();
				});

				jQuery("#project").change(function()    {
					if (jQuery(this).val() != "null")   {
						jQuery("#dateType option[value='projectLifetime']").removeAttr("disabled");
						jQuery("#dateType option").removeAttr("selected");
						jQuery("#dateType option[value='projectLifetime']").attr("selected", "selected");
					} else      {
						jQuery("#dateType option[value='projectLifetime']").attr("disabled","disabled");
						jQuery("#dateType option[value='currentwk']").attr("selected", "selected");
						loadProjects();
					}

					loadDates();
				});
            });
        </script>
        <?php
        }

        //  Update/Approve/Decline Function
        if (isset($_POST["update"]) && $_POST["update"] === "1")        {
            unset($companyArr);
            unset($counterArr);

			$level = $_GET["level"];
            $types = array("Time","Expenses");

            if (is_array($types))       {
				$hasAreas = addslashes(strip_tags($_POST["hasAreas"]));

                foreach ($types as $type)       {
					if ($hasAreas)	$fields = ($type == "Time") ? array("A","B","C","E","F","G","H","I","J","K") : array("L","M","N","P","Q","R","S","T","U","V");
					else			$fields = ($type == "Time") ? array("A","B","E","F","G","H","I","J","K") : array("L","M","P","Q","R","S","T","U","V");

                    $count = addslashes(strip_tags($_POST[$type]));

                    for ($i = 1; $i < $count; $i++)     {
						$doUpdate = false;

						$pos = 0;

                        $id = addslashes(strip_tags($_POST[$fields[$pos++].$i]));
                        $projectID = addslashes(strip_tags($_POST[$fields[$pos++].$i]));

                        if ($hasAreas)	$areaID = addslashes(strip_tags($_POST[$fields[$pos++].$i]));

						$activityID = addslashes(strip_tags($_POST[$fields[$pos++].$i]));
                        $descr = addslashes(strip_tags($_POST[$fields[$pos++].$i]));

                        $amount = addslashes(strip_tags($_POST[$fields[$pos++].$i]));
                        $rate = addslashes(strip_tags($_POST[$fields[$pos++].$i]));
                        $cost = addslashes(strip_tags($_POST[$fields[$pos++].$i]));
                        $previous_approved = addslashes(strip_tags($_POST[$fields[$pos].$i."_Prev"]));
                        $approved = addslashes(strip_tags($_POST[$fields[$pos++].$i]));
                        $previous_declined = addslashes(strip_tags($_POST[$fields[$pos].$i."_Prev"]));
                        $declined = addslashes(strip_tags($_POST[$fields[$pos++].$i]));

                        if ($amount == "-")	$amount = "";
						if ($type == "Expenses")	$expenseType = q("SELECT name FROM dropdowns WHERE id = (SELECT expense_type_id FROM ApprovedExpense WHERE id = '".$id."')");

						if ($hasAreas)	$select = ($type == "Time") ? "company_id,project_id,area_id,activity_id,descr,time_spent,rate,status,status2"
																	: (($expenseType == "Driving") ? "company_id,project_id,area_id,activity_id,descr,kilometers,rate,status,status2"
																									: "company_id,project_id,area_id,activity_id,descr,total,rate,status,status2");
						else			$select = ($type == "Time") ? "company_id,project_id,activity_id,descr,time_spent,rate,status,status2"
																	: (($expenseType == "Driving") ? "company_id,project_id,activity_id,descr,kilometers,rate,status,status2"
																									: "company_id,project_id,activity_id,descr,total,rate,status,status2");

                        $table = ($type == "Time") ? "ApprovedTime" : "ApprovedExpense";

						$previous_values = q("SELECT $select FROM $table WHERE id = '".$id."'");
						$originalID = ($type == "Time") ? q("SELECT timesheet_id FROM $table WHERE id = '".$id."'") : q("SELECT expensesheet_id FROM $table WHERE id = '".$id."'");

                        $status = 0;

						//  COMPARE PREVIOUS AND CURRENT VALUES
						if ($hasAreas)	{
							if ($projectID != $previous_values[0][1] ||
								$areaID != $previous_values[0][2] ||
								$activityID != $previous_values[0][3] ||
								$descr != $previous_values[0][4] ||
								$amount != $previous_values[0][5] ||
								$rate != $previous_values[0][6])
								$status = 1;
						} else	{
							if ($projectID != $previous_values[0][1] ||
								$activityID != $previous_values[0][2] ||
								$descr != $previous_values[0][3] ||
								$amount != $previous_values[0][4] ||
								$rate != $previous_values[0][5])
								$status = 1;
						}

						if ($approved == "1" && $previous_approved != "1")
							$status = 2;

						if ($declined == "1" && $previous_declined != "1")
							$status = 3;

						if (($approved != "1" && $previous_approved == "1" && $status != 2) || ($declined != "1" && $previous_declined == "1" && $status != 3))
							$status = 1;

						//echo "APPROVED [".$approved."] | PREVIOUS APPROVED [".$previous_approved."] | TEST [".($approved == "1" && $previous_approved != "1")."] | STATUS [".$status."]<br/>";
						//echo "DECLINED [".$declined."] | PREVIOUS DECLINED [".$previous_declined."] | TEST [".($approved == "1" && $previous_approved != "1")."] | STATUS [".$status."]<br/>";
						//exit;

                        $isPM = q("SELECT manager FROM Project_User WHERE user_id = '".$_SESSION["user_id"]."' AND project_id = '".$projectID."' AND company_id = '".$previous_values[0][0]."'");
                        $isSPM = q("SELECT spm_manager FROM Project_User WHERE user_id = '".$_SESSION["user_id"]."' AND project_id = '".$projectID."'AND company_id = '".$_SESSION["company_id"]."'");

                        $auditTable = ($type == "Time") ? "auditlogtime" : "auditlogexpense";
						$auditFields = ($type == "Time") ? "user_id,project_id,area_id,activity_id,descr,rate,time_spent,on_date,on_time,status,status2"
															: "user_id,project_id,area_id,expense_type_id,vehicle_id,disbursement_id,descr,kilometers,total,rate,expense,on_date,on_time,odoStart,odoFinish,businessKM,".
																"privateKM,status,status2";

						if ($hasAreas)	$updateFields = ($type == "Time") ? "project_id = '".$projectID."',area_id = '".$areaID."',activity_id = '".$activityID."',descr = '".$descr."',time_spent = '".$amount."',".
																				"rate = '".$rate."',"
																			: (($expenseType == "Driving") ? "project_id = '".$projectID."',area_id = '".$areaID."',activity_id = '".$activityID."',descr = '".$descr."',".
																												"kilometers = '".$amount."',rate = '".$rate."',expense = '".$cost."',"
																											: "project_id = '".$projectID."',area_id = '".$areaID."',activity_id = '".$activityID."',descr = '".$descr."',".
																												"total = '".$amount."',rate = '".$rate."',expense = '".$cost."',");
						else			$updateFields = ($type == "Time") ? "project_id = '".$projectID."',activity_id = '".$activityID."',descr = '".$descr."',time_spent = '".$amount."',".
																				"rate = '".$rate."',"
																			: (($expenseType == "Driving") ? "project_id = '".$projectID."',activity_id = '".$activityID."',descr = '".$descr."',".
																												"kilometers = '".$amount."',rate = '".$rate."',expense = '".$cost."',"
																											: "project_id = '".$projectID."',activity_id = '".$activityID."',descr = '".$descr."',".
																												"total = '".$amount."',rate = '".$rate."',expense = '".$cost."',");

                        $originalTable = ($type == "Time") ? "TimeSheet" : "ExpenseSheet";

/**/
                        if ($status > 0)        {
                            $insert = q("INSERT INTO ".$auditTable." (id_approved,".$auditFields.",date_changed) SELECT ".$id.",".$auditFields.",'".date("Y-m-d H:i:s")."' FROM ".$table." WHERE id = '".$id."'");

                            if ($isPM == 1 && $isSPM == 1)	{
								$update = q("UPDATE ".$table." SET ".$updateFields."status = '".$status."',status2 = '".$status."' WHERE id = '".$id."'");
								$doUpdate = true;
							} else if ($isPM == 1)	{
								$update = q("UPDATE ".$table." SET ".$updateFields."status = '".$status."' WHERE id = '".$id."'");
								$doUpdate = true;
							} else if ($isSPM == 1)	{
								$update = q("UPDATE ".$table." SET ".$updateFields."status2 = '".$status."' WHERE id = '".$id."'");
								$doUpdate = true;
							}

/*
							echo "ORIGINAL PROJECT ID [".$previous_values[0][1]."]<br/>";
							echo "NEW PROJECT ID      [".$projectID."]<br/>";
							if ($projectID != $previous_values[0][1])	echo "PROJECT ID CHANGED<br/>";
							if ($activityID != $previous_values[0][2])	echo "ACTIVITY ID CHANGED<br/>";
							if ($descr != $previous_values[0][3])	echo "DESCRIPTION CHANGED<br/>";
							if ($amount != $previous_values[0][4])	echo "AMOUNT CHANGED<br/>";
							if ($rate != $previous_values[0][5])	echo "RATE CHANGED<br/>";

                            if ($isPM == 1 && $isSPM == 1)	echo "QUERY [UPDATE ".$table." SET ".$updateFields."status = '".$status."',status2 = '".$status."' WHERE id = '".$id."']<br/>";
                            else if ($isPM == 1)			echo "QUERY [UPDATE ".$table." SET ".$updateFields."status = '".$status."' WHERE id = '".$id."']<br/>";
                            else if ($isSPM == 1)			echo "QUERY [UPDATE ".$table." SET ".$updateFields."status2 = '".$status."' WHERE id = '".$id."']<br/>";

							echo "<br/>";
*/
                            if ($doUpdate && is_numeric($originalID))        $update = q("UPDATE ".$originalTable." SET locked = '1' WHERE id = '".$originalID."'");
                        }
/**/
                    }
                }
            }
/*
            if (is_array($companyArr))  {
                foreach ($companyArr as $c)     {
                    if ($counterArr[$c] >= 1)   {
                        $pms = q("SELECT e.frstname, e.lstname, e.email FROM (Project_User AS pu INNER JOIN Employee AS e ON pu.user_id = e.id)
                                    WHERE pu.project_id = '".$projectID."' AND pu.company_id = '".$c."' AND pu.manager = '1'");

                        if (is_array($pms)) {
                            foreach ($pms as $pm) {
                                $usr = "".$pm[0]." ".$pm[1]." <".$pm[2].">";

                                if ($usr == "Johan Coertzen <jc@ite.co.za>")            $recipients .= ",Johan Coertzen <jc@integrityengineering.co.za>";
                                else if ($usr == "Franszo Faul <ffaul@ite.co.za>")      $recipients .= ",Franszo Faul <ffaul@integrityengineering.co.za>";
                                else if ($usr == "Mauritz Kruger <mkruger@ite.co.za>")  $recipients .= ",Mauritz Kruger <mkruger@integrityengineering.co.za>";
                                else                                                    $recipients .= ",".$usr;
                            }
                        }

                        if ($recipients[0] == ",")      $recipients = substr($recipients, 1);

                        //approvalsDeclined(q("SELECT name FROM Project WHERE id = '".$projectID."'"), q("SELECT name FROM Company WHERE id = '".$c."'"), $recipients);
                    }
                }
            }
*/

            header("Location: _module_handler.php?module=approval&what=display&level=".$level);
        }
    }

    //  Get Time Approval Information
    function getDisplay()       {
        getJQeury();
	?>
        <script language="JavaScript">
			var fieldID, fieldVal = "";

            jQuery(function()        {
				loadAreas = function()	{
					jQuery.post("_ajax.php", {func: "get_area_info", projectID: fieldVal}, function(data)	{
						data = json_decode(data);

						fieldID = (fieldID.substr(0,1) == "B") ? "C"+fieldID.substr(1) : "N"+fieldID.substr(1);

						var html = "";

						if (is_array(data))	{
							html += "<select id='"+fieldID+"' name='"+fieldID+"' class=''>";

							jQuery.each(data,function(i, v)	{
								html += "<option value='"+v[0]+"'>"+v[1]+"</option>";
							});

							html += "</select>";
						} else	{
							html += "<input id=\""+fieldID+"\" name=\""+fieldID+"\" class=\"\" type=\"hidden\" value=\"0\">-";
						}

						jQuery("#"+fieldID).parent().html(html);

						loadActivityTypes();
					});
				};

				loadActivityTypes = function()	{
					jQuery.post("_ajax.php", {func: "getActivityTypes", projectID: fieldVal}, function(data)	{
						data = json_decode(data);

						fieldID = (fieldID.substr(0,1) == "C") ? "D"+fieldID.substr(1) : "O"+fieldID.substr(1);

						var prevVal = jQuery("#"+fieldID).val();

						jQuery("#"+fieldID+" option").remove();

						jQuery.each(data,function(i, v)	{
							if (v[0] == prevVal)
								jQuery("#"+fieldID).append(jQuery("<option></option>").attr("value",v[0]).attr("selected",true).text(v[1]));
							else
								jQuery("#"+fieldID).append(jQuery("<option></option>").attr("value",v[0]).text(v[1]));
						});
					});
				};

				loadActivities = function()	{
					jQuery.post("_ajax.php", {func: "get_activities", activityType: fieldVal}, function(data)	{
						data = json_decode(data);

						fieldID = (fieldID.substr(0,1) == "D") ? "E"+fieldID.substr(1) : "P"+fieldID.substr(1);

						var prevVal = jQuery("#"+fieldID).val();

						var html = "";

						html += "<select id='"+fieldID+"' name='"+fieldID+"' class=''>";

						jQuery.each(data,function(i, v)	{
							html += "<option value='"+v[0]+"' "+((v[0] == prevVal) ? "selected" : "")+">"+v[1]+"</option>";
						});

						html += "</select>";

						jQuery("#"+fieldID).parent().html(html);
					});
				};

				jQuery(".project").change(function()	{
					fieldID = jQuery(this).attr("id");
					fieldVal = jQuery(this).val();

					loadAreas();
				});

				jQuery(".activityType").change(function()	{
					fieldID = jQuery(this).attr("id");
					fieldVal = jQuery(this).val();

					loadActivities();
				});

				jQuery(".selectAll").click(function()	{
					var field = jQuery(this).attr("id");
					var checked = jQuery(this).is(":checked");;

					if (field == "J")	{
						if (checked)	jQuery("#K").attr("checked",!checked);

						var recordCount = jQuery("#Time").val();

						for (var i = 1; i < recordCount; i++)	{
							jQuery("#J"+i).attr("checked",checked);
							if (checked)	jQuery("#K"+i).attr("checked",!checked);
						}
					} else if (field == "K")	{
						if (checked)	jQuery("#J").attr("checked",!checked);

						var recordCount = jQuery("#Time").val();

						for (var i = 1; i < recordCount; i++)	{
							if (checked)	jQuery("#J"+i).attr("checked",!checked);
							jQuery("#K"+i).attr("checked",checked);
						}
					} else if (field == "U")	{
						if (checked)	jQuery("#V").attr("checked",!checked);

						var recordCount = jQuery("#Expenses").val();

						for (var i = 1; i < recordCount; i++)	{
							jQuery("#U"+i).attr("checked",checked);
							if (checked)	jQuery("#V"+i).attr("checked",!checked);
						}
					} else if (field == "V")	{
						if (checked)	jQuery("#U").attr("checked",!checked);

						var recordCount = jQuery("#Expenses").val();

						for (var i = 1; i < recordCount; i++)	{
							if (checked)	jQuery("#U"+i).attr("checked",!checked);
							jQuery("#V"+i).attr("checked",checked);
						}
					}
				});

				jQuery(".deselect_decline").click(function()	{
					var field = jQuery(this).attr("id");

					field = (field.substr(0,1) == "J") ? "K"+field.substr(1) : "V"+field.substr(1);

					jQuery("#"+field).attr("checked",false);
				});

				jQuery(".deselect_approve").click(function()	{
					var field = jQuery(this).attr("id");

					field = (field.substr(0,1) == "K") ? "J"+field.substr(1) : "U"+field.substr(1);

					jQuery("#"+field).attr("checked",false);
				});

				jQuery(".recalcTime").change(function()	{
					var recordCount = jQuery("#Time").val();
					var ttlTime = 0;
					var ttlCost = 0;
					var time = "";
					var rate = "";

					for (var i = 1; i < recordCount; i++)	{
						time = parseFloat(jQuery("#G"+i).val()).toFixed(2);
						rate = parseFloat(jQuery("#H"+i).val()).toFixed(2);

						ttlTime += (time * 1);
						ttlCost += time * rate;

						jQuery("#H"+i).val(parseFloat(rate).toFixed(2));
						jQuery("#I"+i).val(parseFloat(time * rate).toFixed(2));
					}

					jQuery("#totalTime").val(parseFloat(ttlTime).toFixed(2));
					jQuery("#totalCost").val(parseFloat(ttlCost).toFixed(2));
				});

				jQuery(".recalcExpense").click(function()	{
					var recordCount = jQuery("#Expenses").val();
					var ttlCost = 0;
					var amount = "";
					var rate = "";

					for (var i = 1; i < recordCount; i++)	{
						amount = jQuery("#R"+i).val();
						rate = jQuery("#S"+i).val();

						if (amount != "-" && rate != "-")	{
							ttlCost += amount * rate;

							jQuery("#R"+i).val(parseFloat(amount).toFixed(2));
							jQuery("#S"+i).val(parseFloat(rate).toFixed(2));
							jQuery("#T"+i).val(parseFloat(amount * rate).toFixed(2));
						} else if (rate != "-")	{
							ttlCost += rate;

							jQuery("#S"+i).val(parseFloat(rate).toFixed(2));
							jQuery("#T"+i).val(parseFloat(rate).toFixed(2));
						}
					}

					jQuery("#expenseTotal").val(parseFloat(ttlCost).toFixed(2));
				});

				jQuery("#btnDisplay").click(function()	{
					var valid = true;
					var test;
					var dateType = jQuery("#dateType").val();

					if (dateType == "custom")	{
						test = validation("date", jQuery("#dateFrom").val());

						if (!test)  jQuery("#dateFrom").show();
						else        jQuery("#dateFrom").hide();

						valid &= test;

						test = validation("date", jQuery("#dateTo").val());

						if (!test)  jQuery("#dateTo").show();
						else        jQuery("#dateTo").hide();

						valid &= test;
					}

					if (valid)	{
						jQuery("#display").val("1");
						jQuery("#approvals").submit();
					}
				});

				jQuery("#btnUpdate").click(function()	{
					jQuery("#update").val("1");
					jQuery("#approvals").submit();
				});
			});
		</script>
	<?php
		$today = date("Y-m-d");
		$level = $_GET["level"];

		$entryStatus = "unapproved";
		$projectType = "invoiceable";
		$mainProjectType = "null";
		$project = "null";
		$company = "null";
		$employee = "null";
		$dateType = "currentmnth";

		if (isset($_POST["entryStatus"]) && $_POST["entryStatus"] != "null")			$entryStatus = $_POST["entryStatus"];
		if (isset($_POST["projectType"]) && $_POST["projectType"] != "null")			$projectType = $_POST["projectType"];
		if (isset($_POST["mainProjectType"]) && $_POST["mainProjectType"] != "null")	$mainProjectType = $_POST["mainProjectType"];
		if (isset($_POST["project"]) && $_POST["project"] != "null")					$project = $_POST["project"];
		if (isset($_POST["company"]) && $_POST["company"] != "null")					$company = $_POST["company"];
		if (isset($_POST["employee"]) && $_POST["employee"] != "null")					$employee = $_POST["employee"];
		if (isset($_POST["dateType"]) && $_POST["dateType"] != "null")					$dateType = $_POST["dateType"];

		if ($dateType == "currentwk")      {
			$dateFrom = currentWeekStart();
			$dateTo = $today;
		}
		else if ($dateType == "previouswk")        {
			$dateFrom = getDates(currentWeekStart(), -7);
			$dateTo = getDates(currentWeekStart(), -1);
		}
		else if ($dateType == "currentmnth")       {
			$dateFrom = getMonthStart($today);
			$dateTo = $today;
		}
		else if ($dateType == "previousmnth")      {
			$dateFrom = getPreviousMonthStart();
			$dateTo = getMonthEnd(getPreviousMonthStart());
		}
		else if ($dateType == "projectLifetime")      {
			$dateFromT = q("SELECT MIN(date) FROM ApprovedTime WHERE project_id = '".$project."'");
			$dateFromE = q("SELECT MIN(date) FROM ApprovedExpense WHERE project_id = '".$project."'");

			if ($dateFromE == null)	$dateFromE = $dateFromT;

			$dateFrom = ($dateFromT <= $dateFromE) ? $dateFromT : $dateFromE;
			$dateTo = $today;
		}
		else if ($dateType == "custom")    {
			$dateFrom = addslashes(strip_tags($_POST["dateFrom"]));
			$dateTo = addslashes(strip_tags($_POST["dateTo"]));
		}

		$n[0][0] = "0"; $n[0][1] = "Shared Projects";
		$mainProjectTypes = q("SELECT id, type FROM ProjectTypes WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY type");

		if (!is_array($mainProjectTypes))	$mainProjectTypes = $n;
		else											$mainProjectTypes = array_merge($n,$mainProjectTypes);

		$mainProjectTypes = sortArrayByColumn($mainProjectTypes, 1);

		$where = "";

		if ($level == "SP")	$where .= "AND p.p_type = 'SP' AND pu.spm_manager = '1' ";

		if ($entryStatus == "unapproved")		$where .= "AND (CASE WHEN p.p_type = 'CP'
																	THEN (ts.status IN ('0','1') OR es.status IN ('0','1'))
																	ELSE (ts.status IN ('0','1') OR es.status IN ('0','1') OR ts.status2 IN ('0','1') OR es.status2 IN ('0','1'))
																END) ";
		else if ($entryStatus == "approved")	$where .= "AND (CASE WHEN p.p_type = 'CP'
																	THEN (ts.status IN ('2') OR es.status IN ('2'))
																	ELSE (ts.status IN ('2') OR es.status IN ('2') OR ts.status2 IN ('2') OR es.status2 IN ('2'))
																END) ";
		else if ($entryStatus == "declined")	$where .= "AND (CASE WHEN p.p_type = 'CP'
																	THEN (ts.status IN ('3') OR es.status IN ('3'))
																	ELSE (ts.status IN ('3') OR es.status IN ('3') OR ts.status2 IN ('3') OR es.status2 IN ('3'))
																END) ";

		if ($projectType == "invoiceable")			$where .= " AND cot.total_budget > 0 AND cot.total_budget != '' AND p.due_date != ''";
		if ($projectType == "non-invoiceable")	$where .= " AND (cot.total_budget = '' OR p.due_date = '')";

		$where .= ($mainProjectType != "null") ? "AND p.type_id = '".$mainProjectType."' " : "";

		$projects = q("SELECT DISTINCT(p.id), p.name
						FROM (Project AS p
							INNER JOIN Project_User AS pu ON pu.project_id = p.id
							INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
							LEFT JOIN ApprovedTime AS ts ON ts.project_id = p.id
							LEFT JOIN ApprovedExpense AS es ON es.project_id = p.id)
						WHERE pu.user_id = '".$_SESSION["user_id"]."'
							AND cot.company_id = '".$_SESSION["company_id"]."'
							AND pu.company_id = '".$_SESSION["company_id"]."'
							AND p.completed = '0'
							AND p.deleted = '0'
							$where
						ORDER BY UPPER(p.name)");

		if ($level == "SP" && $project != "null")	{
			$companies = q("SELECT DISTINCT(c.id), c.name
							FROM (Company AS c INNER JOIN companiesOnTeam AS cot ON c.id = cot.company_id
								INNER JOIN Project AS p ON p.id =  cot.project_id)
							WHERE cot.project_id = '".$project."'
							ORDER BY c.name");
		}

		$employees = q("SELECT e.id, e.lstname, e.frstname
						FROM (Employee AS e
							INNER JOIN Company_Users AS cu ON e.id = cu.user_id)
						WHERE cu.company_id = '".$_SESSION["company_id"]."' AND e.email != 'admin'
						ORDER BY e.lstname, frstname");

        ?>
        <table width="100%">
            <tr>
                <td class="centerdata">
                    <form id="approvals" name="approvals" action="" method="post">
						<table width="100%">
							<tr>
								<td class="centerdata">
									<h6>Approve Time/Expense Sheet(s)</h6>
								</td>
							</tr>
						</table>
						<br/>
						<table width="100%">
							<tr>
								<td class="on-description" width="50%">
									Entry Status:
								</td>
								<td width="50%">
									<select id="entryStatus" name="entryStatus" class="on-field">
										<option value="unapproved" <?php echo ($entryStatus == "unapproved") ? "selected" : ""; ?>>Unapproved/Updated Entries</option>
										<option value="approved" <?php echo ($entryStatus == "approved") ? "selected" : ""; ?>>Approved Entries</option>
										<option value="declined" <?php echo ($entryStatus == "declined") ? "selected" : ""; ?>>Declined Entries</option>
									</select>
								</td>
							</tr>
							<tr>
								<td class="on-description" width="50%">
									Project Type:
								</td>
								<td width="50%">
									<select id="projectType" name="projectType" class="on-field">
										<option value="invoiceable" <?php echo ($entryStatus == "invoiceable") ? "selected" : ""; ?>>Invoiceable</option>
										<option value="non-invoiceable" <?php echo ($entryStatus == "non-invoiceable") ? "selected" : ""; ?>>Non-Invoiceable</option>
									</select>
								</td>
							</tr>
							<tr>
								<td class="on-description" width="50%">
									Project Type:
								</td>
								<td width="50%">
                                    <select id="mainProjectType" name="mainProjectType" class="on-field">
                                        <option value="null">All Project Types</option>
                                        <?php
                                            if (is_array($mainProjectTypes))	{
                                                foreach ($mainProjectTypes as $mPT)	{
                                                    if ($mainProjectType == $mPT[0])	echo "<option value='".$mPT[0]."' selected>".$mPT[1]."</option>";
                                                    else								echo "<option value='".$mPT[0]."'>".$mPT[1]."</option>";
												}
											}
                                        ?>
                                    </select>
								</td>
							</tr>
							<tr>
								<td class="on-description" width="50%">
									Project:
								</td>
								<td width="50%">
									<select id="project" name="project" class="on-field" <?php echo ($level == "SP") ? "onChange=\"loadCompanies();\"" : ""; ?>>
										<option value="null">All Projects</option>
										<?php
                                            if (is_array($projects))	{
                                                foreach ($projects as $p)	{
                                                    if ($project == $p[0])	echo "<option value='".$p[0]."' selected>".$p[1]."</option>";
                                                    else					echo "<option value='".$p[0]."'>".$p[1]."</option>";
												}
											}
										?>
									</select>
								</td>
							</tr>
							<tr class="<?php echo ($level == "CP") ? "hidden" : ""; ?>">
								<td class="on-description" width="50%">
									Company:
								</td>
								<td width="50%">
									<select id="company" name="company" class="on-field">
										<option value="null">All Companies</option>
										<?php
                                            if (is_array($companies))	{
                                                foreach ($companies as $com)	{
                                                    if ($company == $com[0])	echo "<option value='".$com[0]."' selected>".$com[1]."</option>";
                                                    else						echo "<option value='".$com[0]."'>".$com[1]."</option>";
												}
											}
										?>
									</select>
								</td>
							</tr>
							<tr>
								<td class="on-description" width="50%">
									Employee:
								</td>
								<td width="50%">
									<select id="employee" name="employee" class="on-field">
										<option value="null">All Employees</option>
										<?php
                                            if (is_array($employees))	{
                                                foreach ($employees as $emp)	{
                                                    if ($employee == $emp[0])	echo "<option value='".$emp[0]."' selected>".$emp[1].", ".$emp[2]."</option>";
                                                    else						echo "<option value='".$emp[0]."'>".$emp[1].", ".$emp[2]."</option>";
												}
											}
										?>
									</select>
								</td>
							</tr>
							<tr>
								<td class="on-description" width="50%">
									Date Type:
								</td>
								<td width="50%">
									<select id="dateType" name="dateType" class="on-field">
										<option value="currentwk" <?php echo ($dateType == "currentwk") ? "selected" : ""; ?>>Current Week</option>
										<option value="previouswk" <?php echo ($dateType == "previouswk") ? "selected" : ""; ?>>Previous Week</option>
										<option value="currentmnth" <?php echo ($dateType == "currentmnth") ? "selected" : ""; ?>>Current Month</option>
										<option value="previousmnth" <?php echo ($dateType == "previousmnth") ? "selected" : ""; ?>>Previous Month</option>
										<option value="projectLifetime" <?php echo ($dateType == "projectLifetime") ? "selected" : ""; ?> disabled>Project Lifetime</option>
										<option value="custom" <?php echo ($dateType == "custom") ? "selected" : ""; ?>>Custom Dates</option>
									</select>
								</td>
							</tr>
						</table>
						<div id="datesDiv" style="display:<?php echo ($dateType == "custom") ? "block" : "none"; ?>;">
							<table width="100%">
								<tr>
									<td class="on-description" width="50%">
										Date From:
									</td>
									<td  width="50%">
										<input id="dateFrom" name="dateFrom" class="on-field-date date" type="text" style="text-align:right;" value="<?php echo $dateFrom; ?>">
										<div id="dateFromDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo date("Y-m-d"); ?></font></div>
									</td>
								</tr>
								<tr>
									<td class="on-description" width="50%">
										Date To:
									</td>
									<td  width="50%">
										<input id="dateTo" name="dateTo" class="on-field-date date" type="text" style="text-align:right;" value="<?php echo $dateTo; ?>">
										<div id="dateToDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo date("Y-m-d"); ?></font></div>
									</td>
								</tr>
							</table>
						</div>
						<br/>
						<input id="btnDisplay" name="btnDisplay" type="button" value="Display Information">
						<input id="report" name="report" type="hidden" value="0" />
						<input id="level" name="level" type="hidden" value="<?php echo $level; ?>" />
						<input id="display" name="display" type="hidden" value="0" />

						<?php
							if (isset($_POST["display"]) && $_POST["display"] == "1")	{
						?>

							<br/><br/><br/>
						<?php
                            $types = array("Time","Expenses");
							$timeHasAreas = 0;
							$expensesHasAreas = 0;
							$timeHasSP = 0;
							$expensesHasSP = 0;

                            if (is_array($types))       {
                                foreach ($types as $type)       {
									$select = "";
									$where = "";

									$prefix = ($type == "Time") ? "ts" : "es";

									$where .= ($level == "SP") ? "AND p.p_type = 'SP' AND pu.spm_manager = '1' " : "";
									$where .= ($level == "SP" && $company != "null") ? "AND c.id = '".$company."' " : "";
									$where .= ($level == "CP") ? "AND c.id = '".$_SESSION["company_id"]."' " : "";

									if ($entryStatus == "unapproved")		$where .= "AND (CASE WHEN p.p_type = 'CP'
																								THEN (".$prefix.".status IN ('0','1'))
																								ELSE (".$prefix.".status IN ('0','1') OR ".$prefix.".status2 IN ('0','1'))
																							END) ";
									else if ($entryStatus == "approved")	$where .= "AND (CASE WHEN p.p_type = 'CP'
																								THEN (".$prefix.".status IN ('2'))
																								ELSE (".$prefix.".status IN ('2') OR ".$prefix.".status2 IN ('2'))
																							END) ";
									else if ($entryStatus == "declined")	$where .= "AND (CASE WHEN p.p_type = 'CP'
																								THEN (".$prefix.".status IN ('3'))
																								ELSE (".$prefix.".status IN ('3') OR ".$prefix.".status2 IN ('3'))
																							END) ";

									if ($projectType == "invoiceable")			$where .= " AND cot.total_budget > 0 AND cot.total_budget != '' AND p.due_date != ''";
									if ($projectType == "non-invoiceable")	$where .= " AND (cot.total_budget = '' OR p.due_date = '')";

									$where .= ($mainProjectType != "null") ? "AND p.type_id = '".$mainProjectType."' " : "";
									$where .= ($project != "null") ? "AND p.id = '".$project."' " : "";
									$where .= ($employee != "null") ? "AND e.id = '".$employee."' " : "";
									$where .= "AND p.completed = '0' ";
									$where .= "AND p.deleted = '0' ";

									if ($type == "Time")	{
										$areaCount = q("SELECT COUNT(DISTINCT(ar.name))
													FROM (ApprovedTime AS ts
														INNER JOIN Employee AS e ON ts.user_id = e.id
														INNER JOIN Activities AS a ON ts.activity_id = a.id
														INNER JOIN ActivityTypes AS at ON a.parent_id = at.id
														INNER JOIN Project AS p ON ts.project_id = p.id
														INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
														INNER JOIN Company AS c ON cot.company_id = c.id
														LEFT JOIN areas AS ar ON ts.area_id = ar.id )
													WHERE ts.date BETWEEN '".$dateFrom."' AND '".$dateTo."' AND ts.invoiced = '0'
														$where");
										$projectTypes = q("SELECT DISTINCT(p.p_type),p.p_type
														FROM (ApprovedTime AS ts
															INNER JOIN Employee AS e ON ts.user_id = e.id
															INNER JOIN Activities AS a ON ts.activity_id = a.id
															INNER JOIN ActivityTypes AS at ON a.parent_id = at.id
															INNER JOIN Project AS p ON ts.project_id = p.id
															INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
															INNER JOIN Company AS c ON cot.company_id = c.id
															LEFT JOIN areas AS ar ON ts.area_id = ar.id )
														WHERE ts.date BETWEEN '".$dateFrom."' AND '".$dateTo."' AND ts.invoiced = '0'
															$where");

                                        if ($areaCount > 0)	$timeHasAreas = 1;
										if (count($projectTypes) > 1)	$timeHasSP = 1;
										else if ($projectTypes[0][0] == "SP")	$timeHasSP = 1;
									} else	{
										$areaCount = q("SELECT COUNT(DISTINCT(ar.name))
														FROM (ApprovedExpense AS es
																INNER JOIN Employee AS e ON es.user_id = e.id
																INNER JOIN dropdowns AS dd ON es.expense_type_id = dd.id
																LEFT JOIN Activities AS a ON es.activity_id = a.id
																LEFT JOIN ActivityTypes AS at ON a.parent_id = at.id
																INNER JOIN Project AS p ON es.project_id = p.id
																INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
																INNER JOIN Company AS c ON cot.company_id = c.id
																LEFT JOIN areas AS ar ON es.area_id = ar.id
																LEFT JOIN Vehicle AS v ON es.vehicle_id = v.id
																LEFT JOIN Disbursements AS d ON es.disbursement_id = d.id
																LEFT JOIN DisbursementTypes AS dt ON dt.id = d.parent_id)
														WHERE es.date BETWEEN '".$dateFrom."' AND '".$dateTo."' AND es.invoiced = '0'
														$where");
										$projectTypes = q("SELECT DISTINCT(p.p_type),p.p_type
															FROM (ApprovedExpense AS es
																	INNER JOIN Employee AS e ON es.user_id = e.id
																	INNER JOIN dropdowns AS dd ON es.expense_type_id = dd.id
																	LEFT JOIN Activities AS a ON es.activity_id = a.id
																	LEFT JOIN ActivityTypes AS at ON a.parent_id = at.id
																	INNER JOIN Project AS p ON es.project_id = p.id
																	INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
																	INNER JOIN Company AS c ON cot.company_id = c.id
																	LEFT JOIN areas AS ar ON es.area_id = ar.id
																	LEFT JOIN Vehicle AS v ON es.vehicle_id = v.id
																	LEFT JOIN Disbursements AS d ON es.disbursement_id = d.id
																	LEFT JOIN DisbursementTypes AS dt ON dt.id = d.parent_id)
															WHERE es.date BETWEEN '".$dateFrom."' AND '".$dateTo."' AND es.invoiced = '0'
																$where");

                                        if ($areaCount > 0)	$expensesHasAreas = 1;
										if (count($projectTypes) > 1)	$expensesHasSP = 1;
										else if ($projectTypes[0][0] == "SP")	$expensesHasSP = 1;
									}
								}
							}

							$hasAreas = ($timeHasAreas || $expensesHasAreas) ? 1 : 0;
							$isSP = ($timeHasSP || $expensesHasSP) ? 1 : 0;

                            if (is_array($types))       {
                                foreach ($types as $type)       {
									$select = "";
									$where = "";

									$prefix = ($type == "Time") ? "ts" : "es";

									$where .= ($level == "SP") ? "AND p.p_type = 'SP' AND pu.spm_manager = '1' " : "";
									$where .= ($level == "SP" && $company != "null") ? "AND c.id = '".$company."' " : "";
									$where .= ($level == "CP") ? "AND c.id = '".$_SESSION["company_id"]."' " : "";

									if ($entryStatus == "unapproved")		$where .= "AND (CASE WHEN p.p_type = 'CP'
																								THEN (".$prefix.".status IN ('0','1'))
																								ELSE (".$prefix.".status IN ('0','1') OR ".$prefix.".status2 IN ('0','1'))
																							END) ";
									else if ($entryStatus == "approved")	$where .= "AND (CASE WHEN p.p_type = 'CP'
																								THEN (".$prefix.".status IN ('2'))
																								ELSE (".$prefix.".status IN ('2') OR ".$prefix.".status2 IN ('2'))
																							END) ";
									else if ($entryStatus == "declined")	$where .= "AND (CASE WHEN p.p_type = 'CP'
																								THEN (".$prefix.".status IN ('3'))
																								ELSE (".$prefix.".status IN ('3') OR ".$prefix.".status2 IN ('3'))
																							END) ";

									if ($projectType == "invoiceable")			$where .= " AND cot.total_budget > 0 AND cot.total_budget != '' AND p.due_date != ''";
									if ($projectType == "non-invoiceable")	$where .= " AND (cot.total_budget = '' OR p.due_date = '')";

									$where .= ($mainProjectType != "null") ? "AND p.type_id = '".$mainProjectType."' " : "";
									$where .= ($project != "null") ? "AND p.id = '".$project."' " : "";
									$where .= ($employee != "null") ? "AND e.id = '".$employee."' " : "";
									$where .= "AND p.completed = '0' ";
									$where .= "AND p.deleted = '0' ";

									if ($type == "Time")	{
                                        if ($hasAreas)	{
                                            if ($isSP == 1)	{
                                                $select .= "c.name,p.id,CONCAT(e.lstname, ', ', e.frstname),ar.id,at.id,a.id,ts.descr,ts.status,ts.status2,ts.date,ts.time_spent,ts.rate,ts.time_spent * ts.rate,1,1";
                                                $headings[$type] = array("ID","Company Name","Project Name","Employee Name","Area","Activity Type","Activity","Description","PM Status","SPM Status","Date","Time Spent",
                                                                            "Rate","Cost <i>(".$_SESSION["currency"].")</i>","Approve","Decline");
                                            } else	{
                                                $select .= "c.name,p.id,CONCAT(e.lstname, ', ', e.frstname),ar.id,at.id,a.id,ts.descr,ts.status,ts.date,ts.time_spent,ts.rate,ts.time_spent * ts.rate,1,1";
                                                $headings[$type] = array("ID","Company Name","Project Name","Employee Name","Area","Activity Type","Activity","Description","PM Status","Date","Time Spent",
                                                                            "Rate","Cost <i>(".$_SESSION["currency"].")</i>","Approve","Decline");
                                            }
                                        } else	{
                                            if ($isSP == 1)	{
                                                $select .= "c.name,p.id,CONCAT(e.lstname, ', ', e.frstname),at.id,a.id,ts.descr,ts.status,ts.status2,ts.date,ts.time_spent,ts.rate,ts.time_spent * ts.rate,1,1";
                                                $headings[$type] = array("ID","Company Name","Project Name","Employee Name","Activity Type","Activity","Description","PM Status","SPM Status","Date","Time Spent",
                                                                            "Rate","Cost <i>(".$_SESSION["currency"].")</i>","Approve","Decline");
                                            } else	{
                                                $select .= "c.name,p.id,CONCAT(e.lstname, ', ', e.frstname),at.id,a.id,ts.descr,ts.status,ts.date,ts.time_spent,ts.rate,ts.time_spent * ts.rate,1,1";
                                                $headings[$type] = array("ID","Company Name","Project Name","Employee Name","Activity Type","Activity","Description","PM Status","Date","Time Spent",
                                                                            "Rate","Cost <i>(".$_SESSION["currency"].")</i>","Approve","Decline");
                                            }
                                        }

                                        $timeData = q("SELECT ts.id,$select
														FROM (ApprovedTime AS ts
															INNER JOIN Employee AS e ON ts.user_id = e.id
															INNER JOIN Activities AS a ON ts.activity_id = a.id
															INNER JOIN ActivityTypes AS at ON a.parent_id = at.id
															INNER JOIN Project AS p ON ts.project_id = p.id
															INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
															INNER JOIN Company AS c ON cot.company_id = c.id
															LEFT JOIN areas AS ar ON ts.area_id = ar.id )
														WHERE ts.date BETWEEN '".$dateFrom."' AND '".$dateTo."' AND ts.invoiced = '0'
															$where
                                                        ORDER BY c.name, p.name, ts.date, ts.on_date, ts.on_time, ar.name");
									} else	{
                                        if ($hasAreas)	{
                                            if ($isSP == 1)	{
                                                $select .= "c.name,p.id,CONCAT(e.lstname, ', ', e.frstname),dd.name,IF(dd.name = 'Driving',v.type,IF(dd.name = 'Printing',CONCAT(dt.type,': ',d.name),es.otherDescr)),ar.id,at.id,".
                                                            "a.id,es.descr,es.status,es.status2,es.date,IF(dd.name = 'Driving',es.kilometers,es.total), es.rate, es.expense,1,1";
                                                $headings[$type] = array("ID","Company Name","Project Name","Employee Name","Expense Type","Expense Info","Area","Activity Type","Activity","Description","PM Status","SPM Status",
																			"Date","Amount","Rate","Cost <i>(".$_SESSION["currency"].")</i>","Approve","Decline");
                                            } else	{
                                                $select .= "c.name,p.id,CONCAT(e.lstname, ', ', e.frstname),dd.name,IF(dd.name = 'Driving',v.type,IF(dd.name = 'Printing',CONCAT(dt.type,': ',d.name),es.otherDescr)),ar.id,at.id,".
                                                            "a.id,es.descr,es.status,es.date,IF(dd.name = 'Driving',es.kilometers,es.total), es.rate, es.expense,1,1";
                                                $headings[$type] = array("ID","Company Name","Project Name","Employee Name","Expense Type","Expense Info","Area","Activity Type","Activity","Description","PM Status","Date","Amount",
                                                                            "Rate","Cost <i>(".$_SESSION["currency"].")</i>","Approve","Decline");
                                            }
                                        } else	{
                                            if ($isSP == 1)	{
                                                $select .= "c.name,p.id,CONCAT(e.lstname, ', ', e.frstname),dd.name,IF(dd.name = 'Driving',v.type,IF(dd.name = 'Printing',CONCAT(dt.type,': ',d.name),es.otherDescr)),at.id,".
                                                            "a.id,es.descr,es.status,es.status2,es.date,IF(dd.name = 'Driving',es.kilometers,es.total), es.rate, es.expense,1,1";
                                                $headings[$type] = array("ID","Company Name","Project Name","Employee Name","Expense Type","Expense Info","Activity Type","Activity","Description","PM Status","SPM Status","Date",
																			"Amount","Rate","Cost <i>(".$_SESSION["currency"].")</i>","Approve","Decline");
                                            } else	{
                                                $select .= "c.name,p.id,CONCAT(e.lstname, ', ', e.frstname),dd.name,IF(dd.name = 'Driving',v.type,IF(dd.name = 'Printing',CONCAT(dt.type,': ',d.name),es.otherDescr)),at.id,".
                                                            "a.id,es.descr,es.status,es.date,IF(dd.name = 'Driving',es.kilometers,es.total), es.rate, es.expense,1,1";
                                                $headings[$type] = array("ID","Company Name","Project Name","Employee Name","Expense Type","Expense Info","Activity Type","Activity","Description","PM Status","Date","Amount",
                                                                            "Rate","Cost <i>(".$_SESSION["currency"].")</i>","Approve","Decline");
                                            }
                                        }

                                        $expenseData = q("SELECT es.id,$select
                                                            FROM (ApprovedExpense AS es
                                                                INNER JOIN Employee AS e ON es.user_id = e.id
                                                                INNER JOIN dropdowns AS dd ON es.expense_type_id = dd.id
                                                                LEFT JOIN Activities AS a ON es.activity_id = a.id
                                                                LEFT JOIN ActivityTypes AS at ON a.parent_id = at.id
                                                                INNER JOIN Project AS p ON es.project_id = p.id
                                                                INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
                                                                INNER JOIN Company AS c ON cot.company_id = c.id
                                                                LEFT JOIN areas AS ar ON es.area_id = ar.id
                                                                LEFT JOIN Vehicle AS v ON es.vehicle_id = v.id
                                                                LEFT JOIN Disbursements AS d ON es.disbursement_id = d.id
                                                                LEFT JOIN DisbursementTypes AS dt ON dt.id = d.parent_id)
                                                            WHERE es.date BETWEEN '".$dateFrom."' AND '".$dateTo."' AND es.invoiced = '0'
																$where
                                                            ORDER BY c.name, es.date, es.on_date, es.on_time, ar.name");
									}
								}
							}

                            if (is_array($timeData) || is_array($expenseData))	{
                                $status = array("<font class='on-validate-error'>Unapproved</font>","Updated","Approved","Declined");

								$projects = q("SELECT DISTINCT(p.id), p.name
												FROM (Project AS p
													INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id)
												WHERE cot.company_id = '".$_SESSION["company_id"]."'
													AND p.completed = '0'
													AND p.deleted = '0'
												ORDER BY UPPER(p.name)");

                                $timeColumns = count($headings["Time"]) - 2;
                                $expenseColumns = count($headings["Expenses"]) - 2;

                                $columns = (!is_array($expenseData)) ? $timeColumns : $expenseColumns;

                                if (is_array($types))   {
                                    echo "<div class='on-20px'>";
                                        echo "<table class='on-table on-table-center'>";

                                        foreach ($types as $type)	{
                                            $displayString = "";

                                            $data = ($type == "Time") ? $timeData : $expenseData;

                                            if (is_array($data))	{
												$dataStatus = ($entryStatus == "unapproved") ? "Unapproved/Updated" : (($entryStatus == "approved") ? "Approved" : "Declined");

												$displayString .= "<tr>";
													$displayString .= "<td style='text-align:center' class='on-table-clear' colspan='100%'>";
														 $displayString .= "<h6>".$dataStatus." ".$type."</h6>";
													$displayString .= "</td>";
												$displayString .= "</tr>";

												//  Headings
												$displayString .= "<tr>";

												foreach ($headings[$type] as $h)	{
													if ($h != "ID")	{
														$colspan = 1;

														if ($type == "Time")	$colspan = ($h == "Employee Name") ? 3 : 1;

														if ($h == "Approve")	{
															$checked = ($entryStatus == "approved") ? "checked" : "";

															$displayString .= "<th colspan='".$colspan."' style='white-space:nowrap'>".$h.
																					"&nbsp;<input id='".(($type == "Time") ? "J" : "U")."' name='".(($type == "Time") ? "J" : "U")."' class='selectAll' type='checkbox' value='1' ".
																						$checked." />".
																				"</th>";
														} else if ($h == "Decline")	{
															$checked = ($entryStatus == "declined") ? "checked" : "";

															$displayString .= "<th colspan='".$colspan."' style='white-space:nowrap'>".$h.
																					"&nbsp;<input id='".(($type == "Time") ? "K" : "V")."' name='".(($type == "Time") ? "K" : "V")."' class='selectAll' type='checkbox' value='1' ".
																						$checked." />".
																				"</th>";
														} else
															$displayString .= "<th colspan='".$colspan."' style='white-space:nowrap'>".$h."</th>";
													}
												}

												$displayString .= "</tr>";

												//  Data
												$counter = 1;

												if ($hasAreas)	$fields = ($type == "Time") ? array("A","B","C","D","E","F","G","H","I","J","K") : array("L","M","N","O","P","Q","R","S","T","U","V");
												else			$fields = ($type == "Time") ? array("A","B","D","E","F","G","H","I","J","K") : array("L","M","O","P","Q","R","S","T","U","V");

												if ($type == "Time")	$totalTime = 0;

												$totalCost = 0;

												// - BUDGET: Track Individual Project Budget for adding an over-budget indicator per line - //
												$all_projects = array("project_id" => array(), "project_budget" => array(), "invoiceable" => array());
												$budget_column = ($type == "Time") ? "consulting" : "expenses";
												foreach($data as $d) {
													if(!in_array($d[2], $all_projects["project_id"])) {
														$all_projects["project_id"][] = $d[2];

														$project_name = q("SELECT name FROM Project WHERE id = '".$d[2]."'");
														$project_due_date = q("SELECT due_date FROM Project WHERE id = '".$d[2]."'");
														$project_budget = q("SELECT total_budget FROM companiesOnTeam WHERE project_id='".$d[2]."' AND company_id='".$_SESSION['company_id']."'");

														$invoiceable = "";

														if ($project_due_date != "" && $project_budget > 0 && $project_budget != "")	$invoiceable = "YES";
														else if ($project_due_date == "" || $project_budget == "")								$invoiceable = "NO";

														$all_projects["invoiceable"][$d[2]] = $invoiceable;

														$all_projects["project_budget"][$d[2]] = q("SELECT ".$budget_column." FROM companiesOnTeam WHERE project_id='".$d[2]."' AND company_id='".$_SESSION['company_id']."' AND ".$budget_column." > 0 AND ".$budget_column." IS NOT NULL");

														if ($invoiceable == "YES")	{
															//  GET All Approved
															$approved = ($type == "Time") ? q("SELECT SUM(rate * time_spent) FROM ApprovedTime WHERE (CASE WHEN projectType = 'CP' THEN (status IN ('2')) ELSE (status2 IN ('2')) END)
																													AND project_id = '".$d[2]."'")
																											: q("SELECT SUM(expense) FROM ApprovedExpense WHERE (CASE WHEN projectType = 'CP' THEN (status IN ('2')) ELSE (status2 IN ('2')) END)
																													AND project_id = '".$d[2]."'") ;
															$all_projects["project_budget"][$d[2]] -= $approved;
														}
													}
												}

												foreach ($data as $r) {
													$fieldCount = 0;
													$col = 0;

													$table = ($type == "Time") ? "ApprovedTime" : "ApprovedExpense";
													$isSPM = q("SELECT spm_manager
																FROM Project_User
																WHERE user_id = (SELECT user_id FROM $table WHERE id = '".$r[array_search("ID",$headings[$type])]."')
																	AND project_id = (SELECT project_id FROM $table WHERE id = '".$r[array_search("ID",$headings[$type])]."')
																	AND company_id = (SELECT company_id FROM $table WHERE id = '".$r[array_search("ID",$headings[$type])]."')");
													$projectType = q("SELECT p_type FROM Project WHERE id = '".$r[array_search("Project Name",$headings[$type])]."'");

													$displayString .= "<tr>";
														foreach ($r as $d)	{
															$class = "";
															$readonly = "";

															$colspan = ($type == "Time" && $headings[$type][$col] == "Employee Name") ? 3 : 1;

															if ($headings[$type][$col] == "Time Spent")                                    $totalTime += $d;
															if ($headings[$type][$col] == "Cost <i>(".$_SESSION["currency"].")</i>")       $totalCost += $d;

															//  Field Class(es)
															if ($headings[$type][$col] == "Project Name")	$class = "project";
															else if ($headings[$type][$col] == "Activity Type")	$class = "activityType";
															else if ($headings[$type][$col] == "Time Spent")	$class = "recalcTime";
															else if ($type == "Time" && $headings[$type][$col] == "Rate")	$class = "recalcTime";
															else if ($type == "Time" && $headings[$type][$col] == "Cost <i>(".$_SESSION["currency"].")</i>")	$class = "recalcTime";
															else if ($headings[$type][$col] == "Amount")	$class = "recalcExpense";
															else if ($type == "Expenses" && $headings[$type][$col] == "Rate")	$class = "recalcExpense";
															else if ($type == "Expenses" && $headings[$type][$col] == "Cost <i>(".$_SESSION["currency"].")</i>")	$class = "recalcExpense";
															else if ($headings[$type][$col] == "Approve")	$class = "deselect_decline";
															else if ($headings[$type][$col] == "Decline")	$class = "deselect_approve";

															//  Field Read-Only
															if ($headings[$type][$col] == "Rate")	$readonly = ($isSP == 1 && $isSPM == 0) ? "readonly" : "";
															else if ($headings[$type][$col] == "Cost <i>(".$_SESSION["currency"].")</i>")	$readonly = "readonly";

															//  ID
															if ($headings[$type][$col] == "ID")
																$displayString .= "<input id='".$fields[$fieldCount].$counter."' name='".$fields[$fieldCount++].$counter."' $readonly class='".$class."' type='hidden' value='".str_replace("'", "&#39;" , $d)."' />";
															//  PROJECT NAME
															else if ($headings[$type][$col] == "Project Name")	{
																$displayString .= "<td style='white-space:nowrap'>";
																	$displayString .= "<select id='".$fields[$fieldCount].$counter."' name='".$fields[$fieldCount++].$counter."' $readonly class='".$class."'>";

																		if (is_array($projects))	{
																			foreach ($projects as $p)	{
																				$selected = ($d == $p[0]) ? "selected" : "";
																				$displayString .= "<option value='".$p[0]."' ".$selected.">".$p[1]."</option>";
																			}
																		}

																	$displayString .= "</select>";
																$displayString .= "</td>";
															}
															//  PROJECT AREA(S)
															else if ($headings[$type][$col] == "Area")	{
																$displayString .= "<td style='white-space:nowrap'>";
																	$areas = q("SELECT id,name FROM areas WHERE pID = '".$r[array_search("Project Name",$headings[$type])]."' AND active = '1' ORDER BY name");

																	if (is_array($areas))	{
																		$displayString .= "<select id='".$fields[$fieldCount].$counter."' name='".$fields[$fieldCount].$counter."' $readonly class='".$class."'>";

																		foreach ($areas as $a)	{
																			$selected = ($d == $a[0]) ? "selected" : "";
																			$displayString .= "<option value='".$a[0]."' ".$selected.">".$a[1]."</option>";
																		}

																		$displayString .= "</select>";
																	} else {
																		$displayString .= "<input id='".$fields[$fieldCount].$counter."' name='".$fields[$fieldCount].$counter."' $readonly class='".$class."' type='hidden' value='".str_replace("'", "&#39;" , $d)."' />";
																		$displayString .= "-";
																	}

																	$fieldCount++;

																$displayString .= "</td>";
															}
															//  ACTIVITY TYPE(S)
															else if ($headings[$type][$col] == "Activity Type")	{
																$displayString .= "<td style='white-space:nowrap'>";
																	$projectType = q("SELECT p_type FROM Project WHERE id = '".$r[array_search("Project Name", $headings[$type])]."'");
																	$activityTypes = ($projectType == "CP") ? q("SELECT id,type FROM ActivityTypes WHERE company_id = '".$_SESSION["company_id"]."' AND active = '1' ORDER BY type")
																						: q("SELECT id,type FROM ActivityTypes WHERE pID = '".$r[array_search("Project Name",$headings[$type])]."' AND active = '1' ORDER BY type");

																	if (is_array($activityTypes))	{
																		$displayString .= "<select id='".$fields[$fieldCount].$counter."' name='".$fields[$fieldCount].$counter."' $readonly class='".$class."'>";

																		foreach ($activityTypes as $actT)	{
																			$selected = ($d == $actT[0]) ? "selected" : "";
																			$displayString .= "<option value='".$actT[0]."' ".$selected.">".$actT[1]."</option>";
																		}

																		$displayString .= "</select>";
																	} else {
																		$d = ($d != "") ? $d : 0;

																		$displayString .= "<input id='".$fields[$fieldCount].$counter."' name='".$fields[$fieldCount].$counter."' $readonly $onChange type='hidden' value='".str_replace("'", "&#39;" , $d)."' />";
																		$displayString .= "-";
																	}

																	$fieldCount++;

																$displayString .= "</td>";
															}
															//  ACTIVITY/ACTIVITIES
															else if ($headings[$type][$col] == "Activity")	{
																$displayString .= "<td style='white-space:nowrap'>";
																	$activities = q("SELECT id,name FROM Activities WHERE parent_id = '".$r[array_search("Activity Type", $headings[$type])]."' AND active = '1' ORDER BY name");

																	if (is_array($activities))	{
																		$displayString .= "<select id='".$fields[$fieldCount].$counter."' name='".$fields[$fieldCount].$counter."' $readonly class='".$class."'>";

																		foreach ($activities as $act)	{
																			$selected = ($d == $act[0]) ? "selected" : "";
																			$displayString .= "<option value='".$act[0]."' ".$selected.">".$act[1]."</option>";
																		}

																		$displayString .= "</select>";
																	} else {
																		$displayString .= "<input id='".$fields[$fieldCount].$counter."' name='".$fields[$fieldCount].$counter."' $readonly type='hidden' value='".str_replace("'", "&#39;" , $d)."' />";
																		$displayString .= "-";
																	}

																	$fieldCount++;

																$displayString .= "</td>";
															}
															//  DESCRIPTION
															else if ($headings[$type][$col] == "Description")	{
																$displayString .= "<td style='white-space:nowrap'>";
																	$displayString .= "<input id='".$fields[$fieldCount].$counter."' name='".$fields[$fieldCount++].$counter."' $readonly type='text' class='descr ".$class."' value='".str_replace("'", "&#39;" , $d)."' />";
																$displayString .= "</td>";
															}
															//  TIME SPENT
															else if ($headings[$type][$col] == "Time Spent")	{
																$displayString .= "<td style='white-space:nowrap'>";
																	$displayString .= "<select id='".$fields[$fieldCount].$counter."' name='".$fields[$fieldCount++].$counter."' $readonly class='".$class."'>";
																		$displayString .= "<option value='0.25' ".(($d == "0.25") ? "selected" : "").">0.25</option>";
																		$displayString .= "<option value='0.50' ".(($d == "0.50") ? "selected" : "").">0.50</option>";
																		$displayString .= "<option value='0.75' ".(($d == "0.75") ? "selected" : "").">0.75</option>";

																		for ($i = 1; $i <= 12; $i += 0.5)
																			$displayString .= "<option value='".number_format($i, 2)."' ".(($d == number_format($i, 2)) ? "selected" : "").">".number_format($i, 2, ".", "")."</option>";

																	$displayString .= "</select>";
																$displayString .= "</td>";
															}
															//  RATE | AMOUNT | COST
															else if ($headings[$type][$col] == "Rate" ||
																		$headings[$type][$col] == "Amount" ||
																		$headings[$type][$col] == "Cost <i>(".$_SESSION["currency"].")</i>") {

																// - Budget check - //
																$over_budget_css = "";
																if($headings[$type][$col] == "Cost <i>(".$_SESSION["currency"].")</i>") {
																	if ($all_projects["invoiceable"][$r[2]] == "YES")	{
																		$all_projects["project_budget"][$r[2]] -= $d;	// Subtract value from budget
																		if($all_projects["project_budget"][$r[2]] <= 0) {
																			$over_budget_css = "background:#cf6363; border-color:#a65050;";
																		}
																	}
																}
																////////////////////

																$displayString .= "<td style='white-space:nowrap;'>";
																	$displayString .= "<input style='".$over_budget_css."' id='".$fields[$fieldCount].$counter."' name='".$fields[$fieldCount++].$counter."' $readonly type='text' class='number ".$class."' value='".(($d != "") ? number_format((double)$d, 2, ".", "") : "-")."' title='".$_SESSION["currency"].number_format($all_projects["project_budget"][$r[2]], 2, ".", "")." remaining' />";
																$displayString .= "</td>";
															}
															//  ENTRY STATUS
															else if ($headings[$type][$col] == "PM Status" || $headings[$type][$col] == "SPM Status")	{
																if ($projectType == "CP" && $headings[$type][$col] == "SPM Status")	$displayString .= "<td colspan='".$colspan."' style='white-space:nowrap'>-</td>";
																else																$displayString .= "<td colspan='".$colspan."' style='white-space:nowrap'>".$status[$d]."</td>";
															}
															//  APPROVED/DECLINED
															else if ($headings[$type][$col] == "Approve" || $headings[$type][$col] == "Decline")	{
																$checked = "";

																if ($projectType == "CP")	{
																	if ($headings[$type][$col] == "Approve" && $status[$r[array_search("PM Status", $headings[$type])]] == "Approved")		$checked = "checked";
																	else if ($headings[$type][$col] == "Decline" && $status[$r[array_search("PM Status", $headings[$type])]] == "Declined")	$checked = "checked";
																} else if ($projectType == "SP")	{
																	if ($headings[$type][$col] == "Approve" && $status[$r[array_search("SPM Status", $headings[$type])]] == "Approved")			$checked = "checked";
																	else if ($headings[$type][$col] == "Decline" && $status[$r[array_search("SPM Status", $headings[$type])]] == "Declined")	$checked = "checked";
																}

																$displayString .= "<td class='centerdata' style='white-space:nowrap'>";
																	$displayString .= "<input id='".$fields[$fieldCount].$counter."_Prev' name='".$fields[$fieldCount].$counter."_Prev' $readonly class='".$class."' type='hidden' value='".(($checked == "checked") ? "1" : "0")."' />";
																	$displayString .= "<input id='".$fields[$fieldCount].$counter."' name='".$fields[$fieldCount++].$counter."' $readonly $checked class='".$class."' type='checkbox' value='1' />";
																$displayString .= "</td>";
															}
															//  OTHER FIELDS
															else	{
																if (preg_match("/^((\d{2}(([02468][048])|([13579][26]))[\-\/\s]?((((0?[13578])|(1[02]))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-\/\s]?((0?[1-9])|([1-2][0-9])))))|(\d{2}(([02468][1235679])|([13579][01345789]))[\-\/\s]?((((0?[13578])|(1[02]))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-\/\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))(\s(((0?[1-9])|(1[0-2]))\:([0-5][0-9])((\s)|(\:([0-5][0-9])\s))([AM|PM|am|pm]{2,2})))?$/", $d))
																	$displayString .= "<td colspan='".$colspan."' style='white-space:nowrap'>".$d."</td>";
																else if (is_numeric($d))
																	$displayString .= "<td colspan='".$colspan."' style='white-space:nowrap' class='rightdata'>".number_format((double)$d, 2, ".", "")."</td>";
																else if ($d == "")
																	$displayString .= "<td colspan='".$colspan."' style='white-space:nowrap'>-</td>";
																else
																	$displayString .= "<td colspan='".$colspan."' style='white-space:nowrap'>".$d."</td>";
															}

															$col++;
														}
													$displayString .= "</tr>";

													$counter++;
												}

												$displayString .= "<tr>";
													$colspan = ($type == "Time") ? array_search("Time Spent", $headings[$type]) + 2 : array_search("Cost <i>(".$_SESSION["currency"].")</i>", $headings[$type]);

													$displayString .= "<td class='on-table-total' colspan='".($colspan - 1)."'>Total:</td>";

													if ($type == "Time")    {
														$displayString .= "<td class='on-table-total'>";
															$displayString .= "<input id='totalTime' name='totalTime' class='number' readonly type='text' value='".number_format((double)$totalTime, 2, ".", "")."'>";
														$displayString .= "</td>";
														$displayString .= "<td class='on-table-total'></td>";
														$displayString .= "<td class='on-table-total'>";
															$displayString .= "<input id='totalCost' name='totalCost' class='number' readonly type='text' value='".number_format((double)$totalCost, 2, ".", "")."'>";
														$displayString .= "</td>";
													} else  {
														$displayString .= "<td class='on-table-total'>";
															$displayString .= "<input id='expenseTotal' name='expenseTotal' class='number' readonly type='text' value='".number_format((double)$totalCost, 2, ".", "")."'>";
														$displayString .= "</td>";
													}

													$displayString .= "<td class='on-table-total' colspan='2'></td>";
												$displayString .= "</tr>";
												$displayString .= "<input id='".$type."' name='".$type."' type='hidden' value='".$counter."' />";
                                            }

                                            echo $displayString;

                                            if ($type == "Time")        echo "<tr><td style='background-color:transparent' colspan='100%'>&nbsp;</td></tr>";
                                        }

                                            echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                                        echo "</table>";
                                    echo "</div><br/>";
                                }
                                ?>
                            <input id="btnUpdate" name="btnUpdate" type="button" value="Update Data">
                            <input id="hasAreas" name="hasAreas" type="hidden" value="<?php echo $hasAreas; ?>" />
                            <input id="update" name="update" type="hidden" value="0" />
                            <br/><br/><br/>
                            <?php
                            }
						}
						?>
					</form>
                </td>
            </tr>
        </table>
    <?php
    }

    //  Get Approved Time Information - Report
    function getReport()    {
        getJQeury();
	?>
        <script language="JavaScript">
            jQuery(function()        {
				jQuery("#btnDisplay").click(function()	{
					var valid = true;
					var test;
					var dateType = jQuery("#dateType").val();

					if (dateType == "custom")	{
						test = validation("date", jQuery("#dateFrom").val());

						if (!test)  jQuery("#dateFrom").show();
						else        jQuery("#dateFrom").hide();

						valid &= test;

						test = validation("date", jQuery("#dateTo").val());

						if (!test)  jQuery("#dateTo").show();
						else        jQuery("#dateTo").hide();

						valid &= test;
					}

					if (valid)	{
						jQuery("#display").val("1");
						jQuery("#approvals").submit();
					}
				});
			});
		</script>
	<?php
		$today = date("Y-m-d");
		$level = $_GET["level"];

		$entryStatus = "unapproved";
		$projectType = "invoiceable";
		$mainProjectType = "null";
		$project = "null";
		$company = "null";
		$employee = "null";
		$dateType = "currentmnth";

		if (isset($_POST["entryStatus"]) && $_POST["entryStatus"] != "null")			$entryStatus = $_POST["entryStatus"];
		if (isset($_POST["projectType"]) && $_POST["projectType"] != "null")			$projectType = $_POST["projectType"];
		if (isset($_POST["mainProjectType"]) && $_POST["mainProjectType"] != "null")	$mainProjectType = $_POST["mainProjectType"];
		if (isset($_POST["project"]) && $_POST["project"] != "null")					$project = $_POST["project"];
		if (isset($_POST["company"]) && $_POST["company"] != "null")					$company = $_POST["company"];
		if (isset($_POST["employee"]) && $_POST["employee"] != "null")					$employee = $_POST["employee"];
		if (isset($_POST["dateType"]) && $_POST["dateType"] != "null")					$dateType = $_POST["dateType"];

		if ($dateType == "currentwk")      {
			$dateFrom = currentWeekStart();
			$dateTo = $today;
		}
		else if ($dateType == "previouswk")        {
			$dateFrom = getDates(currentWeekStart(), -7);
			$dateTo = getDates(currentWeekStart(), -1);
		}
		else if ($dateType == "currentmnth")       {
			$dateFrom = getMonthStart($today);
			$dateTo = $today;
		}
		else if ($dateType == "previousmnth")      {
			$dateFrom = getPreviousMonthStart();
			$dateTo = getMonthEnd(getPreviousMonthStart());
		}
		else if ($dateType == "projectLifetime")      {
			$dateFromT = q("SELECT MIN(date) FROM ApprovedTime WHERE project_id = '".$project."'");
			$dateFromE = q("SELECT MIN(date) FROM ApprovedExpense WHERE project_id = '".$project."'");

			if ($dateFromE == null)	$dateFromE = $dateFromT;

			$dateFrom = ($dateFromT <= $dateFromE) ? $dateFromT : $dateFromE;
			$dateTo = $today;
		}
		else if ($dateType == "custom")    {
			$dateFrom = addslashes(strip_tags($_POST["dateFrom"]));
			$dateTo = addslashes(strip_tags($_POST["dateTo"]));
		}

		$n[0][0] = "0"; $n[0][1] = "Shared Projects";
		$mainProjectTypes = q("SELECT id, type FROM ProjectTypes WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY type");

		if (!is_array($mainProjectTypes))	$mainProjectTypes = $n;
		else								$mainProjectTypes = array_merge($n,$mainProjectTypes);

		$mainProjectTypes = sortArrayByColumn($mainProjectTypes, 1);

		$where = "";

		if ($level == "SP")	$where .= "AND p.p_type = 'SP' AND pu.spm_manager = '1' ";

		if ($entryStatus == "unapproved")		$where .= "AND (CASE WHEN p.p_type = 'CP'
																	THEN (ts.status IN ('0') OR es.status IN ('0'))
																	ELSE (ts.status IN ('0') OR es.status IN ('0') OR ts.status2 IN ('0') OR es.status2 IN ('0'))
																END) ";
		else if ($entryStatus == "updated")		$where .= "AND (CASE WHEN p.p_type = 'CP'
																	THEN (ts.status IN ('1') OR es.status IN ('1'))
																	ELSE (ts.status IN ('1') OR es.status IN ('1') OR ts.status2 IN ('1') OR es.status2 IN ('1'))
																END) ";
		else if ($entryStatus == "approved")	$where .= "AND (CASE WHEN p.p_type = 'CP'
																	THEN (ts.status IN ('2') OR es.status IN ('2'))
																	ELSE (ts.status IN ('2') OR es.status IN ('2') OR ts.status2 IN ('2') OR es.status2 IN ('2'))
																END) ";
		else if ($entryStatus == "declined")	$where .= "AND (CASE WHEN p.p_type = 'CP'
																	THEN (ts.status IN ('3') OR es.status IN ('3'))
																	ELSE (ts.status IN ('3') OR es.status IN ('3') OR ts.status2 IN ('3') OR es.status2 IN ('3'))
																END) ";

		if ($projectType == "invoiceable")			$where .= " AND cot.total_budget > 0 AND cot.total_budget != '' AND p.due_date != ''";
		if ($projectType == "non-invoiceable")	$where .= " AND (cot.total_budget = '' OR p.due_date = '')";

		$where .= ($mainProjectType != "null") ? "AND p.type_id = '".$mainProjectType."' " : "";

		$projects = q("SELECT DISTINCT(p.id), p.name
						FROM (Project AS p
							INNER JOIN Project_User AS pu ON pu.project_id = p.id
							INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
							LEFT JOIN ApprovedTime AS ts ON ts.project_id = p.id
							LEFT JOIN ApprovedExpense AS es ON es.project_id = p.id)
						WHERE pu.user_id = '".$_SESSION["user_id"]."'
							AND cot.company_id = '".$_SESSION["company_id"]."'
							AND pu.company_id = '".$_SESSION["company_id"]."'
							AND p.completed = '0'
							AND p.deleted = '0'
							AND (ts.date BETWEEN '".$dateFrom."' AND '".$dateTo."' OR es.date BETWEEN '".$dateFrom."' AND '".$dateTo."')
							$where
						ORDER BY UPPER(p.name)");

		if ($level == "SP" && $project != "null")	{
			$companies = q("SELECT DISTINCT(c.id), c.name
							FROM (Company AS c INNER JOIN companiesOnTeam AS cot ON c.id = cot.company_id
								INNER JOIN Project AS p ON p.id =  cot.project_id)
							WHERE cot.project_id = '".$project."'
							ORDER BY c.name");
		}

		$employees = q("SELECT e.id, e.lstname, e.frstname
						FROM (Employee AS e
							INNER JOIN Company_Users AS cu ON e.id = cu.user_id)
						WHERE cu.company_id = '".$_SESSION["company_id"]."' AND e.email != 'admin'
						ORDER BY e.lstname, frstname");

        ?>
        <table width="100%">
            <tr>
                <td class="centerdata">
                    <form id="approvals" name="approvals" action="" method="post">
						<table width="100%">
							<tr>
								<td class="centerdata">
									<h6>Approve Time/Expense Sheet(s)</h6>
								</td>
							</tr>
						</table>
						<br/>
						<table width="100%">
							<tr>
								<td class="on-description" width="50%">
									Entry Status:
								</td>
								<td width="50%">
									<select id="entryStatus" name="entryStatus" class="on-field">
										<option value="all" <?php echo ($entryStatus == "all") ? "selected" : ""; ?>>All Entries</option>
										<option value="unapproved" <?php echo ($entryStatus == "unapproved") ? "selected" : ""; ?>>Unapproved Entries</option>
										<option value="updated" <?php echo ($entryStatus == "updated") ? "selected" : ""; ?>>Updated Entries</option>
										<option value="approved" <?php echo ($entryStatus == "approved") ? "selected" : ""; ?>>Approved Entries</option>
										<option value="declined" <?php echo ($entryStatus == "declined") ? "selected" : ""; ?>>Declined Entries</option>
									</select>
								</td>
							</tr>
							<tr>
								<td class="on-description" width="50%">
									Project Type:
								</td>
								<td width="50%">
									<select id="projectType" name="projectType" class="on-field">
										<option value="invoiceable" <?php echo ($entryStatus == "invoiceable") ? "selected" : ""; ?>>Invoiceable</option>
										<option value="non-invoiceable" <?php echo ($entryStatus == "non-invoiceable") ? "selected" : ""; ?>>Non-Invoiceable</option>
									</select>
								</td>
							</tr>
							<tr>
								<td class="on-description" width="50%">
									Project Type:
								</td>
								<td width="50%">
                                    <select id="mainProjectType" name="mainProjectType" class="on-field">
                                        <option value="null">All Project Types</option>
                                        <?php
                                            if (is_array($mainProjectTypes))	{
                                                foreach ($mainProjectTypes as $mPT)	{
                                                    if ($mainProjectType == $mPT[0])	echo "<option value='".$mPT[0]."' selected>".$mPT[1]."</option>";
                                                    else								echo "<option value='".$mPT[0]."'>".$mPT[1]."</option>";
												}
											}
                                        ?>
                                    </select>
								</td>
							</tr>
							<tr>
								<td class="on-description" width="50%">
									Project:
								</td>
								<td width="50%">
									<select id="project" name="project" class="on-field" <?php echo ($level == "SP") ? "onChange=\"loadCompanies();\"" : ""; ?>>
										<option value="null">All Projects</option>
										<?php
                                            if (is_array($projects))	{
                                                foreach ($projects as $p)	{
                                                    if ($project == $p[0])	echo "<option value='".$p[0]."' selected>".$p[1]."</option>";
                                                    else					echo "<option value='".$p[0]."'>".$p[1]."</option>";
												}
											}
										?>
									</select>
								</td>
							</tr>
							<tr class="<?php echo ($level == "CP") ? "hidden" : ""; ?>">
								<td class="on-description" width="50%">
									Company:
								</td>
								<td width="50%">
									<select id="company" name="company" class="on-field">
										<option value="null">All Companies</option>
										<?php
                                            if (is_array($companies))	{
                                                foreach ($companies as $com)	{
                                                    if ($company == $com[0])	echo "<option value='".$com[0]."' selected>".$com[1]."</option>";
                                                    else						echo "<option value='".$com[0]."'>".$com[1]."</option>";
												}
											}
										?>
									</select>
								</td>
							</tr>
							<tr>
								<td class="on-description" width="50%">
									Employee:
								</td>
								<td width="50%">
									<select id="employee" name="employee" class="on-field">
										<option value="null">All Employees</option>
										<?php
                                            if (is_array($employees))	{
                                                foreach ($employees as $emp)	{
                                                    if ($employee == $emp[0])	echo "<option value='".$emp[0]."' selected>".$emp[1].", ".$emp[2]."</option>";
                                                    else						echo "<option value='".$emp[0]."'>".$emp[1].", ".$emp[2]."</option>";
												}
											}
										?>
									</select>
								</td>
							</tr>
							<tr>
								<td class="on-description" width="50%">
									Date Type:
								</td>
								<td width="50%">
									<select id="dateType" name="dateType" class="on-field">
										<option value="currentwk" <?php echo ($dateType == "currentwk") ? "selected" : ""; ?>>Current Week</option>
										<option value="previouswk" <?php echo ($dateType == "previouswk") ? "selected" : ""; ?>>Previous Week</option>
										<option value="currentmnth" <?php echo ($dateType == "currentmnth") ? "selected" : ""; ?>>Current Month</option>
										<option value="previousmnth" <?php echo ($dateType == "previousmnth") ? "selected" : ""; ?>>Previous Month</option>
										<option value="projectLifetime" <?php echo ($dateType == "projectLifetime") ? "selected" : ""; ?> disabled>Project Lifetime</option>
										<option value="custom" <?php echo ($dateType == "custom") ? "selected" : ""; ?>>Custom Dates</option>
									</select>
								</td>
							</tr>
						</table>
						<div id="datesDiv" style="display:<?php echo ($dateType == "custom") ? "block" : "none"; ?>;">
							<table width="100%">
								<tr>
									<td class="on-description" width="50%">
										Date From:
									</td>
									<td  width="50%">
										<input id="dateFrom" name="dateFrom" class="on-field-date date" type="text" style="text-align:right;" value="<?php echo $dateFrom; ?>">
										<div id="dateFromDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo date("Y-m-d"); ?></font></div>
									</td>
								</tr>
								<tr>
									<td class="on-description" width="50%">
										Date To:
									</td>
									<td  width="50%">
										<input id="dateTo" name="dateTo" class="on-field-date date" type="text" style="text-align:right;" value="<?php echo $dateTo; ?>">
										<div id="dateToDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo date("Y-m-d"); ?></font></div>
									</td>
								</tr>
							</table>
						</div>
						<br/>
						<input id="btnDisplay" name="btnDisplay" type="button" value="Display Information">
						<input id="report" name="report" type="hidden" value="1" />
						<input id="level" name="level" type="hidden" value="<?php echo $level; ?>" />
						<input id="display" name="display" type="hidden" value="0" />
						<?php
						if (isset($_POST["display"]) && $_POST["display"] == "1")	{
						?>
							<br/><br/><br/>
						<?php
                            $types = array("Time","Expenses");
							$timeHasAreas = 0;
							$expensesHasAreas = 0;
							$timeHasSP = 0;
							$expensesHasSP = 0;

                            if (is_array($types))       {
                                foreach ($types as $type)       {
									$select = "";
									$where = "";

									$prefix = ($type == "Time") ? "ts" : "es";

									$where .= ($level == "SP") ? "AND p.p_type = 'SP' AND pu.spm_manager = '1' " : "";
									$where .= ($level == "SP" && $company != "null") ? "AND c.id = '".$company."' " : "";
									$where .= ($level == "CP") ? "AND c.id = '".$_SESSION["company_id"]."' " : "";

									if ($entryStatus == "unapproved")		$where .= "AND (CASE WHEN p.p_type = 'CP'
																								THEN (".$prefix.".status IN ('0'))
																								ELSE (".$prefix.".status IN ('0') OR ".$prefix.".status2 IN ('0'))
																							END) ";
									else if ($entryStatus == "updated")		$where .= "AND (CASE WHEN p.p_type = 'CP'
																								THEN (".$prefix.".status IN ('1'))
																								ELSE (".$prefix.".status IN ('1') OR ".$prefix.".status2 IN ('1'))
																							END) ";
									else if ($entryStatus == "approved")	$where .= "AND (CASE WHEN p.p_type = 'CP'
																								THEN (".$prefix.".status IN ('2'))
																								ELSE (".$prefix.".status IN ('2') OR ".$prefix.".status2 IN ('2'))
																							END) ";
									else if ($entryStatus == "declined")	$where .= "AND (CASE WHEN p.p_type = 'CP'
																								THEN (".$prefix.".status IN ('3'))
																								ELSE (".$prefix.".status IN ('3') OR ".$prefix.".status2 IN ('3'))
																							END) ";

									if ($projectType == "invoiceable")			$where .= " AND cot.total_budget > 0 AND cot.total_budget != '' AND p.due_date != ''";
									if ($projectType == "non-invoiceable")	$where .= " AND (cot.total_budget = '' OR p.due_date = '')";

									$where .= ($mainProjectType != "null") ? "AND p.type_id = '".$mainProjectType."' " : "";
									$where .= ($project != "null") ? "AND p.id = '".$project."' " : "";
									$where .= ($employee != "null") ? "AND e.id = '".$employee."' " : "";
									$where .= "AND p.completed = '0' ";
									$where .= "AND p.deleted = '0' ";

									if ($type == "Time")	{
										$areaCount = q("SELECT COUNT(DISTINCT(ar.name))
													FROM (ApprovedTime AS ts
														INNER JOIN Employee AS e ON ts.user_id = e.id
														INNER JOIN Activities AS a ON ts.activity_id = a.id
														INNER JOIN ActivityTypes AS at ON a.parent_id = at.id
														INNER JOIN Project AS p ON ts.project_id = p.id
														INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
														INNER JOIN Company AS c ON cot.company_id = c.id
														LEFT JOIN areas AS ar ON ts.area_id = ar.id )
													WHERE ts.date BETWEEN '".$dateFrom."' AND '".$dateTo."'
														$where");
										$projectTypes = q("SELECT DISTINCT(p.p_type),p.p_type
														FROM (ApprovedTime AS ts
															INNER JOIN Employee AS e ON ts.user_id = e.id
															INNER JOIN Activities AS a ON ts.activity_id = a.id
															INNER JOIN ActivityTypes AS at ON a.parent_id = at.id
															INNER JOIN Project AS p ON ts.project_id = p.id
															INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
															INNER JOIN Company AS c ON cot.company_id = c.id
															LEFT JOIN areas AS ar ON ts.area_id = ar.id )
														WHERE ts.date BETWEEN '".$dateFrom."' AND '".$dateTo."'
															$where");

                                        if ($areaCount > 0)	$timeHasAreas = 1;
										if (count($projectTypes) > 1)	$timeHasSP = 1;
										else if ($projectTypes[0][0] == "SP")	$timeHasSP = 1;
									} else	{
										$areaCount = q("SELECT COUNT(DISTINCT(ar.name))
														FROM (ApprovedExpense AS es
																INNER JOIN Employee AS e ON es.user_id = e.id
																INNER JOIN dropdowns AS dd ON es.expense_type_id = dd.id
																LEFT JOIN Activities AS a ON es.activity_id = a.id
																LEFT JOIN ActivityTypes AS at ON a.parent_id = at.id
																INNER JOIN Project AS p ON es.project_id = p.id
																INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
																INNER JOIN Company AS c ON cot.company_id = c.id
																LEFT JOIN areas AS ar ON es.area_id = ar.id
																LEFT JOIN Vehicle AS v ON es.vehicle_id = v.id
																LEFT JOIN Disbursements AS d ON es.disbursement_id = d.id
																LEFT JOIN DisbursementTypes AS dt ON dt.id = d.parent_id)
														WHERE es.date BETWEEN '".$dateFrom."' AND '".$dateTo."'
														$where");
										$projectTypes = q("SELECT DISTINCT(p.p_type),p.p_type
															FROM (ApprovedExpense AS es
																	INNER JOIN Employee AS e ON es.user_id = e.id
																	INNER JOIN dropdowns AS dd ON es.expense_type_id = dd.id
																	LEFT JOIN Activities AS a ON es.activity_id = a.id
																	LEFT JOIN ActivityTypes AS at ON a.parent_id = at.id
																	INNER JOIN Project AS p ON es.project_id = p.id
																	INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
																	INNER JOIN Company AS c ON cot.company_id = c.id
																	LEFT JOIN areas AS ar ON es.area_id = ar.id
																	LEFT JOIN Vehicle AS v ON es.vehicle_id = v.id
																	LEFT JOIN Disbursements AS d ON es.disbursement_id = d.id
																	LEFT JOIN DisbursementTypes AS dt ON dt.id = d.parent_id)
															WHERE es.date BETWEEN '".$dateFrom."' AND '".$dateTo."'
																$where");

                                        if ($areaCount > 0)	$expensesHasAreas = 1;
										if (count($projectTypes) > 1)	$expensesHasSP = 1;
										else if ($projectTypes[0][0] == "SP")	$expensesHasSP = 1;
									}
								}
							}

							$hasAreas = ($timeHasAreas || $expensesHasAreas) ? 1 : 0;
							$isSP = ($timeHasSP || $expensesHasSP) ? 1 : 0;

                            if (is_array($types))       {
                                foreach ($types as $type)       {
									$select = "";
									$where = "";

									$prefix = ($type == "Time") ? "ts" : "es";

									$where .= ($level == "SP") ? "AND p.p_type = 'SP' AND pu.spm_manager = '1' " : "";
									$where .= ($level == "SP" && $company != "null") ? "AND c.id = '".$company."' " : "";
									$where .= ($level == "CP") ? "AND c.id = '".$_SESSION["company_id"]."' " : "";

									if ($entryStatus == "unapproved")		$where .= "AND (CASE WHEN p.p_type = 'CP'
																								THEN (".$prefix.".status IN ('0'))
																								ELSE (".$prefix.".status IN ('0') OR ".$prefix.".status2 IN ('0'))
																							END) ";
									else if ($entryStatus == "updated")		$where .= "AND (CASE WHEN p.p_type = 'CP'
																								THEN (".$prefix.".status IN ('1'))
																								ELSE (".$prefix.".status IN ('1') OR ".$prefix.".status2 IN ('1'))
																							END) ";
									else if ($entryStatus == "approved")	$where .= "AND (CASE WHEN p.p_type = 'CP'
																								THEN (".$prefix.".status IN ('2'))
																								ELSE (".$prefix.".status IN ('2') OR ".$prefix.".status2 IN ('2'))
																							END) ";
									else if ($entryStatus == "declined")	$where .= "AND (CASE WHEN p.p_type = 'CP'
																								THEN (".$prefix.".status IN ('3'))
																								ELSE (".$prefix.".status IN ('3') OR ".$prefix.".status2 IN ('3'))
																							END) ";

									if ($projectType == "invoiceable")			$where .= " AND cot.total_budget > 0 AND cot.total_budget != '' AND p.due_date != ''";
									if ($projectType == "non-invoiceable")	$where .= " AND (cot.total_budget = '' OR p.due_date = '')";

									$where .= ($mainProjectType != "null") ? "AND p.type_id = '".$mainProjectType."' " : "";
									$where .= ($project != "null") ? "AND p.id = '".$project."' " : "";
									$where .= ($employee != "null") ? "AND e.id = '".$employee."' " : "";
									$where .= "AND p.completed = '0' ";
									$where .= "AND p.deleted = '0' ";

									if ($type == "Time")	{
                                        if ($hasAreas)	{
                                            if ($isSP == 1)	{
                                                $select .= "c.name,p.name,CONCAT(e.lstname, ', ', e.frstname),ar.name,at.type,a.name,ts.descr,ts.status,ts.status2,ts.date,ts.time_spent,ts.rate,ts.time_spent * ts.rate";
                                                $headings[$type] = array("Company Name","Project Name","Employee Name","Area","Activity Type","Activity","Description","PM Status","SPM Status","Date","Time Spent",
                                                                            "Rate","Cost <i>(".$_SESSION["currency"].")</i>");
                                            } else	{
                                                $select .= "c.name,p.name,CONCAT(e.lstname, ', ', e.frstname),ar.name,at.type,a.name,ts.descr,ts.status,ts.date,ts.time_spent,ts.rate,ts.time_spent * ts.rate";
                                                $headings[$type] = array("Company Name","Project Name","Employee Name","Area","Activity Type","Activity","Description","PM Status","Date","Time Spent",
                                                                            "Rate","Cost <i>(".$_SESSION["currency"].")</i>");
                                            }
                                        } else	{
                                            if ($isSP == 1)	{
                                                $select .= "c.name,p.name,CONCAT(e.lstname, ', ', e.frstname),at.type,a.name,ts.descr,ts.status,ts.status2,ts.date,ts.time_spent,ts.rate,ts.time_spent * ts.rate";
                                                $headings[$type] = array("Company Name","Project Name","Employee Name","Activity Type","Activity","Description","PM Status","SPM Status","Date","Time Spent",
                                                                            "Rate","Cost <i>(".$_SESSION["currency"].")</i>");
                                            } else	{
                                                $select .= "c.name,p.name,CONCAT(e.lstname, ', ', e.frstname),at.type,a.name,ts.descr,ts.status,ts.date,ts.time_spent,ts.rate,ts.time_spent * ts.rate";
                                                $headings[$type] = array("Company Name","Project Name","Employee Name","Activity Type","Activity","Description","PM Status","Date","Time Spent",
                                                                            "Rate","Cost <i>(".$_SESSION["currency"].")</i>");
                                            }
                                        }

                                        $timeData = q("SELECT $select
														FROM (ApprovedTime AS ts
															INNER JOIN Employee AS e ON ts.user_id = e.id
															INNER JOIN Activities AS a ON ts.activity_id = a.id
															INNER JOIN ActivityTypes AS at ON a.parent_id = at.id
															INNER JOIN Project AS p ON ts.project_id = p.id
															INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
															INNER JOIN Company AS c ON cot.company_id = c.id
															LEFT JOIN areas AS ar ON ts.area_id = ar.id )
														WHERE ts.date BETWEEN '".$dateFrom."' AND '".$dateTo."'
															$where
                                                        ORDER BY c.name, p.name, ts.date, ts.on_date, ts.on_time, ar.name");
									} else	{
                                        if ($hasAreas)	{
                                            if ($isSP == 1)	{
                                                $select .= "c.name,p.name,CONCAT(e.lstname, ', ', e.frstname),dd.name,IF(dd.name = 'Driving',v.type,IF(dd.name = 'Printing',CONCAT(dt.type,': ',d.name),es.otherDescr)),ar.name,".
															"at.type,a.name,es.descr,es.status,es.status2,es.date,IF(dd.name = 'Driving',es.kilometers,es.total), es.rate, es.expense";
                                                $headings[$type] = array("Company Name","Project Name","Employee Name","Expense Type","Expense Info","Area","Activity Type","Activity","Description","PM Status","SPM Status",
																			"Date","Amount","Rate","Cost <i>(".$_SESSION["currency"].")</i>");
                                            } else	{
                                                $select .= "c.name,p.name,CONCAT(e.lstname, ', ', e.frstname),dd.name,IF(dd.name = 'Driving',v.type,IF(dd.name = 'Printing',CONCAT(dt.type,': ',d.name),es.otherDescr)),ar.name,".
															"at.type,a.name,es.descr,es.status,es.date,IF(dd.name = 'Driving',es.kilometers,es.total), es.rate, es.expense";
                                                $headings[$type] = array("Company Name","Project Name","Employee Name","Expense Type","Expense Info","Area","Activity Type","Activity","Description","PM Status",
																			"Date","Amount","Rate","Cost <i>(".$_SESSION["currency"].")</i>");
                                            }
                                        } else	{
                                            if ($isSP == 1)	{
                                                $select .= "c.name,p.name,CONCAT(e.lstname, ', ', e.frstname),dd.name,IF(dd.name = 'Driving',v.type,IF(dd.name = 'Printing',CONCAT(dt.type,': ',d.name),es.otherDescr)),".
															"at.type,a.name,es.descr,es.status,es.status2,es.date,IF(dd.name = 'Driving',es.kilometers,es.total), es.rate, es.expense";
                                                $headings[$type] = array("Company Name","Project Name","Employee Name","Expense Type","Expense Info","Activity Type","Activity","Description","PM Status","SPM Status",
																			"Date","Amount","Rate","Cost <i>(".$_SESSION["currency"].")</i>");
                                            } else	{
                                                $select .= "c.name,p.name,CONCAT(e.lstname, ', ', e.frstname),dd.name,IF(dd.name = 'Driving',v.type,IF(dd.name = 'Printing',CONCAT(dt.type,': ',d.name),es.otherDescr)),".
															"at.type,a.name,es.descr,es.status,es.date,IF(dd.name = 'Driving',es.kilometers,es.total), es.rate, es.expense";
                                                $headings[$type] = array("Company Name","Project Name","Employee Name","Expense Type","Expense Info","Activity Type","Activity","Description","PM Status",
																			"Date","Amount","Rate","Cost <i>(".$_SESSION["currency"].")</i>");
                                            }
                                        }

                                        $expenseData = q("SELECT $select
                                                            FROM (ApprovedExpense AS es
                                                                INNER JOIN Employee AS e ON es.user_id = e.id
                                                                INNER JOIN dropdowns AS dd ON es.expense_type_id = dd.id
                                                                LEFT JOIN Activities AS a ON es.activity_id = a.id
                                                                LEFT JOIN ActivityTypes AS at ON a.parent_id = at.id
                                                                INNER JOIN Project AS p ON es.project_id = p.id
                                                                INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
                                                                INNER JOIN Company AS c ON cot.company_id = c.id
																LEFT JOIN areas AS ar ON es.area_id = ar.id
                                                                LEFT JOIN Vehicle AS v ON es.vehicle_id = v.id
                                                                LEFT JOIN Disbursements AS d ON es.disbursement_id = d.id
                                                                LEFT JOIN DisbursementTypes AS dt ON dt.id = d.parent_id)
                                                            WHERE es.date BETWEEN '".$dateFrom."' AND '".$dateTo."'
																$where
                                                            ORDER BY c.name, p.name, es.date, es.on_date, es.on_time, ar.name");
									}
								}
							}

                            if (is_array($timeData) || is_array($expenseData))	{
								unset($exceldata);

                                $status = array("<font class='on-validate-error'>Unapproved</font>","Updated","Approved","Declined");

								$projects = q("SELECT DISTINCT(p.id), p.name
												FROM (Project AS p
													INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id)
												WHERE cot.company_id = '".$_SESSION["company_id"]."'
													AND p.completed = '0'
													AND p.deleted = '0'
												ORDER BY UPPER(p.name)");

                                $timeColumns = count($headings["Time"]) - 2;
                                $expenseColumns = count($headings["Expenses"]) - 2;
								$row = 0;

                                $columns = (!is_array($expenseData)) ? $timeColumns : $expenseColumns;

                                if (is_array($types))   {
                                    echo "<div class='on-20px'>";
                                        echo "<table class='on-table on-table-center'>";

                                        foreach ($types as $type)	{
                                            $displayString = "";

                                            $data = ($type == "Time") ? $timeData : $expenseData;

                                            if (is_array($data))	{
												$dataStatus = ($entryStatus == "unapproved") ? "Unapproved"
													: (($entryStatus == "updated") ? "Updated"
													: ((($entryStatus == "approved") ? "Approved"
													: (($entryStatus == "declined") ? "Declined" : "All"))));

												$displayString .= "<tr>";
													$displayString .= "<td style='text-align:center' class='on-table-clear' colspan='100%'>";
														 $displayString .= "<h6>".$dataStatus." ".$type."</h6>";
													$displayString .= "</td>";
												$displayString .= "</tr>";

												$exceldata[$row][] = strip_tags($dataStatus." ".$type);

												for ($a = 1; $a < $columns; $a++)    $exceldata[$row][] = "";

												$row++;

												//  Headings
												$displayString .= "<tr>";

												foreach ($headings[$type] as $h)	{
													$colspan = 1;

													if ($type == "Time")	$colspan = ($h == "Employee Name") ? 3 : 1;

													$displayString .= "<th colspan='".$colspan."' style='white-space:nowrap'>".$h."</th>";
													$exceldata[$row][] = strip_tags($h);

													for ($i = 1; $i < $colspan; $i++)	$exceldata[$row][] = "";
												}

												$displayString .= "</tr>";
												$row++;

												//  Data
												if ($type == "Time")	$totalTime = 0;

												$totalCost = 0;

												foreach ($data as $r)	{
													$fieldCount = 0;
													$col = 0;

													$table = ($type == "Time") ? "ApprovedTime" : "ApprovedExpense";
													$isSPM = q("SELECT spm_manager
																FROM Project_User
																WHERE user_id = (SELECT user_id FROM $table WHERE id = '".$r[array_search("ID",$headings[$type])]."')
																	AND project_id = (SELECT project_id FROM $table WHERE id = '".$r[array_search("ID",$headings[$type])]."')
																	AND company_id = (SELECT company_id FROM $table WHERE id = '".$r[array_search("ID",$headings[$type])]."')");
													$projectType = q("SELECT p_type FROM Project WHERE id = '".$r[array_search("Project Name",$headings[$type])]."'");

													$displayString .= "<tr>";

													foreach ($r as $d)	{
														$colspan = ($type == "Time" && $headings[$type][$col] == "Employee Name") ? 3 : 1;

														if ($headings[$type][$col] == "Time Spent")                                    $totalTime += $d;
														if ($headings[$type][$col] == "Cost <i>(".$_SESSION["currency"].")</i>")       $totalCost += $d;

														//  ENTRY STATUS
														if ($headings[$type][$col] == "PM Status" || $headings[$type][$col] == "SPM Status")	{
															if ($projectType == "CP" && $headings[$type][$col] == "SPM Status")	{
																$displayString .= "<td colspan='".$colspan."' style='white-space:nowrap'>-</td>";

																$exceldata[$row][] = "";
															} else	{
																$displayString .= "<td colspan='".$colspan."' style='white-space:nowrap'>".$status[$d]."</td>";

																$exceldata[$row][] = strip_tags($status[$d]);
															}
														}
														//  OTHER FIELDS
														else	{
															if (preg_match("/^((\d{2}(([02468][048])|([13579][26]))[\-\/\s]?((((0?[13578])|(1[02]))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-\/\s]?((0?[1-9])|([1-2][0-9])))))|(\d{2}(([02468][1235679])|([13579][01345789]))[\-\/\s]?((((0?[13578])|(1[02]))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-\/\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))(\s(((0?[1-9])|(1[0-2]))\:([0-5][0-9])((\s)|(\:([0-5][0-9])\s))([AM|PM|am|pm]{2,2})))?$/", $d))
																$displayString .= "<td colspan='".$colspan."' style='white-space:nowrap'>".$d."</td>";
															else if (is_numeric($d))
																$displayString .= "<td colspan='".$colspan."' style='white-space:nowrap' class='rightdata'>".number_format((double)$d, 2, ".", "")."</td>";
															else if ($d == "")
																$displayString .= "<td colspan='".$colspan."' style='white-space:nowrap'>-</td>";
															else
																$displayString .= "<td colspan='".$colspan."' style='white-space:nowrap'>".$d."</td>";

															$exceldata[$row][] = strip_tags($d);
														}


														for ($i = 1; $i < $colspan; $i++)	$exceldata[$row][] = "";

														$col++;
													}

													$displayString .= "</tr>";
													$row++;
												}

												$displayString .= "<tr>";
													$colspan = ($type == "Time") ? array_search("Time Spent", $headings[$type]) + 2 : array_search("Cost <i>(".$_SESSION["currency"].")</i>", $headings[$type]);

													$displayString .= "<td class='on-table-total' colspan='".$colspan."'>Total:</td>";

													for ($i = 1; $i < $colspan; $i++)	$exceldata[$row][] = "";

													$exceldata[$row][] .= "Total:";

													if ($type == "Time")    {
														$displayString .= "<td class='on-table-total'>".number_format((double)$totalTime, 2, ".", "")."</td>";
														$displayString .= "<td class='on-table-total'></td>";
														$displayString .= "<td class='on-table-total'>".number_format((double)$totalCost, 2, ".", "")."</td>";

														$exceldata[$row][] .= $totalTime;
														$exceldata[$row][] .= "";
														$exceldata[$row][] .= $totalCost;
													} else  {
														$displayString .= "<td class='on-table-total'>".number_format((double)$totalCost, 2, ".", "")."</td>";

														$exceldata[$row][] .= $totalCost;
													}

												$displayString .= "</tr>";
												$row++;
                                            }

                                            echo $displayString;

                                            if ($type == "Time")	{
												echo "<tr><td style='background-color:transparent' colspan='100%'>&nbsp;</td></tr>";

												for ($a = 0; $a < $columns; $a++)    $exceldata[$row][] = "";

												$row++;
											}
                                        }

                                            echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                                        echo "</table>";
                                    echo "</div><br/>";

									///////////////////////////
									$_SESSION["fileName"] = "Approval_Report";
									$_SESSION["fileData"] = $exceldata;
									///////////////////////////

									echo "<input name='btnExport' onClick=\"location.href='_export.php'\" type='button' value='Export to Excel' />";
                                }
                                ?>
                            <br/><br/><br/>
                            <?php
                            }
						}
						?>
					</form>
                </td>
            </tr>
        </table>
    <?php
    }
}
?>
