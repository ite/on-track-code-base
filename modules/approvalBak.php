<?php
class Module    {
    ///////////////////////////
    //   Module Constructor
    ///////////////////////////
    function Module()       {
    }

    ///////////////////////////
    //   Functions Of Module
    ///////////////////////////
    //  Get PHP Script For Before Headers Are Printed
    function getPHPScript()     {
        include("_functions.php");

        //Global Variables
        GLOBAL $isSharedProject;
        GLOBAL $isSPM;

        //  Approve And Update Function
        if (isset($_POST["update"]) && $_POST["update"] === "1")        {
            //  Get Information - Time Information
            $nRecords = addslashes(strip_tags($_POST["count"]));

            $timeUpdates = 0;
            $timeApprovals = 0;
            $notifyMessage = "An update to an unapproved time entry has been made by: ".$_SESSION["email"];
            $approveMessage = "An unapproved time entry has been approved by: ".$_SESSION["email"];

            unset($companyArr);
            unset($counterArr);

            for ($a = 1; $a <= $nRecords; $a++) {
                $id = addslashes(strip_tags($_POST["A".$a]));
                $description = addslashes(strip_tags($_POST["B".$a]));
                $rate = addslashes(strip_tags($_POST["C".$a]));
                $time = addslashes(strip_tags($_POST["D".$a]));
                $status = 0;

                $userID = q("SELECT user_id FROM ApprovedTime WHERE id = '$id'");   
                $companyID = q("SELECT company_id FROM Company_Users WHERE user_id = '$userID'");   
                $projectType = q("SELECT projectType FROM ApprovedTime WHERE id = '$id'");  
                $projectID = q("SELECT project_id FROM ApprovedTime WHERE id = '$id'");  
                $activityID = q("SELECT activity_id FROM ApprovedTime WHERE id = '$id'");

                $isSharedProject = ($projectType == "CP") ? 0 : 1;
                $isPM = q("SELECT manager FROM Project_User WHERE user_id = '".$_SESSION["user_id"]."' AND project_id = '".$projectID."' AND company_id = '".$_SESSION["company_id"]."'");
                $isSPM = q("SELECT spm_manager FROM Project_User WHERE user_id = '".$_SESSION["user_id"]."' AND project_id = '".$projectID."'AND company_id = '".$_SESSION["company_id"]."'");

                $oldDescription = q("SELECT descr FROM ApprovedTime WHERE id = '$id'");
                $oldRate  = q("SELECT rate FROM ApprovedTime WHERE id = '$id'");
                $oldTime = q("SELECT time_spent FROM ApprovedTime WHERE id = '$id'");
                $oldStatus1 = q("SELECT status FROM ApprovedTime WHERE id = '$id'");
                $oldStatus2 = q("SELECT status2 FROM ApprovedTime WHERE id = '$id'");

                $ondate = q("SELECT on_date FROM ApprovedTime WHERE id = '$id'");
                $ontime = q("SELECT on_time FROM ApprovedTime WHERE id = '$id'");
                $datechanged = date("Y-m-d H:i:s");

                $dateNow = date("Y-m-d");
                $timeNow = date("H:i:s");                    

                //  Get Information As In Database --TIME--
                $record = q("SELECT descr, time_spent, timesheet_id, company_id FROM ApprovedTime WHERE id = '$id'");

                if (addslashes(strip_tags($_POST["G".$a])) == "1") {
                    //$delete = q("DELETE FROM ApprovedTime WHERE id = '$id'");                    
                    $status = 3;

                    if (!in_array($record[3], $companyArr))  {
                        $companyArr[] = $record[3];
                        $counterArr[$record[3]] = 1;
                    }

                    $counterArr[$record[3]]++;
                }
                else if (addslashes(strip_tags($_POST["F".$a])) == "1") {
                    $status = 2;
                    $timeApprovals++;
                }
                else if ($description != $record[0][0] || $time != $record[0][1])   {
                    $status = 1;
                    $timeUpdates++;
                }

                if  ($isSharedProject == 1) {   // IF it is a Shared Project, check user type
                    if ($status > 0)    {
                        $insert = q("INSERT INTO auditlogtime (id_approved,user_id,project_id,activity_id,descr,rate,time_spent,on_date,on_time,date_changed,status,status2) 
                                            VALUES ('$id','$userID','$projectID','$activityID','$oldDescription','$oldRate','$oldTime','$ondate','$ontime','$datechanged',$oldStatus1,$oldStatus2)");
                    }

                    if ($isPM == 1 && $isSPM == 1)  {
                        $status1 = ($oldStatus1 < $status) ? $status : $oldStatus1;
                        $status2 = ($oldStatus2 < $status) ? $status : $oldStatus2;
                        $update = q("UPDATE ApprovedTime SET descr = '$description', rate = '$rate', time_spent = '$time', status = '$status1', status2 = '$status2' WHERE id = '$id'");
                    }
                    else if ($isPM == 1)    {
                        $status1 = ($oldStatus1 < $status) ? $status : $oldStatus1;
                        $update = q("UPDATE ApprovedTime SET descr = '$description',rate = '$rate',time_spent = '$time',status = '$status1' WHERE id = '$id'");
                    }
                    else if ($isSPM == 1)   {
                        $status2 = ($oldStatus2 < $status) ? $status : $oldStatus2;
                        $update = q("UPDATE ApprovedTime SET descr = '$description',rate = '$rate',time_spent = '$time',status2 = '$status2' WHERE id = '$id'");
                    }
                }

                if ($isSharedProject == 0) {   // ELSE the PM may do final approval
                    if ($status > 0)    {
                        $insert = q("INSERT INTO auditlogtime (id_approved,user_id,project_id,activity_id,descr,rate,time_spent,on_date,on_time,date_changed,".
                                            "status) VALUES ('$id','$userID','$projectID','$activityID','$oldDescription','$oldRate','$oldTime','$ondate','$ontime','$datechanged',$oldStatus1)");
                    }

                    if ($isPM == 1) {
                        $status1 = ($oldStatus1 < $status) ? $status : $oldStatus1;
                        $update = q("UPDATE ApprovedTime SET descr = '$description',rate = '$rate',time_spent = '$time',status = '$status1' WHERE id = '$id'");
                    }
                }

                if (($status == "1" || $status == "2" || $status == "3") && $update)
                    $update = q("UPDATE TimeSheet SET locked = '1' WHERE id = '".$record[0][2]."'");
            }
/*
            // EVENT NOTIFICATIONS
            if ($timeApprovals > 0) {
                $notify = q ("INSERT INTO Events (date,time,message,notify_id,processed,user_id,company_id) ".
                                "VALUES ('$dateNow','$timeNow','$approveMessage','8','0','$userid','$companyid ')");
            }
            if($timeUpdates > 0){
                $notify = q ("INSERT INTO Events (date,time,message,notify_id,processed,user_id,$company_id) ".
                                "VALUES ('$dateNow','$timeNow','$notifyMessage','8','0','$userid','$companyid ')");
            }
*/
            //  Get Information - Expense Information
            $nRecords = addslashes(strip_tags($_POST["count2"]));
        
            $expenseUpdates = 0;
            $expenseApprovals = 0;
            $notifyMessage = "An update to an unapproved expense entry has been made by: ".$_SESSION["email"];
            $approveMessage = "An unapproved expense entry has been approved by: ".$_SESSION["email"];

            for ($a = 1; $a <= $nRecords; $a++) {
                $id = addslashes(strip_tags($_POST["H".$a]));
                $description = addslashes(strip_tags($_POST["I".$a]));
                $rate = addslashes(strip_tags($_POST["J".$a]));
                $kilometers = addslashes(strip_tags($_POST["K".$a]));
                $expense = addslashes(strip_tags($_POST["L".$a]));
                $status = 0;

                $userID = q("SELECT user_id FROM ApprovedExpense WHERE id = '$id'");
                $companyID = q("SELECT company_id FROM Company_Users WHERE user_id='$userID'");  
                $projectType = q("SELECT projectType FROM ApprovedExpense WHERE id = '$id'");  
                $projectID = q("SELECT project_id FROM ApprovedExpense WHERE id = '$id'");  
                $expenseTypeID = q("SELECT expense_type_id FROM ApprovedExpense WHERE id = '$id'"); 
                $vehicleID = q("SELECT vehicle_id FROM ApprovedExpense WHERE id = '$id'"); 
                $disbursementID = q("SELECT disbursement_id FROM ApprovedExpense WHERE id = '$id'"); 
                $oldDescription = q("SELECT descr FROM ApprovedExpense WHERE id = '$id'");
                $oldKilometers = q("SELECT kilometers FROM ApprovedExpense WHERE id = '$id'"); 

                $isSharedProject = ($projectType == "CP") ? 0 : 1;
                $isPM = q("SELECT manager FROM Project_User WHERE user_id = '".$_SESSION["user_id"]."' AND project_id = '".$projectID."' AND company_id = '".$_SESSION["company_id"]."'");
                $isSPM = q("SELECT spm_manager FROM Project_User WHERE user_id = '".$_SESSION["user_id"]."' AND project_id = '".$projectID."' AND company_id = '".$_SESSION["company_id"]."'");

                $odoStart = q("SELECT odoStart FROM ApprovedExpense WHERE id = '$id'");
                $odoFinish = q("SELECT odoFinish FROM ApprovedExpense WHERE id = '$id'");
                $businessKM = q("SELECT businessKM FROM ApprovedExpense WHERE id = '$id'");
                $privateKM = q("SELECT privateKM FROM ApprovedExpense WHERE id = '$id'");

                $oldTotal = q("SELECT total FROM ApprovedExpense WHERE id = '$id'"); 
                $oldRate = q("SELECT rate FROM ApprovedExpense WHERE id = '$id'"); 
                $oldExpense = q("SELECT expense FROM ApprovedExpense WHERE id = '$id'"); 
                $oldStatus1 = q("SELECT status FROM ApprovedExpense WHERE id = '$id'");
                $oldStatus2 = q("SELECT status2 FROM ApprovedExpense WHERE id = '$id'");

                $ondate = q("SELECT on_date FROM ApprovedExpense WHERE id = '$id'");
                $ontime = q("SELECT on_time FROM ApprovedExpense WHERE id = '$id'");
                $expsheetid = $id;

                $datechanged = date("Y-m-d H:i:s");
                $dateNow = date("Y-m-d");
                $timeNow = date("H:i:s");

                //  Get Information As In Database -- EXPENSE --
                $record = q("SELECT descr, expense, expensesheet_id FROM ApprovedExpense WHERE id = '$id'");

                if (addslashes(strip_tags($_POST["N".$a])) == "1") {
                    //$delete = q("DELETE FROM ApprovedExpense WHERE id = '$id'");
                    $status = 3;

                    if (!in_array($record[3], $companyArr))  {
                        $companyArr[] = $record[3];
                        $counterArr[$record[3]] = 1;
                    }

                    $counterArr[$record[3]]++;
                }
                else if (addslashes(strip_tags($_POST["M".$a])) == "1") {
                    $status = 2;
                    $expenseApprovals++;
                }
                else if ($description != $record[0][0] || $expense != $record[0][1])    {
                    $status = 1;
                    $expenseUpdates++;
                }

                if  ($isSharedProject == 1) {   // IF it is a Shared Project, check user type
                    if ($status > 0)    {
                        $insert = q("INSERT INTO auditlogexpense (id_approved,user_id,project_id,expense_type_id,vehicle_id,disbursement_id,descr,kilometers,".
                                        "odoStart,odoFinish,businessKM,privateKM,total,rate,expense,on_date,on_time,expensesheet_id,date_changed,status,".
                                        "status2) ".
                                    "VALUES ('$id','$userID','$projectID','$expenseTypeID','$vehicleID','$disbursementID','$oldDescription','$oldKilometers',".
                                        "'$odoStart','$odoFinish','$businessKM','$privateKM','$oldTotal','$oldRate','$oldExpense','$ondate','$ontime',".
                                        "'$expsheetid','$datechanged',$oldStatus1,$oldStatus2)");
                    }

                    if ($isPM == 1 && $isSPM == 1)  {
                        $status1 = ($oldStatus1 < $status) ? $status : $oldStatus1;
                        $status2 = ($oldStatus2 < $status) ? $status : $oldStatus2;
                        $update = q("UPDATE ApprovedExpense SET descr = '$description',rate = '$rate',kilometers = '$kilometers',expense = '$expense',".
                                        "status = '$status1',status2 = '$status2' WHERE id = '$id'");
                    }
                    else if ($isPM == 1)    {
                        $status1 = ($oldStatus1 < $status) ? $status : $oldStatus1;
                        $update = q("UPDATE ApprovedExpense SET descr = '$description',kilometers = '$kilometers',expense = '$expense',status = '$status1' ".
                                    "WHERE id = '$id'");
                    }
                    else if ($isSPM == 1)   {
                        $status2 = ($oldStatus2 < $status) ? $status : $oldStatus2;
                        $update = q("UPDATE ApprovedExpense SET descr = '$description',kilometers = '$kilometers',expense = '$expense',status2 = '$status2' ".
                                    "WHERE id = '$id'");
                    }
                }

                if ($isSharedProject == 0) {   // ELSE the PM may do final approval
                    if ($status > 0)    {
                        $insert = q("INSERT INTO auditlogexpense (id_approved,user_id,project_id,expense_type_id,vehicle_id,disbursement_id,descr,kilometers,".
                                        "odoStart,odoFinish,businessKM,privateKM,total,rate,expense,on_date,on_time,expensesheet_id,date_changed,status) ".
                                    "VALUES ('$id','$userID','$projectID','$expenseTypeID','$vehicleID','$disbursementID','$oldDescription','$oldKilometers',".
                                        "'$odoStart','$odoFinish','$businessKM','$privateKM','$oldTotal','$oldRate','$oldExpense','$ondate','$ontime',".
                                        "'$expsheetid','$datechanged',$oldStatus1)");
                    }

                    if ($isPM == 1) {
                        $status1 = ($oldStatus1 < $status) ? $status : $oldStatus1;
                        $update = q("UPDATE ApprovedExpense SET descr = '$description',kilometers = '$kilometers',expense = '$expense',status = '$status1' ".
                                    "WHERE id = '$id'");
                    }
                }

                if (($status == "1" || $status == "2" || $status == "3") && $update)
                    $update = q("UPDATE ExpenseSheet SET locked = '1' WHERE id = '".$record[0][2]."'");
            }

            if (is_array($companyArr))  {
                foreach ($companyArr as $c)      {
                    if ($counterArr[$c] >= 1)   {
                        $pms = q("SELECT e.frstname, e.lstname, e.email FROM (Project_User AS pu INNER JOIN Employee AS e ON pu.user_id = e.id) 
                                    WHERE pu.project_id = '".$projectID."' AND pu.company_id = '".$c."' AND pu.manager = '1'");

                        if (is_array($pms)) {
                            foreach ($pms as $pm) {
                                $usr = "".$pm[0]." ".$pm[1]." <".$pm[2].">";

                                if ($usr == "Johan Coertzen <jc@ite.co.za>")            $recipients .= ",Johan Coertzen <jc@integrityengineering.co.za>";
                                else if ($usr == "Franszo Faul <ffaul@ite.co.za>")      $recipients .= ",Franszo Faul <ffaul@integrityengineering.co.za>";
                                else if ($usr == "Mauritz Kruger <mkruger@ite.co.za>")  $recipients .= ",Mauritz Kruger <mkruger@integrityengineering.co.za>";
                                else                                                    $recipients .= ",".$usr;
                            }
                        }

                        if ($recipients[0] == ",")      $recipients = substr($recipients, 1);

                        approvalsDeclined(q("SELECT name FROM Project WHERE id '".$projectID."'"), q("SELECT name FROM Company WHERE id '".$c."'"), $recipients);
                    }
                }
            }
/*
            // EVENT NOTIFICATIONS
            if($expenseApprovals > 0){
                $notify = q ("INSERT INTO Events (date,time,message,notify_id,processed,user_id,company_id) ".
                                "VALUES ('$dateNow','$timeNow','$approveMessage','8','0','$userid','$companyid' )");
            }
            if($expenseUpdates > 0){
                $notify = q ("INSERT INTO Events (date,time,message,notify_id,processed,user_id,company_id) ".
                                "VALUES ('$dateNow','$timeNow','$notifyMessage','8','0','$userid','$companyid')");
            }
*/
            if ($update)
                header("Location: _module_handler.php?module=approval&what=display");
        }
    }

    //  Get List Of Projects With Unapproved Time
    function getList()  {
        //  Set Link Details
        $link = "color:#336699;font-weight:normal;";

        $projectType = "all";

        if (isset($_POST["projectType"]))       $projectType = addslashes(strip_tags($_POST["projectType"]));

        echo "<table width='100%'>";
            echo "<tr>";
                echo "<td class='centerdata'>";
                    echo "<form action='' method='post' name='getDisplay'>";
                        echo "<table width='100%'>";
                            echo "<tr>";
                                echo "<td class='on-description' width='50%'>";
                                    echo "Project Type:";
                                echo "</td>";
                                echo "<td class='on-description-left' width='50%'>";
                                    $checked = ($projectType == "all") ? "checked" : "";
                                    echo "<input method='post' name='projectType' type='radio' value='all' onClick='submit();' $checked><a>All Projects</a><br/>";

                                    $checked = ($projectType == "invoiceable") ? "checked" : "";
                                    echo "<input method='post' name='projectType' type='radio' value='invoiceable' onClick='submit();' $checked><a>Invoiceable Projects</a><br/>";

                                    $checked = ($projectType == "non_invoicable") ? "checked" : "";
                                    echo "<input method='post' name='projectType' type='radio' value='non_invoicable' onClick='submit();' $checked><a>Non-Invoicable Projects</a><br/>";
                                echo "</td>";
                            echo "</tr>";
                        echo "</table>";

                        //  Unapproved Time
                        echo "<br/>";
                        echo "<table width='100%'>";
                            echo "<tr>";
                                echo "<td class='centerdata'>";
                                    echo "<h6>";
                                        echo "Project(s) with Unapproved Time";
                                    echo "</h6>";
                                echo "</td>";
                            echo "</tr>";
                        echo "</table>";
                        echo "<br/>";

                        $where = ($projectType == "all") ? "" : (($projectType == "invoiceable") ? "AND cot.total_budget > 0 " : "AND cot.total_budget = '' ");

                        if ($_GET["type"] == "SP")      $where2 .= "AND p.p_type = 'SP' AND pu.spm_manager = '1' ";
                        //if ($_GET["type"] == "CP")      $where2 .= "AND p.p_type = 'CP' ";

                        $projects = q("SELECT DISTINCT(p.id), p.name, p_type FROM (Project AS p 
                                                INNER JOIN Project_User AS pu ON p.id = pu.project_id 
                                                INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) 
                                                WHERE pu.user_id = '".$_SESSION["user_id"]."' 
                                                AND cot.company_id = '".$_SESSION["company_id"]."' 
                                                AND pu.company_id = '".$_SESSION["company_id"]."' 
                                                AND p.completed = '0' 
                                                AND p.deleted = '0' 
                                                AND pu.deleted = '0' $where2 ORDER BY UPPER(p.name)");

                        if (is_array($projects))    {
                            unset ($tmpData);

                            foreach ($projects as $project) {
                                $hasUnapproved = ($project[2] == "CP") ? q("SELECT * FROM ApprovedTime WHERE project_id = '".$project[0]."' AND status NOT IN ('2','3')") 
                                                                        : q("SELECT * FROM ApprovedTime WHERE project_id = '".$project[0]."' AND (status NOT IN ('2','3') OR status2 NOT IN ('2','3'))");

                                if (is_array($hasUnapproved))   $tmpData[] = $project;
                            }

                            $projects = $tmpData;
                        }

                        echo "<table class='on-table-center on-table'>";
                            //  Headings
                            echo "<tr>";
                                echo "<th>Project Name</th>";
                            echo "</tr>";
                            //  Table Information
                            if (is_array($projects))    {
                                foreach ($projects as $project) {
                                    echo "<tr>";
                                        echo "<td>";
                                            $id = $project[0];

                                            echo "<a href='_module_handler.php?module=approval&what=display&type=".$project[2]."&project_id=".$id."'>".$project[1]."</a>";
                                        echo "</td>";
                                    echo "</tr>";
                                }
                            } else      {
                                echo "<tr>";
                                    echo "<td class='tableBodyCenter'>";
                                        echo "No projects with unapproved time";
                                    echo "</td>";
                                echo "</tr>";
                            }
                        echo "</table>";

                        //  Unapproved Expenses
                        echo "<br/>";
                        echo "<table width='100%'>";
                            echo "<tr>";
                                echo "<td class='centerdata'>";
                                    echo "<h6>";
                                        echo "Project(s) with Unapproved Expenses";
                                    echo "</h6>";
                                echo "</td>";
                            echo "</tr>";
                        echo "</table>";
                        echo "<br/>";

                        //  Company Projects
                        $where = ($projectType == "all") ? "" : (($projectType == "invoiceable") ? "AND cot.total_budget > 0 " : "AND cot.total_budget = '' ");

                        if ($_GET["type"] == "SP")      $where .= "AND p.p_type = 'SP' AND pu.spm_manager = '1' ";
                        //if ($_GET["type"] == "CP")      $where .= "AND p.p_type = 'CP' ";

                        $projects = q("SELECT DISTINCT(p.id), p.name, p_type FROM (Project AS p 
                                                INNER JOIN Project_User AS pu ON p.id = pu.project_id 
                                                INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) 
                                                WHERE pu.user_id = '".$_SESSION["user_id"]."' 
                                                AND cot.company_id = '".$_SESSION["company_id"]."' 
                                                AND pu.company_id = '".$_SESSION["company_id"]."' 
                                                AND p.completed = '0' 
                                                AND p.deleted = '0' 
                                                AND pu.deleted = '0' $where ORDER BY UPPER(p.name)");

                        if (is_array($projects))        {
                            unset ($tmpData);

                            foreach ($projects as $project)     {
                                $hasUnapproved = ($project[2] == "CP") ? q("SELECT * FROM ApprovedExpense WHERE project_id = '".$project[0]."' AND status NOT IN ('2','3')") 
                                                                        : q("SELECT * FROM ApprovedExpense WHERE project_id = '".$project[0]."' AND (status NOT IN ('2','3') OR status2 NOT IN ('2','3'))");

                                if (is_array($hasUnapproved))   $tmpData[] = $project;
                            }

                            $projects = $tmpData;
                        }

                        echo "<table class='on-table-center on-table'>";
                            //  Headings
                            echo "<tr>";
                                echo "<th>";
                                        echo "Project Name"; // Project Name
                                echo "</th>";
                            echo "</tr>";
                            //  Table Information
                            if (is_array($projects))    {
                                foreach ($projects as $project) {
                                    echo "<tr>";
                                        echo "<td>";
                                            $id = $project[0];

                                            echo "<a href='_module_handler.php?module=approval&what=display&type=".$project[2]."&project_id=".$id."'>".$project[1]."</a>";
                                        echo "</td>";
                                    echo "</tr>";
                                }
                            } else      {
                                echo "<tr>";
                                    echo "<td class='tableBodyCenter'>";
                                        echo "No invoicable projects with unapproved expenses";
                                    echo "</td>";
                                echo "</tr>";
                            }
                        echo "</table>";
                        echo "<br/>";
                    echo "</form>";
                echo "</td>";
            echo "</tr>";
        echo "</table>";
    }

    //  Get Time Approval Information
    function getDisplay()   {
        ?>
        <link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
        <script type="text/javascript" src="include/datepicker.js"></script>
        <script language="JavaScript" src="include/validation.js"></script>
        <script language="JavaScript">
            $(function()        {
                //  Calendar(s)
                $("#date_from").DatePicker({
                        format:"Y-m-d",
                        date: $("#date_from").val(),
                        current: $("#date_from").val(),
                        starts: 1,
                        position: "right",
                        onBeforeShow: function(){
                            var _date = (!validation("date",$("#date_from").val())) ? new Date() : $("#date_from").val();
                            $("#date_from").DatePickerSetDate(_date, true);
                        },
                        onChange: function(formated, dates){
                            $("#date_from").val(formated);
                            $("#date_from").DatePickerHide();
                        }
                });

                $("#date_to").DatePicker({
                        format:"Y-m-d",
                        date: $("#date_to").val(),
                        current: $("#date_to").val(),
                        starts: 1,
                        position: "right",
                        onBeforeShow: function(){
                            var _date = (!validation("date",$("#date_to").val())) ? new Date() : $("#date_to").val();
                            $("#date_to").DatePickerSetDate(_date, true);
                        },
                        onChange: function(formated, dates){
                            $("#date_to").val(formated);
                            $("#date_to").DatePickerHide();
                        }
                });

                $("#dateType").change(function()        {
                    if ($(this).val() == "custom")      $("#datesDiv").show();
                    else                                $("#datesDiv").hide();
                });
            });

            function check()    {
                document.forms["getInformation"].update.value = 1;
                document.forms["getInformation"].submit();
            }

            function display()  {
                var valid = 1;

                //  Check That Project Is Selected
                if (document.forms["getDisplay"].project.value == "null")   {
                    ShowLayer("projectDiv", "block");
                    valid = 0;
                }
                else
                    ShowLayer("projectDiv", "none");

                //  Check That Date From Is Entered
                if (document.forms["getDisplay"].date_from.value == "") {
                    ShowLayer("dateFrom", "block");
                    valid = 0;
                }
                //  Check That Entered Date From Is Valid
                else    {
                    ShowLayer("dateFrom", "none");

                    if (!validation("date", document.forms["getDisplay"].date_from.value))  {
                        ShowLayer("dateFromDiv", "block");
                        valid = 0;
                    }
                    else
                        ShowLayer("dateFromDiv", "none");
                }

                //  Check That Date To Is Entered
                if (document.forms["getDisplay"].date_to.value == "")   {
                    ShowLayer("dateTo", "block");
                    valid = 0;
                }
                //  Check That Entered Date To Is Valid
                else    {
                    ShowLayer("dateTo", "none");

                    if (!validation("date", document.forms["getDisplay"].date_to.value))    {
                        ShowLayer("dateToDiv", "block");
                        valid = 0;
                    }
                    else
                        ShowLayer("dateToDiv", "none");
                }

                if (valid == 1) {
                    document.forms["getDisplay"].save.value = 1;
                    document.forms["getDisplay"].submit();
                }
            }

            function recalculateCost()  {
                var nRecords = document.forms["getInformation"].count.value;
                var total_time = 0;
                var total_cost = 0;

                for (var i = 1; i <= nRecords; i++) {
                    var rate = "C" + i;
                    var time = "D" + i;
                    var cost = "E" + i;

                    var new_value = eval("document.forms['getInformation']."+rate+".value * document.forms['getInformation']."+time+".value;");

                    eval("document.forms['getInformation']."+time+".value = parseFloat(document.forms['getInformation']."+time+".value).toFixed(2);");
                    eval("document.forms['getInformation']."+cost+".value = new_value.toFixed(2);");

                    total_time += parseFloat(eval("document.forms['getInformation']."+time+".value;"));
                    total_cost += parseFloat(eval("document.forms['getInformation']."+cost+".value;"));
                }

                document.forms["getInformation"].time.value = total_time.toFixed(2);
                document.forms["getInformation"].cost.value = total_cost.toFixed(2);
            }

            function recalculateTime()  {
                var nRecords = document.forms["getInformation"].count.value;
                var total_time = 0;
                var total_cost = 0;

                for (var i = 1; i <= nRecords; i++) {
                    var rate = "C" + i;
                    var time = "D" + i;
                    var cost = "E" + i;

                    if (eval("document.forms['getInformation']."+cost+".value") == 0 || eval("document.forms['getInformation']."+cost+".value") == "")  {
                        eval("document.forms['getInformation']."+time+".value = 0.00;");
                        eval("document.forms['getInformation']."+cost+".value = 0.00;");
                    }
                    else    {
                        var new_value = eval("document.forms['getInformation']."+cost+".value / document.forms['getInformation']."+rate+".value;");

                        eval("document.forms['getInformation']."+cost+".value = parseFloat(document.forms['getInformation']."+cost+".value).toFixed(2);");
                        eval("document.forms['getInformation']."+time+".value = new_value.toFixed(2);");
                    }

                    total_time += parseFloat(eval("document.forms['getInformation']."+time+".value;"));
                    total_cost += parseFloat(eval("document.forms['getInformation']."+cost+".value;"));
                }

                document.forms["getInformation"].time.value = total_time.toFixed(2);
                document.forms["getInformation"].cost.value = total_cost.toFixed(2);
            }

            function recalculateExpense()   {
                var nRecords = document.forms["getInformation"].count2.value;
                var total_expense = 0;

                for (var i = 1; i <= nRecords; i++) {
                    var rate = "J" + i;
                    var distance = "K" + i;
                    var expense = "L" + i;

                    if (eval("document.forms['getInformation']."+rate+".value") != "-") {
                        var new_value = eval("document.forms['getInformation']."+rate+".value * document.forms['getInformation']."+distance+".value;");

                        eval("document.forms['getInformation']."+distance+".value = parseFloat(document.forms['getInformation']."+distance+".value).toFixed(2);");
                        eval("document.forms['getInformation']."+expense+".value = new_value.toFixed(2);");

                        total_expense += parseFloat(eval("document.forms['getInformation']."+expense+".value;"));
                   }
                }

                document.forms["getInformation"].expenseTotal.value = total_expense.toFixed(2);
            }

            function recalculateRate()  {
                var nRecords = document.forms["getInformation"].count2.value;
                var total_expense = 0;

                for (var i = 1; i <= nRecords; i++) {
                    var rate = "J" + i;
                    var distance = "K" + i;
                    var expense = "L" + i;

                    if (eval("document.forms['getInformation']."+expense+".value") == 0 || eval("document.forms['getInformation']."+expense+".value") == "")    {
                        eval("document.forms['getInformation']."+distance+".value = 0.00;");
                        eval("document.forms['getInformation']."+expense+".value = 0.00;");
                    }
                    else    {
                        if (eval("document.forms['getInformation']."+rate+".value") != "-") {
                            var new_value = eval("document.forms['getInformation']."+expense+".value / document.forms['getInformation']."+distance+".value;");

                            eval("document.forms['getInformation']."+expense+".value = parseFloat(document.forms['getInformation']."+expense+".value).toFixed(2);");
                            eval("document.forms['getInformation']."+rate+".value = new_value.toFixed(2);");
                        }
                    }

                    total_expense += parseFloat(eval("document.forms['getInformation']."+expense+".value;"));
                }

                document.forms["getInformation"].expenseTotal.value = total_expense.toFixed(2);
            }

            function selection(check_value) {
                var nRecords = document.forms["getInformation"].count.value;

                for (var i = 1; i <= nRecords; i++) {
                    var box = "F" + i;
                    var box2 = "G" + i;

                    eval("document.forms['getInformation']."+box+".checked = "+check_value);
                    eval("document.forms['getInformation']."+box2+".checked = "+"false");
                }
            }

            function selection2(check_value)    {
                var nRecords = document.forms["getInformation"].count2.value;

                for (var i = 1; i <= nRecords; i++) {
                    var box = "M" + i;
                    var box2 = "N" + i;

                    eval("document.forms['getInformation']."+box+".checked = "+check_value);
                    eval("document.forms['getInformation']."+box2+".checked = "+"false");
                }
            }
            
            ///// -- Check/Uncheck -- /////
            function myTimeCheck(boxName){
               $("#G"+boxName).attr("checked",false);
            }
            
            function myTimeCheck2(boxName){
               $("#F"+boxName).attr("checked",false);
            }
            
            function myExCheck(boxName){
               $("#N"+boxName).attr("checked",false);
               //alert($("#M"+boxName));
            }
            
            function myExCheck2(boxName){
               $("#M"+boxName).attr("checked",false);
               //alert($("#N"+boxName));
            }
            
        </script>
        <?php
        //Global Variables
        GLOBAL $isSharedProject;
        GLOBAL $isSPM;

        $today = date("Y-m-d");

        $where = "";

        if ($_GET["type"] == "SP")      $where .= "AND p.p_type = 'SP' AND pu.spm_manager = '1' ";
        //if ($_GET["type"] == "CP")      $where .= "AND p.p_type = 'CP' ";

        //  Company Projects
        $projects = q("SELECT DISTINCT(p.id), p.name, p_type FROM (Project AS p 
                                INNER JOIN Project_User AS pu ON p.id = pu.project_id 
                                INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) 
                                WHERE pu.user_id = '".$_SESSION["user_id"]."' 
                                AND cot.company_id = '".$_SESSION["company_id"]."' 
                                AND pu.company_id = '".$_SESSION["company_id"]."' 
                                AND p.completed = '0' 
                                AND p.deleted = '0' 
                                AND pu.deleted = '0' $where ORDER BY UPPER(p.name)");

        if ($_GET["project_id"] != "")  $projectID = $_GET["project_id"];

        if (isset($_POST["project"]) && $_POST["project"] != "null")    $projectID = $_POST["project"];

        $dateFrom = currentWeekStart();
        $dateTo = $today;

        echo "<table width='100%'>";
            echo "<tr>";
                echo "<td class='centerdata'>";
                    echo "<form action='' method='post' id='getDisplay' name='getDisplay'>";

                    $date_type = $_POST["dateType"];

                    //  Get Dates
                    if ($date_type == "currentwk")      {
                        $dateFrom = currentWeekStart();
                        $dateTo = $today;
                    }
                    else if ($date_type == "previouswk")        {
                        $dateFrom = getDates(currentWeekStart(), -7);
                        $dateTo = getDates(currentWeekStart(), -1);
                    }
                    else if ($date_type == "currentmnth")       {
                        $dateFrom = getMonthStart($today);
                        $dateTo = $today;
                    }
                    else if ($date_type == "previousmnth")      {
                        $dateFrom = getPreviousMonthStart();
                        $dateTo = getMonthEnd(getPreviousMonthStart());
                    }
                    else if ($date_type == "custom")    {
                        $dateFrom = addslashes(strip_tags($_POST["date_from"]));
                        $dateTo = addslashes(strip_tags($_POST["date_to"]));
                    }

                        echo "<table width='100%'>";
                            echo "<tr>";
                                echo "<td class='centerdata'>";
                                    echo "<h6>";
                                        echo "Approve Time/Expense Sheet(s)";
                                    echo "</h6>";
                                echo "</td>";
                            echo "</tr>";
                        echo "</table>";
                        echo "<br/>";
                        echo "<table width='100%'>";
                            echo "<tr>";
                                echo "<td class='on-description' width='50%'>";
                                        echo "Select Project:";
                                echo "</td>";
                                echo "<td  width='50%'>";
                                    echo "<select class='on-field' method='post' id='project' name='project' class='required' tabindex='1'>";
                                        echo "<option value='null'>--  Select Project  --</option>";
                                            if (is_array($projects))    {
                                                foreach ($projects as $project) {
                                                    if ($projectID == $project[0])      echo "<option value='".$project[0]."' selected>".$project[1]."</option>";
                                                    else                                echo "<option value='".$project[0]."'>".$project[1]."</option>";
                                                }
                                            }
                                    echo "</select>";
                                    echo "<div id='projectDiv' style='display: none;'><font class='on-validate-error'>* Project must be selected</font></div>";
                                echo "</td>";
                            echo "</tr>";
                            echo "<tr>";
                                echo "<td class='on-description' width='50%'>";
                                        echo "Date Type(s):";
                                echo "</td>";
                                echo "<td  width='50%'>";
                                    echo "<select class='on-field' method='post' id='dateType' name='dateType'>";
                                        $selected = ($date_type == "currentwk") ? "selected" : "";
                                        echo "<option value='currentwk' $selected>Current Week</option>";

                                        $selected = ($date_type == "previouswk") ? "selected" : "";
                                        echo "<option value='previouswk' $selected>Previous Week</option>";

                                        $selected = ($date_type == "currentmnth") ? "selected" : "";
                                        echo "<option value='currentmnth' $selected>Current Month</option>";

                                        $selected = ($date_type == "previousmnth") ? "selected" : "";
                                        echo "<option value='previousmnth' $selected>Previous Month</option>";

                                        $selected = ($date_type == "custom") ? "selected" : "";
                                        echo "<option value='custom' $selected>Custom Dates</option>";
                                    echo "</select>";
                                echo "</td>";
                            echo "</tr>";
                        echo "</table>";

                        $show = ($date_type == "custom") ? "block" : "none";

                        echo "<div id='datesDiv' style='display: $show;'>";
                            echo "<table width='100%'>";
                                echo "<tr>";
                                    echo "<td class='on-description' width='50%'>";
                                            echo "Date From:";
                                    echo "</td>";
                                    echo "<td  width='50%'>";
                                        echo "<input id='date_from' name='date_from' class='on-field-date date' type='text' style='text-align:right;' value='".$dateFrom."'>";
                                        echo "<div id='dateFrom' style='display: none;'><font class='on-validate-error'>* Date must be entered</font></div>";
                                        echo "<div id='dateFromDiv' style='display: none;'><font class='on-validate-error'>* Date not valid, eg. ".date("Y-m-d")."</font></div>";
                                    echo "</td>";
                                echo "</tr>";
                                echo "<tr>";
                                    echo "<td class='on-description' width='50%'>";
                                        echo "Date To:";
                                    echo "</td>";
                                    echo "<td  width='50%'>";
                                        echo "<input id='date_to' name='date_to' class='on-field-date date' type='text' style='text-align:right;' value='".$dateTo."'>";
                                        echo "<div id='dateTo' style='display: none;'><font class='on-validate-error'>* Date must be entered</font></div>";
                                        echo "<div id='dateToDiv' style='display: none;'><font class='on-validate-error'>* Date not valid, eg. ".date("Y-m-d")."</font></div>";
                                    echo "</td>";
                                echo "</tr>";
                            echo "</table>";
                        echo "</div>";
                        echo "<br/>";
                        echo "<input name='btnDisplay' onClick='display();' tabindex='2' type='button' value='Display Information'>";
                        echo "<input method='post' name='save' type='hidden' value='0' />";
                    echo "</form>";

                    //  If Display Button Is Pressed
                    if ((isset($_POST["save"]) && $_POST["save"] === "1") || $projectID != "")  {
                   
                        echo "<form action='' method='post' id='getInformation' name='getInformation'>";

                            ///////////////////////////
                            //  Get Project TIME Information
                            if (isset($_POST["project"]) && $_POST["project"] != "null")        $projectID = $_POST["project"];

                            $projectType = q("SELECT p_type FROM Project WHERE id = '".$projectID."'");
                            $projectCID = q("SELECT company_id FROM Project WHERE id = '".$projectID."'");

                            $isSharedProject = ($projectType == "CP") ? 0 : 1;
                            $isPM = q("SELECT manager FROM Project_User WHERE user_id = '".$_SESSION["user_id"]."' AND project_id = '".$projectID."' AND company_id = '".$_SESSION["company_id"]."'");
                            $isSPM = q("SELECT spm_manager FROM Project_User WHERE user_id = '".$_SESSION["user_id"]."' AND project_id = '".$projectID."' AND company_id = '".$_SESSION["company_id"]."'");

                            //Generate Where clause
                            $where = ($isSharedProject == 1) ? "AND (ta.status != '2' OR ta.status2 != '2') AND ta.status2 != '3' " : "AND ta.status NOT IN ('2','3') ";

                            if ($projectCID != $_SESSION["company_id"]) $where .= "AND cu.company_id = '".$_SESSION["company_id"]."' ";

                            if ($projectCID == $_SESSION["company_id"] && $isPM && !$isSPM)     $where .= "AND cu.company_id = '".$_SESSION["company_id"]."' ";

                            $nRecords = q("SELECT COUNT(DISTINCT(ta.id)) FROM (ApprovedTime AS ta 
                                            INNER JOIN Activities AS a ON ta.activity_id = a.id 
                                            INNER JOIN ActivityTypes AS at ON a.parent_id = at.id 
                                            INNER JOIN Employee AS e ON e.id = ta.user_id 
                                            INNER JOIN Company_Users AS cu ON e.id = cu.user_id) 
                                            WHERE ta.project_id = '$projectID' $where 
                                            AND ta.date >= '$dateFrom' 
                                            AND ta.date <= '$dateTo'");

                            //$checkAreas = q("SELECT ar.name FROM (ApprovedTime AS ta INNER JOIN areas AS ar ON ar.id = ta.area_id) WHERE ta.project_id = '$projectID'");

                            //if (is_array($checkAreas))  {
                                $areaSelect = ", ar.name";                
                                $areaJoin = " LEFT JOIN areas AS ar ON ar.id = ta.area_id ";
                                $areaTH = "<th>Area</th>";
                                $hasArea = 1;
                           // } else      {
                            //    $areaSelect = "";                
                            //    $areaJoin = "";
                            //    $areaTH ="";
                            //    $hasArea = 0;
                            //}
                            
                            // Query -- TIME -- data
                            $records = q("SELECT DISTINCT(ta.id), CONCAT(e.lstname, ', ', e.frstname) AS emp, ta.date, at.type, a.name, ta.descr, ta.status, ta.status2, ta.rate, ta.time_spent ".$areaSelect."
                                            FROM (ApprovedTime AS ta 
                                            INNER JOIN Activities AS a ON ta.activity_id = a.id 
                                            INNER JOIN ActivityTypes AS at ON a.parent_id = at.id 
                                            INNER JOIN Employee AS e ON e.id = ta.user_id 
                                            ".$areaJoin."
                                            INNER JOIN Company_Users AS cu ON e.id = cu.user_id) 
                                            WHERE ta.project_id = '$projectID' $where 
                                            AND ta.date >= '$dateFrom' 
                                            AND ta.date <= '$dateTo' ORDER BY ta.date");

                            ///////////////////////////
                            //  Display Information
                            $index = 1;
                            $total_time = 0;
                            $total_cost = 0;

                            $status = array();
                            $status[0] = "<font class='on-validate-error'>Unapproved</font>";
                            $status[1] = "Updated";
                            $status[2] = "Approved";
                            $status[3] = "Declined";

                            ////////// -- TIME -- //////////
                            if (is_array(($records)))   {
                                echo "<div class='on-20px'>";
                                    echo "<table class='on-table on-table-center'>";
                                        echo "<tr>";
                                            echo "<td style='text-align:center' class='on-table-clear' colspan='100%'>";
                                                 echo "<h6>Unapproved Time</h6>";
                                            echo "</td>";
                                        echo "</tr>"; 
                                        echo "<tr>";
                                            $colspan = ($isSharedProject == 0) ? 11 : 12;
                                            echo "<td colspan='100%' class='rightdata'>";
                                                echo "<input name='btnSelect' onClick='selection(true);' tabindex='3' type='button' value='Select All'>";
                                                echo "<input name='btnDeselect' onClick='selection(false);' tabindex='4' type='button' value='Deselect All'><br/>";
                                            echo "</td>";
                                        echo "</tr>";
                                        echo "<tr>";
                                            echo "<th>Employee Name</th>";
                                            echo "<th>Date</th>";
                                            echo "<th>&nbsp;</th>";
                                            echo $areaTH;
                                            echo "<th>Activity Type</th>";
                                            echo "<th>Activity</th>";
                                            echo "<th>Description</th>";
                                            echo "<th>PM Status</th>";
                                            if ($isSharedProject == 1)  echo "<th>SPM Status</th>";
                                            echo "<th>Rate</th>";
                                            echo "<th>Time Spent</th>";
                                            echo "<th>Cost <i>(".$_SESSION["currency"].")</i></th>";
                                            echo "<th>Approve</th>";
                                            echo "<th>Decline</th>";
                                        echo "</tr>";

                                        //  Table Information
                                        foreach ($records as $record)   {
                                            echo "<tr>";
                                                echo "<input method='post' name='A".$index."' type='hidden' value='".$record[0]."' />"; //  ID
                                                echo "<td style='white-space:nowrap'>".$record[1]."</td>";   //  Employee Name
                                                echo "<td style='white-space:nowrap'>".$record[2]."</td>";   //  Date
                                                echo "<td>&nbsp;</td>";
                                                if($hasArea){    
                                                    if($record[10] == "")   echo "<td style='white-space:nowrap'> - </td>";   //  Area
                                                    else echo "<td style='white-space:nowrap'>".$record[10]."</td>";   //  Area
                                                }
                                                echo "<td style='white-space:nowrap'>".$record[3]."</td>";   //  Activity Type
                                                echo "<td style='white-space:nowrap'>".$record[4]."</td>";   //  Activity
                                                echo "<td>";  //  Description
                                                echo "<input name='B".$index."' type='text' class='left' size='50' value='".str_replace("'", "&#39;" , $record[5])."'>";
                                                echo "</td>";
                                                if ($isSharedProject == 1)      {   // IF it is a Shared Project,
                                                    echo "<td style='white-space:nowrap'>".$status[$record[6]]."</td>";  //  Project Manager Status
                                                    echo "<td style='white-space:nowrap'>".$status[$record[7]]."</td>";  // Shared Project Manager Manager
                                                    $readonly = ($isSPM == 0) ? "readonly" : "";
                                                    echo "<td>";  //  Rate
                                                        echo "<a style='font-weight:normal;line-height:16px;vertical-align:top;'>";
                                                            echo "<input $readonly name='C".$index."' type='text' class='right' size='18' value='".number_format((double)$record[8], 2, ".", "")."' onChange='recalculateCost();'>";
                                                        echo "</a><br/>";             
                                                    echo "</td>";
                                                }
                                                if ($isSharedProject == 0)      {
                                                    echo "<td>".$status[$record[6]]."</td>";   //  Project Manager Status
                                                    echo "<td>";  //  Rate
                                                        echo "<a style='font-weight:normal;line-height:16px;vertical-align:top;'>";
                                                            echo "<input name='C".$index."' type='text' class='right' size='18' value='".number_format((double)$record[8], 2, ".", "")."' onChange='recalculateCost();'>";
                                                        echo "</a><br/>";             
                                                    echo "</td>";
                                                }
                                                $total_time += $record[9];
                                                echo "<td class='rightdata'>";  //  Time Spent
                                                    echo "<a style='font-weight:normal;line-height:16px;vertical-align:top;'>";
                                                        echo "<input name='D".$index."' type='text' class='right' size='11' value='".number_format((double)$record[9], 2, ".", "")."' onChange='recalculateCost();'>";
                                                    echo "</a><br/>";
                                                echo "</td>";
                                                $total_cost += ($record[8] * $record[9]);
                                                echo "<td>";  //  Cost
                                                    echo "<a style='font-weight:normal;line-height:16px;vertical-align:top;'>";
                                                        echo "<input readonly name='E".$index."' type='text' class='right' size='18' value='".number_format((double)($record[8] * $record[9]), 2, ".", "")."' 
                                                                onChange='recalculateCost();'>";
                                                    echo "</a><br/>";
                                                echo "</td>";
                                                //  Approve Checkbox
                                                echo "<td class='centerdata'>";
                                                    echo "<input id='F".$index."' name='F".$index."' type='checkbox' onChange='myTimeCheck($index)' value='1'>";
                                                echo "</td>";
                                                //  Delete Checkbox
                                                echo "<td class='centerdata'>";
                                                    echo "<input id='G".$index."' name='G".$index."' type='checkbox' onChange='myTimeCheck2($index)' value='1'>";
                                                echo "</td>";
                                            echo "</tr>";

                                            $index++;
                                        }// END foreach record

                                        if ($total_time > 0 || $total_cost > 0) {
                                            echo "<tr>";
                                                $colspan = ($isSharedProject == 0) ? 7 : 8;
                                                echo "<td colspan='".($colspan+1+$hasArea)."' class='on-table-total'>Total:</td>";
                                                echo "<td class='on-table-total'>";
                                                    echo "<a style='font-weight:normal;line-height:16px;vertical-align:top;'>";
                                                        echo "<input name='time' class='right' readonly type='text' size='11' value='".number_format((double)$total_time, 2, ".", "")."' onChange='recalculateCost();'>";
                                                    echo "</a><br/>";
                                                echo "</td>";
                                                echo "<td class='on-table-total'>";
                                                    echo "<input name='cost' class='right' readonly type='text' size='18' value='".number_format((double)$total_cost, 2, ".", "")."' onChange='recalculateCost();'>";
                                                echo "</td>";
                                                echo "<td class='on-table-total' colspan='2'></td>";
                                            echo "</tr>";
                                        }

                                        echo "<tfoot>
                                            <tr>
                                                <td colspan='100%'></td>
                                            </tr>
                                        </tfoot>
                                    </table>";
                                echo "</div>";
                                //echo "<br/>";
                                echo "<input method='post' name='project' type='hidden' value='".$_POST["project"]."' />";
                                echo "<input method='post' name='count' type='hidden' value='".$nRecords."' />";
                                //echo "<input method='post' name='update' type='hidden' value='0' />";
                                echo "<input name='btnUpdate' onClick='check();' tabindex='$index' type='button' value='Update/Approve/Decline Time'>";
                            } // END time

                            ///////////////////////////
                            //  Get Project EXPENSE Information
                            if (isset($_POST["project"]) && $_POST["project"] != "null")        $projectID = $_POST["project"];

                            $projectType = q("SELECT p_type FROM Project WHERE id = '".$projectID."'");
                            $projectCID = q("SELECT company_id FROM Project WHERE id = '".$projectID."'");

                            $isSharedProject = ($projectType == "CP") ? 0 : 1;
                            $isPM = q("SELECT manager FROM Project_User WHERE user_id = '".$_SESSION["user_id"]."' AND project_id = '".$projectID."' AND company_id = '".$_SESSION["company_id"]."'");
                            $isSPM = q("SELECT spm_manager FROM Project_User WHERE user_id = '".$_SESSION["user_id"]."' AND project_id = '".$projectID."' AND company_id = '".$_SESSION["company_id"]."'");
                            // Generate WHERE Clause
                            $where = ($isSharedProject == 1) ? "AND (ae.status != '2' OR ae.status2 != '2') AND ae.status2 != '3' " : "AND ae.status NOT IN ('2','3') ";

                            if ($projectCID != $_SESSION["company_id"]) $where .= "AND cu.company_id = '".$_SESSION["company_id"]."' ";
                            if ($projectCID == $_SESSION["company_id"] && $isPM && !$isSPM)     $where .= "AND cu.company_id = '".$_SESSION["company_id"]."' ";

                            $nRecords = q("SELECT COUNT(DISTINCT(ae.id)) 
                                            FROM (ApprovedExpense AS ae 
                                            LEFT JOIN Vehicle AS v ON ae.vehicle_id = v.id 
                                            INNER JOIN Activities AS a ON ae.activity_id = a.id 
                                            INNER JOIN ActivityTypes AS at ON a.parent_id = at.id 
                                            INNER JOIN Employee AS e ON e.id = ae.user_id 
                                            INNER JOIN Company_Users AS cu ON e.id = cu.user_id) 
                                            WHERE ae.project_id = '$projectID' $where 
                                            AND ae.date >= '$dateFrom' 
                                            AND ae.date <= '$dateTo'");

                            //$checkAreas = q("SELECT ar.name FROM (ApprovedExpense AS ae INNER JOIN areas AS ar ON ar.id = ae.area_id) WHERE ae.project_id = '$projectID'");

                          // if (is_array($checkAreas))   {
                                $areaSelect = ", ar.name";                
                                $areaJoin = " LEFT JOIN areas AS ar ON ar.id = ae.area_id ";
                                $areaTH = "<th>Area</th>";
                                $hasArea = 1;
                            //} else      {
                              //  $areaSelect = "";                
                               // $areaJoin = "";
                              //  $areaTH ="";
                               // $hasArea = 0;
                            //} 

                            // Query EXPENSE data
                            $records = q("SELECT DISTINCT(ae.id),CONCAT(e.lstname, ', ', e.frstname) AS emp,ae.date,v.type,at.type,a.name,ae.descr,ae.status,ae.status2,ae.rate,ae.kilometers,ae.expense,ae.expense_type_id 
                                                ".$areaSelect."
                                            FROM (ApprovedExpense AS ae 
                                            LEFT JOIN Vehicle AS v ON ae.vehicle_id = v.id 
                                            INNER JOIN Employee AS e ON e.id = ae.user_id 
                                            INNER JOIN Activities AS a ON ae.activity_id = a.id 
                                            INNER JOIN ActivityTypes AS at ON a.parent_id = at.id 
                                            ".$areaJoin."
                                            INNER JOIN Company_Users AS cu ON e.id = cu.user_id) 
                                            WHERE ae.project_id = '$projectID' $where AND ae.date >= '$dateFrom' AND ae.date <= '$dateTo' ORDER BY ae.date");

                            ///////////////////////////
                            //  Display Information
                            $index2 = 1;
                            $total_expenses = 0;

                            ////////// -- Expense -- //////////
                            if (is_array(($records)))   {
                                echo "<div class='on-20px'>";
                                    echo "<table class='on-table on-table-center'>";
                                        echo "<tr>";
                                            echo "<td colspan='100%' style='text-align:center' class='on-table-clear'>";
                                                echo "<h6>Unapproved Expenses</h6>";
                                            echo "</td>";
                                        echo "</tr>";
                                        echo "<tr>";
                                            $colspan = ($isSharedProject == 0) ? 10 : 11;
                                            echo "<td colspan='100%' class='rightdata'>";
                                                echo "<input name='btnSelect' onClick='selection2(true);' tabindex='$index' type='button' value='Select All'>";
                                                echo "<input name='btnDeselect' onClick='selection2(false);' tabindex='$index' type='button' value='Deselect All'><br/>";
                                            echo "</td>";
                                        echo "</tr>";
                                        echo "<tr>";
                                            echo "<th>Employee<br>Name</th>";
                                            echo "<th>Date</th>";
                                            echo "<th>Vehicle</th>";
                                            if($hasArea)        echo "<th>Area</th>";
                                            echo "<th>Activity Type</th>";
                                            echo "<th>Activity</th>";
                                            echo "<th>Description</th>";
                                            echo "<th>PM Status</th>";
                                            if ($isSharedProject == 1)  echo "<th style='white-space:nowrap'>SPM Status</th>";
                                            echo "<th>Rate</th>";
                                            echo "<th>Kilometers</th>";
                                            echo "<th>Expense<i> (".$_SESSION["currency"].")</i></th>";
                                            echo "<th>Approve</th>";
                                            echo "<th>Decline</th>";
                                        echo "</tr>";

                                        //  Table Information
                                        foreach ($records as $record)   {
                                            echo "<tr>";
                                                echo "<input method='post' name='H".$index2."' type='hidden' value='".$record[0]."' />";    //  ID
                                                echo "<td style='white-space:nowrap'>".$record[1]."</td>";  //  Employee Name
                                                echo "<td style='white-space:nowrap'>".$record[2]."</td>"; //  Date
                                                if($record[3] == "")    echo "<td style='white-space:nowrap'>-</td>";  //  Vehicle Name
                                                else                    echo "<td style='white-space:nowrap'>".$record[3]."</td>";  //  Vehicle Name
                                                if($hasArea){    
                                                    if($record[13] == "")   echo "<td style='white-space:nowrap'> - </td>";   //  Area
                                                    else echo "<td style='white-space:nowrap'>".$record[13]."</td>";   //  Area
                                                }
                                                echo "<td style='white-space:nowrap'>".$record[4]."</td>";
                                                echo "<td style='white-space:nowrap'>".$record[5]."</td>";
                                                echo "<td>"; //  Description
                                                    echo "<input name='I".$index2."' type='text' class='left' size='50' value='".str_replace("'", "&#39;" , $record[6])."'>";
                                                echo "</td>";
                                                if ($isSharedProject == 1)      {   // IF it is a Shared Project,
                                                    echo "<td style='white-space:nowrap'>".$status[$record[7]]."</td>";  //  Project Manager Status
                                                    echo "<td style='white-space:nowrap'>".$status[$record[8]]."</td>";  // Shared Project Manager Manager

                                                    $value = ($record[12] == "2") ? number_format((double)$record[7], 2, ".", "") : "-";
                                                    $action = ($record[12] == "2") ? "onChange='recalculateExpense();'" : "";
                                                    $readonly = ($isSPM == 0 || $value == "-") ? "readonly" : "";

                                                    echo "<td>";  //  Rate
                                                        echo "<input $readonly name='J".$index2."' type='text' class='right' size='18' value='".$value."' $action>";
                                                    echo "</td>";
                                                }

                                                if ($isSharedProject == 0)  {
                                                    echo "<td style='white-space:nowrap'>".$status[$record[7]]."</td>";   //  Project Manager Status

                                                    $value = ($record[12] == "2") ? number_format((double)$record[9], 2, ".", "") : "-";
                                                    $action = ($record[12] == "2") ? "onChange='recalculateExpense();'" : "";
                                                    $readonly = ($value == "-") ? "readonly" : "";

                                                    echo "<td>";  //  Rate
                                                        echo "<input $readonly name='J".$index2."' type='text' class='right' size='18' value='".$value."' $action>";
                                                    echo "</td>";
                                                }

                                                echo "<td>";  //  Kilometers
                                                    $value = ($record[10] > 0) ? number_format((double)$record[10], 2, ".", "") : "-";
                                                    $readonly = ($record[10] > 0) ? "" : "readonly";

                                                    echo "<input $readonly name='K".$index2."' type='text' class='right' size='18' value='".$value."' onChange='recalculateExpense();'>";
                                                echo "</td>";

                                                $total_expenses += $record[11];

                                                echo "<td>"; //  Expense
                                                    echo "<input $readonly name='L".$index2."' type='text' class='right' size='18' value='".number_format((double)$record[11], 2, ".", "")."' onChange='recalculateExpense();'>";
                                                echo "</td>";
                                                echo "<td class='centerdata'>";    //  Approve Checkbox
                                                    echo "<input id= 'M".$index2."' name='M".$index2."' type='checkbox' onChange='myExCheck($index2)' value='1'>";
                                                echo "</td>";
                                                echo "<td class='centerdata'>";    //  Delete Checkbox
                                                    echo "<input id= 'N".$index2."' name='N".$index2."' type='checkbox' onChange='myExCheck2($index2)' value='1'>";
                                                echo "</td>";
                                            echo "</tr>";

                                            $index++;
                                            $index2++;
                                        }

                                        if ($total_expenses > 0)        {
                                            echo "<tr>";
                                                $colspan = ($isSharedProject == 0) ? 9 : 10;

                                                echo "<td colspan='".($colspan+$hasArea)."' class='on-table-total'>Total:</td>";

                                                echo "<td class='on-table-total'>";
                                                    echo "<input name='expenseTotal' readonly class='right' type='text' size='18' value='".number_format((double)$total_expenses, 2, ".", "")."' onChange='recalculateCost();'>";
                                                echo "</td>";
                                                echo "<td class='on-table-total' colspan='2'></td>";
                                            echo "</tr>";
                                        }
                                    echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot></table>";
                                echo "</div>";
                                echo "<br/>";
                                echo "<br/>";
                                echo "<input method='post' name='count2' type='hidden' value='".$nRecords."' />";
                                echo "<input name='btnUpdate' onClick='check();' tabindex='$index' type='button' value='Update/Approve/Decline Expenses'><br/><br/>";
                            } else      {
                                echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot></table>";
                            }   
                            echo "<input method='post' name='update' type='hidden' value='0' /><br><br>";

                        // END form
                        echo "</form>";
                    }

                echo "</td>";
            echo "</tr>";
        echo "</table>";
    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                            /*     --  REPORT   --    */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //  Get Approved Time Information - Report
    function getReport()    {
        ?>
        <link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
        <script type="text/javascript" src="include/datepicker.js"></script>
        <script language="JavaScript" src="include/validation.js"></script>
        <script language="JavaScript">
            //  Calendar
            $(function(){
                //  Calendar(s)
                $("#date_from").DatePicker({
                        format:"Y-m-d",
                        date: $("#date_from").val(),
                        current: $("#date_from").val(),
                        starts: 1,
                        position: "right",
                        onBeforeShow: function(){
                            var _date = (!validation("date",$("#date_from").val())) ? new Date() : $("#date_from").val();
                            $("#date_from").DatePickerSetDate(_date, true);
                        },
                        onChange: function(formated, dates){
                            $("#date_from").val(formated);
                            $("#date_from").DatePickerHide();
                        }
                });

                $("#date_to").DatePicker({
                        format:"Y-m-d",
                        date: $("#date_to").val(),
                        current: $("#date_to").val(),
                        starts: 1,
                        position: "right",
                        onBeforeShow: function(){
                            var _date = (!validation("date",$("#date_to").val())) ? new Date() : $("#date_to").val();
                            $("#date_to").DatePickerSetDate(_date, true);
                        },
                        onChange: function(formated, dates){
                            $("#date_to").val(formated);
                            $("#date_to").DatePickerHide();
                        }
                });
            });

            function display()  {
                var valid = 1;

                //  Check That Project Is Selected
                if (document.forms["getDisplay"].project.value == "null")   {
                    ShowLayer("projectDiv", "block");
                    valid = 0;
                }
                else
                    ShowLayer("projectDiv", "none");

                //  Check That Date From Is Entered
                if (document.forms["getDisplay"].date_from.value == "") {
                    ShowLayer("dateFrom", "block");
                    valid = 0;
                }
                //  Check That Entered Date From Is Valid
                else    {
                    ShowLayer("dateFrom", "none");

                    if (!validation("date", document.forms["getDisplay"].date_from.value))  {
                        ShowLayer("dateFromDiv", "block");
                        valid = 0;
                    }
                    else
                        ShowLayer("dateFromDiv", "none");
                }

                //  Check That Date To Is Entered
                if (document.forms["getDisplay"].date_to.value == "")   {
                    ShowLayer("dateTo", "block");
                    valid = 0;
                }
                //  Check That Entered Date To Is Valid
                else    {
                    ShowLayer("dateTo", "none");

                    if (!validation("date", document.forms["getDisplay"].date_to.value))    {
                        ShowLayer("dateToDiv", "block");
                        valid = 0;
                    }
                    else
                        ShowLayer("dateToDiv", "none");
                }

                if (valid == 1) {
                    document.forms["getDisplay"].save.value = 1;
                    document.forms["getDisplay"].submit();
                }
            }
        </script>
        <?php
        //  Set Current Days Date
        $today = date("Y-m-d");

        $where = "";

        if ($_GET["type"] == "SP")      $where2 .= "AND p.p_type = 'SP' AND pu.spm_manager = '1' ";
        //if ($_GET["type"] == "CP")      $where2 .= "AND p.p_type = 'CP' ";

        //  Company Projects
        $projects = q("SELECT DISTINCT(p.id), p.name, p_type FROM (Project AS p 
                                INNER JOIN Project_User AS pu ON p.id = pu.project_id 
                                INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) 
                                WHERE pu.user_id = '".$_SESSION["user_id"]."' 
                                AND cot.company_id = '".$_SESSION["company_id"]."' 
                                AND pu.company_id = '".$_SESSION["company_id"]."' 
                                AND p.completed = '0' 
                                AND p.deleted = '0' 
                                AND pu.deleted = '0' $where2 
                                ORDER BY UPPER(p.name)");

        echo "<table width='100%'>";
            echo "<tr>";
                echo "<td class='centerdata'>";
                    echo "<form action='' method='post' name='getDisplay'>";
                        echo "<table width='100%'>";
                            echo "<tr>";
                                echo "<td class='centerdata'>";
                                    echo "<h6>";
                                        echo "Approved Time Sheet Report";
                                    echo "</h6>";
                                echo "</td>";
                            echo "</tr>";
                        echo "</table>";
                        
                            echo "<br/>";
                        echo "<table width='100%'>";
                            echo "<tr>";
                                echo "<td class='on-description' width='50%'>";
                                    echo "Select Project:";
                                echo "</td>";
                                echo "<td  width='50%'>";
                                    echo "<select method='post' id='project' name='project' class='on-field required' tabindex='1'>";
                                        echo "<option value='null'>--  Select Project  --</option>";
                                        if (is_array($projects))    {
                                            foreach ($projects as $project) {
                                                $value = $project[0];

                                                if ($_POST["project"] == $value)
                                                    echo "<option value='".$value."' selected>".$project[1]."</option>";
                                                else
                                                    echo "<option value='".$value."'>".$project[1]."</option>";
                                            }
                                        }
                                    echo "</select>";
                                    echo "<div id='projectDiv' style='display: none;'><font class='on-validate-error'>* Project must be selected</font></div>";
                                echo "</td>";
                            echo "</tr>";
                            
                            echo "<tr>";
                                echo "<td class='on-description' width='50%'>";
                                    echo "Select Status:";
                                echo "</td>";
                                echo "<td  width='50%'>";
                                    echo "<select method='post' id='status' name='status' class='on-field required' tabindex='1'>";
                                        echo "<option value='null' selected>All</option>";
                                        echo "<option value='0'>Unapproved</option>";
                                        echo "<option value='1'>Updated</option>";
                                        echo "<option value='2'>Approved</option>";
                                        echo "<option value='3'>Declined</option>";
                                    echo "</select>";
                                    echo "<div id='projectDiv' style='display: none;'><font class='on-validate-error'>* Project must be selected</font></div>";
                                echo "</td>";
                            echo "</tr>";
                            
                            echo "<tr>";
                                echo "<td class='on-description' width='50%'>";
                                    echo "Date From:";
                                echo "</td>";
                                echo "<td  width='50%'>";
                                    echo "<input id='date_from' name='date_from' class='on-field-date date' type='text' style='text-align:right;' value='".getMonthStart($today)."'>";
                                    echo "<div id='dateFrom' style='display: none;'><font class='on-validate-error'>* Date must be entered</font></div>";
                                    echo "<div id='dateFromDiv' style='display: none;'><font class='on-validate-error'>* Date not valid, eg. ".date("Y-m-d")."</font></div>";
                                echo "</td>";
                            echo "</tr>";
                            echo "<tr>";
                                echo "<td class='on-description' width='50%'>";
                                    echo "Date To:";
                                echo "</td>";
                                echo "<td  width='50%'>";
                                    echo "<input id='date_to' name='date_to' class='on-field-date date' type='text' style='text-align:right;' value='".getMonthEnd($today)."'>";
                                    echo "<div id='dateTo' style='display: none;'><font class='on-validate-error'>* Date must be entered</font></div>";
                                    echo "<div id='dateToDiv' style='display: none;'><font class='on-validate-error'>* Date not valid, eg. ".date("Y-m-d")."</font></div>";
                                echo "</td>";
                            echo "</tr>";
                        echo "</table>";
                        echo "<br/>";
                        echo "<input name='btnDisplay' onClick='display();' tabindex='2' type='button' value='Display Information'>";
                        echo "<input method='post' name='save' type='hidden' value='0' />";
                    echo "</form>";

                    //  If Display Button Is Pressed
                    if (isset($_POST["save"]) && $_POST["save"] === "1")    {
                        ///////////////////////////
                        
                        GLOBAL $excelheadings;
                        GLOBAL $exceldata;
                        
                        //  Get Information - Time
                        $projectID = $_POST["project"];
                        $project_name = q("SELECT name FROM Project WHERE id = '".$projectID."'");
                        
                        $approveStatus = $_POST["status"];
                        $date_from = addslashes(strip_tags($_POST["date_from"]));
                        $date_to = addslashes(strip_tags($_POST["date_to"]));

                        $projectType = q("SELECT p_type FROM Project WHERE id = '".$projectID."'");
                        $projectCID = q("SELECT company_id FROM Project WHERE id = '".$projectID."'");

                        $isSharedProject = ($projectType == "CP") ? 0 : 1;
                        //$where = ($isSharedProject == 1) ? "AND ta.status = '2' AND ta.status2 = '2' " : "AND ta.status = '2' ";
                        
                        if($approveStatus == "all")
                            $where = ($isSharedProject == 1) ? "AND (ta.status = '2' AND ta.status2 = '2') OR (ta.status = '3' AND ta.status2 = '3') " : "AND ta.status = '2' OR ta.status = '3' ";
                        else if($approveStatus == "0")
                            $where = ($isSharedProject == 1) ? "AND ta.status = '0' AND ta.status2 = '0' " : "AND ta.status = '0' ";
                        else if($approveStatus == "1")
                            $where = ($isSharedProject == 1) ? "AND ta.status = '1' AND ta.status2 = '1' " : "AND ta.status = '1' ";
                        else if($approveStatus == "2")
                            $where = ($isSharedProject == 1) ? "AND ta.status = '2' AND ta.status2 = '2' " : "AND ta.status = '2' ";
                        else if($approveStatus == "3")
                            $where = ($isSharedProject == 1) ? "AND ta.status = '3' AND ta.status2 = '3' " : "AND ta.status = '3' ";

                        if ($projectCID != $_SESSION["company_id"])
                            $where .= "AND cu.company_id = '".$_SESSION["company_id"]."' ";
                            
                            ////
                            $managerType = ($isSharedProject == 1) ? "AND ta.status = '2' AND ta.status2 = '2' " : "AND ta.status = '2' ";
                            $checkAreas = q("SELECT ar.name FROM (ApprovedTime AS ta 
                                                        INNER JOIN areas AS ar ON ar.id = ta.area_id) 
                                                        WHERE ta.project_id = '$projectID' $managerType");
                                                        
                           //if(is_array($checkAreas) || strlen($checkAreas >1)){
                                //$areaSelect = ", ar.name";                
                                //$areaJoin = " INNER JOIN areas AS ar ON ar.id = ta.area_id ";
                                $areaTH = "<th>Area</th>";
                                $hasArea = 1;
                                $areaTime = 1;
                           // }else{
                           //     $areaSelect = "";                
                           //     $areaJoin = "";
                           //     $areaTH ="";
                           //     $hasArea = 0;
                           // } 
                            
                         if($isSharedProject == 1){
                            $status2 = "ta.status2,";
                            $statusBool = 1;
                        }else{
                            $status2 = "";
                            $statusBool = 0;
                        }
                        $records = q("SELECT DISTINCT(ta.id), CONCAT(e.lstname, ', ', e.frstname) AS emp, ta.date, at.type, a.name, ta.descr, ta.status, ".$status2." ta.rate, ta.time_spent 
                                                ,ar.name
                                                FROM (ApprovedTime AS ta 
                                                INNER JOIN Activities AS a ON ta.activity_id = a.id 
                                                INNER JOIN ActivityTypes AS at ON a.parent_id = at.id 
                                                INNER JOIN Employee AS e ON e.id = ta.user_id 
                                                LEFT JOIN areas AS ar ON ar.id = ta.area_id
                                                INNER JOIN Company_Users AS cu ON e.id = cu.user_id) 
                                                WHERE ta.project_id = '$projectID' $where 
                                                AND ta.date >= '$date_from' 
                                                AND ta.date <= '$date_to' ORDER BY ta.date");
                        ///////////////////////////
                        //  Display Information
                        $project_name = q("SELECT name FROM Project WHERE id = '".$projectID."'");
                        $display = "";
                        $total_time = 0;

                        //  Timestamp
                        if ($date_from === $date_to)    $timestamp = "Time Period: ".$date_from;
                        else                            $timestamp = "Time Period: ".$date_from." - ".$date_to;
                        
                        $excelheadings[$row][] = "";      
                        $excelheadings[$row][] = "";
                        $excelheadings[$row][] = "";
                        $excelheadings[$row][] = "";
                        $excelheadings[$row][] = "";
                        $excelheadings[$row][] = "";
                        $excelheadings[$row][] = "";
                    if($isSharedProject == 1)
                        $excelheadings[$row][] = "";
                        $excelheadings[$row][] = "";
                        $excelheadings[$row][] = "";
                            $row++;
                            
                        $exceldata[$row][] = "Approval Report (Time)";             
                        $exceldata[$row][] = "";                        
                        $exceldata[$row][] = "";                 
                        $exceldata[$row][] = "";                            
                        $exceldata[$row][] = "";                            
                        $exceldata[$row][] = "";                 
                        $exceldata[$row][] = ""; 
                    if($isSharedProject == 1)
                        $exceldata[$row][] = "";        
                        $exceldata[$row][] = "";        
                        $exceldata[$row][] = ""; 
                            $row++;
                            
                        $exceldata[$row][] = "Project Name: ".$project_name;             
                        $exceldata[$row][] = "";                        
                        $exceldata[$row][] = "";                 
                        $exceldata[$row][] = "";                            
                        $exceldata[$row][] = "";                            
                        $exceldata[$row][] = "";                 
                        $exceldata[$row][] = ""; 
                    if($isSharedProject == 1)
                        $exceldata[$row][] = "";        
                        $exceldata[$row][] = "";        
                        $exceldata[$row][] = ""; 
                            $row++;
                            
                        $exceldata[$row][] = $timestamp;             
                        $exceldata[$row][] = "";                        
                        $exceldata[$row][] = "";                 
                        $exceldata[$row][] = "";                            
                        $exceldata[$row][] = "";                            
                        $exceldata[$row][] = "";                 
                        $exceldata[$row][] = ""; 
                    if($isSharedProject == 1)
                        $exceldata[$row][] = "";        
                        $exceldata[$row][] = "";        
                        $exceldata[$row][] = ""; 
                            $row++;
                        
                        $exceldata[$row][] = "Employee Name";             
                        $exceldata[$row][] = "Date";                        
                        $exceldata[$row][] = "Area";                 
                        $exceldata[$row][] = "Activity Type";                            
                        $exceldata[$row][] = "Activity";                            
                        $exceldata[$row][] = "Description";                 
                        $exceldata[$row][] = "PM Status";   
                    if($isSharedProject == 1)
                        $exceldata[$row][] = "SPM Status";        
                        $exceldata[$row][] = "Time Spent";        
                        $exceldata[$row][] = "Cost"; 
                            $row++;

                        //  Table Headers
                        $report_heading = "<tr><td class='on-table-clear' colspan='100%'><a>Approval<font style='color:orange'> Time </font>Report</a></td></tr>
                                                <tr><td class='on-table-clear' colspan='100%'>Project Name: ".$project_name."</td></tr>".
                                            "<tr><td class='on-table-clear' colspan='100%'>".$timestamp."</td></tr>";
                        $headers1 = "<tr>".
                                        "<th>Employee Name</th>".
                                        "<th>Date</th>".
                                        $areaTH.
                                        "<th>Activity Type</th>".
                                        "<th>Activity</th>".
                                        "<th>Description</th>".
                                        "<th>PM Status</th>";
                        if($isSharedProject == 1)
                            $headers1 .= "<th>SPM Status</th>";
                        $headers1 .= "<th>Time Spent</th>".
                                        "<th>Cost <i>(".$_SESSION["currency"].")</i></th>".
                                    "</tr>";
                                    
                        $status = array();
                        $status[0] = "<font class='on-validate-error'>Unapproved</font>";
                        $status[1] = "Updated";
                        $status[2] = "Approved";
                        $status[3] = "Declined";

                        //  Table Information
                        if (is_array($records)) {
                            foreach ($records as $record) {
                            $status[0] = "<font class='on-validate-error'>Unapproved</font>";
                                $display .= "<tr>";
                                    $display .= "<td style='white-space:nowrap'>".$record[1]."</td>";   //  Employee Name
                                    $display .= "<td style='white-space:nowrap' class='rightdata'>".$record[2]."</td>";  //  Date
                                if($hasArea)
                                    $display .= "<td style='white-space:nowrap'>".$record[9+$statusBool]."</td>";    // Area
                                    $display .= "<td style='white-space:nowrap'>".$record[3]."</td>";   //  Activity Type
                                    $display .= "<td style='white-space:nowrap'>".$record[4]."</td>";   //  Activity
                                    $display .= "<td>".$record[5]."</td>";    //  Description
                                    $display .= "<td style='white-space:nowrap'>".$status[$record[6]]."</td>";   //  PM Status
                                if($status2 != "")
                                    $display .= "<td style='white-space:nowrap'>".$status[$record[7]]."</td>";   //  SPM Status

                                    //  Time Spent    
                                    $total_time += $record[8+$statusBool];

                                    $display .= "<td class='rightdata'>".number_format((double)$record[8+$statusBool], 2, ".", "")."</td>";

                                    //  Cost
                                    $total_cost += ($record[7+$statusBool] * $record[8+$statusBool]);
                               
                                    $display .= "<td class='rightdata'>".number_format((double)($record[7+$statusBool] * $record[8+$statusBool]), 2, ".", "")."</td>";
                                $display .= "</tr>";
                                
                                $status[0] = "Unapproved";
                                
                                $exceldata[$row][] = $record[1];      
                                $exceldata[$row][] = $record[2];
                                $exceldata[$row][] = $record[9+$statusBool];
                                $exceldata[$row][] = $record[3];
                                $exceldata[$row][] = $record[4];
                                $exceldata[$row][] = $record[5];
                                $exceldata[$row][] = $status[$record[6]];
                            if($isSharedProject == 1)
                                $exceldata[$row][] = $status[$record[7]];
                                $exceldata[$row][] = $record[8+$statusBool];
                                $exceldata[$row][] = $record[7+$statusBool] * $record[8+$statusBool];
                                    $row++;
                            }
                        }
                        else
                            $display .= "<tr><td class='centerdata' colspan='100%'>No information to display</td></tr>";

                        if ($total_time > 0 || $total_cost > 0) {
                            $display .= "<tr>";
                                $display .= "<td class='on-table-total' colspan='".(6+$hasArea+$statusBool)."'>Total:</td>";
                                $display .= "<td class='on-table-total'>".number_format((double)$total_time, 2, ".", "")."</td>";
                                $display .= "<td class='on-table-total'>".number_format((double)$total_cost, 2, ".", "")."</td>";
                            $display .= "</tr>";
                            
                            $exceldata[$row][] = "";      
                            $exceldata[$row][] = "";
                            $exceldata[$row][] = "";
                            $exceldata[$row][] = "";
                            $exceldata[$row][] = "";
                            $exceldata[$row][] = "";
                        if($isSharedProject == 1)
                            $exceldata[$row][] = "";
                            $exceldata[$row][] = "Total:";
                            $exceldata[$row][] = $total_time;
                            $exceldata[$row][] = $total_cost;
                                $row++;
                        }

                        $displayTime = $report_heading.$headers1.$display;
                        ///////////////////////////

                        //  EXPENSES
                        ///////////////////////////
                        //  Get Information - Expenses
                        //$where = ($isSharedProject == 1) ? "AND ae.status = '2' AND ae.status2 = '2' " : "AND ae.status = '2' ";
                        
                        if($approveStatus == "all")
                            $where = ($isSharedProject == 1) ? "AND (ae.status = '2' AND ae.status2 = '2') OR (ae.status = '3' AND ae.status2 = '3') " : "AND ae.status = '2' OR ae.status = '3' ";
                        else if($approveStatus == "0")
                            $where = ($isSharedProject == 1) ? "AND ae.status = '0' AND ae.status2 = '0' " : "AND ae.status = '0' ";
                        else if($approveStatus == "1")
                            $where = ($isSharedProject == 1) ? "AND ae.status = '1' AND ae.status2 = '1' " : "AND ae.status = '1' ";
                        else if($approveStatus == "2")
                            $where = ($isSharedProject == 1) ? "AND ae.status = '2' AND ae.status2 = '2' " : "AND ae.status = '2' ";
                        else if($approveStatus == "3")
                            $where = ($isSharedProject == 1) ? "AND ae.status = '3' AND ae.status2 = '3' " : "AND ae.status = '3' ";

                        if ($projectCID != $_SESSION["company_id"])
                            $where .= "AND cu.company_id = '".$_SESSION["company_id"]."' ";




                         $managerType = ($isSharedProject == 1) ? "AND ae.status = '2' AND ae.status2 = '2' " : "AND ae.status = '2' ";
                            $checkAreas = q("SELECT ar.name FROM (ApprovedExpense AS ae 
                                                        INNER JOIN areas AS ar ON ar.id = ae.area_id) 
                                                        WHERE ae.project_id = '$projectID' $managerType");
                                                        
                           //if(is_array($checkAreas) || strlen($checkAreas >1)){
                                $areaSelect = ", ar.name";                
                                $areaJoin = " LEFT JOIN areas AS ar ON ar.id = ae.area_id ";
                                $areaTH = "<th>Area</th>";
                                $hasArea = 1;
                           // }else{
                           //     $areaSelect = "";                
                          //      $areaJoin = "";
                          //      $areaTH ="";
                          //      $hasArea = 0;
                          //  } 
                            
                            if($areaTime = 1){
                                $areaTH = "<th>Area</th>";
                                $hasArea = 1;
                            }
                            
                         if($isSharedProject == 1){
                            $status2 = "ae.status2,";
                            $statusBool = 1;
                        }else{
                            $status2 = "";
                            $statusBool = 0;
                        }
                        
                        $records = q("SELECT DISTINCT(ae.id), CONCAT(e.lstname, ', ', e.frstname) AS emp, ae.date, v.type, ae.descr, ae.status, ".$status2." ae.kilometers, ae.expense 
                                                ,ar.name
                                                FROM (ApprovedExpense AS ae 
                                                LEFT JOIN Vehicle AS v ON ae.vehicle_id = v.id 
                                                INNER JOIN Employee AS e ON e.id = ae.user_id
                                                LEFT JOIN areas AS ar ON ar.id = ae.area_id
                                                INNER JOIN Company_Users AS cu ON e.id = cu.user_id) 
                                                WHERE ae.project_id = '$projectID' $where 
                                                AND ae.date >= '$date_from' 
                                                AND ae.date <= '$date_to' ORDER BY ae.date");

                        ///////////////////////////
                        //  Display Information
                        $display = "";
                        $total_cost2 = 0;

                        //  Table Headers
                        $report_heading = "<tr><td class='on-table-clear' colspan='100%'><a>Approval<font style='color:orange'> Expense </font>Report</a></td></tr>
                                                <tr><td class='on-table-clear' colspan='100%'>Project Name: ".$project_name."</td></tr>".
                                            "<tr><td class='on-table-clear' colspan='100%'>".$timestamp."</td></tr>";
                        $headers2 = "<tr>".
                                        "<th>Employee Name</th>".
                                        "<th>Date</th>".
                                        $areaTH.
                                        "<th>Vehicle</th>".
                                        "<th colspan='2'>Description</th>".
                                        "<th>PM Status</th>";
                        if($isSharedProject == 1)
                            $headers2 .= "<th>SPM Status</th>";
                        $headers2 .= "<th>Kilometers</th>".
                                        "<th>Expense <i>(".$_SESSION["currency"].")</i></th>".
                                    "</tr>";
                                    
                        $status = array();
                        $status[0] = "<font class='on-validate-error'>Unapproved</font>";
                        $status[1] = "Updated";
                        $status[2] = "Approved";
                        $status[3] = "Declined";

                        //  Table Information
                        if (is_array($records)) {
                        
                            $exceldata[$row][] = "";      
                            $exceldata[$row][] = "";
                            $exceldata[$row][] = "";
                            $exceldata[$row][] = "";
                            $exceldata[$row][] = "";
                            $exceldata[$row][] = "";
                            $exceldata[$row][] = "";
                        if($isSharedProject == 1)
                            $exceldata[$row][] = "";
                            $exceldata[$row][] = "";
                            $exceldata[$row][] = "";
                                $row++;
                                
                            $exceldata[$row][] = "Approval Report (Expense)";      
                            $exceldata[$row][] = "";
                            $exceldata[$row][] = "";
                            $exceldata[$row][] = "";
                            $exceldata[$row][] = "";
                            $exceldata[$row][] = "";
                            $exceldata[$row][] = "";
                        if($isSharedProject == 1)
                            $exceldata[$row][] = "";
                            $exceldata[$row][] = "";
                            $exceldata[$row][] = "";
                                $row++;
                                
                            $exceldata[$row][] = "Employee Name";             
                            $exceldata[$row][] = "Date";                        
                            $exceldata[$row][] = "Area";                 
                            $exceldata[$row][] = "Vehicle";                            
                            $exceldata[$row][] = "";                            
                            $exceldata[$row][] = "Description";                 
                            $exceldata[$row][] = "PM Status";   
                        if($isSharedProject == 1)
                            $exceldata[$row][] = "SPM Status";        
                            $exceldata[$row][] = "KM/Amount";        
                            $exceldata[$row][] = "Expense"; 
                                $row++;
                                
                            foreach ($records as $record)   {
                                $status[0] = "<font class='on-validate-error'>Unapproved</font>";
                                $display .= "<tr>";
                                    $display .= "<td style='white-space:nowrap'>".$record[1]."</td>";   //  Employee Name
                                    $display .= "<td style='white-space:nowrap' class='rightdata'>".$record[2]."</td>";   //  Date
                                    if($hasArea){
                                            if($record[10] == "") $display .= "<td style='white-space:nowrap'> - </td>";    // Area
                                            else $display .= "<td style='white-space:nowrap'>".$record[8+$statusBool]."</td>";    // Area
                                    }
                                    if($record[3] == "")
                                        $display .= "<td style='white-space:nowrap'>-</td>";   //  Vehicle
                                    else
                                        $display .= "<td style='white-space:nowrap'>".$record[3]."</td>";   //  Vehicle
                                    $display .= "<td colspan='2'>".$record[4]."</td>";   //  Description
                                    $display .="<td style='white-space:nowrap'>".$status[$record[5]]."</td>";      // PM Status
                                      if($status2 != "")
                                        $display .="<td style='white-space:nowrap'>".$status[$record[6]]."</td>";      // SPM Status
                                    
                                    $display .= "<td class='rightdata'>".number_format((double)$record[6+$statusBool], 2, ".", "")."</td>";    //  Kilometers

                                    //  Cost
                                    $total_expense += $record[7+$statusBool];
                                    $display .= "<td class='rightdata'>".number_format((double)$record[7+$statusBool], 2, ".", "")."</td>";   // Expense
                                $display .= "</tr>";
                                
                                $status[0] = "Unapproved";
                                
                                $exceldata[$row][] = $record[1];      
                                $exceldata[$row][] = $record[2];
                                $exceldata[$row][] = $record[8+$statusBool];
                                $exceldata[$row][] = $record[3];
                                $exceldata[$row][] = "";
                                $exceldata[$row][] =$record[4];
                                $exceldata[$row][] = $status[$record[5]];
                            if($isSharedProject == 1)
                                $exceldata[$row][] = $status[$record[6]];
                                $exceldata[$row][] =$record[6+$statusBool];
                                $exceldata[$row][] = $record[7+$statusBool];
                                    $row++;
                            }

                            if ($total_expense > 0) {
                                $display .= "<tr>";
                                    $display .= "<td class='on-table-total' colspan='".(7+$hasArea+$statusBool)."'>Total:</td>";
                                    $display .= "<td class='on-table-total'>".number_format((double)$total_expense, 2, ".", "")."</td>";
                                $display .= "</tr>";
                                
                                $exceldata[$row][] = "";      
                                $exceldata[$row][] = "";  
                                $exceldata[$row][] = "";  
                                $exceldata[$row][] = "";  
                                $exceldata[$row][] = "";
                                $exceldata[$row][] = "";  
                                $exceldata[$row][] = "";  
                            if($isSharedProject == 1)
                                $exceldata[$row][] = "";  
                                $exceldata[$row][] = "Total: ";  
                                $exceldata[$row][] = $total_expense; 
                                    $row++;
                            }

                            $display .= "<tr>";
                                $display .= "<td class='on-table-total' colspan='".(7+$hasArea+$statusBool)."'>Grand Total:</td>";
                                $display .= "<td class='on-table-total'>".number_format((double)$total_expense + $total_cost, 2, ".", "")."</td>";
                            $display .= "</tr>";
                            
                            $exceldata[$row][] = "";      
                            $exceldata[$row][] = "";  
                            $exceldata[$row][] = "";  
                            $exceldata[$row][] = "";  
                            $exceldata[$row][] = "";
                            $exceldata[$row][] = "";  
                            $exceldata[$row][] = ""; 
                        if($isSharedProject == 1)
                            $exceldata[$row][] = "";  
                            $exceldata[$row][] = "Grand Total: ";  
                            $exceldata[$row][] = $total_expense + $total_cost; 
                                $row++;

                            $displayExpenses = $report_heading.$headers2.$display;
                        }

                        echo "<br/>";
                        
                        if($displayTime != "" && $displayExpenses != ""){
                            echo "<div class='on-20px'><table class='on-table-center on-table'>".$displayTime."<tr><td style='background-color:transparent' colspan='100%'>&nbsp;</td></tr>".$displayExpenses."</table></div>";
                        }else{
                            if ($displayTime != "") echo "<div class='on-20px'><table class='on-table-center on-table'>".$displayTime."</table></div>";
                            if ($displayExpenses != "") echo "<div class='on-20px'><table class='on-table-center on-table'>".$displayExpenses."</table></div>";
                        }
                        
                        ///////////////////////////
                        //  Set Export Information
                        $_SESSION["fileName"] = "Approval Report";
                        $_SESSION["fileData"] = array_merge($excelheadings, $exceldata);
                        ///////////////////////////

                        echo "<br><input name='btnExport' onClick=\"location.href='_export.php'\" type='button' value='Export to Excel' /><br><br>";

                    }
                echo "</td>";
            echo "</tr>";
        echo "</table>";
    }
}
?>
