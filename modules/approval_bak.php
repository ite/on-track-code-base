<?php
class Module    {
    ///////////////////////////
    //   Module Constructor
    ///////////////////////////
    function Module()       {
    }

    ///////////////////////////
    //   Functions Of Module
    ///////////////////////////
    //  Get PHP Script For Before Headers Are Printed
    function getPHPScript()     {
        include("_functions.php");

        function getJQeury()        {
        ?>
        <link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
        <script type="text/javascript" src="include/datepicker.js"></script>
        <script language="JavaScript" src="include/validation.js"></script>
        <script language="JavaScript">
            jQuery(function()        {
            <?php
                $dateFields = array("date_from","date_to");

                foreach ($dateFields as $df)    jQDate($df);
            ?>

                jQuery("#dateType").change(function()        {
                    if (jQuery(this).val() == "custom") jQuery("#datesDiv").show();
                    else                                jQuery("#datesDiv").hide();
                });
            });

            //  Sajax
            <?php sajax_show_javascript(); ?>

            ///////////////////////////
            //  Sajax
            function ClearOptions(OptionList)   {
                for (x = OptionList.length; x >= 0; x--)        {
                    OptionList[x] = null;
                }
            }

            // Set Company
            function setCompanies(data) {
                var c = 0;

                if (!data)      ShowLayer("extraDiv", "none");

                document.forms["getDisplay"].company.options[(c++)] = new Option("All Companies", "null");

                for (var i in data)     eval("document.forms['getDisplay'].company.options["+(c++)+"] = new Option('" + data[i][1] + "', '" + data[i][0] + "');");
            }

            // Get Companies
            function getCompanies(select) {
                ClearOptions(document.getDisplay.company);

                if (select.value != "null")     ShowLayer("extraDiv", "block");
                else                            ShowLayer("extraDiv", "none");

                x_getCompanies(select.value, setCompanies);
            }
        </script>
        <?php
        }

        //Global Variables
        global $isSharedProject;
        global $isSPM;

        $today = date("Y-m-d");
        $level = $_GET["type"];

        //  Update/Approve/Decline Function
        if (isset($_POST["update"]) && $_POST["update"] === "1")        {
            unset($companyArr);
            unset($counterArr);

            $types = array("Time","Expenses");

            $projectID = addslashes(strip_tags($_POST["projectID"]));

            if (is_array($types))       {
                foreach ($types as $type)       {
                    $fields = ($type == "Time") ? array("A","B","C","D","E","F","G") : array("H","I","J","K","L","M","N");

                    $count = addslashes(strip_tags($_POST[$type]));

                    for ($i = 1; $i < $count; $i++)     {
                        $id = addslashes(strip_tags($_POST[$fields[0].$i]));
                        $descr = addslashes(strip_tags($_POST[$fields[1].$i]));
                        $rate = addslashes(strip_tags($_POST[$fields[2].$i]));
                        $amount = addslashes(strip_tags($_POST[$fields[3].$i]));
                        $cost = addslashes(strip_tags($_POST[$fields[4].$i]));

                        if ($amount == "-")     $amount = "";

                        $approved = addslashes(strip_tags($_POST[$fields[5].$i]));
                        $declined = addslashes(strip_tags($_POST[$fields[6].$i]));

                        $status = 0;

                        $select = ($type == "Time") ? "company_id,descr,time_spent,timesheet_id" : "company_id,descr,expense,expensesheet_id";
                        $table = ($type == "Time") ? "ApprovedTime" : "ApprovedExpense";

                        $recordInfo = q("SELECT $select FROM $table WHERE id = '".$id."' LIMIT 1");

                        if ($declined == "1")   {
                            $status = 3;

                            if (!in_array($recordInfo[0][0], $companyArr))  {
                                $companyArr[] = $recordInfo[0][0];
                                $counterArr[$recordInfo[0][0]] = 1;
                            }

                            $counterArr[$recordInfo[0][0]]++;
                        } else if ($approved == "1")    {
                            $status = 2;
                        } else if (($descr != $recordInfo[0][1] || $amount != $recordInfo[0][2]) && $type == "Time") {
                            $status = 1;
                        } else if (($descr != $recordInfo[0][1] || $cost != $recordInfo[0][2]) && $type == "Expenses") {
                            $status = 1;
                        }

                        $isPM = q("SELECT manager FROM Project_User WHERE user_id = '".$_SESSION["user_id"]."' AND project_id = '".$projectID."' AND company_id = '".$recordInfo[0][0]."'");
                        $isSPM = q("SELECT spm_manager FROM Project_User WHERE user_id = '".$_SESSION["user_id"]."' AND project_id = '".$projectID."'AND company_id = '".$_SESSION["company_id"]."'");

                        $auditTable = ($type == "Time") ? "auditlogtime" : "auditlogexpense";
                        $auditFields = ($type == "Time") ? ",user_id,project_id,activity_id,descr,rate,time_spent,on_date,on_time,status,status2"
                                                        : ",user_id,project_id,expense_type_id,vehicle_id,disbursement_id,descr,kilometers,odoStart,odoFinish,businessKM,privateKM,total,rate,expense,on_date,on_time,".
                                                            "expensesheet_id,status,status2";

                        if ($type == "Expenses")        $expenseType = q("SELECT name FROM dropdowns WHERE id = (SELECT expense_type_id FROM ".$table." WHERE id = '".$id."' LIMIT 1)");

                        $updateFields = ($type == "Time") ? "descr='".$descr."',rate='".$rate."',time_spent='".$amount."',"
                                                        : (($expenseType == "Driving") ? "descr='".$descr."',rate='".$rate."',kilometers='".$amount."',expense='".$cost."',"
                                                                                        : "descr='".$descr."',rate='".$rate."',total='".$amount."',expense='".$cost."',");
                        $updateTable = ($type == "Time") ? "TimeSheet" : "ExpenseSheet";

                        if ($status > 0)        {
                            $insert = q("INSERT INTO ".$auditTable." (id_approved".$auditFields.",date_changed) SELECT ".$id."".$auditFields.",'".date("Y-m-d H:i:s")."' FROM ".$table." WHERE id = '".$id."'");

                            if ($isPM == 1 && $isSPM == 1)  {
                                $status1 = ($oldStatus1 < $status) ? $status : $oldStatus1;
                                $status2 = ($oldStatus2 < $status) ? $status : $oldStatus2;
                                $update = q("UPDATE ".$table." SET ".$updateFields."status='".$status1."',status2='".$status2."' WHERE id = '".$id."'");
                            }
                            else if ($isPM == 1)    {
                                $status1 = ($oldStatus1 < $status) ? $status : $oldStatus1;
                                $update = q("UPDATE ".$table." SET ".$updateFields."status='".$status1."' WHERE id = '".$id."'");
                            }
                            else if ($isSPM == 1)   {
                                $status2 = ($oldStatus2 < $status) ? $status : $oldStatus2;
                                $update = q("UPDATE ".$table." SET ".$updateFields."status2='".$status2."' WHERE id = '".$id."'");
                            }

                            if ($update)        $update = q("UPDATE ".$updateTable." SET locked = '1' WHERE id = '".$recordInfo[0][3]."'");
                        }
                    }
                }
            }
/*
            if (is_array($companyArr))  {
                foreach ($companyArr as $c)     {
                    if ($counterArr[$c] >= 1)   {
                        $pms = q("SELECT e.frstname, e.lstname, e.email FROM (Project_User AS pu INNER JOIN Employee AS e ON pu.user_id = e.id)
                                    WHERE pu.project_id = '".$projectID."' AND pu.company_id = '".$c."' AND pu.manager = '1'");

                        if (is_array($pms)) {
                            foreach ($pms as $pm) {
                                $usr = "".$pm[0]." ".$pm[1]." <".$pm[2].">";

                                if ($usr == "Johan Coertzen <jc@ite.co.za>")            $recipients .= ",Johan Coertzen <jc@integrityengineering.co.za>";
                                else if ($usr == "Franszo Faul <ffaul@ite.co.za>")      $recipients .= ",Franszo Faul <ffaul@integrityengineering.co.za>";
                                else if ($usr == "Mauritz Kruger <mkruger@ite.co.za>")  $recipients .= ",Mauritz Kruger <mkruger@integrityengineering.co.za>";
                                else                                                    $recipients .= ",".$usr;
                            }
                        }

                        if ($recipients[0] == ",")      $recipients = substr($recipients, 1);

                        //approvalsDeclined(q("SELECT name FROM Project WHERE id = '".$projectID."'"), q("SELECT name FROM Company WHERE id = '".$c."'"), $recipients);
                    }
                }
            }
*/

            header("Location: _module_handler.php?module=approval&what=display");
        }
    }

    //  Get List Of Projects With Unapproved Time
    function getList()  {
        $level = $_GET["type"];

        $where = "";

        if ($level == "CP")     {
            $projectType = "all";

            if (isset($_POST["projectType"]))   $projectType = addslashes(strip_tags($_POST["projectType"]));
        }
    ?>
        <table width="100%">
            <tr>
                <td class="centerdata">
                    <form action="" method="post" name="getDisplay">
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Project Type:
                                </td>
                                <td class="on-description-left" width="50%">
                                    <input method="post" name="projectType" type="radio" value="all" onClick="submit();" <?php echo ($projectType == "all") ? "checked" : ""; ?>><a>All Projects</a><br/>
                                    <input method="post" name="projectType" type="radio" value="invoiceable" onClick="submit();" <?php echo ($projectType == "invoiceable") ? "checked" : ""; ?>><a>Invoiceable Projects</a><br/>
                                    <input method="post" name="projectType" type="radio" value="non_invoicable" onClick="submit();" <?php echo ($projectType == "non_invoicable") ? "checked" : ""; ?>><a>Non-Invoicable Projects</a><br/>
                                </td>
                            </tr>
                        </table>
                        <?php
                            $types = array("Time","Expenses");

                            if (is_array($types))       {
                                foreach ($types as $type)       {
                                    $where = "";

                                    ?>
                                    <br/>
                                    <table width="100%">
                                        <tr>
                                            <td class="centerdata">
                                                <h6>Project(s) with Unapproved <?php echo $type; ?></h6>
                                            </td>
                                        </tr>
                                    </table>
                                    <br/>
                                    <?php
                                        if ($level == "CP")
                                            $where .= ($projectType == "all") ? "" : (($projectType == "invoiceable") ? "AND cot.total_budget > 0 " : "AND cot.total_budget = '' ");

                                        if ($level == "SP")     $where .= "AND p.p_type = 'SP' AND pu.spm_manager = '1' ";
                                        //if ($level == "CP")     $where .= "AND p.p_type = 'CP' ";

                                        $projects = q("SELECT DISTINCT(p.id), p.name, p_type FROM (Project AS p
                                                        INNER JOIN Project_User AS pu ON p.id = pu.project_id
                                                        INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id)
                                                        WHERE pu.user_id = '".$_SESSION["user_id"]."'
                                                        AND cot.company_id = '".$_SESSION["company_id"]."'
                                                        AND pu.company_id = '".$_SESSION["company_id"]."'
                                                        AND p.completed = '0'
                                                        AND p.deleted = '0'
                                                        $where ORDER BY UPPER(p.name)");

                                        if (is_array($projects))    {
                                            unset ($tmpData);

                                            $where = "";

                                            foreach ($projects as $project) {
                                                $where = ($level == "CP") ? "AND company_id = '".$_SESSION["company_id"]."'" : "";
                                                $table = ($type == "Time") ? "ApprovedTime" : "ApprovedExpense";

                                                $hasUnapproved = ($project[2] == "CP") ? q("SELECT * FROM ".$table." WHERE project_id = '".$project[0]."' AND status NOT IN ('2','3') $where")
                                                                                        : q("SELECT * FROM ".$table." WHERE project_id = '".$project[0]."' AND (status NOT IN ('2','3') OR status2 NOT IN ('2','3')) $where");

                                                if (is_array($hasUnapproved))   $tmpData[] = $project;
                                            }

                                            $projects = $tmpData;
                                        }
                                    ?>
                                    <table class="on-table-center on-table">
                                        <tr>
                                            <th>Project Name</th>
                                        </tr>
                                        <?php
                                        if (is_array($projects))        {
                                            foreach ($projects as $project)     {
                                            ?>
                                                <tr>
                                                    <td>
                                                        <a href="_module_handler.php?module=approval&what=display&type=<?php echo $level."&project_id=".$project[0]; ?>"><?php echo $project[1]; ?></a>
                                                    </td>
                                                </tr>
                                            <?php
                                            }
                                        } else  {
                                        ?>
                                            <tr>
                                                <td class="tableBodyCenter">
                                                    No projects with unapproved <?php echo strtolower($type); ?>
                                                </td>
                                            </tr>
                                        <?php
                                        }
                                    ?>
                                    </table>
                                    <?php
                                }
                            }
                            ?>
                    </form>
                </td>
            </tr>
        </table>
    <?php
    }

    //  Get Time Approval Information
    function getDisplay()       {
        getJQeury();
    ?>
        <script language="JavaScript">
            function check()    {
                document.forms["getInformation"].update.value = 1;
                document.forms["getInformation"].submit();
            }

            function display()  {
                var valid = 1;

                //  Check That Project Is Selected
                if (document.forms["getDisplay"].project.value == "null")   {
                    ShowLayer("projectDiv", "block");
                    valid = 0;
                }
                else
                    ShowLayer("projectDiv", "none");

                //  Check That Date From Is Entered
                if (document.forms["getDisplay"].date_from.value == "") {
                    ShowLayer("dateFrom", "block");
                    valid = 0;
                }
                //  Check That Entered Date From Is Valid
                else    {
                    ShowLayer("dateFrom", "none");

                    if (!validation("date", document.forms["getDisplay"].date_from.value))  {
                        ShowLayer("dateFromDiv", "block");
                        valid = 0;
                    }
                    else
                        ShowLayer("dateFromDiv", "none");
                }

                //  Check That Date To Is Entered
                if (document.forms["getDisplay"].date_to.value == "")   {
                    ShowLayer("dateTo", "block");
                    valid = 0;
                }
                //  Check That Entered Date To Is Valid
                else    {
                    ShowLayer("dateTo", "none");

                    if (!validation("date", document.forms["getDisplay"].date_to.value))    {
                        ShowLayer("dateToDiv", "block");
                        valid = 0;
                    }
                    else
                        ShowLayer("dateToDiv", "none");
                }

                if (valid == 1) {
                    document.forms["getDisplay"].save.value = 1;
                    document.forms["getDisplay"].submit();
                }
            }

            function recalculateCost()  {
                var nRecords = document.forms["getInformation"].Time.value;
                var total_time = 0;
                var total_cost = 0;

                for (var i = 1; i < nRecords; i++) {
                    var rate = "C" + i;
                    var time = "D" + i;
                    var cost = "E" + i;

                    var new_value = eval("document.forms['getInformation']."+rate+".value * document.forms['getInformation']."+time+".value;");

                    eval("document.forms['getInformation']."+time+".value = parseFloat(document.forms['getInformation']."+time+".value).toFixed(2);");
                    eval("document.forms['getInformation']."+cost+".value = new_value.toFixed(2);");

                    total_time += parseFloat(eval("document.forms['getInformation']."+time+".value;"));
                    total_cost += parseFloat(eval("document.forms['getInformation']."+cost+".value;"));
                }

                document.forms["getInformation"].time.value = total_time.toFixed(2);
                document.forms["getInformation"].cost.value = total_cost.toFixed(2);
            }

            function recalculateTime()  {
                var nRecords = document.forms["getInformation"].Time.value;
                var total_time = 0;
                var total_cost = 0;

                for (var i = 1; i < nRecords; i++) {
                    var rate = "C" + i;
                    var time = "D" + i;
                    var cost = "E" + i;

                    if (eval("document.forms['getInformation']."+cost+".value") == 0 || eval("document.forms['getInformation']."+cost+".value") == "")  {
                        eval("document.forms['getInformation']."+time+".value = 0.00;");
                        eval("document.forms['getInformation']."+cost+".value = 0.00;");
                    }
                    else    {
                        var new_value = eval("document.forms['getInformation']."+cost+".value / document.forms['getInformation']."+rate+".value;");

                        eval("document.forms['getInformation']."+cost+".value = parseFloat(document.forms['getInformation']."+cost+".value).toFixed(2);");
                        eval("document.forms['getInformation']."+time+".value = new_value.toFixed(2);");
                    }

                    total_time += parseFloat(eval("document.forms['getInformation']."+time+".value;"));
                    total_cost += parseFloat(eval("document.forms['getInformation']."+cost+".value;"));
                }

                document.forms["getInformation"].time.value = total_time.toFixed(2);
                document.forms["getInformation"].cost.value = total_cost.toFixed(2);
            }

            function recalculateExpense()   {
                var nRecords = document.forms["getInformation"].Expenses.value;
                var total_expense = 0;

                for (var i = 1; i < nRecords; i++) {
                    var rate = "J" + i;
                    var distance = "K" + i;
                    var expense = "L" + i;

                    if (eval("document.forms['getInformation']."+rate+".value") != "-" && eval("document.forms['getInformation']."+distance+".value") != "-")   {
                        var new_value = eval("document.forms['getInformation']."+rate+".value * document.forms['getInformation']."+distance+".value;");

                        eval("document.forms['getInformation']."+distance+".value = parseFloat(document.forms['getInformation']."+distance+".value).toFixed(2);");
                        eval("document.forms['getInformation']."+expense+".value = new_value.toFixed(2);");

                        total_expense += parseFloat(eval("document.forms['getInformation']."+expense+".value;"));
                    } else if (eval("document.forms['getInformation']."+rate+".value") != "-")      {
                        var new_value = parseFloat(eval("document.forms['getInformation']."+rate+".value;"));

                        eval("document.forms['getInformation']."+rate+".value = new_value.toFixed(2);");
                        eval("document.forms['getInformation']."+expense+".value = new_value.toFixed(2);");

                        total_expense += parseFloat(eval("document.forms['getInformation']."+expense+".value;"));
                   }
                }

                document.forms["getInformation"].expenseTotal.value = total_expense.toFixed(2);
            }

            function recalculateRate()  {
                var nRecords = document.forms["getInformation"].Expenses.value;
                var total_expense = 0;

                for (var i = 1; i < nRecords; i++) {
                    var rate = "J" + i;
                    var distance = "K" + i;
                    var expense = "L" + i;

                    if (eval("document.forms['getInformation']."+expense+".value") == 0 || eval("document.forms['getInformation']."+expense+".value") == "")    {
                        eval("document.forms['getInformation']."+distance+".value = 0.00;");
                        eval("document.forms['getInformation']."+expense+".value = 0.00;");
                    }
                    else    {
                        if (eval("document.forms['getInformation']."+rate+".value") != "-") {
                            var new_value = eval("document.forms['getInformation']."+expense+".value / document.forms['getInformation']."+distance+".value;");

                            eval("document.forms['getInformation']."+expense+".value = parseFloat(document.forms['getInformation']."+expense+".value).toFixed(2);");
                            eval("document.forms['getInformation']."+rate+".value = new_value.toFixed(2);");
                        }
                    }

                    total_expense += parseFloat(eval("document.forms['getInformation']."+expense+".value;"));
                }

                document.forms["getInformation"].expenseTotal.value = total_expense.toFixed(2);
            }

            function selection(check_value) {
                var nRecords = document.forms["getInformation"].Time.value;

                for (var i = 1; i < nRecords; i++) {
                    var box = "F" + i;
                    var box2 = "G" + i;

                    eval("document.forms['getInformation']."+box+".checked = "+check_value);
                    eval("document.forms['getInformation']."+box2+".checked = false");
                }
            }

            function selection2(check_value)    {
                var nRecords = document.forms["getInformation"].Expenses.value;

                for (var i = 1; i < nRecords; i++) {
                    var box = "M" + i;
                    var box2 = "N" + i;

                    eval("document.forms['getInformation']."+box+".checked = "+check_value);
                    eval("document.forms['getInformation']."+box2+".checked = "+"false");
                }
            }

            ///// -- Check/Uncheck -- /////
            function myTimeCheck(boxName){
               jQuery("#G"+boxName).attr("checked",false);
            }

            function myTimeCheck2(boxName){
               jQuery("#F"+boxName).attr("checked",false);
            }

            function myExCheck(boxName){
               jQuery("#N"+boxName).attr("checked",false);
               //alert(jQuery("#M"+boxName));
            }

            function myExCheck2(boxName){
               jQuery("#M"+boxName).attr("checked",false);
               //alert(jQuery("#N"+boxName));
            }

        </script>
        <?php
        global $isSharedProject;
        global $isSPM;

        $today = date("Y-m-d");
        $level = $_GET["type"];
        $companyID = "null";

        $where = "";

        if ($level == "SP")     $where .= "AND p.p_type = 'SP' AND pu.spm_manager = '1' ";
        //if ($level == "CP")     $where .= "AND p.p_type = 'CP' ";

        $projects = q("SELECT DISTINCT(p.id), p.name, p_type FROM (Project AS p
                        INNER JOIN Project_User AS pu ON p.id = pu.project_id
                        INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id)
                        WHERE pu.user_id = '".$_SESSION["user_id"]."'
                        AND cot.company_id = '".$_SESSION["company_id"]."'
                        AND pu.company_id = '".$_SESSION["company_id"]."'
                        AND p.completed = '0'
                        AND p.deleted = '0'
                        $where ORDER BY UPPER(p.name)");

        if ($_GET["project_id"] != "")  $projectID = $_GET["project_id"];

        if (isset($_POST["project"]) && $_POST["project"] != "null")    $projectID = $_POST["project"];
        if (isset($_POST["company"]) && $_POST["company"] != "null")    $companyID = $_POST["company"];

        if (is_numeric($projectID))     $companies = q("SELECT DISTINCT(c.id), c.name FROM (Company AS c INNER JOIN companiesOnTeam AS cot ON c.id = cot.company_id
                                                        INNER JOIN Project AS p ON p.id =  cot.project_id) WHERE cot.project_id = '".$projectID."' ORDER BY c.name");

        $dateFrom = currentWeekStart();
        $dateTo = $today;
        $date_type = $_POST["dateType"];
        ?>
        <table width="100%">
            <tr>
                <td class="centerdata">
                    <form action="" method="post" id="getDisplay" name="getDisplay">
                    <?php
                    //  Get Dates
                    if ($date_type == "currentwk")      {
                        $dateFrom = currentWeekStart();
                        $dateTo = $today;
                    }
                    else if ($date_type == "previouswk")        {
                        $dateFrom = getDates(currentWeekStart(), -7);
                        $dateTo = getDates(currentWeekStart(), -1);
                    }
                    else if ($date_type == "currentmnth")       {
                        $dateFrom = getMonthStart($today);
                        $dateTo = $today;
                    }
                    else if ($date_type == "previousmnth")      {
                        $dateFrom = getPreviousMonthStart();
                        $dateTo = getMonthEnd(getPreviousMonthStart());
                    }
                    else if ($date_type == "projectLifetime")      {
                        $dateFromT = q("SELECT MIN(date) FROM TimeSheet WHERE project_id = '".$project_id."'");
                        $dateFromE = q("SELECT MIN(date) FROM ExpenseSheet WHERE project_id = '".$project_id."'");
                        $dateFrom = ($dateFromT <= $dateFromE) ? $dateFromT : $dateFromE;
                        $dateTo = $today;
                    }
                    else if ($date_type == "custom")    {
                        $dateFrom = addslashes(strip_tags($_POST["date_from"]));
                        $dateTo = addslashes(strip_tags($_POST["date_to"]));
                    }
                    ?>
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Approve Time/Expense Sheet(s)</h6>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                Select Project:
                            </td>
                            <td  width="50%">
                                <select class="on-field" method="post" id="project" name="project" class="required" tabindex="1" <?php if ($level == "SP") echo "onChange=\"getCompanies(project);\""; ?>>
                                    <option value="null">--  Select Project  --</option>
                                    <?php
                                        if (is_array($projects))        {
                                            foreach ($projects as $project)     {
                                            ?>
                                                <option value="<?php echo $project[0]; ?>" <?php echo ($projectID == $project[0]) ? "selected" : ""; ?>><?php echo $project[1]; ?></option>";
                                            <?php
                                            }
                                        }
                                    ?>
                                </select>
                                <div id="projectDiv" style="display: none;"><font class="on-validate-error">* Project must be selected</font></div>
                            </td>
                        </tr>
                    </table>
                    <?php
                        if ($level == "SP")     {
                    ?>
                    <div id="extraDiv" style="display:<?php echo ($projectID != "") ? "block" : "none"; ?>;">
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Company:
                                </td>
                                <td  width="50%">
                                    <select class="on-field" method="post" id="company" name="company">
                                        <option value="null">All Companies</option>
                                        <?php
                                            if (is_array($companies))        {
                                                foreach ($companies as $company)     {
                                                ?>
                                                    <option value="<?php echo $company[0]; ?>" <?php echo ($companyID == $company[0]) ? "selected" : ""; ?>><?php echo $company[1]; ?></option>";
                                                <?php
                                                }
                                            }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <?php
                        }
                    ?>
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                Date Type(s):
                            </td>
                            <td  width="50%">
                                <select class="on-field" method="post" id="dateType" name="dateType">
                                    <option value="currentwk" <?php echo ($date_type == "currentwk") ? "selected" : ""; ?>>Current Week</option>
                                    <option value="previouswk" <?php echo ($date_type == "previouswk") ? "selected" : ""; ?>>Previous Week</option>
                                    <option value="currentmnth" <?php echo ($date_type == "currentmnth") ? "selected" : ""; ?>>Current Month</option>
                                    <option value="previousmnth" <?php echo ($date_type == "previousmnth") ? "selected" : ""; ?>>Previous Month</option>
                                    <option value="projectLifetime" <?php echo ($date_type == "projectLifetime") ? "selected" : ""; ?>>Project Lifetime</option>
                                    <option value="custom" <?php echo ($date_type == "custom") ? "selected" : ""; ?>>Custom Dates</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                    <div id="datesDiv" style="display:<?php echo ($date_type == "custom") ? "block" : "none"; ?>;">
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Date From:
                                </td>
                                <td  width="50%">
                                    <input id="date_from" name="date_from" class="on-field-date date" type="text" style="text-align:right;" value="<?php echo $dateFrom; ?>">
                                    <div id="dateFrom" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                    <div id="dateFromDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo date("Y-m-d"); ?></font></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Date To:
                                </td>
                                <td  width="50%">
                                    <input id="date_to" name="date_to" class="on-field-date date" type="text" style="text-align:right;" value="<?php echo $dateTo; ?>">
                                    <div id="dateTo" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                    <div id="dateToDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo date("Y-m-d"); ?></font></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <br/>
                    <input name="btnDisplay" onClick="display();" tabindex="2" type="button" value="Display Information">
                    <input method="post" name="save" type="hidden" value="0" />
                    </form>
                    <?php
                    //  If Display Button Is Pressed
                    if ((isset($_POST["save"]) && $_POST["save"] === "1") || $projectID != "")  {
                    ?>
                        <form action="" method="post" id="getInformation" name="getInformation">
                        <?php
                            if (isset($_POST["project"]) && $_POST["project"] != "null")        $projectID = $_POST["project"];
                            if (isset($_POST["company"]) && $_POST["company"] != "null")        $companyID = $_POST["company"];

                            if ($companyID == "")       $companyID = "null";

                            $projectType = q("SELECT p_type FROM Project WHERE id = '".$projectID."'");

                            $isSharedProject = ($projectType == "CP") ? 0 : 1;
                            $isPM = q("SELECT manager FROM Project_User WHERE user_id = '".$_SESSION["user_id"]."' AND project_id = '".$projectID."' AND company_id = '".$_SESSION["company_id"]."'");
                            $isSPM = q("SELECT spm_manager FROM Project_User WHERE user_id = '".$_SESSION["user_id"]."' AND project_id = '".$projectID."' AND company_id = '".$_SESSION["company_id"]."'");

                            $types = array("Time","Expenses");

                            if (is_array($types))       {
                                foreach ($types as $type)       {
                                    $where = "";
                                    $hasAreas = 0;

                                    if ($type == "Time")        $where .= ($isSharedProject == 1) ? "AND (ts.status != '2' OR ts.status2 != '2') AND ts.status2 != '3' " : "AND ts.status NOT IN ('2','3') ";
                                    if ($type == "Expenses")    $where .= ($isSharedProject == 1) ? "AND (es.status != '2' OR es.status2 != '2') AND es.status2 != '3' " : "AND es.status NOT IN ('2','3') ";

                                    if ($companyID != "null")   $where .= "AND c.id = '".$companyID."' ";

                                    if ($level == "CP") $where .= "AND c.id = '".$_SESSION["company_id"]."' ";

                                    if ($type == "Time")        {
                                        $areas  = q("SELECT ts.project_id,ts.area_id
                                                        FROM (ApprovedTime AS ts
                                                            INNER JOIN Employee AS e ON ts.user_id = e.id
                                                            INNER JOIN Activities AS a ON ts.activity_id = a.id
                                                            INNER JOIN ActivityTypes AS at ON a.parent_id = at.id
                                                            INNER JOIN Project AS p ON ts.project_id = p.id
                                                            INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
                                                            INNER JOIN Company AS c ON cot.company_id = c.id
                                                            LEFT JOIN areas AS ar ON ts.area_id = ar.id)
                                                        WHERE ts.date >= '".$dateFrom."' AND ts.date <= '".$dateTo."' AND ts.project_id = '".$projectID."' AND ts.area_id > 0 AND ts.company_id = c.id $where");

                                        if (is_array($areas))   $hasAreas = 1;

                                        $select = "";

                                        if ($hasAreas)  {
                                            if ($isSharedProject == 1)  {
                                                $select .= "c.name,CONCAT(e.lstname, ', ', e.frstname),ar.name,at.type,a.name,ts.descr,ts.status,ts.status2,ts.date,ts.time_spent,ts.rate,ts.time_spent * ts.rate,1,1";
                                                $headings[$type] = array("ID","Company Name","Employee Name","Area","Activity Type","Activity","Description","PM Status","SPM Status","Date","Time Spent",
                                                                            "Rate","Cost <i>(".$_SESSION["currency"].")</i>","Approve","Decline");
                                            }
                                            else        {
                                                $select .= "c.name,CONCAT(e.lstname, ', ', e.frstname),ar.name,at.type,a.name,ts.descr,ts.status,ts.date,ts.time_spent,ts.rate,ts.time_spent * ts.rate,1,1";
                                                $headings[$type] = array("ID","Company Name","Employee Name","Area","Activity Type","Activity","Description","PM Status","Date","Time Spent",
                                                                            "Rate","Cost <i>(".$_SESSION["currency"].")</i>","Approve","Decline");
                                            }
                                        } else  {
                                            if ($isSharedProject == 1)  {
                                                $select .= "c.name,CONCAT(e.lstname, ', ', e.frstname),at.type,a.name,ts.descr,ts.status,ts.status2,ts.date,ts.time_spent,ts.rate,ts.time_spent * ts.rate,1,1";
                                                $headings[$type] = array("ID","Company Name","Employee Name","Activity Type","Activity","Description","PM Status","SPM Status","Date","Time Spent",
                                                                            "Rate","Cost <i>(".$_SESSION["currency"].")</i>","Approve","Decline");
                                            }
                                            else        {
                                                $select .= "c.name,CONCAT(e.lstname, ', ', e.frstname),at.type,a.name,ts.descr,ts.status,ts.date,ts.time_spent,ts.rate,ts.time_spent * ts.rate,1,1";
                                                $headings[$type] = array("ID","Company Name","Employee Name","Activity Type","Activity","Description","PM Status","Date","Time Spent",
                                                                            "Rate","Cost <i>(".$_SESSION["currency"].")</i>","Approve","Decline");
                                            }
                                        }

                                        $timeData = q("SELECT ts.id,$select
                                                        FROM (ApprovedTime AS ts
                                                            INNER JOIN Employee AS e ON ts.user_id = e.id
                                                            INNER JOIN Activities AS a ON ts.activity_id = a.id
                                                            INNER JOIN ActivityTypes AS at ON a.parent_id = at.id
                                                            INNER JOIN Project AS p ON ts.project_id = p.id
                                                            INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
                                                            INNER JOIN Company AS c ON cot.company_id = c.id
                                                            LEFT JOIN areas AS ar ON ts.area_id = ar.id )
                                                        WHERE ts.date >= '".$dateFrom."' AND ts.date <= '".$dateTo."' AND ts.project_id = '".$projectID."' AND ts.company_id = c.id $where
                                                        ORDER BY c.name, ts.date, ts.on_date, ts.on_time, ar.name");
                                    } else      {
                                        $areas  = q("SELECT es.project_id,es.area_id
                                                        FROM (ApprovedExpense AS es
                                                            INNER JOIN Employee AS e ON es.user_id = e.id
                                                            INNER JOIN dropdowns AS dd ON es.expense_type_id = dd.id
                                                            LEFT JOIN Activities AS a ON es.activity_id = a.id
                                                            LEFT JOIN ActivityTypes AS at ON a.parent_id = at.id
                                                            INNER JOIN Project AS p ON es.project_id = p.id
                                                            INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
                                                            INNER JOIN Company AS c ON cot.company_id = c.id
                                                            LEFT JOIN areas AS ar ON es.area_id = ar.id
                                                            LEFT JOIN Vehicle AS v ON es.vehicle_id = v.id
                                                            LEFT JOIN Disbursements AS d ON es.disbursement_id = d.id
                                                            LEFT JOIN DisbursementTypes AS dt ON dt.id = d.parent_id)
                                                        WHERE es.date >= '".$dateFrom."' AND es.date <= '".$dateTo."' AND es.project_id = '".$projectID."' AND es.area_id > 0 AND es.company_id = c.id $where");

                                        if (is_array($areas))   $hasAreas = 1;

                                        $select = "";

                                        if ($hasAreas)  {
                                            if ($isSharedProject == 1)  {
                                                $select .= "c.name,CONCAT(e.lstname, ', ', e.frstname),dd.name,IF(dd.name = 'Driving',v.type,IF(dd.name = 'Printing',CONCAT(dt.type,': ',d.name),es.otherDescr)),ar.name,at.type,".
                                                            "a.name,es.descr,es.status,es.status2,es.date,IF(dd.name = 'Driving',es.kilometers,es.total), es.rate, es.expense,1,1";
                                                $headings[$type] = array("ID","Company Name","Employee Name","Expense Type","Expense Info","Area","Activity Type","Activity","Description","PM Status","SPM Status","Date","Amount",
                                                                            "Rate","Cost <i>(".$_SESSION["currency"].")</i>","Approve","Decline");
                                            }
                                            else        {
                                                $select .= "c.name,CONCAT(e.lstname, ', ', e.frstname),dd.name,IF(dd.name = 'Driving',v.type,IF(dd.name = 'Printing',CONCAT(dt.type,': ',d.name),es.otherDescr)),ar.name,at.type,".
                                                            "a.name,es.descr,es.status,es.date,IF(dd.name = 'Driving',es.kilometers,es.total), es.rate, es.expense,1,1";
                                                $headings[$type] = array("ID","Company Name","Employee Name","Expense Type","Expense Info","Area","Activity Type","Activity","Description","PM Status","Date","Amount",
                                                                            "Rate","Cost <i>(".$_SESSION["currency"].")</i>","Approve","Decline");
                                            }
                                        } else  {
                                            if ($isSharedProject == 1)  {
                                                $select .= "c.name,CONCAT(e.lstname, ', ', e.frstname),dd.name,IF(dd.name = 'Driving',v.type,IF(dd.name = 'Printing',CONCAT(dt.type,': ',d.name),es.otherDescr)),at.type,".
                                                            "a.name,es.descr,es.status,es.status2,es.date,IF(dd.name = 'Driving',es.kilometers,es.total), es.rate, es.expense,1,1";
                                                $headings[$type] = array("ID","Company Name","Employee Name","Expense Type","Expense Info","Activity Type","Activity","Description","PM Status","SPM Status","Date","Amount",
                                                                            "Rate","Cost <i>(".$_SESSION["currency"].")</i>","Approve","Decline");
                                            }
                                            else        {
                                                $select .= "c.name,CONCAT(e.lstname, ', ', e.frstname),dd.name,IF(dd.name = 'Driving',v.type,IF(dd.name = 'Printing',CONCAT(dt.type,': ',d.name),es.otherDescr)),at.type,".
                                                            "a.name,es.descr,es.status,es.date,IF(dd.name = 'Driving',es.kilometers,es.total), es.rate, es.expense,1,1";
                                                $headings[$type] = array("ID","Company Name","Employee Name","Expense Type","Expense Info","Activity Type","Activity","Description","PM Status","Date","Amount",
                                                                            "Rate","Cost <i>(".$_SESSION["currency"].")</i>","Approve","Decline");
                                            }
                                        }

                                        $expenseData = q("SELECT es.id,$select
                                                            FROM (ApprovedExpense AS es
                                                                INNER JOIN Employee AS e ON es.user_id = e.id
                                                                INNER JOIN dropdowns AS dd ON es.expense_type_id = dd.id
                                                                LEFT JOIN Activities AS a ON es.activity_id = a.id
                                                                LEFT JOIN ActivityTypes AS at ON a.parent_id = at.id
                                                                INNER JOIN Project AS p ON es.project_id = p.id
                                                                INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
                                                                INNER JOIN Company AS c ON cot.company_id = c.id
                                                                LEFT JOIN areas AS ar ON es.area_id = ar.id
                                                                LEFT JOIN Vehicle AS v ON es.vehicle_id = v.id
                                                                LEFT JOIN Disbursements AS d ON es.disbursement_id = d.id
                                                                LEFT JOIN DisbursementTypes AS dt ON dt.id = d.parent_id)
                                                            WHERE es.date >= '".$dateFrom."' AND es.date <= '".$dateTo."' AND es.project_id = '".$projectID."' AND es.company_id = c.id $where
                                                            ORDER BY c.name, es.date, es.on_date, es.on_time, ar.name");
                                    }
                                }
                            }

                            if (is_array($timeData) || is_array($expenseData))    {
                                $status = array("<font class='on-validate-error'>Unapproved</font>","Updated","Approved","Declined");

                                $timeColumns = count($headings["Time"]) - 2;
                                $expenseColumns = count($headings["Expenses"]) - 2;

                                $columns = (!is_array($expenseData)) ? $timeColumns : $expenseColumns;

                                if (is_array($types))   {
                                    echo "<div class='on-20px'>";
                                        echo "<table class='on-table on-table-center'>";

                                        foreach ($types as $type)   {
                                            $displayString = "";

                                            $data = ($type == "Time") ? $timeData : $expenseData;

                                            if (is_array($data))    {
                                                        $displayString .= "<tr>";
                                                            $displayString .= "<td style='text-align:center' class='on-table-clear' colspan='100%'>";
                                                                 $displayString .= "<h6>Unapproved ".$type."</h6>";
                                                            $displayString .= "</td>";
                                                        $displayString .= "</tr>";
                                                        $displayString .= "<tr>";
                                                            $displayString .= "<td colspan='100%' class='rightdata'>";
                                                                $btnSelect = ($type == "Time") ? "selection(true);" : "selection2(true);";
                                                                $btnDeselect = ($type == "Time") ? "selection(false);" : "selection2(false);";
                                                                $displayString .= "<input name='btnSelect' onClick='".$btnSelect."' tabindex='1' type='button' value='Select All'>";
                                                                $displayString .= "<input name='btnDeselect' onClick='".$btnDeselect."' tabindex='1' type='button' value='Deselect All'><br/>";
                                                            $displayString .= "</td>";
                                                        $displayString .= "</tr>";

                                                        //  Headings
                                                        $displayString .= "<tr>";

                                                        foreach ($headings[$type] as $h)    {
                                                            if ($h != "ID") {
                                                                $colspan = 1;

                                                                if ($type == "Time")
                                                                    $colspan = ($h == "Employee Name") ? 3 : 1;

                                                                $displayString .= "<th colspan='".$colspan."' style='white-space:nowrap'>".$h."</th>";
                                                            }
                                                        }

                                                        $displayString .= "</tr>";

                                                        //  Data
                                                        $counter = 1;
                                                        $fields = ($type == "Time") ? array("A","B","D","C","E","F","G") : array("H","I","K","J","L","M","N");

                                                        if ($type == "Time")
                                                            $totalTime = 0;

                                                        $totalCost = 0;

                                                        foreach ($data as $r)       {
                                                            $fieldCount = 0;
                                                            $col = 0;

                                                            $displayString .= "<tr>";

                                                            foreach ($r as $d) {
                                                                $onChange = "";
                                                                $readonly = "";

                                                                $colspan = ($headings[$type][$col] == "Employee Name" && $type == "Time") ? 3 : 1;

                                                                if ($headings[$type][$col] == "Time Spent")                                    $totalTime += $d;
                                                                if ($headings[$type][$col] == "Cost <i>(".$_SESSION["currency"].")</i>")       $totalCost += $d;

                                                                if ($type == "Time")        {
                                                                    if ($headings[$type][$col] == "Rate")   {
                                                                        $onChange = "onChange='recalculateCost();'";
                                                                        $readonly = ($isSharedProject == 1 && $isSPM == 0) ? "readonly" : "";
                                                                    } else if ($headings[$type][$col] == "Time Spent")      {
                                                                        $onChange = "onChange='recalculateCost();'";
                                                                    } else if ($headings[$type][$col] == "Cost <i>(".$_SESSION["currency"].")</i>") {
                                                                        $onChange = "onChange='recalculateCost();'";
                                                                        $readonly = "readonly";
                                                                    } else if ($headings[$type][$col] == "Approve") {
                                                                        $onChange = "onChange='myTimeCheck(".$counter.");'";
                                                                    } else if ($headings[$type][$col] == "Decline") {
                                                                        $onChange = "onChange='myTimeCheck2(".$counter.");'";
                                                                    }
                                                                } else      {
                                                                    if ($headings[$type][$col] == "Rate")   {
                                                                        $onChange = ($d != "") ? "onChange='recalculateExpense();'" : "";
                                                                        $readonly = ($isSharedProject == 1 && $isSPM == 0) ? "readonly" : "";
                                                                        $readonly = ($d == "") ? "readonly" : $readonly;
                                                                    } else if ($headings[$type][$col] == "Amount")      {
                                                                        $onChange = "onChange='recalculateExpense();'";
                                                                        $readonly = ($d == "") ? "readonly" : "";
                                                                    } else if ($headings[$type][$col] == "Cost <i>(".$_SESSION["currency"].")</i>") {
                                                                        $onChange = "onChange='recalculateExpense();'";
                                                                        $readonly = "readonly";
                                                                    } else if ($headings[$type][$col] == "Approve") {
                                                                        $onChange = "onChange='myExCheck(".$counter.");'";
                                                                    } else if ($headings[$type][$col] == "Decline") {
                                                                        $onChange = "onChange='myExCheck2(".$counter.");'";
                                                                    }
                                                                }

                                                                if ($headings[$type][$col] == "ID")
                                                                    $displayString .= "<input id='".$fields[$fieldCount].$counter."' name='".$fields[$fieldCount++].$counter."' $readonly $onChange type='hidden' value='".str_replace("'", "&#39;" , $d)."' />";
                                                                else if ($headings[$type][$col] == "Description")   {
                                                                    $displayString .= "<td style='white-space:nowrap'>";
                                                                        $displayString .= "<input id='".$fields[$fieldCount].$counter."' name='".$fields[$fieldCount++].$counter."' $readonly $onChange type='text' class='descr' value='".str_replace("'", "&#39;" , $d)."' />";
                                                                    $displayString .= "</td>";
                                                                }
                                                                else if ($headings[$type][$col] == "Rate" || $headings[$type][$col] == "Amount" || $headings[$type][$col] == "Time Spent" || $headings[$type][$col] == "Cost <i>(".$_SESSION["currency"].")</i>") {
                                                                    $displayString .= "<td style='white-space:nowrap'>";
                                                                        $displayString .= "<input id='".$fields[$fieldCount].$counter."' name='".$fields[$fieldCount++].$counter."' $readonly $onChange type='text' class='number' value='".(($d != "") ? number_format((double)$d, 2, ".", "") : "-")."' />";
                                                                    $displayString .= "</td>";
                                                                }
                                                                else if ($headings[$type][$col] == "Approve" || $headings[$type][$col] == "Decline")        {
                                                                    $displayString .= "<td class='centerdata' style='white-space:nowrap'>";
                                                                        $displayString .= "<input id='".$fields[$fieldCount].$counter."' name='".$fields[$fieldCount++].$counter."' $readonly $onChange type='checkbox' value='1' />";
                                                                    $displayString .= "</td>";
                                                                }
                                                                else if ($headings[$type][$col] == "PM Status" || $headings[$type][$col] == "SPM Status")
                                                                    $displayString .= "<td colspan='".$colspan."' style='white-space:nowrap'>".$status[$d]."</td>";
                                                                else {
                                                                    if (preg_match("/^((\d{2}(([02468][048])|([13579][26]))[\-\/\s]?((((0?[13578])|(1[02]))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-\/\s]?((0?[1-9])|([1-2][0-9])))))|(\d{2}(([02468][1235679])|([13579][01345789]))[\-\/\s]?((((0?[13578])|(1[02]))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-\/\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))(\s(((0?[1-9])|(1[0-2]))\:([0-5][0-9])((\s)|(\:([0-5][0-9])\s))([AM|PM|am|pm]{2,2})))?$/", $d))
                                                                        $displayString .= "<td colspan='".$colspan."' style='white-space:nowrap'>".$d."</td>";
                                                                    else if (is_numeric($d))
                                                                        $displayString .= "<td colspan='".$colspan."' style='white-space:nowrap' class='rightdata'>".number_format((double)$d, 2, ".", "")."</td>";
                                                                    else if ($d == "")
                                                                        $displayString .= "<td colspan='".$colspan."' style='white-space:nowrap'>-</td>";
                                                                    else
                                                                        $displayString .= "<td colspan='".$colspan."' style='white-space:nowrap'>".$d."</td>";
                                                                }

                                                                $col++;
                                                            }

                                                            $displayString .= "</tr>";
                                                            $counter++;
                                                        }

                                                        $displayString .= "<tr>";
                                                            $colspan = ($type == "Time") ? array_search("Time Spent", $headings[$type]) + 2 : array_search("Cost <i>(".$_SESSION["currency"].")</i>", $headings[$type]);

                                                            $displayString .= "<td class='on-table-total' colspan='".($colspan - 1)."'>Total:</td>";

                                                            if ($type == "Time")    {
                                                                $displayString .= "<td class='on-table-total'>";
                                                                    $displayString .= "<input name='time' class='number' readonly type='text' value='".number_format((double)$totalTime, 2, ".", "")."'>";
                                                                $displayString .= "</td>";
                                                                $displayString .= "<td class='on-table-total'></td>";
                                                                $displayString .= "<td class='on-table-total'>";
                                                                    $displayString .= "<input name='cost' class='number' readonly type='text' value='".number_format((double)$totalCost, 2, ".", "")."'>";
                                                                $displayString .= "</td>";
                                                            } else  {
                                                                $displayString .= "<td class='on-table-total'>";
                                                                    $displayString .= "<input name='expenseTotal' class='number' readonly type='text' value='".number_format((double)$totalCost, 2, ".", "")."'>";
                                                                $displayString .= "</td>";
                                                            }

                                                            $displayString .= "<td class='on-table-total' colspan='2'></td>";
                                                        $displayString .= "</tr>";
                                                        $displayString .= "<input id='".$type."' name='".$type."' type='hidden' value='".$counter."' />";
                                            }

                                            echo $displayString;

                                            if ($type == "Time")        echo "<tr><td style='background-color:transparent' colspan='100%'>&nbsp;</td></tr>";
                                        }

                                            echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                                        echo "</table>";
                                    echo "</div><br/>";
                                }
                                ?>
                            <input id="projectID" name="projectID" type="hidden" value="<?php echo $projectID; ?>" />
                            <input id="update" name="update" type="hidden" value="0" />
                            <input id="btnUpdate" name="btnUpdate" onClick="check();" type="button" value="Update/Approve/Decline Data">
                            <br/><br/><br/>
                            <?php
                            }
                            ?>
                        </form>
                    <?php
                    }
                    ?>
                </td>
            </tr>
        </table>
    <?php
    }

    //  Get Approved Time Information - Report
    function getReport()    {
        getJQeury();
    ?>
        <script language="JavaScript">
            function display()  {
                var valid = 1;

                //  Check That Project Is Selected
                if (document.forms["getDisplay"].project.value == "null")   {
                    ShowLayer("projectDiv", "block");
                    valid = 0;
                }
                else
                    ShowLayer("projectDiv", "none");

                //  Check That Date From Is Entered
                if (document.forms["getDisplay"].date_from.value == "") {
                    ShowLayer("dateFrom", "block");
                    valid = 0;
                }
                //  Check That Entered Date From Is Valid
                else    {
                    ShowLayer("dateFrom", "none");

                    if (!validation("date", document.forms["getDisplay"].date_from.value))  {
                        ShowLayer("dateFromDiv", "block");
                        valid = 0;
                    }
                    else
                        ShowLayer("dateFromDiv", "none");
                }

                //  Check That Date To Is Entered
                if (document.forms["getDisplay"].date_to.value == "")   {
                    ShowLayer("dateTo", "block");
                    valid = 0;
                }
                //  Check That Entered Date To Is Valid
                else    {
                    ShowLayer("dateTo", "none");

                    if (!validation("date", document.forms["getDisplay"].date_to.value))    {
                        ShowLayer("dateToDiv", "block");
                        valid = 0;
                    }
                    else
                        ShowLayer("dateToDiv", "none");
                }

                if (valid == 1) {
                    document.forms["getDisplay"].save.value = 1;
                    document.forms["getDisplay"].submit();
                }
            }
        </script>
        <?php
        global $isSharedProject;
        global $isSPM;

        $today = date("Y-m-d");
        $level = $_GET["type"];
        $companyID = "null";

        $where = "";

        if ($level == "SP")     $where .= "AND p.p_type = 'SP' AND pu.spm_manager = '1' ";
        //if ($level == "CP")     $where .= "AND p.p_type = 'CP' ";

        $projects = q("SELECT DISTINCT(p.id), p.name, p_type FROM (Project AS p
                        INNER JOIN Project_User AS pu ON p.id = pu.project_id
                        INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id)
                        WHERE pu.user_id = '".$_SESSION["user_id"]."'
                        AND cot.company_id = '".$_SESSION["company_id"]."'
                        AND pu.company_id = '".$_SESSION["company_id"]."'
                        AND p.completed = '0'
                        AND p.deleted = '0'
                        $where ORDER BY UPPER(p.name)");

        if ($_GET["project_id"] != "")  $projectID = $_GET["project_id"];

        if (isset($_POST["project"]) && $_POST["project"] != "null")    $projectID = $_POST["project"];
        if (isset($_POST["company"]) && $_POST["company"] != "null")    $companyID = $_POST["company"];

        if (is_numeric($projectID))     $companies = q("SELECT DISTINCT(c.id), c.name FROM (Company AS c INNER JOIN companiesOnTeam AS cot ON c.id = cot.company_id
                                                        INNER JOIN Project AS p ON p.id =  cot.project_id) WHERE cot.project_id = '".$projectID."' ORDER BY c.name");

        $dateFrom = currentWeekStart();
        $dateTo = $today;
        $date_type = $_POST["dateType"];
        $status = $_POST["status"];
        ?>
        <table width="100%">
            <tr>
                <td class="centerdata">
                    <form action="" method="post" id="getDisplay" name="getDisplay">
                    <?php
                    //  Get Dates
                    if ($date_type == "currentwk")      {
                        $dateFrom = currentWeekStart();
                        $dateTo = $today;
                    }
                    else if ($date_type == "previouswk")        {
                        $dateFrom = getDates(currentWeekStart(), -7);
                        $dateTo = getDates(currentWeekStart(), -1);
                    }
                    else if ($date_type == "currentmnth")       {
                        $dateFrom = getMonthStart($today);
                        $dateTo = $today;
                    }
                    else if ($date_type == "previousmnth")      {
                        $dateFrom = getPreviousMonthStart();
                        $dateTo = getMonthEnd(getPreviousMonthStart());
                    }
                    else if ($date_type == "projectLifetime")      {
                        $dateFromT = q("SELECT MIN(date) FROM TimeSheet WHERE project_id = '".$project_id."'");
                        $dateFromE = q("SELECT MIN(date) FROM ExpenseSheet WHERE project_id = '".$project_id."'");
                        $dateFrom = ($dateFromT <= $dateFromE) ? $dateFromT : $dateFromE;
                        $dateTo = $today;
                    }
                    else if ($date_type == "custom")    {
                        $dateFrom = addslashes(strip_tags($_POST["date_from"]));
                        $dateTo = addslashes(strip_tags($_POST["date_to"]));
                    }
                    ?>
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Time/Expense Sheet(s) Report</h6>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                Select Project:
                            </td>
                            <td  width="50%">
                                <select class="on-field" method="post" id="project" name="project" class="required" tabindex="1" <?php if ($level == "SP") echo "onChange=\"getCompanies(project);\""; ?>>
                                    <option value="null">--  Select Project  --</option>
                                    <?php
                                        if (is_array($projects))        {
                                            foreach ($projects as $project)     {
                                            ?>
                                                <option value="<?php echo $project[0]; ?>" <?php echo ($projectID == $project[0]) ? "selected" : ""; ?>><?php echo $project[1]; ?></option>";
                                            <?php
                                            }
                                        }
                                    ?>
                                </select>
                                <div id="projectDiv" style="display: none;"><font class="on-validate-error">* Project must be selected</font></div>
                            </td>
                        </tr>
                    </table>
                    <?php
                        if ($level == "SP")     {
                    ?>
                    <div id="extraDiv" style="display:<?php echo ($projectID != "") ? "block" : "none"; ?>;">
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Company:
                                </td>
                                <td  width="50%">
                                    <select class="on-field" method="post" id="company" name="company">
                                        <option value="null">All Companies</option>
                                        <?php
                                            if (is_array($companies))        {
                                                foreach ($companies as $company)     {
                                                ?>
                                                    <option value="<?php echo $company[0]; ?>" <?php echo ($companyID == $company[0]) ? "selected" : ""; ?>><?php echo $company[1]; ?></option>";
                                                <?php
                                                }
                                            }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <?php
                        }
                    ?>
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                Time/Expense Status:
                            </td>
                            <td  width="50%">
                                <select class="on-field" method="post" id="status" name="status">
                                    <option value="all" <?php echo ($status == "all") ? "selected" : ""; ?>>All</option>
                                    <option value="0" <?php echo ($status == "0") ? "selected" : ""; ?>>Unapproved</option>
                                    <option value="1" <?php echo ($status == "1") ? "selected" : ""; ?>>Updated</option>
                                    <option value="2" <?php echo ($status == "2") ? "selected" : ""; ?>>Approved</option>
                                    <option value="3" <?php echo ($status == "3") ? "selected" : ""; ?>>Declined</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                Date Type(s):
                            </td>
                            <td  width="50%">
                                <select class="on-field" method="post" id="dateType" name="dateType">
                                    <option value="currentwk" <?php echo ($date_type == "currentwk") ? "selected" : ""; ?>>Current Week</option>
                                    <option value="previouswk" <?php echo ($date_type == "previouswk") ? "selected" : ""; ?>>Previous Week</option>
                                    <option value="currentmnth" <?php echo ($date_type == "currentmnth") ? "selected" : ""; ?>>Current Month</option>
                                    <option value="previousmnth" <?php echo ($date_type == "previousmnth") ? "selected" : ""; ?>>Previous Month</option>
                                    <option value="projectLifetime" <?php echo ($date_type == "projectLifetime") ? "selected" : ""; ?>>Project Lifetime</option>
                                    <option value="custom" <?php echo ($date_type == "custom") ? "selected" : ""; ?>>Custom Dates</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                    <div id="datesDiv" style="display:<?php echo ($date_type == "custom") ? "block" : "none"; ?>;">
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Date From:
                                </td>
                                <td  width="50%">
                                    <input id="date_from" name="date_from" class="on-field-date date" type="text" style="text-align:right;" value="<?php echo $dateFrom; ?>">
                                    <div id="dateFrom" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                    <div id="dateFromDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo date("Y-m-d"); ?></font></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Date To:
                                </td>
                                <td  width="50%">
                                    <input id="date_to" name="date_to" class="on-field-date date" type="text" style="text-align:right;" value="<?php echo $dateTo; ?>">
                                    <div id="dateTo" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                    <div id="dateToDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo date("Y-m-d"); ?></font></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <br/>
                    <input name="btnDisplay" onClick="display();" tabindex="2" type="button" value="Display Information">
                    <input method="post" name="save" type="hidden" value="0" />
                    </form>
                    <?php
                    //  If Display Button Is Pressed
                    if ((isset($_POST["save"]) && $_POST["save"] === "1") || $projectID != "")  {
                        if (isset($_POST["project"]) && $_POST["project"] != "null")    $projectID = $_POST["project"];
                        if (isset($_POST["company"]) && $_POST["company"] != "null")    $companyID = $_POST["company"];
                        if (isset($_POST["status"]) && $_POST["status"] != "null")      $status = $_POST["status"];

                        if ($companyID == "")       $companyID = "null";

                        $projectType = q("SELECT p_type FROM Project WHERE id = '".$projectID."'");

                        $isSharedProject = ($projectType == "CP") ? 0 : 1;
                        $isPM = q("SELECT manager FROM Project_User WHERE user_id = '".$_SESSION["user_id"]."' AND project_id = '".$projectID."' AND company_id = '".$_SESSION["company_id"]."'");
                        $isSPM = q("SELECT spm_manager FROM Project_User WHERE user_id = '".$_SESSION["user_id"]."' AND project_id = '".$projectID."' AND company_id = '".$_SESSION["company_id"]."'");

                        $types = array("Time","Expenses");

                        if (is_array($types))       {
                            foreach ($types as $type)       {
                                $where = "";
                                $hasAreas = 0;

                                if ($type == "Time")    {
                                    if ($status != "all" && is_numeric($status))
                                        $where .= ($isSharedProject == 1) ? "AND (ts.status = '".$status."' OR ts.status2 = '".$status."') " : "AND ts.status = '".$status."' ";
                                } else if ($type == "Expenses") {
                                    if ($status != "all" && is_numeric($status))
                                        $where .= ($isSharedProject == 1) ? "AND (es.status = '".$status."' OR es.status2 = '".$status."') " : "AND es.status = '".$status."' ";
                                }

                                if ($companyID != "null")   $where .= "AND c.id = '".$companyID."' ";

                                if ($level == "CP") $where .= "AND c.id = '".$_SESSION["company_id"]."' ";

                                if ($type == "Time")        {
                                    $areas  = q("SELECT ts.project_id,ts.area_id
                                                    FROM (ApprovedTime AS ts
                                                        INNER JOIN Employee AS e ON ts.user_id = e.id
                                                        INNER JOIN Activities AS a ON ts.activity_id = a.id
                                                        INNER JOIN ActivityTypes AS at ON a.parent_id = at.id
                                                        INNER JOIN Project AS p ON ts.project_id = p.id
                                                        INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
                                                        INNER JOIN Company AS c ON cot.company_id = c.id
                                                        LEFT JOIN areas AS ar ON ts.area_id = ar.id)
                                                    WHERE ts.date >= '".$dateFrom."' AND ts.date <= '".$dateTo."' AND ts.project_id = '".$projectID."' AND ts.area_id > 0 AND ts.company_id = c.id $where");

                                    if (is_array($areas))   $hasAreas = 1;

                                    $select = "";

                                    if ($hasAreas)  {
                                        if ($isSharedProject == 1)  {
                                            $select .= "c.name,CONCAT(e.lstname, ', ', e.frstname),ar.name,at.type,a.name,ts.descr,ts.status,ts.status2,ts.date,ts.time_spent,ts.rate,ts.time_spent * ts.rate";
                                            $headings[$type] = array("Company Name","Employee Name","Area","Activity Type","Activity","Description","PM Status","SPM Status","Date","Time Spent",
                                                                        "Rate","Cost <i>(".$_SESSION["currency"].")</i>");
                                        }
                                        else        {
                                            $select .= "c.name,CONCAT(e.lstname, ', ', e.frstname),ar.name,at.type,a.name,ts.descr,ts.status,ts.date,ts.time_spent,ts.rate,ts.time_spent * ts.rate";
                                            $headings[$type] = array("Company Name","Employee Name","Area","Activity Type","Activity","Description","PM Status","Date","Time Spent",
                                                                        "Rate","Cost <i>(".$_SESSION["currency"].")</i>");
                                        }
                                    } else  {
                                        if ($isSharedProject == 1)  {
                                            $select .= "c.name,CONCAT(e.lstname, ', ', e.frstname),at.type,a.name,ts.descr,ts.status,ts.status2,ts.date,ts.time_spent,ts.rate,ts.time_spent * ts.rate";
                                            $headings[$type] = array("Company Name","Employee Name","Activity Type","Activity","Description","PM Status","SPM Status","Date","Time Spent",
                                                                        "Rate","Cost <i>(".$_SESSION["currency"].")</i>");
                                        }
                                        else        {
                                            $select .= "c.name,CONCAT(e.lstname, ', ', e.frstname),at.type,a.name,ts.descr,ts.status,ts.date,ts.time_spent,ts.rate,ts.time_spent * ts.rate";
                                            $headings[$type] = array("Company Name","Employee Name","Activity Type","Activity","Description","PM Status","Date","Time Spent",
                                                                        "Rate","Cost <i>(".$_SESSION["currency"].")</i>");
                                        }
                                    }

                                    $timeData = q("SELECT $select
                                                    FROM (ApprovedTime AS ts
                                                        INNER JOIN Employee AS e ON ts.user_id = e.id
                                                        INNER JOIN Activities AS a ON ts.activity_id = a.id
                                                        INNER JOIN ActivityTypes AS at ON a.parent_id = at.id
                                                        INNER JOIN Project AS p ON ts.project_id = p.id
                                                        INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
                                                        INNER JOIN Company AS c ON cot.company_id = c.id
                                                        LEFT JOIN areas AS ar ON ts.area_id = ar.id )
                                                    WHERE ts.date >= '".$dateFrom."' AND ts.date <= '".$dateTo."' AND ts.project_id = '".$projectID."' AND ts.company_id = c.id $where
                                                    ORDER BY c.name, ts.date, ts.on_date, ts.on_time, ar.name");
                                } else      {
                                    $areas  = q("SELECT es.project_id,es.area_id
                                                    FROM (ApprovedExpense AS es
                                                        INNER JOIN Employee AS e ON es.user_id = e.id
                                                        INNER JOIN dropdowns AS dd ON es.expense_type_id = dd.id
                                                        LEFT JOIN Activities AS a ON es.activity_id = a.id
                                                        LEFT JOIN ActivityTypes AS at ON a.parent_id = at.id
                                                        INNER JOIN Project AS p ON es.project_id = p.id
                                                        INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
                                                        INNER JOIN Company AS c ON cot.company_id = c.id
                                                        LEFT JOIN areas AS ar ON es.area_id = ar.id
                                                        LEFT JOIN Vehicle AS v ON es.vehicle_id = v.id
                                                        LEFT JOIN Disbursements AS d ON es.disbursement_id = d.id
                                                        LEFT JOIN DisbursementTypes AS dt ON dt.id = d.parent_id)
                                                    WHERE es.date >= '".$dateFrom."' AND es.date <= '".$dateTo."' AND es.project_id = '".$projectID."' AND es.area_id > 0 AND es.company_id = c.id $where");

                                    if (is_array($areas))   $hasAreas = 1;

                                    $select = "";

                                    if ($hasAreas)  {
                                        if ($isSharedProject == 1)  {
                                            $select .= "c.name,CONCAT(e.lstname, ', ', e.frstname),dd.name,IF(dd.name = 'Driving',v.type,IF(dd.name = 'Printing',CONCAT(dt.type,': ',d.name),es.otherDescr)),ar.name,at.type,".
                                                        "a.name,es.descr,es.status,es.status2,es.date,IF(dd.name = 'Driving',es.kilometers,es.total), es.rate, es.expense";
                                            $headings[$type] = array("Company Name","Employee Name","Expense Type","Expense Info","Area","Activity Type","Activity","Description","PM Status","SPM Status","Date","Amount",
                                                                        "Rate","Cost <i>(".$_SESSION["currency"].")</i>");
                                        }
                                        else        {
                                            $select .= "c.name,CONCAT(e.lstname, ', ', e.frstname),dd.name,IF(dd.name = 'Driving',v.type,IF(dd.name = 'Printing',CONCAT(dt.type,': ',d.name),es.otherDescr)),ar.name,at.type,".
                                                        "a.name,es.descr,es.status,es.date,IF(dd.name = 'Driving',es.kilometers,es.total), es.rate, es.expense";
                                            $headings[$type] = array("Company Name","Employee Name","Expense Type","Expense Info","Area","Activity Type","Activity","Description","PM Status","Date","Amount",
                                                                        "Rate","Cost <i>(".$_SESSION["currency"].")</i>");
                                        }
                                    } else  {
                                        if ($isSharedProject == 1)  {
                                            $select .= "c.name,CONCAT(e.lstname, ', ', e.frstname),dd.name,IF(dd.name = 'Driving',v.type,IF(dd.name = 'Printing',CONCAT(dt.type,': ',d.name),es.otherDescr)),at.type,".
                                                        "a.name,es.descr,es.status,es.status2,es.date,IF(dd.name = 'Driving',es.kilometers,es.total), es.rate, es.expense";
                                            $headings[$type] = array("Company Name","Employee Name","Expense Type","Expense Info","Activity Type","Activity","Description","PM Status","SPM Status","Date","Amount",
                                                                        "Rate","Cost <i>(".$_SESSION["currency"].")</i>");
                                        }
                                        else        {
                                            $select .= "c.name,CONCAT(e.lstname, ', ', e.frstname),dd.name,IF(dd.name = 'Driving',v.type,IF(dd.name = 'Printing',CONCAT(dt.type,': ',d.name),es.otherDescr)),at.type,".
                                                        "a.name,es.descr,es.status,es.date,IF(dd.name = 'Driving',es.kilometers,es.total), es.rate, es.expense";
                                            $headings[$type] = array("Company Name","Employee Name","Expense Type","Expense Info","Activity Type","Activity","Description","PM Status","Date","Amount",
                                                                        "Rate","Cost <i>(".$_SESSION["currency"].")</i>");
                                        }
                                    }

                                    $expenseData = q("SELECT $select
                                                        FROM (ApprovedExpense AS es
                                                            INNER JOIN Employee AS e ON es.user_id = e.id
                                                            INNER JOIN dropdowns AS dd ON es.expense_type_id = dd.id
                                                            LEFT JOIN Activities AS a ON es.activity_id = a.id
                                                            LEFT JOIN ActivityTypes AS at ON a.parent_id = at.id
                                                            INNER JOIN Project AS p ON es.project_id = p.id
                                                            INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
                                                            INNER JOIN Company AS c ON cot.company_id = c.id
                                                            LEFT JOIN areas AS ar ON es.area_id = ar.id
                                                            LEFT JOIN Vehicle AS v ON es.vehicle_id = v.id
                                                            LEFT JOIN Disbursements AS d ON es.disbursement_id = d.id
                                                            LEFT JOIN DisbursementTypes AS dt ON dt.id = d.parent_id)
                                                        WHERE es.date >= '".$dateFrom."' AND es.date <= '".$dateTo."' AND es.project_id = '".$projectID."' AND es.company_id = c.id $where
                                                        ORDER BY c.name, es.date, es.on_date, es.on_time, ar.name");
                                }
                            }
                        }

                        if (is_array($timeData) || is_array($expenseData))    {
                            unset($exceldata);

                            $status = array("<font class='on-validate-error'>Unapproved</font>","Updated","Approved","Declined");

                            $row = 0;

                            $timeColumns = count($headings["Time"]);
                            $expenseColumns = count($headings["Expenses"]);

                            $columns = (!is_array($expenseData)) ? $timeColumns : $expenseColumns;

                            if (is_array($types))   {
                                echo "<div class='on-20px'>";
                                    echo "<table class='on-table on-table-center'>";

                                    foreach ($types as $type)   {
                                        $displayString = "";

                                        $data = ($type == "Time") ? $timeData : $expenseData;

                                        if (is_array($data))    {
                                            //  HTML DISPLAY
                                            $displayString .= "<tr>";
                                                $displayString .= "<td style='text-align:center' class='on-table-clear' colspan='100%'>";
                                                     $displayString .= "<h6>Unapproved ".$type."</h6>";
                                                $displayString .= "</td>";
                                            $displayString .= "</tr>";
                                            $displayString .= "<tr>";
                                                $displayString .= "<td colspan='100%' class='rightdata'>";
                                                    $btnSelect = ($type == "Time") ? "selection(true);" : "selection2(true);";
                                                    $btnDeselect = ($type == "Time") ? "selection(false);" : "selection2(false);";
                                                    $displayString .= "<input name='btnSelect' onClick='".$btnSelect."' tabindex='1' type='button' value='Select All'>";
                                                    $displayString .= "<input name='btnDeselect' onClick='".$btnDeselect."' tabindex='1' type='button' value='Deselect All'><br/>";
                                                $displayString .= "</td>";
                                            $displayString .= "</tr>";

                                            //  EXCEL DISPLAY
                                            $exceldata[$row][] = strip_tags("Unapproved ".$type);

                                            for ($a = 1; $a < $columns; $a++)    $exceldata[$row][] = "";

                                            $row++;

                                            //  Headings
                                            //  HTML DISPLAY
                                            $displayString .= "<tr>";

                                            foreach ($headings[$type] as $h)    {
                                                $colspan = 1;

                                                if ($type == "Time")
                                                    $colspan = ($h == "Employee Name") ? 3 : 1;

                                                $displayString .= "<th colspan='".$colspan."' style='white-space:nowrap'>".$h."</th>";
                                            }

                                            $displayString .= "</tr>";

                                            //  EXCEL DISPLAY
                                            foreach ($headings[$type] as $h)
                                                $exceldata[$row][] = strip_tags($h);

                                            for ($a = count($headings[$type]); $a < $columns; $a++)    $exceldata[$row][] = "";

                                            $row++;

                                            //  Data
                                            $counter = 1;
                                            $fields = ($type == "Time") ? array("A","B","D","C","E","F","G") : array("H","I","K","J","L","M","N");

                                            if ($type == "Time")
                                                $totalTime = 0;

                                            $totalCost = 0;

                                            foreach ($data as $r)       {
                                                $fieldCount = 0;
                                                $col = 0;

                                                $displayString .= "<tr>";

                                                foreach ($r as $d) {
                                                    $onChange = "";
                                                    $readonly = "";

                                                    $colspan = ($headings[$type][$col] == "Employee Name" && $type == "Time") ? 3 : 1;

                                                    if ($headings[$type][$col] == "Time Spent")                                    $totalTime += $d;
                                                    if ($headings[$type][$col] == "Cost <i>(".$_SESSION["currency"].")</i>")       $totalCost += $d;

                                                    if ($headings[$type][$col] == "PM Status" || $headings[$type][$col] == "SPM Status")        {
                                                        $displayString .= "<td colspan='".$colspan."' style='white-space:nowrap'>".$status[$d]."</td>";

                                                        $exceldata[$row][] .= strip_tags($status[$d]);
                                                    } else      {
                                                        if (preg_match("/^((\d{2}(([02468][048])|([13579][26]))[\-\/\s]?((((0?[13578])|(1[02]))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-\/\s]?((0?[1-9])|([1-2][0-9])))))|(\d{2}(([02468][1235679])|([13579][01345789]))[\-\/\s]?((((0?[13578])|(1[02]))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-\/\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))(\s(((0?[1-9])|(1[0-2]))\:([0-5][0-9])((\s)|(\:([0-5][0-9])\s))([AM|PM|am|pm]{2,2})))?$/", $d))
                                                            $displayString .= "<td colspan='".$colspan."' style='white-space:nowrap'>".$d."</td>";
                                                        else if (is_numeric($d))
                                                            $displayString .= "<td colspan='".$colspan."' style='white-space:nowrap' class='rightdata'>".number_format((double)$d, 2, ".", "")."</td>";
                                                        else if ($d == "")
                                                            $displayString .= "<td colspan='".$colspan."' style='white-space:nowrap'>-</td>";
                                                        else
                                                            $displayString .= "<td colspan='".$colspan."' style='white-space:nowrap'>".$d."</td>";

                                                        $exceldata[$row][] .= strip_tags($d);
                                                    }

                                                    $col++;
                                                }

                                                for ($a = count($headings[$type]); $a < $columns; $a++)    $exceldata[$row][] = "";

                                                $displayString .= "</tr>";
                                                $row++;
                                            }

                                            $displayString .= "<tr>";
                                                $colspan = ($type == "Time") ? array_search("Time Spent", $headings[$type]) + 2 : array_search("Cost <i>(".$_SESSION["currency"].")</i>", $headings[$type]);

                                                $displayString .= "<td class='on-table-total' colspan='".$colspan."'>Total:</td>";

                                                if ($type == "Time")    {
                                                    $displayString .= "<td class='on-table-total'>";
                                                        $displayString .= "<input name='time' class='number' readonly type='text' value='".number_format((double)$totalTime, 2, ".", "")."'>";
                                                    $displayString .= "</td>";
                                                    $displayString .= "<td class='on-table-total'></td>";
                                                    $displayString .= "<td class='on-table-total'>";
                                                        $displayString .= "<input name='cost' class='number' readonly type='text' value='".number_format((double)$totalCost, 2, ".", "")."'>";
                                                    $displayString .= "</td>";
                                                } else  {
                                                    $displayString .= "<td class='on-table-total'>";
                                                        $displayString .= "<input name='expenseTotal' class='number' readonly type='text' value='".number_format((double)$totalCost, 2, ".", "")."'>";
                                                    $displayString .= "</td>";
                                                }
                                            $displayString .= "</tr>";
                                            $displayString .= "<input id='".$type."' name='".$type."' type='hidden' value='".$counter."' />";

                                            $colspan = ($type == "Time") ? array_search("Time Spent", $headings[$type]) : array_search("Cost <i>(".$_SESSION["currency"].")</i>", $headings[$type]);

                                            for ($a = 1; $a < $colspan; $a++)   $exceldata[$row][] = "";

                                            $exceldata[$row][] .= "Total:";

                                            if ($type == "Time")    {
                                                $exceldata[$row][] .= $totalTime;
                                                $exceldata[$row][] .= "";
                                                $exceldata[$row][] .= $totalCost;
                                            } else  {
                                                $exceldata[$row][] .= $totalCost;
                                            }

                                            for ($a = count($headings[$type]); $a < $columns; $a++)    $exceldata[$row][] = "";

                                            $row++;
                                        }

                                        echo $displayString;

                                        if ($type == "Time" && is_array($data))    {
                                            echo "<tr><td style='background-color:transparent' colspan='100%'>&nbsp;</td></tr>";

                                            for ($a = 0; $a < $columns; $a++)    $exceldata[$row][] = "";

                                            $row++;
                                        }
                                    }

                                        echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                                    echo "</table>";
                                echo "</div><br/>";

                                ///////////////////////////
                                $_SESSION["fileName"] = "Time_Expense_Sheet_Report";
                                $_SESSION["fileData"] = $exceldata;
                                ///////////////////////////

                                echo "<input name='btnExport' onClick=\"location.href='_export.php'\" type='button' value='Export to Excel' />";
                            }
                            ?>
                        <br/><br/><br/>
                        <?php
                        }
                    }
                    ?>
                </td>
            </tr>
        </table>
    <?php
    }
}
?>
