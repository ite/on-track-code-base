<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

	if (!hasAccess("BUDGET_CAT_MANAGE"))
		header("Location: noaccess.php");

    //  Insert Category And Elements Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")        {
        $errorMessage                                                   = "";

        //  Get Information
        $category_name                                                  = addslashes(strip_tags($_POST["category_name"]));

        //  Check If Activity Type Exists In Database
        $exist                                                          = q("SELECT id FROM Category WHERE name = '$category_name' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."'");

        if (!$exist)
        {
            //  Insert Activity Type
            $insert                                                     = q("INSERT INTO Category (name, company_id) ".
                                                                            "VALUES ('$category_name', '".$_SESSION["company_id"]."')");

            //  Insert Sub Activities
            if ($insert)
            {
                $parent_id                                              = q("SELECT id FROM Category WHERE name = '$category_name' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."'");

                //  Get Information
                for ($i = 1; $i <= 10; $i++)
                {
                    $element_name                                       = addslashes(strip_tags($_POST["element_name_$i"]));

                    if ($element_name != "")    {
                        $insert                                         = q("INSERT INTO Elements (parent_id, name, status) VALUES ('$parent_id', '$element_name', '1')");
                        $getID                                          = q("SELECT id FROM Elements WHERE parent_id = '$parent_id' AND name = '$element_name' AND status = '1'");

                        $yearMonth = q("SELECT DISTINCT(CONCAT(year, '-', month)), year, month FROM Budget WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY CONCAT(year, '-', month) ASC");

                        if (is_array($yearMonth))       {
                            foreach ($yearMonth as $yM) {
                                $insert = q("INSERT INTO Budget (year, month, element_id, planned, amount, actual, due_date, company_id) ".
                                            "VALUES ('".$yM[1]."', '".$yM[2]."', '".$getID."', '', '', '0', '', '".$_SESSION["company_id"]."')");
                            }
                        }
                        else    {
                            $insert = q("INSERT INTO Budget (year, month, element_id, planned, amount, actual, due_date, company_id) ".
                                        "VALUES ('".date("Y")."', '".date("m")."', '".$getID."', '', '', '0', '', '".$_SESSION["company_id"]."')");
                        }
                    }
                }
            }

            if ($insert)
            {
                $time                                                   = date("H:i:s");

                $logs                                                   = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('$category_name and elements created', 'Insert', 'Category', ".
                                                                            "'".$_SESSION["email"]."', '$today', '$time', '".$_SESSION["company_id"]."')");

                header("Location: categories.php");
            }
        }
        else
            $errorMessage                                               = "Category Already Exists";
    }

    if ($errorMessage != "")
        echo "<p align='center' style='padding:0px;'><strong><font class='on-validate-error'>$errorMessage</font></strong></p>";

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("2", "budget");
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    var cal                                                             = new CodeThatCalendar(caldef1);

    function check(box)
    {
        var valid                                                       = 1;

        //  Check That Project Type Is Entered
        if (document.forms["category_add"].category_name.value == "")
        {
            ShowLayer("categoryName", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("categoryName", "none");

        if (valid == 1)
        {
            document.forms["category_add"].save.value                   = 1;
            document.forms["category_add"].submit();
        }
    }
</script>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="category_add">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Add New Category</h6>
                            </td>
                        </tr>
                        <tr>
                            <td class="centerdata">
                                <br/>
                            </td>
                        </tr>
                    </table>
                    <table cellpadding="0" cellspacing="2" width="100%">
                        <tr>
                            <td class="on-description">
                                Category
                            </td>
                            <td class="on-description-left">
                                &nbsp;&nbsp;Element
                            </td>
                        </tr>
                        <tr>
                            <td class="rightdata" valign="top" width="50%">
                                <input class="on-field" name="category_name" tabindex="1" type="text">
                                <div id="categoryName" style="display: none;"><font style="margin-right:45px;" class="on-validate-error">* Category must be entered</font></div>
                            </td>
                            <td align="left" width="50%">
                                <?php
                                    for ($i = 1; $i <= 10; $i++)
                                        echo "<input class='on-field' name='element_name_".$i."' tabindex='".($i + 1)."' type='text'><br/>";
                                ?>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <input name="btnAdd" onClick="check();" type="button" value="Add Category">
                    <input method="post" name="save" type="hidden" value="0" />
                </form>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
