<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("include/sajax.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    if (!hasAccess("REP_PROJ_LIST"))
        header("Location: noaccess.php");

    // Class for table background color
    $colorClass = "OnTrack_#_";

    ///////////////////////////
    //  Sajax
    function getProjects($status, $type) {
        if ($status == "all")   $status = " AND p.completed IN (0,1)";
        else                    $status = " AND p.completed = '".$status."'";

        if ($type == "null")
            $projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p
                            INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id)
                            WHERE cot.company_id = '".$_SESSION["company_id"]."'
                            AND p.deleted = '0' $status ORDER BY UPPER(p.name)");
        else
            $projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p
                            INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id)
                            WHERE cot.company_id = '".$_SESSION["company_id"]."'
                            AND p.deleted = '0'
                            AND p.type_id='$type' $status ORDER BY UPPER(p.name)");

        return $projects;
    }

    $sajax_request_type = "GET";
    sajax_init();
    sajax_export("getProjects");
    sajax_handle_client_request();
    ///////////////////////////

    //  Set Report Generated Status
    $generated = "0";

    //  Create Display Information Function
    if (isset($_POST["save"]) && $_POST["save"] === "1") {
        $errorMessage = "";

        $list = $_POST["list"];
        $displayString = "";

        $row = 0;

        $excelheadings[$row][] = "Report: User/Project List";
        $excelheadings[$row++][] = "";

        if ($list == "employee") {
            $projectStatus = $_POST["projectStatus1"];

            $colspan = 1;
            $employee_id = $_POST["employee"];
            $employeeName = q("SELECT CONCAT(frstname, ' ', lstname) From Employee WHERE id = '$employee_id'");

            $heading = "<tr><th>Projects For ".$employeeName."</th><th>Rate(s)</th></tr>";
            $excelheadings[$row][] = $colorClass."Projects For ".$employeeName;
            $excelheadings[$row++][] = $colorClass."Rate(s)";

            if ($projectStatus == "all")    $projectStatus = " AND p.completed IN (0,1) ";
            else                            $projectStatus = " AND p.completed = '".$projectStatus."' ";

            $records = q("SELECT DISTINCT(p.id), p.name, ur.rate FROM (Project AS p
                                    INNER JOIN Project_User AS pu ON p.id = pu.project_id
                                    INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
                                    LEFT JOIN user_rates AS ur ON pu.rateID = ur.id)
                                    WHERE pu.user_id = '".$employee_id."'
                                    AND cot.company_id = '".$_SESSION["company_id"]."'
                                    AND pu.company_id = '".$_SESSION["company_id"]."'
                                    AND p.deleted = '0'
                                    AND pu.deleted = '0'
                                    $projectStatus
                                    ORDER BY UPPER(p.name)");

            if (is_array($records)) {
                $row = 0;

                foreach ($records as $record) {
                    $displayString .= "<tr><td>".$record[1]."</td><td class='rightdata'>".(($record[2] != "") ? $record[2] : "0.00")."</td></tr>";
                    $exceldata[$row][] = $record[1];
                    $exceldata[$row++][] = (($record[2] != "") ? $record[2] : "0.00");
                }
            }
        }
        else {
            $projectStatus = $_POST["projectStatus2"];
            $project_id = $_POST["project"];
            $projectName = q("SELECT name From Project WHERE id = '$project_id'");
            $projectType = q("SELECT p_type From Project WHERE id = '$project_id'");

            if ($projectStatus == "all")    $projectStatus = " AND p.completed IN (0,1) ";
            else                            $projectStatus = " AND p.completed = '".$projectStatus."' ";

            if ($projectType == "SP")   {
                $isSPM = hasAccess("SPM_MANAGE");

                if ($isSPM) {
                    $colspan = 2;
                    $excelheadings[--$row][] = "";
                    $row++;

                    $heading = "<tr>".
                                    "<th>Company Name</th>".
                                    "<th>Team Members For ".$projectName."</th>".
                                    "<th>Rate(s)</th>".
                                "</tr>";
                    $excelheadings[$row][] = "Company Name";
                    $excelheadings[$row][] = "Team Members For ".$projectName;
                    $excelheadings[$row++][] = "Rate(s)";

                    $records = q("SELECT DISTINCT(e.id), c.name, CONCAT(e.frstname, ' ', e.lstname), ur.rate FROM (Project AS p
                                            INNER JOIN Project_User AS pu ON p.id = pu.project_id
                                            INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
                                            INNER JOIN Employee AS e ON e.id = pu.user_id
                                            INNER JOIN Company AS c ON c.id = pu.company_id
                                            LEFT JOIN user_rates AS ur ON ur.id = pu.rateID)
                                            WHERE pu.project_id = '$project_id'
                                            AND cot.company_id = '".$_SESSION["company_id"]."'
                                            AND p.deleted = '0'
                                            AND pu.deleted = '0'
                                            $projectStatus
                                            ORDER BY UPPER(c.name), UPPER(CONCAT(e.frstname, ' ', e.lstname));");

                    if (is_array($records)) {
                        $row = 0;

                        foreach ($records as $record) {
                            $displayString .= "<tr><td>".$record[1]."</td><td>".$record[2]."</td><td class='rightdata'>".(($record[3] != "") ? $record[3] : "0.00")."</td></tr>";
                            $exceldata[$row][] = $record[1];
                            $exceldata[$row][] = $record[2];
                            $exceldata[$row++][] = (($record[3] != "") ? $record[3] : "0.00");
                        }
                    }
                }
                else    {
                    $colspan = 1;
                    $heading = "<tr><td class='on-table-clear'>Team Members For ".$projectName."</td><td class='on-table-clear'>Rate(s)</td></tr>";
                    $excelheadings[$row][] = "Team Members For ".$projectName;
                    $excelheadings[$row++][] = "Rate(s)";

                    $records = q("SELECT DISTINCT(e.id), CONCAT(e.frstname, ' ', e.lstname), ur.rate FROM (Project AS p
                                            INNER JOIN Project_User AS pu ON p.id = pu.project_id
                                            INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
                                            INNER JOIN Employee AS e ON e.id = pu.user_id
                                            INNER JOIN Company AS c ON c.id = pu.company_id
                                            LEFT JOIN user_rates AS ur ON ur.id = pu.rateID)
                                            WHERE pu.project_id = '$project_id'
                                            AND cot.company_id = '".$_SESSION["company_id"]."'
                                            AND pu.company_id = '".$_SESSION["company_id"]."'
                                            AND p.deleted = '0'
                                            AND pu.deleted = '0'
                                            $projectStatus
                                            ORDER BY UPPER(c.name), UPPER(CONCAT(e.frstname, ' ', e.lstname));");

                    if (is_array($records)) {
                        $row = 0;

                        foreach ($records as $record) {
                            $displayString .= "<tr><td>".$record[1]."</td><td class='rightdata'>".(($record[2] != "") ? $record[2] : "0.00")."</td></tr>";
                            $exceldata[$row][] = $record[1];
                            $exceldata[$row++][] = (($record[2] != "") ? $record[2] : "0.00");
                        }
                    }
                }
            }
            else        {
                $colspan = 1;
                $heading = "<tr><td class='on-table-clear'>Team Members For ".$projectName."</td><td class='on-table-clear'>Rate(s)</td></tr>";
                $excelheadings[$row][] = "Team Members For ".$projectName;
                $excelheadings[$row++][] = "Rate(s)";

                $records = q("SELECT DISTINCT(e.id), CONCAT(e.frstname, ' ', e.lstname), ur.rate FROM (Project AS p
                                        INNER JOIN Project_User AS pu ON p.id = pu.project_id
                                        INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
                                        INNER JOIN Employee AS e ON e.id = pu.user_id
                                        INNER JOIN Company AS c ON c.id = pu.company_id
                                        LEFT JOIN user_rates AS ur ON ur.id = pu.rateID)
                                        WHERE pu.project_id = '$project_id'
                                        AND cot.company_id = '".$_SESSION["company_id"]."'
                                        AND pu.company_id = '".$_SESSION["company_id"]."'
                                        AND p.deleted = '0'
                                        AND pu.deleted = '0'
                                        $projectStatus
                                        ORDER BY UPPER(c.name), UPPER(CONCAT(e.frstname, ' ', e.lstname));");

                if (is_array($records)) {
                    $row = 0;

                    foreach ($records as $record) {
                        $displayString .= "<tr><td>".$record[1]."</td><td class='rightdata'>".(($record[2] != "") ? $record[2] : "0.00")."</td></tr>";
                        $exceldata[$row][] = $record[1];
                        $exceldata[$row++][] = (($record[2] != "") ? $record[2] : "0.00");
                    }
                }
            }
        }

        if ($displayString != "") {
            $displayString = "<tr><td class='on-table-clear' colspan='100%'><a>User/Project List Report<a></td></tr>".$heading.$displayString;

            $generated = "1";
        }
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("3", "reports");
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    function check()
    {
        var valid                                                       = 1;

        //  Check That List Item Is Selected
        if (document.forms["report_project"].list.value == "null")
        {
            ShowLayer("listDiv", "block");
            valid                                                       = 0;
        }
        //  Check That Employee/Project Is Selected
        else
        {
            ShowLayer("listDiv", "none");

            //  Check That List Item Is Selected
            if (document.forms["report_project"].list.value == "employee")
            {
                if (document.forms["report_project"].employee.value == "null")
                {
                    ShowLayer("employee2Div", "block");
                    valid                                                       = 0;
                }
                else
                    ShowLayer("employee2Div", "none");
            }
            else
            {
                if (document.forms["report_project"].project.value == "null")
                {
                    ShowLayer("project2Div", "block");
                    valid                                                       = 0;
                }
                else
                    ShowLayer("project2Div", "none");
            }
        }

        if (valid == 1)
        {
            document.forms["report_project"].save.value                 = 1;
            document.forms["report_project"].submit();
        }
    }

    function displayCheck(value)
    {
        if (value == "employee")
        {
            ShowLayer("listDiv", "none");
            ShowLayer("employeeDiv", "block");
            ShowLayer("employee2Div", "none");
            ShowLayer("projectDiv", "none");
            ShowLayer("project2Div", "none");
        }
        else if (value == "project")
        {
            ShowLayer("listDiv", "none");
            ShowLayer("employeeDiv", "none");
            ShowLayer("employee2Div", "none");
            ShowLayer("projectDiv", "block");
            ShowLayer("project2Div", "none");
        }
        else
        {
            ShowLayer("listDiv", "none");
            ShowLayer("employeeDiv", "none");
            ShowLayer("employee2Div", "none");
            ShowLayer("projectDiv", "none");
            ShowLayer("project2Div", "none");
        }
    }

    ///////////////////////////
    //  Sajax
    <?php sajax_show_javascript(); ?>

    function ClearOptions(OptionList) {
        for (x = OptionList.length; x >= 0; x--) {
            OptionList[x] = null;
        }
    }

    function setProjects(data) {
        var c = 0;

        document.forms["report_project"].project.options[(c++)] = new Option("--  Select A Project  --", "null");

        for (var i in data)
            eval("document.forms['report_project'].project.options["+(c++)+"] = new Option('" + data[i][1] + "', '" + data[i][0] + " ');");
    }

    function getProjects(select1,select2) {
        var status = eval("document.report_project."+select1+".value;");
        var type = eval("document.report_project."+select2+".value;");;

        ClearOptions(document.report_project.project);
        x_getProjects(status, type, setProjects);
    }
    ///////////////////////////
</script>
<?php
    $employees = q("SELECT e.id, e.lstname, e.frstname FROM (Employee AS e INNER JOIN Company_Users AS cu ON e.id = cu.user_id) ".
                    "WHERE cu.company_id = '".$_SESSION["company_id"]."' AND e.email != 'admin' ORDER BY e.lstname, frstname");
    $projectTypes = q("SELECT id, type FROM ProjectTypes WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY type");
    $projectTypes[] = array("0","Shared Projects");
    $projectTypes = sortArrayByColumn($projectTypes, 1);

    $projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) ".
		"WHERE cot.company_id = '".$_SESSION["company_id"]."' AND p.completed = '0' AND p.deleted = '0' ORDER BY UPPER(p.name)");

?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="report_project">
                    <div id="" style="<?php if ($generated == "1") echo "display: none;"; else echo "display: block"; ?>">
                        <table width="100%">
                            <tr>
                                <td class="centerdata">
                                    <h6>User/Project List Report</a>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    List According To:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="list" onChange="displayCheck(this.value);">
                                        <option value="null">--  Select An Option  --</option>
                                        <option value="employee">Employee</option>
                                        <option value="project">Project</option>
                                    </select>
                                    <div id="listDiv" style="display: none;"><font class="on-validate-error">* An option must be selected</font></div>
                                </td>
                            </tr>
                        </table>
                        <div id="employeeDiv" style="display: none;">
                            <table width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Select Employee:
                                    </td>
                                    <td width="50%">
                                        <select class="on-field" method="post" name="employee">
                                            <option value="null">--  Select Employee  --</option>
                                            <?php
                                                if (is_array($employees))
                                                    foreach ($employees as $employee)
                                                        if ($_POST["employee"] == $employee[0])
                                                            echo "<option value='".$employee[0]."' selected>".$employee[1].", ".$employee[2]."</option>";
                                                        else
                                                            echo "<option value='".$employee[0]."'>".$employee[1].", ".$employee[2]."</option>";
                                            ?>
                                        </select>
                                        <div id="employee2Div" style="display: none;"><font class="on-validate-error">* Employee must be selected</font></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="on-description" width="50%">
                                        Select Project Status:
                                    </td>
                                    <td width="50%">
                                        <select class="on-field" method="post" name="projectStatus1">
                                            <option value="all">Completed & Uncompleted</option>
                                            <option value="0" selected>Uncompleted</option>
                                            <option value="1">Completed</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="projectDiv" style="display: none;">
                            <table width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Select Project Status:
                                    </td>
                                    <td width="50%">
                                        <select class="on-field" method="post" name="projectStatus2" onChange="getProjects('projectStatus2','projectType');">
                                            <option value="0">Uncompleted</option>
                                            <option value="1">Completed</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="on-description" width="50%">
                                        Select Project Type:
                                    </td>
                                    <td width="50%">
                                        <select class="on-field" method="post" name="projectType" onChange="getProjects('projectStatus2','projectType');">
                                            <option value="null">All Project Types</option>
                                            <?php
                                                if (is_array($projectTypes))
                                                    foreach ($projectTypes as $projectType)
                                                        if ($_POST["projectType"] == $projectType[0])
                                                            echo "<option value='".$projectType[0]."' selected>".$projectType[1]."</option>";
                                                        else
                                                            echo "<option value='".$projectType[0]."'>".$projectType[1]."</option>";
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="on-description" width="50%">
                                        Select Project:
                                    </td>
                                    <td width="50%">
                                        <select class="on-field" method="post" id="project" name="project">
                                            <option value="null">--  Select A Project  --</option>
                                            <?php
                                                if (is_array($projects))
                                                    foreach ($projects as $project)
                                                        if ($_POST["project"] == $project[0])
                                                            echo "<option value='".$project[0]."' selected>".$project[1]."</option>";
                                                        else
                                                            echo "<option value='".$project[0]."'>".$project[1]."</option>";
                                            ?>
                                        </select>
                                        <div id="project2Div" style="display: none;"><font class="on-validate-error">* Project must be selected</font></div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <br/>
                        <input name="btnGenerate" onClick="check();" type="button" value="Generate Report">
                        <input method="post" name="save" type="hidden" value="0" />
                    </div>
                </form>
                <div id="report" style="<?php if ($generated == "1") echo "display: block;"; else echo "display: none"; ?>">
                    <?php
                        echo "<table class='on-table-center on-table'>";
                            echo "".$displayString;
                            echo  "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        echo "</table>";
                        echo "<br/>";

                        ///////////////////////////
                        //  Set Export Information
                        $_SESSION["fileName"] = "Project List Report";
                        $_SESSION["fileData"] = array_merge($excelheadings, $exceldata);
                        ///////////////////////////

                        echo "<input name='btnExport' onClick=\"location.href='_export.php'\" type='button' value='Export to Excel' />";
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
