<?php
	session_start();

	include("_db.php");
	include("graphics.php");
	include("fusion/FusionCharts.php");

	if (!$_SESSION["logged_in"] === true)
		header("Location: login.php");

	if (!isset($_SESSION["company_id"]))
		header("Location: home.php");

	if (!hasAccess("REP_TIME_COM"))
		header("Location: noaccess.php");

	//  Set Report Generated Status
	$generated = "0";
	$invoiceable = "both";

	if (isset($_POST["invoiceable"]))	$invoiceable = addslashes(strip_tags($_POST["invoiceable"]));

	function createDataSet($date_from, $date_to, $total, $view, $employees,$table,$where)	{
		$dataSetString                                                  = "";

		if (is_array($employees))	{
			foreach ($employees as $employee)	{
				$dataSetString                                          .= "<dataset seriesName='".$employee[1].", ".$employee[2]."'>";

				if ($view == "day")	{
					for ($i = 0; $i < $total; $i++)	{
						$time_spent                                     = q("SELECT SUM(ts.time_spent) FROM ($table AS ts INNER JOIN Project AS p ON p.id = ts.project_id INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) ".
																			"WHERE ts.user_id = '".$employee[0]."' ".
																			"AND ts.date = '".getDates($date_from, $i)."' ".
																			"AND ts.company_id = '".$_SESSION["company_id"]."' $where");

						if ($time_spent == "")
							$time_spent                                 = 0;

						$dataSetString                                  .= "<set value='".number_format($time_spent, 2, ".", "")."'/>";
					}
				} else if ($view == "week")	{
					$i                                                  = 0;

					for ($i; $i < ($total - 1); $i++)	{
						$time_spent                                     = q("SELECT SUM(ts.time_spent) FROM ($table AS ts INNER JOIN Project AS p ON p.id = ts.project_id INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) ".
																			"WHERE ts.user_id = '".$employee[0]."' ".
																			"AND ts.date >= '".getDates($date_from, ($i * 7))."' ".
																			"AND ts.date < '".getDates($date_from, ($i * 7) + 7)."' ".
																			"AND ts.company_id = '".$_SESSION["company_id"]."' $where");

						if ($time_spent == "")
							$time_spent                                 = 0;

						$dataSetString                                  .= "<set value='".number_format($time_spent, 2, ".", "")."'/>";
					}

					if (getDates($date_from, ($i * 7)) < $date_to)	{
						$time_spent                                     = q("SELECT SUM(ts.time_spent) FROM ($table AS ts INNER JOIN Project AS p ON p.id = ts.project_id INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) ".
																			"WHERE ts.user_id = '".$employee[0]."' ".
																			"AND ts.date >= '".getDates($date_from, ($i * 7))."' ".
																			"AND ts.date < '$date_to' AND ts.company_id = '".$_SESSION["company_id"]."' $where");

						if ($time_spent == "")
							$time_spent                                 = 0;

						$dataSetString                                  .= "<set value='".number_format($time_spent, 2, ".", "")."'/>";
					}
				} else	{
					for ($i = 0; $i < $total; $i++)	{
						$time_spent                                     = q("SELECT SUM(ts.time_spent) FROM ($table AS ts INNER JOIN Project AS p ON p.id = ts.project_id INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) ".
																			"WHERE ts.user_id = '".$employee[0]."' ".
																			"AND ts.date >= '".getNextMonthStart($date_from, $i)."' ".
																			"AND ts.date < '".getNextMonthStart($date_from, ($i + 1))."' ".
																			"AND ts.company_id = '".$_SESSION["company_id"]."' $where");

						if ($time_spent == "")
							$time_spent                                 = 0;

						$dataSetString                                  .= "<set value='".number_format($time_spent, 2, ".", "")."'/>";
					}
				}

				$dataSetString                                          .= "</dataset>";
			}
		}

		return $dataSetString;
	}

	function createXMLString($date_from, $date_to, $total, $view, $employees,$table,$where)	{
		$xmlString                                                      = "";

		$xmlString                                                      = "<chart caption='Time Comparison Report' subcaption='' xAxisName='Date' numberPrefix='' ".
																			"showValues='0' labelDisplay='rotate' ".fusionChart().">";

		//  Create Categories
		$xmlString                                                      .= "<categories>";

		if ($view == "day")
		{
			for ($i = 0; $i < $total; $i++)
			{
				$xmlString                                              .= "<category label='".getDates($date_from, $i)."' />";
			}
		}
		else if ($view == "week")
		{
			$i                                                          = 0;

			for ($i; $i < ($total - 1); $i++)
			{
				$xmlString                                              .= "<category label='".getDates($date_from, ($i * 7))." - ".getDates($date_from, ($i * 7) + 6)."' />";
			}

			if (getDates($date_from, ($i * 7)) < $date_to)
			{
				$xmlString                                              .= "<category label='".getDates($date_from, ($i * 7))." - ".$date_to."' />";
			}
		}
		else
		{
			for ($i = 0; $i < $total; $i++)
			{
				$xmlString                                              .= "<category label='".getNextMonth($date_from, $i)."' />";
			}
		}

		$xmlString                                                      .= "</categories>";

		//  Create Dataset - Per Employee
		$xmlString                                                      .= createDataSet($date_from, $date_to, $total, $view, $employees,$table,$where);


		$xmlString                                                      .= "</chart>";

		return $xmlString;
	}

	//  Create Display Information Function
	if (isset($_POST["save"]) && $_POST["save"] === "1")
	{
		$errorMessage                                                   = "";

		$reportOn = $_POST["reportOn"];
		$invoiceable = addslashes(strip_tags($_POST["invoiceable"]));

		$employee = $_POST["employee"];
		$date_from = addslashes(strip_tags($_POST["date_from"]));
		$date_to = addslashes(strip_tags($_POST["date_to"]));
		$view = $_POST["view"];
		$displayString = "";

		$table = ($reportOn == "approved") ? "ApprovedTime" : "TimeSheet";
		$where = ($reportOn == "approved") ? "AND ts.status = '2' " : "";

		if ($invoiceable == "invoiceable")	$where .= " AND cot.total_budget > 0 AND cot.total_budget != '' AND p.due_date != ''";
		if ($invoiceable == "non_invoiceable")	$where .= " AND (cot.total_budget = '' OR p.due_date = '')";

		if ($view == "day")
			$total                                                      = getDays($date_from, $date_to);
		else if ($view == "week")
		{
			$date_from                                                  = getWeekStart($date_from);
			$date_to                                                    = getWeekEnd($date_to);

			$total                                                      = getDays($date_from, $date_to) / 7;
		}
		else
		{
			$date_from                                                  = getMonthStart($date_from);
			$date_to                                                    = getMonthEnd($date_to);

			$total                                                      = getMonthPeriod($date_from, $date_to);
		}

		if ($employee == "all")
		{
			$employees                                                 = q("SELECT e.id, e.lstname, e.frstname FROM (Employee AS e INNER JOIN Company_Users ".
																		   "AS cu ON e.id = cu.user_id) WHERE cu.company_id = '".$_SESSION["company_id"]."' ".
																		   "AND e.email != 'admin' AND e.id IN (SELECT DISTINCT(user_id) ".
																				"FROM TimeSheet WHERE company_id = '".$_SESSION["company_id"]."' AND date BETWEEN '".$date_from."' AND '".$date_to."') ".
																			"ORDER BY e.lstname, frstname");
		}
		else
		{
			$employees                                                 = q("SELECT e.id, e.lstname, e.frstname FROM (Employee AS e INNER JOIN Company_Users ".
																		   "AS cu ON e.id = cu.user_id) WHERE cu.company_id = '".$_SESSION["company_id"]."' ".
																		   "AND e.email != 'admin' AND e.id = '$employee' ORDER BY e.lstname, frstname");
		}

		$graph                                                          = createXMLString($date_from, $date_to, $total, $view, $employees,$table,$where);
		$displayString                                                  = renderChart("fusion/MSLine.swf", "", $graph, "multigraph", 800, 800, false, false);

		if ($displayString != "")
		{
			$displayString                                              = "<tr><td class='on-table-clear' colspan='".($total + 1)."'><a>Time Comparison Report</a></td>
																					</tr><td colspan='".($total + 1)."'><br/>".$displayString."<br/></td></tr>";

			$generated                                                  = "1";
		}
	}

	//  Print Header
	print_header();
	//  Print Menu
	print_menus("5", "reports");

	if ($errorMessage != "")
	{
		echo "<p align='center' style='padding:0px;'><strong><font color='#999999'>$errorMessage</font></strong></p>";
		echo "<br/>";
	}
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
	//  Calendar
	jQuery(function(){
		jQuery('#date_from').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_from').val(),
		current: jQuery('#date_from').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_from').val()))
			_date = new Date();
			else _date = jQuery('#date_from').val();
			jQuery('#date_from').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_from').val(formated);
			jQuery('#date_from').DatePickerHide();
		}
	});
	})

	jQuery(function(){
	jQuery('#date_to').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_to').val(),
		current: jQuery('#date_to').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_to').val()))
			_date = new Date();
			else _date = jQuery('#date_to').val();
			jQuery('#date_to').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_to').val(formated);
			jQuery('#date_to').DatePickerHide();
		}
	});
	})

	function check()
	{
		var valid                                                       = 1;

		//  Check That Employee Is Selected
		if (document.forms["report_time_comparison"].employee.value == "null")
		{
			ShowLayer("employeeDiv", "block");
			valid                                                       = 0;
		}
		else
			ShowLayer("employeeDiv", "none");

		//  Check That Date From Is Entered
		if (document.forms["report_time_comparison"].date_from.value == "")
		{
			ShowLayer("dateFrom", "block");
			valid                                                       = 0;
		}
		//  Check That Entered Date From Is Valid
		else
		{
			ShowLayer("dateFrom", "none");

			if (!validation("date", document.forms["report_time_comparison"].date_from.value))
			{
				ShowLayer("dateFromDiv", "block");
				valid                                                   = 0;
			}
			else
				ShowLayer("dateFromDiv", "none");
		}

		//  Check That Date To Is Entered
		if (document.forms["report_time_comparison"].date_to.value == "")
		{
			ShowLayer("dateTo", "block");
			valid                                                       = 0;
		}
		//  Check That Entered Date To Is Valid
		else
		{
			ShowLayer("dateTo", "none");

			if (!validation("date", document.forms["report_time_comparison"].date_to.value))
			{
				ShowLayer("dateToDiv", "block");
				valid                                                   = 0;
			}
			else
				ShowLayer("dateToDiv", "none");
		}

		if (valid == 1)
		{
			document.forms["report_time_comparison"].save.value         = 1;
			document.forms["report_time_comparison"].submit();
		}
	}
</script>
<?php
	//  nEmployees                                                      = Number of Employees
	$nEmployees                                                        = q("SELECT COUNT(e.id) FROM (Employee AS e INNER JOIN Company_Users ".
																		   "AS cu ON e.id = cu.user_id) WHERE cu.company_id = '".$_SESSION["company_id"]."' ".
																		   "AND e.email != 'admin' AND e.deleted = '0'");
	$employees                                                         = q("SELECT e.id, e.lstname, e.frstname FROM (Employee AS e INNER JOIN Company_Users ".
																		   "AS cu ON e.id = cu.user_id) WHERE cu.company_id = '".$_SESSION["company_id"]."' ".
																		   "AND e.email != 'admin' AND e.deleted = '0' ORDER BY e.lstname, frstname");
?>
	<table width="100%">
		<tr height="380px">
			<td class="centerdata">
				<form action="" method="post" name="report_time_comparison">
					<div id="" style="<?php if ($generated == "1") echo "display: none;"; else echo "display: block"; ?>">
						<table width="100%">
							<tr>
								<td class="centerdata">
									<h6>Time Comparison Report</h6>
								</td>
							</tr>
						</table>
						<br/>
						<table width="100%">
							<tr>
								<td class="on-description" width="50%">
									Report On:
								</td>
								<td width="50%">
									<select class="on-field" method="post" name="reportOn">
										<option value="normal">Actual Time</option>
										<option value="approved">Approved Time</option>
									</select>
								</td>
							</tr>
							<tr>
								<td class="on-description" width="50%">
									Project Invoiceable Type:
								</td>
								<td class="on-description-left" width="50%">
									<input method="post" name="invoiceable" onChange="getProjects(invoiceable,projectType);" type="radio" value="both" <?php echo ($invoiceable == "both") ? "checked" : ""; ?>><a>All Projects (Invoiceable & Non-Invoicable)</a><br/>
									<input method="post" name="invoiceable" onChange="getProjects(invoiceable,projectType);" type="radio" value="invoiceable" <?php echo ($invoiceable == "invoiceable") ? "checked" : ""; ?>><a>Invoiceable Projects</a><br/>
									<input method="post" name="invoiceable" onChange="getProjects(invoiceable,projectType);" type="radio" value="non_invoiceable" <?php echo ($invoiceable == "non_invoiceable") ? "checked" : ""; ?>><a>Non-Invoicable Projects</a><br/>
								</td>
							</tr>
							<tr><td colspan="100%"><br/></td></tr>
							<tr>
								<td class="on-description" width="50%">
									Select Employee:
								</td>
								<td width="50%">
									<select class="on-field" method="post" name="employee">
										<option value="null">--  Select Employee  --</option>
										<option value="all">All Employees</option>
										<?php
											if ($nEmployees > 1)
												foreach ($employees as $employee)
													if ($_POST["employee"] == $employee[0])
														echo "<option value='".$employee[0]."' selected>".$employee[1].", ".$employee[2]."</option>";
													else
														echo "<option value='".$employee[0]."'>".$employee[1].", ".$employee[2]."</option>";
											else if ($nEmployees == 1)
												if ($_POST["employee"] == $employees[0][0])
													echo "<option value='".$employees[0][0]."' selected>".$employees[0][1].", ".$employees[0][2]."".
														"</option>";
												else
													echo "<option value='".$employees[0][0]."'>".$employees[0][1].", ".$employees[0][2]."</option>";
										?>
									</select>
									<div id="employeeDiv" style="display: none;"><font class="on-validate-error">* Employee must be selected</font></div>
								</td>
							</tr>
							<tr>
								<td class="on-description" width="50%">
									Date From:
								</td>
								<td width="50%">
									<input class="on-field-date" id="date_from" name="date_from" type="text" style="text-align:right;" value="<?php
										if ($_POST["date_from"] != "") echo "".$_POST["date_from"]; else echo "".getMonthStart($today); ?>">
									<div id="dateFrom" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
									<div id="dateFromDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
								</td>
							</tr>
							<tr>
								<td class="on-description" width="50%">
									Date To:
								</td>
								<td width="50%">
									<input class="on-field-date" id="date_to" name="date_to" type="text" style="text-align:right;" value="<?php
										if ($_POST["date_to"] != "") echo "".$_POST["date_to"]; else echo "".getMonthEnd($today); ?>">
									<div id="dateTo" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
									<div id="dateToDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
								</td>
							</tr>
							<tr>
								<td class="on-description" width="50%">
									View Per:
								</td>
								<td class="on-description-left" width="50%">
									<input method="post" name="view" type="radio" value="day" checked><a>Day</a><br/>
									<input method="post" name="view" type="radio" value="week"><a>Week</a><br/>
									<input method="post" name="view" type="radio" value="month"><a>Month</a><br/>
								</td>
							</tr>
						</table>
						<br/>
						<input name="btnGenerate" onClick="check();" type="button" value="Generate Report">
						<input method="post" name="save" type="hidden" value="0" />
					</div>
				</form>
				<div id="report" style="<?php if ($generated == "1") echo "display: block;"; else echo "display: none"; ?>">
					<?php
						echo "<table class='on-table-center on-table'>";
							echo "".$displayString;
							echo  "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
						echo "</table>";
					?>
				</div>
			</td>
		</tr>
		<tr>
			<td align="center">
				<br/>
			</td>
		</tr>
	</table>
<?php
	//  Print Footer
	print_footer();
?>
