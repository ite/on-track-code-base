<?php
	session_start();

	include("_db.php");
	include("graphics.php");
	include("include/sajax.php");

	if (!$_SESSION["logged_in"] === true)
		header("Location: login.php");

	if (!isset($_SESSION["company_id"]))
		header("Location: home.php");

	if (!hasAccess("REP_EX_SHEET"))
		header("Location: noaccess.php");

	// Class for table background color
	$colorClass = "OnTrack_#_";

	///////////////////////////
	//  Sajax
	function getPrinting($id) {
		return q("SELECT id, name FROM Disbursements WHERE parent_id = '$id' ORDER BY name");
	}

	function getProjects($status, $invoiceable, $type) {
		if ($status == "all")   $status = " AND p.completed IN (0,1)";
		else                    $status = " AND p.completed = '".$status."'";

		if ($invoiceable == "invoiceable")	$invoiceable = " AND cot.total_budget > 0 AND cot.total_budget != '' AND p.due_date != ''";
		else if ($invoiceable == "non_invoiceable")	$invoiceable = " AND (cot.total_budget = '' OR p.due_date = '')";
		else $invoiceable = "";

		if ($type == "null")
			$projects = q("SELECT DISTINCT(p.id),p.p_type, p.name FROM (Project AS p
							INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id)
							WHERE cot.company_id = '".$_SESSION["company_id"]."'
							AND p.deleted = '0' $status $invoiceable ORDER BY UPPER(p.name)");
		else
			$projects = q("SELECT DISTINCT(p.id),p.p_type, p.name FROM (Project AS p
							INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id)
							WHERE cot.company_id = '".$_SESSION["company_id"]."'
							AND p.deleted = '0'
							AND p.type_id='$type' $status $invoiceable ORDER BY UPPER(p.name)");

		return $projects;
	}

	$sajax_request_type = "GET";
	sajax_init();
	sajax_export("getPrinting");
	sajax_export("getProjects");
	sajax_handle_client_request();
	///////////////////////////

	//  Set Report Generated Status
	$generated = "0";

	//  Create Display Information Function
	if (isset($_POST["save"]) && $_POST["save"] === "1")        {
		unset($exceldata);

		$downloadList = "";

		$errorMessage = "";

		$reportOn = $_POST["reportOn"];
		$projectStatus = $_POST["projectStatus"];
		$invoiceable = $_POST["invoiceableStatus"];

		$employee_id = $_POST["employee"];
		$project_type_id = $_POST["projectType"];
		$project_id = $_POST["project"];
		$expenseType = $_POST["expenseType"];
		$vehicle_id = $_POST["vehicle"];
		$printingType_id = $_POST["printing_type"];
		$printing_id = $_POST["printing"];
		$date_type = $_POST["dateType"];

		//  Get Dates
		if ($date_type == "currentwk")  {
			$date_from = currentWeekStart();
			$date_to = $today;
		}
		else if ($date_type == "previouswk")    {
			$date_from = getDates(currentWeekStart(), -7);
			$date_to = getDates(currentWeekStart(), -1);
		}
		else if ($date_type == "currentmnth")   {
			$date_from = getMonthStart($today);
			$date_to = $today;
		}
		else if ($date_type == "previousmnth")  {
			$date_from = getPreviousMonthStart();
			$date_to = getMonthEnd(getPreviousMonthStart());
		}
		else if ($date_type == "projectLifetime")      {
			$date_from = q("SELECT MIN(date) FROM ExpenseSheet WHERE project_id = '".$project_id."'");
			$date_to = $today;
		}
		else if ($date_type == "custom")        {
			$date_from = addslashes(strip_tags($_POST["date_from"]));
			$date_to = addslashes(strip_tags($_POST["date_to"]));
		}

		//  Timestamp
		$timestamp = ($date_from === $date_to) ? "Time Period: ".$date_from : "Time Period: ".$date_from." - ".$date_to;

		$hasAreas = 0;

		$table = ($reportOn == "approved") ? "ApprovedExpense" : "ExpenseSheet";
		$where = ($reportOn == "approved") ? "AND es.status = '2' " : "";

		if (($employee_id != "null" && is_numeric($employee_id)))       {
			$empInfo = q("SELECT lstname, frstname FROM Employee WHERE id = '$employee_id'");
			$employeeInfo = $empInfo[0][0].", ".$empInfo[0][1];
			$where .= "AND es.user_id = '$employee_id' ";
		} else  $employeeInfo = "All Employees";

		if (!($project_id != "null" && is_numeric($project_id)))        {
			if (!($project_type_id != "null" && is_numeric($project_type_id)))  {
				$projectInfo = "All Projects <i>{All Project Types}</i>";
				$where .= "AND cot.company_id = '".$_SESSION["company_id"]."' ";
			} else      {
				$projectType = ($project_type_id != 0) ? q("SELECT type FROM ProjectTypes WHERE id = '".$project_type_id."'") : "Shared Projects";
				$projectInfo = "All Projects <i>{".$projectType."}</i>";
				$where .= "AND cot.company_id = '".$_SESSION["company_id"]."' AND p.type_id = '$project_type_id' ";
			}
		} else  {
			$projectInfo = q("SELECT name FROM Project WHERE id = '$project_id'");
			$where .= "AND cot.project_id = '$project_id' AND cot.company_id = '".$_SESSION["company_id"]."'  ";
		}

		if (($expenseType != "null" && is_numeric($expenseType)))       {
			$expenseInfo = q("SELECT name FROM dropdowns WHERE id = '$expenseType'");
			$where .= "AND es.expense_type_id = '$expenseType' ";
		} else  $expenseInfo = "All Expense Types";

		if ($vehicle_id != "null" && is_numeric($vehicle_id))   $where .= "AND es.vehicle_id = '$vehicle_id' ";

		if (($printingType_id != "null" && is_numeric($printingType_id)) && ($printing_id != "null" && is_numeric($printing_id)))
			$where .= "AND es.disbursement_id = '$printing_id' ";
		else if (($printingType_id != "null" && is_numeric($printingType_id)) && !($printing_id != "null" && is_numeric($printing_id)))
			$where .= "AND dt.id = '$printingType_id' ";

		if ($projectStatus == "all")    $projectStatus = " AND p.completed IN (0,1) ";
		else                            $projectStatus = " AND p.completed = '".$projectStatus."' ";

		if ($invoiceable == "invoiceable")	$where .= " AND cot.total_budget > 0 AND cot.total_budget != '' AND p.due_date != ''";
		else if ($invoiceable == "non_invoiceable")	$where .= " AND (cot.total_budget = '' OR p.due_date = '')";

		//  Create Headings
		$reportHead = array("<a>Report: Expense Sheet</a>","<a>Report For: ".$employeeInfo."</a>","<a>Report For: ".$projectInfo."</a>","<a>Report For: ".$expenseInfo."</a>",$timestamp);

		$areas  = q("SELECT es.project_id,es.area_id
						FROM ($table AS es
							INNER JOIN Employee AS e ON es.user_id = e.id
							INNER JOIN dropdowns AS dd ON es.expense_type_id = dd.id
							LEFT JOIN Activities AS a ON es.activity_id = a.id
							LEFT JOIN ActivityTypes AS at ON a.parent_id = at.id
							INNER JOIN Project AS p ON es.project_id = p.id
							INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
							LEFT JOIN areas AS ar ON es.area_id = ar.id
							LEFT JOIN Vehicle AS v ON es.vehicle_id = v.id
							LEFT JOIN Disbursements AS d ON es.disbursement_id = d.id
							LEFT JOIN DisbursementTypes AS dt ON dt.id = d.parent_id)
						WHERE es.date >= '".$date_from."' AND es.date <= '".$date_to."' AND es.area_id > 0 AND es.company_id = '".$_SESSION["company_id"]."' $projectStatus $where");

		if (is_array($areas))    $hasAreas = 1;

		$select = "";

		if ($hasAreas)  {
			$select .= "ar.name,";
			$expenseHeadings = array("Employee Name","Project Name","Project #","Expense Type","Expense Info","Area","Activity Type","Activity","Description","Date","Rate","Quantity","Cost <i>(".$_SESSION["currency"].")</i>");
		} else  $expenseHeadings = array("Employee Name","Project Name","Project #","Expense Type","Expense Info","Activity Type","Activity","Description","Date","Rate","Quantity","Cost <i>(".$_SESSION["currency"].")</i>");

		//  Get Information - Expense
		$expenseInfo = q("SELECT CONCAT(e.lstname, ', ', e.frstname),p.name,p.number,dd.name,IF(dd.name = 'Driving',v.type,IF(dd.name = 'Printing',CONCAT(dt.type,': ',d.name),es.otherDescr)),".$select."at.type,a.name,es.descr,es.date,
								es.rate, IF(dd.name = 'Driving',es.kilometers,es.total), es.expense".(($table == "ExpenseSheet") ? ", es.id" : ", es.expensesheet_id")."
							FROM ($table AS es
								INNER JOIN Employee AS e ON es.user_id = e.id
								INNER JOIN dropdowns AS dd ON es.expense_type_id = dd.id
								LEFT JOIN Activities AS a ON es.activity_id = a.id
								LEFT JOIN ActivityTypes AS at ON a.parent_id = at.id
								INNER JOIN Project AS p ON es.project_id = p.id
								INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
								LEFT JOIN areas AS ar ON es.area_id = ar.id
								LEFT JOIN Vehicle AS v ON es.vehicle_id = v.id
								LEFT JOIN Disbursements AS d ON es.disbursement_id = d.id
								LEFT JOIN DisbursementTypes AS dt ON dt.id = d.parent_id)
							WHERE es.date >= '".$date_from."' AND es.date <= '".$date_to."' AND es.company_id = '".$_SESSION["company_id"]."' $projectStatus $where
							ORDER BY es.date, es.on_date, es.on_time, p.name");

		if (is_array($expenseInfo))     {
			$columns = count($expenseHeadings) - 1;

			$displayString = "";
			$row = 0;

			//  HTML Display
			foreach ($reportHead as $rh)
				$displayString .= "<tr><td class='on-table-clear' colspan='".($columns + 1)."'>".$rh."</td></tr>";

			//  EXCEL DISPLAY
			foreach ($reportHead as $rh)        {
				$exceldata[$row][] = strip_tags($rh);

				for ($a = 0; $a < $columns; $a++)       $exceldata[$row][] = "";

				$row++;
			}

			$total_cost = 0;

			//  Headings
			//  HTML Display
			$displayString .= "<tr>";

			foreach ($expenseHeadings as $h)    $displayString .= "<th>".$h."</th>";

			$displayString .= "</tr>";

			//  EXCEL DISPLAY
			foreach ($expenseHeadings as $h)    $exceldata[$row][] = $colorClass.strip_tags($h);

			$row++;

			//  Data
			foreach ($expenseInfo as $r)        {
				$col = 0;
				$displayString .= "<tr>";

				$columns = count($r) - 1;

				foreach ($r as $d)      {
					if (isset($expenseHeadings[$col]))	{
						if ($expenseHeadings[$col] == "Cost <i>(".$_SESSION["currency"].")</i>")   $total_cost += $d;

						if (preg_match("/^((\d{2}(([02468][048])|([13579][26]))[\-\/\s]?((((0?[13578])|(1[02]))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-\/\s]?((0?[1-9])|([1-2][0-9])))))|(\d{2}(([02468][1235679])|([13579][01345789]))[\-\/\s]?((((0?[13578])|(1[02]))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-\/\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))(\s(((0?[1-9])|(1[0-2]))\:([0-5][0-9])((\s)|(\:([0-5][0-9])\s))([AM|PM|am|pm]{2,2})))?$/", $d))
							$displayString .= "<td style='white-space:nowrap'>".$d."</td>";
						else if (is_numeric($d))
							$displayString .= "<td style='white-space:nowrap' class='rightdata'>".number_format($d, 2, ".", "")."</td>";
						else if ($d == "")
							$displayString .= "<td style='white-space:nowrap'>-</td>";
						else	{
							if ($expenseHeadings[$col] == "Project Name")	{
								$displayString .= "<td style='white-space:nowrap'>";
								if (hasBackingDocuments($r[$columns]))	{
									$displayString .= $d. " <img onClick='window.open(\"serveFile.php?type=files&expense=".$r[$columns]."\")' class='hand' title='Click to download backing document(s)' src='images/icons/download.png' />";
									$downloadList .= $r[$columns].",";
								} else
									$displayString .= $d;
								$displayString .= "</td>";
							}
							else
								$displayString .= "<td style='white-space:nowrap'>".$d."</td>";
						}

						$exceldata[$row][] .= $d;
					}

					$col++;
				}

				$displayString .= "</tr>";
				$row++;
			}

			$colspan = ($hasAreas) ? 11 : 10;

			$displayString .= "<tr>
									<td class='on-table-total' colspan='".$colspan."'><a style='color:#FFFFFF;'>Total <i>(".$_SESSION["currency"].")</a></td>
									<td class='on-table-total'>".number_format($total_cost, 2, ".", "")."</td>
								</tr>";

			for ($a = 0; $a < $colspan - 1; $a++)       $exceldata[$row][] = "";

			$exceldata[$row][] .= "Total (".$_SESSION["currency"]."):";
			$exceldata[$row][] .= $total_cost;

			$row++;
		}

		if ($displayString != "")
			$generated = "1";
	}

	//  Print Header
	print_header();
	//  Print Menu
	print_menus("3", "reports");
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
	//  Calendar
	jQuery(function(){
	jQuery('#date_from').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_from').val(),
		current: jQuery('#date_from').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_from').val()))
			_date = new Date();
			else _date = jQuery('#date_from').val();
			jQuery('#date_from').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_from').val(formated);
			jQuery('#date_from').DatePickerHide();
		}
	});

		jQuery('#date_to').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_to').val(),
		current: jQuery('#date_to').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_to').val()))
			_date = new Date();
			else _date = jQuery('#date_to').val();
			jQuery('#date_to').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_to').val(formated);
			jQuery('#date_to').DatePickerHide();
		}
	});

		jQuery("#project").change(function()    {
			if (jQuery(this).val() != "null")   {
				jQuery("#dateType option[value='projectLifetime']").removeAttr("disabled");
			} else      {
				jQuery("#dateType option[value='projectLifetime']").attr("disabled","disabled");
				jQuery("#dateType option[value='currentwk']").attr("selected", "selected");
			}
		});

		//////////////////////////////
		//  Expense Type Selection
		jQuery("#expenseType").change(function()     {
			var divName = jQuery(this).find("option:selected").text().replace(/ /, "_");
			var divName = jQuery(this).find("option:selected").text().replace(/All /, "");
			var divName = jQuery(this).find("option:selected").text().replace(/ Expenses/, "_");

			jQuery(".expenseDiv").hide();

			if (divName == "Driving" || divName == "Printing")   jQuery("#"+divName+"Div").show();

			jQuery("#"+divName).show();
		});
		//////////////////////////////
	});

	//  Sajax
	<?php sajax_show_javascript(); ?>

	function check() {
		var valid = 1;

		if (document.forms["report"].dateType.value == "custom") {
			//  Check That Date From Is Entered
			if (document.forms["report"].date_from.value == "") {
				ShowLayer("dateFrom", "block");
				valid = 0;
			}
			//  Check That Entered Date From Is Valid
			else {
				ShowLayer("dateFrom", "none");

				if (!validation("date", document.forms["report"].date_from.value)) {
					ShowLayer("dateFromDiv", "block");
					valid = 0;
				}
				else
					ShowLayer("dateFromDiv", "none");
			}

			//  Check That Date To Is Entered
			if (document.forms["report"].date_to.value == "") {
				ShowLayer("dateTo", "block");
				valid = 0;
			}
			//  Check That Entered Date To Is Valid
			else {
				ShowLayer("dateTo", "none");

				if (!validation("date", document.forms["report"].date_to.value)) {
					ShowLayer("dateToDiv", "block");
					valid = 0;
				}
				else
					ShowLayer("dateToDiv", "none");
			}
		}

		if (valid == 1) {
			document.forms["report"].save.value = 1;
			document.forms["report"].submit();
		}
	}

	function dateSelection() {
		if (document.forms["report"].dateType.value == "custom")
			ShowLayer("datesDiv", "block");
		else
			ShowLayer("datesDiv", "none");
	}

	///////////////////////////
	//  Sajax
	function ClearOptions(OptionList) {
		for (x = OptionList.length; x >= 0; x--) {
			OptionList[x] = null;
		}
	}

	function setPrinting(data) {
		var c = 0;

		document.report.printing.options[(c++)] = new Option("All Sub Colour Types", "null");

		for (var i in data) {
			var display = data[i][1];
			display = display.replace(new RegExp(/\'/g),"");
			eval("document.report.printing.options[" + (c++) + "] = new Option('" + display + "', '" + data[i][0] + "');");
		}
	}

	function setProjects(data) {
		var c                                                           = 0;

		document.report.project.options[(c++)]                         = new Option("All Projects", "null");

		for (var i in data)
			eval("document.report.project.options["+(c++)+"] = new Option('" + data[i][1] + "', '" + data[i][0] + "');");
	}

	function getProjects(select1,select2,select3) {
		var status = eval("document.report."+select1+".value;");
		var invoiceable = eval("document.report."+select2+".value;");;
		var type = eval("document.report."+select3+".value;");;

		ClearOptions(document.report.project);
		x_getProjects(status, invoiceable, type, setProjects);
	}

	function getData(select)
	{
		ClearOptions(document.report.printing);

		if (select.value != "null") {
			ShowLayer("printingSubDiv", "block");
			x_getPrinting(select.value, setPrinting);
		}
		else
			ShowLayer("printingSubDiv", "none");
	}
	///////////////////////////
</script>
<?php
    $expenseTypes = q("SELECT id, name FROM dropdowns WHERE categoryID = (SELECT id FROM categories WHERE code = 'expenseType') ORDER BY ranking ASC");

    $employees = q("SELECT e.id, e.lstname, e.frstname FROM (Employee AS e
                                INNER JOIN Company_Users AS cu ON e.id = cu.user_id)
                                WHERE cu.company_id = '".$_SESSION["company_id"]."'
                                AND e.email != 'admin' ORDER BY e.lstname, frstname");
    $projectTypes = q("SELECT id, type FROM ProjectTypes
                                    WHERE company_id = '".$_SESSION["company_id"]."'
                                    ORDER BY type");
    $projectTypes[] = array("0","Shared Projects");
    $projectTypes = sortArrayByColumn($projectTypes, 1);

	$projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p
                            INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id)
                            WHERE cot.company_id = '".$_SESSION["company_id"]."'
                            AND p.deleted = '0' ORDER BY UPPER(p.name)");//AND p.completed = '0'
    $vehicles = q("SELECT id, type FROM Vehicle WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY type");
    $disbursementTypes = q("SELECT id, type FROM DisbursementTypes WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY type");
?>
	<table width="100%">
		<tr height="380px">
			<td class="centerdata" valign="top">
				<form action="" method="post" name="report">
					<div id="" style="<?php if ($generated == "1") echo "display: none;"; else echo "display: block"; ?>">
						<table width="100%">
							<tr>
								<td class="centerdata">
									<h6>Expense Sheet Report</h6>
								</td>
							</tr>
						</table>
						<br/>
						<table width="100%">
							<tr>
								<td class="on-description" width="50%">
									Report On:
								</td>
								<td width="50%">
									<select class="on-field" method="post" name="reportOn">
										<option value="normal">Actual Expenses</option>
										<option value="approved">Approved Expenses</option>
									</select>
								</td>
							</tr>
							<tr>
								<td class="on-description" width="50%">
									Select Project Status:
								</td>
								<td width="50%">
									<select class="on-field" method="post" name="projectStatus" onChange="getProjects('projectStatus','projectType');">
										<option value="all" selected>Completed & Uncompleted</option>
										<option value="0">Uncompleted</option>
										<option value="1">Completed</option>
									</select>
								</td>
							</tr>
							<tr>
								<td class="on-description" width="50%">
									Project Invoiceable Type:
								</td>
								<td width="50%">
									<select class="on-field" method="post" name="invoiceableStatus" onChange="getProjects('projectStatus','invoiceableStatus','projectType');">
										<option value="both" selected>All (Invoiceable & Non-Invoicable)</option>
										<option value="invoiceable">Invoiceable</option>
										<option value="non_invoiceable">Non-Invoicable</option>
									</select>
								</td>
							</tr>
							<tr><td colspan="100%"><br/></td></tr>
							<tr>
								<td class="on-description" width="50%">
									Select Employee:
								</td>
								<td width="50%">
									<select class="on-field" method="post" name="employee">
										<option value="null">All Employees</option>
										<?php
											if (is_array($employees))
												foreach ($employees as $employee)
													if ($_POST["employee"] == $employee[0])
														echo "<option value='".$employee[0]."' selected>".$employee[1].", ".$employee[2]."</option>";
													else
														echo "<option value='".$employee[0]."'>".$employee[1].", ".$employee[2]."</option>";
										?>
									</select>
								</td>
							</tr>
							<tr>
								<td class="on-description" width="50%">
									Select Project Type:
								</td>
								<td width="50%">
									<select class="on-field" method="post" name="projectType" onChange="getProjects('projectStatus','projectType');">
										<option value="null">All Project Types</option>
										<?php
											if (is_array($projectTypes))
												foreach ($projectTypes as $projectType)
													if ($_POST["projectType"] == $projectType[0])
														echo "<option value='".$projectType[0]."' selected>".$projectType[1]."</option>";
													else
														echo "<option value='".$projectType[0]."'>".$projectType[1]."</option>";
										?>
									</select>
								</td>
							</tr>
							<tr>
								<td class="on-description" width="50%">
									Select Project:
								</td>
								<td width="50%">
									<select class="on-field" method="post" id="project" name="project">
										<option value="null">All Projects</option>
										<?php
											if (is_array($projects))
												foreach ($projects as $project)
													if ($_POST["project"] == $project[0])
														echo "<option value='".$project[0]."' selected>".$project[1]."</option>";
													else
														echo "<option value='".$project[0]."'>".$project[1]."</option>";
										?>
									</select>
								</td>
							</tr>
							<tr>
								<td class="on-description" width="50%">
									Expense Type:
								</td>
								<td width="50%">
									<select class="on-field" method="post" id="expenseType" name="expenseType">
										<option value="null">All Expense Types</option>
										<?php
										// onChange="expenseSelection();"
											if (is_array($expenseTypes))
												foreach ($expenseTypes as $expenseType) {
													$descr = ($expenseType[1] == "Expenses") ? "All Sustenance Expenses" : "All ".$expenseType[1]." Expenses";

													if ($_POST["expenseType"] == $expenseType[0])       echo "<option value='".$expenseType[0]."' selected>".$expenseType[1]."</option>";
													else                                                echo "<option value='".$expenseType[0]."'>".$expenseType[1]."</option>";
												}
										?>
									</select>
								</td>
							</tr>
						</table>
						<div id="DrivingDiv" style="display: none;" class="expenseDiv">
							<table cellpadding="0" cellspacing="2" width="100%">
								<tr>
									<td class="on-description" width="50%">
										Vehicle:
									</td>
									<td width="50%">
										<select class="on-field" method="post" name="vehicle">
											<option value="null">All Vehicles</option>
											<?php
												if (is_array($vehicles))
													foreach ($vehicles as $vehicle)
														if ($_POST["vehicle"] == $vehicle[0])
															echo "<option value='".$vehicle[0]."' selected>".$vehicle[1]."</option>";
														else
															echo "<option value='".$vehicle[0]."'>".$vehicle[1]."</option>";
											?>
										</select>
									</td>
								</tr>
							</table>
						</div>
						<div id="PrintingDiv" style="display: none;" class="expenseDiv">
							<table width="100%">
								<tr>
									<td class="on-description" width="50%">
										Printing Type:
									</td>
									<td width="50%">
										<select class="on-field" method="post" name="printing_type" onChange="getData(printing_type);" tabindex="3">
										<option value="null">All Printing Types</option>
										<?php
											if (is_array($disbursementTypes))
												foreach ($disbursementTypes as $disbursementType)
													if ($_POST["printing_type"] == $disbursementType[0])
														echo "<option value='".$disbursementType[0]."' selected>".$disbursementType[1]."</option>";
													else
														echo "<option value='".$disbursementType[0]."'>".$disbursementType[1]."</option>";
										?>
										</select>
									</td>
								</tr>
							</table>
						</div>
						<div id="printingSubDiv" style="display: none;" class="expenseDiv">
							<table cellpadding="0" cellspacing="2" width="100%">
								<tr>
									<td class="on-description" width="50%">
										Colour:
									</td>
									<td width="50%">
										<select class="on-field" method="post" name="printing" tabindex="3">
											<option value="null">All Sub Colour Types</option>
										</select>
									</td>
								</tr>
							</table>
						</div>
						<table width="100%">
							<tr>
								<td class="on-description" width="50%">
									Date Type(s):
								</td>
								<td width="50%">
									<select class="on-field" method="post" id="dateType" name="dateType" onChange="dateSelection();">
										<option value="currentwk">Current Week</option>
										<option value="previouswk">Previous Week</option>
										<option value="currentmnth">Current Month</option>
										<option value="previousmnth">Previous Month</option>
										<option value="projectLifetime" disabled>Project Lifetime</option>
										<option value="custom">Custom Dates</option>
									</select>
								</td>
							</tr>
						</table>
						<div id="datesDiv" style="display: none;">
							<table cellpadding="0" cellspacing="2" width="100%">
								<tr>
									<td class="on-description" width="50%">
										Date From:
									</td>
									<td width="50%">
										<input class="on-field-date" id="date_from" name="date_from" type="text" style="text-align:right;" value="<?php
											if ($_POST["date_from"] != "") echo "".$_POST["date_from"]; else echo "".getMonthStart($today); ?>">
										<div id="dateFrom" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
										<div id="dateFromDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font>
										</div>
									</td>
								</tr>
								<tr>
									<td class="on-description" width="50%">
										Date To:
									</td>
									<td width="50%">
										<input class="on-field-date" id="date_to" name="date_to" type="text" style="text-align:right;" value="<?php
											if ($_POST["date_to"] != "") echo "".$_POST["date_to"]; else echo "".getMonthEnd($today); ?>">
										<div id="dateTo" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
										<div id="dateToDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
									</td>
								</tr>
							</table>
						</div>
						<br/>
						<input name="btnGenerate" onClick="check();" type="button" value="Generate Report">
						<input method="post" name="save" type="hidden" value="0" />
					</div>
				</form>
				<div id="report" style="<?php if ($generated == "1") echo "display: block;"; else echo "display: none"; ?>">
					<?php
						echo "<div class='on-20px'><table class='on-table-center on-table'>";
							echo "".$displayString;
							echo  "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
						echo "</table></div>";
						echo "<br/>";

						///////////////////////////
						//  Set Export Information
						$_SESSION["fileName"] = "Expense Sheet Report";
						$_SESSION["fileData"] = $exceldata;
						///////////////////////////

						echo "<input name='btnExport' onClick=\"location.href='_export.php'\" type='button' value='Export to Excel' />";

						if ($downloadList != "")	{
							$downloadList = substr($downloadList,0,-1);

							echo "<br/><br/><input onClick='window.open(\"serveFile.php?type=files&expense=".$downloadList ."\")' class='hand' type='button' value='Download backing document(s)' />";
						}
					?>
				</div>
			</td>
		</tr>
		<tr>
			<td align="center">
				<br/>
			</td>
		</tr>
	</table>
<?php
    //  Print Footer
    print_footer();
?>
