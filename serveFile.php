<?php
	session_start();
	error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);

	include("_db.php");
	include("include/PDFMerger.php");

	global $file_directory;

	switch ($_GET["type"])	{
		case "certificate":	{
			$fileLocation = $file_directory."certificates/";
			$fileName = "Payment Certificate - ".$_GET["invoice"].".xlsx";
/*
			error_reporting(E_ALL);
			ini_set("display_errors", TRUE);
			ini_set("display_startup_errors", TRUE);

			define("EOL",(PHP_SAPI == "cli") ? PHP_EOL : "<br />");

			require_once "include/PHPExcel.php";
			require_once "include/PHPExcel/IOFactory.php";

			// NB -- SPECIFY RENDERING LIBRARY
			$rendererName = PHPExcel_Settings::PDF_RENDERER_DOMPDF;
			$rendererLibrary = 'DomPDF';
			$rendererLibraryPath = 'include/PHPExcel/Writer/PDF/';
			if (!PHPExcel_Settings::setPdfRenderer(
					$rendererName,
					$rendererLibraryPath
				))
			{
				die('Please set the RENDER NAME and RENDER LIBRARY PATH (serveFile.php LINE: 34)');
			}

			// LOAD EXCEL FILE
			$objPHPExcel = PHPExcel_IOFactory::load(preg_replace("/ /","_",$fileLocation.$fileName));

			$sheets = $objPHPExcel->getSheetNames();

			$objPageSetup = new PHPExcel_Worksheet_PageSetup();
			$objPageSetup->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
			$objPageSetup->setFitToPage(true);
			$objPageSetup->setFitToHeight(1);
			$objPageSetup->setFitToWidth(1);
			$objPageSetup->setHorizontalCentered(true);
			$objPageSetup->setVerticalCentered(true);

			foreach ($sheets as $key=>$value)	{
				$objPHPExcel->setActiveSheetIndex(array_search($value, $sheets))->setShowGridlines(false);
				switch ($value)	{
					case "Tax Invoice":	{
						$objPageSetup->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
						$objPageSetup->setPrintArea("B2:C50");
						break;
					}
					case "Payment Certificate":	{
						$objPageSetup->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
						$objPageSetup->setPrintArea("B2:L88");
						break;
					}
					case "Time Based":
					case "Travel Time":
					case "Motor Vehicle":
					case "Car Hire":
					case "Flights":
					case "Accommodation":
					case "Other":
					case "Toll Gate":
					case "Prints & Copies":	{
						$objPageSetup->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
						break;
					}
					default:
						break;
				}
				$objPHPExcel->getActiveSheet()->setPageSetup($objPageSetup);
			}

			// SAVE PDF FILE
			$objWriter = new PHPExcel_Writer_PDF($objPHPExcel);
			$objWriter->setSheetIndex(2);
			//$objWriter->writeAllSheets();
			$objWriter->save(preg_replace("/.xlsx/",".pdf",preg_replace("/ /","_",$fileLocation.$fileName)));
			echo date('H:i:s') . "- Done writing files -";
*/
			break;
		}
		case "documentation":	{
			unset($files);

			$invoiceID = $_GET["invoice"];
			$invoiceName = q("SELECT name FROM LoadInvoices WHERE id = '".$invoiceID."'");

			$fileLocation = $file_directory;
			$fileName = "Backing Documentation - ".$invoiceName.".zip";

			$entries = q("SELECT expensesheet_id, id FROM ApprovedExpense WHERE invoiceID = '".$invoiceID."'");

			$i_pdf = 0;
			$i_image = 0;

			if (is_array($entries))	{
				foreach ($entries as $e)	{
					if (is_dir($file_directory.$e[0]))	{
						if ($handle = opendir($file_directory.$e[0]))	{
							/* This is the correct way to loop over the directory. */
							while (false !== ($entry = readdir($handle)))	{
								if ($entry != "." && $entry != "..")	{
									$ext = substr(strrchr($entry, "."), 1);

									if ($ext == "pdf")	{
										$pdfs[$i_pdf][0] = $file_directory.$e[0]."/".$entry;
										$pdfs[$i_pdf][1] = $entry;

										$i_pdf++;
									} else	{
										$images[$i_image][0] = $file_directory.$e[0]."/".$entry;
										$images[$i_image][1] = $entry;

										$i_image++;
									}
								}
							}

							closedir($handle);
						}
					}
				}
			}

			if (is_array($pdfs) || is_array($images))	{
				$zip = new ZipArchive();

				if ($zip->open(preg_replace("/ /","_",$fileLocation.$fileName), ZIPARCHIVE::CREATE) !== TRUE)
					exit("Cannot Open <".preg_replace("/ /","_",$fileLocation.$fileName).">. Contact ITE at support@integrityengineering.co.za\n");

				//BACKING DOCUMENTATION - PDF
				if (is_array($pdfs))	{
					$pdf = new PDFMerger;

					foreach ($pdfs as $pdf_file)
						$pdf->addPDF($pdf_file[0], "all");

					$pdf->merge("file", preg_replace("/.zip/",".pdf",preg_replace("/ /","_",$fileLocation.$fileName)));

					$zip->addFile(preg_replace("/.zip/",".pdf",preg_replace("/ /","_",$fileLocation.$fileName)), preg_replace("/.zip/",".pdf",preg_replace("/ /","_",$fileName)));
				}

				//BACKING DOCUMENTATION - JPG
				if (is_array($images))
					foreach ($images as $image)
						$zip->addFile($image[0], $image[1]);

				$zip->close();
			}

			break;
		}
		case "certificate_documentation":	{
			unset($files);

			$invoiceID = $_GET["invoice"];
			$invoiceName = q("SELECT name FROM LoadInvoices WHERE id = '".$invoiceID."'");

			$entries = q("SELECT expensesheet_id, id FROM ApprovedExpense WHERE invoiceID = '".$invoiceID."'");

			$i_pdf = 0;
			$i_image = 0;

			if (is_array($entries))	{
				foreach ($entries as $e)	{
					if (is_dir($file_directory.$e[0]))	{
						if ($handle = opendir($file_directory.$e[0]))	{
							/* This is the correct way to loop over the directory. */
							while (false !== ($entry = readdir($handle)))	{
								if ($entry != "." && $entry != "..")	{
									$ext = substr(strrchr($entry, "."), 1);

									if ($ext == "pdf")	{
										//echo "PDF [".$file_directory.$e[0]."/".$entry."]<br/>";
										$pdfs[$i_pdf][0] = $file_directory.$e[0]."/".$entry;
										$pdfs[$i_pdf][1] = $entry;

										$i_pdf++;
									} else	{
										//echo "JPG [".$file_directory.$e[0]."/".$entry."]<br/>";
										$images[$i_image][0] = $file_directory.$e[0]."/".$entry;
										$images[$i_image][1] = $entry;

										$i_image++;
									}
								}
							}

							closedir($handle);
						}
					}
				}
			}

			$fileLocation = $file_directory."certificates/";
			$fileName = "Payment Certificate - ".$invoiceID.".xlsx";

			if (is_array($pdfs) || is_array($images) || file_exists(preg_replace("/ /","_",$fileLocation.$fileName)))	{
				$fileLocation = $file_directory;
				$fileName = "Invoice Documentation - ".$invoiceName.".zip";

				$zip = new ZipArchive();

				if ($zip->open(preg_replace("/ /","_",$fileLocation.$fileName), ZIPARCHIVE::CREATE) !== TRUE)
					exit("Cannot Open <".preg_replace("/ /","_",$fileLocation.$fileName).">. Contact ITE at support@integrityengineering.co.za\n");

				//BACKING DOCUMENTATION - PDF
				if (is_array($pdfs))	{
					$pdf = new PDFMerger;

					foreach ($pdfs as $pdf_file)
						$pdf->addPDF($pdf_file[0], "all");

					$pdf->merge("file", preg_replace("/.zip/",".pdf",preg_replace("/ /","_",$fileLocation.$fileName)));

					$zip->addFile(preg_replace("/.zip/",".pdf",preg_replace("/ /","_",$fileLocation.$fileName)), preg_replace("/.zip/",".pdf",preg_replace("/ /","_",$fileName)));
				}

				//BACKING DOCUMENTATION - JPG
				if (is_array($images))
					foreach ($images as $image)
						$zip->addFile($image[0], $image[1]);

				//XLSX File
				$fileLocation = $file_directory."certificates/";
				$fileName = "Payment Certificate - ".$invoiceID.".xlsx";

				if (file_exists(preg_replace("/ /","_",$fileLocation.$fileName)))
					$zip->addFile(preg_replace("/ /","_",$fileLocation.$fileName), preg_replace("/ /","_",$fileName));

				$zip->close();

				$fileLocation = $file_directory;
				$fileName = "Invoice Documentation - ".$invoiceName.".zip";
			}

			break;
		}
		case "files":	{
			unset($pdfs);
			unset($images);

			$expenseID = $_GET["expense"];
			//$expenseID = (strpos($expenseID,",") !== false) ? preg_split("/,/",$expenseID) : $expenseID;
			$expenseID = preg_split("/,/",$expenseID);
			$expenseID = fixarr($expenseID);

			$fileLocation = $file_directory;
			$fileName = "Backing Documentation - ".date("Ymd His").".zip";

			$i_pdf = 0;
			$i_image = 0;

			if (is_array($expenseID ))	{
				foreach ($expenseID  as $eID)	{
					if (is_dir($file_directory.$eID))	{
						if ($handle = opendir($file_directory.$eID))	{
							/* This is the correct way to loop over the directory. */
							while (false !== ($entry = readdir($handle)))	{
								if ($entry != "." && $entry != "..")	{
									$ext = substr(strrchr($entry, "."), 1);

									if ($ext == "pdf")	{
										$pdfs[$i_pdf][0] = $file_directory.$eID."/".$entry;
										$pdfs[$i_pdf][1] = $entry;

										$i_pdf++;
									} else	{
										$images[$i_image][0] = $file_directory.$eID."/".$entry;
										$images[$i_image][1] = $entry;

										$i_image++;
									}
								}
							}

							closedir($handle);
						}
					}
				}
			}

			if (is_array($pdfs) || is_array($images))	{
				$zip = new ZipArchive();

				if ($zip->open(preg_replace("/ /","_",$fileLocation.$fileName), ZIPARCHIVE::CREATE) !== TRUE)
					exit("Cannot Open <".preg_replace("/ /","_",$fileLocation.$fileName).">. Contact ITE at support@integrityengineering.co.za\n");

				if (is_array($pdfs))	{
					$pdf = new PDFMerger;

					foreach ($pdfs as $pdf_file)
						$pdf->addPDF($pdf_file[0], "all");

					$pdf->merge("file", preg_replace("/.zip/",".pdf",preg_replace("/ /","_",$fileLocation.$fileName)));

					$zip->addFile(preg_replace("/.zip/",".pdf",preg_replace("/ /","_",$fileLocation.$fileName)), preg_replace("/.zip/",".pdf",preg_replace("/ /","_",$fileName)));
				}

				if (is_array($images))
					foreach ($images as $image)
						$zip->addFile($image[0], $image[1]);

				$zip->close();
			}

			break;
		}
		default:	{
			exit;
			break;
		}
	}

	header("Content-type:".content_type(preg_replace("/ /","_",$fileName)));
	header("Content-disposition:attachment;filename=".preg_replace("/ /","_",$fileName));

	readfile(preg_replace("/ /","_",$fileLocation.$fileName));
	//echo "FILE [".preg_replace("/ /","_",$fileLocation.$fileName)."]<br/>";
?>
