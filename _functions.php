<?php
    session_start();

    function approvalsDeclined($projectName, $companyName, $recipients)  {
		global $urlLink;

        //  Get To/From Email Addresses
        $to = $recipients;
        $from = "On Track Notification <support@integrityengineering.co.za>";
        $reply_to = "On Track Support <support@integrityengineering.co.za>";

        $SPManager = q("SELECT frstname, lstname FROM Employee WHERE id = '".$_SESSION["user_id"]."'");
        $SPManager = $SPManager[0][0]." ".$SPManager[0][1];

        //  Create Message Details
        $subject = "On-Track: Time/Expenses Declined";
        $msg = "<html>\n".
                "<head>\n".
                    "<title>On-Track: Time/Expenses Declined</title>\n".
                    "<style>\n".
                        "<!--\n".
                        "body {\n".
                            "width:100%;\n".
                            "min-width:1000px;\n".
                            "height:100%;\n".
                            "margin:0;\n".
                            "padding:0;\n".
                            "font-size:12px;\n".
                            "font-family:Meriad,Verdana,Arial,Helvetica,sans-serif;\n".
                        "}\n".
                        "a {color:#A56C02;text-decoration:none;}\n".
                        "-->\n".
                    "</style>\n".
                "</head>\n".
                "<body style='background-image:url(".$urlLink."CSSFiles/im/bg.jpg) repeat #000;'>\n".
                    "<center>\n".
                        "<div style='height:500px; background-image:url(".$urlLink."CSSFiles/im/status.png);'>\n".
                            "<br>\n".
                            "<img src ='".$urlLink."CSSFiles/im/logo.png'/>\n".
                            "<div style='text-align:justify;line-height:20px;width:800px;'>\n".
                                "Please note that the shared project manager has declined a time/expense entry for company ".$companyName." on project ".$projectName.".<br/>\n".
                                "To view the declined time/expense entry, \n<a href='".$urlLink."_module_handler.php?module=approval&what=report&type=CP'>Click Here</a>\n".
                                "<br/><br/>\n".
                                "Regards,<br/>\n".
                                $SPManager."<br/>\n".
                            "</div>\n".
                        "</div>\n".
                    "</center>\n".
                "</body>\n".
            "</html>";

        //$to = "Mauritz Kruger <mkruger@integrityengineering.co.za>,Elrich Faul <efaul@integrityengineering.co.za>,Franszo Faul <ffaul@integrityengineering.co.za>";
        if ($to != "")  sendmail($to, $subject, $msg, $from, $reply_to, true);

        return;
    }

    function sp_Invite($projectID, $projectName, $companyID)  {
		global $urlLink;

        $url = $urlLink."scripts/";

        $webLink = $urlLink."_statusUpdate.php?projectID=".$projectID."&companyID=".$companyID;

        $url1 = "sp_status.php?projectID=".$projectID."&companyID=".$companyID."&status=acceptBoth";    //  Accept Both Company && Team Participation
        $url2 = "sp_status.php?projectID=".$projectID."&companyID=".$companyID."&status=acceptCompany"; //  Accept Company Participation, Decline Team Participation
        $url3 = "sp_status.php?projectID=".$projectID."&companyID=".$companyID."&status=decline";       //  Decline Company Participation

        //  Get To/From Email Addresses
        $to = "";
        $from = "On Track Notification <support@integrityengineering.co.za>";
        $reply_to = "On Track Support <support@integrityengineering.co.za>";

        $users = q("SELECT DISTINCT(e.id), CONCAT(e.frstname, ' ', e.lstname, '<', e.email, '>') ".
                    "FROM (((Employee AS e INNER JOIN user_role AS ur ON ur.userid = e.id) INNER JOIN role_action AS ra ON ur.roleid = ra.roleid) ".
                        "INNER JOIN roles AS r ON r.id = ra.roleid) ".
                    "WHERE ur.companyid = '".$companyID."' AND r.companyid = '".$companyID."' AND r.role = 'MAN' AND e.email != 'admin' AND e.deleted = '0' ORDER BY e.lstname, e.frstname, e.email");

        if (is_array($users)) {
            foreach ($users as $user) {
                if ($user[1] == "Johan Coertzen <jc@ite.co.za>")
                    $to .= ",Johan Coertzen <jc@iteonline.co.za>";
                else if ($user[1] == "Franszo Faul <ffaul@ite.co.za>")
                    $to .= ",Franszo Faul <ffaul@iteonline.co.za>";
                else if ($user[1] == "Jp de Beer <jpdb@ite.co.za>")
                    $to .= ",Jp de Beer <jpdb@iteonline.co.za>";
                else if ($user[1] == "Mauritz Kruger <mkruger@ite.co.za>")
                    $to .= ",Mauritz Kruger <mk@iteonline.co.za>";
                else
                    $to .= ",".$user[1];
            }
        }

        if ($to[0] == ",")
            $to = substr($to, 1);

        $SPManager = q("SELECT frstname, lstname FROM Employee WHERE id = '".$_SESSION["user_id"]."'");
        $SPManager = $SPManager[0][0]." ".$SPManager[0][1];

        //  Create Message Details
        $subject = "On-Track: Shared Project Invitation";
        $msg = "<html>\n".
                "<head>\n".
                    "<title>On-Track: Shared Project Invitation</title>\n".
                    "<style>\n".
                        "<!--\n".
                        "body {\n".
                            "width:100%;\n".
                            "min-width:1000px;\n".
                            "height:100%;\n".
                            "margin:0;\n".
                            "padding:0;\n".
                            "font-size:12px;\n".
                            "font-family:Meriad,Verdana,Arial,Helvetica,sans-serif;\n".
                        "}\n".
                        "a {color:#A56C02;text-decoration:none;}\n".
                        "-->\n".
                    "</style>\n".
                "</head>\n".
                "<body style='background-image:url(".$urlLink."CSSFiles/im/bg.jpg) repeat #000;'>\n".
                    "<center>\n".
                        "<div style='height:500px; background-image:url(".$urlLink."CSSFiles/im/status.png);'>\n".
                            "<br>\n".
                            "<img src ='".$urlLink."CSSFiles/im/logo.png'/>\n".
                            "<div style='text-align:justify;line-height:20px;width:800px;'>\n".
                                "<center>\n".
                                "<a href='".$webLink."' target='_blank'>View Online</a>\n".
                                "</center>\n".
                                "<br/>\n".
                                "Please note that you've been selected to participate in a shared project on On-Track.<br/>\n".
                                "<br/>\n".
                                "<strong>Project Name:</strong> ".$projectName."<br/>\n".
                                "<strong>Managing Company:</strong> ".$_SESSION["company_name"]."<br/>\n".
                                "<br/>\n".
                                "You need to respond to this by clicking on one of the following links:<br/>\n".
                                "<br/>\n".
                                "To participate in the project and give the shared project manager the privilege of managing your project team, \n".
                                "<a href='".$url.$url1."'>Click Here</a>\n".
                                "<br/>\n".
                                "To participate in the project, but manage your own project team, \n".
                                "<a href='".$url.$url2."'>Click Here</a>\n".
                                "<br/>\n".
                                "If you choose not to participate in the project, \n".
                                "<a href='".$url.$url3."'>Click Here</a>\n".
                                "<br/><br/>\n".
                                "Regards,<br/>\n".
                                $SPManager."<br/>\n".
                            "</div>\n".
                        "</div>\n".
                    "</center>\n".
                "</body>\n".
            "</html>";

        //$to = "Mauritz Kruger <mkruger@integrityengineering.co.za>,Elrich Faul <efaul@integrityengineering.co.za>,Franszo Faul <ffaul@integrityengineering.co.za>";
        if ($to != "")  sendmail($to, $subject, $msg, $from, $reply_to, true);

        return;
    }

    //  Replace special chars for fusion charts
    function removeSC($str)	{
		$str = str_replace("%","%25",$str);
		$str = str_replace("&","%26",$str);
		$str = str_replace("'","`",$str);
		$str = str_replace("\"","%22",$str);
		$str = str_replace(",","%2C",$str);
		$str = str_replace("-","%2D",$str);
		$str = str_replace(".","%2E",$str);
		$str = str_replace("/","%5C%2F",$str);
		$str = str_replace(":","%3A",$str);
		$str = str_replace("(","%28",$str);
		$str = str_replace(")","%29",$str);
		return $str;
    }

    //  Create Budget Report For Message...
    function createMessageReport($year, $month, $company_id) {
        //  Set Table Border & Background Details
        $top = "border-top: 1px solid #336699;";
        $left = "border-left: 1px solid #336699;";
        $right = "border-right: 1px solid #336699;";
        $bottom = "border-bottom: 1px solid #336699;";
        $background = "background-color:#6699CC;";

        $displayString = "";

        $headers = "<tr>".
                    "<td align='center' style='".$top.$left.$bottom.$background."'><a>&nbsp;Category&nbsp;</a></td>".
                    "<td align='center' style='".$top.$left.$bottom.$background."'><a>&nbsp;Element&nbsp;</a></td>".
                    "<td align='center' style='".$top.$left.$bottom.$background."'><a>&nbsp;Planned&nbsp;<br/>".
                        "&nbsp;Amount <i>(".$_SESSION["currency"].")</i>&nbsp;</a></td>".
                    "<td align='center' style='".$top.$left.$bottom.$background."'><a>&nbsp;Actual&nbsp;<br/>".
                        "&nbsp;Amount <i>(".$_SESSION["currency"].")</i>&nbsp;</a></td>".
                    "<td align='center' style='".$top.$left.$bottom.$background."'><a>&nbsp;Paid&nbsp;</a></td>".
                    "<td align='center' style='".$top.$left.$right.$bottom.$background."'><a>&nbsp;Date Payable&nbsp;</a></td></tr>";

        $grand_total_planned = q("SELECT SUM(planned) FROM Budget WHERE year = '".$year."' AND month = '".$month."' AND company_id = '".$_SESSION["company_id"]."'");
        $grand_total_amount = q("SELECT SUM(amount) FROM Budget WHERE year = '".$year."' AND month = '".$month."' AND company_id = '".$_SESSION["company_id"]."'");
        $grand_total_actual = q("SELECT SUM(amount) FROM Budget WHERE year = '".$year."' AND month = '".$month."' AND company_id = '".$_SESSION["company_id"]."' ".
                                "AND actual = '1'");

        if ($grand_total_planned == "")
            $grand_total_planned = "-";

        if ($grand_total_amount == "")
            $grand_total_amount = "-";

        if ($grand_total_actual == "")
            $grand_total_actual = "-";

        $categories = q("SELECT DISTINCT(c.id), c.name FROM (Category AS c INNER JOIN Elements AS e ON c.id = e.parent_id) WHERE e.status = '1' ".
                        "AND c.company_id = '".$_SESSION["company_id"]."' ORDER BY c.name");

        if (is_array($categories)) {
            foreach ($categories as $category) {
                $elements = q("SELECT id, name, status FROM Elements WHERE parent_id = '".$category[0]."' AND status = '1' ORDER BY name");

                $displayString .= "<tr>";
                $displayString .= "<td align='left' style='".$left.$bottom."padding-top:3px;' valign='top'>";
                $displayString .= "&nbsp;".$category[1]."&nbsp;";
                $displayString .= "</td>";

                if (is_array($elements)) {
                    $displayString .= "<td align='left' style='".$left.$bottom."'>";
                    foreach ($elements as $element) {
                        $displayString .= "&nbsp;".$element[1]."</a>&nbsp;<br/>";
                    }
                    $displayString .= "</td>";
                    //  Planned Amount
                    $displayString .= "<td align='right' style='".$left.$bottom."'>";
                    foreach ($elements as $element) {
                        $amount = q("SELECT planned FROM Budget WHERE year = '$year' AND month = '$month' AND element_id = '".$element[0]."'");

                        if ($amount)
                            $displayString .= "".$_SESSION["currency"]." ".number_format($amount, 2, ".", "")."<br/>";
                        else
                            $displayString .= "-<br/>";
                    }
                    $displayString .= "</td>";
                    //  Actual Amount
                    $displayString .= "<td align='right' style='".$left.$bottom."'>";
                    foreach ($elements as $element) {
                        $amount = q("SELECT amount FROM Budget WHERE year = '$year' AND month = '$month' AND element_id = '".$element[0]."'");

                        if ($amount)
                            $displayString .= "".$_SESSION["currency"]." ".number_format($amount, 2, ".", "")."<br/>";
                        else
                            $displayString .= "-<br/>";
                    }
                    $displayString .= "</td>";
                    //  Paid
                    $displayString .= "<td align='right' style='".$left.$bottom."'>";
                    foreach ($elements as $element) {
                        $bool = q("SELECT actual FROM Budget WHERE year = '$display_year' AND month = '$display_month' AND element_id = '".$element[0]."'");

                        if ($bool == "1")
                            $displayString .= "Yes<br/>";
                        else
                            $displayString .= "No<br/>";
                    }
                    $displayString .= "</td>";
                    //  Date
                    $displayString .= "<td align='right' style='".$left.$right.$bottom."'>";
                    foreach ($elements as $element) {
                        $due_date = q("SELECT due_date FROM Budget WHERE year = '$year' AND month = '$month' AND element_id = '".$element[0]."'");

                        if ($due_date)
                            $displayString .= "".$due_date."<br/>";
                        else
                            $displayString .= "-<br/>";
                    }
                    $displayString .= "</td>";
                }
                else {
                    $displayString .= "<td align='left' style='".$left.$bottom.$right."'>";
                    $displayString .= "&nbsp;-&nbsp;";
                    $displayString .= "</td>";
                }
                $displayString .= "</tr>";
            }
        }

        if ($displayString != "")
            $displayString = $headers.$displayString."<tr>".
                                "<td align='right' colspan='2' style='".$top.$left.$bottom.$background."'><a>Grand Total <i>(".
                                    $_SESSION["currency"].")</i>:&nbsp;</a></td>".
                                "<td align='right' style='".$top.$left.$bottom."'>".$_SESSION["currency"]." ".number_format((double)$grand_total_planned, 2, ".", "")."</td>".
                                "<td align='right' style='".$top.$left.$bottom."'>".$_SESSION["currency"]." ".number_format((double)$grand_total_amount, 2, ".", "")."</td>".
                                "<td align='right' style='".$top.$left.$bottom."'>".$_SESSION["currency"]." ".number_format((double)$grand_total_actual, 2, ".", "")."</td>".
                                "<td align='right' style='".$top.$left.$right.$bottom.$background."'>&nbsp;</td></tr>";

        if ($displayString != "")
            $generated = "1";

        $file_name = "".date("Ymd")."_-_Budget_Report.xls";
        $displayString = "header('Expires: 0');".
                            "header('Cache-Control: must-revalidate, post-check=0, pre-check=0');".
                            "header('Content-Type: application/vnd.ms-excel');".
                            "header('Content-Disposition: attachment; filename='.$file_name);".
                            "<html><head><title>Excel Spreadsheet</title><meta http-equiv='Content-Type' ".
                            "content='text/html; charset=iso-8859-1'></head><body><table cellpadding='0' ".
                            "cellspacing='0'><tr><td align='center' colspan='6'>Budget created/updated for ".
                            $year."-".$month." by ".$_SESSION["email"]." on ".date("Y-m-d")." at ".date("H:i:s").
                            "</td></tr>".$displayString."</table></body></html>";

        if ($generated == "1")
            return "".$displayString;
        else
            return "";
    }

	function p($s)	{echo "$s<br>\n";}
    //  Save Expense Status Once Per Month - On The 1'st Of Every Month
    function statusSaveMonthly($date)	{
        //  Check for Status Before Date
        $companies												= q("SELECT id, name FROM Company ORDER BY id");
        if (is_array($companies)) {
            foreach ($companies as $company) {
                $balance = 0;
                $tempDate										= q("SELECT id, date FROM ExpenseStatus WHERE date < '$date' AND ".
																	"company_id = '".$company[0]."' ORDER BY date DESC");
                $tempDate = $tempDate[0][1];
                //  Expense Balance @ Start Date
				$dateFrom = "";
                if ($tempDate != 0)								$dateFrom = "AND li.due_date >= '$tempDate'";
				$balance										= q("SELECT amount FROM ExpenseStatus WHERE date = '$tempDate' ".
																	"AND company_id = '".$company[0]."'");
				$invoiced										= q("SELECT SUM(li.expenses + (li.expenses * (li.vat / 100))), ".
																	"SUM(li.diverse_income + (li.diverse_income * (li.vat / 100))) FROM (LoadInvoices AS li ".
																	"INNER JOIN Project AS p ON p.id = li.project_id) WHERE ".
																	"li.due_date < '$date' AND p.company_id = '".$company[0]."' $dateFrom ");
				$budget_expenses								= q("SELECT SUM(amount + (amount * (vat / 100))) FROM ExpenseBudget AS li ".
																	"WHERE due_date < '$date' AND company_id='".$company[0]."' $dateFrom ");
				$balance										+= ($invoiced[0][0] - $invoiced[0][1] - $budget_expenses);
                //  Save Status If Not Yet Inserted
				$delete = q("DELETE FROM ExpenseStatus WHERE date = '$date' AND company_id = '$company[0]'");
				$insert											= q("INSERT INTO ExpenseStatus (date, amount, company_id) ".
                                                             		"VALUES ('$date', '".number_format($balance, 2, ".", "")."', '$company[0]')");
            }
        }
        return;
    }

    //  Recalculate Status And Save
    //  $type could be "invoice" or "expense"
    function statusUpdateDate($old_date, $new_date, $type, $amount, $company_id)	{
        if ($old_date > $new_date)	{
            $d = "date > '$new_date' AND date <= '$old_date'";
            $sign = ($type == "invoice") ? "+" : "-";
        }
        else	{
            $d = "date > '$old_date' AND date <= '$new_date'";
            $sign = ($type == "invoice") ? "-" : "+";
        }

        $update = q("UPDATE ExpenseStatus SET amount = amount + ($sign$amount) WHERE $d AND company_id = '$company_id'");
        return;
    }

    //  Recalculate Status And Save
    function statusUpdateAmount($date, $amount, $company_id)	{
        $update = q("UPDATE ExpenseStatus SET amount = amount + ($amount) WHERE date > '$date' AND company_id = '$company_id'");
    }

    function budgetSpawn($company_id = "", $datePassed = "") {
        if ($company_id != "")  $companies = q("SELECT id, name FROM Company WHERE id = $company_id ORDER BY id");
        else                    $companies = q("SELECT id, name FROM Company ORDER BY id");

        if (is_array($companies)) {
            foreach ($companies as $c) {
                if ($datePassed != "")  $date = $datePassed;
                else                    $date = date("Y-m-d");

                for ($i = 0; $i < 3; $i++) {
                    $year = substr($date, 0, strpos($date, "-"));
                    $month = substr($date, (strpos($date, "-") + 1), 2);
                    $prevDate = getPreviousMonth($year, $month);

                    $exists = q("SELECT * FROM Budget WHERE year = '".$year."' AND month = '".$month."' AND company_id = '".$c[0]."'");

                    if (!is_array($exists)) {
                        $categories = q("SELECT DISTINCT(c.id), c.name FROM (Category AS c INNER JOIN Elements AS e ON c.id = e.parent_id) WHERE c.company_id = '".$c[0]."' AND e.status = '1'");

                        if (is_array($categories)) {
                            foreach ($categories as $category) {
                                $elements = q("SELECT id, name FROM Elements WHERE parent_id = '".$category[0]."' AND status = '1'");

                                if (is_array($elements)) {
                                    foreach ($elements as $element) {
                                        $testYear = substr($prevDate, 0, strpos($prevDate, "-"));
                                        $testMonth = substr($prevDate, (strpos($prevDate, "-") + 1), 2);

                                        $info = q("SELECT planned, amount, due_date FROM Budget WHERE year = '$testYear' AND month = '$testMonth' AND element_id = '".$element[0]."'");

                                        if (is_array($info)) {
                                            if ($c[0] != "1" && $c[0] != "52")     {
                                                $planned = $info[0][0];
                                                $amount = $info[0][0];          //  @ Spawn, Actual = Planned
                                                $due_date = ($info[0][2] != "") ? getDates($info[0][2], 30) : "";
                                            } else      {
                                                $planned = $info[0][1];         //  @ Spawn, Planned = Actual
                                                $amount = "0.00";
                                                $due_date = ($info[0][2] != "") ? date("Y-m-d", strtotime("+1 month", strtotime($info[0][2]))) : "";
                                            }

                                            $insert = q("INSERT INTO Budget (year, month, element_id, planned, amount, due_date, company_id) ".
                                                        "VALUES ('$year', '$month', '".$element[0]."', '".$planned."', '".$amount."', '$due_date', '".$c[0]."')");
                                        } else  {
                                            //   Budget doesn't exist yet, create one
                                            $planned = "0.00";
                                            $amount = "0.00";
                                            $due_date = "";

                                            $insert = q("INSERT INTO Budget (year, month, element_id, planned, amount, due_date, company_id) ".
                                                        "VALUES ('$year', '$month', '".$element[0]."', '".$planned."', '".$amount."', '$due_date', '".$c[0]."')");
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $date = getNextMonthStart($date, 1);
                }
            }
        }

        return;
    }

    function status($date = "", $amount, $company_id) {
        if ($date == "" && date("Y-m-d") == date("Y-m-01")) {
//            statusSaveMonthly(date("Y-m-d"));
            budgetSpawn();
        }
        else if ($date != "")
            statusUpdateAmount($date, $amount, $company_id);

        return;
    }

    function updateDueDates($company_id) {
        $today = date("Y-m-d");

        $due_date = date("Y-m-d", strtotime("+7 days", strtotime($today)));

        //  Invoices
        $update = q("UPDATE LoadInvoices SET due_date = '".$due_date."' ".
                    "WHERE project_id IN (SELECT id FROM Project WHERE company_id = '".$company_id."') AND paid = '0' AND due_date <= '".$today."' AND company_id = '".$company_id."'");

        //  Expenses
        $update = q("UPDATE ExpenseBudget SET due_date = '".$due_date."' ".
                    "WHERE project_id IN (SELECT id FROM Project WHERE company_id = '".$company_id."') AND paid = '0' AND due_date <= '".$today."' AND company_id = '".$company_id."'");

        $insert = q("INSERT INTO Events (date,time,message,notify_id,processed,user_id,company_id) VALUES ('".date("Y-m-d")."','".date("H:i:s")."','Due Date Update','0','1','1','".$company_id."')");

        return;
    }

    // Excel Exports
     //  Create Zip File
    function createZip($zipFileName, $fileToZip) {
		set_time_limit(0);
        $zipFileName = str_replace(" ", "_", $zipFileName);
        $fileToZip = str_replace(" ", "_", $fileToZip);

		$zip = new ZipArchive();

		if ($zip->open("data/".$zipFileName, ZIPARCHIVE::CREATE) !== TRUE)
			exit("Cannot Open <data/".$zipFileName.">. Contact ITE at support@integrityengineering.co.za\n");

		$zip->addFile("data/".$fileToZip);
		$zip->close();

		return;
	}

	//  Create Excel File
    function createExport($fileName, $fileData, $fileType) {
		set_time_limit(0);

        $fileName = str_replace(" ", "_", $fileName);

		if (!is_array($fileData))
			return "";

		//  Columns Array
		$columns = array("","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");

		/** Error reporting */
		error_reporting(E_ALL);

		/** PHPExcel */
		include_once("include/PHPExcel.php");

		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		// Set properties
		$objPHPExcel->getProperties()->setTitle($fileName);

		// Create a first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		//  Add Column Headings/Data
		$row = 1;
		$cols = count($fileData[0]);

		foreach ($fileData as $d)	{
                    for ($i = 0; $i < $cols; $i++)	{
                        $color = "";

                        $cell = "".$columns[($i / 26)].$columns[(($i % 26) + 1)].$row;

                        $value = "";

                        if (is_array($d[$i]))	{
                            $valCount = count($d[$i]) - 1;
                            $valC = 0;
                            foreach ($d[$i] as $v)	{
                                if ($valC < $valCount)  $value .= $v."\n";
                                else                    $value .= $v;

                                $valC++;
                            }
                        }
                        else    {
                            if (strpos($d[$i], "_#_") !== false)	{
                                $tmp = preg_split("/_#_/", $d[$i]);
                                $cls = ($tmp[0] != "") ? $tmp[0] : "";
                                $val = $tmp[1];

                                $value .= $val;

                                $color = getARGBColor($cls);
                            }
                            else    $value .= $d[$i];
                        }

                        $objPHPExcel->getActiveSheet()->setCellValue($cell, $value);
                        $objPHPExcel->getActiveSheet()->getStyle($cell)->getAlignment()->setWrapText(true);
                        $objPHPExcel->getActiveSheet()->getStyle($cell)->getAlignment()->setWrapText(true);

                        // Set autowidth of column
                        $col = "".$columns[($i / 26)].$columns[(($i % 26) + 1)];
                        $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);

                        if ($color != "")
                            $objPHPExcel->getActiveSheet()->getStyle($cell)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($color);
                    }

                    $row++;
		}

		// Add borders to field
		$styleArray = array(
			"borders" => array(
				"allborders" => array(
					"style" => PHPExcel_Style_Border::BORDER_THIN,
				),
			),
		);

		$cell = "A1:".$columns[($cols / 26)].$columns[(($cols % 26))].--$row;

		$objPHPExcel->getActiveSheet()->getStyle($cell)->applyFromArray($styleArray);

		// Freeze panes
		$objPHPExcel->getActiveSheet()->freezePane("A2");

		// Rows to repeat at top
		$objPHPExcel->getActiveSheet()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(1, 1);

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		$fileName = $fileName.".".$fileType;

		// Delete File If Exists On Server
		if (file_exists("data/".$fileName))	{
			unlink("data/".$fileName);
		}

		// Save Excel File
		if ($fileType == "xls" || $fileType == "xlsx")	{
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel5");
			$objWriter->save("data/".$fileName);
		}
		else if ($fileType == "csv")	{
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "CSV")
				->setDelimiter(",")
				->setEnclosure("")
				->setLineEnding("\r\n")
				->setSheetIndex(0)
				->save("data/".$fileName);
		}

		return $fileName;
    }

  	/////////////////////////////////////////////////////////////////
	//MenuIndex creation functions
  	/////////////////////////////////////////////////////////////////
  	$menus_ = array();
    function getSubMenus_($parentID) {	//this function has actions assosiated with menu items
        global $menus_;
		$subMenus = q("SELECT m.id,m.parentID,m.level,m.ranking,m.urlLink,m.menuDescr,m.icon,a.action ".
						"FROM (menus AS m INNER JOIN menuAction AS ma ON m.id = ma.menuID INNER JOIN actions AS a ON a.id = ma.actionID) ".
                        "WHERE m.parentID = '".$parentID."' AND m.menuType = 'MAIN' ORDER BY m.ranking");
        if (is_array($subMenus))
            foreach ($subMenus as $subMenu)     {
				$menus_[] = $subMenu;
				getSubMenus_($subMenu[0]);
            }
	}
    function getMenus_()   {
		global $menus_;
		$parentID = 0;
		$menuItems=q("SELECT id,parentID,level,ranking,urlLink,menuDescr,icon FROM menus WHERE parentID='".$parentID."' AND menuType='MAIN' ORDER BY ranking");
		if (is_array($menuItems))
			foreach ($menuItems as $menuItem)  {
				$menus_[] = $menuItem;
				getSubMenus_($menuItem[0]);
			}
//		$data = array();
//		foreach ($menus as $m)
//			if (!in_array($m, $data))	$data[] = $m;
//		$menus = $data;
		return $menus_;
	}
	function hasAccessRebuild($a,$e)	{	if (!is_array($a))      return 0; return in_array($e,$a);	}
	function addMenuIndex($menus,$comp_id,$user_id,$username)	{
		$lookup = array();
		$new = array();
                if ($username == "admin")
                    $actions = q("SELECT `action` FROM actions");
                else
                    $actions = q("SELECT a.`action` FROM (((roles AS r INNER JOIN role_action AS ra ON r.id = ra.roleid) INNER JOIN user_role AS ur ".
                                    "ON r.id = ur.roleid) INNER JOIN actions AS a ON ra.actionid = a.id) ".
                                    "WHERE ur.companyid = '$comp_id' AND ra.companyid = '$comp_id' AND r.companyid = '$comp_id' AND ur.userid = '$user_id'");

		if (is_array($actions))	foreach($actions as $a)	$new[] = $a[0];	$actions = $new;//fix array for hasAccessRebuild function
		$sql = "INSERT INTO MenuIndex (company_id,user_id,menu_id) VALUES ";
		$origsql = "INSERT INTO MenuIndex (company_id,user_id,menu_id) VALUES";
		$skipped = 100;	//nested menu items to be skipped if parents dont have access
		$bool = 1;
		foreach($menus as $m)	{
			if (!isset($m[7]))	$m[7] = "";
//			for($i=0;$i<$m[2];++$i)	echo "\t";
//			echo $m[5]." - ".$m[0]." - ".$m[2]." - ".$m[7]." - $skipped - ";
			$access = hasAccessRebuild($actions,$m[7]);
			if ($skipped > $m[2])	{$bool = 1;$skipped = 100;}
			if ($bool)
			if (!($username == "admin" || $m[2] == 0 || $access))
				{$bool = 0;$skipped = $m[2];}
			if ($bool || $m[2] <= $skipped)	{
				if ($access || $m[2] == 0)	{
//					if ($m[5] == "On-Track Admin")
//					   if ($username != "admin")	continue;
					if (!in_array($m[0],$lookup))	{
						if ($m[2] == 0)	{
							$check = 0;
							$menuActions = q("SELECT a.action,a.id FROM menuAction AS ma INNER JOIN actions AS a ON a.id=ma.actionID WHERE ma.menuID='$m[0]'");
							foreach($menuActions as $ma)
								if (hasAccessRebuild($actions,$ma[0]))	{$check = 1;break;}
							if ($check == 1)
								$sql .= "($comp_id,$user_id,$m[0]),";
						}
						else
							$sql .= "($comp_id,$user_id,$m[0]),";
						$lookup[] = $m[0];
//						echo "ADDED ADDED ADDED ADDED ADDED ADDED ADDED ADDED ADDED ADDED ADDED \n";
					}
//					else	echo "SKIPPED ALREADY ADDED\n";
					++$skipped;
				}
//				else	echo "SKIPPED NO ACCESS\n";
			}
//			else	echo "SKIPPED OUTER\n";
//			if ($m[5] == "Finances")	exit();
		}
		$sql = substr($sql,0,-1);
		if ($origsql != $sql)	q($sql);	//if no menu items added, don't add menuIndex record
		return count($lookup);	//return the amount of menu items we added indexes for
	}
	function updateMenuIndex($cID,$uID)	{
		$username = q("SELECT email,id FROM Employee WHERE id = '$uID'");
		$menus = getMenus_();
		$drop = q("DELETE FROM MenuIndex WHERE company_id = $cID AND user_id = $uID");
		$lookup = addMenuIndex($menus, $cID, $uID, $username[0][0]);
	}

	function deleteDir($dir)	{
		if (substr($dir, strlen($dir) - 1, 1) != "/")
			$dir .= "/";

		if ($handle = opendir($dir))	{
			while ($obj = readdir($handle))	{
				if ($obj != "." && $obj != "..")	{
					if (is_dir($dir.$obj))	{
						if (!deleteDir($dir.$obj))
							return false;
					} elseif (is_file($dir.$obj))	{
						if (!unlink($dir.$obj))
							return false;
					}
				}
			}

			closedir($handle);

			if (!@rmdir($dir))
				return false;

			return true;
		}

		return false;
	}
	/////////////////////////////////////////////////////////////////

?>
