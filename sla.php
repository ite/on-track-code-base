<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("include/sajax.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

	if (!hasAccess("SLA_MANAGE"))
		header("Location: noaccess.php");

    ///////////////////////////
    //  Sajax
    function getLimit($id)
    {
        return q("SELECT time_limit FROM SLA WHERE project_id = '$id'");
    }

    $sajax_request_type                                                 = "GET";
    sajax_init();
    sajax_export("getLimit");
    sajax_handle_client_request();
    ///////////////////////////

    //  If Delete Was Clicked
    if (isset($_POST["deleteBox"]) && $_POST["deleteBox"] === "1")
    {
	$id                                                             = $_POST["idBox"];
        $project_id                                                     = q("SELECT project_id FROM SLA WHERE id = '$id'");
        $delete                                                         = q("DELETE FROM SLA WHERE id = '$id'");

        if ($delete)
        {
            $time                                                       = date("H:i:s");

            $project_name                                               = q("SELECT name FROM Project WHERE id = '$project_id'");

            $logs                                                       = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('SLA Agreement for ".$project_name." deleted', 'Delete', 'SLA', ".
                                                                            "'".$_SESSION["email"]."', '$today', '$time', '".$_SESSION["company_id"]."')");

            $errorMessage                                               = "SLA Agreement For ".$project_name." Removed Successfully";
        }
    }

    //  Add/Update SLA Button Pressed
    if (isset($_POST["save"]) && $_POST["save"] === "1")
    {
        $project_id                                                     = $_POST["project"];
        $limit                                                          = addslashes(strip_tags($_POST["limit"]));

        //  Check If SLA Agreement Exists In Database
        $exists                                                         = q("SELECT id FROM SLA WHERE project_id = '$project_id'");

        //  If Exists - Update
        if ($exists)
        {
            $update                                                     = q("UPDATE SLA SET time_limit = '$limit' WHERE project_id = '$project_id'");

            if ($update)
            {
                $project_name                                           = q("SELECT name FROM Project WHERE id = '$project_id'");
                $time                                                   = date("H:i:s");
                $logs                                                   = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('SLA Agreement for ".$project_name." updated', 'Update', 'SLA', ".
                                                                            "'".$_SESSION["email"]."', '$today', '$time', '".$_SESSION["company_id"]."')");

                $errorMessage                                           = "SLA Agreement For ".$project_name." Updated Successfully";
            }
        }
        else
        {
            $insert                                                     = q("INSERT INTO SLA (project_id, time_limit) VALUES ('$project_id', '$limit')");

            if ($insert)
            {
                $project_name                                           = q("SELECT name FROM Project WHERE id = '$project_id'");
                $time                                                   = date("H:i:s");
                $logs                                                   = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('SLA Agreement for ".$project_name." created', 'Insert', 'SLA', ".
                                                                            "'".$_SESSION["email"]."', '$today', '$time', '".$_SESSION["company_id"]."')");

                $errorMessage                                           = "SLA Agreement For ".$project_name." Created Successfully";
            }
        }
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0", "sla");

    if ($errorMessage != "")
    {
        echo "<p  class='centerdata' style='padding:0px;'><strong><font class='on-validate-error'>$errorMessage</font></strong></p>";
        echo "<br/>";
    }
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Sajax
    <?php sajax_show_javascript(); ?>

    function check()
    {
        var valid                                                       = 1;

        //  Check That Project Is Selected
        if (document.forms["sla"].project.value != "null")
        {
            //  Check That Time Limit Is Entered
            if (document.forms["sla"].limit.value == "")
            {
                ShowLayer("limitEmpty", "block");
                ShowLayer("limitDiv", "none");
                valid                                                   = 0;
            }
            //  Check That Time Limit Is Valid
            else
            {
                ShowLayer("limitEmpty", "none");

                if (!validation("currency", document.forms["sla"].limit.value))
                {
                    ShowLayer("limitDiv", "block");
                    valid                                               = 0;
                }
                else
                    ShowLayer("limitDiv", "none");
            }

            if (valid == 1)
            {
                document.forms["sla"].save.value                        = 1;
                document.forms["sla"].submit();
            }
        }
    }

    function delete_confirmation(id)
    {
        document.forms["sla"].idBox.value                               = id;
        document.forms["sla"].deleteBox.value                           = 1;
        document.forms["sla"].submit();
    }

    ///////////////////////////
    //  Sajax
    function setLimit(data)
    {
        if (data > 0)
            document.forms["sla"].limit.value                           = data;
        else
            document.forms["sla"].limit.value                           = "";
    }

    function getData(select)
    {
        if (select.value != "null")
            x_getLimit(select.value, setLimit);
    }
    ///////////////////////////
</script>
<?php
    //  nProjects                                                       = Number of Projects
    $nProjects                                                          = q("SELECT COUNT(id) FROM Project WHERE company_id = '".$_SESSION["company_id"]."' ".
                                                                            "AND completed = 0");
    $projects                                                           = q("SELECT id, name FROM Project WHERE company_id = '".$_SESSION["company_id"]."' ".
                                                                            "AND completed = 0 ORDER BY name");
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata">
                <form action="" method="post" name="sla">
                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Service Level Agreement</h6>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                Project Name:
                            </td>
                            <td width="50%">
                                <select class="on-field" method="post" id="project" name="project" onChange="getData(project);" tabindex="1">
                                    <option value="null">--  Select A Project  --&nbsp;&nbsp;</option>
                                    <?php
                                        if ($nProjects > 1)
                                            foreach ($projects as $project)
                                                echo "<option value='".$project[0]."'>".$project[1]."&nbsp;&nbsp;</option>";
                                        else if ($nProjects == 1)
                                            echo "<option value='".$projects[0][0]."'>".$projects[0][1]."&nbsp;&nbsp;</option>";
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                Monthly Time Limit:
                            </td>
                            <td width="50%">
                                <input class="on-field" name="limit" style="text-align:right;" tabindex="2" type="text" value="">
                                <font class="on-description-left"><i>* hours</i></font>
                                <div id="limitEmpty" style="display: none;">
                                    <font class="on-validate-error">* Monthly time limit must be entered</font>
                                </div>
                                <div id="limitDiv" style="display: none;">
                                    <font class="on-validate-error">* Enter value invalid, eg. 100</font>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <input name="btnAdd" onClick="check();" tabindex="3" type="button" value="Add/Update SLA">
                    <?php
                        //  nSLA                                        = Number of Service Level Agreements
                        $nSLA                                           = q("SELECT COUNT(s.id) FROM (SLA AS s INNER JOIN Project AS p ON s.project_id = p.id) ".
                                                                            "WHERE p.company_id = '".$_SESSION["company_id"]."'");
                        $slas                                           = q("SELECT s.id, p.name, s.time_limit FROM (SLA AS s INNER JOIN Project AS p ".
                                                                            "ON s.project_id = p.id) WHERE p.company_id = '".$_SESSION["company_id"]."' ORDER BY p.name");

                        if ($nSLA > 0)
                        {
                            echo "<br/><br/>";
                            echo "<table class='on-table-center on-table'>";
                                //  Table Headings
                                echo "<tr>";
                                    echo "<th>Project Name</th>";
                                    echo "<th>Time Limit</th>";
                                    echo "<th>Remove</th>";
                                echo "</tr>";
                                //  Table Information
                                if ($nSLA > 1)
                                {
                                    foreach ($slas as $sla)
                                    {
                                        echo "<tr>";
                                            echo "<td>".$sla[1]."</td>";
                                            echo "<td class='rightdata'>".number_format($sla[2], 2, ".", "")."</td>";
                                            echo "<td class='centerdata'>";
                                                echo "<input name='btnDelete' onClick=\"delete_confirmation(".$sla[0].");\" tabindex='4' type='button' value='Remove'>";
                                            echo "</td>";
                                        echo "</tr>";
                                    }
                                }
                                else if ($nSLA == 1)
                                {
                                    echo "<tr>";
                                        echo "<td>".$slas[0][1]."</td>";
                                        echo "<td class='rightdata'>".number_format($slas[0][2], 2, ".", "")."</td>";
                                        echo "<td class='centerdata'>";
                                            echo "<input name='btnDelete' onClick=\"delete_confirmation(".$slas[0][0].");\" tabindex='4' type='button' value='Remove'>";
                                        echo "</td>";
                                    echo "</tr>";
                                }
                            echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                            echo "</table>";
                        }
                    ?>
                    <input method="post" name="idBox" type="hidden" value="">
                    <input method="post" name="deleteBox" type="hidden" value="">
                    <input method="post" name="save" type="hidden" value="0" />
                </form>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
