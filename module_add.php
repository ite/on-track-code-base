<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if ($_SESSION["logged_in"] === true && !($_SESSION["usertype"] === "0"))
        header("Location: home.php");

    //  Insert Module Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")
    {
        $errorMessage                                                   = "";

        //  Get Information
        $module_name                                                    = addslashes(strip_tags($_POST["module_name"]));
        $module_code                                                    = addslashes(strip_tags($_POST["module_code"]));

        //  Check If Module Exists In Database
        $exist                                                          = q("SELECT id FROM Modules WHERE name = '$module_name' AND code = '$module_code'");

        if (!$exist)
        {
            $insert                                                     = q("INSERT INTO Modules (name, code) ".
                                                                            "VALUES ('$module_name', '$module_code')");

            if ($insert)
            {
                $time                                                   = date("H:i:s");

                $logs                                                   = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time) ".
                                                                            "VALUES ('$module_name added', 'Insert', 'Modules', '".$_SESSION["email"]."', ".
                                                                            "'$today', '$time')");

                header("Location: modules.php");
            }
        }
        else
            $errorMessage                                               = "Module Already Exists";
    }

    if ($errorMessage != "")
        echo "<p align='center' style='padding:0px;'><strong><font class='on-validate-error'>$errorMessage</font></strong></p>";

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0", "modules");
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    var cal                                                             = new CodeThatCalendar(caldef1);

   function check()
    {
        var valid                                                       = 1;

        //  Check That Module Name Is Entered
        if (document.forms["module_add"].module_name.value == "")
        {
            ShowLayer("moduleName", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("moduleName", "none");

        //  Check That Module Code Is Entered
        if (document.forms["module_add"].module_code.value == "")
        {
            ShowLayer("moduleCode", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("moduleCode", "none");

        if (valid == 1)
        {
            document.forms["module_add"].save.value                     = 1;
            document.forms["module_add"].submit();
        }
    }
</script>
    <table width="100%">
        <tr>
            <td class="centerdata" valign="top">
                <form action="" method="post" name="module_add">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Add New Module</h6>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <table cellpadding="0" cellspacing="2" width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                Module Name:
                            </td>
                            <td align="left" width="50%">
                                <input class="on-field" name="module_name" tabindex="1" type="text">
                                <div id="moduleName" style="display: none;"><font class="on-validate-error">* Name must be entered</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                Module Code:
                            </td>
                            <td align="left" width="50%">
                                <input class="on-field" name="module_code" tabindex="1" type="text">
                                <div id="moduleCode" style="display: none;"><font class="on-validate-error">* Code must be entered</font></div>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <input name="btnAdd" onClick="check();" tabindex="4" type="button" value="Add Module">
                    <input method="post" name="save" type="hidden" value="0" />
                </form>
            </td>
        </tr>
        <tr>
            <td class="centerdata">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>