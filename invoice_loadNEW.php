<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("_functions.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    if (!hasAccess("INVOICE_MANAGE_NEW") && !hasAccess("INVOICE_VIEW_NEW"))
        header("Location: noaccess.php");

    if (hasAccess("INVOICE_MANAGE_NEW"))
        $RA_VIEW = 1;

    $project_id                                                         = "null";

    if (isset($_POST["project"]))
        $project_id                                                     = $_POST["project"];

    //  Update Invoice Information Function
    if (isset($_POST["update"]) && $_POST["update"] === "1")    {
        $project_id                                                     = $_POST["project"];

        if ($project_id != "null")      {
            //  nInvoices = Number of Invoices
            $nInvoices = q("SELECT COUNT(id) FROM LoadInvoices WHERE project_id = '$project_id'");
            $invoices = q("SELECT id, name FROM LoadInvoices WHERE project_id = '$project_id'");
        }
        else    {
            //  nInvoices = Number of Invoices
            $nInvoices = q("SELECT COUNT(id) FROM LoadInvoices WHERE project_id IN (SELECT id FROM Project WHERE company_id = '".$_SESSION["company_id"]."') AND paid = '0'");
            $invoices = q("SELECT id, create_date, name, consulting, expenses, diverse_income, due_date, paid, vat FROM LoadInvoices WHERE project_id IN (SELECT id FROM Project ".
                            "WHERE company_id = '".$_SESSION["company_id"]."') AND paid = '0' ORDER BY due_date");
        }

        if ($nInvoices > 1)     {
            foreach($invoices as $invoice)      {
                $date = addslashes(strip_tags($_POST["date".$invoice[0]]));
                $information = q("SELECT expenses, diverse_income, vat, due_date FROM LoadInvoices WHERE id = '".$invoice[0]."'");
                $old_expense = ($information[0][0]) * 1;
                $old_diverse_income = ($information[0][1]) * 1;
                $old_vat = ($information[0][2]) * 1;
                $old_date = $information[0][3];
                $old_amount = ($old_expense+($old_expense*($old_vat/100)))-($old_diverse_income + ($old_diverse_income*($old_vat/100)));

                if ($_POST["box".$invoice[0]] == "1")   $update = q("UPDATE LoadInvoices SET paid = '1', due_date = '".$date."' WHERE id = '".$invoice[0]."'");
                else                                    $update = q("UPDATE LoadInvoices SET paid = '0', due_date = '".$date."' WHERE id = '".$invoice[0]."'");

                if ($old_date != $date) {
                    statusUpdateDate($old_date, $date, "invoice", $old_amount, $_SESSION["company_id"]);
                }

            }
        }
        else if ($nInvoices == 1)       {
            $date = addslashes(strip_tags($_POST["date".$invoices[0][0]]));
            $information = q("SELECT expenses, diverse_income, vat, due_date FROM LoadInvoices WHERE id = '".$invoices[0][0]."'");
            $old_expense = ($information[0][0]) * 1;
            $old_diverse_income = ($information[0][1]) * 1;
            $old_vat = ($information[0][2]) * 1;
            $old_date = $information[0][3];
            $old_amount = ($old_expense+($old_expense*($old_vat/100)))-($old_diverse_income + ($old_diverse_income*($old_vat/100)));


            if ($_POST["box".$invoice[0]] == "1")       $update = q("UPDATE LoadInvoices SET paid = '1', due_date = '".$date."' WHERE id = '".$invoices[0][0]."'");
            else                                        $update = q("UPDATE LoadInvoices SET paid = '0', due_date = '".$date."' WHERE id = '".$invoices[0][0]."'");

            if ($old_date != $date) {
                statusUpdateDate($old_date, $date, "invoice", $old_amount, $_SESSION["company_id"]);
            }
        }

        if ($update)    header("Location: invoice_loadNEW.php");
    }

    //  Insert Invoice Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")        {
        $errorMessage = "";

        //  Get Information
        $project_id = $_POST["project"];
        if($_POST["area"])      $area_id = $_POST["area"];
        else                    $area_id = 0;
        $create_date = addslashes(strip_tags($_POST["create_date"]));
        $invoice_name = addslashes(strip_tags($_POST["invoice_name"]));
        $consulting = addslashes(strip_tags($_POST["consulting"]));
        $expenses = addslashes(strip_tags($_POST["expenses"]));
        $diverse_income = addslashes(strip_tags($_POST["diverse_income"]));
        $vat = addslashes(strip_tags($_POST["VAT"]));
        $due_date = addslashes(strip_tags($_POST["due_date"]));

        if ($consulting != "")          $consulting = round($consulting, 2);
        if ($expenses != "")            $expenses = round($expenses, 2);
        if ($diverse_income != "")      $diverse_income = round($diverse_income, 2);

        if ($vat != "") $vat = round($vat, 2);
        else            $vat = 0;

        //  Check If Activity Type Exists In Database
        $exist = q("SELECT id FROM LoadInvoices WHERE name = '$invoice_name' AND project_id = '$project_id'");

        if (submitDateCheck($create_date) && submitDateCheck($due_date))        {
        if (!$exist)    {
            $insert                                                     = q("INSERT INTO LoadInvoices (project_id, create_date, name, consulting, expenses, ".
                                                                            "diverse_income, vat, due_date, paid, company_id, area_id) VALUES ('$project_id', '$create_date', ".
                                                                            "'$invoice_name', '".number_format($consulting, 2, ".", "")."', ".
                                                                            "'".number_format($expenses, 2, ".", "")."', ".
                                                                            "'".number_format($diverse_income, 2, ".", "")."', '$vat', '$due_date', '0', '".$_SESSION["company_id"]."', '$area_id')");

            if ($insert)        {
/*
                if ($_POST["loadExpense"] == "1") {
                    $invoiceID                                          = q("SELECT id FROM LoadInvoices WHERE name = '$invoice_name' AND project_id = '$project_id'");
                    $expenseName                                        = addslashes(strip_tags($_POST["expDescr"]));
                    $expenseAmount                                      = addslashes(strip_tags($_POST["expAmount"]));
                    $expenseDueDate                                     = addslashes(strip_tags($_POST["expDueDate"]));

                    $insert                                             = q("INSERT INTO ExpenseBudget (project_id, descr, amount, vat, due_date, invoice_id, ".
                                                                            "company_id, paid) VALUES ('$project_id', '$expenseName', '$expenseAmount', '$vat', ".
                                                                            "'$expenseDueDate', '$invoiceID', '".$_SESSION["company_id"]."', '0')");
                }
*/
                if ($expenses != "" && $expenses != 0)  {
                    $invoiceID                                          = q("SELECT id FROM LoadInvoices WHERE name = '$invoice_name' AND project_id = '$project_id'");
                    $expenseName                                        = $invoice_name;
                    $expenseAmount                                      = number_format(($expenses - $diverse_income), 2, ".", "");
                    $expenseDueDate                                     = getDates($due_date, 7);

                    $insert                                             = q("INSERT INTO ExpenseBudget (project_id, descr, amount, vat, due_date, invoice_id, ".
                                                                            "company_id, paid) VALUES ('$project_id', '$expenseName', '$expenseAmount', '$vat', ".
                                                                            "'$expenseDueDate', '$invoiceID', '".$_SESSION["company_id"]."', '0')");
                }

                //  Call Recalculation Function
                $amount = ($expenses + ($expenses * ($vat / 100))) - ($diverse_income + ($diverse_income * ($vat / 100)));
                statusUpdateAmount($due_date, $amount, $_SESSION["company_id"]);

                $errorMessage                                           = "Invoice Saved Successfully";
                $time                                                   = date("H:i:s");

                $project_name                                           = q("SELECT name FROM Project WHERE id = '$project_id'");
                $total                                                  = ($consulting + $expenses);
                $message                                                = "Invoice uploaded by ".$_SESSION["email"]." on ".$today." at ".$time.
                                                                            "\nProject Name:\t".$project_name."\nInvoice Name:\t".$invoice_name."\nTotal:\t\t".
                                                                            $_SESSION["currency"]." ".number_format(($total + ($total * ($vat / 100))), 2, ".", "").
                                                                            "\nConsulting:\t\t".$_SESSION["currency"]." ".number_format($consulting, 2, ".", "").
                                                                            "\nExpenses:\t\t".$_SESSION["currency"]." ".number_format($expenses, 2, ".", "").
                                                                            "\nDiverse Income:\t".$_SESSION["currency"]." ".number_format($diverse_income, 2, ".", "").
                                                                            "\nDue Date:\t\t".$due_date;
                $notify_id                                              = q("SELECT id FROM Notify WHERE code = 'Invoice'");
                $events                                                 = q("INSERT INTO Events (date, time, message, notify_id, processed, user_id, company_id) ".
                                                                            "VALUES ('".$today."', '".$time."', '$message', '$notify_id', '0', ".
                                                                            "'".$_SESSION["user_id"]."', '".$_SESSION["company_id"]."')");

                $logs                                                   = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('".$invoice_name." created', 'Insert', 'LoadInvoices', '".$_SESSION["email"]."', ".
                                                                            "'$today', '$time', '".$_SESSION["company_id"]."')");
            }
        }
        else
            $errorMessage                                               = "Invoice Already Exists";
        }
    }

    //  nProjects                                                       = Number of Contacts
    $nProjects = q("SELECT COUNT(id) FROM Project WHERE company_id = '".$_SESSION["company_id"]."' AND (completed = '0' OR id IN (SELECT DISTINCT(project_id) FROM LoadInvoices WHERE paid = '0'))");
    $projects = q("SELECT id, name FROM Project WHERE company_id = '".$_SESSION["company_id"]."' AND (completed = '0' OR id IN (SELECT DISTINCT(project_id) FROM LoadInvoices WHERE paid = '0')) ORDER BY name");


    //  Print Header
    print_header();
    //  Print Menu
    print_menus("2", "budget");

    if ($errorMessage != "")    {
        if ($errorMessage == "Please note that we are currently busy with data management, no entries are allowed for 2010-08-26. Please check back in 24 Hours.")
            echo "<p style='width:100%; text-align:center'><strong style='font-size:12px;'><font class='on-validate-error'>$errorMessage</font></strong></p>";
        else
            echo "<p style='width:100%; text-align:center'><strong><font class='on-validate-error'>$errorMessage</font></strong></p>";
        echo "<br/>";
    }
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    var loadStatementLinks;
    var loadLinkedExpenses;

    jQuery(function(){
	jQuery('#create_date').DatePicker({
            format:'Y-m-d',
            date: jQuery('#create_date').val(),
            current: jQuery('#create_date').val(),
            starts: 1,
            position: 'right',
            onBeforeShow: function(){
                var _date;
                if (!validation('date',jQuery('#create_date').val()))
                _date = new Date();
                else _date = jQuery('#create_date').val();
                jQuery('#create_date').DatePickerSetDate(_date, true);
            },
            onChange: function(formated, dates){
                jQuery('#create_date').val(formated);
                jQuery('#create_date').DatePickerHide();
            }
	});

	jQuery('#due_date').DatePicker({
            format:'Y-m-d',
            date: jQuery('#due_date').val(),
            current: jQuery('#due_date').val(),
            starts: 1,
            position: 'right',
            onBeforeShow: function(){
                var _date;
                if (!validation('date',jQuery('#due_date').val()))
                _date = new Date();
                else _date = jQuery('#due_date').val();
                jQuery('#due_date').DatePickerSetDate(_date, true);
            },
            onChange: function(formated, dates){
                jQuery('#due_date').val(formated);
                jQuery('#due_date').DatePickerHide();
            }
	});

        jQuery('#expDueDate').DatePicker({
            format:'Y-m-d',
            date: jQuery('#expDueDate').val(),
            current: jQuery('#expDueDate').val(),
            starts: 1,
            position: 'right',
            onBeforeShow: function(){
                var _date;
                if (!validation('date',jQuery('#expDueDate').val()))
                _date = new Date();
                else _date = jQuery('#expDueDate').val();
                jQuery('#expDueDate').DatePickerSetDate(_date, true);
            },
            onChange: function(formated, dates){
                jQuery('#expDueDate').val(formated);
                jQuery('#expDueDate').DatePickerHide();
            }
	});

        loadStatementLinks = function(e)   {
            jQuery.post("_ajax.php", {func: "getStatementLinks", elementType: "INVOICE", elementID: e}, function(data)	{
                data = json_decode(data);

                var bool = false;
                jQuery("#statementLinks tbody>tr").not(':first').not(':last').remove();    //remove current entries
                var last = jQuery("#statementLinks tbody>tr:last");

                jQuery.each(data,function(i, v)  {
                    var n = last.clone(true);
                    n.children().eq(0).html(v[0]);
                    n.children().eq(1).html(v[1]);
                    n.children().eq(2).html(parseFloat(v[2]).toFixed(2));
                    n.insertBefore(last);
                    bool = true;
                });

                if (bool)	last.remove();
            });
        };

        loadLinkedExpenses = function(e)   {
            jQuery.post("_ajax.php", {func: "getLinkedExpenses", invoiceID: e}, function(data)	{
                data = json_decode(data);

                var bool = false;
                jQuery("#linkedExpenses tbody>tr").not(':first').not(':last').remove();    //remove current entries
                var last = jQuery("#linkedExpenses tbody>tr:last");

                jQuery.each(data,function(i, v)  {
                    //alert(v);
                    var n = last.clone(true);
                    if (v[5] == 0)      n.children().eq(0).html(v[1]);
                    else                n.children().eq(0).html("<a href='expense_edit.php?id="+v[0]+"'>"+v[1]+"</a>");
                    n.children().eq(1).html(parseFloat(v[2]).toFixed(2));
                    n.children().eq(2).html(parseFloat(v[3]).toFixed(2));
                    n.children().eq(3).html(v[4]);
                    n.insertBefore(last);
                    bool = true;
                });

                if (bool)	last.remove();
            });
        };

        //  Open Modal
        jQuery(".openModal").click(function()   {
            var modal = (jQuery(this).attr("id").indexOf("view") == 0) ? "modal_linkedExpenses" : "modal_statementLinks";
            var id = "#" + modal;

            if (modal == "modal_statementLinks")    loadStatementLinks(jQuery(this).attr("id"));
            if (modal == "modal_linkedExpenses")    loadLinkedExpenses(jQuery(this).attr("id").replace(/view/g, ""));

            jQuery("#mask").css({"width":jQuery(document).width(),"height":jQuery(document).height()});
            jQuery('#mask').fadeIn("fast").fadeTo("fast",0.8);   //transition effect
            //Set the popup window to center
            jQuery(id).css('top',  jQuery(window).scrollTop()+(jQuery(window).height()/2)-(jQuery(id).height()/1.1));
            jQuery(id).css('left', (jQuery(window).width()/2)-(jQuery(id).width()));
            jQuery(id).fadeIn("fast");   //transition effect
        });

        //  Close Modal
        jQuery(".closeModal").click(function()  {
            jQuery("#mask").hide();
            jQuery('.on-drop-menu').hide();
            jQuery('.on-contact-modal').hide();
        });
    });

    function check(box) {
        var valid = 1;

        //  Check That Date Created Is Valid, If Entered
        if (document.forms["invoice_loadNEW"].create_date.value != "")     {
            if (!validation("date", document.forms["invoice_loadNEW"].create_date.value))  {
                ShowLayer("createDate", "block");
                valid = 0;
            } else      ShowLayer("createDate", "none");
        } else  ShowLayer("createDate", "none");

        //  Check That Invoice Name Is Entered
        if (document.forms["invoice_loadNEW"].invoice_name.value == "")    {
            ShowLayer("invoiceName", "block");
            valid = 0;
        } else  ShowLayer("invoiceName", "none");

        //  Check That Consulting Is Valid, If Entered
        if (document.forms["invoice_loadNEW"].consulting.value != "")      {
            if (!validation("currency", document.forms["invoice_loadNEW"].consulting.value))       {
                ShowLayer("consultingDiv", "block");
                valid = 0;
            } else      ShowLayer("consultingDiv", "none");
        } else  ShowLayer("consultingDiv", "none");

        //  Check That Expenses Is Valid, If Entered
        if (document.forms["invoice_loadNEW"].expenses.value != "")        {
            if (!validation("currency", document.forms["invoice_loadNEW"].expenses.value)) {
                ShowLayer("expensesDiv", "block");
                valid = 0;
            } else      ShowLayer("expensesDiv", "none");
        } else  ShowLayer("expensesDiv", "none");

        //  Check That Diverse Income Is Valid, If Entered
        if (document.forms["invoice_loadNEW"].diverse_income.value != "")  {
            if (!validation("currency", document.forms["invoice_loadNEW"].diverse_income.value))   {
                ShowLayer("diverseIncomeDiv", "block");
                valid = 0;
            } else      ShowLayer("diverseIncomeDiv", "none");
        } else  ShowLayer("diverseIncomeDiv", "none");

        //  Check That Diverse Income Is Valid, If Entered
        if (document.forms["invoice_loadNEW"].VAT.value != "")     {
            if (!validation("percentage", document.forms["invoice_loadNEW"].VAT.value))    {
                ShowLayer("vatDiv", "block");
                valid = 0;
            } else      ShowLayer("vatDiv", "none");
        } else  ShowLayer("vatDiv", "none");

        //  Check That Due Date Is Valid, If Entered
        if (document.forms["invoice_loadNEW"].due_date.value != "")        {
            if (!validation("date", document.forms["invoice_loadNEW"].due_date.value))     {
                ShowLayer("dueDate", "block");
                valid = 0;
            } else      ShowLayer("dueDate", "none");
        } else  ShowLayer("dueDate", "none");

        //  Check That Consulting/Expense Is Entered If Diverse Income Is Entered
        if (document.forms["invoice_loadNEW"].diverse_income.value != "")  {
            if (document.forms["invoice_loadNEW"].expenses.value == "" && document.forms["invoice_loadNEW"].consulting.value == "")   {
                ShowLayer("invoiceDiv", "block");
                valid = 0;
            } else      ShowLayer("invoiceDiv", "none");
        } else  ShowLayer("invoiceDiv", "none");

        if (valid == 1) {
            document.forms["invoice_loadNEW"].save.value = 1;
            document.forms["invoice_loadNEW"].submit();
        }
    }

    function showExpense()      {
        if (document.forms["invoice_loadNEW"].loadExpense.checked == true) {
            document.forms["invoice_loadNEW"].expDescr.value = document.forms["invoice_loadNEW"].invoice_name.value;
            document.forms["invoice_loadNEW"].expAmount.value = document.forms["invoice_loadNEW"].expenses.value - document.forms["invoice_loadNEW"].diverse_income.value;
            var dateString = document.forms["invoice_loadNEW"].due_date.value;
            dateString = dateString.substring(5, 7) + "/" + dateString.substring(8, 10) + "/" + dateString.substring(0, 4);

            var newDate = new Date(dateString);
            newDate.setDate(newDate.getDate()+7);

            var year = newDate.getFullYear( );
            var month = newDate.getMonth( ) + 1;

            if (month < 10)     month = '0' + month;

            var day = newDate.getDate( );

            if (day < 10)       day = '0' + day;

            document.forms["invoice_loadNEW"].expDueDate.value = year + "-" + month + "-" + day;

            ShowLayer("expenseDiv", "block");
        } else  ShowLayer("expenseDiv", "none");
    }

    function updateInfo()       {
        document.forms["invoice_loadNEW"].update.value = 1;
        document.forms["invoice_loadNEW"].submit();
    }
</script>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="invoice_loadNEW">
                    <table class="on-table-center" style="width:100%;">
                        <tr>
                            <td width="100%"class="centerdata">
                                <h6>Load Invoices</h6>
                            </td>
                            <td width="50%"><br/></td>
                        </tr>
                        <tr>
                            <td >
                                <br/>
                            </td>
                        </tr>
                        <tr>
                    </table>
                    <table style="width:100%;">
                        <tr>
                            <td class="on-description">
                                    Project Name:
                            </td>
                            <td width="50%">
                                <select class="on-field" method="post" id="project" name="project" onChange="submit();" tabindex="1">
                                    <option value="null">--  Select Project --</option>
                                    <?php
                                        if ($nProjects > 1)
                                        {
                                            foreach ($projects as $project)
                                                if ($project_id == $project[0])
                                                    echo "<option value='".$project[0]."' selected>".$project[1]."</option>";
                                                else
                                                    echo "<option value='".$project[0]."'>".$project[1]."</option>";
                                        }
                                        else if ($nProjects == 1)
                                            if ($project_id == $projects[0][0])
                                                echo "<option value='".$projects[0][0]."' selected>".$projects[0][1]."</option>";
                                            else
                                                echo "<option value='".$projects[0][0]."'>".$projects[0][1]."</option>";
                                    ?>
                                </select>
                            </td>
                        </tr>
                    </table>

                    <table style="width:100%;">
                        <?php
                            if ($RA_VIEW)
                            if ($project_id != "null"){
                            // Select Areas
                            $areas = q("SELECT id, name FROM areas WHERE pID='".$project_id."' ORDER BY name");
                            if(is_array($areas)){
                                echo "<tr>
                                        <td class='on-description' width='50%'>
                                            Select Area:
                                        </td>
                                        <td width='50%'>
                                            <select class='on-field' method='post' name='area'>
                                                <option value='none'>No Area</option>";
                                                    if (is_array($areas))
                                                        foreach ($areas as $area)
                                                            if ($_POST['area'] == $area[0])     echo "<option value='".$area[0]."' selected>".$area[1]."</option>";
                                                            else                                echo "<option value='".$area[0]."'>".$area[1]."</option>";
                                            echo "</select>
                                            </td>
                                        </tr>";
                                }

                                echo "<tr>";
                                    echo "<td class='on-description' width='50%'>";
                                        echo "Date Created:";
                                    echo "</td>";
                                    echo "<td width='50%'>";
                                        echo "<input id='create_date' name='create_date' tabindex='2' type='text' class='on-field-date' value='".$today."'>".
                                            "<div id='createDate' class='error'><font class='on-description-left'>* Registration date not valid, eg.".$today."</font></div>";
                                    echo "</td>";
                                echo "</tr>";
                                echo "<tr>";
                                    echo "<td class='on-description' width='50%'>";
                                        echo "Invoice Name:";
                                    echo "</td>";
                                    echo "<td width='50%'>";
                                        echo "<input name='invoice_name' tabindex='3' type='text' value='' class='on-field' onBlur='showExpense();'>".
                                            "<div id='invoiceName' class='error'><font class='on-description-left'>* Invoice name must be entered</font></div>";
                                    echo "</td>";
                                echo "</tr>";
                                echo "<tr>";
                                    echo "<td class='on-description' width='50%'>";
                                        echo "Consulting <i>(".$_SESSION["currency"].")</i>:";
                                    echo "</td>";
                                    echo "<td width='50%'>";
                                        echo "<input name='consulting' tabindex='4' class='on-field' type='text' value=''><font class='on-description-left'>* excl".
                                            "</font><div id='consultingDiv' class='error'><font class='on-description-left'>* Entered amount must be numeric, eg. 123.45".
                                            "</font></div>";
                                    echo "</td>";
                                echo "</tr>";
                                echo "<tr>";
                                    echo "<td class='on-description' width='50%'>";
                                        echo "Expenses <i>(".$_SESSION["currency"].")</i>:";
                                    echo "</td>";
                                    echo "<td width='50%'>";
                                        echo "<input name='expenses' tabindex='5' type='text' value='' class='on-field' onBlur='showExpense();'>".
                                            "<font class='on-description-left'>* excl</font><div id='expensesDiv' class='error'><font class='on-description-left'>".
                                            "* Entered amount must be numeric, eg. 123.45</font></div>";
                                    echo "</td>";
                                echo "</tr>";
                            echo "</table>";
                            echo "<table style='width:100%;'>";
                                echo "<tr>";
                                    echo "<td class='on-description' width='10%'></td>";
                                    echo "<td class='on-description' width='45%'>";
                                        echo "Diverse Income <i>(".$_SESSION["currency"].")</i>:";
                                    echo "</td>";
                                    echo "<td width='45%'>";
                                        echo "<input name='diverse_income' tabindex='6' type='text' value='' class='on-field' onBlur='showExpense();'>".
                                            "<font class='on-description-left'>* excl</font><div id='diverseIncomeDiv' class='error'><font class='on-description-left'>".
                                            "* Entered amount must be numeric, eg. 123.45</font></div><div id='invoiceDiv' class='error'><font class='on-description-left'>* Invoice cannot consist of only diverse income".
                                            "</font></div>";
                                    echo "</td>";
                                echo "</tr>";
                            echo "</table>";
                            echo "<table style='width:100%;'>";
                                echo "<tr>";
                                    echo "<td class='on-description' width='50%'>";
                                        echo "VAT <i>(%)</i>:";
                                    echo "</td>";
                                    echo "<td width='50%'>";
                                        echo "<input name='VAT' tabindex='7' type='text' class='on-field-date' value='".addslashes(strip_tags($_SESSION["VAT"]))."'>".
                                            "<div id='vatDiv' class='error'><font class='on-description-left'>* Entered amount must be a percentage, eg. 100%</font></div>";
                                    echo "</td>";
                                echo "</tr>";
                                echo "<tr>";
                                    echo "<td class='on-description' width='50%'>";
                                        echo "Due Date:";
                                    echo "</td>";
                                    echo "<td width='50%'>";
                                        echo "<input id='due_date' name='due_date' tabindex='8' type='text' class='on-field-date' value='".getDates($today, 30)."' onBlur='showExpense();'>".
                                            "<div id='dueDate' class='error'><font class='on-description-left'>* Due date not valid, eg.".$today."</font></div>";
                                    echo "</td>";
                                echo "</tr>";
                                echo "<tr>";
                                    echo "<td width='50%'></td>";
                                echo "</tr>";
/*
                                echo "<tr>";
                                    echo "<td class='on-description' width='50%'>";
                                        echo "Load Expense Dynamically:";
                                    echo "</td>";
                                    echo "<td width='50%'>";
                                        echo "<input name='loadExpense' tabindex='9' type='checkbox' value='1' onChange='showExpense();' onClick='showExpense();'>";
                                    echo "</td>";
                                echo "</tr>";
                                echo "<tr>";
                                    echo "<td width='50%'></td>";
                                echo "</tr>";
                            echo "</table>";
                                echo "<div id='expenseDiv' style='display: none;'>";
                                ?>
                                    <table width="100%">
                                        <tr>
                                            <td class="centerdata">
                                                <h6>
                                                    Load Expense
                                                </h6>
                                            </td>
                                        </tr>
                                    </table>
                                <?php
                                    echo "<table width='100%'>";
                                        echo "<tr>";
                                            echo "<td class='on-description' width='50%'>";
                                                echo "Expense Description:";
                                            echo "</td>";
                                            echo "<td width='50%'>";
                                                echo "<input name='expDescr' readonly type='text' class='on-field' value=''>";
                                            echo "</td>";
                                        echo "</tr>";
                                        echo "<tr>";
                                            echo "<td class='on-description' width='50%'>";
                                                echo "Expense Amount:";
                                            echo "</td>";
                                            echo "<td width='50%'>";
                                                echo "<input name='expAmount' readonly type='text' class='on-field' value=''>";
                                            echo "</td>";
                                        echo "</tr>";
                                        echo "<tr>";
                                            echo "<td class='on-description' width='50%'>";
                                                echo "Expense Due Date:";
                                            echo "</td>";
                                            echo "<td width='50%'>";
                                                echo "<input id='expDueDate' name='expDueDate' type='text' class='on-field' tabindex='10' value=''>";
                                            echo "</td>";
                                        echo "</tr>";
                                    echo "</table>";
                                echo "</div>";
                                echo "<table width='100%'>";
*/
                                echo "<tr>";
                                    echo "<td class='centerdata' colspan='2'>";
                                        echo "<input name='btnAdd' onClick='check();' tabindex='11' type='button' value='Load Invoice'>";
                                    echo "</td>";
                                echo "</tr>";
                            }
                        ?>
                    </table>
                    <?php
                        $totalInvoiced = 0;
                        $totalPaid = 0;
                        $totalOutstanding = 0;

                        if ($project_id != "null") {
                            $hasAreas = q("SELECT SUM(area_id) FROM LoadInvoices WHERE project_id = '$project_id'");
                            $invoices = q("SELECT li.id,li.create_date,ar.name,li.name,(li.consulting + li.expenses + ((li.consulting + li.expenses) * (li.vat / 100))),(SELECT SUM(s.amount) FROM ((Statement AS s ".
                                                "INNER JOIN StatementLink AS sl ON s.id = sl.statement_id) INNER JOIN ElementTypes AS et ON et.id = sl.elementType) ".
                                                "WHERE et.name = 'INVOICE' AND et.company_id = '".$_SESSION["company_id"]."' AND sl.element_id = li.id),li.due_date,li.paid ".
                                            "FROM (LoadInvoices AS li INNER JOIN Project AS p ON li.project_id = p.id LEFT JOIN areas AS ar ON ar.id = li.area_id) ".
                                            "WHERE li.project_id = '$project_id' ORDER BY li.paid ASC, li.due_date ASC");

                            if (is_array($invoices))    {
                                echo "<br/>";
                                echo "<center>";
                                echo "<table class='on-table'>";
                                    //  Table Headings
                                    echo "<tr>";
                                        echo "<th>Date Created</th>";
                                        if ($hasAreas > 0)      echo "<th>Area</th>";
                                        echo "<th>Invoice<br/>Name</th>";
                                        echo "<th>Invoice<br/>Amount <i>(".$_SESSION["currency"].")</i></th>";
                                        echo "<th>Paid<br/>Amount <i>(".$_SESSION["currency"].")</i></th>";
                                        echo "<th>Outstanding<br/>Amount <i>(".$_SESSION["currency"].")</i></th>";
                                        echo "<th>Due<br/>Date</th>";
                                        echo "<th>Paid</th>";
                                        echo "<th></th>";
                                    echo "</tr>";
                                    //  Table Information
                                    foreach ($invoices as $invoice)     {
                                        echo "<tr valign='top'>";
                                            echo "<td style='white-space:nowrap;'>".$invoice[1]."</td>"; // Date

                                            if ($hasAreas > 0)  {
                                                if($invoice[2] == "")   echo "<td style='white-space:nowrap;'>-</td>"; // Area
                                                else                    echo "<td style='white-space:nowrap;'>".$invoice[2]."</td>"; // Area
                                            }

                                            echo "<td style='white-space:nowrap;'>"; // Invoice Name
                                            if (!$RA_VIEW)      echo "".$invoice[3]."";
                                            else                echo "<a href='invoice_edit.php?id=".$invoice[0]."'>".$invoice[3]."</a>";
                                            echo "</td>";

                                            //  Invoice Amount
                                            echo "<td style='white-space:nowrap;' class='rightdata'>".number_format((double)$invoice[4], 2, ".", "")."</td>";
                                            //  Paid Amount
                                            echo "<td id='".$invoice[0]."' name='".$invoice[0]."' style='white-space:nowrap;' class='rightdata openModal'>".number_format((double)$invoice[5], 2, ".", "")."</td>";
                                            //  Outstanding Amount
                                            echo "<td style='white-space:nowrap;' class='rightdata'>".number_format((double)($invoice[4] - $invoice[5]), 2, ".", "")."</td>";
                                            //  Due Date
                                            echo "<td style='white-space:nowrap;' class='rightdata'><input name='date".$invoice[0]."' type='text' style='text-align:right;' value='".$invoice[6]."'></td>";
                                            //  Paid/Unpaid
                                            if ($invoice[7] == "1")     echo "<td style='white-space:nowrap;' class='centerdata'><input name='box".$invoice[0]."' type='checkbox' value='1' checked></td>";
                                            else                        echo "<td style='white-space:nowrap;' class='centerdata'><input name='box".$invoice[0]."' type='checkbox' value='1'></td>";
                                            //  View Linked Expenses
                                            echo "<td style='white-space:nowrap;' class='rightdata'>";
                                                echo "<input id='view".$invoice[0]."' name='view".$invoice[0]."' class='openModal' type='button' value='...' title='View Linked Expenses'>";
                                            echo "</td>";
                                        echo "</tr>";

                                        $totalInvoiced += number_format((double)$invoice[4], 2, ".", "");
                                        $totalPaid += number_format((double)$invoice[5], 2, ".", "");
                                        $totalOutstanding += number_format((double)($invoice[4] - $invoice[5]), 2, ".", "");
                                    }

                                    echo "<tr>";
                                        if ($hasAreas > 0)      echo "<td class='on-table-total' colspan='3'>Grand Total:</td>";
                                        else                    echo "<td class='on-table-total' colspan='2'>Grand Total:</td>";
                                        echo "<td class='on-table-tota do-not-wrap'>".$_SESSION["currency"]." ".number_format((double)$totalInvoiced, 2, ".", "")."</td>";
                                        echo "<td class='on-table-total do-not-wrap'>".$_SESSION["currency"]." ".number_format((double)$totalPaid, 2, ".", "")."</td>";
                                        echo "<td class='on-table-total do-not-wrap'>".$_SESSION["currency"]." ".number_format((double)$totalOutstanding, 2, ".", "")."</td>";
                                        echo "<td class='on-table-total' colspan='3'></td>";
                                        echo "<td class='on-table-total' colspan='3'></td>";
                                    echo "</tr>";
                                    echo "<tr>";
                                        echo "<td class='centerdata' colspan='100%'><br/><input name='btnUpdate' onClick='updateInfo();' type='button' value='Update Information'></td>";
                                    echo "</tr><tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                                echo "</table>";
                                echo "</center>";
                            }
                        } else  {
                            $invoices = q("SELECT li.id,li.create_date,p.name,ar.name,li.name,(li.consulting + li.expenses + ((li.consulting + li.expenses) * (li.vat / 100))),(SELECT SUM(s.amount) FROM ((Statement AS s ".
                                                "INNER JOIN StatementLink AS sl ON s.id = sl.statement_id) INNER JOIN ElementTypes AS et ON et.id = sl.elementType) ".
                                                "WHERE et.name = 'INVOICE' AND et.company_id = '".$_SESSION["company_id"]."' AND sl.element_id = li.id),li.due_date,li.paid ".
                                            "FROM (LoadInvoices AS li INNER JOIN Project AS p ON li.project_id = p.id LEFT JOIN areas AS ar ON ar.id = li.area_id) ".
                                            "WHERE p.company_id = '".$_SESSION["company_id"]."' AND li.paid = '0' ORDER BY li.due_date ASC");

                            if (is_array($invoices))    {
                                echo "<br/>";
                                echo "<table class='on-table'>";
                                    //  Table Headings
                                    echo "<tr>";
                                        echo "<th>Date<br/>Created</th>";
                                        echo "<th>Project Name</th>";
                                        echo "<th>Area</th>";
                                        echo "<th>Invoice<br/>Name</th>";
                                        echo "<th>Invoice<br/>Amount <i>(".$_SESSION["currency"].")</i></th>";
                                        echo "<th>Paid<br/>Amount <i>(".$_SESSION["currency"].")</i></th>";
                                        echo "<th>Outstanding<br/>Amount <i>(".$_SESSION["currency"].")</i></th>";
                                        echo "<th>Due<br/>Date</th>";
                                        echo "<th>Paid</th>";
                                        echo "<th></th>";
                                    echo "</tr>";
                                    //  Table Information
                                    foreach ($invoices as $invoice)
                                    {
                                        echo "<tr>";
                                            echo "<td style='white-space:nowrap; width:80px'>".$invoice[1]."</td>";
                                            echo "<td style='white-space:nowrap;'>".$invoice[2]."</td>";

                                            if($invoice[3] == "")       echo "<td style='white-space:nowrap;'>-</td>";
                                            else                        echo "<td style='white-space:nowrap;'>".$invoice[3]."</td>";

                                            echo "<td style='white-space:nowrap;'>";
                                            if (!$RA_VIEW)      echo "".$invoice[4]."";
                                            else                echo "<a href='invoice_edit.php?id=".$invoice[0]."'>".$invoice[4]."</a>";
                                            echo "</td>";

                                            //  Invoice Amount
                                            echo "<td style='white-space:nowrap;' class='rightdata'>".number_format((double)$invoice[5], 2, ".", "")."</td>";
                                            //  Paid Amount
                                            echo "<td id='".$invoice[0]."' name='".$invoice[0]."' style='white-space:nowrap;' class='rightdata openModal'>".number_format((double)$invoice[6], 2, ".", "")."</td>";
                                            //  Outstanding Amount
                                            echo "<td style='white-space:nowrap;' class='rightdata'>".number_format((double)($invoice[5] - $invoice[6]), 2, ".", "")."</td>";
                                            //  Due Date
                                            echo "<td style='white-space:nowrap;' class='rightdata'><input name='date".$invoice[0]."' type='text' style='text-align:right;' value='".$invoice[7]."'></td>";
                                            //  Paid/Unpaid
                                            if ($invoice[8] == "1")     echo "<td style='white-space:nowrap;' class='centerdata'><input name='box".$invoice[0]."' type='checkbox' value='1' checked></td>";
                                            else                        echo "<td style='white-space:nowrap;' class='centerdata'><input name='box".$invoice[0]."' type='checkbox' value='1'></td>";
                                            //  View Linked Expenses
                                            echo "<td style='white-space:nowrap;' class='rightdata'>";
                                                echo "<input id='view".$invoice[0]."' name='view".$invoice[0]."' class='openModal' type='button' value='...' title='View Linked Expenses'>";
                                            echo "</td>";
                                        echo "</tr>";

                                        $totalInvoiced += number_format((double)$invoice[5], 2, ".", "");
                                        $totalPaid += number_format((double)$invoice[6], 2, ".", "");
                                        $totalOutstanding += number_format((double)($invoice[5] - $invoice[6]), 2, ".", "");
                                    }

                                    echo "<tr>";
                                        echo "<td class='on-table-total' colspan='4'>Grand Total:</td>";
                                        echo "<td class='on-table-total do-not-wrap'>".$_SESSION["currency"]." ".number_format((double)$totalInvoiced, 2, ".", "")."</td>";
                                        echo "<td class='on-table-total do-not-wrap'>".$_SESSION["currency"]." ".number_format((double)$totalPaid, 2, ".", "")."</td>";
                                        echo "<td class='on-table-total do-not-wrap'>".$_SESSION["currency"]." ".number_format((double)$totalOutstanding, 2, ".", "")."</td>";
                                        echo "<td class='on-table-total' colspan='3'></td>";
                                    echo "</tr>";
                                    echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";

                                    echo "</table><table style='width:100%'><tr>";
                                        echo "<td style='padding-left:50%;' colspan='100%'><br/><input name='btnUpdate' onClick='updateInfo();' type='button' ".
                                            "value='Update Information'></td>";
                                    echo "</tr>";
                                echo "</table>";
                            }
                        }
                    ?>
                    <br/>
                    <input method="post" name="save" type="hidden" value="0" />
                    <input method="post" name="update" type="hidden" value="0" />
                </form>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
    <!--  BEGIN: STATEMENT LINKS MODAL  -->
    <div id="modal_statementLinks" name="modal_statementLinks" class="on-contact-modal" style='margin-left:auto;margin-right:auto;text-align:center;'>
        <table style='width:100%'>
            <tr>
                <td colspan="100%" class='centerdata'><a><b>Linked Statement Entries</b></a><br><br></td>
            </tr>
            <tr><td>&nbsp;</td></tr>
        </table>
        <div style="height:300px;overflow:scroll;overflow-x:hidden;">
            <table id='statementLinks'>
                <tr>
                    <td class='noborder'><strong>Date</strong></td>
                    <td class='noborder'><strong>Description</strong></td>
                    <td class='noborder'><strong>Amount <?php echo "<i>(".$_SESSION["currency"].")</i>"; ?></strong></td>
                </tr>
                <tr>
                    <td class='noborder'><a></a></td>
                    <td class='noborder'><a></a></td>
                    <td class='noborder'><a></a></td>
                </tr>
            </table>
        </div>
        <br/>
        <br/>
        <table style='width:100%'>
            <tr>
                <td class="centerdata">
                    <input id="modal_statementLinksClose" name="modal_statementLinksClose" class="closeModal" type="button" value="Close" />
                </td>
            </tr>
        </table>
    </div>
    <!--  END: STATEMENT LINKS MODAL  -->
    <!--  BEGIN: LINKED EXPENSES MODAL  -->
    <div id="modal_linkedExpenses" name="modal_linkedExpenses" class="on-contact-modal" style='margin-left:auto;margin-right:auto;text-align:center;'>
        <table style='width:100%'>
            <tr>
                <td colspan="100%" class='centerdata'><a><b>Linked Expenses</b></a><br><br></td>
            </tr>
            <tr><td>&nbsp;</td></tr>
        </table>
        <div style="height:300px;overflow:scroll;overflow-x:hidden;">
            <table id='linkedExpenses'>
                <tr>
                    <td class='noborder'><strong>Description</strong></td>
                    <td class='noborder'><strong>Amount </strong><?php echo "<i><strong>(".$_SESSION["currency"].")</strong>(excl)</i>"; ?></td>
                    <td class='noborder'><strong>Amount </strong><?php echo "<i><strong>(".$_SESSION["currency"].")</strong>(incl)</i>"; ?></td>
                    <td class='noborder'><strong>Date</strong></td>
                </tr>
                <tr>
                    <td class='noborder'><a></a></td>
                    <td class='noborder'><a></a></td>
                    <td class='noborder'><a></a></td>
                    <td class='noborder'><a></a></td>
                </tr>
            </table>
        </div>
        <br/>
        <br/>
        <table style='width:100%'>
            <tr>
                <td class="centerdata">
                    <input id="modal_linkedExpensesClose" name="modal_linkedExpensesClose" class="closeModal" type="button" value="Close" />
                </td>
            </tr>
        </table>
    </div>
    <!--  END: LINKED EXPENSES MODAL  -->
<?php
    //  Print Footer
    print_footer();
?>
