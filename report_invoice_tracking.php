<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("include/sajax.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    if (!hasAccess("REP_INVOICE_TRACK"))
        header("Location: noaccess.php");

    // Class for table background color
    $colorClass = "OnTrack_#_";

    ///////////////////////////
    //  Sajax
    function getProjects($id) {
        if ($id == "null")
            $projects = q("SELECT p.id, p.name FROM Project as p
                                    INNER JOIN companiesOnTeam as cot ON p.id = cot.project_id
                                    WHERE p.completed = 0
                                    AND cot.company_id = '".$_SESSION["company_id"]."' ORDER BY p.name");
        else
            $projects = q("SELECT p.id, p.name FROM Project as p
                                    INNER JOIN companiesOnTeam as cot ON p.id = cot.project_id
                                    WHERE p.completed = 0
                                    AND p.type_id = '$id'
                                    AND cot.company_id = '".$_SESSION["company_id"]."' ORDER BY p.name");

        return $projects;
    }

    function getAreas($id){
        $areas = q("SELECT id, name FROM areas WHERE pID='$id' ORDER BY name");
        return $areas ;
    }

    $sajax_request_type = "GET";
    sajax_init();
    sajax_export("getProjects");
    sajax_export("getAreas");
    sajax_handle_client_request();
    ///////////////////////////

    //  Set Report Generated Status
    $generated                                                          = "0";

    function createDisplay($projectName, $invoiceName, $invoiceTotal, $createDate, $dueDate, $areaName, $days, $top, $left, $right, $bottom)	{
        $display = "<tr>".
            "<td>".$projectName."</td>";
            if($_POST["area"] == "null" && $areaName !=""){
                $display .= "<td>".$areaName."</td>";
            }else if($_POST["area"] == "null" && $areaName ==""){
                $display .= "<td> - </td>";
            }
            $display .= "<td>".$invoiceName."</td>".
                            "<td class='rightdata'>".number_format($invoiceTotal, 2, ".", "")."</td>".
                            "<td class='rightdata'>".$createDate."</td>".
                            "<td class='rightdata'>".$dueDate."</td>".
                            "<td class='rightdata'>".$days."</td>".
                        "</tr>";

        return $display;
    }

    //  Create Display Information Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")
    {
        $errorMessage                                                   = "";

        $project_type_id = $_POST["projectType"];
        $project_id = $_POST["project"];
        $area_id = $_POST["area"];
        $viewType = $_POST["viewType"];

        if ($viewType == "between") {
            $date_from = addslashes(strip_tags($_POST["date_from"]));
            $date_to = addslashes(strip_tags($_POST["date_to"]));
        }

        if ($project_type_id != "null" && is_numeric($project_type_id))
            $where .= "AND p.type_id = '$project_type_id'";

        //echo "<a>AREA_ID: $area_id</a><br>";
        if($area_id == "null"){                                                  // All Areas
            $areaSelect = ",ar.name";
            $areaFrom = "LEFT JOIN areas AS ar ON ar.id = ld.area_id";
            $areaWhere = "";
            $areaTH = "<th>Area</th>";
            $showAreas = 1;
        }else if($area_id == "" || $area_id == "none"){           // No Areas Present
            $areaSelect = "";
            $areaFrom = "";
            $areaWhere = "";
            $areaTH = "";
            $showAreas = 1;
        }else{                                                                          // A specific area
            $areaSelect = ",ar.name";
            $areaFrom = "LEFT JOIN areas AS ar ON ar.id = ld.area_id";
            $areaWhere = "AND ld.area_id = '$area_id'";
            $areaTH = "";
            $showAreas = 0;
        }

        $row                                                              = 0;

                        $excelheadings[$row][]                                     = "";
                        $excelheadings[$row][]                                     = "";
                        $excelheadings[$row][]                                     = "";
                        $excelheadings[$row][]                                     = "";
                        $excelheadings[$row][]                                     = "";
                        $excelheadings[$row][]                                     = "";
                    if($showAreas)
                        $excelheadings[$row][]                                     = "";
                            $row++;

        $headers = "<tr>".
						"<th>Project Name</th>".
                        $areaTH.
						"<th>Invoice Name</th>".
						"<th>Invoice Total<br/>(incl VAT)</th>".
						"<th style='white-space:nowrap'>Date Created</th>".
						"<th style='width:80px; white-space:nowrap'>Due Date</th>".
						"<th>Days <br>From <br>Date<br> Created</th>".
					"</tr>";

		if ($viewType == "between")	{
			if ($project_id == "all")	{
				$invoices = q("SELECT p.name, ld.name, ((ld.consulting + ld.expenses) + ((ld.consulting + ld.expenses) * (ld.vat / 100))), ld.create_date, ld.due_date, ld.paid $areaSelect ".
								"FROM (LoadInvoices AS ld INNER JOIN Project AS p ON p.id = ld.project_id $areaFrom) ".
								"WHERE ld.company_id = '".$_SESSION["company_id"]."' AND ld.create_date >= '$date_from' AND ld.create_date <= '$date_to' $where ".
								"AND ld.paid = '0' $areaWhere ORDER BY ld.due_date, p.name");
                       /* echo "<a>001: SELECT p.name, ld.name, (ld.consulting + ld.expenses), ld.create_date, ld.due_date, ld.paid $areaSelect ".
                        "FROM (LoadInvoices AS ld INNER JOIN Project AS p ON p.id = ld.project_id $areaFrom) ".
                        "WHERE ld.company_id = '".$_SESSION["company_id"]."' AND ld.create_date >= '$date_from' AND ld.create_date <= '$date_to' $where ".
                        "AND ld.paid = '0' $areaWhere ORDER BY ld.due_date, p.name</a>";    */
			}
			else	{
				$invoices = q("SELECT p.name, ld.name, ((ld.consulting + ld.expenses) + ((ld.consulting + ld.expenses) * (ld.vat / 100))), ld.create_date, ld.due_date, ld.paid $areaSelect ".
								"FROM (LoadInvoices AS ld INNER JOIN Project AS p ON p.id = ld.project_id $areaFrom) ".
								"WHERE ld.company_id = '".$_SESSION["company_id"]."' AND ld.create_date >= '$date_from' AND ld.create_date <= '$date_to' $where ".
								"AND ld.paid = '0' AND p.id = '$project_id' $areaWhere ORDER BY ld.due_date, p.name");
                        /* echo "<a>002: SELECT p.name, ld.name, (ld.consulting + ld.expenses), ld.create_date, ld.due_date, ld.paid $areaSelect ".
                        "FROM (LoadInvoices AS ld INNER JOIN Project AS p ON p.id = ld.project_id $areaFrom) ".
                        "WHERE ld.company_id = '".$_SESSION["company_id"]."' AND ld.create_date >= '$date_from' AND ld.create_date <= '$date_to' $where ".
                        "AND ld.paid = '0' AND p.id = '$project_id' $areaWhere ORDER BY ld.due_date, p.name</a>";   */
			}
		}
		else	{
			if ($project_id == "all")	{
				$invoices = q("SELECT p.name, ld.name, ((ld.consulting + ld.expenses) + ((ld.consulting + ld.expenses) * (ld.vat / 100))), ld.create_date, ld.due_date, ld.paid $areaSelect ".
								"FROM (LoadInvoices AS ld INNER JOIN Project AS p ON p.id = ld.project_id $areaFrom) ".
								"WHERE ld.company_id = '".$_SESSION["company_id"]."' $where ".
								"AND ld.paid = '0' $areaWhere ORDER BY ld.due_date, p.name");
                        /* echo "<a>003: SELECT p.name, ld.name, (ld.consulting + ld.expenses), ld.create_date, ld.due_date, ld.paid $areaSelect ".
                        "FROM (LoadInvoices AS ld INNER JOIN Project AS p ON p.id = ld.project_id $areaFrom) ".
                        "WHERE ld.company_id = '".$_SESSION["company_id"]."' $where ".
                        "AND ld.paid = '0' $areaWhere ORDER BY ld.due_date, p.name</a>";    */
			}
			else	{
				$invoices = q("SELECT p.name, ld.name, ((ld.consulting + ld.expenses) + ((ld.consulting + ld.expenses) * (ld.vat / 100))), ld.create_date, ld.due_date, ld.paid $areaSelect ".
								"FROM (LoadInvoices AS ld INNER JOIN Project AS p ON p.id = ld.project_id $areaFrom) ".
								"WHERE ld.company_id = '".$_SESSION["company_id"]."' $where ".
								"AND ld.paid = '0' AND p.id = '$project_id' $areaWhere ORDER BY ld.due_date, p.name");
                        /* echo "<a>004: SELECT p.name, ld.name, (ld.consulting + ld.expenses), ld.create_date, ld.due_date, ld.paid $areaSelect ".
                        "FROM (LoadInvoices AS ld INNER JOIN Project AS p ON p.id = ld.project_id $areaFrom) ".
                        "WHERE ld.company_id = '".$_SESSION["company_id"]."' $where ".
                        "AND ld.paid = '0' AND p.id = '$project_id' $areaWhere ORDER BY ld.due_date, p.name</a>";   */
			}
		}

        $displayString = "";
		$lessThan30 = "";
		$lessThan60 = "";
		$lessThan90 = "";
		$other = "";
		$lessThan30Total = "";
		$lessThan60Total = "";
		$lessThan90Total = "";
		$otherTotal = "";

        $arr30;
        $arr60;
        $arr90;
        $arrOther;

		if (is_array($invoices)) {
			foreach ($invoices as $invoice) {
				$days = getDays($invoice[3], date("Y-m-d")) - 1;

				if ($days <= 30)	{
					$lessThan30 .= createDisplay($invoice[0], $invoice[1], $invoice[2], $invoice[3], $invoice[4], $invoice[6], $days, $top, $left, $right, $bottom);
					$lessThan30Total += $invoice[2];

                    $c = 0;
                    $arr30 [$c][] = $invoice[0];
                if($showAreas)
                    $arr30 [$c][] = $invoice[6];
                    $arr30 [$c][] = $invoice[1];
                    $arr30 [$c][] = number_format($invoice[2], 2, ".", "");
                    $arr30 [$c][] = $invoice[3];
                    $arr30 [$c][] = $invoice[4];
                    $arr30 [$c][] = $days;
                    $c++;
				}
				else if ($days <= 60)	{
					$lessThan60 .= createDisplay($invoice[0], $invoice[1], $invoice[2], $invoice[3], $invoice[4], $invoice[6], $days, $top, $left, $right, $bottom);
					$lessThan60Total += $invoice[2];

                    $c = 0;
                    $arr60 [$c][] = $invoice[0];
                if($showAreas)
                    $arr60 [$c][] = $invoice[6];
                    $arr60 [$c][] = $invoice[1];
                    $arr60 [$c][] = number_format($invoice[2], 2, ".", "");
                    $arr60 [$c][] = $invoice[3];
                    $arr60 [$c][] = $invoice[4];
                    $arr60 [$c][] = $days;
                    $c++;
				}
				else if ($days <= 90)	{
					$lessThan90 .= createDisplay($invoice[0], $invoice[1], $invoice[2], $invoice[3], $invoice[4], $invoice[6], $days, $top, $left, $right, $bottom);
					$lessThan90Total += $invoice[2];

                    $c = 0;
                    $arr90 [$c][] = $invoice[0];
                if($showAreas)
                    $arr90 [$c][] = $invoice[6];
                    $arr90 [$c][] = $invoice[1];
                    $arr90 [$c][] = number_format($invoice[2], 2, ".", "");
                    $arr90 [$c][] = $invoice[3];
                    $arr90 [$c][] = $invoice[4];
                    $arr90 [$c][] = $days;
                    $c++;
				}
				else	{
					$other .= createDisplay($invoice[0], $invoice[1], $invoice[2], $invoice[3], $invoice[4], $invoice[6], $days, $top, $left, $right, $bottom);
					$otherTotal += $invoice[2];

                    $c = 0;
                    $arrOther [$c][] = $invoice[0];
                if($showAreas)
                    $arrOther [$c][] = $invoice[6];
                    $arrOther [$c][] = $invoice[1];
                    $arrOther [$c][] = number_format($invoice[2], 2, ".", "");
                    $arrOther [$c][] = $invoice[3];
                    $arrOther [$c][] = $invoice[4];
                    $arrOther [$c][] = $days;
                    $c++;
				}
			}
		}

        if(is_numeric($area_id) && is_array($invoices) && $area_id != "null"){
            $aName = q("SELECT name FROM areas WHERE id= '".$area_id."' ");
            $displayString .= "<tr><td class='on-table-clear' colspan='100%'>Area: ".$aName."</td></tr>";
        }

        $exceldata[$row][]                                     = "Report: Invoice Tracking Report";
        $exceldata[$row][]                                     = "";
        $exceldata[$row][]                                     = "";
        $exceldata[$row][]                                     = "";
        $exceldata[$row][]                                     = "";
        $exceldata[$row][]                                     = "";
    if($showAreas)
         $exceldata[$row][]                                     = "";
            $row++;

        $exceldata[$row][]                                     = "Company Name: ".$_SESSION["company_name"];
        $exceldata[$row][]                                     = "";
        $exceldata[$row][]                                     = "";
        $exceldata[$row][]                                     = "";
        $exceldata[$row][]                                     = "";
        $exceldata[$row][]                                     = "";
    if($showAreas)
         $exceldata[$row][]                                     = "";
            $row++;

		if ($lessThan30 != ""){
			$displayString .= "<tr>
                                            <td class='on-table-clear' colspan='100%'>Less than 30 days</td>
                                        </tr>".$headers.$lessThan30."
                                        <tr>
                                            <td class='on-table-total' colspan='".(1+$showAreas)."'>Total (incl VAT):</td>
                                            <td class='on-table-total'>".number_format($lessThan30Total, 2, ".", "")."</td>
                                            <td class='on-table-total' colspan='3'></td>
                                        </tr><tr><td colspan='100%'></td></tr>";

            $exceldata[$row][]                                     = "Less than 30 days";
            $exceldata[$row][]                                     = "";
            $exceldata[$row][]                                     = "";
            $exceldata[$row][]                                     = "";
            $exceldata[$row][]                                     = "";
            $exceldata[$row][]                                     = "";
        if($showAreas)
             $exceldata[$row][]                                     = "";
                $row++;

            $exceldata[$row][]                                     = $colorClass."Project Name ";
        if($showAreas)
            $exceldata[$row][]                                     = $colorClass."Area";
            $exceldata[$row][]                                     = $colorClass."Invoice Name";
            $exceldata[$row][]                                     = $colorClass."Invoice Total (incl VAT)";
            $exceldata[$row][]                                     = $colorClass."Date Created";
            $exceldata[$row][]                                     = $colorClass."Due Date";
            $exceldata[$row][]                                     = $colorClass."Days From Date Created";
                $row++;

            foreach ($arr30 as $val1) {
                $counter = 0;
                foreach ($val1 as $val2) {
                        $exceldata[$row][]                                     = $val2;
                        $counter++;

                   if($counter == 6+$showAreas){
                                $row++;
                                $counter = 0;
                        }
                }
                $row++;
            }

            $exceldata[$row][]                                     = $colorClass."";
        if($showAreas)
            $exceldata[$row][]                                     = $colorClass."";
            $exceldata[$row][]                                     = $colorClass."Total (incl VAT):";
            $exceldata[$row][]                                     = $colorClass.number_format($lessThan30Total, 2, ".", "");
            $exceldata[$row][]                                     =$colorClass."";
            $exceldata[$row][]                                     = $colorClass."";
            $exceldata[$row][]                                     = $colorClass."";
                $row++;
        }

		if ($lessThan60 != ""){
			$displayString .= "<tr>
                                            <td class='on-table-clear' colspan='100%'>30-60 days</td>
                                        </tr>".$headers.$lessThan60."
                                        <tr>
                                            <td class='on-table-total' colspan='".(1+$showAreas)."'>Total (incl VAT):</td>
                                            <td class='on-table-total'>".number_format($lessThan60Total, 2, ".", "")."</td>
                                            <td class='on-table-total' colspan='3'></td>
                                        </tr><tr><td colspan='100%'></td></tr>";

            $exceldata[$row][]                                     = "30-60 days";
            $exceldata[$row][]                                     = "";
            $exceldata[$row][]                                     = "";
            $exceldata[$row][]                                     = "";
            $exceldata[$row][]                                     = "";
            $exceldata[$row][]                                     = "";
        if($showAreas)
            $exceldata[$row][]                                     = "";
                $row++;

            $exceldata[$row][]                                     =$colorClass."Project Name ";
        if($showAreas)
            $exceldata[$row][]                                     = $colorClass."Area";
            $exceldata[$row][]                                     = $colorClass."Invoice Name";
            $exceldata[$row][]                                     = $colorClass."Invoice Total (incl VAT)";
            $exceldata[$row][]                                     = $colorClass."Date Created";
            $exceldata[$row][]                                     = $colorClass."Due Date";
            $exceldata[$row][]                                     = $colorClass."Days From Date Created";
                $row++;

        foreach ($arr60 as $val1) {
                $counter = 0;
                foreach ($val1 as $val2) {
                        $exceldata[$row][]                                     = $val2;
                        $counter++;

                        if($counter == 6+$showAreas){
                            $row++;
                            $counter = 0;
                        }
                }
                $row++;
            }

            $exceldata[$row][]                                     = $colorClass."";
        if($showAreas)
            $exceldata[$row][]                                     =$colorClass."";
            $exceldata[$row][]                                     = $colorClass."Total (incl VAT):";
            $exceldata[$row][]                                     = $colorClass.number_format($lessThan60Total, 2, ".", "");
            $exceldata[$row][]                                     = $colorClass."";
            $exceldata[$row][]                                     = $colorClass."";
            $exceldata[$row][]                                     = $colorClass."";
                $row++;
        }


		if ($lessThan90 != ""){
			$displayString .= "<tr>
                                            <td class='on-table-clear' colspan='100%'>60-90 days</td>
                                        </tr>".$headers.$lessThan90."
                                        <tr>
                                            <td class='on-table-total' colspan='".(1+$showAreas)."'>Total (incl VAT):</td>
                                            <td class='on-table-total'>".number_format($lessThan90Total, 2, ".", "")."</td>
                                            <td class='on-table-total' colspan='3'></td>
                                        </tr><tr><td colspan='100%'></td></tr>";

            $exceldata[$row][]                                     = "60-90 days";
            $exceldata[$row][]                                     = "";
            $exceldata[$row][]                                     = "";
            $exceldata[$row][]                                     = "";
            $exceldata[$row][]                                     = "";
            $exceldata[$row][]                                     = "";
        if($showAreas)
            $exceldata[$row][]                                     = "";
                $row++;

            $exceldata[$row][]                                     = $colorClass."Project Name ";
        if($showAreas)
            $exceldata[$row][]                                     = $colorClass."Area";
            $exceldata[$row][]                                     = $colorClass."Invoice Name";
            $exceldata[$row][]                                     = $colorClass."Invoice Total (incl VAT)";
            $exceldata[$row][]                                     = $colorClass."Date Created";
            $exceldata[$row][]                                     = $colorClass."Due Date";
            $exceldata[$row][]                                     = $colorClass."Days From Date Created";
                $row++;

        foreach ($arr90 as $val1) {
                $counter = 0;
                foreach ($val1 as $val2) {
                        $exceldata[$row][]                                     = $val2;
                        $counter++;

                        if($counter == 6+$showAreas){
                            $row++;
                            $counter = 0;
                        }
                }
                $row++;
            }

            $exceldata[$row][]                                     = $colorClass."";
        if($showAreas)
            $exceldata[$row][]                                     = $colorClass."";
            $exceldata[$row][]                                     = $colorClass."Total (incl VAT):";
            $exceldata[$row][]                                     = $colorClass.number_format($lessThan90Total, 2, ".", "");
            $exceldata[$row][]                                     = $colorClass."";
            $exceldata[$row][]                                     = $colorClass."";
            $exceldata[$row][]                                     = $colorClass."";
                $row++;
        }

		if ($other != ""){
			$displayString .= "<tr>
                                            <td class='on-table-clear' colspan='100%'>More than 90 days</td>
                                        </tr>".$headers.$other."
                                        <tr>
                                            <td class='on-table-total' colspan='".(1+$showAreas)."'>Total (incl VAT):</td>
                                            <td class='on-table-total'>".number_format($otherTotal, 2, ".", "")."</td>
                                            <td class='on-table-total' colspan='3'></td>
                                        </tr>";

            $exceldata[$row][]                                     = "More than 90 days ";
            $exceldata[$row][]                                     = "";
            $exceldata[$row][]                                     = "";
            $exceldata[$row][]                                     = "";
            $exceldata[$row][]                                     = "";
            $exceldata[$row][]                                     = "";
         if($showAreas)
            $exceldata[$row][]                                     = "";
                $row++;

            $exceldata[$row][]                                     = $colorClass."Project Name ";
        if($showAreas)
            $exceldata[$row][]                                     = $colorClass."Area";
            $exceldata[$row][]                                     = $colorClass."Invoice Name";
            $exceldata[$row][]                                     = $colorClass."Invoice Total (incl VAT)";
            $exceldata[$row][]                                     = $colorClass."Date Created";
            $exceldata[$row][]                                     = $colorClass."Due Date";
            $exceldata[$row][]                                     = $colorClass."Days From Date Created";
                $row++;

            foreach ($arrOther as $val1) {
                $counter = 0;
                foreach ($val1 as $val2) {
                        $exceldata[$row][]                                     = $val2;
                        $counter++;

                        if($counter == 6+$showAreas){
                                $row++;
                                $counter = 0;
                        }
                }
                $row++;
            }

            $exceldata[$row][]                                     = $colorClass."";
        if($showAreas)
            $exceldata[$row][]                                     = $colorClass."";
            $exceldata[$row][]                                     = $colorClass."Total (incl VAT):";
            $exceldata[$row][]                                     = $colorClass.number_format($otherTotal, 2, ".", "");
            $exceldata[$row][]                                     =$colorClass."";
            $exceldata[$row][]                                     = $colorClass."";
            $exceldata[$row][]                                     = $colorClass."";
        }

        if ($displayString != "")
            $generated = "1";
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("5", "reports");

    /*
    echo "<a>";
    foreach ($exceldata as $e){
        print_r($e);
        echo "<br>";
    }
    echo "</a>";
    */


    if ($errorMessage != "") {
        echo "<p align='center' style='padding:0px;'><strong><font class='on-validate-error'>$errorMessage</font></strong></p>";
        echo "<br/>";
    }
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    jQuery(function(){
	jQuery('#date_from').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_from').val(),
		current: jQuery('#date_from').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_from').val()))
			_date = new Date();
			else _date = jQuery('#date_from').val();
			jQuery('#date_from').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_from').val(formated);
			jQuery('#date_from').DatePickerHide();
		}
	});
    })

    jQuery(function(){
	jQuery('#date_to').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_to').val(),
		current: jQuery('#date_to').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_to').val()))
			_date = new Date();
			else _date = jQuery('#date_to').val();
			jQuery('#date_to').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_to').val(formated);
			jQuery('#date_to').DatePickerHide();
		}
	});
    })

    function check()
    {
        var valid                                                       = 1;

        //  Check That Employee Is Selected
        if (document.forms["report_loaded_invoices"].project.value == "null")
        {
            ShowLayer("projectDiv", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("projectDiv", "none");

        //  Check That Date From Is Entered
        if (document.forms["report_loaded_invoices"].date_from.value == "")
        {
            ShowLayer("dateFrom", "block");
            valid                                                       = 0;
        }
        //  Check That Entered Date From Is Valid
        else
        {
            ShowLayer("dateFrom", "none");

            if (!validation("date", document.forms["report_loaded_invoices"].date_from.value))
            {
                ShowLayer("dateFromDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("dateFromDiv", "none");
        }

        //  Check That Date To Is Entered
        if (document.forms["report_loaded_invoices"].date_to.value == "")
        {
            ShowLayer("dateTo", "block");
            valid                                                       = 0;
        }
        //  Check That Entered Date To Is Valid
        else
        {
            ShowLayer("dateTo", "none");

            if (!validation("date", document.forms["report_loaded_invoices"].date_to.value))
            {
                ShowLayer("dateToDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("dateToDiv", "none");
        }

        if (valid == 1)
        {
            document.forms["report_loaded_invoices"].save.value         = 1;
            document.forms["report_loaded_invoices"].submit();
        }
    }

    function viewSelection()
    {
        if (document.forms["report_loaded_invoices"].viewType.value == "between")
            ShowLayer("dateDisplay", "block");
        else
            ShowLayer("dateDisplay", "none");
    }

    ///////////////////////////
    //  Sajax
    <?php sajax_show_javascript(); ?>

    function ClearOptions(OptionList) {
        for (x = OptionList.length; x >= 0; x--) {
            OptionList[x] = null;
        }
    }

    function setProjects(data) {
        var c = 0;

        document.forms["report_loaded_invoices"].project.options[(c++)] = new Option("--  Select A Project  --", "null");
        document.forms["report_loaded_invoices"].project.options[(c++)] = new Option("All Projects", "all");

        for (var i in data)
            eval("document.forms['report_loaded_invoices'].project.options["+(c++)+"] = new Option('" + data[i][1] + "', '" + data[i][0] + " ');");
    }

    //Set Areas
    function setAreas(data) {
        if(!data)
            ShowLayer("areaDiv", "none");
        var c                                                           = 0;
        document.report_loaded_invoices.area.options[(c++)]                         = new Option("All Areas", "null");
        for (var i in data)
            eval("document.report_loaded_invoices.area.options["+(c++)+"] = new Option('" + data[i][1] + "', '" + data[i][0] + "');");
    }

    function getProject(select) {
        ClearOptions(document.forms["report_loaded_invoices"].project);

        x_getProjects(select.value, setProjects);
    }

    //Get Areas
    function getAreas(select) {
        ClearOptions(document.report_loaded_invoices.area);
        if (select.value != "null")
        {
            ShowLayer("areaDiv", "block");
            x_getAreas(select.value, setAreas);
        }
        else{
            ShowLayer("areaDiv", "none");
        }
    }
    ///////////////////////////
</script>
<?php
    $projectTypes = q("SELECT id, type FROM ProjectTypes WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY type");
    $projectTypes[] = array("0","Shared Projects");
    $projectTypes = sortArrayByColumn($projectTypes, 1);

    $projects = q("SELECT p.id, p.name FROM Project as p
                            INNER JOIN companiesOnTeam as cot ON p.id = cot.project_id
                            WHERE p.completed = 0
                            AND cot.company_id = '".$_SESSION["company_id"]."' ORDER BY p.name");
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="report_loaded_invoices">
                    <div id="" style="<?php if ($generated == "1") echo "display: none;"; else echo "display: block"; ?>">
                        <table width="100%">
                            <tr>
                                <td class="centerdata">
                                    <h6>Invoice Tracking Report</h6>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Project Type:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="projectType" onChange="getProject(projectType);">
                                        <option value="null">All Project Types</option>
                                        <?php
                                            if (is_array($projectTypes))
                                                foreach ($projectTypes as $projectType)
                                                    if ($_POST["projectType"] == $projectType[0])
                                                        echo "<option value='".$projectType[0]."' selected>".$projectType[1]."</option>";
                                                    else
                                                        echo "<option value='".$projectType[0]."'>".$projectType[1]."</option>";
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Project:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" id="project" name="project" onChange="getAreas(project)">
                                        <option value="all">All Projects</option>
                                        <?php
                                            if (is_array($projects))
                                                foreach ($projects as $project)
                                                    if (isset($_POST["project"]) && $_POST["project"] == $project[0]){
                                                        echo "<option value='".$project[0]."' selected>".$project[1]."</option>";
                                                        $proj_id  = $project[0];
                                                    }else{
                                                        echo "<option value='".$project[0]."'>".$project[1]."</option>";
                                                    }
                                        ?>
                                    </select>
                                    <div id="projectDiv" style="display: none;"><font class="on-validate-error">* Project must be selected</font></div>
                                </td>
                            </tr>
                        </table>

                         <div id="areaDiv" style="display: none;">
                            <table width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Area:
                                    </td>
                                    <td width='50%'>
                                        <select class='on-field' method='post' name='area'>
                                            <option value='none'>All Areas</option>";
                                                <?php if (is_array($areas))
                                                    foreach ($areas as $area)
                                                        if ($invoice_info[0][8] == $area[0])
                                                            echo "<option value='".$area[0]."' selected>".$area[1]."</option>";
                                                        else
                                                            echo "<option value='".$area[0]."'>".$area[1]."</option>";
                                                        ?>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Viewing Type:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="viewType" onChange="viewSelection();">
                                        <option value="all">View All Invoices</option>
                                        <option value="between">View Invoices Between Dates</option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                        <div id="dateDisplay" style="display: none;">
                            <table width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                            Date From:
                                    </td>
                                    <td width="50%">
                                        <input class="on-field-date" id="date_from" name="date_from" type="text" style="text-align:right;" value="<?php
                                            if ($_POST["date_from"] != "") echo "".$_POST["date_from"]; else echo "".getMonthStart($today); ?>">
                                        <div id="dateFrom" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                        <div id="dateFromDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="on-description" width="50%">
                                        Date To:
                                    </td>
                                    <td width="50%">
                                        <input class="on-field-date" id="date_to" name="date_to" type="text" style="text-align:right;" value="<?php
                                            if ($_POST["date_to"] != "") echo "".$_POST["date_to"]; else echo "".getMonthEnd($today); ?>">
                                        <div id="dateTo" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                        <div id="dateToDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <br/>
                        <input name="btnGenerate" onClick="check();" type="button" value="Generate Report">
                        <input method="post" name="save" type="hidden" value="0" />
                    </div>
                </form>
                <div id="report" style="<?php if ($generated == "1") echo "display: block;"; else echo "display: none"; ?>">
                    <?php
                        $report_heading = "<tr>
                                                            <td class='on-table-clear' colspan='100%'><a>Invoice Tracking Report</a></td>
                                                        </tr><tr>
                                                            <td class='on-table-clear' colspan='100%'>Company Name: ".$_SESSION["company_name"]."</td>
                                                        </tr>";

                        echo "<div class='on-20px'><table class='on-table-center on-table'>";
                            echo "".$report_heading.$displayString;
                            echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        echo "</table></div>";
                        echo "<br/>";

                        ///////////////////////////
                        //  Set Export Information
                        if($excelheadings && $exceldata){
                            $_SESSION["fileName"] = "Invoice Tracking Report";
                            $_SESSION["fileData"] = array_merge($excelheadings, $exceldata);
                        }
                        ///////////////////////////

                        echo "<input name='btnExport' onClick=\"location.href='_export.php'\" type='button' value='Export to Excel' />";
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
