<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if ($_SESSION["logged_in"] === true && !($_SESSION["usertype"] === "0"))
        header("Location: home.php");

    //  Update Company Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")
    {
        $errorMessage                                                   = "";

        //  Get Information
        $id                                                             = $_GET["id"];
        $company_name                                                   = addslashes(strip_tags($_POST["company_name"]));
        $company_rate                                                   = addslashes(strip_tags($_POST["company_rate"]));
        $company_currency                                               = addslashes(strip_tags($_POST["company_currency"]));
        $company_vat                                                    = addslashes(strip_tags($_POST["company_vat"]));
        $sars_compliance												= addslashes(strip_tags($_POST["sars_compliance"]));
        $backing_documents												= addslashes(strip_tags($_POST["backing_documents"]));
        $gpsLogbook												= addslashes(strip_tags($_POST["gpsLogbook"]));

        $company_rate												= number_format($company_rate, 2, ".", "");
		$sars_compliance											= ($sars_compliance == "") ? 0 : 1;
		$backing_documents										= ($backing_documents == "") ? 0 : 1;
		$gpsLogbook												= ($gpsLogbook == "") ? 0 : 1;

        //  Check If Company Exists In Database
        $exist                                                          = q("SELECT id FROM Company WHERE name = '$company_name' AND id != '$id'");

        if (!$exist)
        {
            $update                                                     = q("UPDATE Company ".
																			"SET name = '$company_name', rate = '$company_rate', currency = '$company_currency', vat = '$company_vat', ".
																				"sars_compliance = '$sars_compliance', backing_documents = '$backing_documents', gpsLogbook = '$gpsLogbook' ".
																			"WHERE id = '$id'");

            if ($update)
            {
                $time                                                   = date("H:i:s");

                $logs                                                   = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time) ".
                                                                            "VALUES ('$company_name updated', 'Update', 'Company', '".$_SESSION["email"]."', ".
                                                                            "'$today', '$time')");

                header("Location: companies.php");
            }
        }
        else
            $errorMessage                                               = "Company Already Exists";
    }

    if ($errorMessage != "")
        echo "<p align='center' style='padding:0px;'><strong><font color='#999999'>$errorMessage</font></strong></p>";

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0", "companies");
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    var cal                                                             = new CodeThatCalendar(caldef1);

    function check()
    {
        var valid                                                       = 1;

        //  Check That Company Name Is Entered
        if (document.forms["company_edit"].company_name.value == "")
        {
            ShowLayer("companyName", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("companyName", "none");

        //  Check That Company Rate Is Valid If Entered
        if (document.forms["company_edit"].company_rate.value != "")
        {
            if (!validation("currency", document.forms["company_edit"].company_rate.value))
            {
                ShowLayer("companyRate", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("companyRate", "none");
        }
        else
            ShowLayer("companyRate", "none");

        if (valid == 1)
        {
            document.forms["company_edit"].save.value                    = 1;
            document.forms["company_edit"].submit();
        }
    }
</script>
<?php
    $id                                                                 = $_GET["id"];
    $company_info                                                       = q("SELECT name, rate, currency, vat, sars_compliance, backing_documents, gpsLogbook FROM Company WHERE id = '$id'");
?>
    <table width="100%">
        <tr>
            <td class="centerdata" valign="top">
                <form action="" method="post" name="company_edit">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Edit Company</h6>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <table cellpadding="0" cellspacing="2" width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                    Company Name:
                            </td>
                            <td align="left" width="50%">
                                <input class="on-field" name="company_name" tabindex="1" type="text" value="<?php echo $company_info[0][0]; ?>">
                                <div id="companyName" style="display: none;"><font class="on-validate-error">* Name must be entered</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                    Rate/Employee:
                            </td>
                            <td align="left" width="50%">
                                <input class="on-field" name="company_rate" tabindex="2" type="text" value="<?php echo "".number_format($company_info[0][1], 2, ".", ""); ?>">
                                <div id="companyRate" style="display: none;"><font class="on-validate-error">* Entered amount must be numeric, eg. 123.45</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                    Currency:
                            </td>
                            <td align="left" width="50%">
                                <input class="on-field" name="company_currency" tabindex="3" type="text" value="<?php echo $company_info[0][2]; ?>">
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                    Default VAT:
                            </td>
                            <td align="left" width="50%">
                                <input class="on-field" name="company_vat" tabindex="3" type="text" value="<?php echo $company_info[0][3]; ?>">
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                    Require SARS Logbook Compliance:
                            </td>
                            <td align="left" width="50%">
                                <input class="on-field" name="sars_compliance" tabindex="4" type="checkbox" value="1" <?php echo ($company_info[0][4] == "1") ? "checked" : ""; ?>>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                    Enforce Backing Documentation Uploads:
                            </td>
                            <td align="left" width="50%">
                                <input class="on-field" name="backing_documents" tabindex="4" type="checkbox" value="1" <?php echo ($company_info[0][5] == "1") ? "checked" : ""; ?>>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                    Use GPS Logbook:
                            </td>
                            <td align="left" width="50%">
                                <input class="on-field" name="gpsLogbook" tabindex="4" type="checkbox" value="1" <?php echo ($company_info[0][6] == "1") ? "checked" : ""; ?>>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <input name="btnUpdate" onClick="check();" tabindex="5" type="button" value="Update Company">
                    <input method="post" name="save" type="hidden" value="0" />
                </form>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>