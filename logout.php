<?php
    session_start();

    session_destroy();

    //  Connect To Database & Close Connection
    include("_db.php");
    include("_dbclose.php");

    header("Location: login.php");
?>