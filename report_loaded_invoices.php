<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("include/sajax.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");
        
    if (!hasAccess("REP_LOADED_INV"))
        header("Location: noaccess.php");
        
    // Class for table background color
    $colorClass = "OnTrack_#_";

    ///////////////////////////
    //  Sajax
    function getProjects($id) {
        if ($id == "null")
            $projects = q("SELECT p.id, p.name FROM Project as p 
                                    INNER JOIN companiesOnTeam as cot ON p.id = cot.project_id  
                                    WHERE p.completed = 0 
                                    AND cot.company_id = '".$_SESSION["company_id"]."' ORDER BY p.name");
        else
            $projects = q("SELECT p.id, p.name FROM Project as p 
                                    INNER JOIN companiesOnTeam as cot ON p.id = cot.project_id
                                    WHERE p.completed = 0 
                                    AND p.type_id = '$id' 
                                    AND cot.company_id = '".$_SESSION["company_id"]."' ORDER BY p.name");

        return $projects;
    }
    
    function getAreas($id){
        $areas = q("SELECT id, name FROM areas WHERE pID='$id' ORDER BY name");
        return $areas ;
    }

    $sajax_request_type = "GET";
    sajax_init();
    sajax_export("getProjects");
    sajax_export("getAreas");
    sajax_handle_client_request();
    ///////////////////////////

    //  Set Report Generated Status
    $generated                                                          = "0";

    //  Create Display Information Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")
    {   
        $errorMessage                                                   = "";

        $project_type_id = $_POST["projectType"];
        $project_id = $_POST["project"];
        $area_id = $_POST["area"];
        $invoiceType = $_POST["invoiceType"];
        $viewType = $_POST["viewType"];

        if ($viewType == "between") {
            $date_from = addslashes(strip_tags($_POST["date_from"]));
            $date_to = addslashes(strip_tags($_POST["date_to"]));
        }

        if ($project_type_id != "null" && is_numeric($project_type_id))
            $where .= "AND p.type_id = '$project_type_id'";
            
        //echo "<a>AREA_ID: $area_id</a><br>";
        if($area_id == "null"){                                                  // All Areas
            $areaSelect = ",ar.name";
            $areaFrom = "LEFT JOIN areas AS ar ON ar.id = ld.area_id";
            $areaWhere = "";
            $areaTH = "<th>Area</th>";
            $showAreas = 1;
        }else if($area_id == "" || $area_id == "none"){           // No Areas Present
            $areaSelect = "";
            $areaFrom = "";
            $areaWhere = "";
            $areaTH = "";
            $showAreas = 0;
        }else{                                                                          // A specific area
            $areaSelect = ",ar.name";
            $areaFrom = "LEFT JOIN areas AS ar ON ar.id = ld.area_id";
            $areaWhere = "AND ld.area_id = '$area_id'";
            $areaTH = "";
            $showAreas = 0;
        }

        $displayString                                                  = "";
        
        $row                                                              = 0;
        
                        $excelheadings[$row][]                              = "";   // Proj
                    if($showAreas)
                        $excelheadings[$row][]                              = "";   // Area
                        $excelheadings[$row][]                              = "";   // Inv Name
                        $excelheadings[$row][]                              = "";   // Date Created
                        $excelheadings[$row][]                              = "";   // Total
                        $excelheadings[$row][]                              = "";   // Consult
                        $excelheadings[$row][]                              = "";   // Expense
                        $excelheadings[$row][]                              = "";   // DIncome
                        $excelheadings[$row][]                              = "";   // Due Date
                        $excelheadings[$row][]                              = "";   // Paid
                            
                        $exceldata[$row][]                                     = "Report: Loaded Invoice";
                    if($showAreas)
                        $exceldata[$row][]                                     = "";
                        $exceldata[$row][]                                     = "";
                        $exceldata[$row][]                                     = "";
                        $exceldata[$row][]                                     = "";
                        $exceldata[$row][]                                     = "";
                        $exceldata[$row][]                                     = "";
                        $exceldata[$row][]                                     = "";
                        $exceldata[$row][]                                     = "";
                        $exceldata[$row][]                                     = "";
                            $row++;
                            
                        $exceldata[$row][]                                     = "Company Name: ".$_SESSION["company_name"];
                    if($showAreas)
                        $exceldata[$row][]                                     = "";
                        $exceldata[$row][]                                     = "";
                        $exceldata[$row][]                                     = "";
                        $exceldata[$row][]                                     = "";
                        $exceldata[$row][]                                     = "";
                        $exceldata[$row][]                                     = "";
                        $exceldata[$row][]                                     = "";
                        $exceldata[$row][]                                     = "";
                        $exceldata[$row][]                                     = "";
                            $row++;
                            
                 if(is_numeric($area_id) && $area_id != "null" && !$showAreas){
                    $aName = q("SELECT name FROM areas WHERE id= '".$area_id."' ");
                        
                    $exceldata[$row][]                                     = "Area: ".$aName;
                    $exceldata[$row][]                                     = "";
                    $exceldata[$row][]                                     = "";
                    $exceldata[$row][]                                     = "";
                    $exceldata[$row][]                                     = "";
                    $exceldata[$row][]                                     = "";
                    $exceldata[$row][]                                     = "";
                    $exceldata[$row][]                                     = "";
                    $exceldata[$row][]                                     = "";
                        $row++;
                }
                            
                        $exceldata[$row][]                                     = $colorClass."Project "; 
                    if($showAreas)
                        $exceldata[$row][]                                     = $colorClass."Area"; 
                        $exceldata[$row][]                                     = $colorClass."Invoice Name";
                        $exceldata[$row][]                                     = $colorClass."Date Created"; 
                        $exceldata[$row][]                                     = $colorClass."Total (".$_SESSION["currency"].")";
                        $exceldata[$row][]                                     = $colorClass."Consulting (".$_SESSION["currency"].")";
                        $exceldata[$row][]                                     = $colorClass."Expenses (".$_SESSION["currency"].")";
                        $exceldata[$row][]                                     = $colorClass."Diverse Income (".$_SESSION["currency"].")";
                        $exceldata[$row][]                                     = $colorClass."Due Date";
                        $exceldata[$row][]                                     = $colorClass."Paid";
                            $row++;
        
        $headers                                                        .= "<tr>
                                                                                        <th>Project Name</th>".
                                                                                        $areaTH."
                                                                                        <th>Invoice Name</th>
                                                                                        <th>Date Created</th>
                                                                                        <th>Total <i>(".$_SESSION["currency"].")</i></th>
                                                                                        <th>Consulting <i>(".$_SESSION["currency"].")</i></th>
                                                                                        <th>Expenses <i>(".$_SESSION["currency"].")</i></th>
                                                                                        <th>Diverse Income <i>(".$_SESSION["currency"].")</i></th>
                                                                                        <th>Due Date</th>
                                                                                        <th>Paid</th>
                                                                                    </tr>";

        if ($invoiceType == "all") {
            if ($viewType == "between") {
                if ($project_id == "all") {
                    $invoices = q("SELECT p.name, ld.name, ld.create_date, ld.consulting, ld.expenses, ld.diverse_income, ld.due_date, ld.paid, ld.vat $areaSelect 
                                            FROM (LoadInvoices AS ld INNER JOIN Project AS p ON p.id = ld.project_id 
                                            INNER JOIN companiesOnTeam as cot ON p.id = cot.project_id $areaFrom) 
                                            WHERE cot.company_id = '".$_SESSION["company_id"]."' 
                                            AND ld.create_date >= '$date_from' 
                                            AND ld.create_date <= '$date_to' $where $areaWhere ORDER BY ld.due_date, p.name");
                                           /* echo "<a>001:
                                            SELECT p.name, ld.name, ld.create_date, ld.consulting, ld.expenses, ld.diverse_income, ld.due_date, ld.paid, ld.vat $areaSelect 
                                            FROM (LoadInvoices AS ld INNER JOIN Project AS p ON p.id = ld.project_id 
                                            INNER JOIN companiesOnTeam as cot ON p.id = cot.project_id $areaFrom) 
                                            WHERE cot.company_id = '".$_SESSION["company_id"]."' 
                                            AND ld.create_date >= '$date_from' 
                                            AND ld.create_date <= '$date_to' $where $areaWhere ORDER BY ld.due_date, p.name
                                            </a>";*/
                }
                else {
                    $invoices = q("SELECT p.name, ld.name, ld.create_date, ld.consulting, ld.expenses, ld.diverse_income, ld.due_date, ld.paid, ld.vat $areaSelect  
                                            FROM (LoadInvoices AS ld INNER JOIN Project AS p ON p.id = ld.project_id 
                                            INNER JOIN companiesOnTeam as cot ON p.id = cot.project_id $areaFrom) 
                                            WHERE cot.company_id = '".$_SESSION["company_id"]."' 
                                            AND ld.create_date >= '$date_from' 
                                            AND ld.create_date <= '$date_to' $where 
                                            AND p.id = '$project_id' $areaWhere ORDER BY ld.due_date, p.name");
                                            /*echo "<a>002:
                                            SELECT p.name, ld.name, ld.create_date, ld.consulting, ld.expenses, ld.diverse_income, ld.due_date, ld.paid, ld.vat $areaSelect  
                                            FROM (LoadInvoices AS ld INNER JOIN Project AS p ON p.id = ld.project_id 
                                            INNER JOIN companiesOnTeam as cot ON p.id = cot.project_id $areaFrom) 
                                            WHERE cot.company_id = '".$_SESSION["company_id"]."' 
                                            AND ld.create_date >= '$date_from' 
                                            AND ld.create_date <= '$date_to' $where 
                                            AND p.id = '$project_id' $areaWhere ORDER BY ld.due_date, p.name
                                            </a>";*/
                }
            }
            else{
                if ($project_id == "all") {
                    $invoices = q("SELECT p.name, ld.name, ld.create_date, ld.consulting, ld.expenses, ld.diverse_income, ld.due_date, ld.paid, ld.vat $areaSelect  
                                            FROM (LoadInvoices AS ld INNER JOIN Project AS p ON p.id = ld.project_id
                                            INNER JOIN companiesOnTeam as cot ON p.id = cot.project_id $areaFrom)
                                            WHERE cot.company_id = '".$_SESSION["company_id"]."' $where $areaWhere ORDER BY ld.due_date, p.name");
                                            /*echo "<a>003:
                                            SELECT p.name, ld.name, ld.create_date, ld.consulting, ld.expenses, ld.diverse_income, ld.due_date, ld.paid, ld.vat $areaSelect  
                                            FROM (LoadInvoices AS ld INNER JOIN Project AS p ON p.id = ld.project_id
                                            INNER JOIN companiesOnTeam as cot ON p.id = cot.project_id $areaFrom)
                                            WHERE cot.company_id = '".$_SESSION["company_id"]."' $where $areaWhere ORDER BY ld.due_date, p.name
                                            </a>";*/
                }
                else {
                    $invoices = q("SELECT p.name, ld.name, ld.create_date, ld.consulting, ld.expenses, ld.diverse_income, ld.due_date, ld.paid, ld.vat $areaSelect  
                                            FROM (LoadInvoices AS ld INNER JOIN Project AS p ON p.id = ld.project_id 
                                            INNER JOIN companiesOnTeam as cot ON p.id = cot.project_id $areaFrom) 
                                            WHERE cot.company_id = '".$_SESSION["company_id"]."' $where 
                                            AND p.id = '$project_id' $areaWhere ORDER BY ld.due_date, p.name");
                                           /* echo "<a>004:
                                            SELECT p.name, ld.name, ld.create_date, ld.consulting, ld.expenses, ld.diverse_income, ld.due_date, ld.paid, ld.vat $areaSelect  
                                            FROM (LoadInvoices AS ld INNER JOIN Project AS p ON p.id = ld.project_id 
                                            INNER JOIN companiesOnTeam as cot ON p.id = cot.project_id $areaFrom) 
                                            WHERE cot.company_id = '".$_SESSION["company_id"]."' $where 
                                            AND p.id = '$project_id' $areaWhere ORDER BY ld.due_date, p.name
                                            </a>";*/
                }
            }

            //  Grand Totals
            $consulting_total                                           = 0;
            $expenses_total                                             = 0;
            $diverse_income_total                                       = 0;

            if (is_array($invoices)) {                
                foreach ($invoices as $invoice) {
                    $consulting_total                                   += $invoice[3];
                    $expenses_total                                     += $invoice[4];
                    $diverse_income_total                               += $invoice[5];

                    if ($invoice[7] == "1")
                        $checkbox                                       = "<input disabled type='checkbox' value='1' checked>";
                    else
                        $checkbox                                       = "<input disabled type='checkbox' value='1'>";

                    $total                                              = number_format(($invoice[3] + $invoice[4]), 2, ".", "");
                    $total                                              += number_format((($invoice[3] + $invoice[4]) * ($invoice[8] / 100)), 2, ".", "");
                    $sum_total                                          += number_format($total, 2, ".", "");

                    if ($invoice[3] != "")
                        $consulting                                     = number_format($invoice[3], 2, ".", "");
                    else
                        $consulting                                     = "-";

                    if ($invoice[4] != "")
                        $expenses                                       = number_format($invoice[4], 2, ".", "");
                    else
                        $expenses                                       = "-";

                    if ($invoice[5] != "")
                        $diverse_income                                 = number_format($invoice[5], 2, ".", "");
                    else
                        $diverse_income                                 = "-";
                        
                    if(!$invoice[9] || $invoice[9] == "")       // Areas
                        $invoice[9] = " - ";

                    $displayString                                      .= "<tr>
                                                                                        <td style='white-space:nowrap'>".$invoice[0]."</td>";
                    if($showAreas)
                        $displayString                                  .= "<td style='white-space:nowrap'>".$invoice[9]."</td>";
                    $displayString                                      .= "<td style='white-space:nowrap'>".$invoice[1]."</td>
                                                                                        <td class='rightdata' style='white-space:nowrap'>".$invoice[2]."</td>
                                                                                        <td class='rightdata' style='white-space:nowrap'>".number_format($total, 2, ".", "")."</td>
                                                                                        <td class='rightdata' style='white-space:nowrap'>".$consulting."</td>
                                                                                        <td class='rightdata' style='white-space:nowrap'>".$expenses."</td>
                                                                                        <td class='rightdata' style='white-space:nowrap'>".$diverse_income."</td>
                                                                                        <td class='rightdata' style='white-space:nowrap'>".$invoice[6]."</td>
                                                                                        <td class='centerdata'>".$checkbox."</td>
                                                                                    </tr>";
                                                                            
                    $exceldata[$row][]                                     = $invoice[0];
                if($showAreas)
                    $exceldata[$row][]                                     = $invoice[9];
                    $exceldata[$row][]                                     = $invoice[1];
                    $exceldata[$row][]                                     = $invoice[2];
                    $exceldata[$row][]                                     = number_format($total, 2, ".", "");
                    $exceldata[$row][]                                     = $consulting;
                    $exceldata[$row][]                                     = $expenses;
                    $exceldata[$row][]                                     = $diverse_income;
                    $exceldata[$row][]                                     = $invoice[6];
                    
                    $pos = strrpos($checkbox, "checked");
                    if ($pos === false) { 
                        $exceldata[$row][]                                     = "No";
                    }else{
                        $exceldata[$row][]                                     = "Yes";
                    }
                        $row++;
                }
            }
        }
        else {
            if ($viewType == "between"){
                if ($project_id == "all") {
                    $invoices                                           = q("SELECT p.name, ld.name, ld.create_date, ld.consulting, ld.expenses, ld.diverse_income, ld.due_date, ld.paid, ld.vat $areaSelect 
                                                                                    FROM (LoadInvoices AS ld INNER JOIN Project AS p ON p.id = ld.project_id 
                                                                                    INNER JOIN companiesOnTeam as cot ON p.id = cot.project_id $areaFrom) 
                                                                                    WHERE ld.paid = '$invoiceType' $where 
                                                                                    AND cot.company_id = '".$_SESSION["company_id"]."' 
                                                                                    AND ld.create_date >= '$date_from' 
                                                                                    AND ld.create_date <= '$date_to' $areaWhere ORDER BY ld.due_date, p.name");
                                                                                    /*echo "<a>005:
                                                                                    SELECT p.name, ld.name, ld.create_date, ld.consulting, ld.expenses, ld.diverse_income, ld.due_date, ld.paid, ld.vat $areaSelect 
                                                                                    FROM (LoadInvoices AS ld INNER JOIN Project AS p ON p.id = ld.project_id 
                                                                                    INNER JOIN companiesOnTeam as cot ON p.id = cot.project_id $areaFrom) 
                                                                                    WHERE ld.paid = '$invoiceType' $where 
                                                                                    AND cot.company_id = '".$_SESSION["company_id"]."' 
                                                                                    AND ld.create_date >= '$date_from' 
                                                                                    AND ld.create_date <= '$date_to' $areaWhere ORDER BY ld.due_date, p.name
                                                                                    </a>";*/
                }
                else {
                    $invoices                                           = q("SELECT p.name, ld.name, ld.create_date, ld.consulting, ld.expenses, ld.diverse_income, ld.due_date, ld.paid, ld.vat $areaSelect 
                                                                                    FROM (LoadInvoices AS ld INNER JOIN Project AS p ON p.id = ld.project_id 
                                                                                    INNER JOIN companiesOnTeam as cot ON p.id = cot.project_id $areaFrom) 
                                                                                    WHERE ld.paid = '$invoiceType' $where 
                                                                                    AND cot.company_id = '".$_SESSION["company_id"]."' 
                                                                                    AND ld.create_date >= '$date_from' 
                                                                                    AND ld.create_date <= '$date_to' 
                                                                                    AND p.id = '$project_id' $areaWhere ORDER BY ld.due_date, p.name");
                                                                                    /*echo "<a>006:
                                                                                    SELECT p.name, ld.name, ld.create_date, ld.consulting, ld.expenses, ld.diverse_income, ld.due_date, ld.paid, ld.vat $areaSelect 
                                                                                    FROM (LoadInvoices AS ld INNER JOIN Project AS p ON p.id = ld.project_id 
                                                                                    INNER JOIN companiesOnTeam as cot ON p.id = cot.project_id $areaFrom) 
                                                                                    WHERE ld.paid = '$invoiceType' $where 
                                                                                    AND cot.company_id = '".$_SESSION["company_id"]."' 
                                                                                    AND ld.create_date >= '$date_from' 
                                                                                    AND ld.create_date <= '$date_to' 
                                                                                    AND p.id = '$project_id' $areaWhere ORDER BY ld.due_date, p.name
                                                                                    </a>";*/
                }
            }
            else{
                if ($project_id == "all") {
                    $invoices                                           = q("SELECT p.name, ld.name, ld.create_date, ld.consulting, ld.expenses, ld.diverse_income, ld.due_date, ld.paid, ld.vat $areaSelect 
                                                                                    FROM (LoadInvoices AS ld INNER JOIN Project AS p ON p.id = ld.project_id
                                                                                    INNER JOIN companiesOnTeam as cot ON p.id = cot.project_id $areaFrom)
                                                                                    WHERE ld.paid = '$invoiceType' $where 
                                                                                    AND cot.company_id = '".$_SESSION["company_id"]."' 
                                                                                    $areaWhere ORDER BY ld.due_date, p.name");
                                                                                    /*echo "<a>007:
                                                                                    SELECT p.name, ld.name, ld.create_date, ld.consulting, ld.expenses, ld.diverse_income, ld.due_date, ld.paid, ld.vat $areaSelect 
                                                                                    FROM (LoadInvoices AS ld INNER JOIN Project AS p ON p.id = ld.project_id
                                                                                    INNER JOIN companiesOnTeam as cot ON p.id = cot.project_id $areaFrom)
                                                                                    WHERE ld.paid = '$invoiceType' $where 
                                                                                    AND cot.company_id = '".$_SESSION["company_id"]."' 
                                                                                    $areaWhere ORDER BY ld.due_date, p.name
                                                                                    </a>";*/
                }
                else {
                    $invoices                                           = q("SELECT p.name, ld.name, ld.create_date, ld.consulting, ld.expenses, ld.diverse_income, ld.due_date, ld.paid, ld.vat $areaSelect 
                                                                                    FROM (LoadInvoices AS ld INNER JOIN Project AS p ON p.id = ld.project_id 
                                                                                    INNER JOIN companiesOnTeam as cot ON p.id = cot.project_id $areaFrom) 
                                                                                    WHERE ld.paid = '$invoiceType' $where 
                                                                                    AND cot.company_id = '".$_SESSION["company_id"]."' 
                                                                                    AND p.id = '$project_id' 
                                                                                    $areaWhere ORDER BY ld.due_date, p.name");
                                                                                    /*echo "<a>008:
                                                                                    SELECT p.name, ld.name, ld.create_date, ld.consulting, ld.expenses, ld.diverse_income, ld.due_date, ld.paid, ld.vat $areaSelect 
                                                                                    FROM (LoadInvoices AS ld INNER JOIN Project AS p ON p.id = ld.project_id 
                                                                                    INNER JOIN companiesOnTeam as cot ON p.id = cot.project_id $areaFrom) 
                                                                                    WHERE ld.paid = '$invoiceType' $where 
                                                                                    AND cot.company_id = '".$_SESSION["company_id"]."' 
                                                                                    AND p.id = '$project_id' 
                                                                                    $areaWhere ORDER BY ld.due_date, p.name
                                                                                    </a>";*/
                }
            }

            //  Grand Totals
            $consulting_total                                           = 0;
            $expenses_total                                             = 0;
            $diverse_income_total                                   = 0;
            $sum_total                                                     = 0;

            if (is_array($invoices)) {
                foreach ($invoices as $invoice) {
                    $consulting_total                                   += $invoice[3];
                    $expenses_total                                     += $invoice[4];
                    $diverse_income_total                               += $invoice[5];

                    if ($invoice[7] == "1")
                        $checkbox                                       = "<input disabled type='checkbox' value='1' checked>";
                    else
                        $checkbox                                       = "<input disabled type='checkbox' value='1'>";

                    $total                                              = number_format(($invoice[3] + $invoice[4]), 2, ".", "");
                    $total                                              += number_format((($invoice[3] + $invoice[4]) * ($invoice[8] / 100)), 2, ".", "");
                    $sum_total                                          += number_format($total, 2, ".", "");

                    if ($invoice[3] != "")
                        $consulting                                     = number_format($invoice[3], 2, ".", "");
                    else
                        $consulting                                     = "-";

                    if ($invoice[4] != "")
                        $expenses                                       = number_format($invoice[4], 2, ".", "");
                    else
                        $expenses                                       = "-";

                    if ($invoice[5] != "")
                        $diverse_income                                 = number_format($invoice[5], 2, ".", "");
                    else
                        $diverse_income                                 = "-";
                        
                    if(!$invoice[9] || $invoice[9] == "")
                        $invoice[9] = " - ";

                    $displayString                                 .= "<tr><td style='white-space:nowrap'>".$invoice[0]."</td>";
                if($showAreas)
                    $displayString                                 .= "<td style='white-space:nowrap'>".$invoice[9]."</td>";
                     $displayString                                .= "<td style='white-space:nowrap'>".$invoice[1]."</td>".
                                                                            "<td class='rightdata' style='white-space:nowrap'>".$invoice[2]."</td>".
                                                                            "<td class='rightdata' style='white-space:nowrap'>".number_format($total, 2, ".", "")."</td>".
                                                                            "<td class='rightdata' style='white-space:nowrap'>".$consulting."</td>".
                                                                            "<td class='rightdata' style='white-space:nowrap'>".$expenses."</td>".
                                                                            "<td class='rightdata' style='white-space:nowrap'>".$diverse_income."</td>".
                                                                            "<td class='rightdata' style='white-space:nowrap'>".$invoice[6]."</td>".
                                                                            "<td class='centerdata'>".$checkbox."</td></tr>";
                                                                            
                    $exceldata[$row][]                                     = $invoice[0];
                if($showAreas)
                    $exceldata[$row][]                                     = $invoice[9];
                    $exceldata[$row][]                                     = $invoice[1];
                    $exceldata[$row][]                                     = $invoice[2];
                    $exceldata[$row][]                                     = number_format($total, 2, ".", "");
                    $exceldata[$row][]                                     = $consulting;
                    $exceldata[$row][]                                     = $expenses;
                    $exceldata[$row][]                                     = $diverse_income;
                    $exceldata[$row][]                                     = $invoice[6];
                    
                    $pos = strrpos($checkbox, "checked");
                    if ($pos === false) { 
                        $exceldata[$row][]                                     = "No";
                    }else{
                        $exceldata[$row][]                                     = "Yes";
                    }
                        $row++;
                }
            }
        }

        if ($displayString != "")
        {
            $displayString                                              = "".$headers.$displayString."<tr>
                                                                                    <td class='on-table-total' colspan='".(3+$showAreas)."'>Grand Total <i>(".$_SESSION["currency"].")</i>:</td>
                                                                                    <td class='on-table-total'>".number_format($sum_total, 2, ".", "")."</td>
                                                                                    <td class='on-table-total'>".number_format($consulting_total, 2, ".", "")."</td>
                                                                                    <td class='on-table-total'>".number_format($expenses_total, 2, ".", "")."</td>
                                                                                    <td class='on-table-total'>".number_format($diverse_income_total, 2, ".", "")."</td>
                                                                                    <td class='on-table-total' colspan='2'></td>
                                                                                </tr>";
                                                                            
            $exceldata[$row][]                                     = $colorClass."";
            $exceldata[$row][]                                     = $colorClass."";
        if($showAreas)
            $exceldata[$row][]                                     = $colorClass."";
            $exceldata[$row][]                                     = $colorClass."Grand Total (".$_SESSION["currency"]."):";
            $exceldata[$row][]                                     = $colorClass.number_format($sum_total, 2, ".", "");
            $exceldata[$row][]                                     = $colorClass.number_format($consulting_total, 2, ".", "");
            $exceldata[$row][]                                     = $colorClass.number_format($expenses_total, 2, ".", "");
            $exceldata[$row][]                                     = $colorClass.number_format($diverse_income_total, 2, ".", "");
            $exceldata[$row][]                                     = $colorClass."";
            $exceldata[$row][]                                     = $colorClass."";

            $generated                                                  = "1";
        }
    } 

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("5", "reports");

    if ($errorMessage != "") {
        echo "<p align='center' style='padding:0px;'><strong><font class='on-validate-error'>$errorMessage</font></strong></p>";
        echo "<br/>";
    }
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    jQuery(function(){
	jQuery('#date_from').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_from').val(),
		current: jQuery('#date_from').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_from').val()))
			_date = new Date();
			else _date = jQuery('#date_from').val();
			jQuery('#date_from').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_from').val(formated);
			jQuery('#date_from').DatePickerHide();
		}
	});
    })

    jQuery(function(){
	jQuery('#date_to').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_to').val(),
		current: jQuery('#date_to').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_to').val()))
			_date = new Date();
			else _date = jQuery('#date_to').val();
			jQuery('#date_to').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_to').val(formated);
			jQuery('#date_to').DatePickerHide();
		}
	});
    })

    function check()
    {
        var valid                                                       = 1;

        //  Check That Employee Is Selected
        if (document.forms["report_loaded_invoices"].project.value == "null")
        {
            ShowLayer("projectDiv", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("projectDiv", "none");

        //  Check That Date From Is Entered
        if (document.forms["report_loaded_invoices"].date_from.value == "")
        {
            ShowLayer("dateFrom", "block");
            valid                                                       = 0;
        }
        //  Check That Entered Date From Is Valid
        else
        {
            ShowLayer("dateFrom", "none");

            if (!validation("date", document.forms["report_loaded_invoices"].date_from.value))
            {
                ShowLayer("dateFromDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("dateFromDiv", "none");
        }

        //  Check That Date To Is Entered
        if (document.forms["report_loaded_invoices"].date_to.value == "")
        {
            ShowLayer("dateTo", "block");
            valid                                                       = 0;
        }
        //  Check That Entered Date To Is Valid
        else
        {
            ShowLayer("dateTo", "none");

            if (!validation("date", document.forms["report_loaded_invoices"].date_to.value))
            {
                ShowLayer("dateToDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("dateToDiv", "none");
        }

        if (valid == 1)
        {
            document.forms["report_loaded_invoices"].save.value         = 1;
            document.forms["report_loaded_invoices"].submit();
        }
    }

    function viewSelection()
    {
        if (document.forms["report_loaded_invoices"].viewType.value == "between")
            ShowLayer("dateDisplay", "block");
        else
            ShowLayer("dateDisplay", "none");
    }

    ///////////////////////////
    //  Sajax
    <?php sajax_show_javascript(); ?>

    function ClearOptions(OptionList) {
        for (x = OptionList.length; x >= 0; x--) {
            OptionList[x] = null;
        }
    }
    
    //Set Projects
    function setProjects(data) {
        var c = 0;

        document.forms["report_loaded_invoices"].project.options[(c++)] = new Option("--  Select A Project  --", "null");
        document.forms["report_loaded_invoices"].project.options[(c++)] = new Option("All Projects", "all");

        for (var i in data)
            eval("document.forms['report_loaded_invoices'].project.options["+(c++)+"] = new Option('" + data[i][1] + "', '" + data[i][0] + " ');");
    }

    //Set Areas
    function setAreas(data) {
        if(!data)
            ShowLayer("areaDiv", "none");
        var c                                                           = 0;
        document.report_loaded_invoices.area.options[(c++)]                         = new Option("All Areas", "null");
        for (var i in data)
            eval("document.report_loaded_invoices.area.options["+(c++)+"] = new Option('" + data[i][1] + "', '" + data[i][0] + "');");
    }
    
    //Get Projects
    function getProject(select) {
        ClearOptions(document.forms["report_loaded_invoices"].project);

        x_getProjects(select.value, setProjects);
    }
    
    
    //Get Areas
    function getAreas(select) {
        ClearOptions(document.report_loaded_invoices.area);
        if (select.value != "null")
        {
            ShowLayer("areaDiv", "block");
            x_getAreas(select.value, setAreas);
        }
        else{
            ShowLayer("areaDiv", "none");
        }
    }
    ///////////////////////////
</script>
<?php
    $projectTypes = q("SELECT id, type FROM ProjectTypes WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY type");
    $projectTypes[] = array("0","Shared Projects");
    $projectTypes = sortArrayByColumn($projectTypes, 1);
    
    $projects = q("SELECT p.id, p.name FROM Project as p 
                            INNER JOIN companiesOnTeam as cot ON p.id = cot.project_id
                            WHERE p.completed = 0 
                            AND cot.company_id = '".$_SESSION["company_id"]."' ORDER BY p.name");
    
    ?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="report_loaded_invoices">
                    <div id="" style="<?php if ($generated == "1") echo "display: none;"; else echo "display: block"; ?>">
                        <table width="100%">
                            <tr>
                                <td class="centerdata">
                                    <h6>Loaded Invoice Report</h6>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Project Type:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="projectType" onChange="getProject(projectType);">
                                        <option value="null">All Project Types</option>
                                        <?php
                                            if (is_array($projectTypes))
                                                foreach ($projectTypes as $projectType)
                                                    if ($_POST["projectType"] == $projectType[0])
                                                        echo "<option value='".$projectType[0]."' selected>".$projectType[1]."</option>";
                                                    else
                                                        echo "<option value='".$projectType[0]."'>".$projectType[1]."</option>";
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Project:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" id="project" name="project" onChange="getAreas(project)">
                                        <option value="null">--  Select A Project  --</option>
                                        <option value="all">All Projects</option>
                                        <?php
                                            if (is_array($projects))
                                                foreach ($projects as $project)
                                                    if (isset($_POST["project"]) && $_POST["project"] == $project[0])
                                                        echo "<option value='".$project[0]."' selected>".$project[1]."</option>";
                                                    else
                                                        echo "<option value='".$project[0]."'>".$project[1]."</option>";
                                        ?>
                                    </select>
                                    <div id="projectDiv" style="display: none;"><font class="on-validate-error">* Project must be selected</font></div>
                                </td>
                            </tr>
                        </table>
                        
                        <div id="areaDiv" style="display: none;">
                            <table width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Area:
                                    </td>
                                    <td width='50%'>
                                        <select class='on-field' method='post' name='area'>
                                            <option value='none'>All Areas</option>";
                                                <?php if (is_array($areas))
                                                    foreach ($areas as $area)
                                                        if ($invoice_info[0][8] == $area[0])
                                                            echo "<option value='".$area[0]."' selected>".$area[1]."</option>";
                                                        else
                                                            echo "<option value='".$area[0]."'>".$area[1]."</option>";
                                                        ?>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Invoice Type:
                                </td>
                                <td class="on-description-left" width="50%">
                                    <input method="post" name="invoiceType" type="radio" value="all" checked><a>All Invoices</a><br/>
                                    <input method="post" name="invoiceType" type="radio" value="0"><a>Unpaid Invoices</a><br/>
                                    <input method="post" name="invoiceType" type="radio" value="1"><a>Paid Invoices</a><br/>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Viewing Type:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="viewType" onChange="viewSelection();">
                                        <option value="all">View All Invoices</option>
                                        <option value="between">View Invoices Between Dates</option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                        <div id="dateDisplay" style="display: none;">
                            <table width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Date From:
                                    </td>
                                    <td width="50%">
                                        <input class="on-field-date" id="date_from" name="date_from" type="text" style="text-align:right;" value="<?php
                                            if ($_POST["date_from"] != "") echo "".$_POST["date_from"]; else echo "".getMonthStart($today); ?>">
                                        <div id="dateFrom" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                        <div id="dateFromDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="on-description" width="50%">
                                        Date To:
                                    </td>
                                    <td width="50%">
                                        <input class="on-field-date" id="date_to" name="date_to" type="text" style="text-align:right;" value="<?php
                                            if ($_POST["date_to"] != "") echo "".$_POST["date_to"]; else echo "".getMonthEnd($today); ?>">
                                        <div id="dateTo" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                        <div id="dateToDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <br/>
                        <input name="btnGenerate" onClick="check();" type="button" value="Generate Report">
                        <input method="post" name="save" type="hidden" value="0" />
                    </div>
                </form>
                <div id="report" style="<?php if ($generated == "1") echo "display: block;"; else echo "display: none"; ?>">
                    <?php
                        $report_heading                                 = "<tr>
                                                                                            <td class='on-table-clear' colspan='100%'><a>Loaded Invoice Report<a></td>
                                                                                        </tr><tr>
                                                                                            <td class='on-table-clear' colspan='100%'>Company Name: ".$_SESSION["company_name"]."</td>
                                                                                    </tr>";
                        if(is_numeric($area_id) && is_array($invoices) && $area_id != "null"){
                            $aName = q("SELECT name FROM areas WHERE id= '".$area_id."' ");
                            $report_heading .= "<tr><td class='on-table-clear' colspan='100%'>Area: ".$aName."</td></tr>";
                        }

                        echo "<div class='on-20px'><table class='on-table-center on-table'>";
                            echo "".$report_heading.$displayString;
                            echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        echo "</table></div>";
                        echo "<br/>";

                        ///////////////////////////
                        if($exceldata && $excelheadings){
                            /*
                            echo "<a>";
                            print_r($excelheadings);
                            echo "<br><br>";
                            print_r($exceldata);
                            echo "</a>";
                            */
                            //  Set Export Information
                            $_SESSION["fileName"] = "Loaded Invoices Report";
                            $_SESSION["fileData"] = array_merge($excelheadings, $exceldata);
                        }
                        ///////////////////////////

                        echo "<input name='btnExport' onClick=\"location.href='_export.php'\" type='button' value='Export to Excel' />";
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
