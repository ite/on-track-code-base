<?php
    //TODO:  check if record exists
    //TODO:  check if update necessary
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    if (!hasAccess("LOCATION_MANAGEMENT"))
        header("Location: noaccess.php");

    function getLocationInfo($id) {
        unset($info);

        if ($id != "")  {
            $data = q("SELECT id, description, latitude, longitude FROM locations WHERE id = '".$id."'");

            if (is_array($data))
                $info = $data[0];
        }
        else    {
			$info[0] = (isset($_POST["id"])) ? addslashes(strip_tags($_POST["id"])) : "";
			$info[1] = (isset($_POST["description"])) ? addslashes(strip_tags($_POST["description"])) : "";
			$info[2] = (isset($_POST["latitude"])) ? addslashes(strip_tags($_POST["latitude"])) : "";
			$info[3] = (isset($_POST["longitude"])) ? addslashes(strip_tags($_POST["longitude"])) : "";
        }

        return $info;
    }

    if (isset($_POST["save"]) && $_POST["save"] === "1")    {
        $errorMessage = "";

        $locationInfo = getLocationInfo("");
        $date = date("Y-m-d H:i:s");
        $user = $_SESSION["email"];
        $companyID = $_SESSION["company_id"];

		//  Check if Lat/Long Exists In Database
		$latLongExist = q("SELECT id, description FROM locations WHERE id != '".$locationInfo[0]."' AND latitude = '".$locationInfo[2]."' AND longitude = '".$locationInfo[3]."' AND company_id = '".$companyID."'");

		if (!is_array($latLongExist))	{
			//  Check if Lat/Long Exists In Database
			$exist = q("SELECT id, description FROM locations WHERE id != '".$locationInfo[0]."' AND description = '".$locationInfo[1]."' AND company_id = '".$companyID."'");

			if (!is_array($exist))	{
				if ($locationInfo[0] == "")  {               //  DO INSERT
					$query = "INSERT INTO locations (company_id, description, latitude, longitude,dateCreated,createdBy,dateUpdated,updatedBy) VALUES ";
					$values = "('".$companyID."', '".$locationInfo[1]."','".$locationInfo[2]."','".$locationInfo[3]."','".$date."','".$user."','".$date."','".$user."')";

					$query .= $values;
				}
				else if (is_numeric($locationInfo[0]))    {  //  DO UPDATE
					$query = "UPDATE locations SET description = '".$locationInfo[1]."',latitude = '".$locationInfo[2]."',longitude = '".$locationInfo[3]."',".
								"dateUpdated = '".$date."',updatedBy = '".$user."' WHERE id = '".$locationInfo[0]."' AND company_id = '".$companyID."'";
				}

				$runQuery = q($query);

				$what = (substr($query, 0, 6) == "INSERT") ? $locationInfo[1]." created" : $locationInfo[1]."updated";
				$access = (substr($query, 0, 6) == "INSERT") ? "Insert" : "Update";

				$logs = q("INSERT INTO Logs (what,access,on_table,by_user,on_date,on_time,company_id) VALUES ".
							"('".$what."','".$access."','locations','".$user."','".date("Y-m-d")."','".date("H:i:s")."','".$companyID."')");

				header("Location: locations.php");
			} else
				$errorMessage = "Location with entered description already exists";
		} else
			$errorMessage = "Location with entered Lat/Long already exists - ".$latLongExist[0][1];
    }
    else
        $locationInfo = getLocationInfo($_POST["id"]);

    $index = 1;

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0");
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
	//  Calendar
	jQuery(function()    {
		jQuery("#btnAddEdit").click(function()    {
			var valid = true;

			var description = jQuery("#description").val();
			var latitude = jQuery("#latitude").val();
			var longitude = jQuery("#longitude").val();

			test = validation("required", description);

			if (!test)	jQuery("#descriptionDiv").show();
			else				jQuery("#descriptionDiv").hide();

			valid &= test;

			test = validation("required-latitude", latitude);

			if (!test)	jQuery("#latitudeDiv").show();
			else				jQuery("#latitudeDiv").hide();

			valid &= test;

			test = validation("required-longitude", longitude);

			if (!test)	jQuery("#longitudeDiv").show();
			else				jQuery("#longitudeDiv").hide();

			valid &= test;

			if (valid) {
				jQuery("#save").val("1");
				jQuery("#content").submit();
			}
		});
	});
</script>
<?php
	if ($errorMessage != "")
		echo "<center><strong><font class='on-validate-error'>$errorMessage</font></strong></center><br/>";
?>
<table width="100%">
	<tr height="380px">
		<td class="centerdata" valign="top">
			<form action="" method="post" id="content" name="content">
				<table width="100%">
					<tr>
						<td class="centerdata">
							<h6>
								<?php echo ($locationInfo[0] == "") ? "Add" : "Update"; ?> Location
							</h6>
						</td>
					</tr>
					<tr>
						<td class="centerdata">
							<br/>
						</td>
					</tr>
				</table>
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                   Location Description:
                            </td>
                            <td width="50%">
                                <input class="on-field" id="id" name="id" type="hidden" value="<?php echo $locationInfo[0]; ?>" />
                                <input class="on-field" id="description" name="description" tabindex="<?php echo $index++; ?>" type="text" value="<?php echo $locationInfo[1]; ?>" />
                                <div id="descriptionDiv" style="display: none;"><font class="on-validate-error">* Location description must be entered</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                Latitude
                            </td>
                            <td width="50%">
                                <input class="on-field-date" id="latitude" name="latitude" tabindex="<?php echo $index++; ?>" type="text" value="<?php echo $locationInfo[2]; ?>" />
                                <div id="latitudeDiv" style="display: none;"><font class="on-validate-error">* Need to be entered and fit xx.xxxxxx Example: -23.456324</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                Longitude
                            </td>
                            <td width="50%">
                                <input class="on-field-date" id="longitude" name="longitude" tabindex="<?php echo $index++; ?>" type="text" value="<?php echo $locationInfo[3]; ?>" />
                                <div id="longitudeDiv" style="display: none;"><font class="on-validate-error">* Need to be entered and fit xx.xxxxxx Example: 23.456324</font></div>
                            </td>
                        </tr>
                    </table>
				<br/>
				<input id="btnAddEdit" name="btnAddEdit" tabindex="<?php echo $index++; ?>" type="button" value="Add/Update" />
				<input id="save" name="save" type="hidden" value="0" />
			</form>
		</td>
	</tr>
	<tr>
		<td class="centerdata">
			<br/>
		</td>
	</tr>
</table>
<?php
	//  Print Footer
	print_footer();
?>
