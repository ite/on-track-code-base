<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

	if (!hasAccess("SPM_MANAGE"))
		header("Location: noaccess.php");

    $index = 1;

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0");

    if ($errorMessage != "")    {
        echo "<p style='text-align:center; width:100%; padding:0px;'><strong><font color='orange'>$errorMessage</font></strong></p>";
        echo "<br/>";
    }
?>
<script type="Text/JavaScript">
    var sp_projects_LOAD;

    jQuery(function()    {
        sp_projects_LOAD = function()  {
            var alpha = jQuery("#alpha").val();
            var completed = jQuery("#completed").val();

            jQuery.post("_ajax.php", {func: "get_sp_projects", alpha: alpha, completed: completed}, function(data)	{
                data = json_decode(data);

                jQuery("#tableContent tbody>tr").not(":first").not(":last").remove();
                var lstRow = jQuery("#tableContent tbody>tr:last");

                jQuery.each(data,function(i, v)	{
                    var n = lstRow.clone(true);

                    n.children().eq(0).children().eq(0).text((v[1] != null) ? v[1] : "");
                    n.children().eq(0).children().eq(0).attr("id", (v[0] != null) ? v[0] : "");
                    n.children().eq(0).children().eq(0).attr("name", (v[0] != null) ? v[0] : "");
                    n.children().eq(0).children().eq(0).attr("class", "alpha");
                    n.children().eq(1).children().eq(0).attr("id", (v[0] != null) ? v[0] : "");
                    n.children().eq(1).children().eq(0).attr("name", (v[0] != null) ? v[0] : "");
                    n.children().eq(1).children().eq(0).attr("value", (v[2] != 1) ? "Complete Project" : "Re-Open Project");

                    n.insertBefore(lstRow);
                });

                lstRow.remove();
            });
        };
        sp_projects_LOAD();

        jQuery(".btnAdd").click(function() {
            jQuery("#id").val("");
            jQuery("#form").attr("action", "sp_projectsAddEdit.php");
            jQuery("#form").submit();
        });

        jQuery(".btnEdit").click(function() {
            jQuery("#id").val(jQuery(this).attr("id"));
            jQuery("#form").attr("action", "sp_projectsAddEdit.php");
            jQuery("#form").submit();
        });

        jQuery(".btnComplete").click(function() {
            var btnValue = jQuery(this).val();

            var completed = (btnValue == "Complete Project") ? 1 : 0;

            var query = "UPDATE Project SET completed = '" + completed + "' WHERE id = '" + jQuery(this).attr("id") + "'";

            jQuery.post("_ajax.php", {func: "query_direct", query: query}, function(data)	{
                jQuery("#alpha").val("");

                sp_projects_LOAD();
            });
        });

        jQuery("#completed").change(function() {
            jQuery("#alpha").val("");

            sp_projects_LOAD();
        });

        jQuery("#search").keyup(function() {
            jQuery("#alpha").val(jQuery(this).val());

            sp_projects_LOAD();
        });

        jQuery(".alpha").click(function() {
            jQuery("#alpha").val(jQuery(this).attr("id"));

            sp_projects_LOAD();
        });
    });
</script>
    <table width="100%">
        <tr height="440px">
            <td class="centerdata">
                <form id="form" name="form" action="" method="post">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Shared Project Management</h6>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                Project Status:
                            </td>
                            <td width="50%">
                               <select class="on-field" id="completed" name="completed" method="post" tabindex="<?php echo $index++; ?>">
                                    <option value="0" checked>Uncompleted</option>
                                    <option value="1">Completed</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                    <table id="searchTbl" name="searchTbl" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                Search Uncompleted:
                            </td>
                            <td width="50%">
                               <input class="on-field" id="search" name="search" type="text" tabindex="<?php echo $index++; ?>" value="" />
                            </td>
                        </tr>
                    </table>
                    <?php
                        $cntr = 0;
                        $alpha = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");

                        echo "<br/>";
                        echo "| <a id='' name='' class='alpha'>View All Projects</a> |";
                        echo "<br/><br/>";

                        foreach ($alpha as $a)  {
                            $cntr++;
                            echo "<a id='$a' name='$a' class='alpha'>$a</a>";
                            echo ($cntr != count($alpha)) ? " | " : "";
                        }
                    ?>
                    <br/><br/>
                    <input id="btnAdd" name="btnAdd" class="btnAdd" tabindex="<?php echo $index++; ?>" type="button" value="Add New">
                    <br/><br/>
                    <table id="tableContent" name="tableContent" class="on-table-center on-table">
                        <tr>
                            <th>Project Name</th>
                            <th></th>
                        </tr>
                        <?php
                            echo "<tr>";
                                echo "<td>";
                                    echo "<a id='' name='' class='btnEdit' tabindex='".$index++."'></a>";
                                echo "</td>";
                                echo "<td>";
                                    echo "<input id='' name='' class='btnComplete' type='button' tabindex='".$index++."' value='' />";
                                echo "</td>";
                            echo "</tr>";
                            echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        ?>
                    </table>
                    <br/>
                    <input id="btnAdd" name="btnAdd" class="btnAdd" tabindex="<?php echo $index++; ?>" type="button" value="Add New">
                    <input id="id" name="id" type="hidden" value="">
                    <input id="alpha" name="alpha" type="hidden" value="" />
                </form>
            </td>
        </tr>
        <tr>
            <td>
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
