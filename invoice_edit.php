<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("_functions.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

	if (!hasAccess("INVOICE_MANAGE"))
		header("Location: noaccess.php");

    $project_id                                                         = "null";
    $area_id                                                              = "null";

    if (isset($_POST["project"]))
        $project_id                                                     = $_POST["project"];
        
    if (isset($_POST["area"]))
        $area_id                                                     = $_POST["area"];

    //  Update Invoice Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")
    {
        $errorMessage                                                   = "";

        //  Get Information
        $id                                                             = $_GET["id"];
        $project_id                                                     = $_POST["project"];
        $area_id                                                         = $_POST["area"];
        $create_date                                                    = addslashes(strip_tags($_POST["create_date"]));
        $invoice_name                                                   = addslashes(strip_tags($_POST["invoice_name"]));
        $consulting                                                     = addslashes(strip_tags($_POST["consulting"]));
        $expenses                                                       = addslashes(strip_tags($_POST["expenses"]));
        $diverse_income                                                 = addslashes(strip_tags($_POST["diverse_income"]));
        $vat                                                            = addslashes(strip_tags($_POST["VAT"]));
        $due_date                                                       = addslashes(strip_tags($_POST["due_date"]));

        if ($consulting != "")
            $consulting                                                 = number_format($consulting, 2, ".", "");

        if ($expenses != "")
            $expenses                                                   = number_format($expenses, 2, ".", "");

        if ($diverse_income != "")
            $diverse_income                                             = number_format($diverse_income, 2, ".", "");

        if ($vat != "")
            $vat                                                        = round($vat, 2);
        else
            $vat                                                        = 0;

        //  Get Old Amount Information
        $information                                                    = q("SELECT expenses, diverse_income, vat, due_date FROM LoadInvoices WHERE id = '$id'");
        $old_expense                                                    = ($information[0][0]) * 1;
        $old_diverse_income                                             = ($information[0][1]) * 1;
        $old_vat                                                        = ($information[0][2]) * 1;
        $old_date                                                       = $information[0][3];

        //  Check If Invoice Exists In Database
        $exist                                                          = q("SELECT id FROM LoadInvoices WHERE name = '$invoice_name' AND project_id = '$project_id' ".
                                                                            "AND id != '$id'");
        if (submitDateCheck($create_date) && submitDateCheck($due_date))    {
        if (!$exist)
        {
            if($area_id != ""){
                if($area_id == "none")
                    $area_id = 0;
                
                $update                                                     = q("UPDATE LoadInvoices SET create_date = '$create_date', name = '$invoice_name', ".
                                                                                        "consulting = '$consulting', expenses = '$expenses', diverse_income = '$diverse_income', ".
                                                                                        "vat = '$vat', due_date = '$due_date', area_id = '$area_id' WHERE id = '$id'");
            }else{
                $update                                                     = q("UPDATE LoadInvoices SET create_date = '$create_date', name = '$invoice_name', ".
                                                                                        "consulting = '$consulting', expenses = '$expenses', diverse_income = '$diverse_income', ".
                                                                                        "vat = '$vat', due_date = '$due_date' WHERE id = '$id'");
            }

            if ($update)
            {
                //  Check If Invoice Has Linked Expenses
                $countExpenses                                          = q("SELECT COUNT(id) FROM ExpenseBudget WHERE invoice_id = '$id'");

                if ($countExpenses > 1 && $old_expense != $expenses) {
                    $expenseAmount                                      = $expenses - $diverse_income;
                    $expenseDueDate                                     = getDates($due_date, 7);
                    $delete                                             = q("DELETE FROM ExpenseBudget WHERE invoice_id = '$id'");
                    $insert                                             = q("INSERT INTO ExpenseBudget (project_id, descr, amount, vat, due_date, invoice_id, ".
                                                                            "company_id, paid) VALUES ('$project_id', '$invoice_name', '$expenseAmount', '$vat', ".
                                                                            "'$expenseDueDate', '$id', '".$_SESSION["company_id"]."', '0')");
                }
                else if ($countExpenses == 1 && $old_expense != $expenses) {
                    $expenseAmount                                      = $expenses - $diverse_income;
                    $expenseDueDate                                     = getDates($due_date, 7);
                    $update                                             = q("UPDATE ExpenseBudget SET amount = '$expenseAmount', vat = '$vat', ".
                                                                            "due_date = '$expenseDueDate' WHERE invoice_id = '$id'");
                }
/*
                else if ($countExpenses == 0 && $expenses != "0.00" && $_POST["loadExpense"] == "1") {
                    $expenseName                                        = addslashes(strip_tags($_POST["expDescr"]));
                    $expenseAmount                                      = addslashes(strip_tags($_POST["expAmount"]));
                    $expenseDueDate                                     = addslashes(strip_tags($_POST["expDueDate"]));

                    $insert                                             = q("INSERT INTO ExpenseBudget (project_id, descr, amount, vat, due_date, invoice_id, ".
                                                                            "company_id, paid) VALUES ('$project_id', '$expenseName', '$expenseAmount', '$vat', ".
                                                                            "'$expenseDueDate', '$id', '".$_SESSION["company_id"]."', '0')");
                }
*/
                else if ($countExpenses == 0 && $expenses != "" && $expenses != 0) {
                    $expenseName                                        = $invoice_name;
                    $expenseAmount                                      = number_format(($expenses - $diverse_income), 2, ".", "");
                    $expenseDueDate                                     = getDates($due_date, 7);

                    $insert                                             = q("INSERT INTO ExpenseBudget (project_id, descr, amount, vat, due_date, invoice_id, ".
                                                                            "company_id, paid) VALUES ('$project_id', '$expenseName', '$expenseAmount', '$vat', ".
                                                                            "'$expenseDueDate', '$id', '".$_SESSION["company_id"]."', '0')");
                }

                //  Call Recalculation Function
                $old_amount                                             = ($old_expense+($old_expense*($old_vat/100)))-($old_diverse_income +
                                                                            ($old_diverse_income*($old_vat/100)));
                $new_amount                                             = ($expenses+($expenses*($vat/100)))-($diverse_income+($diverse_income*($vat/100)));
                $amount                                                 = $new_amount - $old_amount;

				if ($amount != 0)
					statusUpdateAmount($old_date, $amount, $_SESSION["company_id"]);

				if ($old_date != $due_date){
					statusUpdateDate($old_date, $due_date, "invoice", $new_amount, $_SESSION["company_id"]);
				}

                $time                                                   = date("H:i:s");

                $project_name                                           = q("SELECT name FROM Project WHERE id = '$project_id'");
                $total                                                  = ($consulting + $expenses);
                $message                                                = "Invoice updated by ".$_SESSION["email"]." on ".$today." at ".$time.
                                                                            "\nProject Name:\t".$project_name."\nInvoice Name:\t".$invoice_name."\nTotal:\t\t".
                                                                            $_SESSION["currency"]." ".number_format(($total + ($total * ($vat / 100))), 2, ".", "").
                                                                            "\nConsulting:\t\t".$_SESSION["currency"]." ".number_format($consulting, 2, ".", "").
                                                                            "\nExpenses:\t\t".$_SESSION["currency"]." ".number_format($expenses, 2, ".", "").
                                                                            "\nDiverse Income:\t".$_SESSION["currency"]." ".number_format($diverse_income, 2, ".", "").
                                                                            "\nDue Date:\t\t".$due_date;
                $notify_id                                              = q("SELECT id FROM Notify WHERE code = 'Invoice'");
                $events                                                 = q("INSERT INTO Events (date, time, message, notify_id, processed, user_id, company_id) ".
                                                                            "VALUES ('".$today."', '".$time."', '$message', '$notify_id', '0', ".
                                                                            "'".$_SESSION["user_id"]."', '".$_SESSION["company_id"]."')");

                $logs                                                   = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('".$invoice_name." updated', 'Update', 'LoadInvoices', '".$_SESSION["email"]."', ".
                                                                            "'$today', '$time', '".$_SESSION["company_id"]."')");

                header("Location: invoice_load.php");
            }
        }
        else
            $errorMessage                                               = "Invoice Already Exists";
        }
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("2", "budget");

    if ($errorMessage != "")
    {
        if ($errorMessage == "Please note that we are currently busy with data management, no entries are allowed for 2010-08-26. Please check back in 24 Hours.")
            echo "<p align='center' style='padding:0px;'><strong style='font-size:12px;'><font class='on-validate-error'>$errorMessage</font></strong></p>";
        else
            echo "<p align='center' style='padding:0px;'><strong><font class='on-validate-error'>$errorMessage</font></strong></p>";
        echo "<br/>";
    }
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    jQuery(function(){
	jQuery('#create_date').DatePicker({
		format:'Y-m-d',
		date: jQuery('#create_date').val(),
		current: jQuery('#create_date').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#create_date').val()))
			_date = new Date();
			else _date = jQuery('#create_date').val();
			jQuery('#create_date').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#create_date').val(formated);
			jQuery('#create_date').DatePickerHide();
		}
	});
    })

    jQuery(function(){
	jQuery('#due_date').DatePicker({
		format:'Y-m-d',
		date: jQuery('#due_date').val(),
		current: jQuery('#due_date').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#due_date').val()))
			_date = new Date();
			else _date = jQuery('#due_date').val();
			jQuery('#due_date').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#due_date').val(formated);
			jQuery('#due_date').DatePickerHide();
		}
	});
    })

    jQuery(function(){
	jQuery('#date_from').DatePicker({
		format:'Y-m-d',
		date: jQuery('#expDueDate').val(),
		current: jQuery('#expDueDate').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#expDueDate').val()))
			_date = new Date();
			else _date = jQuery('#expDueDate').val();
			jQuery('#expDueDate').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#expDueDate').val(formated);
			jQuery('#expDueDate').DatePickerHide();
		}
	});
    })

    function check(box)
    {
        var valid                                                       = 1;

        //  Check That Date Created Is Valid, If Entered
        if (document.forms["invoice_edit"].create_date.value != "")
        {
            if (!validation("date", document.forms["invoice_edit"].create_date.value))
            {
                ShowLayer("createDate", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("createDate", "none");
        }
        else
            ShowLayer("createDate", "none");

        //  Check That Invoice Name Is Entered
        if (document.forms["invoice_edit"].invoice_name.value == "")
        {
            ShowLayer("invoiceName", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("invoiceName", "none");

        //  Check That Consulting Is Valid, If Entered
        if (document.forms["invoice_edit"].consulting.value != "")
        {
            if (!validation("currency", document.forms["invoice_edit"].consulting.value))
            {
                ShowLayer("consultingDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("consultingDiv", "none");
        }
        else
            ShowLayer("consultingDiv", "none");

        //  Check That Expenses Is Valid, If Entered
        if (document.forms["invoice_edit"].expenses.value != "")
        {
            if (!validation("currency", document.forms["invoice_edit"].expenses.value))
            {
                ShowLayer("expensesDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("expensesDiv", "none");
        }
        else
            ShowLayer("expensesDiv", "none");

        //  Check That Diverse Income Is Valid, If Entered
        if (document.forms["invoice_edit"].diverse_income.value != "")
        {
            if (!validation("currency", document.forms["invoice_edit"].diverse_income.value))
            {
                ShowLayer("diverseIncomeDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("diverseIncomeDiv", "none");
        }
        else
            ShowLayer("diverseIncomeDiv", "none");

        //  Check That Diverse Income Is Valid, If Entered
        if (document.forms["invoice_edit"].VAT.value != "")
        {
            if (!validation("percentage", document.forms["invoice_edit"].VAT.value))
            {
                ShowLayer("vatDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("vatDiv", "none");
        }
        else
            ShowLayer("vatDiv", "none");

        //  Check That Due Date Is Valid, If Entered
        if (document.forms["invoice_edit"].due_date.value != "")
        {
            if (!validation("date", document.forms["invoice_edit"].due_date.value))
            {
                ShowLayer("dueDate", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("dueDate", "none");
        }
        else
            ShowLayer("dueDate", "none");

        //  Check That Consulting/Expense Is Entered If Diverse Income Is Entered
        if (document.forms["invoice_edit"].diverse_income.value != "")
        {
            if (document.forms["invoice_edit"].expenses.value == "" && document.forms["invoice_edit"].consulting.value == "")
            {
                ShowLayer("invoiceDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("invoiceDiv", "none");
        }
        else
            ShowLayer("invoiceDiv", "none");

        if (valid == 1)
        {
            document.forms["invoice_edit"].save.value                   = 1;
            document.forms["invoice_edit"].submit();
        }
    }

    function showExpense() {
        if (document.forms["invoice_edit"].loadExpense.checked == true) {
            document.forms["invoice_edit"].expDescr.value = document.forms["invoice_edit"].invoice_name.value; 
            document.forms["invoice_edit"].expAmount.value = (document.forms["invoice_edit"].expenses.value - document.forms["invoice_edit"].diverse_income.value).toFixed(2);
            var dateString = document.forms["invoice_edit"].due_date.value;
            dateString = dateString.substring(5, 7) + "/" + dateString.substring(8, 10) + "/" + dateString.substring(0, 4);

            var newDate = new Date(dateString);
            newDate.setDate(newDate.getDate()+7);

            var year = newDate.getFullYear( );
            var month = newDate.getMonth( ) + 1;

            if (month < 10)
                    month = '0' + month;

            var day = newDate.getDate( );

            if (day < 10)
                    day = '0' + day;

            document.forms["invoice_edit"].expDueDate.value = year + "-" + month + "-" + day;

            ShowLayer("expenseDiv", "block");
        }
        else
            ShowLayer("expenseDiv", "none");
    }
</script>
<?php
    $id                                                                 = $_GET["id"];
    $invoice_info                                                       = q("SELECT project_id, create_date, name, consulting, expenses, diverse_income, vat, due_date, area_id ".
                                                                            "FROM LoadInvoices WHERE id = '$id'");
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="invoice_edit">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Edit Invoice</h6>
                            </td>
                        </tr>
                        <tr>
                            <td class="centerdata">
                                <br/>
                            </td>
                        </tr>
                    </table>
                    <input method="post" name="project" type="hidden" value="<?php echo $invoice_info[0][0]; ?>" />
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                Date Created:
                            </td>
                            <td width="50%">
                                <input class="on-field-date" id="create_date" name="create_date" tabindex="1" type="text" style="text-align:right;" value="<?php echo $invoice_info[0][1]; ?>">
                                <div id="createDate" style="display: none;"><font class="on-validate-error">*Registration date not valid, eg. <?php echo "".$today; ?></font></div>
                            </td>
                        </tr>
                        <?php 
                            $areas = q("SELECT id, name FROM areas WHERE pID='".$invoice_info[0][0]."' ORDER BY name");
                            if(is_array($areas)){
                        ?>
                        <tr>
                            <td class="on-description" width="50%">
                                Area:
                            </td>
                            <td width='50%'>
                                <select class='on-field' method='post' name='area'>
                                    <option value='none'>-- No Area-- </option>";
                                        <?php if (is_array($areas))
                                            foreach ($areas as $area)
                                                if ($invoice_info[0][8] == $area[0])
                                                    echo "<option value='".$area[0]."' selected>".$area[1]."</option>";
                                                else
                                                    echo "<option value='".$area[0]."'>".$area[1]."</option>";
                                                ?>
                                </select>
                            </td>
                        </tr>
                        <?php } //END if ?>
                        <tr>
                            <td class="on-description" width="50%">
                                Invoice Name:
                            </td>
                            <td width="50%">
                                <input class="on-field" name="invoice_name" tabindex="2" type="text" value="<?php echo $invoice_info[0][2]; ?>"><div id="invoiceName"
                                    style="display: none;"><font class="on-validate-error">* Invoice name must be entered</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                Consulting <i>(<?php echo "".$_SESSION["currency"]; ?>)</i>:
                            </td>
                            <td width="50%">
                                <input class="on-field" name="consulting" style="text-align:right;" tabindex="3" type="text" value="<?php echo $invoice_info[0][3]; ?>">
                                <font class="on-description-left">* excl</font><div id="consultingDiv" style="display: none;"><font class="on-validate-error">* Entered amount must be numeric,
                                    eg. 123.45</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                Expenses <i>(<?php echo "".$_SESSION["currency"]; ?>)</i>:
                            </td>
                            <td width="50%">
                                <input class="on-field" name="expenses" style="text-align:right;" tabindex="4" type="text" onBlur="showExpense();" value="<?php echo $invoice_info[0][4]; ?>">
                                <font class="on-description-left">* excl</font><div id="expensesDiv" style="display: none;"><font class="on-validate-error">* Entered amount must be numeric,
                                    eg. 123.45</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                Diverse Income <i>(<?php echo "".$_SESSION["currency"]; ?>)</i>:
                            </td>
                            <td width="50%">
                                <input class="on-field" name="diverse_income" style="text-align:right;" tabindex="5" type="text" value="<?php echo $invoice_info[0][5]; ?>">
                                <font class="on-description-left">* excl</font>
                                <div id="diverseIncomeDiv" style="display: none;">
                                    <font class="on-validate-error">* Entered amount must be numeric, eg. 123.45</font>
                                </div>
                                <div id='invoiceDiv' style='display: none;'>
                                    <font class="on-validate-error">* Invoice cannot consist of only diverseincome</font>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                    VAT <i>(%)</i>:
                            </td>
                            <td width="50%">
                                <input class="on-field-date" name="VAT" style="text-align:right;" tabindex="6" type="text" value="<?php echo $invoice_info[0][6]; ?>"><div id="vatDiv"
                                    style="display: none;"><font class="on-validate-error">* Entered amount must be a percentage, eg. 100%</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                    Due Date:
                            </td>
                            <td width="50%">
                                <input class="on-field-date" id="due_date" name="due_date" tabindex="7" type="text" style="text-align:right;" onBlur="showExpense();" value="<?php echo $invoice_info[0][7]; ?>">
                                <div id="dueDate" style="display: none;"><font class="on-validate-error">* Due date not valid, eg. <?php echo "".$today; ?></font></div>
                            </td>
                        </tr>
<!--
                        <tr>
                            <td width="50%"></td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                Load Expense Dynamically:
                            </td>
                            <td class="on-description-left" width="50%">
                                &nbsp;&nbsp;<input  name="loadExpense" tabindex="9" type="checkbox" value="1" onChange="showExpense();" onClick="showExpense();">
                            </td>
                        </tr>
                        <tr>
                            <td width="50%"></td>
                        </tr>
                    </table>
                    <div id="expenseDiv" style="display: none;">
                        <table width="100%">
                            <tr>
                                <td class="centerdata">
                                    <br/><h6>
                                        Load Expense
                                    </h6>
                                <br/>
                                </td>
                            </tr>
                        </table>
                        <table cellpadding="0" cellspacing="2" width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Expense Description:
                                </td>
                                <td width="50%">
                                    <input class="on-field" name="expDescr" readonly type="text" value="">
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Expense Amount:
                                </td>
                                <td width="50%">
                                    <input class="on-field" name="expAmount" readonly type="text" value="">
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Expense Due Date:
                                </td>
                                <td width="50%">
                                    <input class="on-field" id="expDueDate" name="expDueDate" type="text" tabindex="10" value="">
                                </td>
                            </tr>
                        </table>
                    </div>
-->
                    </table>
                    <br/>
                    <input name="btnAdd" onClick="check();" tabindex="8" type="button" value="Update Invoice">
                    <input method="post" name="save" type="hidden" value="0" />
                </form>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
