<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

	if (!hasAccess("PROJ_MANAGE"))
		header("Location: noaccess.php");

    //  Get Project Information
    function getInfo()
    {
        $project_name                                                   = addslashes(strip_tags($_POST["project_name"]));
        $registration_date                                              = addslashes(strip_tags($_POST["registration_date"]));
        $submitted_by                                                   = $_POST["submitted_by"];
        $project_manager                                                = $_POST["project_manager"];
        $project_manager_tariff                                         = $_POST["project_manager_tariff"];

	if ($project_manager_tariff == 0)
	    $project_manager_tariff					= 1;

        $project_type                                                   = $_POST["project_type"];
        $total_budget                                                   = addslashes(strip_tags($_POST["total_budget"]));

        if ($total_budget != "")
            $total_budget                                               = number_format($total_budget, 2, ".", "");

        $consulting_budget                                              = addslashes(strip_tags($_POST["consulting_budget"]));

        if ($consulting_budget != "")
            $consulting_budget                                          = number_format($consulting_budget, 2, ".", "");

        $expense_budget                                                 = addslashes(strip_tags($_POST["expense_budget"]));

        if ($expense_budget != "")
            $expense_budget                                             = number_format($expense_budget, 2, ".", "");

        $diverse_income_budget                                          = addslashes(strip_tags($_POST["diverse_income_budget"]));

        if ($diverse_income_budget != "")
            $diverse_income_budget                                      = number_format($diverse_income_budget, 2, ".", "");

        $vat                                                            = addslashes(strip_tags($_POST["vat"]));

        if ($vat == "")
            $vat                                                        = 0;

        $buffer                                                         = addslashes(strip_tags($_POST["buffer"]));

        if ($buffer == "")
            $buffer                                                     = 0;

        $due_date                                                       = addslashes(strip_tags($_POST["due_date"]));
        $status                                                         = $_POST["status"];

        return "$project_name, $registration_date, $submitted_by, $project_manager, $project_manager_tariff, $project_type, $total_budget, ".
            "$consulting_budget, $expense_budget, $diverse_income_budget, $vat, $buffer, $due_date, $status";
    }

    //  Project Manager Selected
    if (isset($_POST["project_manager"]))
    {
        //  Temporary Array for Project Info
        $projectInfo                                                    = preg_split("/, /", getInfo());

        //  Set Project Manager Rate To Rate 1
        $projectInfo[4]                                                 = 1;

        $_SESSION["projectInfo"]                                        = $projectInfo;
    }

    //  Create Project Team Button Pressed
    if (isset($_POST["btnCreate"]))
    {
        //  Temporary Array for Project Info
        $projectInfo                                                    = split(", ", getInfo());

        $_SESSION["projectInfo"]                                        = $projectInfo;

        $project_id                                                     = q("SELECT MAX(id) FROM Project");
        $project_id++;

        if (!isset($_SESSION["project_id"]))
            $_SESSION["project_id"]                                     = $project_id;

        if (!isset($_SESSION["location"]))
            $_SESSION["location"]                                       = "Location: project_add.php";

        if ($projectInfo[3] != "null")
            header("Location: project_team.php");
        else
            $errorMessage                                               = "Before Creating Team, Please Select Project Manager";
    }

    //  Insert Project Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")
    {
        $errorMessage                                                   = "";

        //  Get Information
        $projectInfo                                                    = split(", ", getInfo());

        if (!isset($_SESSION["project_id"]))
        {
            $project_id                                                 = q("SELECT MAX(id) FROM Project");
            $project_id++;
            $_SESSION["project_id"]                                     = $project_id;
        }

        //  Check If Project Exists In Database
        $exist                                                          = q("SELECT id FROM Project WHERE name = '$projectInfo[0]' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."'");

        if (submitDateCheck($projectInfo[1]))   {
        if (!$exist)
        {
			$d_ = date("Y-m-d H:i:s");
            $insert                                                     = q("INSERT INTO Project (name, reg_date, submitted_by_id, manager_id, type_id, ".
                                                                            "due_date, company_id, completed, status, dateCreated, createdBy, dateUpdated, updatedBy) ".
                                                                            "VALUES ('$projectInfo[0]', '$projectInfo[1]', '$projectInfo[2]', '$projectInfo[3]', ".
                                                                            "'$projectInfo[5]', '$projectInfo[12]', ".
                                                                            "'".$_SESSION["company_id"]."', '0','$projectInfo[13]','$d_','".$_SESSION["email"]."','$d_','".$_SESSION["email"]."')");
			$pid_ 														= q("SELECT id FROM Project WHERE name = '".$projectInfo[0]."' AND company_id = '".$_SESSION["company_id"]."'");
            $insert2                                                    = q("INSERT INTO companiesOnTeam (project_id, status, teamManage, total_budget, ".
                                                                            "consulting, expenses, diverse_income, vat, buffer, company_id, dateCreated, createdBy, dateUpdated, updatedBy) ".
                                                                            "VALUES ('$pid_', '1', '1', ".
                                                                            "'$projectInfo[6]', '$projectInfo[7]', '$projectInfo[8]', ".
                                                                            "'$projectInfo[9]', '$projectInfo[10]', '$projectInfo[11]',".
                                                                            "'".$_SESSION["company_id"]."','$d_','".$_SESSION["email"]."','$d_','".$_SESSION["email"]."')");

            if ($insert)
            {
                //  Insert/Update Project Manager Details to Team
                if ($projectInfo[3] != "null")
                {
                    $employee_info                                      = q("SELECT frstname, lstname FROM Employee WHERE id = '$projectInfo[3]'");
                    $budget                                             = addslashes(strip_tags($_POST["project_manager_budget"]));

                    if ($budget == "Assign Budget" || $budget == "" || $budget == "0" || $budget == "0.00")
                        $budget                                         = null;

                    $exist                                              = q("SELECT id FROM Project_User WHERE project_id = '".$_SESSION["project_id"]."' ".
                                                                            "AND manager = '1'");

                    if (!$exist)
                    {
                        $insert                                         = q("INSERT INTO Project_User (user_id, user_tariff, user_budget, project_id, manager) ".
                                                                            "VALUES ('$projectInfo[3]', '$projectInfo[4]', '".number_format($budget, 2, ".", "")."', ".
                                                                            "'".$_SESSION["project_id"]."', '1')");

                        if ($insert)
                        {
                            $time                                       = date("H:i:s");

                            $logs                                       = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('".$employee_info[0][0]." ".$employee_info[0][1]." added to project team', ".
                                                                            "'Insert', 'Project_User', '".$_SESSION["email"]."', '$today', '$time', ".
                                                                            "'".$_SESSION["company_id"]."')");
                        }
                    }
                    else
                    {
                        $update                                         = q("UPDATE Project_User SET user_id = '$projectInfo[3]', user_tariff = '$projectInfo[4]', ".
                                                                            "user_budget = '".number_format($budget, 2, ".", "")."' ".
                                                                            "WHERE project_id = '".$_SESSION["project_id"]."' AND manager = '1'");

                        if ($update)
                        {
                            $time                                       = date("H:i:s");

                            $logs                                       = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('".$employee_info[0][0]." ".$employee_info[0][1]." information changed at team', ".
                                                                            "'Update', 'Project_User', '".$_SESSION["email"]."', '$today', '$time', ".
                                                                            "'".$_SESSION["company_id"]."')");
                        }
                    }
                }
                //  If There Is A Manager - Remove
                else
                {
                    $delete                                             = q("DELETE FROM Project_User WHERE project_id = '".$_SESSION["project_id"]."' AND manager = '1'");

                    if ($delete)
                    {
                        $time                                           = date("H:i:s");

                        $logs                                           = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('Project manager removed', 'Delete', 'Project_User', '".$_SESSION["email"]."', ".
                                                                            "'$today', '$time', '".$_SESSION["company_id"]."')");
                    }
                }

                $members                                                = q("SELECT e.id, e.lstname, e.frstname FROM (Employee as e INNER JOIN Project_User as pu ".
                                                                            "ON e.id = pu.user_id) WHERE pu.project_id = '".$_SESSION["project_id"]."' ".
                                                                            "AND pu.manager = '0' ORDER BY lstname, frstname");

                if (is_array($members)) {
                    foreach ($members as $member) {
                        //  Check If Member Is Project Manager
                        $is_manager                                     = q("SELECT * FROM Project_User WHERE project_id = '".$_SESSION["project_id"]."' ".
                                                                            "AND user_id = '$member[0]' AND manager = '1'");

                        if (!is_array($is_manager)) {
                            $budget                                     = addslashes(strip_tags($_POST["team_member_budget".$member[0]]));

                            if ($budget == "Assign Budget" || $budget == "" || $budget == "0" || $budget == "0.00")
                                $budget                                 = null;
                            else
                                $budget                                 = number_format($budget, 2, ".", "");

                            $update                                     = q("UPDATE Project_User SET user_budget = '$budget' WHERE user_id = '$member[0]' ".
                                                                            "AND project_id = '".$_SESSION["project_id"]."'");
                        }
                        else
                            $delete                                     = q("DELETE FROM Project_User WHERE project_id = '".$_SESSION["project_id"]."' ".
                                                                            "AND user_id = '$member[0]' AND manager = '0'");
                    }
                }

                $time                                                   = date("H:i:s");

                $logs                                                   = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('".$projectInfo[0]." created', 'Insert', 'Project', '".$_SESSION["email"]."', ".
                                                                            "'$today', '$time', '".$_SESSION["company_id"]."')");

                header("Location: projects.php");
            }
        }
        else
            $errorMessage                                               = "Project Already Exists";
        }
    }

    if ($errorMessage != "")
    {
        if ($errorMessage == "Please note that we are currently busy with data management, no entries are allowed for 2010-08-26. Please check back in 24 Hours.")
            echo "<p align='center' style='padding:0px;'><strong style='font-size:12px;'><font class='on-validate-error'>$errorMessage</font></strong></p>";
        else
            echo "<p align='center' style='padding:0px;'><strong><font color='#999999'>$errorMessage</font></strong></p>";
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0", "projects");
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    jQuery(function(){
	jQuery('#registration_date').DatePicker({
		format:'Y-m-d',
		date: jQuery('#registration_date').val(),
		current: jQuery('#registration_date').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#registration_date').val()))
			_date = new Date();
			else _date = jQuery('#registration_date').val();
			jQuery('#registration_date').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#registration_date').val(formated);
			jQuery('#registration_date').DatePickerHide();
		}
	});
    })

    jQuery(function(){
	jQuery('#due_date').DatePicker({
		format:'Y-m-d',
		date: jQuery('#due_date').val(),
		current: jQuery('#due_date').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#due_date').val()))
			_date = new Date();
			else _date = jQuery('#due_date').val();
			jQuery('#registration_date').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#due_date').val(formated);
			jQuery('#due_date').DatePickerHide();
		}
	});
    })

    var valid                                                           = 1;

    function check()
    {
        //  Check That Project Name Is Entered
        if (document.forms["project_add"].project_name.value == "")
        {
            ShowLayer("projectName", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("projectName", "none");

        // Check That Registration Date Is Entered
        if (document.forms["project_add"].registration_date.value == "")
        {
            ShowLayer("registrationDate", "block");
            valid                                                       = 0;
        }
        //  Check That Entered Registration Date Is Valid
        else
        {
            ShowLayer("registrationDate", "none");

            if (!validation("date", document.forms["project_add"].registration_date.value))
            {
                ShowLayer("registrationDateDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("registrationDateDiv", "none");
        }

        //  Check That Total Budget Is Valid, If Entered
        if (document.forms["project_add"].total_budget.value != "")
        {
            if (!validation("currency", document.forms["project_add"].total_budget.value))
            {
                ShowLayer("totalBudget", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("totalBudget", "none");
        }
        else
            ShowLayer("totalBudget", "none");

        //  Check That Consulting Budget Is Valid, If Entered
        if (document.forms["project_add"].consulting_budget.value != "")
        {
            if (!validation("currency", document.forms["project_add"].consulting_budget.value))
            {
                ShowLayer("consultingBudget", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("consultingBudget", "none");
        }
        else
            ShowLayer("consultingBudget", "none");

        //  Check That Expense Budget Is Valid, If Entered
        if (document.forms["project_add"].expense_budget.value != "")
        {
            if (!validation("currency", document.forms["project_add"].expense_budget.value))
            {
                ShowLayer("expenseBudget", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("expenseBudget", "none");
        }
        else
            ShowLayer("expenseBudget", "none");

        //  Check That Diverse Income Budget Is Valid, If Entered
        if (document.forms["project_add"].diverse_income_budget.value != "")
        {
            if (!validation("currency", document.forms["project_add"].diverse_income_budget.value))
            {
                ShowLayer("diverseIncomeBudget", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("diverseIncomeBudget", "none");
        }
        else
            ShowLayer("diverseIncomeBudget", "none");

        //  Check That Vat Is Valid, If Entered
        if (document.forms["project_add"].vat.value != "")
        {
            if (!validation("percentage", document.forms["project_add"].vat.value))
            {
                ShowLayer("VAT", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("VAT", "none");
        }
        else
            ShowLayer("VAT", "none");

        //  Check That Buffer Is Valid, If Entered
        if (document.forms["project_add"].buffer.value != "")
        {
            if (!validation("percentage", document.forms["project_add"].buffer.value))
            {
                ShowLayer("BUFFER", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("BUFFER", "none");
        }
        else
            ShowLayer("BUFFER", "none");

        //  Check That Due Date Is Valid, If Entered
        if (document.forms["project_add"].due_date.value != "")
        {
            if (!validation("date", document.forms["project_add"].due_date.value))
            {
                ShowLayer("dueDateDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("dueDateDiv", "none");
        }
        else
            ShowLayer("dueDateDiv", "none");

        if (valid == 1)
        {
            document.forms["project_add"].save.value                    = 1;
            document.forms["project_add"].submit();
        }
    }

    function clearBox(box)
    {
        box.value                                                       = "";
    }

    function test_budget(box, div)
    {
        //  Test Entered Value
        if (box.value != "")
        {
            if (!validation("currency", box.value))
            {
                ShowLayer(div, "block");
                valid                                                   = 0;
            }
            else
            {
                ShowLayer(div, "none");
                valid                                                   = 1;
            }
        }
        else
        {
            box.value                                                   = "Assign Budget";
            ShowLayer(div, "none");
        }
    }

    function projectStatus(type)
    {
        if (type == "proposal")
        {
            ShowLayer("viewActive1", "none");
            ShowLayer("viewActive2", "none");
        }
        else
        {
            ShowLayer("viewActive1", "block");
            ShowLayer("viewActive2", "block");
        }
    }
</script>
<?php
    //  nEmployees                                                      = Number of Employees
    $nEmployees                                                         = q("SELECT COUNT(e.id) FROM (Employee AS e INNER JOIN Company_Users ".
                                                                            "AS cu ON e.id = cu.user_id) WHERE cu.company_id = '".$_SESSION["company_id"]."' ".
                                                                            "AND e.email != 'admin' AND e.deleted = '0'");
    $employees                                                          = q("SELECT e.id, e.lstname, e.frstname FROM (Employee AS e INNER JOIN Company_Users ".
                                                                            "AS cu ON e.id = cu.user_id) WHERE cu.company_id = '".$_SESSION["company_id"]."' ".
                                                                            "AND e.email != 'admin' AND e.deleted = '0' ORDER BY e.lstname, frstname");

    //  nManagers                                                       = Number of Managers & Project Managers
    $nManagers                                                          = q("SELECT COUNT(DISTINCT(e.id)) FROM (((Employee AS e INNER JOIN user_role AS ur ON ur.userid = e.id) ".
                                                                            "INNER JOIN role_action AS ra ON ur.roleid = ra.roleid) INNER JOIN actions AS a ON a.id = ra.actionid) ".
                                                                            "WHERE ur.companyid = '".$_SESSION["company_id"]."' AND a.action = 'PROJ_MANAGE' ".
                                                                            "AND e.email != 'admin' AND e.deleted = '0'");
    $managers                                                           = q("SELECT DISTINCT(e.id), e.lstname, e.frstname FROM (((Employee AS e INNER JOIN user_role AS ur ON ur.userid = e.id) ".
                                                                            "INNER JOIN role_action AS ra ON ur.roleid = ra.roleid) INNER JOIN actions AS a ON a.id = ra.actionid) ".
                                                                            "WHERE ur.companyid = '".$_SESSION["company_id"]."' AND a.action = 'PROJ_MANAGE' ".
                                                                            "AND e.email != 'admin' AND e.deleted = '0'");


    //  nProjectTypes                                                   = Number of Project Types
    $nProjectTypes                                                      = q("SELECT COUNT(id) FROM ProjectTypes WHERE company_id = '".$_SESSION["company_id"]."'");
    $projectTypes                                                       = q("SELECT id, type FROM ProjectTypes WHERE company_id = '".$_SESSION["company_id"]."' ".
                                                                            "ORDER BY type");

    //  Check If Project Team Exists
    $nMembers                                                           = q("SELECT COUNT(e.id) FROM (Employee as e INNER JOIN Project_User as pu ON e.id = pu.user_id) ".
                                                                            "WHERE pu.project_id = '".$_SESSION["project_id"]."' AND pu.manager = '0' ".
                                                                            "AND e.deleted = '0'");
    $members                                                            = q("SELECT e.id, e.lstname, e.frstname FROM (Employee as e INNER JOIN Project_User as pu ".
                                                                            "ON e.id = pu.user_id) WHERE pu.project_id = '".$_SESSION["project_id"]."' ".
                                                                            "AND pu.manager = '0' AND e.deleted = '0' ORDER BY lstname, frstname");

    //  Get Project Information
    if (isset($_SESSION["projectInfo"]))
        $projectInfo                                                    = $_SESSION["projectInfo"];
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="project_add">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>
                                    Add New Project
                                </h6>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">Proposal State
                                <input method="post" name="status" onClick="projectStatus('proposal');" tabindex="1" type="radio" value="proposal" <?php
                                    if ($projectInfo[13] == "proposal" || $projectInfo[13] == "") echo "checked"; ?>>&nbsp;
                            </td>
                            <td class="on-description-left" width="50%">
                                <input method="post" name="status" onClick="projectStatus('active');" tabindex="1" type="radio" value="active" <?php
                                    if ($projectInfo[13] == "active") echo "checked"; ?>>
                                    Active State
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                    Project Name:
                            </td>
                            <td width="50%">
                                <input class="on-field" name="project_name" tabindex="1" size="50" type="text" value="<?php echo "".stripslashes($projectInfo[0]); ?>">
                                <div id="projectName" style="display: none;"><font class="on-validate-error">* Project name must be entered</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                    Registration Date:
                            </td>
                            <td width="50%">
                                <input class="on-field-date" id="registration_date" name="registration_date" tabindex="2" type="text" style="text-align:right;" value="<?php if (isset($_SESSION["projectInfo"]))
                                    echo "".stripslashes($projectInfo[1]); else echo "".$today; ?>">
                                <div id="registrationDate" style="display: none;"><font class="on-validate-error">* Registration date must be entered</font></div>
                                <div id="registrationDateDiv" style="display: none;"><font class="on-validate-error">* Registration date not valid, eg.
                                    <?php echo "".date("Y-m-d"); ?></font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                    Plan Submitted By:
                            </td>
                            <td width="50%">
                                <select class="on-field" method="post" name="submitted_by" tabindex="3">
                                    <option value="null">--  Select Employee --&nbsp;&nbsp;</option>
                                    <?php
                                        if ($nEmployees > 1)
                                        {
                                            foreach ($employees as $employee)
                                                if ($projectInfo[2] == $employee[0])
                                                    echo "<option value='".$employee[0]."' selected>".$employee[1].", ".$employee[2]."&nbsp;&nbsp;</option>";
                                                else
                                                    echo "<option value='".$employee[0]."'>".$employee[1].", ".$employee[2]."&nbsp;&nbsp;</option>";
                                        }
                                        else if ($nEmployees == 1)
                                            if ($projectInfo[2] == $employees[0][0])
                                                echo "<option value='".$employees[0][0]."' selected>".$employees[0][1].", ".$employees[0][2]."&nbsp;&nbsp;</option>";
                                            else
                                                echo "<option value='".$employees[0][0]."'>".$employees[0][1].", ".$employees[0][2]."&nbsp;&nbsp;</option>";
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                    Project Manager:
                            </td>
                            <td width="50%">
                                <?php
                                    echo "<select class='on-field' method='post' name='project_manager' onChange='submit();' tabindex='4'>";
                                        echo "<option value='null'>--  Select Project Manager --&nbsp;&nbsp;</option>";
                                        if ($nManagers > 1)
                                        {
                                            foreach ($managers as $manager)
                                                if ($projectInfo[3] == $manager[0])
                                                    echo "<option value='".$manager[0]."' selected>".$manager[1].", ".$manager[2]."&nbsp;&nbsp;</option>";
                                                else
                                                    echo "<option value='".$manager[0]."'>".$manager[1].", ".$manager[2]."&nbsp;&nbsp;</option>";
                                        }
                                        else if ($nManagers == 1)
                                            if ($projectInfo[3] == $managers[0][0])
                                                echo "<option value='".$managers[0][0]."' selected>".$managers[0][1].", ".$managers[0][2]."&nbsp;&nbsp;</option>";
                                            else
                                                echo "<option value='".$managers[0][0]."'>".$managers[0][1].", ".$managers[0][2]."&nbsp;&nbsp;</option>";
                                    echo "</select>";
                                ?>
                            </td>
                        </tr>
                        <?php
                            if ($projectInfo[3] != "null")
                            {
                                $tariffs                                = q("SELECT tariff1, tariff2, tariff3, tariff4, tariff5 FROM Employee ".
                                                                            "WHERE id = '".$projectInfo[3]."'");

                                echo "<tr>
                                            <td class='on-description' width='50%'>
                                                Project Manager Tariff:
                                            </td>
                                            <td width='50%'>
                                                <select class='on-field' method='post' name='project_manager_tariff' tabindex='5'>";
                                            if ($tariffs[0][0] > 0)
                                                if ($projectInfo[4] == 1)
                                                    echo "<option value='1' selected>".$tariffs[0][0]."&nbsp;&nbsp;</option>";
                                                else
                                                    echo "<option value='1'>".$tariffs[0][0]."&nbsp;&nbsp;</option>";

                                            if ($tariffs[0][1] > 0)
                                                if ($projectInfo[4] == 2)
                                                    echo "<option value='2' selected>".$tariffs[0][1]."&nbsp;&nbsp;</option>";
                                                else
                                                    echo "<option value='2'>".$tariffs[0][1]."&nbsp;&nbsp;</option>";

                                            if ($tariffs[0][2] > 0)
                                                if ($projectInfo[4] == 3)
                                                    echo "<option value='3' selected>".$tariffs[0][2]."&nbsp;&nbsp;</option>";
                                                else
                                                    echo "<option value='3'>".$tariffs[0][2]."&nbsp;&nbsp;</option>";

                                            if ($tariffs[0][3] > 0)
                                                if ($projectInfo[4] == 4)
                                                    echo "<option value='4' selected>".$tariffs[0][3]."&nbsp;&nbsp;</option>";
                                                else
                                                    echo "<option value='4'>".$tariffs[0][3]."&nbsp;&nbsp;</option>";

                                            if ($tariffs[0][4] > 0)
                                                if ($projectInfo[4] == 5)
                                                    echo "<option value='5' selected>".$tariffs[0][4]."&nbsp;&nbsp;</option>";
                                                else
                                                    echo "<option value='5'>".$tariffs[0][4]."&nbsp;&nbsp;</option>";
                                        echo "</select>";
                                    echo "</td>";
                                echo "</tr>";
                            }
                        ?>
                        <tr>
                            <td class="on-description" width="50%">
                                    Project Type:
                            </td>
                            <td width="50%">
                                <?php
                                    echo "<select class='on-field' method='post' name='project_type' tabindex='6'>";
                                        echo "<option value='null'>--  Select Project Type --&nbsp;&nbsp;</option>";
                                        if ($nProjectTypes > 1)
                                        {
                                            foreach ($projectTypes as $projectType)
                                                if ($projectInfo[5] == $projectType[0])
                                                    echo "<option value='".$projectType[0]."' selected>".$projectType[1]."&nbsp;&nbsp;</option>";
                                                else
                                                    echo "<option value='".$projectType[0]."'>".$projectType[1]."&nbsp;&nbsp;</option>";
                                        }
                                        else if ($nProjectTypes == 1)
                                            if ($projectInfo[5] == $projectTypes[0][0])
                                                echo "<option value='".$projectTypes[0][0]."' selected>".$projectTypes[0][1]."&nbsp;&nbsp;</option>";
                                            else
                                                echo "<option value='".$projectTypes[0][0]."'>".$projectTypes[0][1]."&nbsp;&nbsp;</option>";
                                    echo "</select>";
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="centerdata" colspan="2">
                                <input name="btnCreate" type="submit" value="Create/Update Project Team">
                            </td>
                        </tr>
                    </table>
                    <div id="viewActive1" style="<?php if ($projectInfo[13] == "proposal" || $projectInfo[13] == "") echo "display: none;";
                        else echo "display: block;";?>">
                        <table cellpadding="0" cellspacing="2" width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                        Total Project Budget <i>(R)</i>:&nbsp;
                                </td>
                                <td width="50%">
                                    <input class="on-field" name="total_budget" style="text-align:right;" tabindex="7" type="text" value="<?php
                                        echo "".stripslashes($projectInfo[6]); ?>"><font class="on-description-left">* excl</font><div id="totalBudget" style="display: none;">
                                        <font class="on-validate-error">* Entered amount must be numeric, eg. 123.45</font></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Consulting Budget <i>(R)</i>:&nbsp;
                                </td>
                                <td width="50%">
                                    <input class="on-field" name="consulting_budget" style="text-align:right;" tabindex="8" type="text" value="<?php
                                        echo "".stripslashes($projectInfo[7]); ?>"><font class="on-description-left">* excl</font><div id="consultingBudget" style="display: none;">
                                        <font class="on-validate-error">* Entered amount must be numeric, eg. 123.45</font></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Expense Budget <i>(R)</i>:&nbsp;
                                </td>
                                <td width="50%">
                                    <input class="on-field" name="expense_budget" style="text-align:right;" tabindex="9" type="text" value="<?php
                                        echo "".stripslashes($projectInfo[8]); ?>"><font class="on-description-left">* excl</font><div id="expenseBudget" style="display: none;">
                                        <font class="on-validate-error">* Entered amount must be numeric, eg. 123.45</font></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Diverse Income Budget <i>(R)</i>:&nbsp;
                                </td>
                                <td width="50%">
                                    <input class="on-field" name="diverse_income_budget" style="text-align:right;" tabindex="10" type="text" value="<?php
                                        echo "".stripslashes($projectInfo[9]); ?>"><font class="on-description-left">* excl</font><div id="diverseIncomeBudget" style="display: none;">
                                        <font class="on-validate-error">* Entered amount must be numeric, eg. 123.45</font></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                        VAT <i>(%)</i>:&nbsp;
                                </td>
                                <td width="50%">
                                    <input class="on-field-date" name="vat" style="text-align:right;" tabindex="11" type="text" value="<?php echo "".stripslashes($projectInfo[10]); ?>">
                                    <div id="VAT" style="display: none;"><font class="on-validate-error">* Entered amount must be a percentage, eg. 100%</font></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Buffer <i>(%)</i>:&nbsp;
                                </td>
                                <td width="50%">
                                    <input class="on-field-date" name="buffer" style="text-align:right;" tabindex="12" type="text" value="<?php
                                        echo "".stripslashes($projectInfo[11]); ?>"><div id="BUFFER" style="display: none;"><font class="on-validate-error">* Entered amount must
                                        be a percentage, eg. 100%</font></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <?php
                        if ((isset($_SESSION["projectInfo"]) && $projectInfo[3] != "null") || $nMembers > 0)
                        {
                            echo "<table class='on-table-center on-table' width='50%'>";
                                echo "<tr>";
                                    echo "<th colspan='100%'>";
                                        echo "<br/>";
                                            echo "Project Team Budget Assignment";
                                    echo "</th>";
                                echo "</tr>";

                                //  Project Team - Manager
                                if ($projectInfo[3] != "null")
                                {
                                    $project_manager_info               = q("SELECT lstname, frstname FROM Employee WHERE id = '$projectInfo[3]'");

                                    echo "<tr>";
                                        echo "<td class='rightdata' width='50%'>";
                                            echo "".$project_manager_info[0][0].", ".$project_manager_info[0][1]." <i>(PM)</i>&nbsp;";
                                        echo "</td>";
                                        echo "<td width='50%'>";
                                            echo "<input name='project_manager_budget' onBlur=\"test_budget(project_manager_budget, 'projectManagerBudget');\" ".
                                            "tabindex='13' onFocus=\"clearBox(project_manager_budget);\" style='text-align:right;' type='text' value='Assign Budget'>".
                                            "<div id='projectManagerBudget' style='display: none;'><font class='on-validate-error'>* Entered amount must be numeric, eg. 123.45".
                                            "</font></div>";
                                        echo "</td>";
                                    echo "</tr>";
                                }

                                //  Project Team - Team
                                if ($nMembers > 1)
                                {
                                    foreach ($members as $member)
                                    {
                                        echo "<tr>";
                                            echo "<td class='rightdata' width='50%'>";
                                                echo "".$member[1].", ".$member[2]."&nbsp;";
                                            echo "</td>";
                                            echo "<td width='50%'>";
                                                echo "<input name='team_member_budget".$member[0]."' tabindex='14' ".
                                                    "onBlur=\"test_budget(team_member_budget".$member[0].", 'teamMemberBudget".$member[0]."');\" ".
                                                    "onFocus=\"clearBox(team_member_budget".$member[0].");\" style='text-align:right;' type='text' ".
                                                    "value='Assign Budget'><div id='teamMemberBudget".$member[0]."' style='display: none;'>".
                                                    "<font class='on-validate-error'>* Entered amount must be numeric, eg. 123.45</font></div>";
                                            echo "</td>";
                                        echo "</tr>";
                                    }
                                }
                                else if ($nMembers == 1)
                                {
                                    echo "<tr>";
                                        echo "<td class='rightdata' width='50%'>";
                                            echo "".$members[0][1].", ".$members[0][2]."&nbsp;";
                                        echo "</td>";
                                        echo "<td width='50%'>";
                                            echo "<input name='team_member_budget".$members[0][0]."' tabindex='14' ".
                                                "onBlur=\"test_budget(team_member_budget".$members[0][0].", 'teamMemberBudget".$members[0][0]."');\" ".
                                                "onFocus=\"clearBox(team_member_budget".$members[0][0].");\" style='text-align:right;' type='text' ".
                                                "value='Assign Budget'><div id='teamMemberBudget".$members[0][0]."' style='display: none;'>".
                                                "<font class='on-validate-error'>* Entered amount must be numeric, eg. 123.45</font></div>";
                                        echo "</td>";
                                    echo "</tr>";
                                }
                            echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot></table>";
                        }
                    ?>
                    <div id="viewActive2" style="<?php if ($projectInfo[13] == "proposal" || $projectInfo[13] == "") echo "display: none;";
                        else echo "display: block;";?>">
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Due Date:&nbsp;
                                </td>
                                <td width="50%">
                                    <input class="on-field-date" id="due_date" name="due_date" tabindex="15" type="text" style="text-align:right;" value="<?php
                                        echo "".stripslashes($projectInfo[12]); ?>">
                                    <div id="dueDateDiv" style="display: none;"><font class="on-validate-error">* Due date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <br/>
                    <input name="btnAdd" onClick="check();" tabindex="16" type="button" value="Add Project">
                    <input method="post" name="save" type="hidden" value="0" />
                </form>
            </td>
        </tr>
        <tr>
            <td class="centerdata">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
