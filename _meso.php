<?php
    include("_db.php");
    include("_dates.php");
?>
<html>
    <head>
        <title>
            On-Track - Recovery Script...
        </title>
    </head>
    <body>
        <h1 align="center">
            Install
        </h1>
        <?php
            $date = date("Y-m-d H:i:s");
            $user = "admin";

            $activityTypes[] = "Admin";
            $activityTypes[] = "Demonstration";
            $activityTypes[] = "Design";
            $activityTypes[] = "Documentaion";
            $activityTypes[] = "Installation";
            $activityTypes[] = "Management";
            $activityTypes[] = "Marketing";
            $activityTypes[] = "Project Management";
            $activityTypes[] = "Proposal";
            $activityTypes[] = "Reporting";
            $activityTypes[] = "Research";
            $activityTypes[] = "Sales";
            $activityTypes[] = "Support";
            $activityTypes[] = "Training";
            $activityTypes[] = "Vision";

            $activities["Admin"][] = "Documentation";
            $activities["Admin"][] = "Financial";
            $activities["Admin"][] = "Other";
            $activities["Admin"][] = "Personnel";
            $activities["Admin"][] = "Registration";

            $activities["Demonstration"][] = "Integrated Solution";

            $activities["Design"][] = "Database";
            $activities["Design"][] = "Network";
            $activities["Design"][] = "Process";
            $activities["Design"][] = "Server";
            $activities["Design"][] = "Software";
            $activities["Design"][] = "Solution";
            $activities["Design"][] = "Web";

            $activities["Documentaion"][] = "Management";
            $activities["Documentaion"][] = "Manual";
            $activities["Documentaion"][] = "MOU";
            $activities["Documentaion"][] = "Newsletter";
            $activities["Documentaion"][] = "SLA";

            $activities["Installation"][] = "Network";
            $activities["Installation"][] = "Preperation";

            $activities["Management"][] = "Financial";
            $activities["Management"][] = "General";
            $activities["Management"][] = "Meeting";
            $activities["Management"][] = "Personnel";

            $activities["Marketing"][] = "General";
            $activities["Marketing"][] = "Hardware";
            $activities["Marketing"][] = "Integrated Solution";
            $activities["Marketing"][] = "Support";
            $activities["Marketing"][] = "Website";

            $activities["Project Management"][] = "Budget Planning";
            $activities["Project Management"][] = "Correspondence";
            $activities["Project Management"][] = "Meeting";
            $activities["Project Management"][] = "Procurement";
            $activities["Project Management"][] = "Quality Control";
            $activities["Project Management"][] = "Resource Planning";
            $activities["Project Management"][] = "Scoping";
            $activities["Project Management"][] = "Strategy Planning";
            $activities["Project Management"][] = "Tasks";
            $activities["Project Management"][] = "Time Planning";
            $activities["Project Management"][] = "WBS/Gantt";

            $activities["Proposal"][] = "Integrated Solution";
            $activities["Proposal"][] = "Software";
            $activities["Proposal"][] = "Support";

            $activities["Reporting"][] = "Event";
            $activities["Reporting"][] = "Progress";
            $activities["Reporting"][] = "Technical";

            $activities["Research"][] = "Database";
            $activities["Research"][] = "Linux";
            $activities["Research"][] = "Networking";
            $activities["Research"][] = "Other";
            $activities["Research"][] = "Programming";
            $activities["Research"][] = "Software";

            $activities["Sales"][] = "Products";

            $activities["Support"][] = "Desktop";
            $activities["Support"][] = "E-Mail";
            $activities["Support"][] = "General";
            $activities["Support"][] = "Network";
            $activities["Support"][] = "Server";
            $activities["Support"][] = "Integrated Solutions";

            $activities["Training"][] = "Integrated Solutions";

            $activities["Vision"][] = "Mission, Vision & Values";
            $activities["Vision"][] = "Personnel Meeting";
            $activities["Vision"][] = "Preperation";
            $activities["Vision"][] = "Shareholding Meeting";

            $companyID = q("SELECT id FROM Company WHERE name = 'Meso-Group'");

            echo $companyID." - Meso-Group<br/><br/>";

            $inserts = 0;

            if (is_array($activityTypes))    {
                foreach ($activityTypes as $at)       {
                    //echo $at."<br/>";

                    /**/
                    if (!exist("ActivityTypes", "company_id = '".$companyID."' AND type = '".$at."'"))       {
                        $insert = q("INSERT INTO ActivityTypes (company_id,type,dateCreated,createdBy,dateUpdated,updatedBy) ".
                                    "VALUES ('".$companyID."','".$at."','".$date."','".$user."','".$date."','".$user."')");

                        $inserts++;
                    }
                    /**/

                    if (is_array($activities[$at]))    {
                        $parentID = q("SELECT id FROM ActivityTypes WHERE company_id = '".$companyID."' AND type = '".$at."'");

                        foreach ($activities[$at] as $a)       {
                            //echo "&nbsp;&nbsp;&nbsp;".$a."<br/>";

                            /**/
                            if (!exist("Activities", "parent_id = '".$parentID."' AND name = '".$a."'"))       {
                                $insert = q("INSERT INTO Activities (parent_id,name,active,dateCreated,createdBy,dateUpdated,updatedBy) ".
                                            "VALUES ('".$parentID."','".$a."','1','".$date."','".$user."','".$date."','".$user."')");

                                $inserts++;
                            }
                            /**/
                        }
                    }

                    //echo "<br/>";
                }
            }

            echo "INSERTS [".$inserts."]<br/>";

            echo "<p align='center'>Script completed successfully</p>";
        ?>
        <form action="index.php" method="post">
            <center><input type="submit" value="Return" /></center>
        </form>
    </body>
</html>
