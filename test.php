<?php
    session_start();

    include("_db.php");
?>
<?php
    $supportLayer = 'http://'; // Used to easily switch between HTTP and SSL Layers
    $supportURL = 'support.integrityengineering.co.za'; // Support domain name
    $supportSite = $supportLayer . $supportURL;

    $uidGen = md5(RAND(0,255)); // Needed to create unique session
    $randD = rand(0,99); // Random generator for link generator

    function source_code($url)  {
        if (function_exists ('curl_init'))      {
        global $source;
        $curl = @curl_init ($url);
        @curl_setopt ($curl, CURLOPT_HEADER, FALSE);
        @curl_setopt ($curl, CURLOPT_RETURNTRANSFER, TRUE);
        @curl_setopt ($curl, CURLOPT_FOLLOWLOCATION, TRUE);
        @curl_setopt ($curl, CURLOPT_BINARYTRANSFER, TRUE);
        @curl_setopt ($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        $source = @curl_exec ($curl);
        @curl_close ($curl);
        return $source;
        } else  {
        return @file_get_contents ($url);
        }
    }

    $haystack = source_code("$supportSite/visitor/index.php?/LiveChat/Chat/Request/_sessionID=$uidGen/_proactive=0/_filterDepartmentID=/_randomNumber=$randD/_fullName=/_email=/_promptType=chat");
echo "Haystack [$haystack]<br/>";
    $needle = strip_tags($haystack);

echo "Needle [$needle]<br/>";

    if (strpos($needle, 'Online')) {
        print "<a href=\"javascript: void(0);\" onclick=\"javascript: window.open('$supportSite/visitor/index.php?/LiveChat/Chat/Request/_sessionID=$uidGen/_proactive=0/_filterDepartmentID=/_randomNumber=$randD/_fullName=/_email=/_promptType=chat', 'livechatwin', 'toolbar=0,location=0,directories=0,status=1,menubar=0,scrollbars=0,resizable=1,width=600,height=680');\" class=\"livechatlink\">Live Chat Online</a>";
    }else{
        print "<a href=\"javascript: void(0);\" onclick=\"javascript: window.open('$supportSite/visitor/index.php?/LiveChat/Chat/Request/_sessionID=$uidGen/_proactive=0/_filterDepartmentID=/_randomNumber=$randD/_fullName=/_email=/_promptType=chat', 'livechatwin', 'toolbar=0,location=0,directories=0,status=1,menubar=0,scrollbars=0,resizable=1,width=600,height=680');\" class=\"livechatlink\">Live Chat Offline</a>";
    }
?>