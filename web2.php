<html>
    <body>
        <h1 align="center">
            Install
        </h1>
        <?php
        include("_db.php");

        $insert                                                         = q("INSERT INTO Notify (code, descr) ".
                                                                            "VALUES ('ExpenseMonthly', 'Monthly Expense Report/Employee')");

        function statusSaveMonthly($date)   {
            //  Check for Status Before Date
            $companies                                                  = q("SELECT id, name FROM Company ORDER BY id");

            if (is_array($companies))   {
                foreach ($companies as $company)    {
                    $balance                                            = 0;

                    $tempDate                                           = q("SELECT id, date FROM ExpenseStatus WHERE date <= '$date' AND ".
                                                                            "company_id = '".$company[0]."' ORDER BY date DESC");
                    $tempDate                                           = $tempDate[0][1];

                    //  Expense Balance @ Start Date
                    if ($tempDate != 0) {
                        $balance                                        = q("SELECT amount FROM ExpenseStatus WHERE date = '$tempDate' ".
                                                                            "AND company_id = '".$company[0]."'");
                        $invoiced_expenses                              = q("SELECT SUM(li.expenses + (li.expenses * (li.vat / 100))) FROM (LoadInvoices AS li ".
                                                                            "INNER JOIN Project AS p ON p.id = li.project_id) WHERE li.due_date >= '$tempDate' ".
                                                                            "AND li.due_date < '$date' AND p.company_id = '".$company[0]."'");
                        $budget_expenses                                = q("SELECT SUM(amount + (amount * (vat / 100))) FROM ExpenseBudget ".
                                                                            "WHERE due_date >= '$tempDate' AND due_date < '$date' ".
                                                                            "AND company_id = '".$company[0]."'");
                        $invoiced_diverse_income                        = q("SELECT SUM(li.diverse_income + (li.diverse_income * (li.vat / 100))) ".
                                                                            "FROM (LoadInvoices AS li INNER JOIN Project AS p ON p.id = li.project_id) ".
                                                                            "WHERE li.due_date >= '$tempDate' AND li.due_date < '$date' ".
                                                                            "AND p.company_id = '".$company[0]."'");
                        $balance                                        += ($invoiced_expenses - ($budget_expenses + $invoiced_diverse_income));
                    }
                    else    {
                        $invoiced_expenses                              = q("SELECT SUM(li.expenses + (li.expenses * (li.vat / 100))) FROM (LoadInvoices AS li ".
                                                                            "INNER JOIN Project AS p ON p.id = li.project_id) WHERE li.due_date < '$date' ".
                                                                            "AND p.company_id = '".$company[0]."'");
                        $budget_expenses                                = q("SELECT SUM(amount + (amount * (vat / 100))) FROM ExpenseBudget WHERE due_date < '$date' ".
                                                                            "AND company_id = '".$company[0]."'");
                        $invoiced_diverse_income                        = q("SELECT SUM(li.diverse_income + (li.diverse_income * (li.vat / 100))) ".
                                                                            "FROM (LoadInvoices AS li INNER JOIN Project AS p ON p.id = li.project_id) ".
                                                                            "WHERE li.due_date < '$date' AND p.company_id = '".$company[0]."'");
                        $balance                                        += ($invoiced_expenses - ($budget_expenses + $invoiced_diverse_income));
                    }

                    //  Save Status If Not Yet Inserted
                    $exists                                             = q("SELECT id FROM ExpenseStatus WHERE date = '$date' AND company_id = '$company[0]'");

                    if (!$exists && $balance != "0")
                        $insert                                         = q("INSERT INTO ExpenseStatus (date, amount, company_id) ".
                                                                            "VALUES ('$date', '".number_format($balance, 2, ".", "")."', '$company[0]')");
                }
            }

            return;
        }

        $dates = array("2008-07-01", "2008-08-01", "2008-09-01", "2008-10-01");

        foreach ($dates as $d1)
        //echo "".$d."<br/>";
            statusSaveMonthly($d1);

        $dates = array("2008-11-01", "2008-12-01", "2009-01-01", "2009-02-01");

        foreach ($dates as $d2)
        //echo "".$d."<br/>";
            statusSaveMonthly($d2);

        $dates = array("2009-03-01", "2009-04-01", "2009-05-01", "2009-06-01");

        foreach ($dates as $d3)
        //echo "".$d."<br/>";
            statusSaveMonthly($d3);

        $dates = array("2009-07-01", "2009-08-01", "2009-09-01", "2009-10-01");

        foreach ($dates as $d4)
        //echo "".$d."<br/>";
            statusSaveMonthly($d4);

        $dates = array("2009-11-01", "2009-12-01", "2010-01-01", "2010-02-01");

        foreach ($dates as $d5)
        //echo "".$d."<br/>";
            statusSaveMonthly($d5);

        $dates = array("2010-03-01", "2010-04-01", "2010-05-01", "2010-06-01");

        foreach ($dates as $d6)
        //echo "".$d."<br/>";
            statusSaveMonthly($d6);

        $dates = array("2010-07-01", "2010-08-01", "2010-09-01", "2010-10-01");

        foreach ($dates as $d7)
        //echo "".$d."<br/>";
            statusSaveMonthly($d7);

        ?>
        <form action="index.php" method="post">
            <center><input type="submit" value="Return"/></center>
        </form>
    </body>
</html>