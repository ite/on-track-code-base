<?php
    //  Get Day Of Specific Date
    function getDay($date) {
        return date("D", strtotime($date));
    }

    //  Get Week Of Specific Date
    function getWeek($date) {
        return date("W", strtotime($date));
    }

    //  Get Number of Days Between Two Dates
    function getDays($begin, $end)
    {
        $end                                                            = strtotime($end);
        $begin                                                          = strtotime($begin);

        $days                                                           = $end - $begin;
        $days                                                           = floor($days / (24 * 60 * 60)) + 1;
        return $days;
    }

    //  Get Number of Business Days Between Two Dates
    function businessdays($begin, $end)
    {
        $rbegin                                                         = is_string($begin) ? strtotime(strval($begin)) : $begin;
        $rend                                                           = is_string($end) ? strtotime(strval($end)) : $end;

        if ($rbegin < 0 || $rend < 0)
            return 0;

        $begin                                                          = workday($rbegin, TRUE);
        $end                                                            = workday($rend, FALSE);

        if ($end < $begin)
        {
            $end                                                        = $begin;
            $begin                                                      = $end;
        }

        $difftime                                                       = $end - $begin;
        $diffdays                                                       = floor($difftime / (24 * 60 * 60)) + 1;

        if ($diffdays < 7)
        {
            $abegin                                                     = getdate($rbegin);
            $aend                                                       = getdate($rend);

            if ($diffdays == 1 && ($astart["wday"] == 0 || $astart["wday"] == 6) && ($aend["wday"] == 0 || $aend["wday"] == 6))
                return 0;

            $abegin                                                     = getdate($begin);
            $aend                                                       = getdate($end);
            $weekends                                                   = ($aend["wday"] < $abegin["wday"]) ? 1 : 0;
        }
        else
            $weekends                                                   = floor($diffdays / 7);

        return $diffdays - ($weekends * 2);
    }

    function workday($date, $begindate = TRUE)
    {
        $adate                                                          = getdate($date);
        $day                                                            = 24 * 60 * 60;

        if ($adate["wday"] == 0) // Sunday
            $date += $begindate ? $day : -($day * 2);
        else if ($adate["wday"] == 6) // Saterday
            $date += $begindate ? $day * 2 : -$day;

        return $date;
    }

    //  Returns Number of Months Between Two Dates
    function getMonthPeriod($date1, $date2)
    {
        $year1                                                          = substr($date1, 0, strpos($date1, "-"));
        $year2                                                          = substr($date2, 0, strpos($date2, "-"));

        $month1                                                         = substr($date1, (strpos($date1, "-") + 1), 2);
        $month2                                                         = substr($date2, (strpos($date2, "-") + 1), 2);

        $months                                                         = ($month2 - $month1) + 1;

        if (($year2 - $year1) > 0)
            $months                                                     = $months + (12 * ($year2 - $year1));

        return $months;
    }

    //  Returns Week Start
    function getWeekStart($date)
    {
        $day                                                            = date("D", strtotime($date));

        if ($day == "Mon")
            $date                                                       = getDates($date, -0);
        else if ($day == "Tue")
            $date                                                       = getDates($date, -1);
        else if ($day == "Wed")
            $date                                                       = getDates($date, -2);
        else if ($day == "Thu")
            $date                                                       = getDates($date, -3);
        else if ($day == "Fri")
            $date                                                       = getDates($date, -4);
        else if ($day == "Sat")
            $date                                                       = getDates($date, -5);
        else 
            $date                                                       = getDates($date, -6);


        return "".$date;
    }

    //  Returns Month Start
    function getMonthStart($date)
    {
        $year                                                           = substr($date, 0, strpos($date, "-"));
        $month                                                          = substr($date, (strpos($date, "-") + 1), 2);

        return "".$year."-".$month."-01";
    }

    //  Returns Week End
    function getWeekEnd($date)
    {
        $day                                                            = date("D", strtotime($date));

        if ($day == "Mon")
            $date                                                       = getDates($date, 6);
        else if ($day == "Tue")
            $date                                                       = getDates($date, 5);
        else if ($day == "Wed")
            $date                                                       = getDates($date, 4);
        else if ($day == "Thu")
            $date                                                       = getDates($date, 3);
        else if ($day == "Fri")
            $date                                                       = getDates($date, 2);
        else if ($day == "Sat")
            $date                                                       = getDates($date, 1);
        else 
            $date                                                       = getDates($date, 0);


        return "".$date;
    }

    //  Returns Month End
    function getMonthEnd($date)
    {
        $year                                                           = substr($date, 0, strpos($date, "-"));
        $month                                                          = substr($date, (strpos($date, "-") + 1), 2);

        if ($month == "01")
            $last                                                       = "31";
        else if ($month == "02")
        {
            if ($year % 4 != 0)
                $last                                                   = "28";
            else if ($year % 400 == 0)
                $last                                                   = "29";
            else if ($year % 100 == 0)
                $last                                                   = "28";
            else
                $last                                                   = "29";
        }
        else if ($month == "03")
            $last                                                       = "31";
        else if ($month == "04")
            $last                                                       = "30";
        else if ($month == "05")
            $last                                                       = "31";
        else if ($month == "06")
            $last                                                       = "30";
        else if ($month == "07")
            $last                                                       = "31";
        else if ($month == "08")
            $last                                                       = "31";
        else if ($month == "09")
            $last                                                       = "30";
        else if ($month == "10")
            $last                                                       = "31";
        else if ($month == "11")
            $last                                                       = "30";
        else if ($month == "12")
            $last                                                       = "31";

        return "".$year."-".$month."-".$last;
    }

    //  Returns Month String
    function getMonthString($month)
    {
        //  Month Strings
        $mth                                                            = array();
        $mth[ 1]                                                        = "Jan";
        $mth[ 2]                                                        = "Feb";
        $mth[ 3]                                                        = "Mar";
        $mth[ 4]                                                        = "Apr";
        $mth[ 5]                                                        = "May";
        $mth[ 6]                                                        = "Jun";
        $mth[ 7]                                                        = "Jul";
        $mth[ 8]                                                        = "Aug";
        $mth[ 9]                                                        = "Sep";
        $mth[10]                                                        = "Oct";
        $mth[11]                                                        = "Nov";
        $mth[12]                                                        = "Dec";

        $month                                                          = $mth[($month * 1)];
        return $month;
    }

    //  Returns Previous Month
    function getPreviousMonth($year, $month)
    {
        $year                                                           = $year;
        $month                                                          = $month - 1;

        if ($month < 10)
            $month                                                      = "0".$month;

        if ($month == "00") {
            $month                                                      = "12";
            $year                                                       = $year - 1;
        }

        return "".$year."-".$month;
    }

    //  Returns Previous Month Start
    function getPreviousMonthStart($monthsBack = 1)     {
        return date("Y-m-01", strtotime("-$monthsBack months"));
/*
        $year                                                           = date("Y");
        $month                                                          = date("m");

        $month                                                          = $month - $monthsBack;

        if ($month < 10)
            $month                                                      = "0".$month;
        else if ($month == 0)
        {
            $year                                                       = $year - 1;
            $month                                                      = "12";
        }

        return "".$year."-".$month."-01";
*/
    }

    //  Return Next Month
    function getNextMonth($in_date, $plus)
    {
        $year                                                           = substr($in_date, 0, strpos($in_date, "-"));
        $month                                                          = substr($in_date, (strpos($in_date, "-") + 1), 2);

        //  Month Strings
        $mth                                                            = array();
        $mth[ 1]                                                        = "Jan";
        $mth[ 2]                                                        = "Feb";
        $mth[ 3]                                                        = "Mar";
        $mth[ 4]                                                        = "Apr";
        $mth[ 5]                                                        = "May";
        $mth[ 6]                                                        = "Jun";
        $mth[ 7]                                                        = "Jul";
        $mth[ 8]                                                        = "Aug";
        $mth[ 9]                                                        = "Sep";
        $mth[10]                                                        = "Oct";
        $mth[11]                                                        = "Nov";
        $mth[12]                                                        = "Dec";

        $month                                                          = $month + $plus;

        if ($month > 12)
        {
            $month                                                      = $month - (12 * floor($month / 12));

            if ((substr($in_date, (strpos($in_date, "-") + 1), 2) + $plus) > 12)
                $year                                                   = $year + (1 * floor((substr($in_date, (strpos($in_date, "-") + 1), 2) + $plus) / 12));

            if ($month == 0)
            {
                $year                                                   = $year - 1;
                $month                                                  = 12;
            }
        }

        $out_date                                                       = "".$mth[$month]."-".$year;

        return $out_date;
    }

    //  Returns Next Month Start
    function getNextMonthStart($in_date, $plus)
    {
        $year                                                           = substr($in_date, 0, strpos($in_date, "-"));
        $month                                                          = substr($in_date, (strpos($in_date, "-") + 1), 2);

        $month                                                          = $month + $plus;

        if ($month > 12)
        {
            $month                                                      = $month - (12 * floor($month / 12));

            if ((substr($in_date, (strpos($in_date, "-") + 1), 2) + $plus) > 12)
                $year                                                   = $year + (1 * floor((substr($in_date, (strpos($in_date, "-") + 1), 2) + $plus) / 12));

            if ($month == 0)
            {
                $year                                                   = $year - 1;
                $month                                                  = 12;
            }
        }

        if ($month < 10)
            $month                                                      = "0".$month;

        $out_date                                                       = "".$year."-".$month."-01";

        return $out_date;
    }

    //  Returns Date
    function getDates($in_date, $plus)
    {
        $out_date                                                       = strtotime($in_date) + ($plus * (86400));

        return date("Y-m-d", $out_date);
    }

    //  Get Current Week Start
    class GetCurrentWeek_Start
    {
        Function setDay()
        {
            $now                                                        = strtotime(now);
            $day                                                        = date(D);    
            switch($day)
            {
                case "Mon":
                    $startTimeStamp                                     = strtotime(now);
                    $Mon                                                = strtotime(now);
                    $date                                               = $Mon;
                break;            
                case "Tue":
                    $startTimeStamp                                     = strtotime(now);
                    $Mon                                                = strtotime(now) - (86400);
                    $date                                               = $Mon;
                break;
                case "Wed":
                    $startTimeStamp                                     = strtotime(now);
                    $Mon                                                = strtotime(now) - 2 * (86400);
                    $date                                               = $Mon;
                break;            
                case "Thu":
                    $startTimeStamp                                     = strtotime(now);
                    $Mon                                                = strtotime(now) - 3 * (86400);
                    $date                                               = $Mon;
                break;                        
                case "Fri":
                    $startTimeStamp                                     = strtotime(now);
                    $Mon                                                = strtotime(now) - 4 * (86400);
                    $date                                               = $Mon;
                break;                    
                case "Sat":
                    $startTimeStamp                                     = strtotime(now);
                    $Mon                                                = strtotime(now) - 5 * (86400);
                    $date                                               = $Mon;
                break;                    
                case "Sun":
                    $startTimeStamp                                     = strtotime(now);
                    $Mon                                                = strtotime(now) - 6 * (86400);
                    $date                                               = $Mon;
                break;                    
            }

            return ($date);
        }
    }

    function currentWeekStart()
    {
        $start                                                          = new GetCurrentWeek_Start();
        $day                                                            = $start -> setDay();

        return date("Y-m-d", $day);
    }

    //  Takes timestamp, subtracts it from NOW and returns the difference in text. Ex: 5 minutes ago
    function time_since($since) {
            $since = time()-strtotime($since);
            $chunks = array(
                    array(60 * 60 * 24 * 365 , 'year'),
                    array(60 * 60 * 24 * 30 , 'month'),
                    array(60 * 60 * 24 * 7, 'week'),
                    array(60 * 60 * 24 , 'day'),
                    array(60 * 60 , 'hour'),
                    array(60 , 'minute'),
                    array(1 , 'second')
            );
            for ($i = 0, $j = count($chunks); $i < $j; $i++) {
                    $seconds = $chunks[$i][0];
                    $name = $chunks[$i][1];
                    if (($count = floor($since / $seconds)) != 0)   break;
            }
            return ($count == 1) ? "$count {$name} ago" : "$count {$name}s ago";
    }

?>
