<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("_functions.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

	if (!hasAccess("EXPENSE_MANAGE") && !hasAccess("EXPENSE_VIEW"))
		header("Location: noaccess.php");

    $project_id                                                         = "null";

    if (isset($_POST["project"]))
        $project_id                                                     = $_POST["project"];
    else if ($_GET["project"] != "")
        $project_id                                                     = $_GET["project"];

    if (isset($_POST["btnUpdate"])) {
        $pid                                                            = $project_id;

        $expenses = q("SELECT id, descr, amount, vat, due_date, invoice_id, paid FROM ExpenseBudget ".
                        "WHERE company_id = '".$_SESSION["company_id"]."' AND project_id = '$pid' ORDER BY due_date");

        if (is_array($expenses)) {
            foreach($expenses as $expense) {
                $date                                                   = addslashes(strip_tags($_POST["repDate".$expense[0]]));
                $old_amount = q("SELECT amount FROM ExpenseBudget WHERE id = '".$expense[0]."'");
                $old_date = q("SELECT due_date FROM ExpenseBudget WHERE id = '".$expense[0]."'");
                $old_vat = q("SELECT vat FROM ExpenseBudget WHERE id = '".$expense[0]."'");
                $old_expense = ($old_amount+($old_amount*($old_vat/100)));

                if ($date != "") {
                    if ($_POST["box".$expense[0]])
                        $update = q("UPDATE ExpenseBudget SET due_date = '".$date."', paid = '1' WHERE id = '".$expense[0]."'");
                    else
                        $update = q("UPDATE ExpenseBudget SET due_date = '".$date."', paid = '0' WHERE id = '".$expense[0]."'");
					if ($old_date != $date)
						statusUpdateDate($old_date, $date, "expense", $old_expense, $_SESSION["company_id"]);
                }
            }
        }

        if ($update)
            header("Location: expense_split.php?project=$pid");
    }

    if (isset($_POST["btnCommit"])) {
        $pid                                                            = $project_id;
        $expID                                                          = $_GET["id"];
        $expAmount                                                      = $_POST["origAmount1"];
        $origExpense                                                    = $_POST["origAmount1"];
        $origVat                                                        = $_POST["origVat"];
        $origDate                                                       = $_POST["origDate"];
        $split                                                          = $_POST["total"];

        if ($split == "")
            $split                                                      = 2;

        $invoice_id                                                     = q("SELECT invoice_id FROM ExpenseBudget WHERE id = '$expID'");

        for ($a = 0; $a < $split; $a++) {
            $descr                                                      = addslashes(strip_tags($_POST["descr".$a]));
            $amount                                                     = addslashes(strip_tags($_POST["amount".$a]));
            $vat                                                        = addslashes(strip_tags($_POST["vat".$a]));
            $date                                                       = addslashes(strip_tags($_POST["date".$a]));

            if ($vat != "")
                $vat                                                    = round($vat, 2);
            else
                $vat                                                    = 0;

            if (submitDateCheck($date))  {
				//  Split Expense Into [X] Amount Of Expenses
				if (($descr !== "") && ($amount !== "") && ($vat !== "") && ($date !== "") && (is_numeric($amount)) && (is_numeric($vat))) {
					$amount                                                 = number_format($amount, 2, ".", "");
					$expAmount                                              -= $amount;

					$insert = q("INSERT INTO ExpenseBudget (project_id, descr, amount, vat, due_date, invoice_id, ".
						   "company_id, paid) VALUES ('$pid', '$descr', '$amount', '$vat', '$date', '$invoice_id', '".$_SESSION["company_id"]."', '0')");

					$expense_ = -($amount+($amount*($vat/100)));
					if ($expense_ != 0)
						statusUpdateAmount($date, $expense_, $_SESSION["company_id"]);

				}
            }
		}

        if ($expAmount <= "0")
            $delete = q("DELETE FROM ExpenseBudget WHERE id = '$expID'");
        else
            $update = q("UPDATE ExpenseBudget SET amount = '".number_format($expAmount, 2, ".", "")."' WHERE id = '$expID'");
		
		//TODO: status update here. (origExpense - expAmount)
		$old_expense = ($origExpense+($origExpense*($origVat/100)));
		$new_expense = ($expAmount+($expAmount*($origVat/100)));

		$amount = -($new_expense - $old_expense);
		if ($amount != 0)
			statusUpdateAmount($origDate, $amount, $_SESSION["company_id"]);


        //  Update Expense Diverse If Invoice && Expense Are Linked
        if ($invoice_id != 0) {
            $invExp                                                     = q("SELECT expenses FROM LoadInvoices WHERE id = '$invoice_id'");
            $totalExp                                                   = q("SELECT SUM(amount) FROM ExpenseBudget WHERE invoice_id = '$invoice_id'");
            $invDiv                                                     = number_format(($invExp - $totalExp), 2, ".", "");

            $update                                                     = q("UPDATE LoadInvoices SET diverse_income = '$invDiv' WHERE id = '$invoice_id'");
        }

        header("Location: expense_project.php?pid=$pid");
    }

    $projects                                                           = q("SELECT id, name FROM Project WHERE completed = '0' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."' ORDER BY name");

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("2_1", "budget");

    if ($errorMessage != "")
    {
        if ($errorMessage == "Please note that we are currently busy with data management, no entries are allowed for 2010-08-26. Please check back in 24 Hours.")
            echo "<p align='center' style='padding:0px;'><strong style='font-size:12px;'><font color='#FF5900'>$errorMessage</font></strong></p>";
        else
            echo "<p align='center' style='padding:0px;'><strong><font color='#999999'>$errorMessage</font></strong></p>";
        echo "<br/>";
    }
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    function recalculate() {
        var origExp = document.forms["expense_split"].origAmount1.value;
        var splitTotal = document.forms["expense_split"].splitTotal.value;

        if (splitTotal == "")
            splitTotal = 2;

        for (var i = 0; i < splitTotal; i++) {
            var amount = "amount" + i;

            if (eval("document.forms['expense_split']."+amount+".value") != "") {
                eval("document.forms['expense_split']."+amount+".value = parseFloat(document.forms['expense_split']."+amount+".value).toFixed(2);");

                origExp -= parseFloat(eval("document.forms['expense_split']."+amount+".value;"));
            }
        }

        document.forms["expense_split"].origAmount2.value = origExp.toFixed(2);
    }
</script>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="expense_split">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Split Expenses</h6>
                            </td>
                        </tr>
                        <tr>
                            <td class="centerdata">
                                <br/>
                            </td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                Project Name:
                            </td>
                            <td width="50%">
                                <select class="on-field" method="post" id="project" name="project" onChange="submit();" tabindex="1">
                                    <option value="null">--  Select Project --</option>
                                    <?php
                                        if (is_array($projects)) {
                                            foreach ($projects as $project)
                                                if ($project_id == $project[0])
                                                    echo "<option value='".$project[0]."' selected>".$project[1]."</option>";
                                                else
                                                    echo "<option value='".$project[0]."'>".$project[1]."</option>";
                                        }
                                    ?>
                                </select>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <?php
                        if ($project_id != "null" && $_GET["id"] == "") {
                            $expenses = q("SELECT id, descr, amount, vat, due_date, invoice_id, paid FROM ExpenseBudget ".
                                            "WHERE company_id = '".$_SESSION["company_id"]."' AND project_id = '$project_id' ORDER BY due_date");

                            $total_excl = 0;
                            $total_incl = 0;

                            if (is_array($expenses)) {
                                echo "<br/>";
                                echo "<div class='on-20px'><table class='on-table-center on-table'>";
                                    //  Table Headings
                                    echo "<tr>";
                                        echo "<th>Expense Description</th>";
                                        echo "<th>Expense <i>(".$_SESSION["currency"].") (excl)</i></th>";
                                        echo "<th>Expense <i>(".$_SESSION["currency"].") (incl)</i></th>";
                                        echo "<th>Due Date</th>";
                                        echo "<th>Linked Invoice Paid</th>";
                                        echo "<th>Paid</th>";
                                        echo "<th>Split Expense</th>";
                                    echo "</tr>";
                                    //  Table Information
                                    foreach ($expenses as $expense) {
                                        echo "<tr>";
                                            echo "<td>";
                                                echo "<a href='expense_edit.php?id=".$expense[0]."'>".$expense[1]."</a>";
                                            echo "</td>";
                                            if ($expense[2] != "") {
                                                $total_excl += $expense[2];

                                                echo "<td class='rightdata'>".number_format($expense[2], 2, ".", "")."</td>";
                                            }
                                            else
                                                echo "<td class='rightdata'>-</td>";
                                            if ($expense[2] != "") {
                                                $incl_vat = $expense[2] + ($expense[2] * ($expense[3] / 100));
                                                $total_incl += $incl_vat;

                                                echo "<td class='rightdata'>".number_format($incl_vat, 2, ".", "")."</td>";
                                            }
                                            else
                                                echo "<td class='rightdata'>-</td>";
                                            if ($expense[4] != "")
                                                echo "<td class='rightdata'><input name='repDate".$expense[0]."' ".
                                                    "type='text' style='text-align:right;' value='".$expense[4]."'></td>";
                                            else
                                                echo "<td class='rightdata'>-</td>";
                                            if ($expense[5] != "0") {
                                                $paid = q("SELECT paid FROM LoadInvoices WHERE id = '".$expense[5]."'");

                                                if ($paid == "1")
                                                    echo "<td class='centerdata'><input name='paid".$expense[0]."' type='checkbox' value='1' ".
                                                        "disabled checked></td>";
                                                else
                                                    echo "<td class='centerdata'><input name='paid".$expense[0]."' type='checkbox' value='1' ".
                                                        "disabled></td>";
                                            }
                                            else
                                                echo "<td class='centerdata'>-</td>";
                                            if ($expense[6] == "1")
                                                echo "<td class='centerdata'><input name='box".$expense[0]."' type='checkbox' value='1' ".
                                                    "checked></td>";
                                            else
                                                echo "<td class='centerdata'><input name='box".$expense[0]."' type='checkbox' value='1'>".
                                                    "</td>";
                                            echo "<td class='centerdata'>";
                                            if ($expense[6] == "1")
                                                echo "<input name='btnSplit' type='button' disabled value='Split Expense'>";
                                            else
                                                echo "<input name='btnSplit' type='button' ".
                                                    "onClick=\"location.href='expense_split.php?project=$project_id&id=".$expense[0]."'\" value='Split Expense'>";
                                            echo "</td>";
                                        echo "</tr>";
                                    }

                                    echo "<tr><td class='rightdata'>Grand Total ".
                                        "<i>(".$_SESSION["currency"].")</i>:</td><td class='rightdata'>".
                                        number_format($total_excl, 2, ".", "")."</td><td class='rightdata'>".
                                        number_format($total_incl, 2, ".", "")."</td><td class='rightdata' colspan='4'>".
                                        "</td></tr>";
                                    echo "<tr>";
                                        echo "<td class='centerdata' colspan='100%'><br/><input name='btnUpdate' value='Update Information' type='submit'></td>";
                                    echo "</tr>";
                                echo "</table></div>";
                            }
                        }
                        else if ($_GET["id"] != "") {
                            $info = q("SELECT descr, amount, vat, due_date FROM ExpenseBudget WHERE id = '".$_GET["id"]."'");

                            echo "<br/>
                           <table>
                               <tr>
                                   <td class='rightdata'>
                                       <table>
                                           <tr class='on-description'>
                                                <td colspan='2' class='centerdata'>
                                                    <h6>Original Expense</h6><br/></tr>
                                                </td>
                                           <tr>
                                                <td class='centerdata' colspan='2'></td>
                                            </tr>
                                           <tr>
                                               <td style='width:200px;' class='on-description'>
                                                       Expense Description:
                                               </td>
                                               <td>
                                                   <input class='on-field' name='origName' type='text' readonly value='".$info[0][0]."'>
                                               </td>
                                           </tr>
                                           <tr>
                                               <td class='on-description'>
                                                       Expense <i>(".$_SESSION["currency"].")</i>:
                                               </td>
                                               <td>
                                                   <input class='on-field' name='origAmount1' type='hidden' style='text-align:right;' readonly value='".$info[0][1]."'>
                                                   <input class='on-field' name='origAmount2' type='text' style='text-align:right;' readonly value='".$info[0][1]."'>
                                               </td>
                                           </tr>
                                           <tr>
                                               <td class='on-description'>
                                                    VAT <i>(%)</i>:
                                               </td>
                                               <td>
                                                   <input class='on-field-date' name='origVat' type='text' style='text-align:right;' readonly value='".$info[0][2]."'>
                                               </td>
                                           </tr>
                                           <tr>
                                               <td class='on-description'>
                                                    Due Date:
                                               </td>
                                               <td>
                                                   <input class='on-field-date' name='origDate' type='text' style='text-align:right;' readonly value='".$info[0][3]."'>
                                               </td>
                                           </tr>
                                           <tr>
                                               <td class='on-description'>
                                                    Split Into:
                                               </td>
                                               <td>";
                                                    if ($_POST["total"] != "" && $_POST["total"] != "0" && is_numeric($_POST["total"]))
                                                       echo "<input class='on-field' name='total' type='text' style='text-align:right;' value='".$_POST["total"]."'>";
                                                    else
                                                       echo "<input class='on-field' name='total' type='text' style='text-align:right;' value=''>";
                                               echo "</td>
                                           </tr>
                                           <tr><td class='centerdata' colspan='2'><br/><input name='btnGen' type='button' value='Generate Boxes' ".
                                                "onClick='submit();'></td></tr>
                                       </table>
                                   </td>
                                   <td>
                                       <table width='100%'>
                                           <tr>
                                                <td class='centerdata' colspan='4'>
                                                    <h6>Split Expense</h6>
                                                </td>
                                            </tr>
                                        </table>
                                        <table class='on-table'>
                                           <tr>
                                               <th width='25%'>Expense Description</th>
                                               <th width='25%'>Expense <i>(".$_SESSION["currency"].")</i></th>
                                               <th width='25%'>VAT <i>(%)</i></th>
                                               <th width='25%'>Due Date</th>
                                           </tr>";
                                            if (isset($_POST["total"])) {
                                                if ($_POST["total"] != "" && $_POST["total"] != "0" && is_numeric($_POST["total"])) {
                                                    for ($i = 0; $i < $_POST["total"]; $i++) {
                                                        echo "<tr>";
                                                            echo "<td class='centerdata' width='25%'>";
                                                                echo "<input name='descr".$i."' tabindex='".($i + 1)."' type='text' value=''>";
                                                            echo "</td>";
                                                            echo "<td class='centerdata' width='25%'>";
                                                                echo "<input name='amount".$i."' tabindex='".($i + 1)."' type='text' style='text-align:right;' value='' ".
                                                                    "onBlur='recalculate();'>";
                                                            echo "</td>";
                                                            echo "<td class='centerdata' width='25%'>";
                                                                echo "<input name='vat".$i."' tabindex='".($i + 1)."' type='text' style='text-align:right;' ".
                                                                    "value='".$info[0][2]."'>";
                                                            echo "</td>";
                                                            echo "<td class='centerdata' width='25%'>";
                                                                echo "<input name='date".$i."' tabindex='".($i + 1)."' type='text' style='text-align:right;' ".
                                                                    "value='".$info[0][3]."'>";
                                                            echo "</td>";
                                                        echo "</tr>";
                                                    }
                                                }
                                            }
                                            else {
                                                for ($i = 0; $i < 2; $i++) {
                                                    echo "<tr>";
                                                        echo "<td class='centerdata' width='25%'>";
                                                            echo "<input name='descr".$i."' tabindex='".($i + 1)."' type='text' value=''>";
                                                        echo "</td>";
                                                        echo "<td class='centerdata' width='25%'>";
                                                            echo "<input name='amount".$i."' tabindex='".($i + 1)."' type='text' style='text-align:right;' value='' ".
                                                                "onBlur='recalculate();'>";
                                                        echo "</td>";
                                                        echo "<td class='centerdata' width='25%'>";
                                                            echo "<input name='vat".$i."' tabindex='".($i + 1)."' type='text' style='text-align:right;' ".
                                                                "value='".$info[0][2]."'>";
                                                        echo "</td>";
                                                        echo "<td class='centerdata' width='25%'>";
                                                            echo "<input name='date".$i."' tabindex='".($i + 1)."' type='text' style='text-align:right;' ".
                                                                "value='".$info[0][3]."'>";
                                                        echo "</td>";
                                                    echo "</tr>";
                                                }
                                            }

                                            echo "<tr><td class='centerdata' colspan='4'><input name='btnCommit' type='submit' value='Apply Split To Expense'></td></tr>";
                                            echo "<input name='splitTotal' type='hidden' value='".$_POST["total"]."'>";
                                        echo "</table>";
                                    echo "</td>";
                                echo "</tr>";
                            echo "</table>";
                        }
                    ?>
                    <br/>
                    <input method="post" name="display" type="hidden" value="0" />
                    <input method="post" name="save" type="hidden" value="0" />
                    <input method="post" name="update" type="hidden" value="0" />
                </form>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
