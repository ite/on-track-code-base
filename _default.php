<?php
    include("_db.php");
?>
<html>
    <head>
        <title>
            On-Track - Default Script...
        </title>
    </head>
    <body>
        <h1 align="center">
            On-Track - Default Script...
        </h1>
        <?php
            /*
            $drop = q("DROP TABLE IF EXISTS user_defaults");
            $create = q("CREATE TABLE user_defaults (
                                  id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
                                  companyID SMALLINT UNSIGNED,INDEX(companyID),
                                  userID SMALLINT UNSIGNED,INDEX(userID),
                                  landingPage VARCHAR(255) NOT NULL DEFAULT 'bookings.php',
                                  activityType SMALLINT UNSIGNED DEFAULT 0,
                                  activity SMALLINT UNSIGNED DEFAULT 0)");

            $insert = q("INSERT INTO user_defaults (companyID,userID,landingPage) SELECT companyID,userID,landingPage FROM landingPage");
            */

            $alter = q("ALTER TABLE Employee ADD COLUMN target VARCHAR(10) AFTER cost");

            //$update = q("UPDATE Employee SET target = FORMAT((cost*30*8),2)");
            $update = q("UPDATE Employee SET target = REPLACE(FORMAT((cost*30*8),2), ',', '')");
        ?>
        <form action="index.php" method="post">
            <center><input type="submit" value="Return"/></center>
        </form>
    </body>
</html>
