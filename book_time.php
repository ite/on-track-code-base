<?php
	session_start();

	include("_db.php");
	include("graphics.php");
	include("include/sajax.php");

	if (!$_SESSION["logged_in"] === true)
		header("Location: login.php");

	if (!$_SESSION["eula_agree"] === true)
		header("Location: logout.php");

	if (!isset($_SESSION["company_id"]))
		header("Location: home.php");

	if (!hasAccess("TIME_MANAGE"))
		header("Location: noaccess.php");

	$index = 1;
	$currency = "<i>(".$_SESSION["currency"].")</i>";

	$user_name = q("SELECT CONCAT(frstname,' ',lstname) FROM Employee WHERE id = '".$_SESSION["user_id"]."'");

	if (isset($_POST["btnUpdate"])) {
		unset($_POST["btnUpdate"]);

		$array = split("_", addslashes(strip_tags($_POST["idBox"])));

		if (is_array($array))
			$array = array_slice($array, 0, count($array) - 1);

		if (isset($timeArr))
			unset($timeArr);

		$timeArr[] = number_format("0.25", 2, ".", "");
		$timeArr[] = number_format("0.50", 2, ".", "");
		$timeArr[] = number_format("0.75", 2, ".", "");

		for ($i = 1; $i <= 12; $i += 0.5)
			$timeArr[] = number_format($i, 2, ".", "");

		foreach ($array as $a)  {
			$id = substr($a, 0, strpos($a, ":"));
			$locked = substr($a, (strpos($a, ":") + 1), strlen($a));

			if (is_numeric($id))    {
				if (addslashes(strip_tags($_POST["TIME".$id])) == "1")	{
					$date = addslashes(strip_tags($_POST["A".$id]));
					$descr = addslashes(strip_tags($_POST["B".$id]));
					$time = number_format(addslashes(strip_tags($_POST["C".$id])), 2, ".", "");

					if (submitDateCheck($date)) {
						if (in_array($time, $timeArr, true) && $locked == "0")  {
							$update = q("UPDATE TimeSheet SET date = '$date', descr = '$descr', time_spent = '$time' WHERE id = '$id'");
							$update = q("UPDATE ApprovedTime SET date = '$date', descr = '$descr', time_spent = '$time' WHERE timesheet_id = '$id'");
						}
					}
				}
			}
		}

		if ($errorMessage == "")    header("Location: book_time.php");
	}

	if (isset($_POST["save"]) && $_POST["save"] === "1")    {
		$errorMessage = "";

		$today = date("Y-m-d");

		///////////////////////////
		$timeTime = date("H:i:s");
		///////////////////////////

		///////////////////////////
		//  Get Information
		$project = $_POST["project"];
		$projectType = (substr($project,0,3) == "CP_") ? "CP" : "SP";
		$projectID = substr($project,3);
		///////////////////////////
		//  Time Fields
		$area = ($_POST["area"] != "") && is_numeric($_POST["area"]) ? addslashes(strip_tags($_POST["area"])) : 0;      // Check if any AREAs were posted
		$activityType = $_POST["activityType"];
		$activity = $_POST["activity"];
		$timeDescr = addslashes(strip_tags($_POST["timeDescr"]));
		$timeDate = addslashes(strip_tags($_POST["timeDate"]));
		$timeSpent = $_POST["timeSpent"];

		$timeRate = q("SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$_SESSION["user_id"]."' ".
					"AND pu.company_id = '".$_SESSION["company_id"]."' AND pu.project_id = '".$projectID."' AND ur.companyid = '".$_SESSION["company_id"]."' AND ur.userid = '".$_SESSION["user_id"]."'");
		//$tariff = "tariff".q("SELECT user_tariff FROM Project_User WHERE user_id = '".$_SESSION["user_id"]."' AND company_id = '".$_SESSION["company_id"]."' AND project_id = '".$projectID."'");
		//$timeRate = q("SELECT $tariff FROM Employee WHERE id = '".$_SESSION["user_id"]."'");
		///////////////////////////

		if (submitDateCheck($timeDate))   {
			if ($timeDate <= $today)    {
				if ($projectID != "" && $_SESSION["user_id"] != "" && $activity != "" && $timeDate != "" && $timeSpent != "")
				if ($projectID != "null" && $activity != "null" && $timeSpent != "null")
				if ($projectID != "0" && $_SESSION["user_id"] != "0" && $activity != "0" && $timeSpent != "0")   {
					$insert = q("INSERT INTO TimeSheet (company_id, projectType, date, user_id, project_id, area_id, activity_id, descr, rate, time_spent, on_date, on_time) VALUES ".
								"('".$_SESSION["company_id"]."','".$projectType."', '$timeDate', '".$_SESSION["user_id"]."', '$projectID', '$area', '$activity', '$timeDescr', '$timeRate', '$timeSpent', '$today', '$timeTime')");

					if ($insert)    {
							//$timeSheetID = q("SELECT id FROM TimeSheet WHERE user_id = '".$_SESSION["user_id"]."' AND on_date = '$today' AND on_time = '$timeTime'");
							$timeSheetID = q("SELECT id FROM TimeSheet ".
														"WHERE company_id = '".$_SESSION["company_id"]."' AND projectType = '".$projectType."' AND date = '".$timeDate."' AND user_id = '".$_SESSION["user_id"]."' ".
															"AND project_id = '".$projectID."' AND area_id = '".$area."' AND activity_id = '".$activity."' AND descr = '".$timeDescr."' AND rate = '".$timeRate."' ".
															"AND time_spent = '".$timeSpent."' AND on_date = '".$today."' AND on_time = '".$timeTime."'");
							$insert = q("INSERT INTO ApprovedTime (company_id,projectType, date, user_id, project_id, area_id, activity_id, descr, rate, time_spent, on_date, on_time, timesheet_id, ".
										"status) VALUES ('".$_SESSION["company_id"]."','".$projectType."', '$timeDate', '".$_SESSION["user_id"]."', '$projectID','$area', '$activity', '$timeDescr', '$timeRate', '$timeSpent', ".
										"'$today', '$timeTime',  '$timeSheetID', '0')");

						$projectIDName = q("SELECT name FROM Project WHERE id = '$projectID'");

						$logs = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) VALUES ".
									"('".$_SESSION["email"]." booked time on ".$projectIDName."', 'Insert', 'TimeSheet', '".$_SESSION["email"]."', '$today', '$timeTime', ".
									"'".$_SESSION["company_id"]."')");
					}
				}

				header("Location: book_time.php");
			}
			else
				$errorMessage = "Time cannot be booked in advance";
		}
	}

	//  Print Header
	print_header();
	//  Print Menu
	print_menus("0", "bookings", "bookings");

	if ($errorMessage != "")    {
		if ($errorMessage == "Please note that we are currently busy with data management, no entries are allowed for 2010-08-26. Please check back in 24 Hours.")
			echo "<p style='text-align:center'><font color='red'> - $errorMessage - </font></p>";
		else
			echo "<p style='text-align:center'><font class='on-validate-error'> - $errorMessage - </font></p>";
		echo "<br/>";
	}

	$projects = q("SELECT DISTINCT(p.id), p.name, p_type FROM (Project AS p INNER JOIN Project_User AS pu ON p.id = pu.project_id INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) ".
					"WHERE pu.user_id = '".$_SESSION["user_id"]."' AND cot.company_id = '".$_SESSION["company_id"]."' AND pu.company_id = '".$_SESSION["company_id"]."' AND p.completed = '0' AND p.deleted = '0' ".
					"AND pu.deleted = '0' ORDER BY UPPER(p.name)");
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
	var loadActivityTypes;
	var loadActivities;
	var checkTimeBudget;

	var hasAreas = false;

	function is_array(input)    {
		return typeof(input)=='object'&&(input instanceof Array);
	}

	jQuery(function()    {
		//////////////////////////////
		//  Load Areas
		loadAreas = function()  {
			var projectType = jQuery("#project").val().substring(0, 2);
			var projectID = jQuery("#project").val().substring(3);

			jQuery.post("_ajax.php", {func: "get_area_info", projectType: projectType, projectID: projectID}, function(data)	{
				data = json_decode(data);

				jQuery("#area option").remove();

				jQuery("#area").append(jQuery("<option></option>").attr("value","null").text("--  Select Area --"));

				if (is_array(data)) {   // Display area options
					hasAreas = true;

					jQuery.each(data,function(i, v)	{
						jQuery("#area").append(jQuery("<option></option>").attr("value",v[0]).text(v[1]));
					});

					jQuery("#areaInfo1").show();
					jQuery("#areaInfo2").show();
				}
				else    {               // Do not display area options
					hasAreas = false;

					jQuery("#areaInfo1").hide();
					jQuery("#areaInfo2").hide();
				}
			});
		};

		//  Load Activity Types
		loadActivityTypes = function()  {
			var projectType = jQuery("#project").val().substring(0, 2);
			var projectID = jQuery("#project").val().substring(3);

			jQuery.post("_ajax.php", {func: "get_activity_types", projectType: projectType, projectID: projectID}, function(data)	{
				data = json_decode(data);

				jQuery("#activityType option").remove();
				jQuery("#activity option").remove();

				jQuery("#activityType").append(jQuery("<option></option>").attr("value","null").text("--  Select Activity Type  --"));

				if (is_array(data))
				jQuery.each(data,function(i, v)	{
					jQuery("#activityType").append(jQuery("<option></option>").attr("value",v[0]).text(v[1]));
				});

				jQuery("#activityType").val(<?php echo $_SESSION["default_activityType"];?>);

				loadActivities();
			});
		};

		//  Load Activities
		loadActivities = function()  {
			var projectType = jQuery("#project").val().substring(0, 2);
			var activityType = jQuery("#activityType").val();

			jQuery.post("_ajax.php", {func: "get_activities", projectType: projectType, activityType: activityType}, function(data)	{
				data = json_decode(data);

				jQuery("#activity option").remove();
				jQuery("#activity").append(jQuery("<option></option>").attr("value","null").text("--  Select Activity  --"));

				jQuery.each(data,function(i, v)	{
					jQuery("#activity").append(jQuery("<option></option>").attr("value",v[0]).text(v[1]));
				});

				jQuery("#activity").val(<?php echo $_SESSION["default_activity"];?>);
			});
		};

		//  Check Time/Budget
		checkTimeBudget = function()  {
			var projectID = jQuery("#project").val().substring(3);

			jQuery.post("_ajax.php", {func: "checkTimeBudget", projectID: projectID}, function(data)	{
				data = json_decode(data);

				if (!(data[0] == "N/A" || data[0] > 0))
					jQuery("#budgetExceeded").show();

				jQuery("#timeBudgetLeftDiv").show();
				jQuery("#timeBudgetLeft").text(data[1]);
			});
		};
		//////////////////////////////

		//////////////////////////////
		//  Date Picker(s)
		jQuery("#timeDate").DatePicker( {
			format:"Y-m-d",
			date: jQuery("#timeDate").val(),
			current: jQuery("#timeDate").val(),
			starts: 1,
			position: "right",
			onBeforeShow: function()    {
				var _date = (!validation("date", jQuery("#timeDate").val())) ? new Date(): jQuery("#timeDate").val();
				jQuery("#timeDate").DatePickerSetDate(_date, true);
			},
			onChange: function(formated, dates) {
				jQuery("#timeDate").val(formated);
				jQuery("#timeDate").DatePickerHide();
			}
		});
		//////////////////////////////

		//////////////////////////////
		//  Project Change - Show/Hide
		jQuery("#project").change(function()    {
			jQuery("#budgetExceeded").hide();

			if (jQuery(this).val() != "null")    {
				jQuery("#info").show();
				jQuery("#project").focus();

				checkTimeBudget();
				loadAreas();
				loadActivityTypes();
			}
			else    {
				jQuery("#timeBudgetLeftDiv").hide();
				jQuery("#info").hide();
				jQuery("#project").focus();
			}
		});
		//////////////////////////////

		// WATCH FOR CHANGE
		jQuery(".watch-for-change").change(function()	{
			var field = jQuery(this).attr("id").substr(0,1);
			var id = jQuery(this).attr("id").substr(1);

			if (field == "A" || field == "B" || field == "C")
				jQuery("#TIME"+id).val("1");
			else
				jQuery("#EXPENSE"+id).val("1");
		});

		//////////////////////////////
		//  Activity Type Change
		jQuery("#activityType").change(function()    {
			loadActivities();
		});
		//////////////////////////////

		//////////////////////////////
		//  Book Button Pressed
		jQuery("#btnAdd").click(function()    {
			jQuery("#btnAdd").attr("disabled","true");

			var i = 0;
			var j;
			var valid = true;
			var test;

			var fields = [];

			fields[i++] = "project";

			//  Time Fields
			if (hasAreas)       var timeFields = new Array("area", "activityType","activity","timeDescr","timeDate","timeSpent");
			else                var timeFields = new Array("activityType","activity","timeDescr","timeDate","timeSpent");

			// Time, expense or both
			var timeBook = false;

			//  Add Time Fields
			if(jQuery("#activityType").val() != "null")      for (j = 0; j < timeFields.length; j++)  {fields[i++] = timeFields[j];  }

			for (i = 0; i < fields.length; i++)  {
				var field = fields[i];
				var cls = jQuery("#"+field).attr("class");

				cls = cls.split(" ");

				for (j = 0; j < cls.length; j++)  {
					var divName = (cls[j] == "entered") ? "#"+field+"Empty" : "#"+field+"Div";
					test = validation(cls[j], jQuery("#"+field).val());

					if (!test)  jQuery(divName).show();
					else        jQuery(divName).hide();

					valid &= test;
				}
			}

			if (valid) {
				jQuery("#save").val("1");
				jQuery("#bookings").submit();
			} else      jQuery("#btnAdd").removeAttr("disabled");
		});
		//////////////////////////////
	});
</script>
<table width="100%">
	<tr height="380px">
		<td class="centerdata" valign="top">
			<form action="" id="bookings" name="bookings" method="post">
				<table width="100%">
					<tr>
						<td class="centerdata">
							<h6>
								Time Bookings
							</h6>
						</td>
					</tr>
				</table>
				<br/>
				<div id="budgetExceeded" name="budgetExceeded" style="display: none;">
					<center>
						<font class='on-validate-error'>
							- Budget For Project Has Been Reached/Exceeded, Please Notify Your Project Manager -
						</font>
					</center>
					<br/>
				</div>
				<div id="timeBudgetLeftDiv" name="timeBudgetLeftDiv" style="display: none;">
					<table width="100%">
						<tr>
							<td class="on-description" width="50%">
								Time/Budget Left:
							</td>
							<td id="timeBudgetLeft" name="timeBudgetLeft" width="50%" class="on-description-left on-validate-error"></td>
						</tr>
					</table>
				</div>
				<table width="90%">
					<tr>
						<td class="on-description" width="50%">
								Project Name:
						</td>
						<td style="padding-top:2px;" width="50%">
							<select id="project" name="project" method="post" class="on-field required" tabindex="<?php echo $index++; ?>">
								<option value="null">--  Select A Project  --</option>
								<?php
									if (is_array($projects))    {
										foreach ($projects as $project) {
											$value = ($project[2] == "CP") ? "CP_".$project[0]: "SP_".$project[0];

											echo "<option value='".$value."'>".$project[1]."</option>";
										}
									}
								?>
							</select>
							<div id="projectDiv" name="projectDiv" class="error">* Project must be selected</div>
						</td>
					</tr>
				</table>
				<br/>
				<div id="info" name="info" style="display: none;">
					<table width="90%">
						<tr>
							<td class="centerdata" valign="top" style="padding-top:4px;">
								<h6>
									Time Information
								</h6><br>
								<table width="100%">
									<tr>
										<td class="on-description" width="50%">
											<div id="areaInfo1" name="areaInfo1" style="display:none;">
												Area:
											</div>
										</td>
										<td>
											<div id="areaInfo2" name="areaInfo2" style="display:none;">
												<select id="area" name="area" method="post" class="on-field required" tabindex="<?php echo $index++; ?>">
													<option value="null">--  Select A Area  --</option>
													<?php
														if (is_array($areas))
															foreach ($areas as $area)
																echo "<option value='".$area[0]."'>".$area[1]."</option>";
													?>
												</select>
												<div id="areaDiv" name="areaDiv" class="error">* Area must be selected</div>
											</div>
										</td>
									</tr>
									<tr>
										<td class="on-description" width="50%">
											Activity Type:
										</td>
										<td width="50%">
											<select id="activityType" name="activityType" method="post" class="on-field required" tabindex="<?php echo $index++; ?>">
											</select>
											<div id="activityTypeDiv" name="activityTypeDiv" class="error">* Activity type must be selected</div>
										</td>
									</tr>
									<tr>
										<td class="on-description" width="50%">
											Activity:
										</td>
										<td width="50%">
										   <select id="activity" name="activity" method="post" class="on-field required" tabindex="<?php echo $index++; ?>"></select>
											<div id="activityDiv" name="activityDiv" class="error">* Activity must be selected</div>
										</td>
									</tr>

									<tr>
										<td class="on-description" width="50%">
											Description:
										</td>
										<td width="50%">
											<textarea id="timeDescr" name="timeDescr" cols="50" rows="6" class="on-field maxLength" tabindex="<?php echo $index++; ?>"></textarea>
											<div id="timeDescrDiv" name="timeDescrDiv" class="error">* Description may not be longer that 250 characters</div>
										</td>
									</tr>
									<tr>
										<td class="on-description" width="50%">
											Date:
										</td>
										<td width="50%">
											<input id="timeDate" name="timeDate" tabindex="<?php echo $index++; ?>" class="entered required date on-field-date" type="text" style="text-align:right;" value="<?php
												echo "".$today; ?>" />
											<div id="timeDateEmpty" name="timeDateEmpty" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
											<div id="timeDateDiv" name="timeDateDiv" class="error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></div>
										</td>
									</tr>
									<tr>
										<td class="on-description" width="50%">
												Time Spent:
										</td>
										<td  width="50%">
											<select id="timeSpent" name="timeSpent" method="post" class="on-field-date required" tabindex="<?php echo $index++; ?>">
												<option value="null"></option>
												<option value="0.25">0.25</option>
												<option value="0.50">0.50</option>
												<option value="0.75">0.75</option>
												<?php
													for ($i = 1; $i <= 12; $i += 0.5)   echo "<option value='".number_format($i, 2)."'>".number_format($i, 2, ".", "")."</option>";
												?>
											</select>
											<div id="timeSpentDiv" name="timeSpentDiv" class="error">* Time spent must be selected</div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<br/>
					<input id="btnAdd" name="btnAdd" type="button" tabindex="<?php echo $index++; ?>" value="Book Time" />
				</div>
				<br/><br/>
				<input id="save" name="save" method="post" type="hidden" value="0" />
				<br/>
				<?php
					$user_name = q("SELECT CONCAT(frstname, ' ', lstname) FROM Employee WHERE id = '".$_SESSION['user_id']."' ");
					$weekStart = currentWeekStart();
					$date = $today;

					$finalDisplay = "";
					$display = "";

					 ///////////////////////////////
					$hasAreas = 0;                                               // Does Areas exist for the user in expense or time sheet table

					$areasTime                                                     = q("SELECT project_id, area_id FROM TimeSheet WHERE user_id = ".$_SESSION["user_id"]." AND area_id > 0 AND date BETWEEN '".$weekStart."' AND '".$today."'");

					if(is_array($areasTime)) {                              // Time
						$hasAreas = 1;
					}
				////////////////////////////////
				$extraCounter = 0;
				if($hasAreas){
					$areaData = "<th>Area</th>";
					$extraCounter  = 1;
				}else{
					$areaData = "";
					$extraCounter  = 0;
				}

					$printed_headings = 0;

						//  Daily Summary - Time Booked
						$headings = "<tr>".
										"<th>Date</th>".
										"<th>Project Name</th>".
										$areaData.
										"<th>Activity Type</th>".
										"<th>Activity</th>".
										"<th>Description</th>".
										"<th>&nbsp;</th>
										<th>Time</th>".
									"</tr>";

					$dayHours = 0;
					$totalHours = 0;
					$timeDisplay = "";

					while ($date >= $weekStart)    {
						$dayHours = 0;
						$timeDisplay = "";

						$timeSummary = q("SELECT ts.id,ts.projectType,ts.project_id,ts.area_id,ts.activity_id,ts.descr,ts.time_spent,ts.locked FROM (Project AS p
											INNER JOIN Project_User AS pu ON p.id = pu.project_id
											INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
											INNER JOIN TimeSheet AS ts ON ts.project_id = p.id)
											WHERE pu.user_id = '".$_SESSION["user_id"]."'
												AND ts.company_id = '".$_SESSION["company_id"]."'
												AND cot.company_id = '".$_SESSION["company_id"]."'
												AND pu.company_id = '".$_SESSION["company_id"]."'
											AND ts.user_id = '".$_SESSION["user_id"]."'
											AND p.deleted = '0'
											AND pu.deleted = '0'
											AND ts.date = '$date'
											ORDER BY ts.on_date, ts.on_time");

						if (is_array($timeSummary))   {
							foreach ($timeSummary as $s) {
									$project = q("SELECT name FROM Project WHERE id = '".$s[2]."'") ;

									$idBox .= "".$s[0].":".$s[7]."_";
									$dayHours += $s[6];
									$totalHours += $s[6];

									$readonly = ($s[7] != "0") ? "readonly" : "";

									if ($s[1] == "CP")  {
										///////////////
										$activityType = q("SELECT type FROM ActivityTypes WHERE id = (SELECT parent_id FROM Activities WHERE id = '".$s[4]."')");
										$activity = q("SELECT name FROM Activities WHERE id = '".$s[4]."'");
										if($hasAreas){
											$area = q("SELECT name FROM areas WHERE id = ".$s[3]." ");
											if($s[3] != 0){
												$aData = "<td style='white-space:nowrap'>".$area."</td>";
											}else{
												$aData = "<td>-</td>";
											}
										}else{
											$aData = "";
										}
										/////////////////
									}
									else { // if Shared Project
										/////////////////
										$activityType = q("SELECT type FROM ActivityTypes WHERE id = (SELECT parent_id FROM Activities WHERE id = '".$s[4]."')");
										$activity = q("SELECT name FROM Activities WHERE id = '".$s[4]."'");
										if($hasAreas){
											$area = q("SELECT name FROM areas WHERE id = ".$s[3]." ");
											if($s[3] != 0){
												$aData = "<td style='white-space:nowrap'>".$area."</td>";
											}else{
												$aData = "<td style='white-space:nowrap'>-</td>";
											}
										}else{
											$aData = "";
										}
										/////////////////
									}

								$timeDisplay .= "<tr>".
													"<td>
														<input id='TIME".$s[0]."' name='TIME".$s[0]."' type='hidden' value='0'>
														<input id='A".$s[0]."' name='A".$s[0]."' type='text' size='12' $readonly  value='".$date."' tabindex='".$index."' class='watch-for-change'>
													</td>
													<td><div class='projectReportName'>".$project."</div></td>".
													$aData.
													"<td style='white-space:nowrap'>".$activityType."</td>
													<td style='white-space:nowrap'>".$activity."</td>
													<td>
														<input id='B".$s[0]."' name='B".$s[0]."' type='text' size='50' $readonly value='".str_replace("'", "&#39;" , $s[5])."' tabindex='".$index."' class='watch-for-change'>
													</td>
													<td>
														&nbsp;
													</td>
													<td>
														<input id='C".$s[0]."' class='number watch-for-change' name='C".$s[0]."' type='text' size='12' $readonly value='".number_format($s[6], 2, ".", "")."' tabindex='".$index."'>
													</td>
												</tr>";
							}

							if($printed_headings == 1)
								$headings = "";

							$display .= $headings.$timeDisplay.
										"<tr>".
											"<td class='on-table-total'></td>
											<td colspan='".(5+$extraCounter)."' class='on-table-total'>Total:</td>
											<td class='on-table-total'>".number_format($dayHours, 2, ".", "")."</td>
										</tr>";
							$printed_headings = 1;
						}
						$date = getDates($date, -1);
					}

					if ($display != "")   {
						$display .= "<tr>".
										"<td class='on-table-total'></td>
										<td colspan='".(5+$extraCounter)."' class='on-table-total'>Grand Total:</td>
										<td class='on-table-total'>".number_format($totalHours, 2, ".", "")."</td>
										</tr>
									</tr>";
					}
					if ($display != "")
						$finalDisplay .=  "<tr><td class='on-table-clear' colspan='6".+$extraCounter."'><a>Booked <font class='on-validate-error'>Time</font> Information: ".$user_name."</a></td></tr>".$display;

					if ($finalDisplay != "")    {
						echo "<div class='on-20px'><h6>Bookings for this week</h6><table class='on-table on-table-center'>$finalDisplay</table></div>";
						echo "<table class='on-table-center'><tr><td><input id='btnUpdate' name='btnUpdate' type='submit' value='Apply Changes' tabindex='".++$index."'></tr></td></table>";
						echo "<input method='post' name='idBox' type='hidden' value='".$idBox."' />";
						echo "<input method='post' name='idBox2' type='hidden' value='".$idBox2."' />";
					}
				?>
			</form>
		</td>
	</tr>
	<tr>
		<td align="center">
			<br/>
		</td>
	</tr>
</table>
<?php
    //  Print Footer
    print_footer();
?>
