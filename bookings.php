<?php
	session_start();

	include("_db.php");
	include("graphics.php");
	include("include/sajax.php");

	if (!$_SESSION["logged_in"] === true)
		header("Location: login.php");

	if (!$_SESSION["eula_agree"] === true)
		header("Location: logout.php");

	if (!isset($_SESSION["company_id"]))
		header("Location: home.php");

	if (!hasAccess("TIME_MANAGE") || !hasAccess("EXPENSE_MANAGE_OWN"))
		header("Location: noaccess.php");

	///////////////////////////
	//  Sajax
	function getPrinting($id) {
		return q("SELECT id, name FROM Disbursements WHERE parent_id = '$id' ORDER BY name");
	}

	$sajax_request_type = "GET";
	sajax_init();
	sajax_export("getPrinting");
	sajax_handle_client_request();
	///////////////////////////

	$index = 1;
	$currency = "<i>(".$_SESSION["currency"].")</i>";
	global $file_directory;

	//  Expenses
    if ($_SESSION["company_name"] !== 'OMI Solutions')
        $expenseTypes = q("SELECT id, name FROM dropdowns WHERE categoryID = (SELECT id FROM categories WHERE code = 'expenseType') ORDER BY ranking ASC");
    else
        $expenseTypes = q("SELECT id, name FROM dropdowns WHERE categoryID = (SELECT id FROM categories WHERE code = 'expenseType') AND name != 'Toll Gate' ORDER BY ranking ASC");
	$sars_compliance = q("SELECT sars_compliance FROM Company WHERE id = '".$_SESSION["company_id"]."'");
	$backing_documentation = q("SELECT backing_documents FROM Company WHERE id = '".$_SESSION["company_id"]."'");

	//////////////////////////////////////////////////////
	//  Fields For Expense Types
	//////////////////////////////////////////////////////
	$descr["Expense"] = array("Total $currency","Slip(s)","VAT included");
	$fields["Expense"] = array("Expense_expense","Expense_slips","Expense_VAT");
	$validation["Expense"] = array("currency required","file","");

	$descr["Driving"] = array("Traveling From <i>(Use abreviations)</i>","Traveling To <i>(Use abreviations)</i>","ODO Start","ODO Finish","Business Km's","Private KM's","Total KM's","Travel Time");
	$fields["Driving"] = array("Driving_placeFrom","Driving_placeTo","odoStart","odoFinish","businessKM","privateKM","kilometers","Driving_travelTime");
	if ($sars_compliance == "1")
		$validation["Driving"] = array("","","currency required-currency","currency required-currency","currency required-currency","currency required-currency","currency required-currency","");
	else
		$validation["Driving"] = array("","","currency","currency","currency","currency","currency","");

	$descr["Printing"] = array("No of Pages", "Slip(s)");
	$fields["Printing"] = array("Printing_total", "Printing_slips");
	$validation["Printing"] = array("number required","file");

	$descr["Car Hire"] = array("Company","Rate","Invoice(s)","VAT included");
	$fields["Car Hire"] = array("Car_Hire_otherDescr","Car_Hire_rate","Car_Hire_invoices","Car_Hire_VAT");
	$validation["Car Hire"] = array("","currency required","file","");

	$descr["Flights"] = array("Traveling From <i>(Use abreviations)</i>","Traveling To <i>(Use abreviations)</i>","Flight Nr","Flight Cost","Flight Time","Invoice(s)","VAT included");
	$fields["Flights"] = array("Flights_placeFrom","Flights_placeTo","Flights_otherDescr","Flights_rate","Flights_travelTime","Flights_invoices","Flights_VAT");
	$validation["Flights"] = array("","","","currency","","file","");

	$descr["Accommodation"] = array("Establishment","No of Nights","Rate","Invoice(s)","VAT included");
	$fields["Accommodation"] = array("Accommodation_otherDescr","Accommodation_total","Accommodation_rate","Accommodation_invoices","Accommodation_VAT");
	$validation["Accommodation"] = array("","number","currency required","file","");

	$descr["Toll Gate"] = array("Traveling From <i>(Use abreviations)</i>","Traveling To <i>(Use abreviations)</i>","Total $currency","Slip(s)","VAT included");
	$fields["Toll Gate"] = array("Toll_Gate_placeFrom","Toll_Gate_placeTo","Toll_Gate_expense","Toll_Gate_slips","Toll_Gate_VAT");
	$validation["Toll Gate"] = array("","","currency required","file","");
	//////////////////////////////////////////////////////

	$user_name = q("SELECT CONCAT(frstname,' ',lstname) FROM Employee WHERE id = '".$_SESSION["user_id"]."'");

	if (isset($_POST["btnUpdate"])) {
		unset($_POST["btnUpdate"]);

		$array = preg_split("/_/", addslashes(strip_tags($_POST["idBox"])));
		$arrayExpenseFields = preg_split("/_/", addslashes(strip_tags($_POST["idBox2"])));

		if (is_array($array))
			$array = array_slice($array, 0, count($array) - 1);
		if (is_array($arrayExpenseFields))
			$arrayExpenseFields = array_slice($arrayExpenseFields, 0, count($arrayExpenseFields) - 1);

		if (isset($timeArr))
			unset($timeArr);

		$timeArr[] = number_format("0.25", 2, ".", "");
		$timeArr[] = number_format("0.50", 2, ".", "");
		$timeArr[] = number_format("0.75", 2, ".", "");

		for ($i = 1; $i <= 12; $i += 0.5)
			$timeArr[] = number_format($i, 2, ".", "");

		foreach ($array as $a)  {
			$id = substr($a, 0, strpos($a, ":"));
			$locked = substr($a, (strpos($a, ":") + 1), strlen($a));

			if (is_numeric($id))    {
				if (addslashes(strip_tags($_POST["TIME".$id])) == "1")	{
					$date = addslashes(strip_tags($_POST["A".$id]));
					$descr = addslashes(strip_tags($_POST["B".$id]));
					$time = number_format(addslashes(strip_tags($_POST["C".$id])), 2, ".", "");

					if (submitDateCheck($date)) {
						if (in_array($time, $timeArr, true) && $locked == "0")  {
							$update = q("UPDATE TimeSheet SET date = '$date', descr = '$descr', time_spent = '$time' WHERE id = '$id'");
							$update = q("UPDATE ApprovedTime SET date = '$date', descr = '$descr', time_spent = '$time' WHERE timesheet_id = '$id'");
						}
					}
				}
			}
		}

		foreach ($arrayExpenseFields as $a)  {
			$id = substr($a, 0, strpos($a, ":"));
			$locked = substr($a, (strpos($a, ":") + 1), strlen($a));

			if (is_numeric($id))    {
				if (addslashes(strip_tags($_POST["EXPENSE".$id])) == "1")	{
					$date = addslashes(strip_tags($_POST["X".$id]));
					$descr = addslashes(strip_tags($_POST["Y".$id]));
					$val = number_format(addslashes(strip_tags($_POST["Z".$id])), 2, ".", "");

					if (submitDateCheck($date)) {
						if ($locked == "0")  {
							$update = q("UPDATE ExpenseSheet SET date = '$date', descr = '$descr', expense = '$val' WHERE id = '$id'");
							$update = q("UPDATE ApprovedExpense SET date = '$date', descr = '$descr', expense = '$val' WHERE expensesheet_id = '$id'");
						}
					}
				}
			}
		}

		if ($errorMessage == "")    header("Location: bookings.php");
	}

	if (isset($_POST["save"]) && $_POST["save"] === "1")    {
		$errorMessage = "";

		$today = date("Y-m-d");

		///////////////////////////
		$timeTime = date("H:i:s");
		$expenseTime = date("H:i:s");
		///////////////////////////

		///////////////////////////
		//  Get Information
		$project = $_POST["project"];
		$projectType = (substr($project,0,3) == "CP_") ? "CP" : "SP";
		$projectID = substr($project,3);
		///////////////////////////
		//  Time Fields
		$area = ($_POST["area"] != "") && is_numeric($_POST["area"]) ? addslashes(strip_tags($_POST["area"])) : 0;      // Check if any AREAs were posted
		$activityType = $_POST["activityType"];
		$activity = $_POST["activity"];
		$timeDescr = addslashes(strip_tags($_POST["timeDescr"]));
		$timeDate = addslashes(strip_tags($_POST["timeDate"]));
		$timeSpent = $_POST["timeSpent"];

		$timeRate = q("SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$_SESSION["user_id"]."' ".
					"AND pu.company_id = '".$_SESSION["company_id"]."' AND pu.project_id = '".$projectID."' AND ur.companyid = '".$_SESSION["company_id"]."' AND ur.userid = '".$_SESSION["user_id"]."'");
		//$tariff = "tariff".q("SELECT user_tariff FROM Project_User WHERE user_id = '".$_SESSION["user_id"]."' AND company_id = '".$_SESSION["company_id"]."' AND project_id = '".$projectID."'");
		//$timeRate = q("SELECT $tariff FROM Employee WHERE id = '".$_SESSION["user_id"]."'");
		///////////////////////////

		///////////////////////////
		//  Expense Fields
		$area2 = ($_POST["area2"] != "") && is_numeric($_POST["area2"]) ? addslashes(strip_tags($_POST["area2"])) : 0;      // Check if any AREAs were posted
		$activityType2 = $_POST["activityType2"];
		$activity2 = $_POST["activity2"];
		$expenseDescr = addslashes(strip_tags($_POST["expenseDescr"]));
		$expenseDate = addslashes(strip_tags($_POST["expenseDate"]));

		$expenseType = $_POST["expenseType"];
		$expenseTypeStr = "";

		$fields["Expense"] = array("expense");
		$fields["Driving"] = array("vehicle","placeFrom","placeTo","odoStart","odoFinish","businessKM","privateKM","kilometers");
		$fields["Printing"] = array("printing","total");
		$fields["Car Hire"] = array("otherDescr","rate");
		$fields["Flights"] = array("otherDescr","placeFrom","placeTo","rate");
		$fields["Accommodation"] = array("otherDescr","total","rate");
		$fields["Toll Gate"] = array("placeFrom","placeTo","expense");

		if (is_array($expenseTypes))
			foreach ($expenseTypes as $e)
				if ($e[0] == $expenseType)      $expenseTypeStr = $e[1];

		for ($i = 0; $i < count($fields[$expenseTypeStr]); $i++)        {
			$field = $fields[$expenseTypeStr][$i];

			if ($field == "expense" || $field == "placeFrom" || $field == "placeTo" || $field == "otherDescr" || $field == "total" || $field == "rate")
				$field = "".preg_replace("/ /", "_", $expenseTypeStr)."_".$field;

			$values[$i] = addslashes(strip_tags($_POST[$field]));

			if ($fields[$expenseTypeStr][$i] == "rate" && $expenseTypeStr == "Flights") {
				$rate = $values[$i];
				$values[$i] = number_format((double)$rate, 2, ".", "");
			}
		}

		if ($expenseTypeStr != "")      {
			//  Expense Type - Driving
			if ($expenseTypeStr == "Driving")   {
				$travelRate = q("SELECT dd.name FROM (Project AS p INNER JOIN dropdowns AS dd ON p.travelRate = dd.id) WHERE p.id = '".$projectID."'");
				$travelRate = ($travelRate == "0.5 x Professional Rate") ? 0.5 : (($travelRate == "1.0 x Professional Rate") ? 1 : 1);
				$travelTime = addslashes(strip_tags($_POST["Driving_travelTime"]));

				$fields["Driving"][] = "rate";
				$vehicleID = $values[array_search("vehicle", $fields["Driving"])];
				$values[] = q("SELECT rate FROM Vehicle WHERE id = '".$vehicleID."'");

				$fields["Driving"][array_search("vehicle", $fields["Driving"])] = "vehicle_id";

				$values[array_search("kilometers", $fields[$expenseTypeStr])] = $values[array_search("businessKM", $fields[$expenseTypeStr])];
			}
			//  Expense Type - Printing
			else if ($expenseTypeStr == "Printing")   {
				$fields["Printing"][] = "rate";
				$printingID = $values[array_search("printing", $fields["Printing"])];
				$values[] = q("SELECT cost FROM Disbursements WHERE id = '".$printingID."'");

				$fields["Printing"][array_search("printing", $fields["Printing"])] = "disbursement_id";
			}
			//  Expense Type - Flights
			else if ($expenseTypeStr == "Flights")   {
				$travelRate = q("SELECT dd.name FROM (Project AS p INNER JOIN dropdowns AS dd ON p.travelRate = dd.id) WHERE p.id = '".$projectID."'");
				$travelRate = ($travelRate == "0.5 x Professional Rate") ? 0.5 : (($travelRate == "1.0 x Professional Rate") ? 1 : 1);
				$travelTime = addslashes(strip_tags($_POST["Flights_travelTime"]));
			}

			//  Calculate Total Expense Values
			if ($expenseTypeStr == "Driving" || $expenseTypeStr == "Printing" || $expenseTypeStr == "Car Hire" || $expenseTypeStr == "Flights" || $expenseTypeStr == "Accommodation") $fields[$expenseTypeStr][] = "expense";

			if ($expenseTypeStr == "Driving")   {
				$values[] = $values[array_search("kilometers", $fields[$expenseTypeStr])] * $values[array_search("rate", $fields[$expenseTypeStr])];
			}
			else if ($expenseTypeStr == "Printing" || $expenseTypeStr == "Accommodation")   {
				$values[] = $values[array_search("total", $fields[$expenseTypeStr])] * $values[array_search("rate", $fields[$expenseTypeStr])];
			}
			else if ($expenseTypeStr == "Car Hire" || $expenseTypeStr == "Flights")   {
				$values[] = $values[array_search("rate", $fields[$expenseTypeStr])];
			}

			//  Round Numeric Fields To 2 Decimal Places
			$fieldPos = array_search("expense", $fields[$expenseTypeStr]);
			$values[$fieldPos] = number_format($values[$fieldPos], 2, ".", "");

			if ($expenseTypeStr == "Driving" || $expenseTypeStr == "Printing" || $expenseTypeStr == "Car Hire" || $expenseTypeStr == "Flights" || $expenseTypeStr == "Accommodation")   {
				$fieldPos = array_search("rate", $fields[$expenseTypeStr]);
				$values[$fieldPos] = number_format($values[$fieldPos], 2, ".", "");
			}

			if ($expenseTypeStr == "Printing" || $expenseTypeStr == "Accommodation")   {
				$fieldPos = array_search("total", $fields[$expenseTypeStr]);
				$values[$fieldPos] = number_format($values[$fieldPos], 2, ".", "");
			}

			if ($expenseTypeStr == "Driving")   {
				$fieldPos = array_search("kilometers", $fields[$expenseTypeStr]);
				$values[$fieldPos] = number_format($values[$fieldPos], 2, ".", "");
			}

			// - Check if VAT is included in amount - If so - Remove VAT from amount - //
			if(in_array("VAT included", $descr[$expenseTypeStr])) {
				if($_POST[preg_replace("/ /i", "_", $expenseTypeStr)."_VAT_radio"] == "yes") {
					$current_project_VAT = q("SELECT vat FROM companiesOnTeam WHERE project_id = '".$projectID."' AND company_id = '".$_SESSION['company_id']."' ");
					$current_project_VAT = ($current_project_VAT > 0) ? $current_project_VAT : "15";
					if(in_array("expense",$fields[$expenseTypeStr])) {
						$fieldPos = array_search("expense", $fields[$expenseTypeStr]);
						$values[$fieldPos] = (($values[$fieldPos] * 100)/($current_project_VAT + 100));
						$values[$fieldPos] = number_format($values[$fieldPos], 2, ".", "");
					}
					if(in_array("rate",$fields[$expenseTypeStr])) {
						$fieldPos = array_search("rate", $fields[$expenseTypeStr]);
						$values[$fieldPos] = (($values[$fieldPos] * 100)/($current_project_VAT + 100));
						$values[$fieldPos] = number_format($values[$fieldPos], 2, ".", "");
					}
				}
			}

			//  Create Queries
			$query1 = "INSERT INTO ExpenseSheet (company_id,projectType,date,user_id,project_id,area_id,activity_id,expense_type_id,descr,on_date,on_time";
			$query2 = "INSERT INTO ApprovedExpense (company_id,projectType,date,user_id,project_id,area_id,activity_id,expense_type_id,descr,on_date,on_time,expensesheet_id";

			//  Add Fields
			for ($i = 0; $i < count($fields[$expenseTypeStr]); $i++)        {
				$query1 .= ",".$fields[$expenseTypeStr][$i];
				$query2 .= ",".$fields[$expenseTypeStr][$i];
			}

			$query1 .= ") VALUES ('".$_SESSION["company_id"]."','".$projectType."','$expenseDate','".$_SESSION["user_id"]."','$projectID','$area2','$activity2','$expenseType','$expenseDescr','$today','$expenseTime'";
			$query2 .= ") VALUES ('".$_SESSION["company_id"]."','".$projectType."','$expenseDate','".$_SESSION["user_id"]."','$projectID','$area2','$activity2','$expenseType','$expenseDescr','$today','$expenseTime',".
						"(SELECT id FROM ExpenseSheet WHERE user_id = '".$_SESSION["user_id"]."' AND on_date = '$today' AND on_time = '$expenseTime')";

			//  Add Values
			for ($i = 0; $i < count($values); $i++)        {
				$query1 .= ",'".$values[$i]."'";
				$query2 .= ",'".$values[$i]."'";
			}

			$query1 .= ")";
			$query2 .= ")";
		}

		if (submitDateCheck($timeDate))   {
			if ($timeDate <= $today)    {
				if ($projectID != "" && $_SESSION["user_id"] != "" && $activity != "" && $timeDate != "" && $timeSpent != "")
				if ($projectID != "null" && $activity != "null" && $timeSpent != "null")
				if ($projectID != "0" && $_SESSION["user_id"] != "0" && $activity != "0" && $timeSpent != "0")   {
					$insert = q("INSERT INTO TimeSheet (company_id, projectType, date, user_id, project_id, area_id, activity_id, descr, rate, time_spent, on_date, on_time) VALUES ".
								"('".$_SESSION["company_id"]."','".$projectType."', '$timeDate', '".$_SESSION["user_id"]."', '$projectID', '$area', '$activity', '$timeDescr', '$timeRate', '$timeSpent', '$today', '$timeTime')");

					if ($insert)    {
							//$timeSheetID = q("SELECT id FROM TimeSheet WHERE user_id = '".$_SESSION["user_id"]."' AND on_date = '$today' AND on_time = '$timeTime'");
							$timeSheetID = q("SELECT id FROM TimeSheet ".
														"WHERE company_id = '".$_SESSION["company_id"]."' AND projectType = '".$projectType."' AND date = '".$timeDate."' AND user_id = '".$_SESSION["user_id"]."' ".
															"AND project_id = '".$projectID."' AND area_id = '".$area."' AND activity_id = '".$activity."' AND descr = '".$timeDescr."' AND rate = '".$timeRate."' ".
															"AND time_spent = '".$timeSpent."' AND on_date = '".$today."' AND on_time = '".$timeTime."'");

							$insert = q("INSERT INTO ApprovedTime (company_id,projectType, date, user_id, project_id, area_id, activity_id, descr, rate, time_spent, on_date, on_time, timesheet_id, ".
										"status) VALUES ('".$_SESSION["company_id"]."','".$projectType."', '$timeDate', '".$_SESSION["user_id"]."', '$projectID','$area', '$activity', '$timeDescr', '$timeRate', '$timeSpent', ".
										"'$today', '$timeTime',  '$timeSheetID', '0')");

						$projectIDName = q("SELECT name FROM Project WHERE id = '$projectID'");

						$logs = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) VALUES ".
									"('".$_SESSION["email"]." booked time on ".$projectIDName."', 'Insert', 'TimeSheet', '".$_SESSION["email"]."', '$today', '$timeTime', ".
									"'".$_SESSION["company_id"]."')");
					}
				}

				unset($insert);

				if ($expenseTypeStr != "")      {
					$projectIDName = q("SELECT name FROM Project WHERE id = '$projectID'");

					$insert = q($query1);

					if (($expenseTypeStr == "Driving" || $expenseTypeStr == "Flights") && $travelTime != "null")  {
						$insert = q("INSERT INTO TimeSheet (company_id,projectType,date,user_id,project_id,area_id,activity_id,descr,rate,time_spent,on_date,on_time,travelTime) VALUES ".
									"('".$_SESSION["company_id"]."','".$projectType."','$expenseDate','".$_SESSION["user_id"]."','$projectID','$area2','$activity2','$expenseDescr',".
										"'".number_format(($travelRate * $timeRate), 2, ".", "")."','$travelTime','$today','$expenseTime','1')");

						if ($insert)    {
							//$timeSheetID = q("SELECT id FROM TimeSheet WHERE user_id = '".$_SESSION["user_id"]."' AND on_date = '$today' AND on_time = '$expenseTime'");
							$timeSheetID = q("SELECT id FROM TimeSheet ".
														"WHERE company_id = '".$_SESSION["company_id"]."' AND projectType = '".$projectType."' AND date = '".$expenseDate."' AND user_id = '".$_SESSION["user_id"]."' ".
															"AND project_id = '".$projectID."' AND area_id = '".$area2."' AND activity_id = '".$activity2."' AND descr = '".$expenseDescr."' ".
															"AND rate = '".number_format(($travelRate * $timeRate), 2, ".", "")."' AND time_spent = '".$travelTime."' AND on_date = '".$today."' AND on_time = '".$expenseTime."'");
							$insert = q("INSERT INTO ApprovedTime (company_id,projectType,date,user_id,project_id,area_id,activity_id,descr,rate,time_spent,on_date,on_time,timesheet_id,status,travelTime) VALUES ".
										"('".$_SESSION["company_id"]."','".$projectType."','$expenseDate','".$_SESSION["user_id"]."','$projectID','$area2','$activity2','$expenseDescr',".
											"'".number_format(($travelRate * $timeRate), 2, ".", "")."','$travelTime','$today', '$expenseTime',  '$timeSheetID', '0','1')");

							$logs = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) VALUES ".
										"('".$_SESSION["email"]." booked time on ".$projectIDName."', 'Insert', 'TimeSheet', '".$_SESSION["email"]."', '$today', '$expenseTime', ".
										"'".$_SESSION["company_id"]."')");
						}
					}

					$expenseID = q("SELECT id FROM ExpenseSheet WHERE user_id = '".$_SESSION["user_id"]."' AND on_date = '$today' AND on_time = '$expenseTime'");

					if (is_numeric($expenseID) && ($expenseTypeStr != "Driving"))	{
						if ($expenseTypeStr == "Expense" || $expenseTypeStr == "Toll Gate" || $expenseTypeStr == "Printing")	{
							$field = "".preg_replace("/ /", "_", $expenseTypeStr)."_slips";
							$file_name = "Slips";
						} else	{
							$field = "".preg_replace("/ /", "_", $expenseTypeStr)."_invoices";
							$file_name = "Invoices";
						}

						if ($_POST[$field."_radio"] == "hardcopy" && $backing_documentation == "1"){
							if (!is_dir($file_directory.$expenseID))	mkdir($file_directory.$expenseID);

							copy($file_directory."Hard-copy.pdf", $file_directory.$expenseID."/Hard-copy.pdf");
						} else	{
							$count = 1;

							for ($i = 0; $i < $_POST[$field."_tableCntr"]; $i++)	{
								if ($_FILES[$field."_".$i]["name"] != "")	{
									if (!is_dir($file_directory.$expenseID))	mkdir($file_directory.$expenseID);

									$extension = substr($_FILES[$field."_".$i]["name"],strrpos($_FILES[$field."_".$i]["name"],"."));

									move_uploaded_file($_FILES[$field."_".$i]["tmp_name"], $file_directory.$expenseID."/".$file_name."_".$count.$extension);

									$count++;
								}
							}
						}
					}

					if ($insert)    {
						$insert = q($query2);

						$logs = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) VALUES ".
									"('".$_SESSION["email"]." booked expenses on ".$projectIDName."', 'Insert', 'ExpenseSheet', '".$_SESSION["email"]."', '$today', '$timeTime', '".$_SESSION["company_id"]."')");
					}
				}

				header("Location: bookings.php");
			}
			else
				$errorMessage = "Time/Expense cannot be booked in advance";
		}
	}

	//  Print Header
	print_header();
	//  Print Menu
	print_menus("0", "bookings", "bookings");

	if ($errorMessage != "")    {
		if ($errorMessage == "Please note that we are currently busy with data management, no entries are allowed for 2010-08-26. Please check back in 24 Hours.")
			echo "<p style='text-align:center'><font color='red'> - $errorMessage - </font></p>";
		else
			echo "<p style='text-align:center'><font class='on-validate-error'> - $errorMessage - </font></p>";
		echo "<br/>";
	}

	$projects = q("SELECT DISTINCT(p.id), p.name, p_type FROM (Project AS p INNER JOIN Project_User AS pu ON p.id = pu.project_id INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) ".
					"WHERE pu.user_id = '".$_SESSION["user_id"]."' AND cot.company_id = '".$_SESSION["company_id"]."' AND pu.company_id = '".$_SESSION["company_id"]."' AND p.completed = '0' AND p.deleted = '0' ".
					"AND pu.deleted = '0' ORDER BY UPPER(p.name)");

	//  Expenses
	//$vehicles = q("SELECT id, type, rate FROM Vehicle WHERE company_id = '".$_SESSION["company_id"]."' AND deleted = '0' ORDER BY type");
	$vehicles = q("SELECT v.id, v.type FROM Vehicle AS v INNER JOIN vehicle_user AS vu ON vu.vehicle_id = v.id ".
							"WHERE vu.user_id = '".$_SESSION["user_id"]."' AND v.company_id = '".$_SESSION["company_id"]."' AND v.deleted = '0'  ORDER BY v.type");
	$disbursementTypes = q("SELECT id, type FROM DisbursementTypes WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY type");
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
	var loadActivityTypes;
	var loadActivityTypes2;
	var loadActivities;
	var loadActivities2;
	var checkTimeBudget;

	var hasAreas = false;

	//Project Name Scroller
	var origText = "";
	var projectMask = 21;

	function is_array(input)    {
		return typeof(input)=='object'&&(input instanceof Array);
	}

	jQuery(function()    {
		//////////////////////////////
		//  Serve File
		//////////////////////////////
		if ("<?php echo isset($_POST["invoiceID"]); ?>" == "1")	{
			var invoiceID = "<?php echo $_POST["invoiceID"]; ?>";
			//console.log("ISSET ["+<?php //echo isset($_POST["invoiceID"]); ?>+"]\n");
			//console.log("INVOICE ID ["+<?php //echo $_POST["invoiceID"]; ?>+"]\n");
			jQuery("#secretIFrame").attr("src","serveFile.php?type=certificate_documentation&invoice="+invoiceID);
		}
		//////////////////////////////

		//////////////////////////////
		//  Load Areas
		loadAreas = function()  {
			var projectType = jQuery("#project").val().substring(0, 2);
			var projectID = jQuery("#project").val().substring(3);

			jQuery.post("_ajax.php", {func: "get_area_info", projectType: projectType, projectID: projectID}, function(data)	{
				data = json_decode(data);

				jQuery("#area option").remove();
				jQuery("#area2 option").remove();

				jQuery("#area").append(jQuery("<option></option>").attr("value","null").text("--  Select Area --"));
				jQuery("#area2").append(jQuery("<option></option>").attr("value","null").text("--  Select Area --"));

				if (is_array(data)) {   // Display area options
					hasAreas = true;

					jQuery.each(data,function(i, v)	{
						jQuery("#area").append(jQuery("<option></option>").attr("value",v[0]).text(v[1]));
						jQuery("#area2").append(jQuery("<option></option>").attr("value",v[0]).text(v[1]));
					});

					jQuery("#areaInfo1").show();
					jQuery("#areaInfo2").show();
					jQuery("#areaInfo3").show();
					jQuery("#areaInfo4").show();
				}
				else    {               // Do not display area options
					hasAreas = false;

					jQuery("#areaInfo1").hide();
					jQuery("#areaInfo2").hide();
					jQuery("#areaInfo3").hide();
					jQuery("#areaInfo4").hide();
				}
			});
		};

		//  Load Activity Types
		loadActivityTypes = function()  {
			var projectType = jQuery("#project").val().substring(0, 2);
			var projectID = jQuery("#project").val().substring(3);

			jQuery.post("_ajax.php", {func: "get_activity_types", projectType: projectType, projectID: projectID}, function(data)	{
				data = json_decode(data);

				jQuery("#activityType option").remove();
				jQuery("#activity option").remove();

				jQuery("#activityType").append(jQuery("<option></option>").attr("value","null").text("--  Select Activity Type  --"));

				if (is_array(data))
					jQuery.each(data,function(i, v)	{
						jQuery("#activityType").append(jQuery("<option></option>").attr("value",v[0]).text(v[1]));
					});

					jQuery("#activityType").val(<?php echo $_SESSION["default_activityType"];?>);

					loadActivities();
			});
		};

		//  Load Activity Types 2
		loadActivityTypes2 = function()  {
			var projectType = jQuery("#project").val().substring(0, 2);
			var projectID = jQuery("#project").val().substring(3);

			jQuery.post("_ajax.php", {func: "get_activity_types", projectType: projectType, projectID: projectID}, function(data)	{
				data = json_decode(data);

				jQuery("#activityType2 option").remove();
				jQuery("#activity2 option").remove();

				jQuery("#activityType2").append(jQuery("<option></option>").attr("value","null").text("--  Select Activity Type  --"));

				if (is_array(data))
					jQuery.each(data,function(i, v)	{
						jQuery("#activityType2").append(jQuery("<option></option>").attr("value",v[0]).text(v[1]));
					});

				jQuery("#activityType2").val(<?php echo $_SESSION["default_activityType"];?>);

				loadActivities2();
			});
		};

		//  Load Activities
		loadActivities = function()  {
			var projectType = jQuery("#project").val().substring(0, 2);
			var activityType = jQuery("#activityType").val();

			jQuery.post("_ajax.php", {func: "get_activities", projectType: projectType, activityType: activityType}, function(data)	{
				data = json_decode(data);

				jQuery("#activity option").remove();
				jQuery("#activity").append(jQuery("<option></option>").attr("value","null").text("--  Select Activity  --"));

				if (is_array(data)) {
					jQuery.each(data,function(i, v)	{
						jQuery("#activity").append(jQuery("<option></option>").attr("value",v[0]).text(v[1]));
					});
				}

				jQuery("#activity").val(<?php echo $_SESSION["default_activity"];?>);
			});
		};

		//  Load Activities 2
		loadActivities2 = function()  {
			var projectType = jQuery("#project").val().substring(0, 2);
			var activityType = jQuery("#activityType2").val();

			jQuery.post("_ajax.php", {func: "get_activities", projectType: projectType, activityType: activityType}, function(data)	{
				data = json_decode(data);

				jQuery("#activity2 option").remove();
				jQuery("#activity2").append(jQuery("<option></option>").attr("value","null").text("--  Select Activity  --"));

				if (is_array(data)) {
					jQuery.each(data,function(i, v)	{
						jQuery("#activity2").append(jQuery("<option></option>").attr("value",v[0]).text(v[1]));
					});
				}

				jQuery("#activity2").val(<?php echo $_SESSION["default_activity"];?>);
			});
		};

		//  Check Time/Budget
		checkTimeBudget = function()  {
			var projectID = jQuery("#project").val().substring(3);

			jQuery.post("_ajax.php", {func: "checkTimeBudget", projectID: projectID}, function(data)	{
				data = json_decode(data);

				if (!(data[0] == "N/A" || data[0] > 0))
					jQuery("#budgetExceeded").show();

				jQuery("#timeBudgetLeftDiv").show();
				jQuery("#timeBudgetLeft").text(data[1]);
			});
		};
		//////////////////////////////

		//////////////////////////////
		//  Date Picker(s)
		jQuery("#timeDate").DatePicker( {
			format:"Y-m-d",
			date: jQuery("#timeDate").val(),
			current: jQuery("#timeDate").val(),
			starts: 1,
			position: "right",
			onBeforeShow: function()    {
				var _date = (!validation("date", jQuery("#timeDate").val())) ? new Date(): jQuery("#timeDate").val();
				jQuery("#timeDate").DatePickerSetDate(_date, true);
				jQuery("#expenseDate").DatePickerSetDate(_date, true);
			},
			onChange: function(formated, dates) {
				jQuery("#timeDate").val(formated);
				jQuery("#timeDate").DatePickerHide();
				jQuery("#expenseDate").val(formated);
				jQuery("#expenseDate").DatePickerHide();
			}
		});

		jQuery("#expenseDate").DatePicker( {
			format:"Y-m-d",
			date: jQuery("#expenseDate").val(),
			current: jQuery("#expenseDate").val(),
			starts: 1,
			position: "right",
			onBeforeShow: function()    {
				var _date = (!validation("date", jQuery("#expenseDate").val())) ? new Date(): jQuery("#expenseDate").val();
				jQuery("#expenseDate").DatePickerSetDate(_date, true);
			},
			onChange: function(formated, dates) {
				jQuery("#expenseDate").val(formated);
				jQuery("#expenseDate").DatePickerHide();
			}
		});
		//////////////////////////////

		//////////////////////////////
		//  Calculate Difference
		jQuery("#odoStart, #odoFinish").change(function()    {
			var odoStart = jQuery("#odoStart").val();
			var odoFinish = jQuery("#odoFinish").val();

			if (odoStart != "" && odoFinish != "" && validation("currency", odoStart) && validation("currency", odoFinish))   {
				var total = parseFloat(odoFinish).toFixed(2) - parseFloat(odoStart).toFixed(2);

				jQuery("#kilometers").val(total);
				jQuery("#businessKM").val(total);
				jQuery("#privateKM").val("0");
			}
		});

		jQuery("#businessKM").change(function()    {
			var odoStart = jQuery("#odoStart").val();
			var odoFinish = jQuery("#odoFinish").val();
			var businessKM = jQuery("#businessKM").val();
			var kilometers = jQuery("#kilometers").val();

			if (odoStart != "" && odoFinish != "" && businessKM != "" && kilometers != ""
				&& validation("currency", odoStart) && validation("currency", odoFinish) && validation("currency", businessKM) && validation("currency", kilometers))   {
				var total = parseFloat(kilometers).toFixed(2) - parseFloat(businessKM).toFixed(2);

				jQuery("#privateKM").val(total);
			}
			else if (businessKM != "" && validation("currency", businessKM))      {
				jQuery("#kilometers").val(parseFloat(businessKM).toFixed(2));
			}
		});

		jQuery("#privateKM").change(function()    {
			var odoStart = jQuery("#odoStart").val();
			var odoFinish = jQuery("#odoFinish").val();
			var businessKM = jQuery("#businessKM").val();
			var privateKM = jQuery("#privateKM").val();
			var kilometers = jQuery("#kilometers").val();

			if (odoStart != "" && odoFinish != "" && businessKM != "" && kilometers != ""
				&& validation("currency", odoStart) && validation("currency", odoFinish) && validation("currency", businessKM) && validation("currency", kilometers))   {
				var odoKMs = parseFloat(odoFinish).toFixed(2) - parseFloat(odoStart).toFixed(2);
				var businessKM = parseFloat(odoKMs).toFixed(2) - parseFloat(privateKM).toFixed(2);

				jQuery("#businessKM").val(businessKM);
			}
			else if (businessKM != "" && privateKM != "" && validation("currency", businessKM) && validation("currency", privateKM))   {
				var total = 0;
				total += parseFloat(businessKM).toFixed(2) * 1;
				total += parseFloat(privateKM).toFixed(2) * 1;

				jQuery("#kilometers").val(parseFloat(total).toFixed(2));
			}
			else if (privateKM != "" && validation("currency", privateKM))      {
				jQuery("#kilometers").val(parseFloat(privateKM).toFixed(2));
			}
		});

		//////////////////////////////

		//////////////////////////////
		//  Project Change - Show/Hide
		jQuery("#project").change(function()    {
			jQuery("#budgetExceeded").hide();

			if (jQuery(this).val() != "null")    {
				jQuery("#info").show();
				jQuery("#project").focus();

				checkTimeBudget();
				loadAreas();
				loadActivityTypes();
				loadActivityTypes2();
			}
			else    {
				jQuery("#timeBudgetLeftDiv").hide();
				jQuery("#info").hide();
				jQuery("#project").focus();
			}
		});
		//////////////////////////////

		//////////////////////////////
		//  Area Change
		jQuery("#area").change(function()    {
			jQuery("#area2").val(jQuery(this).val()).attr("selected", true);
		});

		//  Activity Type Change
		jQuery("#activityType").change(function()    {
			jQuery("#activityType2").val(jQuery(this).val()).attr("selected", true);

			loadActivities();
			loadActivities2();
		});

		//  Activity Change
		jQuery("#activity").change(function()    {
			jQuery("#activity2").val(jQuery(this).val()).attr("selected", true);
		});

		jQuery("#activityType2").change(function()    {
			loadActivities2();
		});

		// WATCH FOR CHANGE
		jQuery(".watch-for-change").change(function()	{
			var field = jQuery(this).attr("id").substr(0,1);
			var id = jQuery(this).attr("id").substr(1);

			if (field == "A" || field == "B" || field == "C")
				jQuery("#TIME"+id).val("1");
			else
				jQuery("#EXPENSE"+id).val("1");
		});
		//////////////////////////////

		//////////////////////////////
		//  Book Button Pressed
		jQuery("#btnAdd").click(function()    {
			jQuery("#btnAdd").attr("disabled","true");

			var i = 0;
			var j;
			var valid = true;
			var test;

			var fields = [];

			fields[i++] = "project";

			//  Time Fields
			if (hasAreas)       var timeFields = new Array("area", "activityType","activity","timeDescr","timeDate","timeSpent");
			else                var timeFields = new Array("activityType","activity","timeDescr","timeDate","timeSpent");

			//  Expense Fields
			if (hasAreas)   {
				var ExpenseFields = new Array("area2","activityType2","activity2","expenseType","expenseDescr","expenseDate","Expense_expense","Expense_slips");
				var DrivingFields = new Array("area2","activityType2","activity2","expenseType","vehicle","expenseDescr","expenseDate","Driving_placeFrom","Driving_placeTo","odoStart","odoFinish","businessKM","privateKM",
												"kilometers","Driving_travelTime");
				var PrintingFields = new Array("area2","activityType2","activity2","expenseType","printingType","printing","expenseDescr","expenseDate","Printing_total","Printing_slips");
				var Car_HireFields = new Array("area2","activityType2","activity2","expenseType","expenseDescr","expenseDate","Car_Hire_otherDescr","Car_Hire_rate","Car_Hire_invoices");
				var FlightsFields = new Array("area2","activityType2","activity2","expenseType","expenseDescr","expenseDate","Flights_placeFrom","Flights_placeTo","Flights_otherDescr","Flights_rate","Flights_travelTime",
												"Flights_invoices");
				var AccommodationFields = new Array("area2","activityType2","activity2","expenseType","expenseDescr","expenseDate","Accommodation_otherDescr","Accommodation_total","Accommodation_rate","Accommodation_invoices");
				var Toll_GateFields = new Array("area2","activityType2","activity2","expenseType","expenseDescr","expenseDate","Toll_Gate_placeFrom","Toll_Gate_placeTo","Toll_Gate_expense","Toll_Gate_slips");
			} else      {
				var ExpenseFields = new Array("activityType2","activity2","expenseType","expenseDescr","expenseDate","Expense_expense","Expense_slips");
				var DrivingFields = new Array("activityType2","activity2","expenseType","vehicle","expenseDescr","expenseDate","Driving_placeFrom","Driving_placeTo","odoStart","odoFinish","businessKM","privateKM",
												"kilometers","Driving_travelTime");
				var PrintingFields = new Array("activityType2","activity2","expenseType","printingType","printing","expenseDescr","expenseDate","Printing_total","Printing_slips");
				var Car_HireFields = new Array("activityType2","activity2","expenseType","expenseDescr","expenseDate","Car_Hire_otherDescr","Car_Hire_rate","Car_Hire_invoices");
				var FlightsFields = new Array("activityType2","activity2","expenseType","expenseDescr","expenseDate","Flights_placeFrom","Flights_placeTo","Flights_otherDescr","Flights_rate","Flights_travelTime",
												"Flights_invoices");
				var AccommodationFields = new Array("activityType2","activity2","expenseType","expenseDescr","expenseDate","Accommodation_otherDescr","Accommodation_total","Accommodation_rate","Accommodation_invoices");
				var Toll_GateFields = new Array("activityType2","activity2","expenseType","expenseDescr","expenseDate","Toll_Gate_placeFrom","Toll_Gate_placeTo","Toll_Gate_expense","Toll_Gate_slips");
			}

			// Time, expense or both
			var timeBook = false;
			var expenseBook = false;

			//  Add Time Fields
			if(jQuery("#activityType").val() != "null")      for (j = 0; j < timeFields.length; j++)  {fields[i++] = timeFields[j];  }

			//  Add Expense Fields
			if (jQuery("#expenseType").val() != "null")      {
				var tmpArr = jQuery("#expenseType").find("option:selected").text().replace(/ /, "_")+"Fields";

				eval("for(j = 0; j < "+tmpArr+".length; j++)  {fields[i++] = "+tmpArr+"[j];  }");
			}

			for (i = 0; i < fields.length; i++)  {
				var field = fields[i];
				var cls = jQuery("#"+field).attr("class");

				cls = (cls != null) ? cls.split(" ") : "";

				for (j = 0; j < cls.length; j++)  {
					var divName = (cls[j] == "entered") ? "#"+field+"Empty" : ((cls[j] == "file") ? "#"+field+"File" : "#"+field+"Div");
					test = validation(cls[j], jQuery("#"+field).val());

					if (!test)  jQuery(divName).show();
					else        jQuery(divName).hide();

					valid &= test;
				}
			}

			if (jQuery("#expenseType").find("option:selected").text() == "Driving")	{
				//  Check That ODO Start Is Smaller Than ODO Finish
				var odoStart = jQuery("#odoStart").val() * 1;
				var odoFinish = jQuery("#odoFinish").val() * 1;

				if (validation("currency", odoStart) && validation("currency", odoFinish))	{
					if (odoStart > odoFinish)	{
						jQuery("#odoFinishDiv").html("* ODO Finish must be greater that ODO Start").show();
						valid &= false;
					}
					else
						jQuery("#odoFinishDiv").html("* Numeric value, eg. 11.40").hide();
				}
			}

			var expenseType = jQuery("#expenseType").find("option:selected").text();

			if (expenseType == "Expense" || expenseType == "Car Hire" || expenseType == "Flights" || expenseType == "Accommodation" || expenseType == "Toll Gate" || expenseType == "Printing")	{
				if (expenseType == "Expense" || expenseType == "Toll Gate" || expenseType == "Printing")
					var field = expenseType.replace(/ /, "_")+"_slips";
				else
					var field = expenseType.replace(/ /, "_")+"_invoices";

				if (jQuery("#"+field+"_radio:checked").val() == "attachment")	{
					for (i = 0; i < jQuery("#"+field+"_tableCntr").val(); i++)	{
						var cls = jQuery("#"+field+"_"+i).attr("class");

						cls = (cls != null) ? cls.split(" ") : "";

						for (j = 0; j < cls.length; j++)  {
							var divName = (cls[j] == "entered") ? "#"+field+"_"+i+"Empty" : ((cls[j] == "file") ? "#"+field+"_"+i+"File" : "#"+field+"_"+i+"Div");
							test = validation(cls[j], jQuery("#"+field+"_"+i).val());

							if (!test)  jQuery(divName).show();
							else        jQuery(divName).hide();

							valid &= test;
						}
					}
				}
			}

	<?php
	if ($sars_compliance == "1")	{
	?>
			if (jQuery("#expenseType").find("option:selected").text() == "Driving")	{
				//  Check That ODO Start Is Smaller Than ODO Finish
				var odoStart = jQuery("#odoStart").val() * 1;
				var odoFinish = jQuery("#odoFinish").val() * 1;
				var businessKM = jQuery("#businessKM").val() * 1;
				var privateKM = jQuery("#privateKM").val() * 1;
				var totalKms1 = (odoFinish - odoStart);
				var totalKms2 = (businessKM + privateKM);

				if (validation("currency", odoStart) && validation("currency", odoFinish) && validation("currency", businessKM) && validation("currency", privateKM))	{
					if (totalKms1 < totalKms2 && totalKms1 != totalKms2)	{
						jQuery("#privateKMDiv").html("* Business & Private KM's cannot be greater than Total KM's").show();

						valid &= false;
					}
					else
						jQuery("#privateKMDiv").html("* Numeric value, eg. 11.40").hide();
				}
			}
	<?php
	}

	if ($backing_documentation == "1")	{
	?>
			var expenseType = jQuery("#expenseType").find("option:selected").text();

			if (expenseType == "Expense" || expenseType == "Car Hire" || expenseType == "Flights" || expenseType == "Accommodation" || expenseType == "Toll Gate" || expenseType == "Printing")	{
				if (expenseType == "Expense" || expenseType == "Toll Gate" || expenseType == "Printing")
					var field = expenseType.replace(/ /, "_")+"_slips";
				else
					var field = expenseType.replace(/ /, "_")+"_invoices";

				if (jQuery("#"+field+"_radio:checked").val() == "attachment")	{
					var count = 0;

					for (i = 0; i < jQuery("#"+field+"_tableCntr").val(); i++)	{
						var file = "" + jQuery("#"+field+"_"+i).val();

						if (file != "" && file != "undefined")	count++;
					}

					if (count == 0)	{
						jQuery("#"+field+"Div").show();;

						valid &= false;
					}
					else
						jQuery("#"+field+"Div").hide();
				}
			}
	<?php
	}
	?>

			if (valid) {
				jQuery("#save").val("1");
				jQuery("#bookings").submit();
			} else
				jQuery("#btnAdd").removeAttr("disabled");
		});
		//////////////////////////////

		//////////////////////////////
		var ua = jQuery.browser;

		if (ua.mozilla || ua.msie) {
			jQuery.each(jQuery(".expenseDiv2"),function(index, value) {
				jQuery(value).children(0).eq(0).width("100%");
			});

			jQuery.each(jQuery(".expenseDiv2 .on-field"),function(index, value) {
				jQuery(value).parent().width("50%");
			});

			jQuery.each(jQuery(".expenseDiv2 .on-field-date"),function(index, value) {
				jQuery(value).parent().width("50%");
			});
		}
		//////////////////////////////

		//////////////////////////////
		//  Expense Type Selection
		jQuery(".radio-select").change(function()     {
			var option = jQuery(this).val();

			if (option == "attachment")	jQuery("#"+jQuery(this).attr("id")+"DIV").show();
			else						jQuery("#"+jQuery(this).attr("id")+"DIV").hide();
		});

		jQuery("#expenseType").change(function()     {
			var divName = jQuery(this).find("option:selected").text().replace(/ /, "_");

			jQuery(".expenseDiv1").hide();
			jQuery(".expenseDiv2").hide();

			if (divName == "Driving" || divName == "Printing")   jQuery("#"+divName+"Div").show();

			jQuery("#"+divName).show();
		});

		jQuery(".addRow").click(function()    {
			var tbl = "#"+jQuery(this).attr("href");
			var tblCntr = "#"+jQuery(this).attr("href")+"Cntr";

			var lstRow = jQuery(tbl + " tbody>tr:last");
			var counter = jQuery(tblCntr).val();
			var n = lstRow.clone(true);

			var id_name = n.children().eq(0).children().eq(0).attr("id");
			id_name = id_name.substring(0, id_name.lastIndexOf('_') + 1) + counter;

			n.children().eq(0).children().eq(0).attr("id", id_name);
			n.children().eq(0).children().eq(0).attr("name", id_name);
			n.children().eq(0).children().eq(0).attr("value", "");
			n.children().eq(0).children().eq(1).attr("id", id_name+"File");
			n.children().eq(0).children().eq(1).attr("name", id_name+"File");

			n.insertAfter(lstRow);
			counter++;

			lstRow.children().eq(1).children().eq(0).addClass("hidden");
			lstRow.children().eq(1).children().eq(1).removeClass("hidden");

			jQuery(tblCntr).val(counter);

		});

		jQuery(".removeRow").click(function()    {
			jQuery(this).parent().parent().remove();
		});

		//////////////////////////////
	});

	//  Sajax
	<?php sajax_show_javascript(); ?>

	///////////////////////////
	//  Sajax
	function ClearOptions(OptionList) {
		for (x = OptionList.length; x >= 0; x--)        OptionList[x] = null;
	}

	function setPrinting(data) {
		var c = 0;

		for (var i in data) {
			var display = data[i][1];
			display = display.replace(new RegExp(/\'/g), "");
			eval("document.bookings.printing.options[" + (c++) + "] = new Option('" + display + "', '" + data[i][0] + "');");
		}
	}

	function getPrinting(select) {
		ClearOptions(document.bookings.printing);

		if (select.value != "null")     x_getPrinting(select.value, setPrinting);
	}
	///////////////////////////
</script>
<table width="100%">
	<tr height="380px">
		<td class="centerdata" valign="top">
			<form action="" id="bookings" name="bookings" method="post" enctype="multipart/form-data">
				<table width="100%">
					<tr>
						<td class="centerdata">
							<h6>
								Time &amp; Expense Bookings
							</h6>
						</td>
					</tr>
				</table>
				<br/>
				<div id="budgetExceeded" name="budgetExceeded" style="display: none;">
					<center>
						<font class='on-validate-error'>
							- Budget For Project Has Been Reached/Exceeded, Please Notify Your Project Manager -
						</font>
					</center>
					<br/>
				</div>
				<div id="timeBudgetLeftDiv" name="timeBudgetLeftDiv" style="display: none;">
					<table width="100%">
						<tr>
							<td class="on-description" width="50%">
								Time/Budget Left:
							</td>
							<td id="timeBudgetLeft" name="timeBudgetLeft" width="50%" class="on-description-left on-validate-error"></td>
						</tr>
					</table>
				</div>
				<table width="90%">
					<tr>
						<td class="on-description" width="50%">
								Project Name:
						</td>
						<td style="padding-top:2px;" width="50%">
							<select id="project" name="project" method="post" class="on-field required" tabindex="<?php echo $index++; ?>">
								<option value="null">--  Select A Project  --</option>
								<?php
									if (is_array($projects))    {
										foreach ($projects as $project) {
											$value = ($project[2] == "CP") ? "CP_".$project[0]: "SP_".$project[0];

											echo "<option value='".$value."'>".$project[1]."</option>";
										}
									}
								?>
							</select>
							<div id="projectDiv" name="projectDiv" class="error">* Project must be selected</div>
						</td>
					</tr>
				</table>
				<br/>
				<div id="info" name="info" style="display: none;">
					<table width="90%">
						<tr>
							<td class="centerdata" valign="top" style="padding-top:4px;" width="45%">
								<h6>
									Time Information
								</h6><br>
								<table width="100%">
									<tr>
										<td class="on-description" width="50%">
											<div id="areaInfo1" name="areaInfo1" style="display:none;">
												Area:
											</div>
										</td>
										<td>
											<div id="areaInfo2" name="areaInfo2" style="display:none;">
												<select id="area" name="area" method="post" class="on-field required" tabindex="<?php echo $index++; ?>">
													<option value="null">--  Select A Area  --</option>
													<?php
														if (is_array($areas))
															foreach ($areas as $area)
																echo "<option value='".$area[0]."'>".$area[1]."</option>";
													?>
												</select>
												<div id="areaDiv" name="areaDiv" class="error">* Area must be selected</div>
											</div>
										</td>
									</tr>
									<tr>
										<td class="on-description" width="50%">
											Activity Type:
										</td>
										<td width="50%">
											<select id="activityType" name="activityType" method="post" class="on-field required" tabindex="<?php echo $index++; ?>">
											</select>
											<div id="activityTypeDiv" name="activityTypeDiv" class="error">* Activity type must be selected</div>
										</td>
									</tr>
									<tr>
										<td class="on-description" width="50%">
											Activity:
										</td>
										<td width="50%">
										   <select id="activity" name="activity" method="post" class="on-field required" tabindex="<?php echo $index++; ?>"></select>
											<div id="activityDiv" name="activityDiv" class="error">* Activity must be selected</div>
										</td>
									</tr>

									<tr>
										<td class="on-description" width="50%">
											Description:
										</td>
										<td width="50%">
										   <textarea id="timeDescr" name="timeDescr" cols="50" rows="6" class="on-field maxLength" tabindex="<?php echo $index++; ?>"></textarea>
											<div id="timeDescrDiv" name="timeDescrDiv" class="error">* Description may not be longer that 250 characters</div>
										</td>
									</tr>
									<tr>
										<td class="on-description" width="50%">
											Date:
										</td>
										<td width="50%">
											<input id="timeDate" name="timeDate" tabindex="<?php echo $index++; ?>" class="entered required date on-field-date" type="text" style="text-align:right;" value="<?php
												echo "".$today; ?>" />
											<div id="timeDateEmpty" name="timeDateEmpty" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
											<div id="timeDateDiv" name="timeDateDiv" class="error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></div>
										</td>
									</tr>
									<tr>
										<td class="on-description" width="50%">
												Time Spent:
										</td>
										<td  width="50%">
											<select id="timeSpent" name="timeSpent" method="post" class="on-field-date required" tabindex="<?php echo $index++; ?>">
												<option value="null"></option>
												<option value="0.25">0.25</option>
												<option value="0.50">0.50</option>
												<option value="0.75">0.75</option>
												<?php
													for ($i = 1; $i <= 12; $i += 0.5)   echo "<option value='".number_format($i, 2)."'>".number_format($i, 2, ".", "")."</option>";
												?>
											</select>
											<div id="timeSpentDiv" name="timeSpentDiv" class="error">* Time spent must be selected</div>
										</td>
									</tr>
								</table>
							</td>
							<td class="centerdata" style="padding-top:4px;">
								<h6>
									Expense Information
								</h6><br>
								<table width="100%">
									<tr>
										<td class="on-description">
											<div id="areaInfo3" name="areaInfo3" style="display:none;">
												Area:
											</div>
										</td>
										<td>
											<div id="areaInfo4" name="areaInfo4" style="display:none;">
												<select id="area2" name="area2" method="post" class="on-field required" tabindex="<?php echo $index++; ?>">
													<option value="null">--  Select A Area  --</option>
													<?php
														if (is_array($areas))
															foreach ($areas as $area)
																echo "<option value='".$area[0]."'>".$area[1]."</option>";
													?>
												</select>
												<div id="area2Div" name="area2Div" class="error">* Area must be selected</div>
											</div>
										</td>
									</tr>
									<tr>
										<td class="on-description" width="50%">
											Activity Type:
										</td>
										<td width="50%">
											<select id="activityType2" name="activityType2" method="post" class="on-field required" tabindex="<?php echo $index++; ?>">
											</select>
											<div id="activityType2Div" name="activityType2Div" class="error">* Activity type must be selected</div>
										</td>
									</tr>
									<tr>
										<td class="on-description" width="50%">
											Activity:
										</td>
										<td width="50%">
											<select id="activity2" name="activity2" method="post" class="on-field required" tabindex="<?php echo $index++; ?>"></select>
											<div id="activity2Div" name="activity2Div" class="error">* Activity must be selected</div>
										</td>
									</tr>
									<tr>
										<td class="on-description" width="50%">
												Expense Type:
										</td>
										<td width="50%">
											<select id="expenseType" name="expenseType" method="post" class="on-field required" tabindex="<?php echo $index++; ?>">
												<option value="null">--  Select Expense Type  --</option>
												<?php
												// onChange="setInfo(this);"
													if (is_array($expenseTypes))
														foreach ($expenseTypes as $expenseType)
															echo "<option value='".$expenseType[0]."'>".$expenseType[1]."</option>";
												?>
											</select>
											<div id="expenseTypeDiv" name="expenseTypeDiv" class="error">* Expense type must be selected</div>
										</td>
									</tr>
								</table>
								<?php
									if (is_array($expenseTypes))        {
										foreach ($expenseTypes as $expenseType) {
											if ($expenseType[1] == "Driving")       {
											?>
												<div id="DrivingDiv" name="DrivingDiv" style="display: none;" class="expenseDiv1">
													<table width="100%">
														<tr>
															<td class="on-description">
																Vehicle:
															</td>
															<td width="50%">
																<select id="vehicle" name="vehicle" method="post" class="on-field required" tabindex="<?php echo $index++; ?>">
																<option value="null">--  Select A Vehicle  --</option>
																<?php
																	if (is_array($vehicles))
																		foreach ($vehicles as $vehicle)
																			if (count($vehicles) == 1)
																				echo "<option value='".$vehicle[0]."' selected>".$vehicle[1]."</option>";
																			else
																				echo "<option value='".$vehicle[0]."' ".(($_SESSION["default_vehicle"] == $vehicle[0]) ? "selected" : "").">".$vehicle[1]."</option>";
																?>
																</select>
																<div id="vehicleDiv" name="vehicleDiv" class="error">* Vehicle must be selected</div>
															</td>
														</tr>
													</table>
												</div>
											<?php
											} else if ($expenseType[1] == "Printing")   {
											?>
												<div id="PrintingDiv" name="PrintingDiv" style="display: none;" class="expenseDiv1">
													<table width="100%">
														<tr>
															<td class="on-description">
																Printing Type:
															</td>
															<td width="50%">
																<select id="printingType" name="printingType" onChange="getPrinting(this);" method="post" class="on-field required" tabindex="<?php echo $index++; ?>">
																<option value="null">--  Printing Type  --</option>
																<?php
																	if (is_array($disbursementTypes))
																		foreach ($disbursementTypes as $disbursementType)
																			echo "<option value='".$disbursementType[0]."'>".$disbursementType[1]."</option>";
																?>
																</select>
																<div id="printingTypeDiv" name="printingTypeDiv" class="error">* Printing Type must be selected</div>
															</td>
														</tr>
														<tr>
															<td class="on-description">
																Colour:
															</td>
															<td width="50%">
																<select id="printing" name="printing" method="post" class="on-field required" tabindex="<?php echo $index++; ?>"></select>
																<div id="printingDiv" name="printingDiv" class="error">* Colour must be selected</div>
															</td>
														</tr>
													</table>
												</div>
											<?php
											}
										}
									}
								?>
								<table width="100%">
									<tr>
										<td class="on-description">
											Description:
										</td>
										<td width="50%">
											<textarea id="expenseDescr" name="expenseDescr" class="on-field maxLength" cols="50" rows="6" tabindex="<?php echo $index++; ?>"></textarea>
											<div id="expenseDescrDiv" name="expenseDescrDiv" class="error">* Description may not be longer that 250 characters</div>
										</td>
									</tr>
									<tr>
										<td class="on-description" width="50%">
											Date:
										</td>
										<td width="50%">
											<input id="expenseDate" name="expenseDate" class="entered required date on-field-date" tabindex="<?php echo $index++; ?>" type="text" style="text-align:right;" value="<?php
												echo "".$today; ?>" />
											<div id="expenseDateEmpty" name="expenseDateEmpty" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
											<div id="expenseDateDiv" name="expenseDateDiv" class="error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></div>
										</td>
									</tr>
								</table>
								<?php
									if (is_array($expenseTypes))        {
										foreach ($expenseTypes as $expenseType) {
										?>
											<div id="<?php echo preg_replace("/ /", "_", $expenseType[1]); ?>" name="<?php echo preg_replace("/ /", "_", $expenseType[1]); ?>" style="display: none;" class="expenseDiv2">
												<table width="100%">
												<?php
													if (is_array($descr[$expenseType[1]]))       {
														for ($i = 0; $i < count($descr[$expenseType[1]]); $i++) {
														?>
															<tr>
																<td style="white-space:nowrap" class="on-description<?php if ($descr[$expenseType[1]][$i] == "VAT included") echo "-orange"; ?>">
																	<?php echo $descr[$expenseType[1]][$i]; ?>:
																</td>
																<td width="50%">
																<?php
																	if ($descr[$expenseType[1]][$i] != "Travel Time" &&
																		$descr[$expenseType[1]][$i] != "Flight Time" &&
																		$descr[$expenseType[1]][$i] != "Slip(s)" &&
																		$descr[$expenseType[1]][$i] != "Invoice(s)" &&
																		$descr[$expenseType[1]][$i] != "VAT included") {

																		if ($descr[$expenseType[1]][$i] == "Establishment" ||
																			$descr[$expenseType[1]][$i] == "Flight Nr" ||
																			$descr[$expenseType[1]][$i] == "Company"||
																			$descr[$expenseType[1]][$i] == "Traveling From" ||
																			$descr[$expenseType[1]][$i] == "Traveling To") {

																			$align = "left";
																		}else {
																			$align = "right";
																		}

																		$cls = (strpos($validation[$expenseType[1]][$i], "number") !== false || strpos($validation[$expenseType[1]][$i], "currency") !== false) ? "on-field-date": "on-field";
																		$readonly = ($descr[$expenseType[1]][$i] == "Total KM's") ? "readonly" : "";
																		echo "<input id='".$fields[$expenseType[1]][$i]."' name='".$fields[$expenseType[1]][$i]."' style='text-align:$align;' $readonly class='".$cls." ".$validation[$expenseType[1]][$i]."' tabindex='".$index++."' type='text' value=''>";

																		if (strpos($validation[$expenseType[1]][$i], "number") !== false)
																			echo "<div id='".$fields[$expenseType[1]][$i]."Div' name='".$fields[$expenseType[1]][$i]."Div' class='error'>* Numeric value, eg. 15885</div>";
																		else if (strpos($validation[$expenseType[1]][$i], "currency") !== false)
																			echo "<div id='".$fields[$expenseType[1]][$i]."Div' name='".$fields[$expenseType[1]][$i]."Div' class='error'>* Numeric value, eg. 11.40</div>";
																	} else if ($descr[$expenseType[1]][$i] == "Slip(s)" || $descr[$expenseType[1]][$i] == "Invoice(s)")	{
																		echo "<p style='margin-top:-5px;'>&nbsp;</p>";
																		echo "<input id='".$fields[$expenseType[1]][$i]."_radio' name='".$fields[$expenseType[1]][$i]."_radio' style='margin-left:5px;' type='radio' ".
																				"class='radio-select' value='hardcopy' checked><a>&nbsp;Submitted Hardcopy</a><br/>";
																		echo "<input id='".$fields[$expenseType[1]][$i]."_radio' name='".$fields[$expenseType[1]][$i]."_radio' style='margin-left:5px;' type='radio' ".
																				"class='radio-select' value='attachment'><a>&nbsp;Attaching ".$descr[$expenseType[1]][$i]."</a><br/>";

																		echo "<div id='".$fields[$expenseType[1]][$i]."_radioDIV' name='".$fields[$expenseType[1]][$i]."_radioDIV' class='hidden'>";
																			echo "<table id='".$fields[$expenseType[1]][$i]."_table' name='".$fields[$expenseType[1]][$i]."_table'>";
																				for ($j = 0; $j < 1; $j++)	{
																					echo "<tr>";
																						echo "<td>";
																							echo "<input id='".$fields[$expenseType[1]][$i]."_".$j."' name='".$fields[$expenseType[1]][$i]."_".$j."' style='text-align:left;' ".
																									"class='on-field ".$validation[$expenseType[1]][$i]."' tabindex='".$index."' type='file' value=''>";
																							echo "<div id='".$fields[$expenseType[1]][$i]."_".$j."File' name='".$fields[$expenseType[1]][$i]."_".$j."File' class='error'>".
																									"* ".$descr[$expenseType[1]][$i]." need to be of type (jpg/png/pdf)</div>";
																						echo "</td>";
																						echo "<td>";
																							echo "<input style='margin-left:5px;margin-top:9px;' class='addRow' ".
																									"href='".$fields[$expenseType[1]][$i]."_table' type='button' value='+' tabindex='".$index."' />";
																							echo "<input style='margin-left:5px;margin-top:9px;' class='removeRow hidden' ".
																									"href='".$fields[$expenseType[1]][$i]."_table' type='button' value='-' tabindex='".$index++."' />";
																						echo "</td>";
																					echo "</tr>";
																				}
																			echo "</table>";
																		echo "</div>";

																		echo "<input id='".$fields[$expenseType[1]][$i]."_tableCntr' name='".$fields[$expenseType[1]][$i]."_tableCntr' type='hidden' value='".$j."'>";

																		echo "<div id='".$fields[$expenseType[1]][$i]."Div' name='".$fields[$expenseType[1]][$i]."Div' class='error'>* ".$descr[$expenseType[1]][$i]." need to be attached</div>";
																	}else if ($descr[$expenseType[1]][$i] == "VAT included")	{
																		echo "<p style='margin-top:-5px;'>&nbsp;</p>";
																		echo "<input id='".$fields[$expenseType[1]][$i]."_radio' name='".$fields[$expenseType[1]][$i]."_radio' style='margin-left:5px;' type='radio' ".
																				"class='radio-select' value='yes' checked><a>&nbsp;Yes</a><br/>";
																		echo "<input id='".$fields[$expenseType[1]][$i]."_radio' name='".$fields[$expenseType[1]][$i]."_radio' style='margin-left:5px;' type='radio' ".
																				"class='radio-select' value='no'><a>&nbsp;No</a><br/>";
																	}else {
																		echo "<select onChange='avgSpeed()' id='".$fields[$expenseType[1]][$i]."' name='".$fields[$expenseType[1]][$i]."' method='post' ".
																				"class='on-field-date ".$validation[$expenseType[1]][$i]."' tabindex='".$index++."'>";
																			echo "<option value='null'></option>";
																			echo "<option value='0.25'>0.25</option>";
																			echo "<option value='0.50'>0.50</option>";
																			echo "<option value='0.75'>0.75</option>";

																			for ($j = 1; $j <= 12; $j += 0.5)   echo "<option value='".number_format($j, 2)."'>".number_format($j, 2, ".", "")."</option>";
																		echo "</select>";
																		echo "<div id='".$fields[$expenseType[1]][$i]."Div' name='".$fields[$expenseType[1]][$i]."Div' class='error'>* Travel time must be selected</div>";
																	}
																?>
																</td>
															</tr>
														<?php
														}

														if ($expenseType[1] == "Driving")        {
														?>
															<tr>
																<td style="white-space:nowrap" class="on-description">
																	Average Speed:
																</td>
																<td class="on-field-avg" width="50%">
																	<script type="text/javascript">
																		function avgSpeed()     {
																			var time = jQuery("#Driving_travelTime").val();
																			var km = jQuery("#kilometers").val();
																			var avg = 0.00;

																			if ((km > 0) && (time > 0)) {
																				avg = km/time;
																				avg = Math.round(avg);

																				jQuery("#theAvg").html(""+avg+" km/h");
																			} else      jQuery("#theAvg").html("0");
																		}
																	</script>
																	<font>
																		<span name="theAvg" id="theAvg">
																			0
																		</span>
																	</font>
																</td>
															</tr>
														<?php
														}
													}
												?>
												</table>
											</div>
										<?php
										}
									}
								?>
							</td>
						</tr>
					</table>
					<br/>
					<input id="btnAdd" name="btnAdd" type="button" tabindex="<?php echo $index++; ?>" value="Book Time/Expense" />
				</div>
				<br/><br/>
				<input id="save" name="save" method="post" type="hidden" value="0" />
				<br/>
				<?php
					$user_name = q("SELECT CONCAT(frstname, ' ', lstname) FROM Employee WHERE id = '".$_SESSION['user_id']."' ");
					$weekStart = currentWeekStart();
					$date = $today;

					$finalDisplay = "";
					$display = "";

					 ///////////////////////////////
					$hasAreas = 0;                                               // Does Areas exist for the user in expense or time sheet table

					$areasTime                                                     = q("SELECT project_id, area_id FROM TimeSheet WHERE user_id = ".$_SESSION["user_id"]." AND area_id > 0 AND date BETWEEN '".$weekStart."' AND '".$today."'");
					$areasExpense                                                = q("SELECT project_id, area_id FROM ExpenseSheet WHERE  user_id = ".$_SESSION["user_id"]." AND area_id > 0 AND date BETWEEN '".$weekStart."' AND '".$today."'");

					if(is_array($areasTime) || is_array($areasExpense)) {                              // Time
						$hasAreas = 1;
					}
				////////////////////////////////
				$extraCounter = 0;
				if($hasAreas){
					$areaData = "<th>Area</th>";
					$extraCounter  = 1;
				}else{
					$areaData = "";
					$extraCounter  = 0;
				}

					$printed_headings = 0;

						//  Daily Summary - Time Booked
						$headings = "<tr>".
										"<th>Date</th>".
										"<th>Project Name</th>".
										$areaData.
										"<th>Activity Type</th>".
										"<th>Activity</th>".
										"<th>Description</th>".
										"<th>&nbsp;</th>
										<th>Time</th>".
									"</tr>";

					$dayHours = 0;
					$totalHours = 0;
					$timeDisplay = "";

					while ($date >= $weekStart)    {
						$dayHours = 0;
						$timeDisplay = "";

						$timeSummary = q("SELECT ts.id,ts.projectType,ts.project_id,ts.area_id,ts.activity_id,ts.descr,ts.time_spent,ts.locked FROM (Project AS p
											INNER JOIN Project_User AS pu ON p.id = pu.project_id
											INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
											INNER JOIN TimeSheet AS ts ON ts.project_id = p.id)
											WHERE pu.user_id = '".$_SESSION["user_id"]."'
												AND ts.company_id = '".$_SESSION["company_id"]."'
												AND cot.company_id = '".$_SESSION["company_id"]."'
												AND pu.company_id = '".$_SESSION["company_id"]."'
											AND ts.user_id = '".$_SESSION["user_id"]."'
											AND p.deleted = '0'
											AND pu.deleted = '0'
											AND ts.date = '$date'
											ORDER BY ts.on_date, ts.on_time");

						if (is_array($timeSummary))   {
							foreach ($timeSummary as $s) {
									$project = q("SELECT name FROM Project WHERE id = '".$s[2]."'") ;

									$idBox .= "".$s[0].":".$s[7]."_";
									$dayHours += $s[6];
									$totalHours += $s[6];

									$readonly = ($s[7] != "0") ? "readonly" : "";

									if ($s[1] == "CP")  {
										///////////////
										$activityType = q("SELECT type FROM ActivityTypes WHERE id = (SELECT parent_id FROM Activities WHERE id = '".$s[4]."')");
										$activity = q("SELECT name FROM Activities WHERE id = '".$s[4]."'");
										if($hasAreas){
											$area = q("SELECT name FROM areas WHERE id = ".$s[3]." ");
											if($s[3] != 0){
												$aData = "<td style='white-space:nowrap'>".$area."</td>";
											}else{
												$aData = "<td>-</td>";
											}
										}else{
											$aData = "";
										}
										/////////////////
									}
									else { // if Shared Project
										/////////////////
										$activityType = q("SELECT type FROM ActivityTypes WHERE id = (SELECT parent_id FROM Activities WHERE id = '".$s[4]."')");
										$activity = q("SELECT name FROM Activities WHERE id = '".$s[4]."'");
										if($hasAreas){
											$area = q("SELECT name FROM areas WHERE id = ".$s[3]." ");
											if($s[3] != 0){
												$aData = "<td style='white-space:nowrap'>".$area."</td>";
											}else{
												$aData = "<td style='white-space:nowrap'>-</td>";
											}
										}else{
											$aData = "";
										}
										/////////////////
									}

								$timeDisplay .= "<tr>".
													"<td>
														<input id='TIME".$s[0]."' name='TIME".$s[0]."' type='hidden' value='0'>
														<input id='A".$s[0]."' name='A".$s[0]."' type='text' size='12' $readonly  value='".$date."' tabindex='".$index."' class='watch-for-change'>
													</td>
													<td><div class='projectReportName'>".$project."</div></td>".
													$aData.
													"<td style='white-space:nowrap'>".$activityType."</td>
													<td style='white-space:nowrap'>".$activity."</td>
													<td>
														<input id='B".$s[0]."' name='B".$s[0]."' type='text' size='50' $readonly value='".str_replace("'", "&#39;" , $s[5])."' tabindex='".$index."' class='watch-for-change'>
													</td>
													<td>
														&nbsp;
													</td>
													<td>
														<input id='C".$s[0]."' class='number watch-for-change' name='C".$s[0]."' type='text' size='12' $readonly value='".number_format($s[6], 2, ".", "")."' tabindex='".$index."'>
													</td>
												</tr>";
							}

							if($printed_headings == 1)
								$headings = "";

							$display .= $headings.$timeDisplay.
										"<tr>".
											"<td class='on-table-total'></td>
											<td colspan='".(5+$extraCounter)."' class='on-table-total'>Total:</td>
											<td class='on-table-total'>".number_format($dayHours, 2, ".", "")."</td>
										</tr>";
							$printed_headings = 1;
						}
						$date = getDates($date, -1);
					}

					if ($display != "")   {
						$display .= "<tr>".
										"<td class='on-table-total'></td>
										<td colspan='".(5+$extraCounter)."' class='on-table-total'>Grand Total:</td>
										<td class='on-table-total'>".number_format($totalHours, 2, ".", "")."</td>
										</tr>
									</tr>";
					}
					if ($display != "")
						$finalDisplay .=  "<tr><td class='on-table-clear' colspan='6".+$extraCounter."'><a>Booked <font class='on-validate-error'>Time</font> Information: ".$user_name."</a></td></tr>".$display;

	////////////////////////////// EXPENSE /////////////////////////////////////
					//  Daily Summary - Expenses Booked
					$index++;
					$display = "";

				////////////////////////////////
				$extraCounter = 0;
				if($hasAreas){
					$areaData = "<th>Area</th>";
					$extraCounter  = 1;
				}else{
					$areaData = "";
					$extraCounter  = 0;
				}
					$printed_headings2 = 0;

					$headings = "<tr>".
									"<th>Date</th>".
									"<th>Project Name</th>".
									$areaData.
									"<th>Expense Type - Expense Info</th>".
									"<th>Activity Type - Activity</th>".
									"<th>Description</th>".
									"<th>Amount</th>".
									"<th>Expense $currency</th>".
								"</tr>";

					$weekStart = currentWeekStart();
					$date = $today;
					$dayExpenses = 0;
					$totalExpenses = 0;
					$expenseDisplay = "";

					while ($date >= $weekStart)    {
						$dayExpenses = 0;
						$expenseDisplay = "";

						$expenseSummary = q("SELECT es.projectType,es.project_id,es.area_id,es.activity_id,es.expense_type_id,v.type,d.parent_id,d.name,es.descr,es.otherDescr,es.kilometers,es.total,es.expense,es.id,es.locked ".
											"FROM (Project AS p INNER JOIN Project_User AS pu ON p.id = pu.project_id INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id INNER JOIN ".
												"ExpenseSheet AS es ON es.project_id = p.id ".
												"LEFT JOIN Vehicle AS v ON v.id = es.vehicle_id ".
												"LEFT JOIN Disbursements AS d ON d.id = es.disbursement_id) ".
											"WHERE pu.user_id = '".$_SESSION["user_id"]."' AND cot.company_id = '".$_SESSION["company_id"]."' AND pu.company_id = '".$_SESSION["company_id"]."' ".
												"AND es.company_id = '".$_SESSION["company_id"]."' AND es.user_id = '".$_SESSION["user_id"]."' AND es.date = '$date' ".
											"ORDER BY es.expense_type_id, v.type, d.name, p.name");

						if (is_array($expenseSummary))   {
							foreach ($expenseSummary as $s) {
								$project = q("SELECT name FROM Project WHERE id = '".$s[1]."'") ;

								$idBox2 .= "".$s[13].":".$s[14]."_";
								$dayExpenses += $s[12];
								$totalExpenses += $s[12];

								$readonly = ($s[14] != "0") ? "readonly" : "";

								$expenseType = q("SELECT name FROM dropdowns WHERE id = '".$s[4]."'");
								$expenseDescr = ($expenseType == "Driving") ? $s[5] : (($expenseType == "Printing") ? q("SELECT type FROM DisbursementTypes WHERE id = '".$s[6]."'")." [".$s[7]."]" :
												(($expenseType == "Accommodation" || $expenseType == "Flights" || $expenseType == "Car Hire") ? $s[9] : ""));

								$expenseDescr = ($expenseDescr != "") ? $expenseType." - ".$expenseDescr : $expenseType;

								if ($s[0] == "CP")  {
									//////////////////////////
										$activityType2 = q("SELECT type FROM ActivityTypes WHERE id = (SELECT parent_id FROM Activities WHERE id = '".$s[3]."')");
										$activity2 = q("SELECT name FROM Activities WHERE id = '".$s[3]."'");
										if($hasAreas){
											$area = q("SELECT name FROM areas WHERE id = ".$s[2]." ");
											if($s[2] != 0){
												$aData = "<td style='white-space:nowrap'>".$area."</td>";
											}else{
												$aData = "<td style='white-space:nowrap'>-</td>";
											}
										}else{
											$aData = "";
										}
									/////////////////////////
								}
								else {       // if Shared Project
									////////////////////////////
									$activityType2 = q("SELECT type FROM ActivityTypes WHERE id = (SELECT parent_id FROM Activities WHERE id = '".$s[3]."')");
									$activity2 = q("SELECT name FROM Activities WHERE id = '".$s[3]."'");
									if($hasAreas){
										$area = q("SELECT name FROM areas WHERE id = ".$s[2]." ");
										if($s[2] != 0){
											$aData = "<td>".$area."</td>";
										}else{
											$aData = "<td>-</td>";
										}
									}else{
										$aData = "";
									}
									/////////////////////////////
								}

								$activitiesDescr = $activityType2." - ".$activity2;

								if ($s[10].$s[11] == "") $s[10] = " - ";

								$expenseDisplay .= "<tr>".
														"<td>".
															"<input id='EXPENSE".$s[13]."' name='EXPENSE".$s[13]."' type='hidden' value='0'>".
															"<input id='X".$s[13]."' name='X".$s[13]."' type='text' size='12' $readonly  value='".$date."' class='watch-for-change'>".
														"</td>".
														"<td><div class='projectReportName'>".$project.
														((hasBackingDocuments($s[13])) ? " <img onClick='window.open(\"serveFile.php?type=files&expense=".$s[13]."\")' class='hand' title='Click to download backing document(s)' src='images/icons/download.png'/>" : "").
														"</div></td>".
														$aData.
														 "<td>".$expenseDescr."</td>".
														"<td>".$activitiesDescr."</td>".
														"<td><input id='Y".$s[13]."' name='Y".$s[13]."' type='text' size='50' $readonly  value='".str_replace("'", "&#39;" , $s[8])."' class='watch-for-change'></td>
														<td class='rightdata'>".$s[10].$s[11]."</td>
														<td class='rightdata'>";
								$isEditable = q("SELECT name FROM dropdowns WHERE id = '".$s[4]."' ");
								if($isEditable == "Expense" || $isEditable == "Car Hire" || $isEditable == "Flights" ||$isEditable == "Toll Gate"){
									$expenseDisplay .= "<input id='Z".$s[13]."' class='number watch-for-change' name='Z".$s[13]."' type='text' size='12' $readonly value='".number_format($s[12], 2, ".", "")."' tabindex='".$index."'>";
								}else{
									$expenseDisplay .= "<input id='Z".$s[13]."' class='number' name='Z".$s[13]."' type='text' size='12' readonly  value='".number_format($s[12], 2, ".", "")."' tabindex='".$index."'>";
								}
								$expenseDisplay .= "</td>".
															"</tr>";
							}

							if($printed_headings2 == 1)
								$headings = "";

							$displayFoot = "";

							$display .= $headings.$expenseDisplay.
										"<tr>
											<td colspan='".(6+$extraCounter)."' class='on-table-total'>Total $currency:</td>
											<td class='on-table-total'>".number_format($dayExpenses, 2, ".", "")."</td>
										</tr>";
							$printed_headings2 = 1;
						}
						$date = getDates($date, -1);
					}

					if ($display != "")   {
						$display .= "<tr>".
										"<td colspan='".(6+$extraCounter)."' class='on-table-total'>Grand Total $currency:</td>".
										"<td class='on-table-total'>".number_format($totalExpenses, 2, ".", "")."</td>
									</tr>
									<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
					}

					///// -- BLANK TABLE ROW -- /////
					if($printed_headings == 1 && $printed_headings2 == 1){
								$displayBlock = "<tr><td style='background-color:transparent' colspan='100%'><br></td></tr>";
					}

					if ($display != "")
						$finalDisplay .=  $displayBlock."<tr><td class='on-table-clear' align='center' colspan='6".+$extraCounter."'><a>Booked <font class='on-validate-error'>
														Expenses</font> Information: ".$user_name."</a></td></tr>".$display;

					if ($finalDisplay != "")    {
						echo "<div class='on-20px'><h6>Bookings for this week</h6><table class='on-table on-table-center'>$finalDisplay</table></div>";
						echo "<table class='on-table-center'><tr><td><input id='btnUpdate' name='btnUpdate' type='submit' value='Apply Changes' tabindex='".++$index."'></tr></td></table>";
						echo "<input method='post' name='idBox' type='hidden' value='".$idBox."' />";
						echo "<input method='post' name='idBox2' type='hidden' value='".$idBox2."' />";
					}
				?>
			</form>
		</td>
	</tr>
	<tr>
		<td align="center">
			<br/>
		</td>
	</tr>
</table>
<iframe id="secretIFrame" src="" style="display:none; visibility:hidden;"></iframe>
<?php
	//  Print Footer
	print_footer();
?>
