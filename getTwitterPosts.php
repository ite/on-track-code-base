<?php
//	source: http://www.eccemedia.com/blog/blog.html&blogid=7

$twitterRssFeedUrl = "https://api.twitter.com/1/statuses/user_timeline.rss?screen_name=ite_ontrack";
$twitterUsername = "ite_ontrack";
$amountToShow = 20;
$twitterPosts = false;
$xml = @simplexml_load_file($twitterRssFeedUrl);
if(is_object($xml)){
    foreach($xml->channel->item as $twit){
        if(is_array($twitterPosts) && count($twitterPosts)==$amountToShow)
            break;
        $d['title'] = stripslashes(htmlentities($twit->title,ENT_QUOTES,'UTF-8'));
        $description = stripslashes(htmlentities($twit->description,ENT_QUOTES,'UTF-8'));
		//	If you want to only allow posts made by UserName and no retweets etc, uncomment the 2 lines below
//        if(strtolower(substr($description,0,strlen($twitterUsername))) == strtolower($twitterUsername))
//            $description = substr($description,strlen($twitterUsername)+1);
        $d['description'] = $description;
        $d['date'] = date("Y-m-d h:i:s",strtotime($twit->pubDate));
        $d['guid'] = stripslashes(htmlentities($twit->guid,ENT_QUOTES,'UTF-8'));
        $d['link'] = stripslashes(htmlentities($twit->link,ENT_QUOTES,'UTF-8'));
        $twitterPosts[]=$d;
    }
}else{
    die('cannot connect to twitter feed');
}

if(is_array($twitterPosts))
    foreach($twitterPosts as $post)	{
		$exists = q("SELECT id,id FROM TwitterPosts WHERE date = '".$post["date"]."'");
		if (!is_array($exists))	{
			q("INSERT INTO TwitterPosts (title,descr,date,guid,link) VALUES
				('".$post["title"]."','".$post["description"]."','".$post["date"]."','".$post["guid"]."','".$post["link"]."')");
//			echo "Post on '".$post["date"]."' inserted<br>\n";
		}
	}
?>
