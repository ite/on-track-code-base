<?php
    //  Get Recipients Function
    function getRecipients($code, $company_id, $user_id = "")
    {
	$users                                                          = q("SELECT e.frstname, e.lstname, e.email FROM ((Employee AS e ".
                                                                            "INNER JOIN Association AS a ON e.id = a.user_id) INNER JOIN Notify AS n ".
                                                                            "ON a.notify_id = n.id) WHERE n.code = '$code' AND a.company_id = '$company_id' ".
                                                                            "AND e.id != '$user_id' AND e.deleted = '0'");

	$recipients                                                     = "";

	if (is_array($users)) {
            foreach ($users as $user) {
                $usr                                                    = "".$user[0]." ".$user[1]." <".$user[2].">";

                if ($usr == "Johan Coertzen <jc@ite.co.za>")
                    $recipients                                         .= ",Johan Coertzen <jc@iteonline.co.za>";
                else if ($usr == "Franszo Faul <ffaul@ite.co.za>")
                    $recipients                                         .= ",Franszo Faul <ffaul@iteonline.co.za>";
                else if ($usr == "Mauritz Kruger <mkruger@ite.co.za>")
                    $recipients                                         .= ",Mauritz Kruger <mk@iteonline.co.za>";
                else if ($usr == "JP de Beer <jpdb@ite.co.za>")
                    $recipients                                         .= ",JP de Beer <jpdb@iteonline.co.za>";
                else
                    $recipients                                         .= ",".$usr;
            }
        }

	if ($recipients[0] == ",")
            $recipients                                                 = substr($recipients, 1);

	return $recipients;
    }

    //  Actioned Events Function
    function actionedEvents($company_id)
    {
        $events                                                         = q("SELECT n.code, n.descr, e.message, e.id, e.user_id FROM (Events AS e ".
                                                                            "INNER JOIN Notify AS n ON e.notify_id = n.id) WHERE e.processed = '0' ".
                                                                            "AND e.company_id = '$company_id'");

        if (is_array($events)) {
            foreach ($events as $event) {
                $to                                                     = getRecipients($event[0], $company_id, $event[4]);
                $subject                                                = "On Track - ".$event[1];
                $message                                                = "".$event[2];

                $from                                                   = "On Track Notification <support@integrityengineering.co.za>";
                $reply_to                                               = "On Track Support <support@integrityengineering.co.za>";

                ///////////////////////////
                //  Budget Mailing Function
                ///////////////////////////
                if ($event[0] == "Budget") {
                    $semi_rand                                          = md5(time());
                    $mime_boundary                                      = "==Multipart_Boundary_x{$semi_rand}x";

                    $reply_to                                           .= "\nMIME-Version: 1.0\n".
                                                                            "Content-Type: multipart/mixed;\n".
                                                                            " boundary=\"{$mime_boundary}\"";

                    // File Type
                    $fileatt_type                                       = "application/vnd.ms-excel";
                    // Filename that will be used for the file as the attachment
                    $fileatt_name                                       = "".date("Ymd")."_-_Budget_Report.xls";

                    $data                                               = chunk_split(base64_encode($message));

                    $message                                            = "--{$mime_boundary}\n".
                                                                            "Content-Type:text/html; charset=\"iso-8859-1\"\n".
                                                                            "Content-Transfer-Encoding: 7bit\n\n".
                                                                            "See attached file for information...\n\n";
                    $message                                            .= "--{$mime_boundary}\n".
                                                                            "Content-Type: {$fileatt_type};\n".
                                                                            " name=\"{$fileatt_name}\"\n".
                                                                            "Content-Transfer-Encoding: base64\n\n".
                                                                            $data."\n\n".
                                                                            "--{$mime_boundary}\n";

                    unset($data);
                    unset($fileatt_type);
                    unset($fileatt_name);
                }
                ///////////////////////////

                if ($to != "")
                    sendmail($to, $subject, $message, $from, $reply_to);

                $update                                         = q("UPDATE Events SET processed = 1 WHERE id = '$event[3]'");
            }
        }

        return;
    }

    ///////////////////////////
    //  Non-Actioned Events
    ///////////////////////////
    function nonActionedEvents($company_id)
    {
        $fromDate                                                       = getDates(date("Y-m-d"), -1);

        //$testDays                                                       = 3;
        $testDays                                                       = q("SELECT notifyPeriod FROM Company WHERE id = '".$company_id."'");

        $from                                                           = "On Track Notification <support@integrityengineering.co.za>";
        $reply_to                                                       = "On Track Support <support@integrityengineering.co.za>";

        $employees                                                      = q("SELECT e.id, e.frstname, e.lstname, e.email FROM (Employee AS e INNER JOIN Company_Users ".
                                                                            "AS cu ON e.id = cu.user_id) WHERE cu.company_id = '$company_id' AND e.email != 'admin' ".
                                                                            "AND e.deleted = '0' AND e.demo_user = '0' AND e.company_id = '$company_id'");

        if (is_array($employees)) {
            foreach ($employees as $employee) {
                $mail                                                   = 0;

                $employee_email                                         = "".$employee[1]." ".$employee[2]." <".$employee[3].">";

                if ($employee_email == "Johan Coertzen <jc@ite.co.za>")
                    $employee_email                                     = "Johan Coertzen <jc@iteonline.co.za>";
                else if ($employee_email == "Franszo Faul <ffaul@ite.co.za>")
                    $employee_email                                     = "Franszo Faul <ffaul@iteonline.co.za>";
                else if ($employee_email == "Mauritz Kruger <mkruger@ite.co.za>")
                    $employee_email                                     = "Mauritz Kruger <mk@iteonline.co.za>";
                else if ($employee_email == "JP de Beer <jpdb@ite.co.za>")
                   $employee_email                                      = "JP de Beer <jpdb@iteonline.co.za>";
                else
                    $employee_email                                     = "".$employee_email;

                $date_last_booked                                       = q("SELECT MAX(date) FROM TimeSheet WHERE user_id = '".$employee[0]."'");

                if ($date_last_booked != "")        {
                    if ((businessdays($date_last_booked, $fromDate) - totalPublicHolidays($date_last_booked, $fromDate)) > $testDays && !(checkLeave($date_last_booked, $fromDate, $company_id, $employee[0])))     {
                        $mail                                           = 1;

                        $to                                             = getRecipients("Bookings", $company_id);

                        if ($to != "")
                            $to                                         = $employee_email.",".$to;
                        else
                            $to                                         = $employee_email;

                        $subject                                        = "On Track - Employee(s) Behind Schedule {".$employee[1]." ".$employee[2]."}";
                        $message                                        = "".$employee[1]." ".$employee[2]."

You are currently behind schedule with your timesheet(s) and your project manager(s) have been informed.

To book your time now, go to http://live.on-track.ws/

Regards,
On Track Notification";
                    }
                }

                if ($employee[3] == "cvivier@ages-group.com" || $employee[3] == "amelia@ages-group.com" || $employee[3] == "ckriek@ages-group.com" || $employee[3] == "spotgieter@ages-group.com" || $employee[3] == "deon@lemeg.com")
                    $mail = 0;
                
                if ($employee[3] == "joseph@shconsulting.co.za" && date('D') !== "Mon") {
                    $mail = 0;
                }

                if ($mail == 1 && $to != "")
                    sendmail($to, $subject, $message, $from, $reply_to);
            }
        }

        $message                                                        = "Employees that are behind schedule has been notified";
        $notify_id                                                      = q("SELECT id FROM Notify WHERE code = 'Bookings'");
        $events                                                         = q("INSERT INTO Events (date, time, message, notify_id, processed, user_id, company_id) ".
                                                                            "VALUES ('".date("Y-m-d")."', '".date("H:i:s")."', '$message', '$notify_id', '1', '1', '$company_id')");

        return;
    }

    ///////////////////////////
    //  SLA Limit - Daily
    ///////////////////////////
    function slaLimitDaily($company_id)
    {
        $month_start                                                    = date("Y-m-01");
        $today                                                          = date("Y-m-d");

        $from                                                           = "On Track Notification <support@integrityengineering.co.za>";
        $reply_to                                                       = "On Track Support <support@integrityengineering.co.za>";

        $projects                                                       = q("SELECT id, name FROM Project WHERE company_id = '$company_id' AND completed = '0'");

        $to                                                             = getRecipients("SLA", $company_id);
        $subject                                                        = "On Track - Service Level Agreement(s)";

        $notify_id                                                      = q("SELECT id FROM Notify WHERE code = 'SLA'");

        if (is_array($projects)) {
            foreach ($projects as $project) {
                $already_sent                                           = q("SELECT id FROM Events WHERE message LIKE '".addslashes(strip_tags($project[1]))."%' AND date >= '$month_start' ".
                                                                            "AND date <= '$today' AND notify_id = '$notify_id'");

                if (!$already_sent)
                {
                    $limit                                              = q("SELECT time_limit FROM SLA WHERE project_id = '$project[0]'");
                    $used                                               = q("SELECT SUM(time_spent) FROM TimeSheet WHERE project_id = '$project[0]' ".
                                                                            "AND date >= '$month_start' AND date <= '$today'");

                    if ($limit <= $used && $limit != "")
                    {
                        $message                                        = "".$project[1]." has reached its SLA limit.\n\nLimit:\t".$limit."\nUsed:\t\t".$used;

			if ($to != "")
                            sendmail($to, $subject, $message, $from, $reply_to);

                        $insert                                         = q("INSERT INTO Events (date, time, message, notify_id, processed, user_id, company_id) ".
                                                                            "VALUES ('".date("Y-m-d")."', '".date("H:i:s")."', '$message', '$notify_id', '1', '1', ".
                                                                            "'$company_id')");
                    }
                }
            }
        }

        return;
    }

    ///////////////////////////
    //  SLA Limit - Monthly
    ///////////////////////////
    function slaLimitMonthly($company_id)
    {
        //  Set Table Border & Background Details
        $top                                                            = "border-top: 1px solid #336699;";
        $left                                                           = "border-left: 1px solid #336699;";
        $right                                                          = "border-right: 1px solid #336699;";
        $bottom                                                         = "border-bottom: 1px solid #336699;";
        $background                                                     = "background-color:#6699CC;";

        $displayString                                                  = "";

        $headers                                                        = "<tr><td align='center' style='".$top.$left.$bottom.$background."'><a>&nbsp;Project Name&nbsp;".
                                                                            "</a></td><td align='center' style='".$top.$left.$bottom.$background."'><a>&nbsp;Allowed".
                                                                            "&nbsp;</a></td><td align='center' style='".$top.$left.$right.$bottom.$background."'>".
                                                                            "<a>&nbsp;Used&nbsp;</a></td></tr>";

        $month_start                                                    = date("Y-m-01");
        $month_end                                                      = getMonthEnd($month_start);

        $from                                                           = "On Track Notification <support@integrityengineering.co.za>";
        $reply_to                                                       = "On Track Support <support@integrityengineering.co.za>";

        $projects                                                       = q("SELECT id, name FROM Project WHERE company_id = '$company_id' AND completed = '0' ".
                                                                            "ORDER BY name");

        $to                                                             = getRecipients("SLA", $company_id);
        $subject                                                        = "On Track - Service Level Agreement(s)";

        $notify_id                                                      = q("SELECT id FROM Notify WHERE code = 'SLA'");

        if (is_array($projects)) {
            foreach ($projects as $project) {
                $already_sent                                           = q("SELECT id FROM Events WHERE message LIKE '".addslashes(strip_tags($project[1]))."%' AND date >= '$month_start' ".
                                                                            "AND date <= '$month_end' AND notify_id = '$notify_id'");

                if ($already_sent) {
                    $limit                                              = q("SELECT time_limit FROM SLA WHERE project_id = '$project[0]'");
                    $used                                               = q("SELECT SUM(time_spent) FROM TimeSheet WHERE project_id = '$project[0]' ".
                                                                            "AND date >= '$month_start' AND date <= '$month_end'");

                    if ($limit <= $used && $limit != "")
                        $displayString                                  .= "<tr><td align='left' style='".$left.$bottom."'><a>&nbsp;".$project[1]."&nbsp;".
                                                                            "</a></td><td align='right' style='".$left.$bottom."'>".$limit."</td>".
                                                                            "<td align='right' style='".$left.$right.$bottom."'>".$used."</td></tr>";
                }
            }
        }

        if ($displayString != "")
            $displayString                                              = $headers.$displayString;

        $file_name                                                      = "".date("Ymd")."_-_SLA_Report.xls";
        $displayString                                                  = "header('Expires: 0');".
                                                                            "header('Cache-Control: must-revalidate, post-check=0, pre-check=0');".
                                                                            "header('Content-Type: application/vnd.ms-excel');".
                                                                            "header('Content-Disposition: attachment; filename=".$file_name."');".
                                                                            "<html><head><title>Excel Spreadsheet</title><meta http-equiv='Content-Type' ".
                                                                            "content='text/html; charset=iso-8859-1'></head><body><table cellpadding='0' ".
                                                                            "cellspacing='0'>".$displayString."</table></body></html>";

        $message                                                        = "".$displayString;

        ///////////////////////////
        $semi_rand                                                      = md5(time());
        $mime_boundary                                                  = "==Multipart_Boundary_x{$semi_rand}x";

        $reply_to                                                       .= "\nMIME-Version: 1.0\n".
                                                                            "Content-Type: multipart/mixed;\n".
                                                                            " boundary=\"{$mime_boundary}\"";

        // File Type
        $fileatt_type                                                   = "application/vnd.ms-excel";
        // Filename that will be used for the file as the attachment
        $fileatt_name                                                   = "".date("Ymd")."_-_SLA_Report.xls";

        $data                                                           = chunk_split(base64_encode($message));

        $message                                                        = "--{$mime_boundary}\n".
                                                                            "Content-Type:text/html; charset=\"iso-8859-1\"\n".
                                                                            "Content-Transfer-Encoding: 7bit\n\n".
                                                                            "Project(s) that exceeded the limit of the Service Level Agreement between ".$month_start.
                                                                            " and ".$month_end." can be found in the attached file.\n\n";
        $message                                                        .= "--{$mime_boundary}\n".
                                                                            "Content-Type: {$fileatt_type};\n".
                                                                            " name=\"{$fileatt_name}\"\n".
                                                                            "Content-Transfer-Encoding: base64\n\n".
                                                                            $data."\n\n".
                                                                            "--{$mime_boundary}\n";

        unset($data);
        unset($fileatt_type);
        unset($fileatt_name);
        ///////////////////////////

        if ($to != "")
            sendmail($to, $subject, $message, $from, $reply_to);

        $insert                                                         = q("INSERT INTO Events (date, time, message, notify_id, processed, user_id, company_id) ".
                                                                            "VALUES ('".date("Y-m-d")."', '".date("H:i:s")."', 'SLA Monthly', '$notify_id', '1', '1', ".
                                                                            "'$company_id')");

        return;
    }

    ///////////////////////////
    //  Expense Report Monthly
    ///////////////////////////
    function expenseMonthly($company_id) {
        //  Set Table Border & Background Details
        $top = "border-top: 1px solid #336699;";
        $left = "border-left: 1px solid #336699;";
        $right = "border-right: 1px solid #336699;";
        $bottom = "border-bottom: 1px solid #336699;";
        $background = "background-color:#6699CC;";

        $displayString = "";

        $month_start = date("Y-m-01");
        $month_end = getMonthEnd($month_start);

        $from = "On Track Notification <support@integrityengineering.co.za>";
        $reply_to = "On Track Support <support@integrityengineering.co.za>";

        $employees = q("SELECT e.id, e.frstname, e.lstname, e.email FROM (Employee AS e INNER JOIN Company_Users AS cu ON e.id = cu.user_id) ".
                        "WHERE cu.company_id = '$company_id' AND e.email != 'admin' AND e.deleted = '0'");
        $currency = q("SELECT currency FROM Company WHERE id = '$company_id'");

        $to = getRecipients("ExpenseMonthly", $company_id);
        $subject = "On Track - Monthly Expense Report/Employee";

        $notify_id = q("SELECT id FROM Notify WHERE code = 'ExpenseMonthly'");

        if (is_array($employees)) {
            $columns = 7;

            foreach ($employees as $employee) {
                $headings = "<tr><td colspan='".$columns."'><a>Report for ".$employee[1].", ".$employee[2]."</a></td></tr>".
                            "<tr><td colspan='".$columns."'><a>Project:&nbsp;&nbsp;All Projects</a></td></tr>";
                $table_heads = "<tr><td align='center' colspan='".$columns."'><a>All Expense Types</td></tr>".
                                "<tr><td align='center' style='".$top.$left.$bottom.$background."'><a style='color:#FFFFFF;'>&nbsp;Project Name".
                                "&nbsp;</a></td><td align='center' style='".$top.$left.$bottom.$background."'><a style='color:#FFFFFF;'>&nbsp;".
                                "Disbursement Type&nbsp;</a></td><td align='center' style='".$top.$left.$bottom.$background."'>".
                                "<a style='color:#FFFFFF;'>&nbsp;Description&nbsp;</a></td><td align='center' style='".$top.$left.$bottom.$background.
                                "'><a style='color:#FFFFFF;'>&nbsp;Date&nbsp;</a></td><td align='center' style='".$top.$left.$bottom.$background."'>".
                                "<a style='color:#FFFFFF;'>&nbsp;Kilometers&nbsp;</a></td><td align='center' style='".$top.$left.$bottom.$background."'>".
                                "<a style='color:#FFFFFF;'>&nbsp;Rate&nbsp;</a></td><td align='center' style='".$top.$left.$right.$bottom.
                                $background."'><a style='color:#FFFFFF;'>&nbsp;Expense Total <i>(".$currency.")</i>&nbsp;</a></td></tr>";
                $info = q("SELECT p.name, es.expense_type_id, v.type, d.name, es.descr, es.date, es.kilometers, es.rate, es.expense ".
                            "FROM (ExpenseSheet AS es LEFT JOIN Project AS p ON p.id = es.project_id LEFT JOIN Vehicle AS v ON v.id = es.vehicle_id ".
                            "LEFT JOIN Disbursements AS d ON d.id = es.disbursement_id) WHERE p.company_id = '$company_id' ".
                            "AND es.user_id = '".$employee[0]."' AND es.date >= '$month_start' AND es.date <= '$month_end' ORDER BY es.date");

                //  Create Display String
                if (is_array($info)) {
                    $displayString .= "".$headings;
                    $displayString .= "".$table_heads;

                    $total = 0;

                    foreach ($info as $i) {
                        $displayString .= "<tr>";
                            for ($c = 0; $c < $columns; $c++)
                                if ($c == 0)
                                    $displayString .= "<td align='left' style='".$left.$bottom."'>&nbsp;".$i[0]."&nbsp;</td>";
                                else if ($c == 1) {
                                    if ($i[1] == "1")
                                        $displayString .= "<td align='left' style='".$left.$bottom."'>&nbsp;Expense&nbsp;</td>";
                                    else if ($i[1] == "2")
                                        $displayString .= "<td align='left' style='".$left.$bottom."'>&nbsp;Travel - ".$i[2]."</td>";
                                    else if ($i[1] == "3")
                                        $displayString .= "<td align='left' style='".$left.$bottom."'>&nbsp;Printing - ".$i[3]."</td>";
                                }
                                else if ($c == 2 || $c == 3) {
                                    $displayString .= "<td align='left' style='".$left.$bottom."'>&nbsp;".$i[$c+2]."&nbsp;</td>";
                                }
                                else if ($c == 4 || $c == 5) {
                                    if ($i[$c+2] != "")
                                        $displayString .= "<td align='right' style='".$left.$bottom."'>".number_format($i[$c+2], 2, ".", "")."</td>";
                                    else
                                        $displayString .= "<td align='right' style='".$left.$bottom."'>-</td>";
                                }
                                else {
                                    $total += $i[$c+2];

                                    $displayString .= "<td align='right' style='".$left.$right.$bottom."'>".number_format($i[$c+2], 2, ".", "")."</td>";
                                }
                        $displayString .= "</tr>";
                    }

                    $displayString .= "<tr>";
                    $displayString .= "<td align='right' colspan='".($columns - 1)."' style='".$top.$left.$bottom.$background."'>".
                                        "<a style='color:#FFFFFF;'>Total <i>(".$currency.")</i>:</a></td><td align='right' style='".$top.$left.
                                        $right.$bottom."'>".number_format($total, 2, ".", "")."</td>";
                    $displayString .= "</tr><tr><td></td></tr>";
                }
            }
        }

        $file_name = "".date("Ymd")."_-_Expense_Monthly_Report.xls";
        $displayString = "header('Expires: 0');".
                            "header('Cache-Control: must-revalidate, post-check=0, pre-check=0');".
                            "header('Content-Type: application/vnd.ms-excel');".
                            "header('Content-Disposition: attachment; filename='.$file_name);".
                            "<html><head><title>Excel Spreadsheet</title><meta http-equiv='Content-Type' ".
                            "content='text/html; charset=iso-8859-1'></head><body><table cellpadding='0' ".
                            "cellspacing='0'>".$displayString."</table></body></html>";

        $message = "".$displayString;

        ///////////////////////////
        $semi_rand = md5(time());
        $mime_boundary = "==Multipart_Boundary_x{$semi_rand}x";

        $reply_to .= "\nMIME-Version: 1.0\n".
                        "Content-Type: multipart/mixed;\n".
                        " boundary=\"{$mime_boundary}\"";

        // File Type
        $fileatt_type = "application/vnd.ms-excel";
        // Filename that will be used for the file as the attachment
        $fileatt_name = "".date("Ymd")."_-_Expense_Monthly_Report.xls";
        $data = chunk_split(base64_encode($message));

        $message = "--{$mime_boundary}\n".
                    "Content-Type:text/html; charset=\"iso-8859-1\"\n".
                    "Content-Transfer-Encoding: 7bit\n\n".
                    "Expenses booked by employee(s) between ".$month_start.
                    " and ".$month_end." can be found in the attached file.\n\n";
        $message .= "--{$mime_boundary}\n".
                    "Content-Type: {$fileatt_type};\n".
                    " name=\"{$fileatt_name}\"\n".
                    "Content-Transfer-Encoding: base64\n\n".
                    $data."\n\n".
                    "--{$mime_boundary}\n";

        unset($data);
        unset($fileatt_type);
        unset($fileatt_name);
        ///////////////////////////

        if ($to != "")
            sendmail($to, $subject, $message, $from, $reply_to);

        $insert = q("INSERT INTO Events (date, time, message, notify_id, processed, user_id, company_id) ".
                    "VALUES ('".date("Y-m-d")."', '".date("H:i:s")."', 'Expense Monthly', '$notify_id', '1', '1', '$company_id')");

        return;
    }

    ///////////////////////////
    //  Projects Over Budget
    ///////////////////////////
    function overBudget($company_id)
    {
        $from = "On Track Notification <support@integrityengineering.co.za>";
        $reply_to = $from;
        $subject = "On Track - Project(s) Over Budget";

        $to = getRecipients("OverBudget", $company_id);

        $notify_id = q("SELECT id FROM Notify WHERE code = 'OverBudget'");

        if ($to != "")  {
            $projects = q("SELECT DISTINCT(p.id), p.name, p.due_date FROM (Project AS p INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) ".
                            "WHERE p.completed = '0' AND p.deleted = '0' AND cot.status = '1' AND cot.deleted = '0' AND cot.overBudget = '0' AND cot.company_id = '".$company_id."' ORDER BY UPPER(p.name)");

            $projectList = "";

            if (is_array($projects))    {
                foreach ($projects as $p)       {
                    $totalBudget = q("SELECT total_budget FROM companiesOnTeam WHERE project_id = '".$p[0]."' AND company_id = '".$company_id."'");

                    if (is_numeric($totalBudget) && $totalBudget != "") {
                        $sumTime = q("SELECT SUM(rate * time_spent) FROM TimeSheet WHERE project_id = '".$p[0]."' AND company_id = '".$company_id."'");
                        $sumExpense = q("SELECT SUM(expense) FROM ExpenseSheet WHERE project_id = '".$p[0]."' AND company_id = '".$company_id."'");

                        if (number_format($totalBudget, 2, ".", "") <= number_format(($sumTime + $sumExpense), 2, ".", ""))     {
                            $update = q("UPDATE companiesOnTeam SET overBudget = '1' WHERE project_id = '".$p[0]."' AND company_id = '".$company_id."'");

                            $projectList .= $p[1]."<br/>\n";
                        }
                    }
                }
            }

            if ($projectList != "") {
                $message = print_email_header($subject)."The following projects have gone over there budget:<br/>\n".$projectList."<br/><br/>\n".
                            "Regards,<br/>".
                            "On Track Notification<br/>".print_email_footer();

                if ($to != "")
                    sendmail($to, $subject, $message, $from, $reply_to, true);
            }
        }

        $insert = q("INSERT INTO Events (date,time,message,notify_id,processed,user_id,company_id) VALUES ('".date("Y-m-d")."','".date("H:i:s")."','Project(s) Over Budget','$notify_id','1','1','$company_id')");

        return;
    }

    ///////////////////////////
    //  Projects Over Due Date
    ///////////////////////////
    function overDueDate($company_id)
    {
        $from = "On Track Notification <support@integrityengineering.co.za>";
        $reply_to = $from;
        $subject = "On Track - Project(s) Over Due Date";

        $to = getRecipients("PastDueDate", $company_id);

        $notify_id = q("SELECT id FROM Notify WHERE code = 'PastDueDate'");

        $projects = q("SELECT DISTINCT(p.id), p.name, p.due_date FROM (Project AS p INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) ".
                        "WHERE p.completed = '0' AND p.deleted = '0' AND cot.status = '1' AND cot.deleted = '0' AND cot.company_id = '".$company_id."' ORDER BY UPPER(p.name)");

        $projectList = "";

        if (is_array($projects))    {
            foreach ($projects as $p)       {
                if ($p[2] < date("Y-m-d") && $p[2] != "")
                    $projectList .= $p[1]."<br/>\n";
            }
        }

        if ($projectList != "") {
            $message = print_email_header($subject)."The following projects have gone over there due dates:<br/>\n".$projectList."<br/><br/>\n".
                        "Regards,<br/>".
                        "On Track Notification<br/>".print_email_footer();

            if ($to != "")
                sendmail($to, $subject, $message, $from, $reply_to, true);
        }

        $insert = q("INSERT INTO Events (date,time,message,notify_id,processed,user_id,company_id) VALUES ('".date("Y-m-d")."','".date("H:i:s")."','Project(s) Over Due Date','$notify_id','1','1','$company_id')");

        return;
    }

?>
