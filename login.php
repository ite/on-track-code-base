<?php
	session_start();

	$errorMessage = "";

	include("_db.php");
	//include("_mobile.php");

	function print_analytics() {
		global $analytics;
?>
		<script language="JavaScript" src="analytics/<?php echo $analytics; ?>"></script>
<?php
	}

	// Detect and redirect mobile browsers
	function mobile_detection ()    {
		if (isset($_SERVER['HTTP_X_WAP_PROFILE']) || isset($_SERVER['HTTP_PROFILE']))   return 1;
		if (isset ($_SERVER['HTTP_ACCEPT']))    {
			$accept = strtolower($_SERVER['HTTP_ACCEPT']);
			if (strpos($accept, 'wap') !== false)   return 1;
		}
		if (isset ($_SERVER['HTTP_USER_AGENT']))    {
			if (strpos ($_SERVER['HTTP_USER_AGENT'], 'Mobile') !== false)    return 1;
			if (strpos ($_SERVER['HTTP_USER_AGENT'], 'Opera Mini') !== false)   return 1;
		}
		return 0;
	}

	// if (mobile_detection())    {
	// 	echo "<meta http-equiv='Refresh' content='0; url=http://live.on-track.ws/mobile'>";
	// }
	//////////////////////////////////////////

	// Error Message Display settings
	$loginError = "none";
	$loginLocked = "none";

	if ($_SESSION["logged_in"] === true)
		header("Location: home.php");
		//header("Location: introduction.php");

	if (isset($_POST["txtUsername"]) && isset($_POST["txtPassword"]))   {
		if (isset($_SESSION["sleeper"]))
			sleep($_SESSION["sleeper"]);

		$username = addslashes($_POST["txtUsername"]);
		$password = addslashes($_POST["txtPassword"]);
		$salt = q("SELECT salt FROM Employee WHERE email = '$username'");

		$password = generateHash($password, $salt);
		$login = q("SELECT id,email,usertype,company_id,locked FROM Employee WHERE email = '$username' AND password = '$password'");

		if (is_array($login))   {
			if ($login[0][4] === "0")   {
				//  Username & Password Match
				$_SESSION["logged_in"] = true;
				$_SESSION["user_id"] = $login[0][0];
				$_SESSION["email"] = $login[0][1];
				$_SESSION["usertype"] = $login[0][2];
				$_SESSION["primary_company"] = $login[0][3];
				$_SESSION["user"] = q("SELECT CONCAT(frstname, ' ', lstname) FROM Employee WHERE id = '".$_SESSION["user_id"]."' ");

				unset($_SESSION["attempt"]);
				unset($_SESSION["sleeper"]);

				$time = date("H:i:s");

				$logs = q("INSERT INTO Logs (what,access,on_table,by_user,on_date,on_time) ".
							"VALUES ('".$_SESSION["email"]." logged in successfully','Select','Employee','".$_SESSION["email"]."','$today','$time')");

				//  Check If User Agreed With EULA Terms
				$eula_agreed = q("SELECT id FROM EULA WHERE user_id = '".$_SESSION["user_id"]."'");

				if ($eula_agreed != "") {
					$_SESSION["eula_agree"] = true;

	/*
					//  Check If User Choose To Not Seen News/Updated Again
					//$do_not_show = q("SELECT id FROM RSS_Seen WHERE user_id = '".$_SESSION["user_id"]."'");

					//if ($do_not_show != "") {
						$_SESSION["news"] = true;

						header("Location: home.php");
						//header("Location: introduction.php");
					//}
					//else
					//    header("Location: news.php");
	*/
					if ($_SESSION["email"] == "admin")
						$_SESSION["primary_company"] = 1;

					$_SESSION["company_id"] = $_SESSION["primary_company"];
					$_SESSION["company_name"] = q("SELECT name FROM Company WHERE id = '".$_SESSION["primary_company"]."'");
					$_SESSION["currency"] = q("SELECT currency FROM Company WHERE id = '".$_SESSION["primary_company"]."'");
					$_SESSION["VAT"] = q("SELECT vat FROM Company WHERE id = '".$_SESSION["primary_company"]."'");
					//addslashes(strip_tags($_SESSION["VAT"]))

					$actions = q("SELECT a.`action` FROM (((roles AS r INNER JOIN role_action AS ra ON r.id = ra.roleid) INNER JOIN user_role AS ur ".
									"ON r.id = ur.roleid) INNER JOIN actions AS a ON ra.actionid = a.id) ".
									"WHERE ur.companyid = '".$_SESSION["company_id"]."' AND ra.companyid = '".$_SESSION["company_id"]."' AND ur.userid = '".$_SESSION["user_id"]."' ".
										"AND r.companyid = '".$_SESSION["company_id"]."'");

					if ($_SESSION["email"] == "admin")
						$actions = q("SELECT `action` FROM actions");

					if (is_array($actions))
						foreach ($actions as $a)
							$actionArr[] = $a[0];

					$_SESSION["actions"] = $actionArr;

					$modules = q("SELECT m.code, m.name FROM (Modules AS m INNER JOIN Module_Users AS mu ON m.id = mu.module_id) WHERE mu.company_id = '".$_SESSION["company_id"]."' AND m.code = 'approval'");

					if (!is_array($modules))        $_SESSION["approvalModule"] = 0;
					else                            $_SESSION["approvalModule"] = 1;

					$landingPage = q("SELECT landingPage FROM user_defaults WHERE companyID = '".$_SESSION["company_id"]."' AND userID = '".$_SESSION["user_id"]."'");
					$activityType = q("SELECT activityType FROM user_defaults WHERE companyID = '".$_SESSION["company_id"]."' AND userID = '".$_SESSION["user_id"]."'");
					$activity = q("SELECT activity FROM user_defaults WHERE companyID = '".$_SESSION["company_id"]."' AND userID = '".$_SESSION["user_id"]."'");

					$_SESSION["default_activityType"] = $activityType;
					$_SESSION["default_activity"] = $activity;

					$landingPage = ($landingPage == "dashboard.php") ? "bookings.php" : $landingPage;

					//$is_using_gps_logbook = q("SELECT gpsLogbook FROM Company WHERE id = '".$_SESSION['company_id']."' LIMIT 1");
					//$landingPage = ($is_using_gps_logbook == 1 &&  $_SESSION["email"] != "admin" && hasAccess("GPS_LOGBOOK_MANAGEMENT")) ? "gps_logbook_api_import.php" : $landingPage;

					header("Location: ".$landingPage);
				}else {
					header("Location: eula.php");
				}
			}
			else{
				//$errorMessage = "Sorry, your account has been locked";
				$loginLocked = "block";
				$loginError = "none";
			}
		}
		else    {
			if (isset($_SESSION["logged_in"]))
				$_SESSION["logged_in"] = false;

			$_SESSION["attempt"] = (!isset($_SESSION["attempt"])) ? 1 : $_SESSION["attempt"] + 1;
			$_SESSION["sleeper"] = (!isset($_SESSION["sleeper"])) ? 1 : $_SESSION["sleeper"] * 2;

			//$errorMessage = "Sorry, wrong username / password";
			$loginError = "block";
			$loginLocked = "none";

			if ($_SESSION["attempt"] >= 3)   {
				if (is_array(q("SELECT * FROM Employee WHERE email = '$username' AND locked = '0'")))   {
					$update = q("UPDATE Employee SET locked = 1 WHERE email = '$username'");
				}

				//$errorMessage = "Sorry, your account has been locked";
				$loginLocked = "block";
				$loginError = "none";
			}
		}
	}

	//if ($errorMessage != "")
		//echo "<p align='center' style='padding:0px;'><strong><font color='#999999'>$errorMessage</font></strong></p>";
?>
<!--
	Title:					On-Track Login Screen
	For:
	Site:					On-Track
	Author:				DID
	Version:			1.0
	Date Created:	2011/04/12
	Last Modified:	2011/04/13
-->
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html>
	<head>
		<title>On-Track - Login Page</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="shortcut icon" href="images/favicon.png">
		<!-- BEGIN: STYLESHEET -->
		<link rel="stylesheet" href="CSSFiles/css/login.css"/>
		<style type="text/css">
			.browserMessage         {color:#B22222; text-decoration:none;text-shadow: 0.1em 0.1em 0.2em black;}
			#browserMessage a       {color:#E6E6E6; text-decoration:none;text-shadow: 0.1em 0.1em 0.2em black;}
			#browserMessage a:hover {color:#B22222; text-decoration:none;text-shadow: 0.1em 0.1em 0.2em black;}
			.hidden                 {display:none;}
		</style>
		<!--[if IE]>
		<style type="text/css">
			.on-emo	{margin-left:-96px;}
			.on-logon{top:10%;}
			.on-log-bg{background:transparent;border:0;}
			.on-log-con{background:transparent;border:0;}
			.on-integ{background:none;}
			.on-integ-ie{
				background:url(im/on-integ.png);
				display:block;
				height:160px;
				width:160px;
				position:absolute;
				right:0;
				top:0;
				z-index:9999;
			}
			.on-verse{
				display:block;
				width:100%;
				margin-left:-220px;
				top:60%;
				line-height:150%;
				position:absolute;
				text-align:center;
				cursor: pointer;
				padding-top:120px;
			}
			.on-forgotten a   {color:#333333;}
		</style>
		<![endif]-->
		<!-- END: STYLESHEET -->

		<!--  BEGIN:  ANALYTICS  -->
		<?php
			print_analytics();
		?>
		<!--  END:  ANALYTICS  -->

		<!-- BEGIN: JQUERRY -->
		<script language="JavaScript" src="include/jquery.js"></script>
		<script type="Text/JavaScript">
			var successMessage;
			var errorMessage;
			var modalID = "";

			jQuery(function()    {
				if (jQuery.browser.msie)        jQuery("#browserMessage").show();
				else                            jQuery("#browserMessage").hide();

				jQuery("#txtUsername, #txtPassword").keypress(function(e)    {
					if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13))   {
						jQuery("#login").submit();
					}
				});

				jQuery("#btnLogin").click(function() {
					jQuery("#login").submit();
				});

				//  Successful Modal Alerts
				successMessage = function(message)   {
					if (modalID != "")  jQuery(modalID).hide();

					jQuery("#mask").css({"width":jQuery(document).width(),"height":jQuery(document).height()});
					jQuery('#mask').fadeIn("fast").fadeTo("fast",0.8);   //transition effect

					var id = "#modal_success";
					jQuery("#successMessage").html(message);
					//Set the popup window to center
					//jQuery(id).css('top', (jQuery(window).height()/2-jQuery(id).height()/2) - jQuery(id).height()/3);
					//jQuery(id).css('left', jQuery(window).width()/2-jQuery(id).width()/2);
					jQuery(id).fadeIn("fast");   //transition effect
				};

				jQuery("#closeSuccess").click(function()	{
					jQuery("#mask").hide();
					jQuery('.on-drop-menu').hide();
					jQuery('.on-contact-modal').hide();
				});

				//  Error Modal Alerts
				errorMessage = function(message)   {
					if (modalID != "")  jQuery(modalID).hide();

					jQuery("#mask").css({"width":jQuery(document).width(),"height":jQuery(document).height()});
					jQuery('#mask').fadeIn("fast").fadeTo("fast",0.8);   //transition effect

					var id = "#modal_error";
					jQuery("#errorMessage").html(message);
					//Set the popup window to center
					//jQuery(id).css('top', (jQuery(window).height()/2-jQuery(id).height()/2) - jQuery(id).height()/3);
					//jQuery(id).css('left', jQuery(window).width()/2-jQuery(id).width()/2);
					jQuery(id).fadeIn("fast");   //transition effect
				};

				jQuery("#closeError").click(function()	{
					jQuery("#modal_error").hide();

					if (modalID != "")  jQuery(modalID).show();
				});

				///// -- MAIL BUTTON SCRIPT -- /////
				// Mail Clicked
//				jQuery(".on-but-mail").click(function()  {
//					jQuery("#mask").css({"width":jQuery(window).width(),"height":jQuery(document).height()});
//					jQuery("#mask").show();
//
//					jQuery("#modal_help").show();
//					var id = "#modal_help";
//					//Set the popup window to center
//					//jQuery(id).css('top',  "200%"/2-jQuery(id).height()/2);
//					//jQuery(id).css('left', "200%"/2-jQuery(id).width()/2);
//					jQuery(id).fadeIn("fast");   //transition effect
//				});
				// Mask Hide
				jQuery("#mask").click(function()	{
					jQuery("#sendEmail").show();
					jQuery(this).hide();
					jQuery('.on-contact-modal').hide();
				});
				// Send E-Mail
				jQuery("#sendEmail").click(function()   {
					modalID = "#modal_help";
					jQuery("#modal_help").hide();

					<?php
					//global $serverName;

					//if ($serverName == "demo.on-track.ws" || $serverName == "localhost")	{
					?>
					if ((jQuery("#contact_from").val() != "") && (jQuery("#contact_subject").val() != "") && (jQuery("#contact_description").val() != ""))   {
						var from = jQuery("#contact_from").val();
						var subject = jQuery("#contact_subject").val();
						var desc = jQuery("#contact_description").val();

						var exp = /^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/;

						if (exp.test(from))	{
							jQuery.post("_ajax.php", {func: "sendEmail", from: from, subject: subject, desc: desc}, function(data){
								successMessage("Thank you for your feedback.<br><br>You will be contacted via email within the next business day.");
								jQuery("#contact_subject").val("");
								jQuery("#contact_description").val("");
								//jQuery("#modal_help").show();
							});
						} else
							errorMessage("Please enter a valid email address.");
					}
					else{
						errorMessage("Please fill out the entire form");
						//jQuery("#modal_help").show();
					}
					<?php
					/*}	else	{
					?>
					if ((jQuery("#contact_subject").val() != "") && (jQuery("#contact_description").val() != ""))   {
						jQuery.post("_ajax.php", {func: "sendEmail", subject: jQuery("#contact_subject").val(), desc: jQuery("#contact_description").val()}, function(data){
							successMessage("Thank you for your feedback.<br><br>You will be contacted via email within the next business day.");
							jQuery("#contact_subject").val("");
							jQuery("#contact_description").val("");
							//jQuery("#modal_help").show();
						});
					}
					else{
						errorMessage("Please fill out the entire form");
						//jQuery("#modal_help").show();
					}
					<?php
					}*/
					?>
				});
				// Cancel E-Mail
				jQuery("#cancelEmail").click(function()   {
					jQuery("#mask").hide();
					jQuery("#sendEmail").show();
					jQuery('.on-drop-menu').hide();
					jQuery('.on-contact-modal').hide();
					jQuery("#contact_subject").val("");
					jQuery("#contact_description").val("");
				});
				///// -- END MAIL SCRIPT -- /////

				///// -- Forgot password Mail -- /////
				// Send E-Mail
//				jQuery("#on-request-password").click(function()   {
//					if ((jQuery("#txtUsername").val() != ""))   {
//						jQuery.post("_ajax.php", {func: "sendEmail", subject: "Password Request", desc: "User "+jQuery("#txtUsername").val()+" is requesting his/her On-Track Password"}, function(data){
//							successMessage("Thank you for your password request.<br><br>You will be contacted via email within the next business day.");
//						});
//					jQuery("#on-request-password").toggle();
//					}
//					else{
//						errorMessage("Please fill in your Username");
//					}
//				});
				///// -- END Forgot password -- /////

				///// -- COMING SOON BUBBLE -- /////
				//jQuery('.coming-soon').hover(function() {
				//   jQuery("#coming-soon").toggle();
				//});
			});
		</script>
		<!-- END: JQUERRY -->
	</head>
	<body>
		<div id="mask"></div>
		<!----- BEGIN: BODY ----->
		<a  class="on-integ-ie" href="http://www.integrityengineering.co.za/" target="_blank"></a> <!----- ITE LOGO all browsers ----->
		<div class="on-log-con"><a  class="on-integ" href="http://www.integrityengineering.co.za/" target="_blank"></a> <!----- ITE LOGO all browsers ----->
			<div class="on-log-bg"></div>
			<div class="on-logon">
				<!-- BEGIN: ON-TRACK LOGON -->
				<form action="" method="post" id="login" name="login">
					<div class="on-log-fields">
						<table class="" id="" name="" summary="Login Page">
							<tbody>
								<tr>
									<td class="on-description">username</td>
									<td><input class="on-field" type="text" id="txtUsername" name="txtUsername" /></td>
								</tr>
								<tr>
									<td class="on-description">password</td>
									<td><input class="on-field" type="password" id="txtPassword" name="txtPassword" /></td>
								</tr>
							</tbody>
						</table>
						<a class="on-but" id="btnLogin" name="btnLogin" title="Login"><span><div class="on-but-pad">Login</div></span></a>
						<div class="on-login-error" style="display:<?php echo $loginError; ?>"><img src="CSSFiles/im/login-error.png"></div>
						<div class="on-login-locked" style="display:<?php echo $loginLocked; ?>"><img src="CSSFiles/im/login-locked.png"></div>
					</div>
				</form>
				<!--  BEGIN: CONTACT ITE MODAL  -->
				<div id="modal_help" name="modal_help" class="on-contact-modal">
					<table>
						<tr>
							<td colspan="100%" style="text-align:center"><a>Help &amp; Support</a><br><br></td>
						</tr>
						<?php
							//global $serverName;

							//if ($serverName == "demo.on-track.ws" || $serverName == "localhost")	{
						?>
						<tr>
							<td class="on-description-mail">From:</td>
							<td><input id="contact_from" name="contact_from" class="on-field" type="text" /></td>
						</tr>
						<?php
							//}
						?>
						<tr>
							<td class="on-description-mail">Subject:</td>
							<td><input id="contact_subject" name="contact_subject" class="on-field" type="text" /></td>
						</tr>
						<tr>
							<td class="on-description-mail">Desciption:</td>
							<td><textarea id="contact_description" name="contact_description" cols="50" rows="6" class="on-field maxLength" ></textarea></td>
						</tr>
						<tr>
							<td></td>
							<td>
								<input id="sendEmail" name="sendEmail" type="button" value="Send" />
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input id="cancelEmail" name="cancelEmail" type="button" value="Cancel" />
							</td>
						</tr>
					</table>
				</div>
				<!--  END: CONTACT ITE MODAL  -->
				<div id="modal_error" name="modal_error" class="on-contact-modal">
					<table>
						<tr>
							<td colspan="100%" class="centerdata">
								<img src="images/logo.png" />
								<br/><br/>
								<strong style="color:orange;font-weight:bold;font-size:18px;font-variant:small-caps;">
									Request Unsuccessful
								</strong>
								<br/><br/>
								<table width="300px">
									<tr>
										<td>
											<center id="errorMessage" name="errorMessage" style="font-weight:bold;font-size:14px;color:#d2d2d2;">
											</center>
										</td>
									</tr>
								</table>
								<br/>
								<input id="closeError" name="closeError" type="button" value="Close" />
							</td>
						</tr>
					</table>
				</div>
				<div id="modal_success" name="modal_success" class="on-contact-modal">
					<table>
						<tr>
							<td colspan="100%" class="centerdata">
								<img src="images/logo.png" />
								<br/><br/>
								<strong style="color:#028200;font-weight:bold;font-size:18px;font-variant:small-caps;">
									Request Successfully Submitted
								</strong>
								<br/><br/>
								<table width="300px">
									<tr>
										<td>
											<center id="successMessage" name="successMessage" style="font-weight:bold;font-size:14px;color:#d2d2d2;">
											</center>
										</td>
									</tr>
								</table>
								<br/>
								<input id="closeSuccess" name="closeSuccess" type="button" value="Close" />
							</td>
						</tr>
					</table>
				</div>
				<div class="on-forgotten"><a id="on-request-password" href="mailto:support@integrityengineering.co.za?Subject=Forgot%20Password">Forgotten password</a></div>
				<div class="on-emo">
					<!--<div id="coming-soon"><a>coming soon</a></div>-->
					<a class="on-but-mail on-emost" name="on-but-mail" title="mail" href="mailto:support@integrityengineering.co.za?Subject=On-Track%20Support" ></a>
					<a class="on-but-fb on-emost" name="on-but-fb" href="https://www.facebook.com/pages/On-Track-Business-Management/274662732552676?ref=ts" title="facebook" target="_blank"></a>
					<a class="on-but-tw on-emost" name="on-but-tw" href="http://twitter.com/#!/ite_ontrack" title="twitter" target="_blank"></a>
					<a class="on-but-yt on-emost" name="on-but-yt" href="http://www.youtube.com/user/Integrityengeneering" title="youtube" target="_blank"></a>
					<a class="coming-soon on-but-rss on-emost" name="on-but-rss" title="rss - Coming soon"></a>
				</div>
				<div class="on-verse">
					<a>On-Track v3.0</a>
					<br/><br/>
					<iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.facebook.com%2Fpages%2FOn-Track-Business-Management%2F274662732552676&amp;send=false&amp;layout=button_count&amp;width=450&amp;show_faces=false&amp;action=like&amp;colorscheme=dark&amp;font=tahoma&amp;height=21"
						scrolling="no" frameborder="0" style="padding-top:0px; border:none; overflow:hidden; width:80px; height:21px;" allowTransparency="true"></iframe>
					<br/><br/>
					<div id="browserMessage" name="browserMessage" class="browserMessage hidden">
						Please note that On-Track has been optimized for <a href="http://www.mozilla.org/en-US/firefox/new/" style="text-decoration:underline;" target="_blank">Firefox</a>
						and <a href="http://www.google.com/chrome" style="text-decoration:underline;" target="_blank">Google Chrome</a>.  We strongly encourage you to use one of these browsers.
					</div>
				</div>
			<!-- END: ON-TRACK LOGON -->
			</div>
	</div>
	</body>
	<!----- END: BODY ----->
</html>
