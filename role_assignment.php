<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("_functions.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    if (!hasAccess("ROLE_ASSIGN"))
        header("Location: noaccess.php");

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0", "vehicles");
?>
<script language="JavaScript">
    jQuery(function()    {
        jQuery("#user").change(function() {
            jQuery(".chkbox").each(function(i, v)	{
                jQuery(this).attr("checked", "");
            });

            var userid = jQuery("#user").val();

            jQuery.post("_ajax.php", {func: "get_role_assign", userid: userid}, function(data)	{
                data = json_decode(data);
                jQuery.each(data,function(i, v)	{
                    var field = "#role_"+v[3];
                    jQuery(field).attr("checked", "checked");
                });
            });
        });

        // For firs load/view
        jQuery(".chkbox").each(function(i, v)	{
                jQuery(this).attr("checked", "");
            });

            var userid = jQuery("#user").val();

            jQuery.post("_ajax.php", {func: "get_role_assign", userid: userid}, function(data)	{
                data = json_decode(data);
                jQuery.each(data,function(i, v)	{
                    var field = "#role_"+v[3];
                    jQuery(field).attr("checked", "checked");
                });
            });

    });
</script>

    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="assign_roles">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Role Assignment</h6>
                            </td>
                        </tr>
                        <?php if($_POST['saveroles']) echo "<tr><td class='centerdata'><br/><font style='color:#028200'> - Roles have been saved - </font></td></tr>"; ?>
                        <tr>
                            <td>
                                <br/>
                            </td>
                        </tr>
                    </table>

                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                User Name:
                            </td>
                            <td width="50%">
                                <?php
                                    $result = q("SELECT e.lstname,e.frstname,e.id FROM (Employee AS e INNER JOIN Company_Users as cu ON e.id = cu.user_id) ".
                                                "WHERE cu.company_id = ".$_SESSION["company_id"]." AND e.locked = '0' AND e.deleted = '0' AND e.email != 'admin' ".
                                                "ORDER BY e.lstname,e.frstname");

                                    echo "<select class='on-field' id='user' name='userid' value=''>Last Name</option>";

                                    if (is_array($result))
                                    foreach($result as $r){
                                        echo "<option value=".$r[2].">".$r[0].","." "."".$r[1]."</option>";
                                    }
                                    echo "</select>";   // Closing of list box
                                ?>
                                <!-- -->
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                Roles:
                            </td>
                            <td width="50%">
                               <!-- -->
                               <?php
                                        $query = "SELECT id,descr,role FROM roles WHERE active='1' AND companyid = '".$_SESSION["company_id"]."' ";
                                        $result = q($query);
                                    ?>
                                    <table class="on-lefttable on-table">
                                    <?php
                                    $counter = 0;
                                    $colorCounter = 0;
                                    foreach($result as $r){
                                        if( ($r[1] == "Admin") || ($r[1] == "admin") ){
                                            // Do Nothing
                                        }else{
                                            if($colorCounter == 0){
                                                echo "<tr>
                                                            <td>" . $r[1] . "</td><td><input id='role_".$r[0]."' name='role_".$r[0]."' type='checkbox' class='chkbox' value='" . $r[0] . "' /></td>
                                                        </tr>";
                                            }else if($colorCounter == 1){
                                                echo "<tr>
                                                            <td>" . $r[1] . "</td><td><input id='role_".$r[0]."' name='role_".$r[0]."' type='checkbox' class='chkbox' value='" . $r[0] . "' /></td>
                                                        </tr>";
                                            }
                                            $counter++;
                                            $colorCounter ++;
                                            if($colorCounter ==2){
                                                $colorCounter = 0;
                                            }
                                        }
                                    }

                                    ?>
                                    </table>
                               <!-- -->
                            </td>
                        </tr>
                    </table>

                    <br/>
                    <input name="btnroles" type="submit" value="Add/Save Roles">
                    <input name="saveroles" type="hidden" value="saveroles" />
                </form>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>

    <?php
        if ($_POST['saveroles']){
            //Get POST Values
            $companyid = $_SESSION['company_id'];
                //echo "Comp id: ".$companyid."<br/>";
            $userid = $_POST['userid'];
               // echo "User id: ".$userid."<br/>";

            $roleCount = 0;
            $query = q("SELECT id FROM roles WHERE active='1' AND companyid = '".$_SESSION["company_id"]."' ");
            foreach($query AS $q){
                if($_POST['role_'.$q[0]]){
                    $roleid[$roleCount] = $_POST['role_'.$q[0].''];
                    $roleCount++;
                }
            }

            // Remove old entries from database
            $remove = q("DELETE FROM user_role WHERE userid = $userid and companyid = $companyid");

            // Save values to Database
            $theNewRoles;       // Stores the role values
            $byUser = $_SESSION["email"];
            $date__ = date("Y-m-d h:i:s");
            for ($i=0; $i<$roleCount; $i++){
                $insert = q("INSERT INTO user_role (companyid, userid, roleid, dateupdated, byuser) VALUES ('$companyid', '$userid', '".$roleid[$i]."','$date__','$byUser') ");	// Correct Query
                $theNewRoles.= q("SELECT role FROM roles WHERE id='".$roleid[$i]."' ").", ";
            }

            updateMenuIndex($companyid,$userid);

            // Re-Issue User Actions
            if (exist("reissue_actions", "companyID = '".$companyid."' AND userID = '".$userid."'"))
                $update = q("UPDATE reissue_actions SET processed = 0 WHERE companyID = '".$companyid."' AND userID = '".$userid."'");
            else
                $insert = q("INSERT INTO reissue_actions (companyID,userID,processed) VALUES ('".$companyid."','".$userid."','0')");

            // Add a LOG of the addition
            $forUser = q("SELECT lstname FROM Employee WHERE id='".$_POST['userid']."' ");
            $what = $byUser ." changed ".$forUser."(".$_POST['userid'].") roles to ".$theNewRoles;
            $onDate = date("Y-m-d");
            $onTime = date("h:i:s");
            $compId = $_SESSION["company_id"];

            $insert = q("INSERT INTO Logs (what,access,on_table,by_user,on_date,on_time,company_id) VALUES ( '$what','Update','Roles','$byUser','$onDate','$onTime','$compId' )");
            // END Save
        }
    ?>

<?php
    //  Print Footer
    print_footer();
?>
