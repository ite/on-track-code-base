<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("_functions.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!$_SESSION["eula_agree"] === true)
        header("Location: logout.php");

    if (!hasAccess("NEWS_MAN"))
        header("Location: noaccess.php");

    //  If btnA (Continue) Was Pressed
    if (isset($_POST["btnA"]))
    {
        $_SESSION["news"]                                               = true;

        header("Location: home.php");
    }

    //  If btnB (Do Not Show Until Next Update) Was Pressed
    if (isset($_POST["btnB"]))
    {
        $insert                                                         = q("INSERT INTO RSS_Seen (user_id) VALUES ('".$_SESSION["user_id"]."')");

        $_SESSION["news"]                                               = true;

        header("Location: home.php");
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0", "news");
?>
    <div class="on-20px">
    <table width="100%">
        <tr>
            <td align="center" valign="top">
                <form action="" method="post" name="news">
                    <p>
                    <?php
                        include("_news.php");

                        if (!$_SESSION["news"] === true)
                        {
                            echo "</p>";
                            echo "<br/>";
                            echo "<input name='btnA' tabindex='1' type='submit' value='Continue'>";
                            echo "<br/>";
                            echo "<input name='btnB' tabindex='2' type='submit' value='Do Not Show Until Next Update'>";
                            echo "<br/><br/>";
                        }
                    ?>
                </form>
            </td>
        </tr>
    </table>
    </div>
<?php
    //  Print Footer
    print_footer();
?>