<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if ($_SESSION["logged_in"] === true && !isset($_SESSION["company_id"]))
        header("Location: home.php");

    //  Change Password Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")
    {
        $errorMessage                                                   = "";

        //  Get Information
        $current_password                                               = addslashes(strip_tags($_POST["current_password"]));
        $salt                                                           = q("SELECT salt FROM Employee WHERE id = '".$_SESSION["user_id"]."'");
        $current_password                                               = generateHash($current_password, $salt);

        //  Check If Current Password Matches Employees Password
        $match                                                          = q("SELECT id FROM Employee WHERE id = '".$_SESSION["user_id"]."' ".
                                                                            "AND password = '$current_password'");

        if ($match)
        {
            //  Get New Information
            $new_password                                               = addslashes(strip_tags($_POST["new_password"]));
            $new_password                                               = generateHash($new_password);

            $update                                                     = q("UPDATE Employee SET password = '$new_password', salt = '$origsalt' ".
                                                                            "WHERE id = '".$_SESSION["user_id"]."'");

            if ($update)
            {
                $time                                                   = date("H:i:s");

                $logs                                                   = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('Changed password successfully', 'Update', 'Employee', '".$_SESSION["email"]."', ".
                                                                            "'$today', '$time', '".$_SESSION["company_id"]."')");

                $errorMessage                                           = "Password Changed Successfully";
            }
        }
        else
            $errorMessage                                               = "Current Password Does Not Match";
    }

    if ($errorMessage != "")
        echo "<p align='center' style='padding:0px;'><strong><font color='#999999'>$errorMessage</font></strong></p>";

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0", "password");
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    var cal                                                             = new CodeThatCalendar(caldef1);

    function check()
    {
        var valid                                                       = 1;

        //  Check That Current Password Is Entered
        if (document.forms["change_password"].current_password.value == "")
        {
            ShowLayer("currentPassword", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("currentPassword", "none");

        //  Check That New Password Is Entered
        if (document.forms["change_password"].new_password.value == "")
        {
            ShowLayer("newPassword", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("newPassword", "none");

        //  Check That Confirm Password Is Entered
        if (document.forms["change_password"].confirm_password.value == "")
        {
            ShowLayer("confirmPassword", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("confirmPassword", "none");

        //  Check That New & Confirm Passwords Match
        if (document.forms["change_password"].new_password.value != document.forms["change_password"].confirm_password.value)
        {
            ShowLayer("passwordMatch", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("passwordMatch", "none");

        if (valid == 1)
        {
            document.forms["change_password"].save.value                = 1;
            document.forms["change_password"].submit();
        }
    }
</script>
    <table width="100%">
        <tr height="270px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="change_password">
                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Change Password</h6>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <br/>
                            </td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                Current Password:
                            </td>
                            <td width="50%">
                                <input class="on-field" name="current_password" tabindex="1" type="password" value="">
                                <div id="currentPassword" style="display: none;"><font class="on-validate-error">* Current password must be entered</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                New Password:
                            </td>
                            <td width="50%">
                                <input class="on-field" name="new_password" tabindex="2" type="password" value="">
                                <div id="newPassword" style="display: none;"><font class="on-validate-error">* New password must be entered</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                Confirm Password:
                            </td>
                            <td width="50%">
                                <input class="on-field" name="confirm_password" tabindex="3" type="password" value="">
                                <div id="confirmPassword" style="display: none;"><font class="on-validate-error">* Confirm password must be entered</font></div>
                                <div id="passwordMatch" style="display: none;"><font class="on-validate-error">* Passwords need to match</font></div>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <input name="btnChange" onClick="check();" tabindex="4" type="button" value="Change Password">
                    <input method="post" name="save" type="hidden" value="0" />
                </form>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>