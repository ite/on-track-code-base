<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("_functions.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

	if (!hasAccess("EXPENSE_MANAGE") && !hasAccess("EXPENSE_VIEW"))
		header("Location: noaccess.php");
	if (hasAccess("EXPENSE_VIEW") && !hasAccess("EXPENSE_MANAGE"))
		$RA_VIEW = 1;

    //  Set Dates
    $from_date                                                          = "".getDates($today, -15);
    $to_date                                                            = "".getDates($today, 30);

    $min_date                                                           = q("SELECT MIN(eb.due_date) FROM (ExpenseBudget AS eb INNER JOIN Project AS p ".
                                                                            "ON p.id = eb.project_id) WHERE eb.company_id = '".$_SESSION["company_id"]."' ".
                                                                            "AND eb.paid = '0'");
    $max_date                                                           = q("SELECT MAX(eb.due_date) FROM (ExpenseBudget AS eb INNER JOIN Project AS p ".
                                                                            "ON p.id = eb.project_id) WHERE eb.company_id = '".$_SESSION["company_id"]."' ".
                                                                            "AND eb.paid = '0'");

    if ($min_date != "")
        $from_date                                                      = $min_date;

    if ($max_date != "")
        $to_date                                                        = $max_date;

    $display_type                                                       = "0";

    if (isset($_POST["display"]) && $_POST["display"] === "1")
    {
        $from_date                                                      = addslashes(strip_tags($_POST["from_date"]));
        $to_date                                                        = addslashes(strip_tags($_POST["to_date"]));
        $display_type                                                   = $_POST["display_type"];
    }

    $project_id                                                         = "null";

    if (isset($_POST["project"]))
        $project_id                                                     = $_POST["project"];

    //  Update Expense Information Function
    if (isset($_POST["update"]) && $_POST["update"] === "1")
    {
        //  Get Dates
        $from_date                                                      = addslashes(strip_tags($_POST["from_date"]));
        $to_date                                                        = addslashes(strip_tags($_POST["to_date"]));
        $display_type                                                   = $_POST["display_type"];

        if ($display_type == "all")
        {
            //  nExpenses                                               = Number of Expenses
            $nExpenses                                                  = q("SELECT COUNT(eb.id) FROM (ExpenseBudget AS eb INNER JOIN Project AS p ".
                                                                            "ON p.id = eb.project_id) WHERE (eb.due_date BETWEEN '$from_date' ".
                                                                            "AND '$to_date' OR eb.due_date = '') AND eb.company_id = '".$_SESSION["company_id"]."'");
            $expenses                                                   = q("SELECT eb.id, p.name, eb.descr, eb.amount, eb.due_date, eb.paid FROM (ExpenseBudget AS eb ".
                                                                            "INNER JOIN Project AS p ON p.id = eb.project_id) WHERE (eb.due_date BETWEEN '$from_date' ".
                                                                            "AND '$to_date' OR eb.due_date = '') AND eb.company_id = '".$_SESSION["company_id"]."' ".
                                                                            "ORDER BY eb.due_date");
        }
        else
        {
            //  nExpenses                                               = Number of Expenses
            $nExpenses                                                  = q("SELECT COUNT(eb.id) FROM (ExpenseBudget AS eb INNER JOIN Project AS p ".
                                                                            "ON p.id = eb.project_id) WHERE eb.paid = '$display_type' ".
                                                                            "AND (eb.due_date BETWEEN '$from_date' AND '$to_date' OR eb.due_date = '') ".
                                                                            "AND eb.company_id = '".$_SESSION["company_id"]."'");
            $expenses                                                   = q("SELECT eb.id, p.name, eb.descr, eb.amount, eb.due_date, eb.paid FROM (ExpenseBudget AS eb ".
                                                                            "INNER JOIN Project AS p ON p.id = eb.project_id) WHERE eb.paid = '$display_type' ".
                                                                            "AND (eb.due_date BETWEEN '$from_date' AND '$to_date' OR eb.due_date = '') ".
                                                                            "AND eb.company_id = '".$_SESSION["company_id"]."' ORDER BY eb.due_date");
        }

        if ($nExpenses > 1)
        {
            foreach($expenses as $expense) {
                $date                                                   = addslashes(strip_tags($_POST["date".$expense[0]]));
				$old_amount = q("SELECT amount FROM ExpenseBudget WHERE id = '".$expenses[0]."'");
				$old_date = q("SELECT due_date FROM ExpenseBudget WHERE id = '".$expenses[0]."'");
				$old_vat = q("SELECT vat FROM ExpenseBudget WHERE id = '".$expenses[0]."'");
				$old_expense = ($old_amount+($old_amount*($old_vat/100)));

                if ($date != "") {
                    if ($_POST["box".$expense[0]])
                        $update                                         = q("UPDATE ExpenseBudget SET due_date = '".$date."', paid = '1' WHERE id = '".$expense[0]."'");
                    else
                        $update                                         = q("UPDATE ExpenseBudget SET due_date = '".$date."', paid = '0' WHERE id = '".$expense[0]."'");
					if ($old_date != $date) {
						statusUpdateDate($old_date, $date, "expense", $old_expense, $_SESSION["company_id"]);
					}
                }
            }
        }
        else if ($nExpenses == 1)
        {
            $date                                                       = addslashes(strip_tags($_POST["date".$expenses[0][0]]));
			$old_amount = q("SELECT amount FROM ExpenseBudget WHERE id = '".$expenses[0][0]."'");
			$old_date = q("SELECT due_date FROM ExpenseBudget WHERE id = '".$expenses[0][0]."'");
			$old_vat = q("SELECT vat FROM ExpenseBudget WHERE id = '".$expenses[0][0]."'");
			$old_expense = ($old_amount+($old_amount*($old_vat/100)));

            if ($date != "") {
                if ($_POST["box".$expenses[0][0]])
                    $update                                             = q("UPDATE ExpenseBudget SET due_date = '".$date."',paid = '1' WHERE id = '".$expenses[0][0]."'");
                else
                    $update                                             = q("UPDATE ExpenseBudget SET due_date = '".$date."',paid = '0' WHERE id = '".$expenses[0][0]."'");
					if ($old_date != $date) {
						statusUpdateDate($old_date, $date, "expense", $old_expense, $_SESSION["company_id"]);
					}
            }
        }

//        if ($update)
//            header("Location: expense_planning.php");
    }

    //  Insert Expense Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")
    {
        $errorMessage                                                   = "";

        //  Get Information
        $project_id                                                     = $_POST["project"];
        $description                                                    = addslashes(strip_tags($_POST["description"]));
        $expense                                                        = addslashes(strip_tags($_POST["expense"]));
        $vat                                                            = addslashes(strip_tags($_POST["VAT"]));
        $due_date                                                       = addslashes(strip_tags($_POST["due_date"]));

        if ($expense != "")
            $expense                                                    = number_format($expense, 2, ".", "");

        if ($vat != "")
            $vat                                                        = round($vat, 2);
        else
            $vat                                                        = 0;

        //  Check If Expense Exists In Database
        $exist                                                          = q("SELECT id FROM ExpenseBudget WHERE descr = '$description' AND project_id = '$project_id'");

        if (submitDateCheck($due_date))   {
        if (!$exist && $due_date != "")
        {
            $insert                                                     = q("INSERT INTO ExpenseBudget (project_id, descr, amount, vat, due_date, company_id, paid) ".
                                                                            "VALUES ('$project_id', '$description', '$expense', '$vat', '$due_date', ".
                                                                            "'".$_SESSION["company_id"]."', '0')");

            if ($insert)
            {
                //  Call Recalculation Function
				$amount = -($expense+($expense*($vat/100)));
                statusUpdateAmount($due_date, $amount, $_SESSION["company_id"]);

                $project_name                                           = q("SELECT name FROM Project WHERE id = '$project_id'");

                $time                                                   = date("H:i:s");

                $message                                                = "A new expense has been uploaded by ".$_SESSION["email"]." on ".$today." at ".$time.
                                                                            "\nProject Name:\t\t".$project_name."\nExpense Description:\t".$description.
                                                                            "\nExpense:\t\t\t".$_SESSION["currency"]." ".$expense."\nDue Date:\t\t\t".$due_date;
                $notify_id                                              = q("SELECT id FROM Notify WHERE code = 'Expense'");
                $events                                                 = q("INSERT INTO Events (date, time, message, notify_id, processed, user_id, company_id) ".
                                                                            "VALUES ('".$today."', '".$time."', '$message', '$notify_id', '0', ".
                                                                            "'".$_SESSION["user_id"]."', '".$_SESSION["company_id"]."')");

                $logs                                                   = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('".$description." created', 'Insert', 'ExpenseBudget', '".$_SESSION["email"]."', ".
                                                                            "'$today', '$time', '".$_SESSION["company_id"]."')");
            }
        }
        else if ($due_date == "")
            $errorMessage                                               = "Due Date Should Be Entered";
        else
            $errorMessage                                               = "Expense Already Exists";
        }
    }

    $projects = q("SELECT id, name FROM Project WHERE completed = '0' AND company_id = '".$_SESSION["company_id"]."' ORDER BY name");

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("2_1", "budget");

    if ($errorMessage != "")
    {
        if ($errorMessage == "Please note that we are currently busy with data management, no entries are allowed for 2010-08-26. Please check back in 24 Hours.")
            echo "<p align='center' style='padding:0px;'><strong style='font-size:12px;'><font color='#FF5900'>$errorMessage</font></strong></p>";
        else
            echo "<p align='center' style='padding:0px;'><strong><font color='#999999'>$errorMessage</font></strong></p>";
        echo "<br/>";
    }
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    jQuery(function(){
	jQuery('#from_date').DatePicker({
		format:'Y-m-d',
		date: jQuery('#from_date').val(),
		current: jQuery('#from_date').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#from_date').val()))
			_date = new Date();
			else _date = jQuery('#from_date').val();
			jQuery('#from_date').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#from_date').val(formated);
			jQuery('#from_date').DatePickerHide();
		}
	});
    })

    jQuery(function(){
	jQuery('#to_date').DatePicker({
		format:'Y-m-d',
		date: jQuery('#to_date').val(),
		current: jQuery('#to_date').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#to_date').val()))
			_date = new Date();
			else _date = jQuery('#to_date').val();
			jQuery('#to_date').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#to_date').val(formated);
			jQuery('#to_date').DatePickerHide();
		}
	});
    })

    jQuery(function(){
	jQuery('#due_date').DatePicker({
		format:'Y-m-d',
		date: jQuery('#due_date').val(),
		current: jQuery('#due_date').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#due_date').val()))
			_date = new Date();
			else _date = jQuery('#due_date').val();
			jQuery('#due_date').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#due_date').val(formated);
			jQuery('#due_date').DatePickerHide();
		}
	});
    })

    function check(box)
    {
        var valid                                                       = 1;

        //  Check That Invoice Name Is Entered
        if (document.forms["expense_planning"].description.value == "")
        {
            ShowLayer("Description", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("Description", "none");

        //  Check That Expenses Is Valid, If Entered
        if (document.forms["expense_planning"].expense.value != "")
        {
            if (!validation("currency", document.forms["expense_planning"].expense.value))
            {
                ShowLayer("expenseDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("expenseDiv", "none");
        }
        else
            ShowLayer("expenseDiv", "none");

        //  Check That Diverse Income Is Valid, If Entered
        if (document.forms["expense_planning"].VAT.value != "")
        {
            if (!validation("percentage", document.forms["expense_planning"].VAT.value))
            {
                ShowLayer("vatDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("vatDiv", "none");
        }
        else
            ShowLayer("vatDiv", "none");

        //  Check That Due Date Is Entered
        if (document.forms["expense_planning"].due_date.value == "")
        {
            ShowLayer("dueDate", "block");
            valid                                                       = 0;
        }
        //  Check That Entered Due Date Is Valid
        else
        {
            ShowLayer("dueDate", "none");

            if (!validation("date", document.forms["expense_planning"].due_date.value))
            {
                ShowLayer("dueDateDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("dueDateDiv", "none");
        }

        if (valid == 1)
        {
            document.forms["expense_planning"].save.value               = 1;
            document.forms["expense_planning"].submit();
        }
    }

    function check_dates()
    {
        var valid                                                       = 1;

        //  Check That From Date Is Valid
        if (document.forms["expense_planning"].from_date.value != "")
        {
            if (!validation("date", document.forms["expense_planning"].from_date.value))
            {
                ShowLayer("fromDate", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("fromDate", "none");
        }
        else
            ShowLayer("fromDate", "none");

        //  Check That From Date Is Valid
        if (document.forms["expense_planning"].to_date.value != "")
        {
            if (!validation("date", document.forms["expense_planning"].to_date.value))
            {
                ShowLayer("toDate", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("toDate", "none");
        }
        else
            ShowLayer("toDate", "none");

        if (valid == 1)
        {
            document.forms["expense_planning"].display.value            = 1;
            document.forms["expense_planning"].submit();
        }
    }

    function updateInfo()
    {
        document.forms["expense_planning"].update.value                 = 1;
        document.forms["expense_planning"].submit();
    }
</script>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="expense_planning">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Expense Planning</h6>
                            </td>
                        </tr>
                        <tr>
                            <td class="centerdata">
                                <br/>
                            </td>
                        </tr>
                    </table>
                    <!--
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                    Project Name:
                            </td>
                            <td width="50%">
                                <select class="on-field" method="post" id="project" name="project" onChange="submit();" tabindex="1">
                                    <option value="null">--  Select Project --</option>
                                    <?php
                                        /*if (is_array($projects)) {
                                            foreach ($projects as $project)
                                                if ($project_id == $project[0])
                                                    echo "<option value='".$project[0]."' selected>".$project[1]."</option>";
                                                else
                                                    echo "<option value='".$project[0]."'>".$project[1]."</option>";
                                        }*/
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <?php
                            /*if ($project_id != "null" && !$RA_VIEW) {
                                echo "<tr>";
                                    echo "<td class='on-description' width='50%'>";
                                            echo "Expense Description:";
                                    echo "</td>";
                                    echo "<td width='50%'>";
                                        echo "<input class='on-field' name='description' tabindex='2' type='text' value=''><div id='Description' style='display: none;'><font color='#FF5900'>* ".
                                            "Expense description must be entered</font></div>";
                                    echo "</td>";
                                echo "</tr>";
                                echo "<tr>";
                                    echo "<td class='on-description' width='50%'>";
                                            echo "Expense <i>(".$_SESSION["currency"].")</i>:";
                                    echo "</td>";
                                    echo "<td width='50%'>";
                                        echo "<input class='on-field' name='expense' style='text-align:right;' tabindex='3' type='text' value=''><font class='on-description-left'>* excl</font>".
                                            "<div id='expenseDiv' style='display: none;'><font color='#FF5900'>* Entered amount must be numeric, eg. 123.45</font></div>";
                                    echo "</td>";
                                echo "</tr>";
                                echo "<tr>";
                                    echo "<td class='on-description' width='50%'>";
                                            echo "VAT <i>(%)</i>:";
                                    echo "</td>";
                                    echo "<td width='50%'>";
                                        echo "<input class='on-field-date' name='VAT' style='text-align:right;' tabindex='4' type='text' value='14'><div id='vatDiv' style='display: none;'>".
                                            "<font color='#FF5900'>* Entered amount must be a percentage, eg. 100%</font></div>";
                                    echo "</td>";
                                echo "</tr>";
                                echo "<tr>";
                                    echo "<td class='on-description' width='50%'>";
                                            echo "Due Date:";
                                    echo "</td>";
                                    echo "<td width='50%'>";
                                        echo "<input class='on-field-date' id='due_date' name='due_date' tabindex='5' type='text' style='text-align:right;' value='".getDates($today, 30)."'>".
                                            "<div id='dueDate' style='display: none;'><font color='#FF5900'>* Date must be entered</font></div>".
                                            "<div id='dueDateDiv' style='display: none;'><font color='#FF5900'>* Date not valid, eg. ".date("Y-m-d")."></font></div>";
                                    echo "</td>";
                                echo "</tr>";
                                echo "<tr>";
                                    echo "<td class='centerdata' colspan='2'>";
                                        echo "<br/><input name='btnAdd' onClick='check();' tabindex='6' type='button' value='Load Expense'><br/><br/>";
                                    echo "</td>";
                                echo "</tr>";
                            }*/
                        ?>
                    </table>
                    <br/>
                    -->
                    <table width="100%">
                        <tr>
                            <td colspan="2" class="centerdata">
                                <h6>
                                       --  Display Information  --<br/><br/>
                                </h6>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                    Project Name:
                            </td>
                            <td width="50%">
                                <select class="on-field" method="post" id="project" name="project" tabindex="1">
                                    <option value="null">All Projects</option>
                                    <?php
                                        if (is_array($projects)) {
                                            foreach ($projects as $project)
                                                if ($project_id == $project[0])
                                                    echo "<option value='".$project[0]."' selected>".$project[1]."</option>";
                                                else
                                                    echo "<option value='".$project[0]."'>".$project[1]."</option>";
                                        }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                Expense Type:
                            </td>
                            <td width="50%">
                                <select class="on-field" method="post" name="display_type" tabindex="7">
                                    <?php
                                        if ($display_type == "all") {
                                            echo "<option value='all' selected>All</option>";
                                            echo "<option value='0'>Unpaid</option>";
                                            echo "<option value='1'>Paid</option>";
                                        }
                                        else if ($display_type == "0") {
                                            echo "<option value='all'>All</option>";
                                            echo "<option value='0' selected>Unpaid</option>";
                                            echo "<option value='1'>Paid</option>";
                                        }
                                        else {
                                            echo "<option value='all'>All</option>";
                                            echo "<option value='0'>Unpaid</option>";
                                            echo "<option value='1' selected>Paid</option>";
                                        }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                From Date:
                            </td>
                            <td width="50%">
                                <input class="on-field-date" id="from_date" name="from_date" tabindex="8" type="text" style="text-align:right;" value="<?php echo "".$from_date; ?>">
                                <div id="fromDate" style="display: none;"><font color="#FF5900">*
                                From date not valid, eg. <?php echo "".$from_date; ?></font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                To Date:
                            </td>
                            <td width="50%">
                                <input class="on-field-date" id="to_date" name="to_date" tabindex="9" type="text" style="text-align:right;" value="<?php echo "".$to_date; ?>">
                                <div id="toDate" style="display: none;"><font color="#FF5900">*
                                To date not valid, eg. <?php echo "".$to_date; ?></font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="centerdata" colspan="2"><br>
                                <input name="btnGo" onClick="check_dates();" tabindex="10" type="button" value="Display Information">
                            </td>
                        </tr>
                    </table>
                    <?php
                        $project = addslashes(strip_tags($_POST["project"]));

                        $project = ($project != "" && is_numeric($project)) ? "AND p.id = '".$project."' " : "";

                        if ($display_type == "all") {
                            $expenses = q("SELECT eb.id, p.id, p.name, eb.descr, eb.amount, eb.vat, eb.due_date, eb.invoice_id, eb.paid ".
                                            "FROM (ExpenseBudget AS eb INNER JOIN Project AS p ON p.id = eb.project_id) ".
                                            "WHERE (eb.due_date BETWEEN '$from_date' AND '$to_date' OR eb.due_date = '') ".
                                            "AND eb.company_id = '".$_SESSION["company_id"]."' $project ORDER BY eb.due_date");
                        }
                        else {
                            $expenses = q("SELECT eb.id, p.id, p.name, eb.descr, eb.amount, eb.vat, eb.due_date, eb.invoice_id, eb.paid ".
                                            "FROM (ExpenseBudget AS eb INNER JOIN Project AS p ON p.id = eb.project_id) ".
                                            "WHERE eb.paid = '$display_type' AND (eb.due_date BETWEEN '$from_date' ".
                                            "AND '$to_date' OR eb.due_date = '') AND eb.company_id = '".$_SESSION["company_id"]."' $project ".
                                            "ORDER BY eb.due_date");
                        }

                        $total_excl = 0;
                        $total_incl = 0;

                        if (is_array($expenses)) {
                            echo "<br/>";
                            echo "<div class='on-20px'><table class='on-table-center on-table'>";
                                //  Table Headings
                                echo "<tr>";
                                    echo "<th>Project Name</th>";
                                    echo "<th>Expense Description</th>";
                                    echo "<th>Expense <i>(".$_SESSION["currency"].") (excl)</i></th>";
                                    echo "<th>Expense <i>(".$_SESSION["currency"].") (incl)</i></th>";
                                    echo "<th>Due Date</th>";
                                    echo "<th>Linked Invoice Paid</th>";
                                    echo "<th>Paid</th>";
                                    echo "<th>Split</th>";
                                echo "</tr>";
                                //  Table Information
                                foreach ($expenses as $expense) {
                                    echo "<tr>";
                                        echo "<td style='white-space:nowrap;'>".$expense[2]."</td>";
                                        echo "<td>";
                                            if (!$RA_VIEW)      echo "<a href='expense_edit.php?id=".$expense[0]."'>".$expense[3]."</a>";
                                            else                echo "<a>".$expense[3]."</a>";
                                        echo "</td>";
                                        if ($expense[4] != "") {
                                            $total_excl += $expense[4];

                                            echo "<td class='rightdata'>".number_format($expense[4], 2, ".", "")."</td>";
                                        }
                                        else
                                            echo "<td class='rightdata'>-</td>";
                                        if ($expense[4] != "") {
                                            $incl_vat = $expense[4] + ($expense[4] * ($expense[5] / 100));
                                            $total_incl += $incl_vat;

                                            echo "<td class='rightdata'>".number_format($incl_vat, 2, ".", "")."</td>";
                                        }
                                        else
                                            echo "<td class='rightdata'>-</td>";
                                        if ($expense[6] != "")
                                            echo "<td class='rightdata'><input name='date".$expense[0]."' ".
                                                "type='text' style='text-align:right;' value='".$expense[6]."'></td>";
                                        else
                                            echo "<td class='rightdata'>-</td>";
                                        if ($expense[7] != "0") {
                                            $paid = q("SELECT paid FROM LoadInvoices WHERE id = '".$expense[7]."'");

                                            if ($paid == "1")
                                                echo "<td class='centerdata'><input name='paid".$expense[0]."' type='checkbox' value='1' ".
                                                    "disabled checked></td>";
                                            else
                                                echo "<td class='centerdata'><input name='paid".$expense[0]."' type='checkbox' value='1' ".
                                                    "disabled></td>";
                                        }
                                        else
                                            echo "<td class='centerdata'>-</td>";
                                        if ($expense[8] == "1")
                                            echo "<td class='centerdata'><input name='box".$expense[0]."' type='checkbox' value='1' ".
                                                "checked></td>";
                                        else
                                            echo "<td class='centerdata'><input name='box".$expense[0]."' type='checkbox' value='1'>".
                                                "</td>";

                                        echo "<td>";
                                            if ($expense[8] == "1")     echo "<input name='btnSplit' type='button' value='Split' disabled>";
                                            else                        echo "<input name='btnSplit' type='button' value='Split' onClick=\"location.href='expense_split.php?project=".$expense[1]."&id=".$expense[0]."'\">";
                                        echo "</td>";
                                    echo "</tr>";
                                }

                                echo "<tr><td class='on-table-total' colspan='2'>Grand Total ".
                                    "<i>(".$_SESSION["currency"].")</i>:</td><td class='on-table-total'>".
                                    number_format($total_excl, 2, ".", "")."</td><td class='on-table-total'>".
                                    number_format($total_incl, 2, ".", "")."</td><td class='on-table-total' colspan='4'>".
                                    "</td></tr><tfoot><tr><td colspan='100%'></td></tr></tfoot></table></div>";
                                    
                                echo "<table class='on-table-center'><tr>";
                                    echo "<td colspan='100%'><br/><input name='btnUpdate' onClick='updateInfo();' type='button' value='Update Information'></td>";
                                echo "</tr>";
                            echo "</table>";
                        }
                    ?>
                    <br/>
                    <input method="post" name="display" type="hidden" value="0" />
                    <input method="post" name="save" type="hidden" value="0" />
                    <input method="post" name="update" type="hidden" value="0" />
                </form>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
