<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    if (!hasAccess("SPM_MANAGE"))
        header("Location: noaccess.php");

    $index = 1;

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0");
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    var sp_areas_LOAD;
    var project = "<?php echo $_GET["project"]; ?>";

    jQuery(function()    {
        sp_areas_LOAD = function(e)  {
            jQuery.post("_ajax.php", {func: "get_sp_areas", spID: e}, function(data)	{
                data = json_decode(data);
                var displayBool = false;
                jQuery("#tableContent tbody>tr").not(":first").not(":last").remove();
                var lstRow = jQuery("#tableContent tbody>tr:last");

                jQuery.each(data,function(i, v)	{
                    var n = lstRow.clone(true);

                    n.children().eq(0).children().eq(0).text((v[1] != null) ? v[1] : "");
                    n.children().eq(0).children().eq(0).attr("id", (v[0] != null) ? v[0] : "");
                    n.children().eq(0).children().eq(0).attr("name", (v[0] != null) ? v[0] : "");

                    var subs = "";

                    jQuery.each(v[2],function(j, w)	{
                        var cls = (w[2] == 1) ? "active" : "inactive";
                        var val = (w[1] != null) ? w[1] : "";

                        if (w[1] == "-")        displayBool = false;
                        else                    displayBool = true;

                        subs += "<a class='"+cls+"'>"+val+"</a><br/>";
                    });

                    n.children().eq(1).html(subs);

                    for (var a = 0; a < 4; a++) {
                        var subs = "";

                        jQuery.each(v[2],function(j, w)	{
                            var val = (w[3 + a] != null) ? w[3 + a] : "";

                            subs += ""+val+"<br/>";
                        });

                        n.children().eq(2 + a).html(subs);
                    }

                    n.insertBefore(lstRow);
                });

                if (data != "")         lstRow.remove();

                if (displayBool)        jQuery("#informationDiv").show();
                else                    jQuery("#informationDiv").hide();

                jQuery("#addBtnDiv").show();
            });
        };

        if (project != "" && validation("number", project))     sp_areas_LOAD(project);

        jQuery("#project").change(function() {
            if (jQuery(this).val() != "null")        sp_areas_LOAD(jQuery(this).val());
            else                                jQuery("#informationDiv").hide();

            if (jQuery(this).val() != "null")        jQuery("#addBtnDiv").show();
            else                                jQuery("#addBtnDiv").hide();
        });

        jQuery(".btnAdd").click(function() {
            jQuery("#id").val(jQuery("#project").val());
            jQuery("#form").attr("action", "sp_areasAddEdit.php");
            jQuery("#form").submit();
        });

        jQuery(".btnEdit").click(function() {
            jQuery("#id").val(jQuery(this).attr("id"));
            jQuery("#form").attr("action", "sp_areasAddEdit.php");
            jQuery("#form").submit();
        });
    });
</script>
<table width="100%">
    <tr height="380px">
        <td class="centerdata">
            <form id="form" name="form" action="" method="post">
                <table width="100%">
                    <tr>
                        <td class="centerdata">
                            <h6>Shared Project Areas</h6>
                        </td>
                    </tr>
                </table>
                <br/>
                <table width="100%">
                    <tr>
                        <td class="on-description" width="50%">
                            Shared Project:
                        </td>
                        <td style="padding-top:2px;" width="50%">
                            <select class="on-field" id="project" name="project" method="post" tabindex="<?php echo $index++; ?>">
                                <option value="null">--  Select A Shared Project  --</option>
                                <?php
                                    if ($_SESSION["email"] != "admin")   $where .= "AND pu.user_id = '".$_SESSION["user_id"]."' AND pu.spm_manager = '1' ";

                                    $projects = q("SELECT DISTINCT(p.id),p.name FROM (Project AS p INNER JOIN Project_User AS pu ON p.id = pu.project_id INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) 
                                                    WHERE p.p_type = 'SP' AND p.completed = '0' AND cot.company_id = '".$_SESSION["company_id"]."' AND pu.company_id  = '".$_SESSION["company_id"]."' 
                                                    AND p.company_id = '".$_SESSION["company_id"]."' $where ORDER BY p.name");

                                    if (is_array($projects))
                                        foreach ($projects as $project)
                                            if ($_GET["project"] == $project[0])
                                                echo "<option value='".$project[0]."' selected>".$project[1]."</option>";
                                            else
                                                echo "<option value='".$project[0]."'>".$project[1]."</option>";
                                ?>
                            </select>
                        </td>
                    </tr>
                </table>
                <br/><br/>
                <div id="informationDiv" name="informationDiv" class="hidden" style='display:none'>
                    <div class="on-20px">
                    <table id="tableContent" class="on-table-center on-table">
                        <tr>
                            <th>Shared Project</th>
                            <th>Area(s)</th>
                            <th>Date Created</th>
                            <th>Created By</th>
                            <th>Date Updated</th>
                            <th>Updated By</th>
                        </tr>
                        <!--  Table Information   -->
                        <?php                    
                            echo "<tr>";
                                echo "<td>";
                                    echo "<a name='projLink' id='projLink' class='btnEdit' style=' cursor: hand; tabindex='".$index++."'></a>";
                                    //echo "<a onMouseOver='showHand()' name='projLink' id='projLink' class='btnEdit' style='".$link."' tabindex='".$index++."'></a>";
                                echo "</td>";
                                echo "<td style='white-space:nowrap'></td>";
                                echo "<td style='white-space:nowrap'></td>";
                                echo "<td></td>";
                                echo "<td style='white-space:nowrap'></td>";
                                echo "<td></td>";
                            echo "</tr>";
                        ?>
                    </table>
                    </div>
                </div>
                <div id="addBtnDiv" name="addBtnDiv" class="hidden" style='display:none'>
                    <input id="btnAdd" name="btnAdd" class="btnAdd" tabindex="<?php echo $index++; ?>" type="button" value="Add New">
                </div>
                <input id="id" name="id" type="hidden" value="">
            </form>
        </td>
    </tr>
    <tr>
        <td>
            <br/>
        </td>
    </tr>
</table>
<?php
    //  Print Footer
    print_footer();
?>
