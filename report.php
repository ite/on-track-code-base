<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    
    unset($_SESSION["fileName"]);
    unset($_SESSION["fileData"]);

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");
        
    if (!hasAccess("REP_TIME_EX"))
        header("Location: noaccess.php");
        
    // Class for table background color
    $colorClass = "OnTrack_#_";

    //  Set Report Generated Status
    $generated = "0";

    //  Create Display Information Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")        {
        $errorMessage = "";

        $employee_id = $_SESSION["user_id"];
        $project_id = null;
        $area_id = null;
        $activity_type_id = null;
        $activity_id = null;
        $date_type = $_POST["dateType"];

        //  Expense Info
        $expenseType = null;
        $vehicle_id = null;
        $printingType_id = null;
        $printing_id = null;

        //  Get Dates
        if ($date_type == "currentwk")  {
            $date_from = currentWeekStart();
            $date_to = $today;
        }
        else if ($date_type == "previouswk")    {
            $date_from = getDates(currentWeekStart(), -7);
            $date_to = getDates(currentWeekStart(), -1);
        }
        else if ($date_type == "currentmnth")   {
            $date_from = getMonthStart($today);
            $date_to = $today;
        }
        else if ($date_type == "previousmnth")  {
            $date_from = getPreviousMonthStart();
            $date_to = getMonthEnd(getPreviousMonthStart());
        }
        else if ($date_type == "custom")        {
            $date_from = addslashes(strip_tags($_POST["date_from"]));
            $date_to = addslashes(strip_tags($_POST["date_to"]));
        }

        $employee_info = q("SELECT lstname, frstname FROM Employee WHERE id = '$employee_id'");
        $project_info = "All Projects";

        //  Timestamp
        $timestamp = ($date_from === $date_to) ? "Time Period: ".$date_from : "Time Period: ".$date_from." - ".$date_to;

        $total_hours = 0;
        $expense_total = 0;

        $hasAreas = 0;

        $areasTime = q("SELECT project_id, area_id FROM TimeSheet WHERE user_id = ".$_SESSION["user_id"]." AND area_id > 0 AND date >= '".$date_from."' AND date <= '".$date_to."'");
        $areasExpense = q("SELECT project_id, area_id FROM ExpenseSheet WHERE user_id = ".$_SESSION["user_id"]." AND area_id > 0 AND date >= '".$date_from."' AND date <= '".$date_to."'");

        if (is_array($areasTime) || is_array($areasExpense))    {
            $hasAreas = 1;
        } else  {
            $hasAreas = 0;
        }

        if ($hasAreas)  {
            $select = "ar.name,";
        }

        //  Get Information - Time
        $timeInfo = q("SELECT p.name,".$select."at.type,a.name,ts.descr,ts.date,ts.time_spent,ts.time_spent * ts.rate
                        FROM (TimeSheet AS ts 
                            INNER JOIN Activities AS a ON ts.activity_id = a.id 
                            INNER JOIN ActivityTypes AS at ON a.parent_id = at.id 
                            INNER JOIN Project AS p ON ts.project_id = p.id 
                            LEFT JOIN areas AS ar ON ts.area_id = ar.id ) 
                        WHERE ts.user_id = '".$employee_id."' AND ts.date >= '".$date_from."' AND ts.date <= '".$date_to."' AND ts.company_id = '".$_SESSION["company_id"]."' 
                        ORDER BY ts.date, ts.on_date, ts.on_time, p.name");

        //  Get Information - Expense
        $expenseInfo = q("SELECT p.name,dd.name,IF(dd.name = 'Driving',v.type,IF(dd.name = 'Printing',CONCAT(dt.type,': ',d.name),es.otherDescr)),".$select."at.type,a.name,es.descr,es.date,
                                IF(dd.name = 'Driving',es.kilometers,es.total), es.rate, es.expense 
                            FROM (ExpenseSheet AS es
                                INNER JOIN dropdowns AS dd ON es.expense_type_id = dd.id 
                                LEFT JOIN Activities AS a ON es.activity_id = a.id 
                                LEFT JOIN ActivityTypes AS at ON a.parent_id = at.id 
                                INNER JOIN Project AS p ON es.project_id = p.id 
                                LEFT JOIN areas AS ar ON es.area_id = ar.id
                                LEFT JOIN Vehicle AS v ON es.vehicle_id = v.id
                                LEFT JOIN Disbursements AS d ON es.disbursement_id = d.id
                                LEFT JOIN DisbursementTypes AS dt ON dt.id = d.parent_id)
                            WHERE es.user_id = '".$employee_id."' AND es.date >= '".$date_from."' AND es.date <= '".$date_to."' AND es.company_id = '".$_SESSION["company_id"]."' 
                            ORDER BY es.date, es.on_date, es.on_time, p.name");

        //  Create Headings
        $reportHead = array("<a><font style='color:orange'>Time & Expense</font> Report For: ".$employee_info[0][1].", ".$employee_info[0][0]."</a>","Project: ".$project_info,$timestamp);

        if ($hasAreas)  {
            $timeHeadings = array("Project Name","Area","Activity Type","Activity","Description","Date","Time Spent","Cost <i>(".$_SESSION["currency"].")</i>");
            $timeHeadings2 = array("Project Name","Area","","","Activity Type","Activity","Description","Date","Time Spent","","Cost <i>(".$_SESSION["currency"].")</i>");
            $expenseHeadings = array("Project Name","Expense Type","Expense Info","Area","Activity Type","Activity","Description","Date","Quantity","Rate","Cost <i>(".$_SESSION["currency"].")</i>");
        } else  {
            $timeHeadings = array("Project Name","Activity Type","Activity","Description","Date","Time Spent","Cost <i>(".$_SESSION["currency"].")</i>");
            $timeHeadings2 = array("Project Name","","","Activity Type","Activity","Description","Date","Time Spent","","Cost <i>(".$_SESSION["currency"].")</i>");
            $expenseHeadings = array("Project Name","Expense Type","Expense Info","Activity Type","Activity","Description","Date","Quantity","Rate","Cost <i>(".$_SESSION["currency"].")</i>");
        }

        //  Create Display String
        if (is_array($timeInfo) || is_array($expenseInfo))    {
            $timeColumns = count($timeHeadings) - 1;
            $expenseColumns = count($expenseHeadings) - 1;

            $columns = (!is_array($expenseInfo)) ? $timeColumns : $expenseColumns;
            $hasExpense = (is_array($expenseInfo)) ? 1 : 0;
            $extraCounter = 0;
            if(!$hasExpense)
                $extraCounter  = 3;

            $displayString = "";
            $row = 0;

            //  HTML Display
            foreach ($reportHead as $rh)
                $displayString .= "<tr><td class='on-table-clear' colspan='100%'>".$rh."</td></tr>";

            //  EXCEL DISPLAY
            foreach ($reportHead as $rh)        {
                $exceldata[$row][] = strip_tags($rh);

                for ($a = 0; $a < $columns+$extraCounter; $a++)       $exceldata[$row][] = "";

                $row++;
            }

            if (is_array($timeInfo))    {
                $total_hours = 0;
                $total_cost = 0;

                //  Headings
                //  HTML Display
                $displayString .= "<tr>";

                foreach ($timeHeadings as $h)   {
                    $colspan = ($h == "Project Name") ? 3 : (($h == "Time Spent") ? 2 : 1);

                    $displayString .= "<th colspan='".$colspan."'>".$h."</th>";           
                }

                $displayString .= "</tr>";

                //  EXCEL DISPLAY
                foreach ($timeHeadings2 as $h)
                    $exceldata[$row][] = $colorClass.strip_tags($h);

                for ($a = count($timeHeadings); $a < $columns; $a++)    $exceldata[$row][] = $colorClass."";

                $row++;

                //  Data
                foreach ($timeInfo as $r)       {
                    $col = 0;
                    $displayString .= "<tr>";

                    foreach ($r as $d) {
                        $colspan = ($timeHeadings[$col] == "Project Name") ? 3 : (($timeHeadings[$col] == "Time Spent") ? 2 : 1);

                        if ($timeHeadings[$col] == "Time Spent")                                $total_hours += $d;
                        if ($timeHeadings[$col] == "Cost <i>(".$_SESSION["currency"].")</i>")   $total_cost += $d;

                        if (preg_match("/^((\d{2}(([02468][048])|([13579][26]))[\-\/\s]?((((0?[13578])|(1[02]))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-\/\s]?((0?[1-9])|([1-2][0-9])))))|(\d{2}(([02468][1235679])|([13579][01345789]))[\-\/\s]?((((0?[13578])|(1[02]))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-\/\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))(\s(((0?[1-9])|(1[0-2]))\:([0-5][0-9])((\s)|(\:([0-5][0-9])\s))([AM|PM|am|pm]{2,2})))?$/", $d))
                            $displayString .= "<td colspan='".$colspan."' style='white-space:nowrap'>".$d."</td>";
                        else if (is_numeric($d))
                            $displayString .= "<td colspan='".$colspan."' style='white-space:nowrap' class='rightdata'>".number_format($d, 2, ".", "")."</td>";
                        else if ($d == "")
                            $displayString .= "<td colspan='".$colspan."' style='white-space:nowrap'>-</td>";
                        else
                            $displayString .= "<td colspan='".$colspan."' style='white-space:nowrap'>".$d."</td>";

                        $exceldata[$row][] .= $d;
                        if($timeHeadings[$col] == "Project Name")   {
                            $exceldata[$row][] .= "";
                            $exceldata[$row][] .= "";
                        }
                        if($timeHeadings[$col] == "Time Spent")   {
                            $exceldata[$row][] .= "";
                        }
                        $col++;
                    }

                    for ($a = count($timeHeadings) - 1; $a < $columns; $a++)    $exceldata[$row][] = "";

                    $displayString .= "</tr>";
                    $row++;
                }

                $colspan = ($hasAreas) ? 8 : 7;

                $displayString .= "<tr>
                                        <td class='on-table-total' colspan='".$colspan."><a style='color:#FFFFFF;'>Total <i>(".$_SESSION["currency"].")</a></td>
                                        <td class='on-table-total' colspan='2'>".number_format($total_hours, 2, ".", "")."</td>
                                        <td class='on-table-total'>".number_format($total_cost, 2, ".", "")."</td>
                                    </tr>";

                $colspan = ($hasAreas) ? 7 : 6;

                for ($a = 0; $a < $colspan; $a++)   $exceldata[$row][] = "";

                $exceldata[$row][] .= "Total (".$_SESSION["currency"]."):";
                $exceldata[$row][] .= $total_hours;
                $exceldata[$row][] .= "";
                $exceldata[$row][] .= $total_cost;

                for ($a = count($timeHeadings) - 1; $a < $columns; $a++)    $exceldata[$row][] = "";

                $row++;

                if (is_array($expenseInfo))     $displayString .= "<tr><td style='background-color:transparent' colspan='100%'>&nbsp;</td></tr>";
            }

            if (is_array($expenseInfo))    {
                $total_cost = 0;

                //  Headings
                //  HTML Display
                $displayString .= "<tr>";

                foreach ($expenseHeadings as $h)   {
                    $displayString .= "<th>".$h."</th>";
                }

                $displayString .= "</tr>";

                //  EXCEL DISPLAY
                foreach ($expenseHeadings as $h)
                    $exceldata[$row][] = $colorClass.strip_tags($h);

                $row++;

                //  Data
                foreach ($expenseInfo as $r)       {
                    $col = 0;
                    $displayString .= "<tr>";

                    foreach ($r as $d) {
                        if ($expenseHeadings[$col] == "Cost <i>(".$_SESSION["currency"].")</i>")   $total_cost += $d;

                        if (preg_match("/^((\d{2}(([02468][048])|([13579][26]))[\-\/\s]?((((0?[13578])|(1[02]))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-\/\s]?((0?[1-9])|([1-2][0-9])))))|(\d{2}(([02468][1235679])|([13579][01345789]))[\-\/\s]?((((0?[13578])|(1[02]))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-\/\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))(\s(((0?[1-9])|(1[0-2]))\:([0-5][0-9])((\s)|(\:([0-5][0-9])\s))([AM|PM|am|pm]{2,2})))?$/", $d))
                            $displayString .= "<td style='white-space:nowrap'>".$d."</td>";
                        else if (is_numeric($d))
                            $displayString .= "<td style='white-space:nowrap' class='rightdata'>".number_format($d, 2, ".", "")."</td>";
                        else if ($d == "")
                            $displayString .= "<td style='white-space:nowrap'>-</td>";
                        else
                            $displayString .= "<td style='white-space:nowrap'>".$d."</td>";

                        $exceldata[$row][] .= $d;

                        $col++;
                    }

                    $displayString .= "</tr>";
                    $row++;
                }

                $colspan = ($hasAreas) ? 10 : 9;

                $displayString .= "<tr>
                                        <td class='on-table-total' colspan='".$colspan."><a style='color:#FFFFFF;'>Total <i>(".$_SESSION["currency"].")</a></td>
                                        <td class='on-table-total'>".number_format($total_cost, 2, ".", "")."</td>
                                    </tr>";

                for ($a = 0; $a < $colspan - 1; $a++)   $exceldata[$row][] = "";

                $exceldata[$row][] .= "Total (".$_SESSION["currency"]."):";
                $exceldata[$row][] .= $total_cost;

                $row++;
            }
        }

        if ($displayString != "")
            $generated = "1";
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0", "reports");
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    jQuery(function(){
	jQuery('#date_from').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_from').val(),
		current: jQuery('#date_from').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_from').val()))
			_date = new Date();
			else _date = jQuery('#date_from').val();
			jQuery('#date_from').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_from').val(formated);
			jQuery('#date_from').DatePickerHide();
		}
	});
    })

    jQuery(function(){
	jQuery('#date_to').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_to').val(),
		current: jQuery('#date_to').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_to').val()))
			_date = new Date();
			else _date = jQuery('#date_to').val();
			jQuery('#date_to').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_to').val(formated);
			jQuery('#date_to').DatePickerHide();
		}
	});
    })

    function check()
    {
        var valid                                                       = 1;

        if (document.forms["report"].dateType.value == "custom")
        {
            //  Check That Date From Is Entered
            if (document.forms["report"].date_from.value == "")
            {
                ShowLayer("dateFrom", "block");
                valid                                                   = 0;
            }
            //  Check That Entered Date From Is Valid
            else
            {
                ShowLayer("dateFrom", "none");

                if (!validation("date", document.forms["report"].date_from.value))
                {
                    ShowLayer("dateFromDiv", "block");
                    valid                                               = 0;
                }
                else
                    ShowLayer("dateFromDiv", "none");
            }

            //  Check That Date To Is Entered
            if (document.forms["report"].date_to.value == "")
            {
                ShowLayer("dateTo", "block");
                valid                                                   = 0;
            }
            //  Check That Entered Date To Is Valid
            else
            {
                ShowLayer("dateTo", "none");

                if (!validation("date", document.forms["report"].date_to.value))
                {
                    ShowLayer("dateToDiv", "block");
                    valid                                               = 0;
                }
                else
                    ShowLayer("dateToDiv", "none");
            }
        }

        if (valid == 1)
        {
            document.forms["report"].save.value                         = 1;
            document.forms["report"].submit();
        }
    }

    function dateSelection()
    {
        if (document.forms["report"].dateType.value == "custom")
            ShowLayer("datesDiv", "block");
        else
            ShowLayer("datesDiv", "none");
    }
</script>
    <table width="100%">
        <tr>
            <td class="centerdata">
                <form action="" method="post" name="report">
                    <div id="" style="<?php if ($generated == "1") echo "display: none;"; else echo "display: block"; ?>">
                        <table width="100%">
                            <tr>
                                <td class="centerdata">
                                    <h6>Personal Time &amp; Expense Report</h6>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Date Type(s):
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="dateType" onChange="dateSelection();">
                                        <option value="currentwk">Current Week</option>
                                        <option value="previouswk">Previous Week</option>
                                        <option value="currentmnth">Current Month</option>
                                        <option value="previousmnth">Previous Month</option>
                                        <option value="custom">Custom Dates</option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                        <div id="datesDiv" style="display: none;">
                            <table width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Date From:
                                    </td>
                                    <td width="50%">
                                        <input class="on-field-date" id="date_from" name="date_from" type="text" style="text-align:right;" value="<?php
                                            if ($_POST["date_from"] != "") echo "".$_POST["date_from"]; else echo "".getMonthStart($today); ?>">
                                        <div id="dateFrom" style="display: none;"><font color="#FF5900">* Date must be entered</font></div>
                                        <div id="dateFromDiv" style="display: none;"><font color="#FF5900">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="on-description" width="50%">
                                        Date To:
                                    </td>
                                    <td>
                                        <input class="on-field-date" id="date_to" name="date_to" type="text" style="text-align:right;" value="<?php
                                            if ($_POST["date_to"] != "") echo "".$_POST["date_to"]; else echo "".getMonthEnd($today); ?>">
                                        <div id="dateTo" style="display: none;"><font color="#FF5900">* Date must be entered</font></div>
                                        <div id="dateToDiv" style="display: none;"><font color="#FF5900">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <br/>
                        <input name="btnGenerate" onClick="check();" type="button" value="Generate Report">
                        <input method="post" name="save" type="hidden" value="0" />
                    </div>
                </form>
                <div id="report" style="<?php if ($generated == "1") echo "display: block;"; else echo "display: none"; ?>">
                    <?php
                        echo "<div class='on-20px'><table class='on-table-center on-table'>";
                            echo "<h6>Personal Time &amp; Expense Report</h6>";
                            echo "".$displayString;
                            echo  "<tfoot>
                                            <tr>
                                                <td colspan='100%'></td>
                                            </tr>
                                        </tfoot>";
                        echo "</table>
                                </div>";
                        echo "<br/>";

                        ///////////////////////////                        
                        $_SESSION["fileName"] = "Time_Report";
                        $_SESSION["fileData"] = $exceldata;
                        ///////////////////////////

                        echo "<input name='btnExport' onClick=\"location.href='_export.php'\" type='button' value='Export to Excel' />";
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>