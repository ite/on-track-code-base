<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("_functions.php");
    include("fusion/FusionCharts.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");
        
    if (!hasAccess("REP_BUDGET"))
        header("Location: noaccess.php");
        
    // Class for table background color
    $colorClass = "OnTrack_#_";

    //  Set Report Generated Status
    $generated = "0";

    function createPieChartXML($elements,$chartTotals,$on)     {
        //  Create XML String
        $xmlString = "";

        //  Calculate Values
        if (is_array($elements)) {
            $xmlString = "<chart caption='Running Cost Breakdown Report (".ucfirst(strtolower($on)).")' palette='4' decimals='2' enableSmartLabels='1' showBorder='1' ".fusionChart().">";

            foreach ($elements as $e)   {
                $eName = removeSC($e[1]);

                $xmlString .= "<set label='".$eName."' value='".$chartTotals[$on][$e[0]]."'/>";
            }

            $xmlString .= "</chart>";
        }

        return $xmlString;
    }

    //  Create Display Information Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")        {
        unset($totals);
        $displayString = "";

        $dateFrom = addslashes(strip_tags($_POST["dateFrom"]));
        $dateTo = addslashes(strip_tags($_POST["dateTo"]));
        $category = addslashes(strip_tags($_POST["category"]));

        //  Timestamp
        if ($dateFrom === $dateTo)
            $timestamp = "Period: ".date("M Y",strtotime($dateFrom."-01"));
        else
            $timestamp = "Period: ".date("M Y",strtotime($dateFrom."-01"))." - ".date("M Y",strtotime($dateTo."-01"));

        $total = getMonthPeriod($dateFrom."-01", $dateTo."-01");

        $displayString .= "<tr><td class='on-table-clear'><a>Running Cost Breakdown Report</a></td></tr><tr><td>".$timestamp."</td></tr>";

        if ($category == "all") {
            $displayString .= "<tr><td>Running Cost Category: All Categories</td></tr>";

            $categories = q("SELECT id, name FROM Category WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY name");

            if (is_array($categories))  {
                foreach ($categories as $c)     {
                    $elements = q("SELECT id, name FROM Elements WHERE parent_id = '".$c[0]."' ORDER BY name");

                    if (is_array($elements))    {
                        foreach ($elements as $e)       {
                            for ($i = 0; $i < $total; $i++)     {
                                $date = getNextMonthStart($dateFrom."-01", $i);
                                $year = substr($date, 0, strpos($date, "-"));
                                $month = substr($date, (strpos($date, "-") + 1), 2);

                                $planned = q("SELECT planned FROM Budget WHERE element_id = '".$e[0]."' AND year = '".$year."' AND month = '".$month."'");
                                $actual = q("SELECT amount FROM Budget WHERE element_id = '".$e[0]."' AND year = '".$year."' AND month = '".$month."'");

                                $totals["planned"][$c[0]] += $planned;
                                $totals["actual"][$c[0]] += $actual;
                            }
                        }
                    }
                }

                $graph1 = createPieChartXML($categories,$totals,"planned");
                $graph2 = createPieChartXML($categories,$totals,"actual");
            }
        }
        else    {
            $categoryName = q("SELECT name FROM Category WHERE id = '".$category."'");

            $displayString .= "<tr><td>Running Cost Category: ".$categoryName."</td></tr>";

            $elements = q("SELECT id, name FROM Elements WHERE parent_id = '".$category."' ORDER BY name");

            if (is_array($elements))    {
                foreach ($elements as $e)       {
                    for ($i = 0; $i < $total; $i++)     {
                        $date = getNextMonthStart($dateFrom."-01", $i);
                        $year = substr($date, 0, strpos($date, "-"));
                        $month = substr($date, (strpos($date, "-") + 1), 2);

                        $planned = q("SELECT planned FROM Budget WHERE element_id = '".$e[0]."' AND year = '".$year."' AND month = '".$month."'");
                        $actual = q("SELECT amount FROM Budget WHERE element_id = '".$e[0]."' AND year = '".$year."' AND month = '".$month."'");

                        $totals["planned"][$e[0]] += $planned;
                        $totals["actual"][$e[0]] += $actual;
                    }
                }

                $graph1 = createPieChartXML($elements,$totals,"planned");
                $graph2 = createPieChartXML($elements,$totals,"actual");
            }
        }

        if ($graph1 != "")   $displayString .= "<tr><td class='on-table-clear'>".renderChart("fusion/Pie3D.swf", "", $graph1, "multigraph1", 800, 400, false, false)."</td></tr>";
        if ($graph2 != "")   $displayString .= "<tr><td class='on-table-clear'>".renderChart("fusion/Pie3D.swf", "", $graph2, "multigraph2", 800, 400, false, false)."</td></tr>";

        $generated = "1";
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("5", "reports");

    if ($errorMessage != "")
    {
        echo "<p align='center' style='padding:0px;'><strong><font color='#999999'>$errorMessage</font></strong></p>";
        echo "<br/>";
    }
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    function check()    {
        var valid = 1;

        if (valid == 1) {
            document.forms["report_budget"].save.value = 1;
            document.forms["report_budget"].submit();
        }
    }
</script>
<?php
    $budgetRecords = q("SELECT DISTINCT(CONCAT(year,'-',month)),month,year FROM Budget WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY year DESC,month DESC");
    $categories = q("SELECT id, name FROM Category WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY name");
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="report_budget">
                    <div id="" style="<?php if ($generated == "1") echo "display: none;"; else echo "display: block"; ?>">
                        <table width="100%">
                            <tr>
                                <td class="centerdata">
                                    <h6>Running Cost Breakdown Report</h6>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Date From:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" id="dateFrom" name="dateFrom">
                                        <?php
                                            if (is_array($budgetRecords))       {
                                                foreach ($budgetRecords as $br) {
                                                    $selected = (date("m") == $br[1] && date("Y") == $br[2]) ? "selected" : "";

                                                    echo "<option value='".$br[0]."' $selected>".getMonthString($br[1])." ".$br[2]."</option>";
                                                }
                                            }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Date To:
                                </td>
                                <td align="left" width="50%">
                                    <select class="on-field" method="post" id="dateTo" name="dateTo">
                                        <?php
                                            if (is_array($budgetRecords))       {
                                                foreach ($budgetRecords as $br) {
                                                    $selected = (date("m") == $br[1] && date("Y") == $br[2]) ? "selected" : "";

                                                    echo "<option value='".$br[0]."' $selected>".getMonthString($br[1])." ".$br[2]."</option>";
                                                }
                                            }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Category:
                                </td>
                                <td align="left" width="50%">
                                    <select class="on-field" method="post" id="category" name="category">
                                        <option value="all">All Categories</option>
                                        <?php
                                            if (is_array($categories))       {
                                                foreach ($categories as $c) {
                                                    echo "<option value='".$c[0]."'>".$c[1]."</option>";
                                                }
                                            }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <input name="btnGenerate" onClick="check();" type="button" value="Generate Report">
                        <input method="post" name="save" type="hidden" value="0" />
                    </div>
                </form>
                <div id="report" style="<?php if ($generated == "1") echo "display: block;"; else echo "display: none"; ?>">
                    <?php
                        echo "<div class='on-20px'><table class='on-table-center on-table'>";
                            echo "".$displayString;
                            echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        echo "</table></div>";
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>