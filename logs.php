<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

	if (!hasAccess("LOGS_VIEW_OWN") && !hasAccess("LOGS_VIEW_ALL"))
		header("Location: noaccess.php");

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("8", "logs");

    if ($errorMessage != "")
    {
        echo "<p align='center' style='padding:0px;'><strong><font color='#999999'>$errorMessage</font></strong></p>";
        echo "<br/>";
    }
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    jQuery(function(){
	jQuery('#display_date').DatePicker({
		format:'Y-m-d',
		date: jQuery('#display_date').val(),
		current: jQuery('#display_date').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#display_date').val()))
			_date = new Date();
			else _date = jQuery('#display_date').val();
			jQuery('#display_date').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#display_date').val(formated);
			jQuery('#display_date').DatePickerHide();
		}
	});
    })

    function display_check()
    {
        var valid                                                       = 1;

        //  Check That Employee Is Selected
        if (document.forms["employee_logs"].employee.value == "null")
        {
            ShowLayer("employeeDiv", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("employeeDiv", "none");

        // Check That Registration Date Is Entered
        if (document.forms["employee_logs"].display_date.value == "")
        {
            ShowLayer("displayDate", "block");
            valid                                                       = 0;
        }
        //  Check That Entered Registration Date Is Valid
        else
        {
            ShowLayer("displayDate", "none");

            if (!validation("date", document.forms["employee_logs"].display_date.value))
            {
                ShowLayer("displayDateDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("displayDateDiv", "none");
        }

        if (valid == 1)
        {
            document.forms["employee_logs"].save.value                  = 1;
            document.forms["employee_logs"].submit();
        }
    }
</script>
<?php
    $where = (!hasAccess("LOGS_VIEW_ALL") && hasAccess("LOGS_VIEW_OWN")) ? "AND e.id = '".$_SESSION["user_id"]."' " : "";
    $employees                                                          = q("SELECT e.id, e.lstname, e.frstname FROM (Employee AS e INNER JOIN Company_Users ".
                                                                            "AS cu ON e.id = cu.user_id) WHERE cu.company_id = '".$_SESSION["company_id"]."' ".
                                                                            "AND e.email != 'admin' $where ORDER BY e.lstname, frstname");
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="employee_logs">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Employee Logs</h6>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                Select Employee:
                            </td>
                            <td width="50%">
                                <select class="on-field" method="post" name="employee" tabindex="1">
                                    <option value="null">--  Select Employee  --&nbsp;&nbsp;</option>
                                    <?php
                                        if (is_array($employees))
                                            foreach ($employees as $employee)
                                                if ($_POST["employee"] == $employee[0])
                                                    echo "<option value='".$employee[0]."' selected>".$employee[1].", ".$employee[2]."&nbsp;&nbsp;</option>";
                                                else
                                                    echo "<option value='".$employee[0]."'>".$employee[1].", ".$employee[2]."&nbsp;&nbsp;</option>";
                                    ?>
                                </select>
                                <div id="employeeDiv" style="display: none;"><font class="on-validate-error">* Employee must be selected</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                Display Date:
                            </td>
                            <td width="50%">
                                <input class="on-field-date" id="display_date" name="display_date" tabindex="2" type="text" style="text-align:right;" value="<?php
                                    if ($_POST["display_date"] != "") echo "".$_POST["display_date"]; else echo "".$today; ?>">
                                <div id="displayDate" style="display: none;"><font class="on-validate-error">* Display date must be entered</font></div>
                                <div id="displayDateDiv" style="display: none;"><font class="on-validate-error">* Display date not valid, eg.<?php echo "".date("Y-m-d"); ?></font>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <input name="btnDisplay" onClick="display_check();" tabindex="3" type="button" value="Display Information">
                    <input method="post" name="save" type="hidden" value="0" />
                </form>
                <?php
                    //  Display Information Function
                    if (isset($_POST["save"]) && $_POST["save"] === "1")
                    {
                        echo "<form action='' method='post'>";
                            $employee_id                                = $_POST["employee"];
                            $display_date                               = addslashes(strip_tags($_POST["display_date"]));

                            ///////////////////////////
                            //  Get Information
                            $employee_email                             = q("SELECT email FROM Employee WHERE id = '$employee_id'");
                            $nLogs                                      = q("SELECT COUNT(id) FROM Logs WHERE by_user = '$employee_email' ".
                                                                            "AND on_date = '$display_date' AND company_id = '".$_SESSION["company_id"]."'");
                            $logs                                       = q("SELECT what, on_table, on_date, on_time FROM Logs WHERE by_user = '$employee_email' ".
                                                                            "AND on_date = '$display_date' AND company_id = '".$_SESSION["company_id"]."' ".
                                                                            "ORDER BY on_date, on_time DESC");
                            ///////////////////////////
                            //  Display Information
                            echo "<br/><br/>";
                            echo "<div class='on-20px'><table class='on-table-center on-table'>";
                                //  Table Headers
                                echo "<tr>";
                                    echo "<th>What</th>";
                                    echo "<th>On Table</th>";
                                    echo "<th>On Date</th>";
                                    echo "<th>@ Time</th>";
                                echo "</tr>";
                                //  Table Information
                                if ($nLogs > 1)
                                {
                                    foreach ($logs as $log)
                                    {
                                        echo "<tr>";
                                            echo "<td>".$log[0]."&nbsp;</td>";
                                            echo "<td>".$log[1]."&nbsp;</td>";
                                            echo "<td align='right'>".$log[2]."</td>";
                                            echo "<td align='right'>".$log[3]."</td>";
                                        echo "</tr>";
                                    }
                                }
                                else if ($nLogs == 1)
                                {
                                    echo "<tr>";
                                        echo "<td>".$logs[0][0]."</td>";
                                        echo "<td>".$logs[0][1]."</td>";
                                        echo "<td align='right'>".$logs[0][2]."</td>";
                                        echo "<td align='right'>".$logs[0][3]."</td>";
                                    echo "</tr>";
                                }
                                else
                                {
                                    echo "<tr>";
                                        echo "<td class='centerdata' colspan='4'>No information to display</td>";
                                    echo "</tr>";
                                }
                                echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                            echo "</table></div>";
                            ///////////////////////////
                        echo "</form>";
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
