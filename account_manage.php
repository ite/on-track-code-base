<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("_functions.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    if (!hasAccess("ACCOUNT_MANAGE"))
        header("Location: noaccess.php");

    //  Insert 
    if (isset($_POST["save"]) && $_POST["save"] === "1")        {
        $_ids = $_POST["_acc_id"];
        $_bank = $_POST["_bank"];
        $_account = $_POST["_accNum"];
        $_name = $_POST["_accName"];
        $_type = $_POST["_accType"];
        $_startBal = $_POST["_startBal"];
        $_startBalDate = $_POST["_startBalDate"];

		$error = "";

        $counter = 0;
        foreach($_ids as $_id)      {
			if (!($_id == "none" || $_id == ""))	{ 	//UPDATE has to happen
				$boolUpdate = 0;
                $currAcc = q("SELECT account FROM Account WHERE company_id ='".$_SESSION["company_id"]."' AND id = '".$_ids[$counter]."' ");
				if ($currAcc == $_account[$counter])	$boolUpdate = 1;		
				else	{
					$exists = q("SELECT id FROM Account WHERE company_id ='".$_SESSION["company_id"]."' 
								AND account = '".$_account[$counter]."' AND id != '".$_ids[$counter]."'");
					if($exists)	//Other account with the same account number exists already
						$error .= "Account number '".$_account[$counter]."' already exists<br>";
					else	$boolUpdate = 1;
				}
				if ($boolUpdate)
						$update = q("UPDATE Account SET 
											bank_id = '".addslashes(strip_tags($_bank[$counter]))."', 
											account = '".addslashes(strip_tags($_account[$counter]))."', 
											name = '".addslashes(strip_tags($_name[$counter]))."', 
											type_id = '".addslashes(strip_tags($_type[$counter]))."',  
											start_bal = '".addslashes(strip_tags($_startBal[$counter]))."',  
											start_bal_date = '".addslashes(strip_tags($_startBalDate[$counter]))."'
										WHERE id = '$_id'");
            }else  if($_id == "none")        {			//INSERT has to happen
                if($_bank[$counter] != "null" && $_account[$counter] != "null" )        {
                    if($_name[$counter] != "" && $_type[$counter] != "" && $_startBal[$counter] != "" && $_startBalDate[$counter] !=""){
                    
                        $exists = q("SELECT id FROM Account WHERE company_id ='".$_SESSION["company_id"]."' AND account = '".$_account[$counter]."' ");
                        if(!$exists){
                            $insert = q("INSERT INTO Account (company_id, bank_id, account, name, type_id, start_bal, start_bal_date) VALUES ( 
                                            '".$_SESSION["company_id"]."',
                                            '".addslashes(strip_tags($_bank[$counter]))."', 
                                            '".addslashes(strip_tags($_account[$counter]))."',
                                            '".addslashes(strip_tags($_name[$counter]))."',
                                            '".addslashes(strip_tags($_type[$counter]))."',
                                            '".addslashes(strip_tags($_startBal[$counter]))."',
                                            '".addslashes(strip_tags($_startBalDate[$counter]))."' )");
							$bank = q("SELECT name FROM dropdowns WHERE id = '".addslashes(strip_tags($_bank[$counter]))."'");
							$insert = q("INSERT INTO Statement (company_id,bank,account,amount,balance,date,description,description_new) ".
								"VALUES ('".$_SESSION['company_id']."','$bank','".addslashes(strip_tags($_account[$counter]))."','0.00',
								'".number_format(addslashes(strip_tags($_startBal[$counter])),2,".","")."','".
								addslashes(strip_tags($_startBalDate[$counter]))."','BROUGHT FORWARD','BROUGHT FORWARD')");
                        }
						else
							$error .= "Account number '".$_account[$counter]."' already exists<br>";

                    }
                }
            }
            $counter++;
        }          
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("2", "budget");

    if ($errorMessage != ""){
    
    }
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    jQuery(function(){
        jQuery('._datePick').DatePicker({
                format:'Y-m-d',
                date: jQuery(this).val(),
                current: jQuery(this).val(),
                starts: 1,
                position: 'right',
                onBeforeShow: function(){
                    var _date;
                    if (!validation('date',jQuery(this).val()))
                        z_date = new Date();
                    else 
                        _date = jQuery(this).val();
                    jQuery(this).DatePickerSetDate(_date, true);
                },
                onChange: function(formated, dates){
                    jQuery(this).val(formated);
                    jQuery(this).DatePickerHide();
                }
        });

        //Add
        jQuery(".btnAdd").click(function()   {
            var rows = jQuery("#accountTbl tr").length;
            var cloneRow = jQuery("#accountTbl tr:nth-child("+(rows-2)+")");
            var lastRow = jQuery("#accountTbl tr:nth-child("+(rows-1)+")");
            var clone = cloneRow.clone(true);
            
            clone.children().eq(0).children().eq(0).val("none");
            clone.children().eq(0).children().eq(1).val(0);
            clone.children().eq(1).children().eq(0).val(0);
            clone.children().eq(2).children().eq(0).val("");
            clone.children().eq(3).children().eq(0).val("");
            clone.children().eq(4).children().eq(0).val("");
            clone.children().eq(5).children().eq(0).val("");
            clone.children().eq(4).children().eq(0).attr("readonly", false);
            clone.children().eq(5).children().eq(0).attr("readonly", false);
            clone.insertBefore(lastRow);
        });
        
        // Remove
        jQuery(".btnRemove").click(function()   {
			if (jQuery(this).parent().parent().children().eq(0).children().eq(0).val() != "none"){
				alert("You cannot remove saved entries. Please contact support@integrityengineering.co.za for help");
			}else{
                if(jQuery("#accountTbl tr").length != 4){
                    jQuery(this).parent().parent().remove();
                }
            }
        });
        
        // Save
        jQuery("#btnSave").click(
            function(){
                document.forms["acc_add"].submit();
            }
        );
        
     }); // END MAIN jQuery Function

</script>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="acc_add">
                   
                   <table class="on-table-center" style="width:100%;">
                        <tr>
                            <td width="100%" colspan="100%" class="centerdata">
                                <h6>Account Management</h6>
                            </td>
                        </tr>
                    </table>
<?php
					if ($error != "")
						echo "<p style='color:orange;'>$error</p>";

?>
                    <table class="on-table on-table-center" id="accountTbl" name="accountTbl"><!-- Table 1 -->
                        <tr>
                            <th>Bank:</th>
                            <th>Account Type:</th>
                            <th>Account #:</th>
                            <th>Account name:</th>
                            <th>Starting balance:</th>
                            <th>Starting balance<br>date:</th>
                            <th></th>
                        </tr>
                        <?php 
                            $accounts = q("SELECT id, bank_id, account, name, type_id, start_bal, start_bal_date FROM Account WHERE company_id ='".$_SESSION["company_id"]."' ");
                            if(is_array($accounts)){
                                foreach($accounts as $a){
                                    echo "<tr>";
                                        echo "<td>";
                                            echo "<input name='_acc_id[]' type='hidden' value='$a[0]'/>";
                                            echo "<select name='_bank[]' id='_bank[]'>";
											echo "<option value='null' selected> -- Select Bank -- </option>";
                                                        $banks = q("SELECT id, name FROM dropdowns WHERE categoryID = (SELECT id FROM categories WHERE code = 'bank') AND active = 1 ORDER BY ranking");
                                                        if (is_array($banks)){
                                                            foreach ($banks as $b){
                                                                if($b[0] == $a[1])
                                                                    echo "<option value='$b[0]' selected>$b[1]</option>";
                                                                else
                                                                    echo "<option value='$b[0]'>$b[1]</option>";
                                                            }
                                                        }
                                            echo "</select>";
                                        echo "</td>";
                                        echo "<td>";
                                            echo "<select name='_accType[]'  id='_accType[]'>";
											echo "<option value='null' selected> -- Select Account Type -- </option>";
                                                        $accountTypes = q("SELECT id, name FROM dropdowns WHERE categoryID = (SELECT id FROM categories WHERE code = 'accountType') AND active = 1 ORDER BY ranking");
                                                        if (is_array($accountTypes)){
                                                            foreach ($accountTypes as $aType){
                                                                if($aType[0] == $a[4])
                                                                    echo "<option value='$aType[0]' selected>$aType[1]</option>";
                                                                else
                                                                    echo "<option value='$aType[0]'>$aType[1]</option>";
                                                            }
                                                        }
                                            echo "</select>";
                                        echo "</td>";
                                        echo "<td><input type='text' name='_accNum[]' id='_accNum[]' value='".$a[2]."'/></td>";
                                        echo "<td><input type='text' name='_accName[]' id='_accName[]' value='".$a[3]."'/></td>";
                                        echo "<td><input type='text' name='_startBal[]' id='_startBal[]'  value='".number_format($a[5], 2, ".", "")."' readonly/></td>";
                                        echo "<td><input type='text' class='_datePick' name='_startBalDate[]' id='_startBalDate[]'  value='".$a[6]."' readonly/></td>";
                                        echo "<td><input name='btnRemove' class='btnRemove' id='btnRemove' type='button' value='-'></td>";
                                    echo "</tr>";
                                }
                            }else{
                                    echo "<tr>";
                                        echo "<td>";
                                            echo "<input name='_acc_id[]' type='hidden' value='none'/>";
                                            echo "<select name='_bank[]' id='_bank[]'>";
                                                echo "<option value='null' selected> -- Select Bank -- </option>";
                                                        $banks = q("SELECT id, name FROM dropdowns WHERE categoryID = (SELECT id FROM categories WHERE code = 'bank') AND active = 1 ORDER BY ranking");
                                                        if (is_array($banks)){
                                                            foreach ($banks as $b){
                                                                echo "<option value='$b[0]'>$b[1]</option>";
                                                            }
                                                        }
                                            echo "</select>";
                                        echo "</td>";
                                        echo "<td>";
                                            echo "<select name='_accType[]'  id='_accType[]'>";
                                                echo "<option value='null' selected> -- Select Account Type -- </option>";
                                                        $accountTypes = q("SELECT id, name FROM dropdowns WHERE categoryID = (SELECT id FROM categories WHERE code = 'accountType') AND active = 1 ORDER BY ranking");
                                                        if (is_array($accountTypes)){
                                                            foreach ($accountTypes as $aType){
                                                                echo "<option value='$aType[0]'>$aType[1]</option>";
                                                            }
                                                        }
                                            echo "</select>";
                                        echo "</td>";
                                        echo "<td><input type='text' name='_accNum[]' id='_accNum[]' value=''/></td>";
                                        echo "<td><input type='text' name='_accName[]' id='_accName[]' value=''/></td>";
                                        echo "<td><input type='text' name='_startBal[]' id='_startBal[]'  value=''/></td>";
                                        echo "<td><input type='text' class='_datePick' name='_startBalDate[]' id='_startBalDate[]'  value=''/></td>";
                                        echo "<td><input name='btnRemove' class='btnRemove' id='btnRemove' type='button' value='-'></td>";
                                    echo "</tr>";
                            }
                            echo "<tr><td colspan='100%'><input name='btnAdd' class='btnAdd' id='btnAdd' type='button' value='+'></td></tr>";
                        ?>
                        <tfoot><tr><td colspan='100%'></td></tr></tfoot>
                    </table><!-- Table 1 END -->
                    <br/><br/>
                    <input name="btnSave" id="btnSave" class="btnSave" type="button" value="Save">
                    <input method="post" name="save" type="hidden" value="1" />
                </form>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
