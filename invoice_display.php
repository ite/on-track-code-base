<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

	if (!hasAccess("INVOICE_MANAGE"))
        header("Location: noaccess.php");

    //  Get Information
    $id                                                                 = $_GET["id"];
    $info                                                               = q("SELECT project_id, start_date, end_date FROM Invoices WHERE id = '$id'");

    //  Get Expenses Info
    function getExpenses($project_id, $date_from, $date_to, $total_cost)
    {
        GLOBAL $exceldata;
        GLOBAL $row;
        
        $expense_based                                                  = "";

        //  $nExpenses                                                  =  Number of Expenses Booked
        $nExpenses                                                      = q("SELECT COUNT(id) FROM ExpenseSheet WHERE project_id = '$project_id' ".
                                                                            "AND date >= '$date_from' AND date <= '$date_to'");
        $expenses                                                       = q("SELECT id, descr, expense FROM ExpenseSheet WHERE project_id = '$project_id' ".
                                                                            "AND date >= '$date_from' AND date <= '$date_to' ORDER BY descr");

        if ($nExpenses > 1)
        {
            $expense_based                                              .= "<tr><td class='on-table-clear centerdata' colspan='100%'><h6>Expenses</h6></td></tr>
                                                                                          <tr><td>Expenses</td>";

            $expense_based                                              .= "<td><table width='100%'>";

            foreach ($expenses as $expense)
                $expense_based                                          .= "<tr><td class='noborder'>".$expense[1]."</td></tr>";

            $expense_based                                              .= "</table></td><td><table width='100%'>";

            foreach ($expenses as $expense)
            {
                $total_cost                                             = $total_cost + $expense[2];

                $expense_based                                          .= "<tr><td class='noborder rightdata'>".number_format($expense[2], 2, ".", "").
                                                                                        "</td></tr>";
                $exceldata[$row][]  = "Expenses";
                $exceldata[$row][]  = $expense[1];
                $exceldata[$row][]  = $expense[2];
                $row++;
            }

            $expense_based                                              .= "</table></td>";
        }
        else if ($nExpenses == 1)
        {
            $total_cost                                                 = $total_cost + $expenses[0][2];
            $expense_based                                              .= "<tr><td class='on-table-clear centerdata' colspan='100%'><h6>Expenses</h6></td></tr>
                                                                                         <tr><td>Expenses</td>";
            $expense_based                                              .= "<td>
                                                                                            <table width='100%'>
                                                                                                <tr><td class='noborder'>".$expenses[0][1]."</td>
                                                                                            </table>
                                                                                        </td>
                                                                                        <td>
                                                                                            <table width='100%'>
                                                                                                <tr>
                                                                                                    <td class='noborder rightdata'>".number_format($expenses[0][2], 2, ".", "")."</td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>";
            $exceldata[$row][]  = "Expenses";
            $exceldata[$row][]  = $expenses[0][1];
            $exceldata[$row][]  = $expenses[0][2];
            $row++;
        }

        $expense_based                                                  .= "<tr>
                                                                                            <td class='on-table-total'>&nbsp;</td>
                                                                                            <td class='on-table-total'>&nbsp;</td>
                                                                                            <td class='on-table-total'>
                                                                                                <table width='100%'>
                                                                                                    <tr>
                                                                                                        <td class='on-table-total' style='border:0px;'>Total <i>(".$_SESSION["currency"].")</i>:</td>
                                                                                                        <td class='on-table-total' style='border:0px;'>".number_format($total_cost, 2, ".", "")."</td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>";
        $exceldata[$row][]  = "";
        $exceldata[$row][]  = "Total (".$_SESSION["currency"]."): ";
        $exceldata[$row][]  = $total_cost;
        $row++;

        return "".$expense_based;
    }

    //  Create Display Information Function
    if ($info)
    {
        $project_id                                                    = $info[0][0];
        $date_from                                                    = $info[0][1];
        $date_to                                                        = $info[0][2];
        $displayString                                                = "";

        ///////////////////////////
        //  Get Info From Tables
        ///////////////////////////
        $project_name                                                   = q("SELECT name From Project WHERE id = '$project_id'");

        $total_cost                                                     = 0;
        $row                                                               = 0;
        
        $excelheadings[0][] = "";
        $excelheadings[0][] = "";
        $excelheadings[0][] = "";
        
        //  Create Heading
        $table_headers                                                  = "<tr>
                                                                                        <th>Activity Type</th>
                                                                                        <th>Activity</th>
                                                                                        <th>Cost <i>(".$_SESSION["currency"].")</i></th>";   
        $exceldata[$row][] = "Activity Type";                                   // 1
        $exceldata[$row][] = "Activity";                                           // 2
        $exceldata[$row][] = "Cost (".$_SESSION["currency"].")";     // 3
        $row++;

        ///////////////////////////
        //  Create Display
        ///////////////////////////
        //  Time Booked Section
        //  $nActivityTypes                                             = Number of Activities
        $nActivityTypes                                                 =q("SELECT COUNT(DISTINCT(at.id)) FROM (ActivityTypes AS at LEFT JOIN Activities AS a ".
                                                                            "ON at.id = a.parent_id LEFT JOIN TimeSheet AS ts ON ts.activity_id = a.id) ".
                                                                            "WHERE ts.project_id = '$project_id' AND ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                                                            "AND at.company_id = '".$_SESSION["company_id"]."'");
        $activityTypes                                                  =q("SELECT DISTINCT(at.id), at.type FROM (ActivityTypes AS at LEFT JOIN Activities AS a ".
                                                                            "ON at.id = a.parent_id LEFT JOIN TimeSheet AS ts ON ts.activity_id = a.id) ".
                                                                            "WHERE ts.project_id = '$project_id' AND ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                                                            "AND at.company_id = '".$_SESSION["company_id"]."' ORDER BY at.type");

        if ($nActivityTypes > 1)
        {
            foreach ($activityTypes as $activityType)
            {
                //  nActivities                                         = Number of Activities per Type
                $nActivities                                            = q("SELECT COUNT(a.id) FROM (TimeSheet AS ts INNER JOIN Activities AS a ".
                                                                            "ON a.id = ts.activity_id) WHERE a.parent_id = '".$activityType[0]."' ".
                                                                            "AND ts.project_id = '$project_id' AND ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                                                            "GROUP BY a.id");
                $activities                                             = q("SELECT a.id, a.name, SUM(ts.rate * ts.time_spent) FROM (TimeSheet AS ts INNER JOIN ".
                                                                            "Activities AS a ON a.id = ts.activity_id) WHERE a.parent_id = '".$activityType[0]."' ".
                                                                            "AND ts.project_id = '$project_id' AND ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                                                            "GROUP BY a.id ORDER BY a.name");

                $time_based                                             .= "<tr><td>".$activityType[1]."</td>";

                if ($nActivities > 1)
                {
                    $time_based                                         .= "<td>
                                                                                        <table width='100%'>";

                    foreach ($activities as $activity){
                        $time_based                                     .= "<tr>
                                                                                        <td class='noborder'>".$activity[1]."</td>
                                                                                    </tr>";
                    }

                    $time_based                                         .= "</table></td><td><table width='100%'>";

                    foreach ($activities as $activity)
                    {
                        $total_cost                                     = $total_cost + $activity[2];

                        $time_based                                     .= "<tr><td class='noborder rightdata'>".number_format($activity[2], 2, ".", "")."</td></tr>";
                        $exceldata[$row][]                              = $activityType[1];
                        $exceldata[$row][]                              = $activity[1];
                        $exceldata[$row][]                              = $activity[2];
                        $row++;
                    }

                    $time_based                                         .= "</table></td>";
                }
                else if ($nActivities == 1)
                {
                    $total_cost                                         = $total_cost + $activities[0][2];
                    $time_based                                         .= "<td>
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td class='noborder'>".$activities[0][1]."</td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td>
                                                                                        <table width='100%'>
                                                                                            <tr>
                                                                                                <td class='noborder rightdata'>".number_format($activities[0][2], 2, ".", "")."</td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>";
                    $exceldata[$row][]                              = $activityType[1];
                    $exceldata[$row][]                              = $activities[0][1];
                    $exceldata[$row][]                              = $activities[0][2];
                    $row++;
                }
                $time_based                                             .= "</tr>";
            }
        }
        else if ($nActivityTypes == 1)
        {
            //  nActivities                                             = Number of Activities per Type
            $nActivities                                                = q("SELECT COUNT(a.name) FROM (TimeSheet AS ts INNER JOIN Activities AS a ".
                                                                            "ON a.id = ts.activity_id) WHERE a.parent_id = '".$activityTypes[0][0]."' ".
                                                                            "AND ts.project_id = '$project_id' AND ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                                                            "GROUP BY a.id");
            $activities                                                 = q("SELECT a.id, a.name, SUM(ts.rate * ts.time_spent) FROM (TimeSheet AS ts INNER JOIN ".
                                                                            "Activities AS a ON a.id = ts.activity_id) WHERE a.parent_id = '".$activityTypes[0][0]."' ".
                                                                            "AND ts.project_id = '$project_id' AND ts.date >= '$date_from' AND ts.date <= '$date_to' ".
                                                                            "GROUP BY a.id ORDER BY a.name");

            $time_based                                                 .= "<tr><td>".$activityTypes[0][1].
                                                                            "</td>";

            if ($nActivities > 1)
            {
                $time_based                                             .= "<td><table width='100%'>";

                foreach ($activities as $activity)
                    $time_based                                         .= "<tr><td class='noborder'>".$activity[1]."</td></tr>";

                $time_based                                             .= "</table></td><td><table width='100%'>";

                foreach ($activities as $activity)
                {
                    $total_cost                                         = $total_cost + $activity[2];

                    $time_based                                         .= "<tr><td class='noborder rightdata'>".number_format($activity[2], 2, ".", "").
                                                                                    "</td></tr>";
                    $exceldata[$row][]                              = $activityTypes[0][1];
                    $exceldata[$row][]                              = $activity[1];
                    $exceldata[$row][]                              = $activity[2];
                    $row++;
                }

                $time_based                                             .= "</table></td>";
            }
            else if ($nActivities == 1)
            {
                $total_cost                                             = $total_cost + $activities[0][2];
                $time_based                                             .= "<td>
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td class='noborder'>".$activities[0][1]."</td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td>
                                                                                        <table style='width:100%'>
                                                                                            <tr>
                                                                                                <td class='noborder rightdata'>".number_format($activities[0][2], 2, ".", "")."</td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>";
                                                                                    
                $exceldata[$row][]                              = $activityTypes[0][1];
                $exceldata[$row][]                              = $activities[0][1];
                $exceldata[$row][]                              = $activities[0][2];
                $row++;
            }
        }

        $expense_based                                              = getExpenses($project_id, $date_from, $date_to, $total_cost);
        $display                                                           = $table_headers.$time_based.$expense_based;

        if ($date_from == $date_to)
            $timestamp                                                  = "Time Period: ".$date_from;
        else
            $timestamp                                                  = "Time Period: ".$date_from." - ".$date_to;

        if ($display != ""){
            $displayString                                              = "<tr><td class='on-table-clear' colspan='100%'><a>Project Report</a></td></tr>
                                                                                    <tr><td class='on-table-clear' colspan='100%'>Project: ".$project_name."</td></tr>".
                                                                                    "<tr><td class='on-table-clear' colspan='100%'>".$timestamp."</td></tr>".$display;
            $excelheadings[1][] = "Project Report";                                   
            $excelheadings[1][] = "";                                           
            $excelheadings[1][] = "";    

            $excelheadings[2][] = "Project: ".$project_name;                                   
            $excelheadings[2][] = "";                                           
            $excelheadings[2][] = "";
            
            $excelheadings[3][] = $timestamp;                                   
            $excelheadings[3][] = "";                                           
            $excelheadings[3][] = "";
        }
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("3", "reports");
?>
    <table width="100%">
        <tr height="380px">
            <td align="center" valign="top">
                <?php
                    echo "<div class='on-20px'><table class='on-table-center on-table'>";
                        echo "".$displayString;
                        echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                    echo "</table></div>";
                    echo "<br/>";
                    
                    ///////////////////////////
                    if($exceldata && $excelheadings){
                        //  Set Export Information
                        $_SESSION["fileName"] = "Project Invoice";
                        $_SESSION["fileData"] = array_merge($excelheadings, $exceldata);
                    }
                    ///////////////////////////

                    echo "<div style='width:50%; text-align:right'><input name='btnExport' onClick=\"location.href='_export.php'\" type='button' value='Export to Excel' /></div>";
                ?>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
