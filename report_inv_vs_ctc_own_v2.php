<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("fusion/FusionCharts.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    if (!hasAccess("REP_INV_VS_CTC_OWN") && !hasAccess("REP_INV_VS_CTC_OWN_V2"))
        header("Location: noaccess.php");

    //  Set Report Generated Status
    $generated                                                          = "0";

    $fontColor = "";

    function createXMLString($date_from, $date_to, $employee_id,$table,$where)  {
        $xmlString                                                      = "";

        $xmlString                                                      = "<chart palette='1' caption='Invoice Potential vs. Cost to Company Report' shownames='1' showvalues='0' ".
                                                                            "numberPrefix='".$_SESSION["currency"]."' showSum='1' decimals='0' overlapColumns='0' ".
                                                                            "formatNumberScale='0' formatNumber='0' labelDisplay='rotate' ".fusionChart().">";

        //  Create Categories
        $xmlString                                                      .= "<categories>";

        if ($employee_id == "all")      {
            $employees = q("SELECT DISTINCT(e.id), e.lstname, e.frstname FROM (Employee AS e INNER JOIN Company_Users AS cu ON e.id = cu.user_id INNER JOIN $table AS ts ON e.id = ts.user_id) ".
                            "WHERE cu.company_id = '".$_SESSION["company_id"]."' AND ts.date >= '$date_from' AND ts.date <= '$date_to' AND e.email != 'admin' AND e.deleted = '0' $where ORDER BY e.lstname, frstname");

            //  Create Categories
            if (is_array($employees))     {
                foreach ($employees as $employee)
                    $xmlString .= "<category label='".$employee[1].", ".$employee[2]."'></category>";
            }
        }
        else
        {
            $employee_info = q("SELECT lstname, frstname FROM Employee WHERE id = '$employee_id'");

            $xmlString .= "<category label='".$employee_info[0][0].", ".$employee_info[0][1]."'></category>";
        }

        $xmlString .= "</categories>";

        $days = getDays($date_from, $date_to);

        //  Create Arrays
        $target_array = array();
        $potential_array = array();
        $cost_array = array();
        $revenue_array = array();

        if ($employee_id == "all")      {
            if (is_array($employees))   {
                foreach ($employees as $employee)       {
                    $target     = q("SELECT target FROM Employee WHERE id = '".$employee[0]."'");
                    $potential  = q("SELECT SUM(ts.rate * ts.time_spent) FROM ($table AS ts INNER JOIN companiesOnTeam AS cot ON ts.project_id = cot.project_id INNER JOIN Project AS p ON p.id = ts.project_id) ".
                                    "WHERE cot.total_budget > 0 AND ts.user_id = '".$employee[0]."' AND ts.date >= '$date_from' AND ts.date <= '$date_to' AND p.status = 'active' ".
                                    "AND ts.company_id = '".$_SESSION["company_id"]."' $where");
                    $cost       = q("SELECT cost FROM Employee WHERE id = '".$employee[0]."'");

                    $target_array[$employee[0]]         = ($target / 30) * $days;
                    $potential_array[$employee[0]]      = $potential;
                    $cost_array[$employee[0]]           = ($cost * 8) * $days;
                    $revenue_array[$employee[0]]        = $potential_array[$employee[0]] - $cost_array[$employee[0]];
                }
            }
        }
        else
        {
            $target     = q("SELECT target FROM Employee WHERE id = '".$employee_id."'");
            $potential  = q("SELECT SUM(ts.rate * ts.time_spent) FROM ($table AS ts INNER JOIN companiesOnTeam AS cot ON ts.project_id = cot.project_id INNER JOIN Project AS p ON p.id = ts.project_id) ".
                            "WHERE cot.total_budget > 0 AND ts.user_id = '".$employee_id."' AND ts.date >= '$date_from' AND ts.date <= '$date_to' AND p.status = 'active' AND cot.company_id = '".$_SESSION["company_id"]."' $where");
            $cost       = q("SELECT cost FROM Employee WHERE id = '".$employee_id."'");

            $target_array[$employee_id]         = ($target / 30) * $days;
            $potential_array[$employee_id]      = $potential;
            $cost_array[$employee_id]           = ($cost * 8) * $days;
            $revenue_array[$employee_id]        = $potential_array[$employee_id] - $cost_array[$employee_id];
        }

        //  Create Dataset - Target per Employee
        $xmlString                                                      .= "<dataset seriesName='Target' color='0033FF' showValues='0'>";

        if ($employee_id == "all")      {
            if (is_array($employees))
                foreach ($employees as $employee)
                    $xmlString                                          .= "<set value='".$target_array[$employee[0]]."'/>";
        }
        else
            $xmlString                                                  .= "<set value='".$target_array[$employee_id]."'/>";

        $xmlString                                                      .= "</dataset>";

        //  Create Dataset - Invoice Potential per Employee
        $xmlString                                                      .= "<dataset seriesName='Invoice Potential' color='009ACD' showValues='0'>";

        if ($employee_id == "all")      {
            if (is_array($employees))
                foreach ($employees as $employee)
                    $xmlString                                          .= "<set value='".$potential_array[$employee[0]]."'/>";
        }
        else
            $xmlString                                                  .= "<set value='".$potential_array[$employee_id]."'/>";

        $xmlString                                                      .= "</dataset>";

        //  Create Dataset - Cost to Company per Employee
        $xmlString                                                      .= "<dataset seriesName='Cost to Company' color='FF0033' showValues='0'>";

        if ($employee_id == "all")      {
            if (is_array($employees))
                foreach ($employees as $employee)
                    $xmlString                                          .= "<set value='".$cost_array[$employee[0]]."'/>";
        }
        else
            $xmlString                                                  .= "<set value='".$cost_array[$employee_id]."'/>";

        $xmlString                                                      .= "</dataset>";

        //  Create Dataset - Revenue per Employee
        $xmlString                                                      .= "<dataset seriesName='Revenue' color='33FF00' showValues='0'>";

        if ($employee_id == "all")      {
            if (is_array($employees))
                foreach ($employees as $employee)
                    $xmlString                                          .= "<set value='".$revenue_array[$employee[0]]."'/>";
        }
        else
            $xmlString                                                  .= "<set value='".$revenue_array[$employee_id]."'/>";

        $xmlString                                                      .= "</dataset>";

        $xmlString                                                      .= "<trendLines>";
        $xmlString                                                      .= "<line startValue='0' color='000000' showOnTop='1' thickness='4' />";
        $xmlString                                                      .= "</trendLines>";

        $xmlString                                                      .= "</chart>";

        return $xmlString;
    }

    function createTable($date_from, $date_to, $employee_id,$table,$where){
        $tableString = "";

        $headers = "<tr>
                        <th>Employee Name</th>
                        <th>Invoice<br/>Potential / Cost to<br/>Company<i>(".$_SESSION["currency"].")</i></th>
                        <th>Target <i>(".$_SESSION["currency"].")</i><br/><i>(excl. VAT)</i></th>
                        <th>Invoice<br/>Potential <i>(".$_SESSION["currency"].")</i><br/><i>(excl. VAT)</i></th>
                        <th>Cost to<br/>Company <i>(".$_SESSION["currency"].")</i><br/><i>(excl. VAT)</i></td>
                        <th>Revenue <i>(".$_SESSION["currency"].")</i><br/><i>(excl. VAT)</i></th>
                    </tr>";

        $days = getDays($date_from, $date_to);

        //  Create Arrays
        $target_array = array();
        $potential_array = array();
        $cost_array = array();
        $revenue_array = array();

        if ($employee_id == "all")
            $employees = q("SELECT DISTINCT(e.id), e.lstname, e.frstname FROM (Employee AS e INNER JOIN Company_Users AS cu ON e.id = cu.user_id INNER JOIN $table AS ts ON e.id = ts.user_id) ".
                            "WHERE cu.company_id = '".$_SESSION["company_id"]."' AND ts.date >= '$date_from' AND ts.date <= '$date_to' AND e.email != 'admin' AND e.deleted = '0' $where ORDER BY e.lstname, frstname");
        else
            $employee_info = q("SELECT lstname, frstname FROM Employee WHERE id = '$employee_id'");

        //  Create Arrays
        if ($employee_id == "all")      {
            if (is_array($employees))   {
                foreach ($employees as $employee)       {
                    $target     = q("SELECT target FROM Employee WHERE id = '".$employee[0]."'");
                    $potential  = q("SELECT SUM(ts.rate * ts.time_spent) FROM ($table AS ts INNER JOIN companiesOnTeam AS cot ON ts.project_id = cot.project_id INNER JOIN Project AS p ON p.id = ts.project_id) ".
                                    "WHERE cot.total_budget > 0 AND ts.user_id = '".$employee[0]."' AND ts.date >= '$date_from' AND ts.date <= '$date_to' AND p.status = 'active' ".
                                    "AND ts.company_id = '".$_SESSION["company_id"]."' $where");
                    $cost       = q("SELECT cost FROM Employee WHERE id = '".$employee[0]."'");

                    $target_array[$employee[0]]         = ($target / 30) * $days;
                    $potential_array[$employee[0]]      = $potential;
                    $cost_array[$employee[0]]           = ($cost * 8) * $days;
                    $revenue_array[$employee[0]]        = $potential_array[$employee[0]] - $cost_array[$employee[0]];
                }
            }
        }
        else
        {
            $target     = q("SELECT target FROM Employee WHERE id = '".$employee_id."'");
            $potential  = q("SELECT SUM(ts.rate * ts.time_spent) FROM ($table AS ts INNER JOIN companiesOnTeam AS cot ON ts.project_id = cot.project_id INNER JOIN Project AS p ON p.id = ts.project_id) ".
                            "WHERE cot.total_budget > 0 AND ts.user_id = '".$employee_id."' AND ts.date >= '$date_from' AND ts.date <= '$date_to' AND p.status = 'active' AND cot.company_id = '".$_SESSION["company_id"]."' $where");
            $cost       = q("SELECT cost FROM Employee WHERE id = '".$employee_id."'");

            $target_array[$employee_id]         = ($target / 30) * $days;
            $potential_array[$employee_id]      = $potential;
            $cost_array[$employee_id]           = ($cost * 8) * $days;
            $revenue_array[$employee_id]        = $potential_array[$employee_id] - $cost_array[$employee_id];
        }

        if ($employee_id == "all")      {
            if (is_array($employees))   {
                foreach ($employees as $employee)       {
                    $tableString .= "<tr>".
                                        "<td>".$employee[1].", ".$employee[2]."</td>".
                                        "<td class='rightdata'>".number_format(($potential_array[$employee[0]] / $cost_array[$employee[0]]), 1, ".", "")."</td>".
                                        "<td class='rightdata'>".number_format($target_array[$employee[0]], 2, ".", "")."</td>".
                                        "<td class='rightdata'>".number_format($potential_array[$employee[0]], 2, ".", "")."</td>".
                                        "<td class='rightdata'>".number_format($cost_array[$employee[0]], 2, ".", "")."</td>".
                                        "<td class='rightdata'>".number_format($revenue_array[$employee[0]], 2, ".", "")."</td>".
                                    "</tr>";
                }
            }
        }
        else    {
            $tableString .= "<tr>".
                                "<td>".$employee_info[0][0].", ".$employee_info[0][1]."</td>".
                                "<td class='rightdata'>".number_format(($potential_array[$employee_id] / $cost_array[$employee_id]), 1, ".", "")."</td>".
                                "<td class='rightdata'>".number_format($target_array[$employee_id], 2, ".", "")."</td>".
                                "<td class='rightdata'>".number_format($potential_array[$employee_id], 2, ".", "")."</td>".
                                "<td class='rightdata'>".number_format($cost_array[$employee_id], 2, ".", "")."</td>".
                                "<td class='rightdata'>".number_format($revenue_array[$employee_id], 2, ".", "")."</td>".
                            "</tr>";
        }

        if ($tableString != "")
            $tableString                                                = $headers.$tableString;

        return $tableString;
    }

    //  Create Display Information Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")
    {
        $errorMessage                                                   = "";

        $reportOn = $_POST["reportOn"];

        $employee_id                                                    = $_SESSION["user_id"];
        $date_from                                                      = addslashes(strip_tags($_POST["date_from"]));
        $date_to                                                        = addslashes(strip_tags($_POST["date_to"]));
        $displayString                                                  = "";

        $table = ($reportOn == "approved") ? "ApprovedTime" : "TimeSheet";
        $where = ($reportOn == "approved") ? "AND ts.status = '2' " : "";

        $graph                                                          = createXMLString($date_from, $date_to, $employee_id,$table,$where);
        $displayString                                                  = renderChart("fusion/MSColumn3D.swf", "", $graph, "multigraph", 800, 400, false, false);


        if ($displayString != "")
        {
            $displayString                                              = "<tr>
                                                                                <td class='on-table-clear' colspan='100%'><a>Invoice Potential vs. Cost to Company</a></td>
                                                                            </tr><tr>
                                                                                <td class='on-table-clear' colspan='100%'>".$date_from." to ".$date_to."</td>
                                                                            </tr><td colspan='100%'><br/>".$displayString."<br/></td></tr>".
                                                                                createTable($date_from, $date_to, $employee_id,$table,$where);

            $generated                                                  = "1";
        }
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("5", "reports");

    if ($errorMessage != "")
    {
        echo "<p align='center' style='padding:0px;'><strong><font class='on-validate-error'>$errorMessage</font></strong></p>";
        echo "<br/>";
    }
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    jQuery(function(){
	jQuery('#date_from').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_from').val(),
		current: jQuery('#date_from').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_from').val()))
			_date = new Date();
			else _date = jQuery('#date_from').val();
			jQuery('#date_from').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_from').val(formated);
			jQuery('#date_from').DatePickerHide();
		}
	});
    })

    jQuery(function(){
	jQuery('#date_to').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_to').val(),
		current: jQuery('#date_to').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_to').val()))
			_date = new Date();
			else _date = jQuery('#date_to').val();
			jQuery('#date_to').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_to').val(formated);
			jQuery('#date_to').DatePickerHide();
		}
	});
    })

    function check()
    {
        var valid                                                       = 1;

        //  Check That Date From Is Entered
        if (document.forms["report_invoice_vs_cost"].date_from.value == "")
        {
            ShowLayer("dateFrom", "block");
            valid                                                       = 0;
        }
        //  Check That Entered Date From Is Valid
        else
        {
            ShowLayer("dateFrom", "none");

            if (!validation("date", document.forms["report_invoice_vs_cost"].date_from.value))
            {
                ShowLayer("dateFromDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("dateFromDiv", "none");
        }

        //  Check That Date To Is Entered
        if (document.forms["report_invoice_vs_cost"].date_to.value == "")
        {
            ShowLayer("dateTo", "block");
            valid                                                       = 0;
        }
        //  Check That Entered Date To Is Valid
        else
        {
            ShowLayer("dateTo", "none");

            if (!validation("date", document.forms["report_invoice_vs_cost"].date_to.value))
            {
                ShowLayer("dateToDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("dateToDiv", "none");
        }

        if (valid == 1)
        {
            document.forms["report_invoice_vs_cost"].save.value         = 1;
            document.forms["report_invoice_vs_cost"].submit();
        }
    }
</script>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="report_invoice_vs_cost">
                    <div id="" style="<?php if ($generated == "1") echo "display: none;"; else echo "display: block"; ?>">
                        <table width="100%">
                            <tr>
                                <td class="centerdata">
                                    <h6>Invoice Potential vs. Cost to Company Report</h6>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Report On:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="reportOn">
                                        <option value="normal">Actual Time</option>
                                        <option value="approved">Approved Time</option>
                                    </select>
                                </td>
                            </tr>
                            <tr><td colspan="100%"><br/></td></tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Date From:
                                </td>
                                <td width="50%">
                                    <input class="on-field-date" id="date_from" name="date_from" type="text" style="text-align:right;" value="<?php
                                        if ($_POST["date_from"] != "") echo "".$_POST["date_from"]; else echo "".getDates($today, -30); ?>">
                                    <div id="dateFrom" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                    <div id="dateFromDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Date To:
                                </td>
                                <td width="50%">
                                    <input class="on-field-date" id="date_to" name="date_to" type="text" style="text-align:right;" value="<?php
                                        if ($_POST["date_to"] != "") echo "".$_POST["date_to"]; else echo "".$today; ?>">
                                    <div id="dateTo" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                    <div id="dateToDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <input name="btnGenerate" onClick="check();" type="button" value="Generate Report">
                        <input method="post" name="save" type="hidden" value="0" />
                    </div>
                </form>
                <div id="report" style="<?php if ($generated == "1") echo "display: block;"; else echo "display: none"; ?>">
                    <?php
                        echo "<table class='on-table-center on-table'>";
                            echo "".$displayString;
                            echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        echo "</table>";
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
