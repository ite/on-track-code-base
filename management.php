<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

	if (!hasAccess("TIME_MANAGE") && !hasAccess("EXPENSE_MANAGE_OWN") && !hasAccess("TIME_MANAGE_ALL") && !hasAccess("EXPENSE_MANAGE_ALL"))
		header("Location: noaccess.php");

    //  Delete Selected Entries
    if (isset($_POST["btnDelete"]))
    {
        //  Time Sheet Array
        $array                                                          = split("_", addslashes(strip_tags($_POST["checkbox1"])));

        if ($arr[0][0])
            $array                                                      = array_slice($array, 0, count($array) - 1);

        if ($array[0][0])
            foreach ($array as $a)
            {
                if ($_POST["time".$a])
                    $delete                                             = q("DELETE FROM TimeSheet WHERE id = '$a'");
            }

        if ($delete)
        {
            $time                                                       = date("H:i:s");
            $logs                                                       = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('Timesheet Entries Deleted', 'Delete', 'TimeSheet', '".$_SESSION["email"]."', ".
                                                                            "'$today', '$time', '".$_SESSION["company_id"]."')");
        }
    
        //  Expense Sheet Array
        $array                                                          = split("_", addslashes(strip_tags($_POST["checkbox2"])));
        
        if ($arr[0][0])
            $array                                                      = array_slice($array, 0, count($array) - 1);

        if ($array[0][0])
            foreach ($array as $a)
            {
                if ($_POST["expense".$a])
                    $delete                                             = q("DELETE FROM ExpenseSheet WHERE id = '$a'");
            }

        if ($delete)
        {
            $time                                                       = date("H:i:s");
            $logs                                                       = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('ExpenseSheet Entries Deleted', 'Delete', 'ExpenseSheet', '".$_SESSION["email"]."', ".
                                                                            "'$today', '$time', '".$_SESSION["company_id"]."')");
        }

        header("Location: management.php");
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("7", "management");

    if ($errorMessage != "")
    {
        echo "<p align='center' style='padding:0px;'><strong><font color='#999999'>$errorMessage</font></strong></p>";
        echo "<br/>";
    }
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    jQuery(function(){
	jQuery('#display_date').DatePicker({
		format:'Y-m-d',
		date: jQuery('#display_date').val(),
		current: jQuery('#display_date').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#display_date').val()))
			_date = new Date();
			else _date = jQuery('#display_date').val();
			jQuery('#display_date').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#display_date').val(formated);
			jQuery('#display_date').DatePickerHide();
		}
	});
    })

    function display_check()
    {
        var valid                                                       = 1;

        //  Check That Employee Is Selected
        if (document.forms["management"].employee.value == "null")
        {
            ShowLayer("employeeDiv", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("employeeDiv", "none");

        // Check That Registration Date Is Entered
        if (document.forms["management"].display_date.value == "")
        {
            ShowLayer("displayDate", "block");
            valid                                                       = 0;
        }
        //  Check That Entered Registration Date Is Valid
        else
        {
            ShowLayer("displayDate", "none");

            if (!validation("date", document.forms["management"].display_date.value))
            {
                ShowLayer("displayDateDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("displayDateDiv", "none");
        }

        if (valid == 1)
        {
            document.forms["management"].save.value                     = 1;
            document.forms["management"].submit();
        }
    }
</script>
<?php
    $where = (hasAccess("TIME_MANAGE") && hasAccess("EXPENSE_MANAGE_OWN") && !hasAccess("TIME_MANAGE_ALL") && !hasAccess("EXPENSE_MANAGE_ALL")) ? "AND e.id = '".$_SESSION["user_id"]."' " : "";
    $employees                                                          = q("SELECT e.id, e.lstname, e.frstname FROM (Employee AS e INNER JOIN Company_Users ".
                                                                            "AS cu ON e.id = cu.user_id) WHERE cu.company_id = '".$_SESSION["company_id"]."' ".
                                                                            "AND e.email != 'admin' $where ORDER BY e.lstname, frstname");
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="management">
                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Time &amp; Expense Management</h6>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                Select Employee:
                            </td>
                            <td width="50%">
                                <select class="on-field" method="post" name="employee" tabindex="1">
                                    <option value="null">--  Select Employee  --</option>
                                    <?php
                                        if (is_array($employees))
                                            foreach ($employees as $employee)
                                                if ($_POST["employee"] == $employee[0])
                                                    echo "<option value='".$employee[0]."' selected>".$employee[1].", ".$employee[2]."</option>";
                                                else
                                                    echo "<option value='".$employee[0]."'>".$employee[1].", ".$employee[2]."</option>";
                                    ?>
                                </select>
                                <div id="employeeDiv" style="display: none;"><font class="on-validate-error">* Employee must be selected</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                Display Date:
                            </td>
                            <td width="50%">
                                <input class="on-field-date" id="display_date" name="display_date" tabindex="2" type="text" style="text-align:right;" value="<?php
                                    if ($_POST["display_date"] != "") echo "".$_POST["display_date"]; else echo "".$today; ?>">
                                <div id="displayDate" style="display: none;"><font class="on-validate-error">* Display date must be entered</font></div>
                                <div id="displayDateDiv" style="display: none;"><font class="on-validate-error">* Display date not valid, eg.<?php echo "".date("Y-m-d"); ?></font>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <input name="btnDisplay" onClick="display_check();" tabindex="3" type="button" value="Display Information">
                    <input method="post" name="save" type="hidden" value="0" />
                </form>
                <?php
                    //  Display Information Function
                    if (isset($_POST["save"]) && $_POST["save"] === "1")
                    {
                        echo "<form action='' method='post'>";
                            $checkbox1                                  = "";
                            $checkbox2                                  = "";
                            $employee_id                                = $_POST["employee"];
                            $display_date                               = addslashes(strip_tags($_POST["display_date"]));

                            ///////////////////////////
                            //  Get Information
                            ///////////////////////////
                            //  Time Sheet
                            $timesheet_info                             = q("SELECT ts.id, ts.date, p.name, at.type, a.name, ts.descr, ts.time_spent, ts.area_id ".
                                                                            "FROM (TimeSheet AS ts INNER JOIN Project AS p ON p.id = ts.project_id ".
                                                                            "INNER JOIN Activities AS a ON a.id = activity_id INNER JOIN ActivityTypes AS at ".
                                                                            "ON at.id = a.parent_id) WHERE p.company_id = '".$_SESSION["company_id"]."' ".
                                                                            "AND ts.user_id = '$employee_id' AND ts.date = '$display_date' AND ts.locked = '0' ".
                                                                            "ORDER BY p.name, at.type, a.name");
                            ///////////////////////////
                            //  Expense Sheet
                            $expensesheet_info                          = q("SELECT es.id, es.date, p.name, v.type, es.descr, es.kilometers, es.expense, es.area_id ".
                                                                            "FROM (ExpenseSheet AS es LEFT JOIN Project AS p ON es.project_id = p.id ".
                                                                            "LEFT JOIN Vehicle AS v ON es.vehicle_id = v.id) ".
                                                                            "WHERE p.company_id = '".$_SESSION["company_id"]."' AND es.user_id = '$employee_id' ".
                                                                            "AND es.date = '$display_date' AND es.locked = '0' ORDER BY p.name, v.type");
                            ///////////////////////////
                            $allTimeData = "";
                            $allExpenseData = "";
                            ///////////////////////////
                            //  Display Information
                            ///////////////////////////
                                
                            //  Time Sheet
                            if (is_array($timesheet_info)) {
                                //  Table Headers
                                $allTimeData .= "<tr>
                                                            <th class='on-table-light' colspan='100%'><a><font style='color:orange'>Time</font></a></th>
                                                        </tr>
                                                        <tr>
                                                            <th>Date</th>
                                                            <th>Project</th>
                                                            <th>Area</th>
                                                            <th>Activity Type</th>
                                                            <th>Activity</th>   
                                                            <th>Description</th>    
                                                            <th>Time Spent</th> 
                                                            <th></th>
                                                        </tr>";
                                //  Table Information
                                foreach ($timesheet_info as $i) {
                                    if($i[7] != "0"){
                                        $arName = q("SELECT name FROM areas WHERE id = '".$i[7]."' ");
                                    }else{
                                        $arName = " - ";
                                    }
                                    $allTimeData .= "<tr>";
                                        for ($c = 1; $c < 9; $c++){
                                            if($c == 1 || $c == 2){                  // Date, Name
                                                $allTimeData .= "<td style='white-space:nowrap'>".$i[$c]."</td>";
                                            }else if($c == 3){                          // Area
                                                $allTimeData .= "<td>".$arName."</td>";
                                            }else if ($c == 4 || $c <= 6) {
                                                if ($i[$c-1] != "")
                                                    $allTimeData .= "<td>".$i[$c-1]."</td>";
                                                else
                                                    $allTimeData .= "<td> - </td>";
                                            }
                                            else if ($c == 7){
                                                $allTimeData .= "<td class='rightdata'>".number_format($i[$c-1], 2, ".", "")."</td>";
                                            }else {
                                                $checkbox1      .= "".$i[0]."_";
                                                $allTimeData .= "<td align='center'>
                                                    <input method='post' name='time".$i[0]."' type='checkbox' value='1'></td>";
                                            }
                                        }
                                    $allTimeData .= "</tr>";
                                }
                            }
                            ///////////////////////////
                            
                            //  Expense Sheet
                            if (is_array($expensesheet_info)) {
                                //  Table Headers
                                $allExpenseData .= "<tr>
                                                                    <th class='on-table-light' colspan='100%'><a><font style='color:orange'>Expense</font></a></th>
                                                                </tr>
                                                            <tr>
                                                                <th>Date</th>
                                                                <th>Project</th>
                                                                <th>Area</th>
                                                                <th>Vehicle</th>
                                                                <th>Description</th>
                                                                <th>Km</th>
                                                                <th>Expense</th>
                                                                <th></td></th>
                                                            </tr>";
                                //  Table Information
                                foreach ($expensesheet_info as $e) {
                                    if($e[7] != "0"){
                                        $areaName = q("SELECT name FROM areas WHERE id = '".$e[7]."' ");
                                    }else{
                                        $areaName = " - ";
                                    }
                                    $allExpenseData .= "<tr>";
                                        for ($c = 1; $c < 9; $c++){
                                            if ($e[$c] == ""){
                                                $e[$c] = " - ";
                                            }
                                            if ($c == 1|| $c == 2) {
                                                $allExpenseData .= "<td>".$e[$c]."</a></td>";
                                            }else if($c == 3){
                                                $allExpenseData .= "<td>".$areaName."</td>";
                                            }else if ($c == 4 || $c <= 6){
                                                $allExpenseData .= "<td>".$e[$c-1]."</a></td>";
                                            }else if($c == 7){
                                                $allExpenseData .= "<td class='rightdata'>".number_format($e[$c-1], 2, ".", "")."</a></td>";
                                            }else {
                                                $checkbox2      .= "".$e[0]."_";
                                                $allExpenseData .= "<td align='center'><input method='post' name='expense".$e[0]."' type='checkbox' value='1'></td>";
                                            }
                                        }
                                    $allExpenseData .= "</tr>";
                                }    
                            }
                            ///////////////////////////
                        if($allTimeData != "" || $allExpenseData != ""){
                            echo "<br/><br/>";
                            echo "<div class='on-20px'><table class='on-table-center on-table'>";
                            echo $allTimeData;
                            if($allTimeData != "" && $allExpenseData != ""){
                                echo "<tr><td style='background-color:transparent' colspan='100%'> &nbsp;</td></tr>";
                            }
                            echo $allExpenseData;
                            echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                            echo "</table></div>";
                        }
                        
                            if (is_array($timesheet_info) || is_array($expensesheet_info)) {
                                echo "<br/>";
                                echo "<input method='post' name='btnDelete' type='submit' value='Delete Selected Entries'>";
                            }

                            echo "<input method='post' name='checkbox1' type='hidden' value='".$checkbox1."'>";
                            echo "<input method='post' name='checkbox2' type='hidden' value='".$checkbox2."'>";
                        echo "</form>";
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
