<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    if (!hasAccess("CLIENT_MANAGE"))
        header("Location: noaccess.php");

    //  Get Client Information
    //  Clients   
    $client_id = $_GET["id"];
    $client                                                           = q("SELECT id, companyName, address1, address2, city, areaCode, province, country, webURL, phoneNum FROM Client 
                                                                                    WHERE company_id = '".$_SESSION["company_id"]."' 
                                                                                    AND deleted = '0' AND id='".$client_id."' ");

    //  Update Vehicle Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")    {
			include("InvoiceEngine.php");
			$ie_info = q("SELECT invoice_ID, invoice_UN, invoice_PW FROM Company WHERE id = '".$_SESSION["company_id"]."'");
			$ie = new InvoiceEngine($ie_info[0][0],$ie_info[0][1],$ie_info[0][2]); //TODO: get from db

            $client_name = addslashes(strip_tags($_POST["client_name"]));
            $address_1 = addslashes(strip_tags($_POST["address_1"]));
            $address_2 = addslashes(strip_tags($_POST["address_2"]));
            $city_ = addslashes(strip_tags($_POST["city_"]));
            $province_ = addslashes(strip_tags($_POST["province_"]));
            $areaCode_ = addslashes(strip_tags($_POST["areaCode_"]));
            $country_ = addslashes(strip_tags($_POST["country_"]));
            $website_ = addslashes(strip_tags($_POST["website_"]));
            $phoneNumber_ = addslashes(strip_tags($_POST["phoneNumber_"]));
            $tax_id = addslashes(strip_tags($_POST["tax_id"]));

			$external_id = q("SELECT external_id FROM Client WHERE company_id = '".$_SESSION["company_id"]."' AND id='".$client_id."' ");
			//API call
			$ie->client_update($external_id, $client_name, $address_1, $address_2, $city_, $province_, $areaCode, $country_, $website_, 
				$phoneNumber_, "VAT Number", $tax_id);

			$update = q("UPDATE Client SET companyName='".$client_name."', address1='".$address_1."', address2='".$address_2."', 
						city='".$city_."', areaCode='".$areaCode_."', province='".$province_."', country='".$country_."', 
						webURL='".$website_."', phoneNum='".$phoneNumber_."', tax_id='".$tax_id."'
						WHERE id = '".$client_id."' ");
            if($update)
                $errorMessage                                               = "";
            else
                $errorMessage                                               = "Update Error";

        if ($errorMessage == "")    {    
            $time                                                       = date("H:i:s");
            $logs                                                       = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                                    "VALUES ('Client edited', 'Update', 'Client', '".$_SESSION["email"]."', '$today', ".
                                                                                    "'$time', '".$_SESSION["company_id"]."')");

            $errorMessage                                               = "Client Updated Successfully";

            header("Location: clients.php");
        }
    }

    if ($errorMessage != "")
        echo "<div style='width:100%; text-align:center'><strong><font class='on-validate-error'>$errorMessage</font></strong></div>";

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0", "vehicles");
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    function check()
    {
        var valid                                                       = 1;
        
        //  Check That Company Name Is Entered
        if (document.forms["client_edit"].client_name.value == "")  {
            ShowLayer("divName", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("divName", "none");
        /*
        //  Check That Address1 Is Entered
        if (document.forms["client_edit"].address_1.value == "")  {
            ShowLayer("divAddress1", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("divAddress1", "none");
        //  Check That Address2 Is Entered
        if (document.forms["client_edit"].address_2.value == "")  {
            ShowLayer("divAddress2", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("divAddress2", "none");
        //  Check That City Is Entered
        if (document.forms["client_edit"].city_.value == "")  {
            ShowLayer("divCity", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("divCity", "none");
        //  Check That Area code Is Entered
        if (document.forms["client_edit"].areaCode_.value == "")  {
            ShowLayer("divAreaCode", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("divAreaCode", "none");
        //  Check That Province Is Entered
        if (document.forms["client_edit"].province_.value == "")  {
            ShowLayer("divProvince", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("divProvince", "none");
        //  Check That Country Is Entered
        if (document.forms["client_edit"].country_.value == "")  {
            ShowLayer("divCountry", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("divCountry", "none");
        */
        //  Check That Phone Number Is Entered
        if (document.forms["client_edit"].phoneNumber_.value == "")  {
            ShowLayer("divPhoneNumber", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("divPhoneNumber", "none");
        //  Check That Web URL Is Entered
        /*
        if (document.forms["client_edit"].website_.value == "")  {
            ShowLayer("divWebsite", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("divWebsite", "none");
        */
        
        if (valid == 1)
        {
            document.forms["client_edit"].save.value                   = 1;
            document.forms["client_edit"].submit();
        }
    }
</script>
<?php
    //  Clients   
    $client_id = $_GET["id"];
    $client = q("SELECT id, companyName, address1, address2, city, areaCode, province, country, webURL, phoneNum, tax_id FROM Client 
            WHERE company_id = '".$_SESSION["company_id"]."' AND deleted = '0' AND id='".$client_id."' ");
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata">
                <form action="" method="post" name="client_edit">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>
                                    Edit Client
                                </h6>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <br/>
                            </td>
                        </tr>
                    </table>
                    
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                    Client Name:
                            </td>
                            <td width="50%">
                                <?php echo "<input class='on-field' name='client_name' tabindex='1' size='60' type='text' value='".$client[0][1]."'>"; ?>
                                <div id="divName" style="display: none;"><font class="on-validate-error">* Client name must be entered</font></div>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                Address 1
                            </td>
                            <td width="50%">
                                <?php echo "<input class='on-field' name='address_1' tabindex='2' size='60' type='text' value='".$client[0][2]."'>"; ?>
                                <div id="divAddress1" style="display: none;"><font class="on-validate-error">* Address1 must be entered</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                Address 2
                            </td>
                            <td width="50%">
                                <?php echo "<input class='on-field' name='address_2' tabindex='3' size='60' type='text' value='".$client[0][3]."'>"; ?>
                                <div id="divAddress2" style="display: none;"><font class="on-validate-error">* Address2 must be entered</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                City
                            </td>
                            <td width="50%">
                                <?php echo "<input class='on-field' name='city_' tabindex='4' size='60' type='text' value='".$client[0][4]."'>"; ?>
                                <div id="divCity" style="display: none;"><font class="on-validate-error">* City must be entered</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                Area Code
                            </td>
                            <td width="50%">
                                <?php echo "<input class='on-field-date' name='areaCode_' tabindex='5' style='text-align:right' type='text' value='".$client[0][5]."'>"; ?>
                                <div id="divAreaCode" style="display: none;"><font class="on-validate-error">* Area Code must be entered</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                Province
                            </td>
                            <td width="50%">
                                <?php echo "<input class='on-field' name='province_' tabindex='6' size='60' type='text' value='".$client[0][6]."'>"; ?>
                                <div id="divProvince" style="display: none;"><font class="on-validate-error">* Province must be entered</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                Country
                            </td>
                            <td width="50%">
                                <?php echo "<input class='on-field' name='country_' tabindex='7' size='60' type='text' value='".$client[0][7]."'>"; ?>
                                <div id="divCountry" style="display: none;"><font class="on-validate-error">* Country must be entered</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                Phone Number
                            </td>
                            <td width="50%">
                                <?php echo "<input class='on-field' name='phoneNumber_' tabindex='8' size='60' type='text' value='".$client[0][9]."'>"; ?>
                                <div id="divPhoneNumber" style="display: none;"><font class="on-validate-error">* Phone number must be entered</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                Website
                            </td>
                            <td width="50%">
                                <?php echo "<input class='on-field' name='website_' tabindex='9' size='60' type='text' value='".$client[0][8]."'>"; ?>
                                <div id="divWebsite" style="display: none;"><font class="on-validate-error">* Web URL must be entered</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                VAT Number
                            </td>
                            <td width="50%">
                                <?php echo "<input class='on-field' name='tax_id' tabindex='8' size='60' type='text' value='".$client[0][10]."'>"; ?>
                                <div id="divTaxID" style="display: none;"><font class="on-validate-error">* VAT Number must be entered</font></div>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <input name="btnUpdate" onClick="check();" tabindex="2" type="button" value="Update Client">
                    <input method="post" name="save" type="hidden" value="0" />
                </form>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
