<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    //  Create Notification List - Actioned Events
	if (hasAccess("BUDGET_MANAGE"))
        $actioned_events[] = "Budget";
	if (hasAccess("INVOICE_MANAGE"))
        $actioned_events[] = "Invoice";
	if (hasAccess("EXPENSE_MANAGE"))
        $actioned_events[] = "Expense";
	if (hasaccess("LEAVE_MANAGE_ALL"))
        $actioned_events[] = "Leave";
	if (hasAccess("APPROVAL_MANAGE"))
        $actioned_events[] = "Approved/Updated";

    //  Create Notification List - Non-Actioned Events
    $non_actioned_events                                                = array("Bookings", "SLA", "ExpenseMonthly","OverBudget","PastDueDate");

    //  Add/Update SLA Button Pressed
    if (isset($_POST["save"]) && $_POST["save"] === "1")
    {
        //  Assign Actioned Event Notifications
        foreach ($actioned_events as $actioned_event)
        {
            $notify_id                                                  = q("SELECT id FROM Notify WHERE code = '".$actioned_event."'");

            if ($_POST[$actioned_event])
            {
                $exist                                                  = q("SELECT id FROM Association WHERE user_id = '".$_SESSION["user_id"]."' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."' AND notify_id = '$notify_id'");

                if (!$exist)
                    $insert                                             = q("INSERT INTO Association (user_id, company_id, notify_id) ".
                                                                            "VALUES ('".$_SESSION["user_id"]."','".$_SESSION["company_id"]."', '$notify_id')");
            }
            else
            {
                $exist                                                  = q("SELECT id FROM Association WHERE user_id = '".$_SESSION["user_id"]."' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."' AND notify_id = '$notify_id'");

                if ($exist)
                    $delete                                             = q("DELETE FROM Association WHERE user_id = '".$_SESSION["user_id"]."' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."' AND notify_id = '$notify_id'");
            }
        }

        //  Assign Non-Actioned Event Notifications
        foreach ($non_actioned_events as $non_actioned_event)
        {
            $notify_id                                                  = q("SELECT id FROM Notify WHERE code = '".$non_actioned_event."'");

            if ($_POST[$non_actioned_event])
            {
                $exist                                                  = q("SELECT id FROM Association WHERE user_id = '".$_SESSION["user_id"]."' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."' AND notify_id = '$notify_id'");

                if (!$exist)
                    $insert                                             = q("INSERT INTO Association (user_id, company_id, notify_id) ".
                                                                            "VALUES ('".$_SESSION["user_id"]."','".$_SESSION["company_id"]."', '$notify_id')");
            }
            else
            {
                $exist                                                  = q("SELECT id FROM Association WHERE user_id = '".$_SESSION["user_id"]."' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."' AND notify_id = '$notify_id'");

                if ($exist)
                    $delete                                             = q("DELETE FROM Association WHERE user_id = '".$_SESSION["user_id"]."' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."' AND notify_id = '$notify_id'");
            }
        }

        $errorMessage                                                   = "Notification Assignments Successful";
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("7", "notify");

    if ($errorMessage != "")
    {
        echo "<div style='width:100%; text-align:center;'><font class='on-validate-error'>$errorMessage</font></div>";
        echo "<br/>";
    }
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    function check()
    {
        document.forms["notifications"].save.value                      = 1;
        document.forms["notifications"].submit();
    }
</script>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="notifications">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Notifications</h6>
                            </td>
                        </tr>
                    </table>
                    <table class="on-table-center on-table">
                        <tr>
                            <td class="centerdata" colspan="2">
                                <br/>
                                <h6><a>Non-Actioned Events</a></h6>
                                <br/><br/>
                            </td>
                        </tr>
                        <!--  Table Headings  -->
                        <tr>
                            <th>Notification Type</th>
                            <th>Receive It</th>
                        </tr>
                        <!--  Table Information  -->
                        <?php
                            foreach ($actioned_events as $actioned_event)
                            {
                                echo "<tr>";
                                    echo "<td>";
                                        if ($actioned_event == "Budget")
                                            echo "Budget Created/Updated";
                                        else if ($actioned_event == "Invoice")
                                            echo "Invoice Created/Updated";
                                        else if ($actioned_event == "Expense")
                                            echo "Expense Created/Updated";
                                        else if ($actioned_event == "Leave")
                                            echo "Leave Booked";
                                        else if ($actioned_event == "Approved/Updated")
                                            echo "Time/Expenses Approved/Updated";
                                    echo "</td>";

                                    $notify_id                          = q("SELECT id FROM Notify WHERE code = '".$actioned_event."'");
                                    $notify                             = q("SELECT id FROM Association WHERE user_id = '".$_SESSION["user_id"]."' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."' AND notify_id = '$notify_id'");

                                    if ($notify)
                                        echo "<td class='centerdata'><input name='".$actioned_event."' type='checkbox' value='1' checked></td>";
                                    else
                                        echo "<td class='centerdata'><input name='".$actioned_event."' type='checkbox' value='1'></td>";
                                echo "</tr>";
                            }
                        ?>
                        <tr>
                            <td class="centerdata" colspan="2">
                                <br/>
                                <h6><a>Non-Actioned Events</a></h6>
                                <br/><br/>
                            </td>
                        </tr>
                        <!--  Table Headings  -->
                        <tr>
                            <th>Notification Type</th>
                            <th>Receive It</th>
                        </tr>
                        <!--  Table Information  -->
                        <?php
                            foreach ($non_actioned_events as $non_actioned_event)
                            {
                                echo "<tr>";
                                    echo "<td>";
                                        if ($non_actioned_event == "Bookings")
                                            echo "Employee(s) Behind Schedule";
                                        else if ($non_actioned_event == "SLA")
                                            echo "Service Level Agreement(s)";
                                        else if ($non_actioned_event == "ExpenseMonthly")
                                            echo "Monthly Expense Report/Employee";
                                        else if ($non_actioned_event == "OverBudget")
                                            echo "Project Over Budget";
                                        else if ($non_actioned_event == "PastDueDate")
                                            echo "Project Pasted Due Date";
                                    echo "</td>";

                                    $notify_id                          = q("SELECT id FROM Notify WHERE code = '".$non_actioned_event."'");
                                    $notify                             = q("SELECT id FROM Association WHERE user_id = '".$_SESSION["user_id"]."' ".
                                                                            "AND company_id = '".$_SESSION["company_id"]."' AND notify_id = '$notify_id'");

                                    if ($notify)
                                        echo "<td class='centerdata'><input name='".$non_actioned_event."' type='checkbox' value='1' ".
                                            "checked></td>";
                                    else
                                        echo "<td class='centerdata'><input name='".$non_actioned_event."' type='checkbox' value='1'></td>";
                                echo "</tr>";
                            }
                        ?>
                    </table>
                    <br/>
                    <input name="btnAdd" onClick="check();" type="button" value="Assign Notifications">
                    <input method="post" name="save" type="hidden" value="0" />
                </form>
            </td>
        </tr>
        <tr>
            <td class="centerdata">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
