<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

	//if (!hasAccess("VEH_MANAGE"))
	//	header("Location: noaccess.php");

    //  Insert Contact Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")	{
		include("InvoiceEngine.php");
		$ie_info = q("SELECT invoice_ID, invoice_UN, invoice_PW FROM Company WHERE id = '".$_SESSION["company_id"]."'");
		$ie = new InvoiceEngine($ie_info[0][0],$ie_info[0][1],$ie_info[0][2]); //TODO: get from db

        $errorMessage                                                   = "";

        //  Get Information
        $client_id                                                     = addslashes(strip_tags($_POST["client"]));
        $first_name                                                     = addslashes(strip_tags($_POST["first_name"]));
        $last_name                                                      = addslashes(strip_tags($_POST["last_name"]));
        $email_                                                            = addslashes(strip_tags($_POST["email_"]));
        $officeNum_                                                    = addslashes(strip_tags($_POST["officeNum_"]));
        $phoneNum_                                                   = addslashes(strip_tags($_POST["phoneNum_"]));
        

        //  Check If Contact Exists In Database
        $exist = q("SELECT id FROM Contact WHERE firstName = '$first_name' AND lastName = '$last_name' AND company_id = '".$_SESSION["company_id"]."'");

        if (!$exist)    {
			$client_external_id = q("SELECT external_id FROM Client WHERE id = '$client_id'");
            //API call
			$external_id = $ie->people_new($client_external_id, $first_name, $last_name, $email_, $officeNum_, $phoneNum_);
			
            $insert = q("INSERT INTO Contact (client_id, company_id, firstName, lastName, email, officeNum, phoneNum, external_id) 
				VALUES ('".$client_id."', '".$_SESSION["company_id"]."', '".$first_name."', '".$last_name."', '".$email_."', 
				'".$officeNum_."', '".$phoneNum_."', '$external_id')");

            if ($insert)    {
                $time                                                   = date("H:i:s");
                $logs                                                   = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                                "VALUES ('$first_name created', 'Insert', 'Contact', '".$_SESSION["email"]."', ".
                                                                                "'$today', '$time', '".$_SESSION["company_id"]."')");
                $errorMessage                                           = "Contact Added Successfully";
                header("Location: contacts.php?id=".$client_id."");
            }
        }else   {
            $errorMessage                                               = "Contact Already Exists";
        }
    }

    if ($errorMessage != "")    {
        echo "<p align='center' style='padding:0px;'><strong><font class='on-validate-error'>$errorMessage</font></strong></p>";
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0", "vehicles");
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    jQuery(function()    {
        //  Client Change - Show/Hide
        jQuery("#client").change(function()    {
            if(jQuery("#client").val() == "null") {
                jQuery("#divClient").hide();
            }else   {
                jQuery("#divClient").show();
            }
        });
    });
    function check()
    {
        var valid                                                       = 1;

        //  Check That First Name Name Is Entered
        if (document.forms["contact_add"].first_name.value == "")   {
            ShowLayer("firstName", "block");
            valid                                                       = 0;
        }else   {
            ShowLayer("firstName", "none");
        }
        //  Check That Last Name Is Entered
        if (document.forms["contact_add"].last_name.value == "") {
            ShowLayer("lastName", "block");
            valid                                                       = 0;
        }else   {
            ShowLayer("lastName", "none");
        }
        //  Check That Email Is Entered
        if (document.forms["contact_add"].email_.value == "") {
            ShowLayer("email", "block");
            valid                                                       = 0;
        }else   {
            ShowLayer("email", "none");
        }
        //  Check That Office Number Is Entered
        if (document.forms["contact_add"].officeNum_.value == "") {
            ShowLayer("officeNum", "block");
            valid                                                       = 0;
        }else   {
            ShowLayer("officeNum", "none");
        }
        //  Check That Phone Number Is Entered
        if (document.forms["contact_add"].phoneNum_.value == "") {
            ShowLayer("phoneNum", "block");
            valid                                                       = 0;
        }else   {
            ShowLayer("phoneNum", "none");
        }

        if (valid == 1)
        {
            document.forms["contact_add"].save.value                    = 1;
            document.forms["contact_add"].submit();
        }
    }
</script>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata">
                <form action="" method="post" name="contact_add">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>
                                    Add New Contact
                                </h6>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <br/>
                            </td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                           <td class="on-description" width="50%">Client</td>
                            <td width="50%">
                                <select id="client" name="client" method="post" class="on-field required">
                                    <option value="null">--  Select A Client  --</option>
                                    <?php
                                        $clients = q("SELECT id, companyName FROM Client WHERE company_id = '".$_SESSION["company_id"]."' AND deleted ='0' ORDER BY companyName");
                                        if (is_array($clients))    {
                                            foreach ($clients as $client) {
                                                if($client[0] == $_GET["id"])   {
                                                    echo "<option value='".$client[0]."' selected>".$client[1]."</option>";
                                                }else   {
                                                    echo "<option value='".$client[0]."'>".$client[1]."</option>";
                                                }
                                            }
                                        }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <?php 
                        if (!is_array($clients))    {
                            echo "<tr>";
                                echo "<td class='on-description' width='50%'></td>";
                                echo "<td style='width:50%; padding-left:30px;'>";
                                    echo "<a style='color:orange;'> - No Clients added yet - </a>";
                                echo "</td>";
                            echo "</tr>";
                        }
                        ?>
                    </table>
                    
                    <div id="divClient" style="display: block;">
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                        First Name
                                </td>
                                <td width="50%">
                                    <input class="on-field" name="first_name" size="60" type="text">
                                    <div id="firstName" style="display: none;"><font class="on-validate-error">* Name must be entered</font></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Last Name
                                </td>
                                <td width="50%">
                                    <input class="on-field" name="last_name" type="text">
                                    <div id="lastName" style="display: none;"><font class="on-validate-error">* Last Name must be entered</font></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Email
                                </td>
                                <td width="50%">
                                    <input class="on-field" name="email_" type="text">
                                    <div id="email" style="display: none;"><font class="on-validate-error">* Email address must be entered</font></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Office Number
                                </td>
                                <td width="50%">
                                    <input class="on-field" name="officeNum_" type="text">
                                    <div id="officeNum" style="display: none;"><font class="on-validate-error">* Office Number must be entered</font></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Phone Number
                                </td>
                                <td width="50%">
                                    <input class="on-field" name="phoneNum_" type="text">
                                    <div id="phoneNum" style="display: none;"><font class="on-validate-error">* Phone Number Code must be entered</font></div>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <input name="btnAdd" onClick="check();" type="button" value="Add Contact">
                        <input method="post" name="save" type="hidden" value="0" />
                    </div>
                    
                </form>
            </td>
        </tr>
        <tr>
            <td>
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
