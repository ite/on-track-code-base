<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("include/sajax.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    if (!hasAccess("REP_TIME_SHEET"))
        header("Location: noaccess.php");
        
    // Class for table background color
    $colorClass = "OnTrack_#_";

    ///////////////////////////
    //  Sajax
    
    function getProjects($id) {
        if ($id == "null")
            $projects = q("SELECT DISTINCT(p.id),p.p_type, p.name FROM (Project AS p 
                            INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) 
                            WHERE cot.company_id = '".$_SESSION["company_id"]."' 
                            AND p.completed = '0' 
                            AND p.deleted = '0' ORDER BY UPPER(p.name)");
        else
            $projects = q("SELECT DISTINCT(p.id),p.p_type, p.name FROM (Project AS p 
                            INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) 
                            WHERE cot.company_id = '".$_SESSION["company_id"]."' 
                            AND p.completed = '0' 
                            AND p.deleted = '0' 
                            AND p.type_id='$id' ORDER BY UPPER(p.name)");

        return $projects;
    }
    
    function getAreas($id){
        $id = substr($id,3);
        $areas = q("SELECT id, name FROM areas WHERE pID='$id' ORDER BY name");
        return $areas ;
    }
    
    function getActivityTypes($id) {
        $pType = substr($id,0,2);
        $id = substr($id,3);

        if ($pType == "CP")     return q("SELECT id, type FROM ActivityTypes WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY type");
        else                    return q("SELECT id, type FROM ActivityTypes WHERE pID= '".$id."' ORDER BY type");
    }
    
    function getActivities($id) {
        return q("SELECT id, name FROM Activities WHERE parent_id = '$id' ORDER BY name");
    }

    $sajax_request_type = "GET";
    sajax_init();    
    sajax_export("getProjects");
    sajax_export("getAreas");
    sajax_export("getActivityTypes");
    sajax_export("getActivities");

    sajax_handle_client_request();
    ///////////////////////////

    //  Set Report Generated Status
    $generated = "0";
    
    // Excel export Variables
    $excelheadings;
    $row0Head = "";
    $row1Head = "";
    $row2Head = "";
    $row3Head = "";
    $totalColCount = 0;
    
    GLOBAL $excelheadings;
    
    function printHeadings($val, $level, $cols){
        GLOBAL $excelheadings;
        if($cols == 9){                                     // 9
            $excelheadings[$level][] = $val;
            for($i=1; $i<=$cols; $i++)
                $excelheadings[$level][] ="";
        }else  if($cols == 8){                            // 8
            $excelheadings[$level][] = $val;
            for($i=1; $i<=$cols; $i++)
                $excelheadings[$level][] ="";
        }else  if($cols == 7){                            // 7
            $excelheadings[$level][] = $val;
            for($i=1; $i<=$cols; $i++)
                $excelheadings[$level][] ="";
        }else  if($cols == 6){                            // 6
            $excelheadings[$level][] = $val;
            for($i=1; $i<=$cols; $i++)
                $excelheadings[$level][] ="";
        }
    }

    //  Create Display Information Function
    if (isset($_POST["save"]) && $_POST["save"] === "1") 
    {
        GLOBAL $excelheadings;
        
        $errorMessage = "";
        $employee_id = $_POST["employee"];
        $project_type_id  = $_POST["projectType"];
        $project_id = $_POST["project"];
        $project_id = substr($project_id,3);
        $area_id = $_POST["area"];        
        $activity_type_id = $_POST["activityType"];
        $activity_id = $_POST["activity"];
        $date_type = $_POST["dateType"];
        
        //  Get Dates
        if ($date_type == "currentwk") {
            $date_from = currentWeekStart();
            $date_to = $today;
        }
        else if ($date_type == "previouswk") {
            $date_from = getDates(currentWeekStart(), -7);
            $date_to = getDates(currentWeekStart(), -1);
        }
        else if ($date_type == "currentmnth") {
            $date_from = getMonthStart($today);
            $date_to = $today;
        }
        else if ($date_type == "previousmnth") {
            $date_from = getPreviousMonthStart();
            $date_to = getMonthEnd(getPreviousMonthStart());
        }
        else if ($date_type == "custom") {
            $date_from = addslashes(strip_tags($_POST["date_from"]));
            $date_to = addslashes(strip_tags($_POST["date_to"]));
        }

        //  Timestamp
        if ($date_from === $date_to)
            $timestamp = "Time Period: ".$date_from;
        else
            $timestamp = "Time Period: ".$date_from." - ".$date_to;

        //  Get Data According to Selected Values
        $tableHeads = "<tr>";
        $headings = "";
        $columns = 6 ;
		$from = "Project AS p 
                        INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id 
                        INNER JOIN TimeSheet AS ts ON ts.project_id = p.id";
        $select = "";
        $where = "ts.company_id = '".$_SESSION["company_id"]."' 
                        AND cot.company_id = '".$_SESSION["company_id"]."' 
                        AND ts.date >= '$date_from' AND ts.date <= '$date_to' ";
        $order = "ts.date, ts.on_date, ts.on_time";
        
        ///////////////////////  AREAS  /////////////////////////////
        if($project_id  != 0){          // If a specific project is selected
            $areas = q("SELECT id, name FROM areas WHERE active = '1' AND pID='".$project_id."' ORDER BY name");
            if($areas != null){
                $hasAreas = 1;
            }
        }else{  // ELSE Check each project for a area
           $areas2 = q("SELECT area_id FROM TimeSheet WHERE area_id >0 AND date BETWEEN '".$date_from."' AND '".$date_to."' ");
            if($areas2 != null || $areas2 != ""){
                $hasAreas = 1;
            }
        }
        //////////////////////////////////////////////////////////
        
        //  Get Employee Info
        $row = 0;           // Excel
        $row0Head = "Report: Time Sheet";
        
        if (!($employee_id != "null" && is_numeric($employee_id))) {
            $columns++;
            $headings .= "<tr><td class='on-table-clear' colspan='".($columns+2+$hasAreas)."'><a>Report for All Employees<a></td></tr>";
            $tableHeads .= "<th>Employee Name</th>";        // 0
            $from = "".$from." INNER JOIN Employee AS e ON ts.user_id = e.id";
            $select .= "CONCAT(e.lstname, ', ', e.frstname), ";
                $exceldata[$row][] = $colorClass."Employee Name";
                $row1Head = "Report for All Employees";
        }
        else {
            $empInfo = q("SELECT lstname, frstname FROM Employee WHERE id = '$employee_id'");
            $headings .= "<tr><td class='on-table-clear' colspan='".($columns+2+$hasAreas)."'><a>Report for ".$empInfo[0][0].", ".$empInfo[0][1]."</a></td></tr>";       
            $where .= "AND ts.user_id = '$employee_id' ";
            $specificEmployee = 1;
                $row1Head = "Report for ".$empInfo[0][0].", ".$empInfo[0][1];
        }

        //  Get Project Info
        if (!($project_id != "null" && is_numeric($project_id))) {
            $columns++;
            $tableHeads .= "<th>Project Name</th>";     // 1
            
            if (!($project_type_id != "null" && is_numeric($project_type_id))) {
                $headings .= "<tr><td class='on-table-clear' colspan='".($columns+1+$hasAreas)."'>Report for All Projects <i>{All Project Types}</i></td></tr>";        // All Projects - All types
                $from = "".$from." LEFT JOIN areas AS ar ON ts.area_id = ar.id";
                if($hasAreas){
                    $select .= "p.name, ar.name, ";
                    $where .= "AND ts.company_id = '".$_SESSION['company_id']."' ";
                }else{
                    $select .= "p.name, ";
                    $where .= "AND ts.company_id = '".$_SESSION['company_id']."' ";
                }
                $order .= ", p.name ";
                    $exceldata[$row][] = $colorClass."Project Name";
                    $row2Head = "Report for All Projects {All Project Types}";
            }
            else {
                $headings .= "<tr><td class='on-table-clear' colspan='".($columns+1+$hasAreas)."'>Report for All Projects</td></tr>";       // All Projects
                $from = "".$from." LEFT JOIN areas AS ar ON ts.area_id = ar.id";
                if($hasAreas){
                    $select .= "p.name, ar.name, ";
                    $where .= "AND ts.company_id = '".$_SESSION['company_id']."' AND p.type_id = '$project_type_id' ";
                }else{
                    $select .= "p.name, ";
                    $where .= "AND ts.company_id = '".$_SESSION['company_id']."' AND p.type_id = '$project_type_id' ";
                }
                $order .= ", p.name";
                    $exceldata[$row][] = $colorClass."Project Name";
                    $row2Head = "Report for All Projects";
            }
        }
        else {          // Specific Project
            if($hasAreas){
                $select .= "ar.name, ";
            }
            $projectInfo = q("SELECT name FROM Project WHERE id = '$project_id'");
            $headings .= "<tr><td class='on-table-clear' colspan='".($columns+1+$hasAreas)."'>Project: ".$projectInfo."</td></tr>";
            $where .= "AND ts.project_id = '$project_id'";
            $specificProj = 1;
                $row2Head = "Project: ".$projectInfo;
        }

        if($specificProj){
            $from = "".$from." INNER JOIN Activities AS a ON ts.activity_id = a.id INNER JOIN ActivityTypes AS at ON a.parent_id = at.id LEFT JOIN areas AS ar ON ts.area_id = ar.id";
        }else{
            $from = "".$from." INNER JOIN Activities AS a ON ts.activity_id = a.id INNER JOIN ActivityTypes AS at ON a.parent_id = at.id";
        }
        $select .= "at.type, a.name, ts.descr, ts.date, ts.rate, ts.time_spent";
        
        // Get Area Info
        if($area_id != null && is_numeric($area_id))
            $where .= " AND ar.id = '$area_id' ";
        
        //  Get Activity Type/Activity Info
        if (!($activity_id != "null" && is_numeric($activity_id))) {
            if ($activity_type_id != "null" && is_numeric($activity_type_id))
                $where .= "AND at.id = '$activity_type_id' ";
        }
        else
            $where .= "AND ts.activity_id = '$activity_id' ";
        
        // Areas
        $area_head                                             = "";
        if($hasAreas) {                                                 // Check if there is areas linked to these entries
            $area_head                                             = "<th>Area</th>";   // 3
            $exceldata[$row][] = "Area"; 
       }

        $tableHeads .= $area_head; 
        $tableHeads .= "<th>Activity Type</th>";                // 4
        $tableHeads .= "<th>Activity</th>";                         // 5
        $tableHeads .= "<th>Description</th>";                  // 6
        $tableHeads .= "<th>Date</th>";                             // 7
        $tableHeads .= "<th>Rate</th>";                             // 8
        $tableHeads .= "<th>Time Spent</th>";                   // 9
        $tableHeads .= "<th>Cost (".$_SESSION["currency"].")</th>";                             // 10
        
            $exceldata[$row][] = $colorClass."Activity Type";               // 4
            $exceldata[$row][] = $colorClass."Activity";                        // 5
            $exceldata[$row][] = $colorClass."Description";                 // 6
            $exceldata[$row][] =$colorClass. "Date";                            // 7
            $exceldata[$row][] =$colorClass. "Rate";                            // 8
            $exceldata[$row][] = $colorClass."Time Spent";                 // 9
            $exceldata[$row][] = $colorClass."Cost";                           // 10
                $row++;
        
        $headings .= "<tr><td class='on-table-clear' colspan=' ".($columns+$hasAreas+1)." '>".$timestamp."</td></tr>";
            $row3Head = $timestamp;
            
        $query = "SELECT $select FROM $from WHERE $where ORDER BY $order";
        
        //echo "<a>".$query."</a>";
        
        $info = q($query);

        $displayString = "";

        //  Create Display String
        if (is_array($info)) {
            $displayString .= "".$headings;
            $displayString .= "".$tableHeads;            

            $total = 0;
            $colCount = 0;
            $totalcost = 0;
            
            foreach ($info as $r) {
                $displayString .= "<tr>";
                
                //////////  TOTAL  //////////
                $total += $r[count($r)-1];
                
                foreach ($r as $d) {
                    if (!preg_match("/^((\d{2}(([02468][048])|([13579][26]))[\-\/\s]?((((0?[13578])|(1[02]))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-\/\s]?((0?[1-9])|([1-2][0-9])))))|(\d{2}(([02468][1235679])|([13579][01345789]))[\-\/\s]?((((0?[13578])|(1[02]))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-\/\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))(\s(((0?[1-9])|(1[0-2]))\:([0-5][0-9])((\s)|(\:([0-5][0-9])\s))([AM|PM|am|pm]{2,2})))?$/", $d)) {
                        if($hasAreas){      // If Areas
                            if($d == ""){
                                $displayString  .= "<td>-</td>";
                            }else if($colCount ==$columns-2+$hasAreas){     // Rate
                                $displayString  .= "<td class='rightdata'>".$d."</td>";
                                $rate = $d;
                            }else if ($colCount ==$columns-3+$hasAreas){    // Time Spent
                                $displayString  .= "<td class='rightdata'>".$d."</td>";
                                    $timeSpent = $d;
                            }else{
                                $displayString  .= "<td style='white-space:nowrap'>".$d."</td>";
                            }
                        }else{                  // If No Areas
                            if($d == ""){ 
                                $displayString  .= "<td>-</td>";
                           }else if($colCount  == $columns-2) {     // Rate
                                $displayString  .= "<td class='rightdata'>".$d."</td>";
                                $rate = $d;
                           }else if($colCount  == $columns-3) {     // Time Spent
                                $displayString  .= "<td class='rightdata'>".$d."</td>";
                                $timeSpent = $d;
                            }else{
                                $displayString  .= "<td style='white-space:nowrap'>".$d."</td>";
                            }
                        }
                        $colCount++;
                    }else{
                         $displayString  .= "<td style='white-space:nowrap; width:80px'>".$d."</td>";
                    }
                    $exceldata[$row][] = $d;
                }
                $colCount = 0;
                $totalcost += ($rate * $timeSpent);
                $cost = $rate * $timeSpent;                
                
                $exceldata[$row][] = $cost;
                $row++;
                
                $displayString .= "<td class='rightdata'>".number_format($cost, 2, ".", "")."</td></tr>";
            }
            
            for($i=0; $i<$columns-2+$hasAreas; $i++){
                $exceldata[$row][] = "";  
            }
            $exceldata[$row][] = "Total: "; 
            $exceldata[$row][] = number_format($total, 2, ".", ""); 
            $exceldata[$row][] = number_format($totalcost, 2, ".", ""); ; 
            
            $displayString .= "<tr>";
            $displayString .= "<td class='on-table-total' colspan=' ".($columns-1+$hasAreas)." '>Total:</td>
                                         <td class='on-table-total'>".number_format($total, 2, ".", "")."</td>
                                         <td class='on-table-total' style='white-space:nowrap'>".$_SESSION["currency"]." ".number_format($totalcost, 2, ".", "")."</td>";
            $displayString .= "<tr>";
        }

        if ($displayString != "")
            $generated                                                  = "1";
    }
    
    $totalColCount = count($exceldata[1])-1;
    
    printHeadings($row0Head, 0, $totalColCount);
    printHeadings($row1Head, 1, $totalColCount);
    printHeadings($row2Head, 2, $totalColCount);
    printHeadings($row3Head, 3, $totalColCount);
   
    //  Print Header
    print_header();
    //  Print Menu
    print_menus("3", "reports");
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    jQuery(function(){
	jQuery('#date_from').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_from').val(),
		current: jQuery('#date_from').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_from').val()))
			_date = new Date();
			else _date = jQuery('#date_from').val();
			jQuery('#date_from').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_from').val(formated);
			jQuery('#date_from').DatePickerHide();
		}
	});
    })

    jQuery(function(){
	jQuery('#date_to').DatePicker({
		format:'Y-m-d',
		date: jQuery('#date_to').val(),
		current: jQuery('#date_to').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#date_to').val()))
			_date = new Date();
			else _date = jQuery('#date_to').val();
			jQuery('#date_to').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#date_to').val(formated);
			jQuery('#date_to').DatePickerHide();
		}
	});
    })

    //  Sajax
    <?php sajax_show_javascript(); ?>

    function check()
    {
        var valid                                                       = 1;

        if (document.forms["report"].dateType.value == "custom") {
            //  Check That Date From Is Entered
            if (document.forms["report"].date_from.value == "") {
                ShowLayer("dateFrom", "block");
                valid                                                   = 0;
            }
            //  Check That Entered Date From Is Valid
            else {
                ShowLayer("dateFrom", "none");

                if (!validation("date", document.forms["report"].date_from.value)) {
                    ShowLayer("dateFromDiv", "block");
                    valid                                               = 0;
                }
                else
                    ShowLayer("dateFromDiv", "none");
            }

            //  Check That Date To Is Entered
            if (document.forms["report"].date_to.value == "") {
                ShowLayer("dateTo", "block");
                valid                                                   = 0;
            }
            //  Check That Entered Date To Is Valid
            else {
                ShowLayer("dateTo", "none");

                if (!validation("date", document.forms["report"].date_to.value)) {
                    ShowLayer("dateToDiv", "block");
                    valid                                               = 0;
                }
                else
                    ShowLayer("dateToDiv", "none");
            }
        }

        if (valid == 1) {
            document.forms["report"].save.value                         = 1;
            document.forms["report"].submit();
        }
    }

    function dateSelection() {
        if (document.forms["report"].dateType.value == "custom")
            ShowLayer("datesDiv", "block");
        else
            ShowLayer("datesDiv", "none");
    }

    ///////////////////////// -- Sajax -- ///////////////////////////
    function ClearOptions(OptionList) {
        for (x = OptionList.length; x >= 0; x--)
        {
            OptionList[x]                                               = null;
        }
    }
    
    // Set Projects
    function setProjects(data) {
        var c                                                           = 0;
        document.report.project.options[(c++)]                         = new Option("All Projects", "null");
        for (var i in data)
            eval("document.report.project.options["+(c++)+"] = new Option('" + data[i][2] + "', '" + data[i][1] + "_" + data[i][0] + "');");
    }
    
    //Set Areas
    function setAreas(data) {
        var c                                                           = 0;
        document.report.area.options[(c++)]                         = new Option("All Areas", "null");
        for (var i in data)
            eval("document.report.area.options["+(c++)+"] = new Option('" + data[i][1] + "', '" + data[i][0] + "');");
    }
    
    //Set ActivityTypes
    function setActivityTypes(data) {
        var c                                                           = 0;
        document.report.activityType.options[(c++)]                         = new Option("All Activity Types", "null");
        for (var i in data)
            eval("document.report.activityType.options["+(c++)+"] = new Option('" + data[i][1] + "', '" + data[i][0] + "');");
    }
    
    //Set Activities
    function setActivities(data) {
        var c                                                           = 0;
        document.report.activity.options[(c++)]                         = new Option("All Activities", "null");
        for (var i in data)
            eval("document.report.activity.options["+(c++)+"] = new Option('" + data[i][1] + "', '" + data[i][0] + "');");
    }
    
    /////////////////// -- GET Functions -- ///////////////////////
    
    //Get Projects
    function getProjects(select) {
        ClearOptions(document.report.project);
        x_getProjects(select.value, setProjects);
    }
    
    //Get Areas
    function getAreas(select) {
        ClearOptions(document.report.area);
        if (select.value != "null")
        {
            ShowLayer("areaDiv", "block");
            x_getAreas(select.value, setAreas);
        }
        else{
            ShowLayer("areaDiv", "none");
        }
    }
    
    //Get ActivityTypes
    function getActivityTypes(select) {
        ClearOptions(document.report.activityType);
        if (select.value != "null")
        {   
            ShowLayer("activityTypeDiv", "block");
            x_getActivityTypes(select.value, setActivityTypes);
        }
        else{
            ShowLayer("activityTypeDiv", "none");
        }
    }
    
    //Get Activities/Data
    function getData(select) {
        ClearOptions(document.report.activity);

        if (select.value != "null")
        {
            ShowLayer("activityDiv", "block");
            x_getActivities(select.value, setActivities);
        }
        else{
            ShowLayer("activityDiv", "none");
        }
    }
    ///////////////////////////
</script>
<?php
    $employees = q("SELECT e.id, e.lstname, e.frstname FROM (Employee AS e INNER JOIN Company_Users AS cu ON e.id = cu.user_id) ".
                                "WHERE cu.company_id = '".$_SESSION["company_id"]."' AND e.email != 'admin' ORDER BY e.lstname, frstname");
    $projectTypes = q("SELECT id, type FROM ProjectTypes WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY type");
    $projectTypes[] = array("0","Shared Projects");
    $projectTypes = sortArrayByColumn($projectTypes, 1);
    
    $projects = q("SELECT DISTINCT(p.id),p.p_type, p.name FROM (Project AS p INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) ".
                        "WHERE cot.company_id = '".$_SESSION["company_id"]."' AND p.completed = '0' AND p.deleted = '0' ORDER BY UPPER(p.name)");
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata">
                <form action="" method="post" name="report">
                    <div id="" style="<?php if ($generated == "1") echo "display: none;"; else echo "display: block"; ?>">
                        <table width="100%">
                            <tr>
                                <td class="centerdata">
                                    <h6>Time Sheet Report</h6>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Employee:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="employee">
                                        <option value="null">All Employees</option>
                                        <?php
                                            if (is_array($employees))
                                                foreach ($employees as $employee)
                                                    if ($_POST["employee"] == $employee[0])
                                                        echo "<option value='".$employee[0]."' selected>".$employee[1].", ".$employee[2]."</option>";
                                                    else
                                                        echo "<option value='".$employee[0]."'>".$employee[1].", ".$employee[2]."</option>";
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Project Type:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="projectType" onChange="getProjects(projectType);">
                                        <option value="null">All Project Types</option>
                                        <?php
                                            if (is_array($projectTypes))
                                                foreach ($projectTypes as $projectType)
                                                    if ($_POST["projectType"] == $projectType[0])
                                                        echo "<option value='".$projectType[0]."' selected>".$projectType[1]."</option>";
                                                    else
                                                        echo "<option value='".$projectType[0]."'>".$projectType[1]."</option>";
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Project:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" id="project" name="project" onChange="getAreas(project); getActivityTypes(project)">
                                        <option value="null">All Projects</option>
                                        <?php
                                            if (is_array($projects))
                                                foreach ($projects as $project)
                                                    if (isset($_POST["project"]) && $_POST["project"] == $project[1]."_".$project[0])
                                                        echo "<option value='".$project[1]."_".$project[0]."' selected>".$project[2]."</option>";
                                                    else
                                                        echo "<option value='".$project[1]."_".$project[0]."'>".$project[2]."</option>";
                                        ?>
                                    </select>
                                </td>
                            </tr>
                        </table>
                        
                        <div id="areaDiv" style="display: none;">
                            <table width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Select Area:
                                    </td>
                                    <td width="50%">
                                        <select class="on-field" method="post" name="area">
                                            <option value="null">All Areas</option>
                                            <?php
                                                if (is_array($areas))
                                                    foreach ($areas as $area)
                                                        if ($_POST["area"] == $area[0])
                                                            echo "<option value='".$area[0]."' selected>".$area[1]."</option>";
                                                        else
                                                            echo "<option value='".$area[0]."'>".$area[1]."</option>";
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        
                        <div id="activityTypeDiv" style="display: none;">
                            <table width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Activity Type:
                                    </td>
                                    <td width="50%">
                                        <select class="on-field" method="post" name="activityType" onChange="getData(activityType);">
                                            <option value="null">All Activity Types</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        
                        <div id="activityDiv" style="display: none;">
                            <table width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Activity:
                                    </td>
                                    <td width="50%">
                                        <select class="on-field" method="post" name="activity">
                                            <option value="null">All Activities</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Date Type(s):
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="dateType" onChange="dateSelection();">
                                        <option value="currentwk">Current Week</option>
                                        <option value="previouswk">Previous Week</option>
                                        <option value="currentmnth">Current Month</option>
                                        <option value="previousmnth">Previous Month</option>
                                        <option value="custom">Custom Dates</option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                        <div id="datesDiv" style="display: none;">
                            <table width="100%">
                                <tr>
                                    <td class="on-description" width="50%">
                                        Date From:
                                    </td>
                                    <td width="50%">
                                        <input class="on-field-date" id="date_from" name="date_from" type="text" style="text-align:right;" value="<?php
                                            if ($_POST["date_from"] != "") echo "".$_POST["date_from"]; else echo "".getMonthStart($today); ?>">
                                        <div id="dateFrom" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                        <div id="dateFromDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="on-description" width="50%">
                                        Date To:
                                    </td>
                                    <td width="50%">
                                        <input class="on-field-date" id="date_to" name="date_to" type="text" style="text-align:right;" value="<?php
                                            if ($_POST["date_to"] != "") echo "".$_POST["date_to"]; else echo "".getMonthEnd($today); ?>">
                                        <div id="dateTo" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                        <div id="dateToDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <br/>
                        <input name="btnGenerate" onClick="check();" type="button" value="Generate Report">
                        <input method="post" name="save" type="hidden" value="0" />
                    </div>
                </form>
                <div id="report" style="<?php if ($generated == "1") echo "display: block;"; else echo "display: none"; ?>">
                    <?php
                        echo "<div class='on-20px'><table class='on-table-center on-table'>";
                            echo "".$displayString;
                            echo  "<tfoot><tr><td colspan='".($columns+1+$hasAreas)."'></td></tr></tfoot>";
                        echo "</table></div>";
                        echo "<br/>";

                        ///////////////////////////
                        //  Set Export Information
                        $_SESSION["fileName"] = "Time Sheet Report";
                        $_SESSION["fileData"] = array_merge($excelheadings, $exceldata);
                        ///////////////////////////

                        echo "<input name='btnExport' onClick=\"location.href='_export.php'\" type='button' value='Export to Excel' />";
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
