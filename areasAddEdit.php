<?php
    //TODO:  check if record exists
    //TODO:  check if update necessary
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    if (!hasAccess("AREA_MANAGE"))
        header("Location: noaccess.php");

    function getAreaInfo($id) {
        unset($info);

        if ($id != "")  {
            $info = q("SELECT id,pID,name,active FROM areas WHERE pID = '".$id."' ORDER BY name");
        }
        else    {
            $cntr = (isset($_POST["areasTblCntr"])) ? addslashes(strip_tags($_POST["areasTblCntr"])) : 1;

            if ($cntr > 1)  {
                $r = 0;

                for ($i = 1; $i < $cntr; $i++)  {
                    if (addslashes(strip_tags($_POST["name".$i])) != "")   {
                        $info[$r][0] = (isset($_POST["id".$i])) ? addslashes(strip_tags($_POST["id".$i])) : "";
                        $info[$r][1] = (isset($_POST["project".$i])) ? addslashes(strip_tags($_POST["project".$i])) : addslashes(strip_tags($_POST["id"]));
                        $info[$r][2] = (isset($_POST["name".$i])) ? addslashes(strip_tags($_POST["name".$i])) : "";
                        $info[$r][3] = (isset($_POST["active".$i])) ? addslashes(strip_tags($_POST["active".$i])) : "";

                        $r++;
                    }
                }
            }
        }

        return $info;
    }

    if (isset($_POST["save"]) && $_POST["save"] === "1")    {
        $errorMessage = "";

        $areaInfo = getAreaInfo("");
        $date = date("Y-m-d H:i:s");
        $user = $_SESSION["email"];
        $companyID = $_SESSION["company_id"];

        foreach ($areaInfo as $area)    {
            if ($area[0] == "")  {               //  DO INSERT
                $query = "INSERT INTO areas (pID,name,dateCreated,createdBy,dateUpdated,updatedBy) VALUES ";
                $values = "('".$area[1]."','".$area[2]."','".$date."','".$user."','".$date."','".$user."')";

                $query .= $values;
            }
            else if (is_numeric($area[0]))    {  //  DO UPDATE
                $active = ($area[3] == "") ? 0 : 1;
                $query = "UPDATE areas SET pID = '".$area[1]."',name = '".$area[2]."',active = '".$active."',".
                            "dateUpdated = '".$date."',updatedBy = '".$user."' WHERE id = '".$area[0]."'";
            }

            $runQuery = q($query);

            $sp_project = q("SELECT name FROM Project WHERE id = '".$area[1]."'");
            $what = (substr($query, 0, 6) == "INSERT") ? $area[2]." created for project - ".$sp_project : $area[2]."updated for project - ".$sp_project;
            $access = (substr($query, 0, 6) == "INSERT") ? "Insert" : "Update";

            $logs = q("INSERT INTO Logs (what,access,on_table,by_user,on_date,on_time,company_id) VALUES ".
                        "('".$what."','".$access."','areas','".$user."','".date("Y-m-d")."','".date("H:i:s")."','".$companyID."')");
        }

        header("Location: areas.php?project=".$area[1]);
    }
    else
        $areaInfo = getAreaInfo($_POST["id"]);

    $index = 1;

    if ($errorMessage != "")
        echo "<p align='center' style='padding:0px;'><strong><font color='#999999'>$errorMessage</font></strong></p>";

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0");
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    jQuery(function()    {
        jQuery("#addRow").click(function()    {
            var tbl = "#"+jQuery(this).attr("href");
            var tblCntr = "#"+jQuery(this).attr("href")+"Cntr";

            var lstRow = jQuery(tbl + " tbody>tr:last");
            var counter = jQuery(tblCntr).val();
            var n = lstRow.clone(true);

            n.children().eq(0).children().eq(0).attr("id", "id"+counter);
            n.children().eq(0).children().eq(0).attr("name", "id"+counter);
            n.children().eq(0).children().eq(0).attr("value", "");
            n.children().eq(0).children().eq(1).attr("id", "project"+counter);
            n.children().eq(0).children().eq(1).attr("name", "project"+counter);
            n.children().eq(0).children().eq(2).attr("id", "name"+counter);
            n.children().eq(0).children().eq(2).attr("name", "name"+counter);
            n.children().eq(0).children().eq(2).attr("value", "");
            n.children().eq(0).children().eq(3).attr("id", "nameDiv"+counter);
            n.children().eq(0).children().eq(3).attr("name", "nameDiv"+counter);
            n.children().eq(0).children().eq(3).hide();

            n.insertAfter(lstRow);
            counter++;

            jQuery(tblCntr).val(counter);
        });

        jQuery("#btnAddEdit").click(function()    {
            var valid = true;

            var areas = jQuery("#areasTblCntr").val();

            for (var i = 1; i < areas; i++) {
                if (jQuery("#name"+i).val() != "")   {
                    test = validation("number", jQuery("#name"+i).val());

                    if (test)       jQuery("#nameDiv"+i).show();
                    else            jQuery("#nameDiv"+i).hide();

                    valid &= !test;
                }
            }

            if (valid) {
                jQuery("#save").val("1");
                jQuery("#content").submit();
            }
        });
    });
</script>
<table width="100%">
    <tr height="380px">
        <td class="centerdata" valign="top">
            <form action="" method="post" id="content" name="content">
                <table width="100%">
                    <tr>
                        <td class="centerdata">
                            <h6>
                                <?php echo (is_array($areaInfo)) ? "Add" : "Update"; ?> Project Area(s)
                            </h6>
                        </td>
                    </tr>
                    <tr>
                        <td class="centerdata">
                            <br/>
                        </td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td class="on-description-center" class="centerdata" width="100%">
                            <?php
                            if (is_array($areaInfo)) {
                                echo  "Area(s)";
                           }
                           ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="centerdata" width="100%">
                            <table width="100%" id="areasTbl" name="areasTbl" >
                            <?php
                                $counter = 1;

                                if (is_array($areaInfo)) {
                                    foreach ($areaInfo as $area) {
                                        echo "<tr style='width:100%'><td style='padding-left:70px' class='centerdata'>";
                                            echo "<input id='id".$counter."' name='id".$counter."' type='hidden' value='".stripslashes($area[0])."' />";
                                            echo "<input id='project".$counter."' name='project".$counter."' type='hidden' value='".stripslashes($area[1])."' />";
                                            echo "<input class='on-field' id='name".$counter."' name='name".$counter."' type='text' tabindex='".$index."' value='".stripslashes($area[2])."' />";

                                            $checked = ($area[3] == "1") ? "checked" : "";

                                            echo "<input id='active".$counter."' name='active".$counter."' type='checkbox' tabindex='".$index++."' value='1' $checked />&nbsp;&nbsp;<i class='on-description-left'>Active</i>";
                                            echo "<div id='nameDiv".$counter."' name='nameDiv".$counter."' class='error'><font class='on-validate-error'>* Area name must be alpha-numeric, eg. Area 145</font></div>";
                                        echo "</td></tr>";
                                        $counter++;
                                    }

                                    
                                }

                                echo "<tr><td class='on-description centerdata'>Add a new Area</td></tr><tr style='width:100%'><td class='centerdata'>";
                                    echo "<input id='id".$counter."' name='id".$counter."' type='hidden' value='' />";
                                    echo "<input id='project".$counter."' name='project".$counter."' type='hidden' value='".addslashes(strip_tags($_POST["id"]))."' />";
                                    echo "<input class='on-field' id='name".$counter."' name='name".$counter."' type='text' tabindex='".$index++."' value='' />";
                                    echo "<div id='nameDiv".$counter."' name='nameDiv".$counter."' class='error'><font class='on-validate-error'>* Area name must be alpha-numeric, eg. Area 145</font></div>";
                                echo "</td></tr>";
                                $counter++;
                            ?>
                            </table>
                            <input id="addRow" name="addRow" href="areasTbl" type="button" value="+" tabindex="<?php echo $index++; ?>" />
                        </td>
                    </tr>
                </table>
                <br/>
                <input id="btnAddEdit" name="btnAddEdit" tabindex="<?php echo $index++; ?>" type="button" value="Add/Update" />
                <input id="save" name="save" type="hidden" value="0" />
                <input id="areasTblCntr" name="areasTblCntr" type="hidden" value="<?php echo $counter; ?>" />
            </form>
        </td>
    </tr>
    <tr>
        <td class="centerdata">
            <br/>
        </td>
    </tr>
</table>
<?php
    //  Print Footer
    print_footer();
?>
