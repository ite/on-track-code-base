<?php
    session_start();

    include("_db.php");
    include("_functions.php");
    include("_notification.php");
    include("graphics.php");

    $from = "On Track Notification <support@integrityengineering.co.za>";
    $reply_to = $from;
    $subject = "On Track - Project(s) Over Budget";

    $notify_id = q("SELECT id FROM Notify WHERE code = 'OverBudget'");
    $companies = q("SELECT id, name FROM Company WHERE locked = '0'");

    if (is_array($companies)) {
        foreach ($companies as $company) {
        $company_id = $company[0];

        $projects = q("SELECT DISTINCT(p.id), p.name, p.due_date FROM (Project AS p INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) ".
                        "WHERE p.completed = '0' AND p.deleted = '0' AND cot.status = '1' AND cot.deleted = '0' AND cot.company_id = '".$company_id."' ORDER BY UPPER(p.name)");

        $projectList = "";

        if (is_array($projects))    {
            foreach ($projects as $p)       {
                $totalBudget = q("SELECT total_budget FROM companiesOnTeam WHERE project_id = '".$p[0]."' AND company_id = '".$company_id."'");

                if (is_numeric($totalBudget) && $totalBudget != "") {
                    $sumTime = q("SELECT SUM(rate * time_spent) FROM TimeSheet WHERE project_id = '".$p[0]."' AND company_id = '".$company_id."'");
                    $sumExpense = q("SELECT SUM(expense) FROM ExpenseSheet WHERE project_id = '".$p[0]."' AND company_id = '".$company_id."'");

                    if (number_format($totalBudget, 2, ".", "") <= number_format(($sumTime + $sumExpense), 2, ".", ""))     {
                        //$update = q("UPDATE companiesOnTeam SET overBudget = '1' WHERE project_id = '".$p[0]."' AND company_id = '".$company_id."'");

                        $projectList .= $p[1]."<br/>\n";
                    }
                }
            }
        }

        if ($projectList != "") {
            $message = print_email_header($subject)."The following projects have gone over there budget:<br/>\n".$projectList."<br/>\n".
                        "Regards,<br/>".
                        "On Track Notification<br/>".print_email_footer();

            $to = "Mauritz Kruger <mk@iteonline.co.za>";

            if ($to != "")
                sendmail($to, $subject, $message, $from, $reply_to, true);
        }
        }
    }
?>
