<?php
    session_start();

    include("_db.php");
    include("graphics.php");
	include_once("include/PHPExcel.php");

	global $file_directory;

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    if (!hasAccess("PAYMENT_CERTIFICATE"))
        header("Location: noaccess.php");

	$redirect = "false";

	if (isset($_POST["update"]) && $_POST["update"] === "1")        {
		unset($ids);

		$projectID = addslashes(strip_tags($_POST["project"]));
		$projectName = q("SELECT name FROM Project WHERE id = '".$projectID."'");
		$projectNumber = q("SELECT number FROM Project WHERE id = '".$projectID."'");
		$projectDescr = q("SELECT descr FROM Project WHERE id = '".$projectID."'");
		$areaID = addslashes(strip_tags($_POST["area"]));

		$invoiceName = addslashes(strip_tags($_POST["invoiceName"]));
		$claimNumber = addslashes(strip_tags($_POST["claimNumber"]));
		$dateCreated = addslashes(strip_tags($_POST["dateCreated"]));
		$dateDue = addslashes(strip_tags($_POST["dateDue"]));

		$types = array("Time","Expenses");

		$consulting = 0;
		$expenses = 0;

		if (is_array($types))       {
			foreach ($types as $type)       {
				$table = ($type == "Time") ? "ApprovedTime" : "ApprovedExpense";
				$fields = ($type == "Time") ? array("A","B") : array("C","D");

				$count = addslashes(strip_tags($_POST[$type]));

				for ($i = 1; $i < $count; $i++)     {
					$pos = 0;

					$id = addslashes(strip_tags($_POST[$fields[$pos++].$i]));
					$invoice = addslashes(strip_tags($_POST[$fields[$pos++].$i]));

					if ($invoice == "1")	{
						$ids[$type][] = $id;

						if ($type == "Time")	{
							$info = q("SELECT date,'','',time_spent,rate,user_id,descr,travelTime FROM ".$table." WHERE id = '".$id."'");

							if ($info[0][7] == "1")
								$travelTime[] = array(
														$info[0][0],
														$info[0][1],
														$info[0][2],
														number_format((double)$info[0][3],2,".",""),
														number_format((double)$info[0][4],2,".",""),
														number_format((double)($info[0][3] * $info[0][4]),2,".",""),
														q("SELECT CONCAT(lstname, ', ', frstname) FROM Employee WHERE id = '".$info[0][5]."'"),
														$info[0][6]
													);
							else
								$timeBased[] = array(
														$info[0][0],
														$info[0][1],
														$info[0][2],
														number_format((double)$info[0][3],2,".",""),
														number_format((double)$info[0][4],2,".",""),
														number_format((double)($info[0][3] * $info[0][4]),2,".",""),
														q("SELECT CONCAT(lstname, ', ', frstname) FROM Employee WHERE id = '".$info[0][5]."'"),
														$info[0][6]
													);

							$consulting += number_format((double)($info[0][3] * $info[0][4]),2,".","");
						}
						else	{
							$expenseType = q("SELECT name FROM dropdowns WHERE id = (SELECT expense_type_id FROM ApprovedExpense WHERE id = '".$id."')");

							switch ($expenseType)	{
								case "Driving":	{
									$info = q("SELECT date,placeFrom,placeTo,vehicle_id,'',businessKM,rate,expense,user_id,descr FROM ".$table." WHERE id = '".$id."'");

									$motorVehicle[] = array(
															$info[0][0],
															$info[0][1],
															$info[0][2],
															q("SELECT type FROM Vehicle WHERE id = '".$info[0][3]."'"),
															$info[0][4],
															number_format((double)$info[0][5],2,".",""),
															((number_format((double)$info[0][6],2,".","") * 100) / 115),
															((number_format((double)$info[0][7],2,".","") * 100) / 115),
															q("SELECT CONCAT(lstname, ', ', frstname) FROM Employee WHERE id = '".$info[0][8]."'"),
															$info[0][9]
														);
									$expenses += ((number_format((double)$info[0][7],2,".","") * 100) / 115);
									break;
								}
								case "Car Hire":	{
									$info = q("SELECT date,placeFrom,placeTo,otherDescr,rate,expense,user_id,descr FROM ".$table." WHERE id = '".$id."'");

									$carHire[] = array(
															$info[0][0],
															$info[0][1],
															$info[0][2],
															$info[0][3],
															((number_format((double)$info[0][4],2,".","") * 100) / 115),
															((number_format((double)$info[0][5],2,".","") * 100) / 115),
															q("SELECT CONCAT(lstname, ', ', frstname) FROM Employee WHERE id = '".$info[0][6]."'"),
															$info[0][7]
														);
									$expenses += ((number_format((double)$info[0][5],2,".","") * 100) / 115);
									break;
								}
								case "Flights":	{
									$info = q("SELECT date,placeFrom,placeTo,otherDescr,rate,expense,user_id,descr FROM ".$table." WHERE id = '".$id."'");

									$flights[] = array(
														$info[0][0],
														$info[0][1],
														$info[0][2],
														$info[0][3],
														((number_format((double)$info[0][4],2,".","") * 100) / 115),
														((number_format((double)$info[0][5],2,".","") * 100) / 115),
														q("SELECT CONCAT(lstname, ', ', frstname) FROM Employee WHERE id = '".$info[0][6]."'"),
														$info[0][7]);
									$expenses += ((number_format((double)$info[0][5],2,".","") * 100) / 115);
									break;
								}
								case "Accommodation":	{
									$info = q("SELECT date,'',total,otherDescr,rate,expense,user_id,descr FROM ".$table." WHERE id = '".$id."'");

									$accommodation[] = array(
															$info[0][0],
															getDates($info[0][0], $info[0][2]),
															$info[0][3],
															((number_format((double)$info[0][4],2,".","") * 100) / 115),
															((number_format((double)$info[0][5],2,".","") * 100) / 115),
															q("SELECT CONCAT(lstname, ', ', frstname) FROM Employee WHERE id = '".$info[0][6]."'"),
															$info[0][7]
														);
									$expenses += ((number_format((double)$info[0][5],2,".","") * 100) / 115);
									break;
								}
								case "Expense":	{
									$info = q("SELECT date,'','1',expense,descr FROM ".$table." WHERE id = '".$id."'");

									$other[] = array(
															$info[0][0],
															$info[0][1],
															$info[0][2],
															((number_format((double)$info[0][3],2,".","") * 100) / 115),
															$info[0][4]
														);
									$expenses += ((number_format((double)$info[0][3],2,".","") * 100) / 115);
									break;
								}
								case "Toll Gate":	{
									$info = q("SELECT date,placeFrom,placeTo,expense,'',user_id,descr FROM ".$table." WHERE id = '".$id."'");

									$tollGate[] = array(
														$info[0][0],
														$info[0][1],
														$info[0][2],
														((number_format((double)$info[0][3],2,".","") * 100) / 115),
														$info[0][4],
														q("SELECT CONCAT(lstname, ', ', frstname) FROM Employee WHERE id = '".$info[0][5]."'"),
														$info[0][6]
													);
									$expenses += ((number_format((double)$info[0][3],2,".","") * 100) / 115);
									break;
								}
								case "Printing":	{
									$info = q("SELECT date,disbursement_id,total,rate,expense,'',descr FROM ".$table." WHERE id = '".$id."'");

									$printsAndCopies[] = array(
															$info[0][0],
															q("SELECT type FROM DisbursementTypes WHERE id = (SELECT parent_id FROM Disbursements WHERE id = '".$info[0][1]."')")." - ".
																q("SELECT name FROM Disbursements WHERE id = '".$info[0][1]."'"),
															number_format((double)$info[0][2],0,".",""),
															((number_format((double)$info[0][3],2,".","") * 100) / 115),
															((number_format((double)$info[0][4],2,".","") * 100) / 115),
															$info[0][5],
															$info[0][6]
														);
									$expenses += ((number_format((double)$info[0][4],2,".","") * 100) / 115);
									break;
								}
								default:	{
									break;
								}
							}
						}
					}
				}
			}

			//  Create Invoice
			$insert = q("INSERT INTO LoadInvoices (project_id, create_date, name, claimNumber, consulting, expenses, diverse_income, vat, due_date, paid, company_id, area_id) VALUES ".
						"('".$projectID."', '".$dateCreated."','".$invoiceName."','".$claimNumber."', '".number_format((double)$consulting, 2, ".", "")."', '".number_format((double)$expenses, 2, ".", "")."', ".
								"'".number_format((double)0, 2, ".", "")."', '15', '".$dateDue."', '0', '".$_SESSION["company_id"]."', '".$areaID."')");

			$invoiceID = q("SELECT id FROM LoadInvoices WHERE name = '".$invoiceName."' AND project_id = '".$projectID."' ORDER BY id DESC LIMIT 1");

			if ($invoiceID != "" && number_format((double)$expenses, 2, ".", "") > 0)	{
				$expenseDueDate = getDates($dateDue, 7);

				$insert = q("INSERT INTO ExpenseBudget (project_id, descr, amount, vat, due_date, invoice_id, company_id, paid) VALUES ".
							"('".$projectID."','".$invoiceName."','".number_format((double)$expenses, 2, ".", "")."','15','".$expenseDueDate."','".$invoiceID."','".$_SESSION["company_id"]."','0')");
			}

			if ($invoiceID != "")	{
				if (is_array($ids["Time"]))		$update = q("UPDATE ApprovedTime SET invoiceID = '".$invoiceID."', invoiced = '1' WHERE id IN ('".implode("','",$ids["Time"])."')");
				if (is_array($ids["Expenses"]))	$update = q("UPDATE ApprovedExpense SET invoiceID = '".$invoiceID."', invoiced = '1' WHERE id IN ('".implode("','",$ids["Expenses"])."')");
			}

			$tabs = array("Time Based","Travel Time","Motor Vehicle","Car Hire","Flights","Accommodation","Other","Toll Gate","Prints & Copies");

			$objPHPExcel = new PHPExcel();

			$objReader = new PHPExcel_Reader_Excel2007();

			//$objPHPExcel = $objReader->load("templates/Template - Invoice And Payment Certificate.xlsx");
			$objPHPExcel = $objReader->load(preg_replace("/ /","_","templates/Template - Invoice And Payment Certificate.xlsx"));

			$sheets = $objPHPExcel->getSheetNames();

			foreach ($tabs as $tab)	{
				$objPHPExcel->setActiveSheetIndex(array_search($tab, $sheets));

				switch ($tab)	{
					case "Time Based":	{
						if (is_array($timeBased))	{
							$row = 13;

							foreach ($timeBased as $r)	{
								$objPHPExcel->getActiveSheet()->setCellValue("B".$row, $r[0]);
								$objPHPExcel->getActiveSheet()->setCellValue("C".$row, $r[1]);
								$objPHPExcel->getActiveSheet()->setCellValue("D".$row, $r[2]);
								$objPHPExcel->getActiveSheet()->setCellValue("E".$row, $r[3]);
								$objPHPExcel->getActiveSheet()->setCellValue("F".$row, $r[4]);
								$objPHPExcel->getActiveSheet()->setCellValue("G".$row, $r[5]);
								$objPHPExcel->getActiveSheet()->setCellValue("H".$row, $r[6]);
								$objPHPExcel->getActiveSheet()->setCellValue("I".$row, $r[7]);

								$row++;
							}
						}

						break;
					}
					case "Travel Time":	{
						if (is_array($travelTime))	{
							$row = 13;

							foreach ($travelTime as $r)	{
								$objPHPExcel->getActiveSheet()->setCellValue("B".$row, $r[0]);
								$objPHPExcel->getActiveSheet()->setCellValue("C".$row, $r[1]);
								$objPHPExcel->getActiveSheet()->setCellValue("D".$row, $r[2]);
								$objPHPExcel->getActiveSheet()->setCellValue("E".$row, $r[3]);
								$objPHPExcel->getActiveSheet()->setCellValue("F".$row, $r[4]);
								$objPHPExcel->getActiveSheet()->setCellValue("G".$row, $r[5]);
								$objPHPExcel->getActiveSheet()->setCellValue("H".$row, $r[6]);
								$objPHPExcel->getActiveSheet()->setCellValue("I".$row, $r[7]);

								$row++;
							}
						}

						break;
					}
					case "Motor Vehicle":	{
						if (is_array($motorVehicle))	{
							$row = 12;

							foreach ($motorVehicle as $r)	{
								$objPHPExcel->getActiveSheet()->setCellValue("B".$row, $r[0]);
								$objPHPExcel->getActiveSheet()->setCellValue("D".$row, $r[1]);
								$objPHPExcel->getActiveSheet()->setCellValue("E".$row, $r[2]);
								$objPHPExcel->getActiveSheet()->setCellValue("F".$row, $r[3]);
								$objPHPExcel->getActiveSheet()->setCellValue("G".$row, $r[4]);
								$objPHPExcel->getActiveSheet()->setCellValue("H".$row, $r[5]);
								$objPHPExcel->getActiveSheet()->setCellValue("I".$row, $r[6]);
								$objPHPExcel->getActiveSheet()->setCellValue("J".$row, $r[7]);
								$objPHPExcel->getActiveSheet()->setCellValue("K".$row, $r[8]);

								$row++;
							}
						}

						break;
					}
					case "Car Hire":	{
						if (is_array($carHire))	{
							$row = 8;

							foreach ($carHire as $r)	{
								$objPHPExcel->getActiveSheet()->setCellValue("B".$row, $r[0]);
								$objPHPExcel->getActiveSheet()->setCellValue("C".$row, $r[1]);
								$objPHPExcel->getActiveSheet()->setCellValue("D".$row, $r[2]);
								$objPHPExcel->getActiveSheet()->setCellValue("E".$row, $r[3]);
								$objPHPExcel->getActiveSheet()->setCellValue("F".$row, $r[4]);
								$objPHPExcel->getActiveSheet()->setCellValue("G".$row, $r[5]);
								$objPHPExcel->getActiveSheet()->setCellValue("H".$row, $r[6]);
								$objPHPExcel->getActiveSheet()->setCellValue("I".$row, $r[7]);

								$row++;
							}
						}

						break;
					}
					case "Flights":	{
						if (is_array($flights))	{
							$row = 8;

							foreach ($flights as $r)	{
								$objPHPExcel->getActiveSheet()->setCellValue("B".$row, $r[0]);
								$objPHPExcel->getActiveSheet()->setCellValue("C".$row, $r[1]);
								$objPHPExcel->getActiveSheet()->setCellValue("D".$row, $r[2]);
								$objPHPExcel->getActiveSheet()->setCellValue("E".$row, $r[3]);
								$objPHPExcel->getActiveSheet()->setCellValue("F".$row, $r[4]);
								$objPHPExcel->getActiveSheet()->setCellValue("G".$row, $r[5]);
								$objPHPExcel->getActiveSheet()->setCellValue("H".$row, $r[6]);
								$objPHPExcel->getActiveSheet()->setCellValue("I".$row, $r[7]);

								$row++;
							}
						}

						break;
					}
					case "Accommodation":	{
						if (is_array($accommodation))	{
							$row = 8;

							foreach ($accommodation as $r)	{
								$objPHPExcel->getActiveSheet()->setCellValue("B".$row, $r[0]);
								$objPHPExcel->getActiveSheet()->setCellValue("C".$row, $r[1]);
								$objPHPExcel->getActiveSheet()->setCellValue("D".$row, $r[2]);
								$objPHPExcel->getActiveSheet()->setCellValue("E".$row, $r[3]);
								$objPHPExcel->getActiveSheet()->setCellValue("F".$row, $r[4]);
								$objPHPExcel->getActiveSheet()->setCellValue("G".$row, $r[5]);
								$objPHPExcel->getActiveSheet()->setCellValue("H".$row, $r[6]);

								$row++;
							}
						}

						break;
					}
					case "Other":	{
						if (is_array($other))	{
							$row = 8;

							foreach ($other as $r)	{
								$objPHPExcel->getActiveSheet()->setCellValue("B".$row, $r[0]);
								$objPHPExcel->getActiveSheet()->setCellValue("C".$row, $r[1]);
								$objPHPExcel->getActiveSheet()->setCellValue("D".$row, $r[2]);
								$objPHPExcel->getActiveSheet()->setCellValue("E".$row, $r[3]);
								$objPHPExcel->getActiveSheet()->setCellValue("F".$row, $r[4]);

								$row++;
							}
						}

						break;
					}
					case "Toll Gate":	{
						if (is_array($tollGate))	{
							$row = 8;

							foreach ($tollGate as $r)	{
								$objPHPExcel->getActiveSheet()->setCellValue("B".$row, $r[0]);
								$objPHPExcel->getActiveSheet()->setCellValue("C".$row, $r[1]);
								$objPHPExcel->getActiveSheet()->setCellValue("D".$row, $r[2]);
								$objPHPExcel->getActiveSheet()->setCellValue("E".$row, $r[3]);
								$objPHPExcel->getActiveSheet()->setCellValue("F".$row, $r[4]);
								$objPHPExcel->getActiveSheet()->setCellValue("G".$row, $r[5]);
								$objPHPExcel->getActiveSheet()->setCellValue("H".$row, $r[6]);

								$row++;
							}
						}

						break;
					}
					case "Prints & Copies":	{
						if (is_array($printsAndCopies))	{
							$row = 10;

							foreach ($printsAndCopies as $r)	{
								$objPHPExcel->getActiveSheet()->setCellValue("B".$row, $r[0]);
								$objPHPExcel->getActiveSheet()->setCellValue("C".$row, $r[1]);
								$objPHPExcel->getActiveSheet()->setCellValue("D".$row, $r[2]);
								$objPHPExcel->getActiveSheet()->setCellValue("E".$row, $r[3]);
								$objPHPExcel->getActiveSheet()->setCellValue("F".$row, $r[4]);
								$objPHPExcel->getActiveSheet()->setCellValue("G".$row, $r[5]);
								$objPHPExcel->getActiveSheet()->setCellValue("I".$row, $r[6]);

								$row++;
							}
						}

						break;
					}
					default:
						break;
				}
			}

			foreach ($tabs as $tab)	{
				$cells = "";
				switch ($tab)	{
					case "Time Based":	{
						$cells = array("D5","D6","I6");
						break;
					}
					case "Travel Time":
					case "Motor Vehicle":	{
						$cells = array("D5","D6","H6");
						break;
					}
					case "Car Hire":
					case "Flights":
					case "Prints & Copies":	{
						$cells = array("C5","C6","G6");
						break;
					}
					case "Accommodation":
					case "Toll Gate":	{
						$cells = array("D5","D6","G6");
						break;
					}
					case "Other":	{
						$cells = array("C5","C6","F6");
						break;
					}
					default;
						break;
				}

				if (is_array($cells))	{
					$objPHPExcel->setActiveSheetIndex(array_search($tab, $sheets));

					foreach ($cells as $cell)	{
						$objPHPExcel->getActiveSheet()->getStyle($cell)->getFill()->getStartColor()->setARGB("FF95B3D7");
					}
				}
			}

			$objPHPExcel->setActiveSheetIndex(array_search("Payment Certificate", $sheets));
			$objPHPExcel->getActiveSheet()->setCellValue("J10", $projectNumber);
			$objPHPExcel->getActiveSheet()->setCellValue("J17", $projectNumber);
			$objPHPExcel->getActiveSheet()->setCellValue("J11", $claimNumber);
			$objPHPExcel->getActiveSheet()->setCellValue("J12", $projectName);
			$objPHPExcel->getActiveSheet()->setCellValue("J13", $projectDescr);
			$objPHPExcel->getActiveSheet()->setCellValue("J67", $dateCreated);
			$objPHPExcel->getActiveSheet()->setCellValue("J75", $dateCreated);
			$objPHPExcel->getActiveSheet()->setCellValue("J81", $dateCreated);
			$objPHPExcel->getActiveSheet()->getStyle("J10:J18")->getFill()->getStartColor()->setARGB("FF95B3D7");
			$objPHPExcel->getActiveSheet()->getStyle("J19")->getFont()->getColor()->setARGB("FF990000");

			$objPHPExcel->setActiveSheetIndex(array_search("Tax Invoice", $sheets));
			$objPHPExcel->getActiveSheet()->setCellValue("C10", $invoiceName);
			$objPHPExcel->getActiveSheet()->setCellValue("C13", $dateCreated);
			$objPHPExcel->getActiveSheet()->getStyle("C11")->getFill()->getStartColor()->setARGB("FFBFBFBF");
			$objPHPExcel->getActiveSheet()->getStyle("B19")->getFill()->getStartColor()->setARGB("FFD99795");
			$objPHPExcel->getActiveSheet()->getStyle("C22")->getFill()->getStartColor()->setARGB("FFD99795");

			// Delete File If Exists On Server
			if (file_exists(preg_replace("/ /","_",$file_directory."certificates/Payment Certificate - ".$invoiceID.".xlsx")))
				unlink(preg_replace("/ /","_",$file_directory."certificates/Payment Certificate - ".$invoiceID.".xlsx"));
			//if (file_exists($file_directory."certificates/Payment Certificate - ".$invoiceID.".pdf"))	unlink($file_directory."certificates/Payment Certificate - ".$invoiceID.".pdf");

			// Save Excel File
			$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
			$objWriter->save(preg_replace("/ /","_",$file_directory."certificates/Payment Certificate - ".$invoiceID.".xlsx"));

			// Save PDF File
			//$objReader = new PHPExcel_Reader_Excel2007();
			//$objPHPExcel = $objReader->load($file_directory."certificates/Payment Certificate - ".$invoiceID.".xlsx");
			//$objWriter = new PHPExcel_Writer_PDF($objPHPExcel);
			//$objWriter->writeAllSheets();
			//$objWriter->save($file_directory."certificates/Payment Certificate - ".$invoiceID.".pdf");

			//header("Location: invoice_load.php");
			$redirect = "true";
		}
	}

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("2", "budget");
?>
	<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
	<script type="text/javascript" src="include/datepicker.js"></script>
	<script language="JavaScript" src="include/validation.js"></script>
	<script language="JavaScript">
		var loadDates;
		var loadAreas;

		function is_array(input)    {
			return typeof(input)=='object'&&(input instanceof Array);
		}

		jQuery(function()        {
		<?php
			$dateFields = array("dateFrom","dateTo","dateCreated","dateDue");

			foreach ($dateFields as $df)    jQDate($df);

		?>

			if ("<?php echo $redirect; ?>" == "true")	{
				jQuery("#invoiceID").val("<?php echo $invoiceID; ?>");
				jQuery("#approvals").attr("action","bookings.php").submit();
			}

			jQuery("#dateType").change(function()        {
				if (jQuery(this).val() == "custom") jQuery("#datesDiv").show();
				else                                jQuery("#datesDiv").hide();

				loadDates();
			});

			loadDates = function()	{
				var project = jQuery("#project").val();
				var dateType = jQuery("#dateType").val();

				jQuery.post("_ajax.php", {func: "getDates", project: project, dateType: dateType}, function(data)	{
					data = json_decode(data);

					if (is_array(data))	{
						jQuery("#dateFrom").val(data[0]);
						jQuery("#dateTo").val(data[1]);
					}
				});
			}

			loadAreas = function(projectID)	{
				jQuery.post("_ajax.php", {func: "get_area_info", projectID: projectID}, function(data)	{
					data = json_decode(data);

					jQuery("#area option").remove();

					jQuery("#area").append(jQuery("<option></option>").attr("value","0").text("All Areas"));

					if (is_array(data)) {   // Display area options
						jQuery.each(data,function(i, v)	{
							jQuery("#area").append(jQuery("<option></option>").attr("value",v[0]).text(v[1]));
						});

						jQuery("#area").parent().parent().show();
					}
					else    {               // Do not display area options
						jQuery("#area").parent().parent().hide();
					}
				});
			}

			jQuery("#project").change(function()    {
				if (jQuery(this).val() != "null")   {
					jQuery("#dateType option[value='projectLifetime']").removeAttr("disabled");
					jQuery("#dateType option").removeAttr("selected");
					jQuery("#dateType option[value='projectLifetime']").attr("selected", "selected");
				} else      {
					jQuery("#dateType option[value='projectLifetime']").attr("disabled","disabled");
					jQuery("#dateType option[value='currentwk']").attr("selected", "selected");
				}

				loadDates();
				loadAreas(jQuery(this).val());
			});

			jQuery("#btnDisplay").click(function()	{
				var valid = true;
				var test;
				var project = jQuery("#project").val();
				var area = jQuery("#area").val();
				var dateType = jQuery("#dateType").val();

				if (project == "null")	{
					valid &= false;
					jQuery("#projectDiv").show();
				}
				else
					jQuery("#projectDiv").hide();

				if (dateType == "custom")	{
					test = validation("date", jQuery("#dateFrom").val());

					if (!test)  jQuery("#dateFromDiv").show();
					else        jQuery("#dateFromDiv").hide();

					valid &= test;

					test = validation("date", jQuery("#dateTo").val());

					if (!test)  jQuery("#dateToDiv").show();
					else        jQuery("#dateToDiv").hide();

					valid &= test;
				}

				if (valid)	{
					jQuery("#display").val("1");
					jQuery("#approvals").submit();
				}
			});

			jQuery("#btnUpdate").click(function()	{
				var valid = true;
				var test;

				var invoiceName = jQuery("#invoiceName").val();
				var claimNumber = jQuery("#claimNumber").val();

				if (invoiceName == "")	{
					valid &= false;
					jQuery("#invoiceNameDiv").show();
				}
				else
					jQuery("#invoiceNameDiv").hide();

				if (claimNumber == "")	{
					valid &= false;
					jQuery("#claimNumberDiv").show();
				}
				else
					jQuery("#claimNumberDiv").hide();

				test = validation("date", jQuery("#dateCreated").val());

				if (!test)  jQuery("#dateCreatedDiv").show();
				else        jQuery("#dateCreatedDiv").hide();

				valid &= test;

				test = validation("date", jQuery("#dateDue").val());

				if (!test)  jQuery("#dateDueDiv").show();
				else        jQuery("#dateDueDiv").hide();

				valid &= test;

				if (valid)	{
					jQuery("#update").val("1");
					jQuery("#approvals").submit();
				}
			});
		});
	</script>
<?php
	$today = date("Y-m-d");

	$project = "null";
	$dateType = "currentmnth";

	if (isset($_POST["project"]) && $_POST["project"] != "null")					$project = $_POST["project"];
	if (isset($_POST["area"]) && $_POST["area"] != "null")							$area = $_POST["area"];
	if (isset($_POST["dateType"]) && $_POST["dateType"] != "null")					$dateType = $_POST["dateType"];

	if ($dateType == "currentwk")      {
		$dateFrom = currentWeekStart();
		$dateTo = $today;
	}
	else if ($dateType == "previouswk")        {
		$dateFrom = getDates(currentWeekStart(), -7);
		$dateTo = getDates(currentWeekStart(), -1);
	}
	else if ($dateType == "currentmnth")       {
		$dateFrom = getMonthStart($today);
		$dateTo = $today;
	}
	else if ($dateType == "previousmnth")      {
		$dateFrom = getPreviousMonthStart();
		$dateTo = getMonthEnd(getPreviousMonthStart());
	}
	else if ($dateType == "projectLifetime")      {
		$dateFromT = q("SELECT MIN(date) FROM ApprovedTime WHERE project_id = '".$project."'");
		$dateFromE = q("SELECT MIN(date) FROM ApprovedExpense WHERE project_id = '".$project."'");

		if ($dateFromE == null)	$dateFromE = $dateFromT;

		$dateFrom = ($dateFromT <= $dateFromE) ? $dateFromT : $dateFromE;
		$dateTo = $today;
	}
	else if ($dateType == "custom")    {
		$dateFrom = addslashes(strip_tags($_POST["dateFrom"]));
		$dateTo = addslashes(strip_tags($_POST["dateTo"]));
	}

	$where = "";

	$where .= "AND (CASE WHEN p.p_type = 'CP'
						THEN (ts.status IN ('2') OR es.status IN ('2'))
						ELSE (ts.status IN ('2') OR es.status IN ('2') OR ts.status2 IN ('2') OR es.status2 IN ('2'))
					END) ";

	$where .= "AND cot.total_budget > 0 ";

	$projects = q("SELECT DISTINCT(p.id), p.name
					FROM (Project AS p
						INNER JOIN Project_User AS pu ON pu.project_id = p.id
						INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
						LEFT JOIN ApprovedTime AS ts ON ts.project_id = p.id
						LEFT JOIN ApprovedExpense AS es ON es.project_id = p.id)
					WHERE pu.user_id = '".$_SESSION["user_id"]."'
						AND cot.company_id = '".$_SESSION["company_id"]."'
						AND pu.company_id = '".$_SESSION["company_id"]."'
						AND p.completed = '0'
						AND p.deleted = '0'
						$where
					ORDER BY UPPER(p.name)");

	if (is_numeric($project))	$areas = q("SELECT id,name FROM areas WHERE pID = '".$project."' AND active = '1' ORDER BY name");
?>
	<table width="100%">
		<tr>
			<td class="centerdata">
				<form id="approvals" name="approvals" action="" method="post">
					<input id="invoiceID" name="invoiceID" type="hidden" value="" />
					<table width="100%">
						<tr>
							<td class="centerdata">
								<h6>Invoice & Payment Certificate</h6>
							</td>
						</tr>
						<tr>
							<td class="centerdata">
								<h6><i style="font-size:13px;">(Only Approved Time/Expenses)</i></h6>
							</td>
						</tr>
					</table>
					<br/>
					<table width="100%">
						<tr>
							<td class="on-description" width="50%">
								Project:
							</td>
							<td width="50%">
								<select id="project" name="project" class="on-field">
									<option value="null">--  Select Project  --</option>
									<?php
										if (is_array($projects))	{
											foreach ($projects as $p)	{
												if ($project == $p[0])	echo "<option value='".$p[0]."' selected>".$p[1]."</option>";
												else					echo "<option value='".$p[0]."'>".$p[1]."</option>";
											}
										}
									?>
								</select>
								<div id="projectDiv" style="display: none;"><font class="on-validate-error">* Project must be selected</font></div>
							</td>
						</tr>
						<tr class="<?php echo (!is_array($areas)) ? "hidden": "";?>">
							<td class="on-description" width="50%">
								Area:
							</td>
							<td width="50%">
								<select id="area" name="area" class="on-field">
									<option value="0">All Areas</option>
									<?php
										if (is_array($areas))	{
											foreach ($areas as $a)	{
												if ($area == $a[0])	echo "<option value='".$a[0]."' selected>".$a[1]."</option>";
												else				echo "<option value='".$a[0]."'>".$a[1]."</option>";
											}
										}
									?>
								</select>
							</td>
						</tr>
						<tr>
							<td class="on-description" width="50%">
								Date Type:
							</td>
							<td width="50%">
								<select id="dateType" name="dateType" class="on-field">
									<option value="currentwk" <?php echo ($dateType == "currentwk") ? "selected" : ""; ?>>Current Week</option>
									<option value="previouswk" <?php echo ($dateType == "previouswk") ? "selected" : ""; ?>>Previous Week</option>
									<option value="currentmnth" <?php echo ($dateType == "currentmnth") ? "selected" : ""; ?>>Current Month</option>
									<option value="previousmnth" <?php echo ($dateType == "previousmnth") ? "selected" : ""; ?>>Previous Month</option>
									<option value="projectLifetime" <?php echo ($dateType == "projectLifetime") ? "selected" : ""; ?> disabled>Project Lifetime</option>
									<option value="custom" <?php echo ($dateType == "custom") ? "selected" : ""; ?>>Custom Dates</option>
								</select>
							</td>
						</tr>
					</table>
					<div id="datesDiv" style="display:<?php echo ($dateType == "custom") ? "block" : "none"; ?>;">
						<table width="100%">
							<tr>
								<td class="on-description" width="50%">
									Date From:
								</td>
								<td  width="50%">
									<input id="dateFrom" name="dateFrom" class="on-field-date date" type="text" style="text-align:right;" value="<?php echo $dateFrom; ?>">
									<div id="dateFromDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo date("Y-m-d"); ?></font></div>
								</td>
							</tr>
							<tr>
								<td class="on-description" width="50%">
									Date To:
								</td>
								<td  width="50%">
									<input id="dateTo" name="dateTo" class="on-field-date date" type="text" style="text-align:right;" value="<?php echo $dateTo; ?>">
									<div id="dateToDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo date("Y-m-d"); ?></font></div>
								</td>
							</tr>
						</table>
					</div>
					<br/>
					<input id="btnDisplay" name="btnDisplay" type="button" value="Display Information">
					<input id="display" name="display" type="hidden" value="0" />
					<?php
					if (isset($_POST["display"]) && $_POST["display"] == "1")	{
					?>
						<br/><br/><br/>
					<?php
						$types = array("Time","Expenses");
						$timeHasAreas = 0;
						$expensesHasAreas = 0;
						$timeHasSP = 0;
						$expensesHasSP = 0;

						if (is_array($types))       {
							foreach ($types as $type)       {
								$select = "";
								$where = "";

								$prefix = ($type == "Time") ? "ts" : "es";

								$where .= "AND c.id = '".$_SESSION["company_id"]."' ";
								$where .= "AND (CASE WHEN p.p_type = 'CP'
													THEN (".$prefix.".status IN ('2'))
													ELSE (".$prefix.".status IN ('2') OR ".$prefix.".status2 IN ('2'))
												END) ";

								$where .= "AND cot.total_budget > 0 ";
								$where .= ($project != "null") ? "AND p.id = '".$project."' " : "";
								$where .= ($area != "0") ? "AND ar.id = '".$area."' " : "";
								$where .= "AND p.completed = '0' ";
								$where .= "AND p.deleted = '0' ";
								$where .= "AND ".$prefix.".invoiced = '0' ";

								if ($type == "Time")	{
									$areaCount = q("SELECT COUNT(DISTINCT(ar.name))
												FROM (ApprovedTime AS ts
													INNER JOIN Employee AS e ON ts.user_id = e.id
													INNER JOIN Activities AS a ON ts.activity_id = a.id
													INNER JOIN ActivityTypes AS at ON a.parent_id = at.id
													INNER JOIN Project AS p ON ts.project_id = p.id
													INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
													INNER JOIN Company AS c ON cot.company_id = c.id
													LEFT JOIN areas AS ar ON ts.area_id = ar.id )
												WHERE ts.date BETWEEN '".$dateFrom."' AND '".$dateTo."'
													$where");
									$projectTypes = q("SELECT DISTINCT(p.p_type),p.p_type
													FROM (ApprovedTime AS ts
														INNER JOIN Employee AS e ON ts.user_id = e.id
														INNER JOIN Activities AS a ON ts.activity_id = a.id
														INNER JOIN ActivityTypes AS at ON a.parent_id = at.id
														INNER JOIN Project AS p ON ts.project_id = p.id
														INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
														INNER JOIN Company AS c ON cot.company_id = c.id
														LEFT JOIN areas AS ar ON ts.area_id = ar.id )
													WHERE ts.date BETWEEN '".$dateFrom."' AND '".$dateTo."'
														$where");

									if ($areaCount > 0)	$timeHasAreas = 1;
									if (count($projectTypes) > 1)	$timeHasSP = 1;
									else if ($projectTypes[0][0] == "SP")	$timeHasSP = 1;
								} else	{
									$areaCount = q("SELECT COUNT(DISTINCT(ar.name))
													FROM (ApprovedExpense AS es
															INNER JOIN Employee AS e ON es.user_id = e.id
															INNER JOIN dropdowns AS dd ON es.expense_type_id = dd.id
															LEFT JOIN Activities AS a ON es.activity_id = a.id
															LEFT JOIN ActivityTypes AS at ON a.parent_id = at.id
															INNER JOIN Project AS p ON es.project_id = p.id
															INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
															INNER JOIN Company AS c ON cot.company_id = c.id
															LEFT JOIN areas AS ar ON es.area_id = ar.id
															LEFT JOIN Vehicle AS v ON es.vehicle_id = v.id
															LEFT JOIN Disbursements AS d ON es.disbursement_id = d.id
															LEFT JOIN DisbursementTypes AS dt ON dt.id = d.parent_id)
													WHERE es.date BETWEEN '".$dateFrom."' AND '".$dateTo."'
													$where");
									$projectTypes = q("SELECT DISTINCT(p.p_type),p.p_type
														FROM (ApprovedExpense AS es
																INNER JOIN Employee AS e ON es.user_id = e.id
																INNER JOIN dropdowns AS dd ON es.expense_type_id = dd.id
																LEFT JOIN Activities AS a ON es.activity_id = a.id
																LEFT JOIN ActivityTypes AS at ON a.parent_id = at.id
																INNER JOIN Project AS p ON es.project_id = p.id
																INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
																INNER JOIN Company AS c ON cot.company_id = c.id
																LEFT JOIN areas AS ar ON es.area_id = ar.id
																LEFT JOIN Vehicle AS v ON es.vehicle_id = v.id
																LEFT JOIN Disbursements AS d ON es.disbursement_id = d.id
																LEFT JOIN DisbursementTypes AS dt ON dt.id = d.parent_id)
														WHERE es.date BETWEEN '".$dateFrom."' AND '".$dateTo."'
															$where");

									if ($areaCount > 0)	$expensesHasAreas = 1;
									if (count($projectTypes) > 1)	$expensesHasSP = 1;
									else if ($projectTypes[0][0] == "SP")	$expensesHasSP = 1;
								}
							}
						}

						$hasAreas = ($timeHasAreas || $expensesHasAreas) ? 1 : 0;
						$isSP = ($timeHasSP || $expensesHasSP) ? 1 : 0;

						if (is_array($types))       {
							foreach ($types as $type)       {
								$select = "";
								$where = "";

								$prefix = ($type == "Time") ? "ts" : "es";

								$where .= "AND c.id = '".$_SESSION["company_id"]."' ";
								$where .= "AND (CASE WHEN p.p_type = 'CP'
													THEN (".$prefix.".status IN ('2'))
													ELSE (".$prefix.".status IN ('2') OR ".$prefix.".status2 IN ('2'))
												END) ";

								$where .= "AND cot.total_budget > 0 ";
								$where .= ($project != "null") ? "AND p.id = '".$project."' " : "";
								$where .= ($area != "0") ? "AND ar.id = '".$area."' " : "";
								$where .= "AND p.completed = '0' ";
								$where .= "AND p.deleted = '0' ";
								$where .= "AND ".$prefix.".invoiced = '0' ";

								if ($type == "Time")	{
									if ($hasAreas)	{
										$select .= "c.name,p.name,CONCAT(e.lstname, ', ', e.frstname),ar.name,at.type,a.name,ts.descr,ts.date,ts.time_spent,ts.rate,ts.time_spent * ts.rate,1";
										$headings[$type] = array("ID","Company Name","Project Name","Employee Name","Area","Activity Type","Activity","Description","Date","Time Spent",
																	"Rate","Cost <i>(".$_SESSION["currency"].")</i>","Add To Invoice");
									} else	{
										$select .= "c.name,p.name,CONCAT(e.lstname, ', ', e.frstname),at.type,a.name,ts.descr,ts.date,ts.time_spent,ts.rate,ts.time_spent * ts.rate,1";
										$headings[$type] = array("ID","Company Name","Project Name","Employee Name","Activity Type","Activity","Description","Date","Time Spent",
																	"Rate","Cost <i>(".$_SESSION["currency"].")</i>","Add To Invoice");
									}

									$timeData = q("SELECT ts.id,$select
													FROM (ApprovedTime AS ts
														INNER JOIN Employee AS e ON ts.user_id = e.id
														INNER JOIN Activities AS a ON ts.activity_id = a.id
														INNER JOIN ActivityTypes AS at ON a.parent_id = at.id
														INNER JOIN Project AS p ON ts.project_id = p.id
														INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
														INNER JOIN Company AS c ON cot.company_id = c.id
														LEFT JOIN areas AS ar ON ts.area_id = ar.id )
													WHERE ts.date BETWEEN '".$dateFrom."' AND '".$dateTo."'
														$where
													ORDER BY c.name, p.name, ts.date, ts.on_date, ts.on_time, ar.name");
								} else	{
									if ($hasAreas)	{
										$select .= "c.name,p.name,CONCAT(e.lstname, ', ', e.frstname),dd.name,IF(dd.name = 'Driving',v.type,IF(dd.name = 'Printing',CONCAT(dt.type,': ',d.name),es.otherDescr)),ar.name,at.type,".
													"a.name,es.descr,es.date,IF(dd.name = 'Driving',es.kilometers,es.total), es.rate, es.expense,1";
										$headings[$type] = array("ID","Company Name","Project Name","Employee Name","Expense Type","Expense Info","Area","Activity Type","Activity","Description",
																	"Date","Amount","Rate","Cost <i>(".$_SESSION["currency"].")</i>","Add To Invoice");
									} else	{
										$select .= "c.name,p.name,CONCAT(e.lstname, ', ', e.frstname),dd.name,IF(dd.name = 'Driving',v.type,IF(dd.name = 'Printing',CONCAT(dt.type,': ',d.name),es.otherDescr)),at.type,".
													"a.name,es.descr,es.date,IF(dd.name = 'Driving',es.kilometers,es.total), es.rate, es.expense,1";
										$headings[$type] = array("ID","Company Name","Project Name","Employee Name","Expense Type","Expense Info","Activity Type","Activity","Description",
																	"Date","Amount","Rate","Cost <i>(".$_SESSION["currency"].")</i>","Add To Invoice");
									}

									$expenseData = q("SELECT es.id,$select
														FROM (ApprovedExpense AS es
															INNER JOIN Employee AS e ON es.user_id = e.id
															INNER JOIN dropdowns AS dd ON es.expense_type_id = dd.id
															LEFT JOIN Activities AS a ON es.activity_id = a.id
															LEFT JOIN ActivityTypes AS at ON a.parent_id = at.id
															INNER JOIN Project AS p ON es.project_id = p.id
															INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id
															INNER JOIN Company AS c ON cot.company_id = c.id
															LEFT JOIN areas AS ar ON es.area_id = ar.id
															LEFT JOIN Vehicle AS v ON es.vehicle_id = v.id
															LEFT JOIN Disbursements AS d ON es.disbursement_id = d.id
															LEFT JOIN DisbursementTypes AS dt ON dt.id = d.parent_id)
														WHERE es.date BETWEEN '".$dateFrom."' AND '".$dateTo."'
															$where
														ORDER BY c.name, es.date, es.on_date, es.on_time, ar.name");
								}
							}
						}

						if (is_array($timeData) || is_array($expenseData))	{
							$prefix = q("SELECT prefix FROM Project WHERE id = '".$project."'");

							$invoiceName = "".$prefix.date("ym")."-";
							$claimNumber = "".$prefix;

							$invoiceCount = q("SELECT COUNT(name) FROM LoadInvoices WHERE name LIKE '".$invoiceName."%'");
							$claimCount = q("SELECT COUNT(claimNumber) FROM LoadInvoices WHERE claimNumber LIKE '".$claimNumber."%' AND project_id = '".$project."'");

							$invoiceName = "".$prefix.date("ym")."-".str_pad(($invoiceCount + 1), 3, "0", STR_PAD_LEFT);;
							$claimNumber = "".$prefix.($claimCount + 1);;
							/*
							Invoice Number : TD YYMM-Next Number in the Month
							Claim Nuber : Current Claim Number ++ (Next number of claim per project)
							*/
						?>
							<table width="100%">
								<tr>
									<td class="on-description" width="50%">
										Invoice Name/Number:
									</td>
									<td  width="50%">
										<input id="invoiceName" name="invoiceName" class="on-field" type="text" value="<?php echo $invoiceName; ?>">
										<div id="invoiceNameDiv" style="display: none;"><font class="on-validate-error">* Invoice name must be entered</font></div>
									</td>
								</tr>
								<tr>
									<td class="on-description" width="50%">
										Claim Number:
									</td>
									<td  width="50%">
										<input id="claimNumber" name="claimNumber" class="on-field" type="text" value="<?php echo $claimNumber; ?>">
										<div id="claimNumberDiv" style="display: none;"><font class="on-validate-error">* Claim number must be entered</font></div>
									</td>
								</tr>
								<tr>
									<td class="on-description" width="50%">
										Date Created:
									</td>
									<td  width="50%">
										<input id="dateCreated" name="dateCreated" class="on-field-date date" type="text" style="text-align:right;" value="<?php echo (isset($_POST["dateCreated"])) ? $_POST["dateCreated"] : $today; ?>">
										<div id="dateCreatedDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo date("Y-m-d"); ?></font></div>
									</td>
								</tr>
								<tr>
									<td class="on-description" width="50%">
										Date Due:
									</td>
									<td  width="50%">
										<input id="dateDue" name="dateDue" class="on-field-date date" type="text" style="text-align:right;" value="<?php echo (isset($_POST["dateDue"])) ? $_POST["dateDue"] : getDates($today,30); ?>">
										<div id="dateDueDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo date("Y-m-d"); ?></font></div>
									</td>
								</tr>
							</table>
						<?php
							$timeColumns = count($headings["Time"]) - 2;
							$expenseColumns = count($headings["Expenses"]) - 2;
							$row = 0;

							$columns = (!is_array($expenseData)) ? $timeColumns : $expenseColumns;

							if (is_array($types))   {
								echo "<div class='on-20px'>";
									echo "<table class='on-table on-table-center'>";

									foreach ($types as $type)	{
										$displayString = "";

										$data = ($type == "Time") ? $timeData : $expenseData;

										if (is_array($data))	{
											$displayString .= "<tr>";
												$displayString .= "<td style='text-align:center' class='on-table-clear' colspan='100%'>";
													 $displayString .= "<h6>".$type."</h6>";
												$displayString .= "</td>";
											$displayString .= "</tr>";

											$exceldata[$row][] = strip_tags($dataStatus." ".$type);

											for ($a = 1; $a < $columns; $a++)    $exceldata[$row][] = "";

											$row++;

											//  Headings
											$displayString .= "<tr>";

											foreach ($headings[$type] as $h)	{
												if ($h != "ID")	{
													$colspan = 1;

													if ($type == "Time")	$colspan = ($h == "Employee Name") ? 3 : 1;

													$displayString .= "<th colspan='".$colspan."' style='white-space:nowrap'>".$h."</th>";
													$exceldata[$row][] = strip_tags($h);

													for ($i = 1; $i < $colspan; $i++)	$exceldata[$row][] = "";
												}
											}

											$displayString .= "</tr>";
											$row++;

											//  Data
											$counter = 1;

											if ($type == "Time")	$totalTime = 0;

											$fields = ($type == "Time") ? array("A","B") : array("C","D");

											$totalCost = 0;

											foreach ($data as $r)	{
												$fieldCount = 0;
												$col = 0;

												$displayString .= "<tr>";

												foreach ($r as $d)	{
													$colspan = ($type == "Time" && $headings[$type][$col] == "Employee Name") ? 3 : 1;

													if ($headings[$type][$col] == "Time Spent")                                    $totalTime += $d;
													if ($headings[$type][$col] == "Cost <i>(".$_SESSION["currency"].")</i>")       $totalCost += $d;

													//  ID
													if ($headings[$type][$col] == "ID")
														$displayString .= "<input id='".$fields[$fieldCount].$counter."' name='".$fields[$fieldCount++].$counter."' type='hidden' value='".str_replace("'", "&#39;" , $d)."' />";
													//  ADD TO INVOICE
													else if ($headings[$type][$col] == "Add To Invoice")	{
														$displayString .= "<td class='centerdata' style='white-space:nowrap'>";
															$displayString .= "<input id='".$fields[$fieldCount].$counter."' name='".$fields[$fieldCount++].$counter."' type='checkbox' checked value='1' />";
														$displayString .= "</td>";
													}
													//  OTHER FIELDS
													else	{
														if (preg_match("/^((\d{2}(([02468][048])|([13579][26]))[\-\/\s]?((((0?[13578])|(1[02]))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-\/\s]?((0?[1-9])|([1-2][0-9])))))|(\d{2}(([02468][1235679])|([13579][01345789]))[\-\/\s]?((((0?[13578])|(1[02]))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-\/\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))(\s(((0?[1-9])|(1[0-2]))\:([0-5][0-9])((\s)|(\:([0-5][0-9])\s))([AM|PM|am|pm]{2,2})))?$/", $d))
															$displayString .= "<td colspan='".$colspan."' style='white-space:nowrap'>".$d."</td>";
														else if (is_numeric($d))
															$displayString .= "<td colspan='".$colspan."' style='white-space:nowrap' class='rightdata'>".number_format((double)$d, 2, ".", "")."</td>";
														else if ($d == "")
															$displayString .= "<td colspan='".$colspan."' style='white-space:nowrap'>-</td>";
														else
															$displayString .= "<td colspan='".$colspan."' style='white-space:nowrap'>".$d."</td>";

														$exceldata[$row][] = strip_tags($d);
													}


													for ($i = 1; $i < $colspan; $i++)	$exceldata[$row][] = "";

													$col++;
												}

												$displayString .= "</tr>";
												$counter++;
												$row++;
											}

											$displayString .= "<tr>";
												$colspan = ($type == "Time") ? array_search("Time Spent", $headings[$type]) + 2 : array_search("Cost <i>(".$_SESSION["currency"].")</i>", $headings[$type]);

												$displayString .= "<td class='on-table-total' colspan='".($colspan - 1)."'>Total:</td>";

												for ($i = 1; $i < $colspan; $i++)	$exceldata[$row][] = "";

												$exceldata[$row][] .= "Total:";

												if ($type == "Time")    {
													$displayString .= "<td class='on-table-total'>".number_format((double)$totalTime, 2, ".", "")."</td>";
													$displayString .= "<td class='on-table-total'></td>";
													$displayString .= "<td class='on-table-total'>".number_format((double)$totalCost, 2, ".", "")."</td>";

													$exceldata[$row][] .= $totalTime;
													$exceldata[$row][] .= "";
													$exceldata[$row][] .= $totalCost;
												} else  {
													$displayString .= "<td class='on-table-total'>".number_format((double)$totalCost, 2, ".", "")."</td>";

													$exceldata[$row][] .= $totalCost;
												}

												$displayString .= "<td class='on-table-total'></td>";
											$displayString .= "</tr>";
											$displayString .= "<input id='".$type."' name='".$type."' type='hidden' value='".$counter."' />";
											$row++;
										}

										echo $displayString;

										if ($type == "Time")	{
											echo "<tr><td style='background-color:transparent' colspan='100%'>&nbsp;</td></tr>";

											for ($a = 0; $a < $columns; $a++)    $exceldata[$row][] = "";

											$row++;
										}
									}

										echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
									echo "</table>";
								echo "</div><br/>";
							}
							?>
                            <input id="btnUpdate" name="btnUpdate" type="button" value="Create Invoice & Payment Certificate">
                            <input id="update" name="update" type="hidden" value="0" />
                            <br/><br/><br/>
						<?php
						}
					}
					?>
				</form>
			</td>
		</tr>
	</table>
<?php
    //  Print Footer
    print_footer();
?>
