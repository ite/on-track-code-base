<?php
    session_start();

    include("_db.php");
    include("_functions.php");
    include("_notification.php");
    include("graphics.php");

    $from = "On Track Notification <support@integrityengineering.co.za>";
    $reply_to = $from;
    $subject = "On Track - Employee(s) Behind Schedule [User Name & Surname Here]";

    $notify_id = q("SELECT id FROM Notify WHERE code = 'Bookings'");

    $message = print_email_header($subject)."User Name & Surname Here<br/>\n".
                "<br/>\n".
                "You are currently behind schedule with your timesheet(s) and your project manager(s) have been informed.<br/>\n".
                "<br/>\n".
                "To book your time now, go to <a href='http://live.on-track.ws/'>http://live.on-track.ws/</a><br/>\n".
                "<br/>\n".
                "Regards,<br/>".
                "On Track Notification<br/>".print_email_footer();

    $to = "Mauritz Kruger <mk@iteonline.co.za>,Franszo Faul <ffaul@iteonline.co.za>,Elrich Faul <efaul@iteonline.co.za>,Tiaan van Wyk <tvw@iteonline.co.za>";

    if ($to != "")
        sendmail($to, $subject, $message, $from, $reply_to, true);
?>
