<?php
    //  Daily
    include("../_db.php");
    include("../_functions.php");
    include("../_notification.php");
    include("../graphics.php");

    $companies = q("SELECT id, name FROM Company WHERE locked = '0'");

    if (is_array($companies)) {
        foreach ($companies as $company) {
            if ($company[0] == q("SELECT id FROM Company WHERE name = 'integrITy Engineering'") ||
                $company[0] == q("SELECT id FROM Company WHERE name = 'Aurecon Consortium'") ||
                $company[0] == q("SELECT id FROM Company WHERE name = 'Counselling'") ||
                $company[0] == q("SELECT id FROM Company WHERE name = 'DG Consult'") ||
                $company[0] == q("SELECT id FROM Company WHERE name = 'Kimopax'") ||
                $company[0] == q("SELECT id FROM Company WHERE name = 'OMI Solutions'") ||
                $company[0] == q("SELECT id FROM Company WHERE name = 'Sakhiwo'")){}
            else {
                //  Run Non Actioned Events Function
                $notify_id = q("SELECT id FROM Notify WHERE code = 'Bookings'");
                $executed = q("SELECT id FROM Events WHERE date = '".date("Y-m-d")."' AND notify_id = '$notify_id' AND company_id = '".$company[0]."'");

                if (!$executed)
                    nonActionedEvents($company[0]);
            }

            if ($company[0] == q("SELECT id FROM Company WHERE name = 'integrITy Engineering'"))  {
                //  Run Non Actioned Events Function
                $executed = q("SELECT id FROM Events WHERE date = '".date("Y-m-d")."' AND notify_id = '0' AND company_id = '".$company[0]."'");

                if (!$executed)
                    updateDueDates($company[0]);
            }

            //  Run SLA Limit Daily Function
            slaLimitDaily($company[0]);

            //  Run Project Over Budget/Due Date Function
            overBudget($company[0]);
            overDueDate($company[0]);
        }
    }

?>
