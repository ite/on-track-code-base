<?php
    //  Every 15 Minutes
    include("../_db.php");
    include("../_dates.php");
    include("../_functions.php");
    include("../_notification.php");

    $companies = q("SELECT id, name FROM Company WHERE locked = '0'");

    if (is_array($companies)) {
        foreach ($companies as $company) {
            //  Run Actioned Events Function
            actionedEvents($company[0]);
        }
    }

?>
