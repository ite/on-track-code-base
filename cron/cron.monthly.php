<?php
    //  Monthly
    include("../_db.php");
    include("../_dates.php");
    include("../_functions.php");
    include("../_notification.php");

    //calls
    budgetSpawn();

    $companies = q("SELECT id, name FROM Company WHERE locked = '0'");
    if (is_array($companies)) {
        foreach ($companies as $company) {
            //  Run SLA Limit Monthly Function
            $month_start = date("Y-m-01");
            $month_end = getMonthEnd($month_start);
            if (date("Y-m-d") == $month_end) {
                if (date("H:i:s") >= "23:40:00") {
                    $notify_id = q("SELECT id FROM Notify WHERE code = 'SLA'");
                    $executed = q("SELECT id FROM Events WHERE message LIKE 'SLA Monthly' AND date >= '$month_start' ".
                                    "AND date <= '$month_end' AND notify_id = '$notify_id' AND company_id = '".$company[0]."'");

                    if (!$executed)
                        slaLimitMonthly($company[0]);

                    $notify_id = q("SELECT id FROM Notify WHERE code = 'ExpenseMonthly'");
                    $executed = q("SELECT id FROM Events WHERE message LIKE 'Expense Monthly' AND date >= '$month_start' ".
                                    "AND date <= '$month_end' AND notify_id = '$notify_id' AND company_id = '".$company[0]."'");

                    if (!$executed)
                        expenseMonthly($company[0]);
                }
            }       
        }
    }
?>
