<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("include/sajax.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");
        
    if (!hasAccess("REP_EMP_STATUS"))
        header("Location: noaccess.php");
        
    // Class for table background color
    $colorClass = "OnTrack_#_";

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("3", "reports");
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="report_active_projects">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Employee Status Report</h6>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <?php
                        ///////////////////////////
                        //  Get Information
                        ///////////////////////////
                        $users = q("SELECT e.id, CONCAT(e.lstname, ', ', e.frstname) FROM (Employee AS e INNER JOIN Company_Users AS cu ON e.id = cu.user_id) ".
                                    "WHERE cu.company_id = '".$_SESSION["company_id"]."' AND e.email != 'admin' ORDER BY e.lstname, frstname");

                        ///////////////////////////
                        //  Create Information String
                        ///////////////////////////
                        //  Table Headers
                        $string                                         = "";
                        $grand_total                                    = 0;
                        $total_available                                = 0;
                        
                        $excelheadings[$row][] = "Report: Active Projects";
                        $excelheadings[$row][] = "";
                        $excelheadings[$row][] = "";
                        $excelheadings[$row][] = "";
                            $row++;
                        
                        if($_POST['projectType'] == "null")
                            $projectTypeHead = "All Types";
                        else if($_POST['projectType'] == "0")
                            $projectTypeHead = "Shared Projects";
                        else if($_POST['projectType'] != "")
                            $projectTypeHead = q("SELECT type FROM ProjectTypes WHERE id = ".$_POST['projectType']."");
                            
                        if($_POST['project'] == "null")
                            $projectHead = "All Projects";
                        else if($_POST['project'] != "")
                            $projectHead = q("SELECT name FROM Project WHERE id = ".$_POST['project']."");
                            
                        if($_POST['user'] == "null")
                            $username = "All Users";
                        else if($_POST['user'] != "")
                            $username = q("SELECT CONCAT(frstname, lstname) FROM Employee WHERE id = ".$_POST['user']."");
                        
                        $excelheadings[$row][] = "Project Type: $projectTypeHead";
                        $excelheadings[$row][] = "";
                        $excelheadings[$row][] = "";
                        $excelheadings[$row][] = "";
                            $row++;
                        $excelheadings[$row][] = "Project: $projectHead";
                        $excelheadings[$row][] = "";
                        $excelheadings[$row][] = "";
                        $excelheadings[$row][] = "";
                            $row++;
                        $excelheadings[$row][] = "User: $username";
                        $excelheadings[$row][] = "";
                        $excelheadings[$row][] = "";
                        $excelheadings[$row][] = "";
                            $row++;
                            
                        $exceldata[$row][] = $colorClass."Project Name";
                        $exceldata[$row][] = $colorClass."Project Type";
                        $exceldata[$row][] = $colorClass."Due Date";
                        $exceldata[$row][] = $colorClass."Total Budget";
                            $row++;
                        
                        $headers = "";
                        
                        if($_POST['projectType'] || $_POST['project']){
                        $headers                                        =   "<tr>
                                                                                        <td class='on-table-clear' colspan='100%'><a>Active Projects</a></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class='on-table-clear' colspan='100%'>Project Types: $projectTypeHead</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class='on-table-clear' colspan='100%'>Project: $projectHead</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class='on-table-clear' colspan='100%'>User: $username</td>
                                                                                    </tr>";
                        }
                        $headers
                                                                                .= "<tr>
                                                                                        <th>Project Name</th>
                                                                                        <th>Project Type</th>
                                                                                        <th>Due Date</th>
                                                                                        <th>Total Budget <i>(".$_SESSION["currency"].")</i></th>
                                                                                    </tr>";

                        //  Table Information
                        if (is_array($projects))
                        {
                            foreach ($projects as $project)
                            {
                                    $row_counter = 0;                   // Excel data - rows
                                    if(is_numeric($project[4]))
                                        $total_budget                           = number_format($project[4], 2, ".", "");
                                    else
                                        $total_budget                           = number_format(0, 2, ".", "");
                                        
                                    if($project[2] == "")
                                        $project[2] = "Shared Project";

                                    $string                                 .= "<tr>
                                                                                <td align='left'>".$project[1]."</td>
                                                                                <td class='left'>".$project[2]."</td>
                                                                                <td class='rightdata'>".$project[3]."</td>
                                                                                <td class='rightdata'>".$total_budget."</td>
                                                                                </tr>";
                                                                                
                                    $exceldata[$row][] = $project[1];
                                    $exceldata[$row][] = $project[2];
                                    $exceldata[$row][] = $project[3];
                                    $exceldata[$row][] = $total_budget;
                                        $row++;
                            }
                        }

                        ///////////////////////////
                        //  Display Information
                        if ($string != "")      $string = $headers.$string;
                        else                    $string .= $headers."<tr><td align='center' colspan='5'>No Projects To Display</td></tr>";

                        if ($string != "")      {
                            echo "<div class='on-20px'><table class='on-table-center on-table'>";
                                echo "".$string;
                                echo  "<tfoot>
                                            <tr>
                                                <td colspan='100%'></td>
                                            </tr>
                                        </tfoot>";
                            echo "</table></div>";
                            echo "<br/>";

                            ///////////////////////////
                            //  Set Export Information
                            $_SESSION["fileName"] = "Employee_Status_Report";
                            if($excelheadings && $exceldata)
                                $_SESSION["fileData"] = array_merge($excelheadings, $exceldata);
                            ///////////////////////////

                            echo "<input name='btnExport' onClick=\"location.href='_export.php'\" type='button' value='Export to Excel' />";
                        }
                        ///////////////////////////
                    ?>
                </form>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>