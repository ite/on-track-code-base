<?php
	session_start();

	$errorMessage = "";

	include("_db.php");

	function print_analytics() {
		global $analytics;
?>
		<script language="JavaScript" src="analytics/<?php echo $analytics; ?>"></script>
<?php
	}

	$index = 1;
?>
<!--
Title: 					On-Track Login Screen
For:
Site:					On-Track
Author:					DID
Version:				1.0
Date Created:			2011/04/12
Last Modified:			2011/04/13
-->
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html>
	<head>
		<title>On-Track - Registration Wizard</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="shortcut icon" href="images/favicon.png">
		<!-- BEGIN: STYLESHEET -->
		<link rel="stylesheet" href="CSSFiles/css/login.css"/>
		<style type="text/css">
		.hidden                 {display:none;}
		</style>
		<!--[if IE]>
		<style type="text/css">
		.on-emo	{margin-left:-96px;}
		.on-logon{top:10%;}
		.on-log-bg{background:transparent;border:0;}
		.on-log-con{background:transparent;border:0;}
		.on-integ{background:none;}
		.on-integ-ie{
		background:url(im/on-integ.png);
		display:block;
		height:160px;
		width:160px;
		position:absolute;
		right:0;
		top:0;
		z-index:9999;}
		.on-verse{
			display:block;
			width:100%;
			margin-left:-220px;
			top:60%;
			line-height:150%;
			position:absolute;
			text-align:center;
			cursor: pointer;
			padding-top:120px;
		}
		</style>
		<![endif]-->
		<!-- END: STYLESHEET -->

		<!--  BEGIN:  ANALYTICS  -->
		<?php
			print_analytics();
		?>
		<!--  END:  ANALYTICS  -->

		<!-- BEGIN: JQUERRY -->
		<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
		<script language="JavaScript" src="include/jquery.js"></script>
		<script type="text/javascript" src="include/datepicker.js"></script>
		<script language="JavaScript" src="include/validation.js"></script>
		<script language="JavaScript" src="registration/validation.js"></script>
		<script type="Text/JavaScript">
			function json_decode(input) {
				//return JSON.parse(input, null);
				return eval( "(" + input + ")" );
			}

			var wizardValidation;

			var buttonsHideShow;
			var windowPosition;
			var wizardLOAD;

			var projectTypeLOAD;
			var userInfoLOAD;
			var userRatesLOAD;

			var successMessage;
			var errorMessage;
			var modalID = "";
			var currentStep = "step1";

			var companyInfo = new Array();
			var userInfo = new Array();
			var rateInfo = new Array();
			var projectTypeInfo = new Array();
			var activityTypeInfo = new Array();
			var vehicleInfo = new Array();
			var projectInfo = new Array();

			var projectTypes = new Array();
			var userRates = new Array();

			userRates[0] = "0.00";

			jQuery(function()    {
				jQuery("#regDate").DatePicker( {
					format:"Y-m-d",
					date: jQuery("#regDate").val(),
					current: jQuery("#regDate").val(),
					starts: 1,
					position: "right",
					onBeforeShow: function()    {
						var _date = (!validation("date", jQuery("#regDate").val())) ? new Date(): jQuery("#regDate").val();
						jQuery("#regDate").DatePickerSetDate(_date, true);
					},
					onChange: function(formated, dates) {
						jQuery("#regDate").val(formated);
						jQuery("#regDate").DatePickerHide();
					},
				});

				jQuery("#dueDate").DatePicker( {
					format:"Y-m-d",
					date: jQuery("#dueDate").val(),
					current: jQuery("#dueDate").val(),
					starts: 1,
					position: "right",
					onBeforeShow: function()    {
						var _date = (!validation("date", jQuery("#dueDate").val())) ? new Date(): jQuery("#dueDate").val();
						jQuery("#dueDate").DatePickerSetDate(_date, true);
					},
					onChange: function(formated, dates) {
						jQuery("#dueDate").val(formated);
						jQuery("#dueDate").DatePickerHide();
					}
				});

				var checkCompany = function()       {
					jQuery.post("_ajax.php", {func: "recordExists", table: "Company", where: "name = '"+jQuery("#companyName").val()+"'"}, function(data)	{
						data = json_decode(data);

						if (data == "1")       {
							jQuery("#companyNameDiv").children().eq(0).text("Company Name Already In Use");
							jQuery("#companyNameDiv").show();
						} else  {
							jQuery("#companyNameDiv").children().eq(0).text("");
							jQuery("#companyNameDiv").hide();
						}
					});
				}

				var checkUser = function()       {
					jQuery.post("_ajax.php", {func: "recordExists", table: "Employee", where: "email = '"+jQuery("#email").val()+"'"}, function(data)	{
						data = json_decode(data);

						if (data == "1")       {
							jQuery("#emailDiv").children().eq(0).text("E-Mail Address Already In Use");
							jQuery("#emailDiv").show();
						} else  {
							jQuery("#emailDiv").children().eq(0).text("");
							jQuery("#emailDiv").hide();
						}
					});
				}

				wizardValidation = function()   {
					var valid = 1;

					//  Step 1 - Company Information
					valid &= step1_validation();

					//  Step 2 - Personal Information
					if (valid)
						valid &= step2_validation();

					//  Step 3 - Project Type(s)
					if (valid)
						valid &= step3_validation();

					//  Step 4 - Activity Type(s) & Activities
					if (valid)
						valid &= step4_validation();

					//  Step 5 - Vehicle(s)
					if (valid)
						valid &= step5_validation();

					//  Step 6 - Project Information
					if (valid)
						valid &= step6_validation();

					if (valid)	{
						jQuery.post("_ajax.php", {func: "registration", companyInfo: companyInfo,
																		userInfo: userInfo,
																		rateInfo: rateInfo,
																		projectTypeInfo: projectTypeInfo,
																		activityTypeInfo: activityTypeInfo,
																		vehicleInfo: vehicleInfo,
																		projectInfo: projectInfo}, function(data){
							data = json_decode(data);

							jQuery("#register").attr("action", data);
							jQuery("#register").submit();
						});
					}
				}

				buttonsHideShow = function() {
					jQuery("#backBtn").show();
					jQuery("#nextBtn").show();
					jQuery("#skipBtn").show();
					jQuery("#finishBtn").show();

					if (currentStep == "step1") {
						jQuery("#backBtn").hide();
						jQuery("#finishBtn").hide();
					}

					if (currentStep == "step1" || currentStep == "step2" || currentStep == "step3" || currentStep == "step4" || currentStep == "step6" || currentStep == "step7")
						jQuery("#skipBtn").hide();

					if (currentStep == "step7")
						jQuery("#nextBtn").hide();
					else
						jQuery("#finishBtn").hide();
				}

				windowPosition = function() {
					var id = "#modal_wizard";

					jQuery("#mask").css({"width":jQuery(document).width(),"height":jQuery(document).height()});
					jQuery('#mask').fadeIn("fast").fadeTo("fast",0.8);   //transition effect

					//Set the popup window to center
					var top = jQuery(window).height()/2-jQuery(id).height()/2;

					top = (top >= 40) ? top : 40;

					jQuery(id).css('top', top);

					jQuery(id).fadeIn("fast");   //transition effect
				}

				wizardLOAD = function() {
					jQuery("#"+currentStep).show();
					jQuery("li."+currentStep).children().eq(0).addClass("activeStep");

					buttonsHideShow();
					windowPosition();
				}
				wizardLOAD();

				jQuery("li .on-but").click(function()      {
					jQuery("#"+currentStep).hide();

					jQuery("li."+currentStep).children().eq(0).removeClass("activeStep");

					currentStep = jQuery(this).parent().attr("class");

					jQuery("#"+currentStep).show();
					jQuery("li."+currentStep).children().eq(0).addClass("activeStep");

					buttonsHideShow();
					windowPosition();
					userInfoLOAD();
				});

				jQuery("#nextBtn, #skipBtn, #backBtn").click(function()     {
					var direction = (jQuery(this).attr("id") == "backBtn") ? "backward" : "forward";

					jQuery("#"+currentStep).hide();

					jQuery("li."+currentStep).children().eq(0).removeClass("activeStep");
					currentStep = currentStep.replace(/step/gi, "");
					currentStep = (direction == "forward") ? "step"+(parseFloat(currentStep) + 1) : "step"+(parseFloat(currentStep) - 1);

					jQuery("#"+currentStep).show();
					jQuery("li."+currentStep).children().eq(0).addClass("activeStep");

					buttonsHideShow();
					windowPosition();
					userInfoLOAD();
				});

				jQuery("#finishBtn").click(function()   {
					wizardValidation();
				});

				//  Successful Modal Alerts
				successMessage = function(message)   {
					if (modalID != "")  jQuery(modalID).hide();

					jQuery("#maskNotification").css({"width":jQuery(document).width(),"height":jQuery(document).height()});
					jQuery('#maskNotification').fadeIn("fast").fadeTo("fast",0.8);   //transition effect

					var id = "#modal_success";
					jQuery("#successMessage").html(message);
					jQuery(id).css({"z-index":9999}).fadeIn("fast");   //transition effect
				};

				jQuery("#closeSuccess").click(function()	{
					jQuery("#maskNotification").hide();
					jQuery('.on-drop-menu').hide();
					jQuery('.on-contact-modal').hide();
				});

				//  Error Modal Alerts
				errorMessage = function(message)   {
					if (modalID != "")  jQuery(modalID).hide();

					jQuery("#maskNotification").css({"width":jQuery(document).width(),"height":jQuery(document).height()});
					jQuery('#maskNotification').fadeIn("fast").fadeTo("fast",0.8);   //transition effect

					var id = "#modal_error";
					jQuery("#errorMessage").html(message);
					jQuery(id).css({"z-index":9999}).fadeIn("fast");   //transition effect
				};

				jQuery("#closeError").click(function()	{
					jQuery("#modal_error").hide();

					if (modalID != "")  jQuery(modalID).show();
				});

				//  Mail Clicked
				jQuery(".on-but-mail").click(function()  {
					jQuery("#maskNotification").css({"width":jQuery(window).width(),"height":jQuery(document).height()});
					jQuery("#maskNotification").show();

					jQuery("#modal_help").show();
					var id = "#modal_help";
					jQuery(id).fadeIn("fast");   //transition effect
				});

				//  Help Clicked
				jQuery(".on-help").click(function()  {
					jQuery('#maskNotification').css({'width':jQuery(document).width(),'height':jQuery(document).height(),'z-index':9999});  //Set heigth and width to mask to fill up the whole screen
					jQuery('#maskNotification').fadeIn("fast").fadeTo("fast",0.8);   //transition effect
					//jQuery("#modal_help").show();
					var id = "#modal_help";
					//Set the popup window to center
					jQuery(id).css('z-index', 9999);
					jQuery(id).fadeIn("fast");   //transition effect
				});

				//  Mask Hide
				jQuery("#maskNotification").click(function()	{
					jQuery("#sendEmail").show();
					jQuery(this).hide();
					jQuery('.on-contact-modal').hide();
				});

				//  Send E-Mail
				jQuery("#sendEmail").click(function()   {
					modalID = "#modal_help";
					jQuery("#modal_help").hide();

					<?php
					//global $serverName;

					//if ($serverName == "demo.on-track.ws" || $serverName == "localhost")	{
					?>
					if ((jQuery("#contact_from").val() != "") && (jQuery("#contact_subject").val() != "") && (jQuery("#contact_description").val() != ""))   {
						var from = jQuery("#contact_from").val();
						var subject = jQuery("#contact_subject").val();
						var desc = jQuery("#contact_description").val();

						var exp = /^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/;

						if (exp.test(from))	{
							jQuery.post("_ajax.php", {func: "sendEmail", from: from, subject: subject, desc: desc}, function(data){
								successMessage("Thank you for your feedback.<br><br>You will be contacted via email within the next business day.");
								jQuery("#contact_subject").val("");
								jQuery("#contact_description").val("");
								//jQuery("#modal_help").show();
							});
						} else
							errorMessage("Please enter a valid email address.");
					}
					else{
						errorMessage("Please fill out the entire form");
						//jQuery("#modal_help").show();
					}
					<?php
					/*}	else	{
					?>
					if ((jQuery("#contact_subject").val() != "") && (jQuery("#contact_description").val() != ""))   {
						jQuery.post("_ajax.php", {func: "sendEmail", subject: jQuery("#contact_subject").val(), desc: jQuery("#contact_description").val()}, function(data){
							successMessage("Thank you for your feedback.<br><br>You will be contacted via email within the next business day.");
							jQuery("#contact_subject").val("");
							jQuery("#contact_description").val("");
							//jQuery("#modal_help").show();
						});
					}
					else{
						errorMessage("Please fill out the entire form");
						//jQuery("#modal_help").show();
					}
					<?php
					}*/
					?>
				});

				//  Cancel E-Mail
				jQuery("#cancelEmail").click(function()   {
					jQuery("#maskNotification").hide();
					jQuery("#modal_help").hide();
					jQuery("#sendEmail").show();
					jQuery('.on-drop-menu').hide();
					jQuery('.on-contact-modal').hide();
					jQuery("#contact_subject").val("");
					jQuery("#contact_description").val("");
				});

				//  Add Extra Row(s) To Table(s)
				jQuery(".addRow").click(function()    {
					var tbl = "#"+jQuery(this).attr("href");
					var tblLstRow = "#"+jQuery(this).attr("href")+"LstRow";
					var tblCntr = "#"+jQuery(this).attr("href")+"Cntr";
					var tblColumns = "#"+jQuery(this).attr("href")+"Columns";

					var lstRow = jQuery(tbl + " tbody>tr:last");
					var counter = jQuery(tblCntr).val();
					var columns = jQuery(tblColumns).val();
					var n = lstRow.prev().clone(true);

					for (var i = 0; i < columns; i++)   {
						n.children().eq(i).children().each(function(j, v) {
							if (n.children().eq(i).children().eq(j).attr("id") != "")   {
								var field = tbl + "_" + i + "_" + counter;

								field = field.replace(/#/gi, "");

								n.children().eq(i).children().eq(j).attr("id", field);
								n.children().eq(i).children().eq(j).attr("name", field);
								n.children().eq(i).children().eq(j).attr("value", "");
							}
						});
					}

					n.insertBefore(jQuery(tblLstRow));
					counter++;

					jQuery(tblCntr).val(counter);

					windowPosition();
				});

				//  User Info
				jQuery("#userInfo").html("<i>USERNAME</i> <i>[EMAIL ADDRESS]</i>");

				userInfoLOAD = function() {
					var name = jQuery("#frstname").val();;
					var surname = jQuery("#lstname").val();;
					var email = jQuery("#email").val();;

					var userInfo = "";

					if (name == "" || surname == "")    userInfo = "<i>USERNAME</i>";
					else                                userInfo = surname + ", " + name;

					if (email == "")                    userInfo += " <i>[EMAIL ADDRESS]</i>";
					else                                userInfo += " <i>[" + email + "]</i>";

					jQuery("#userInfo").html(userInfo);
				}

				jQuery("#frstname, #lstname, #email").change(function()   {
					userInfoLOAD();
				});

				jQuery("#frstname, #lstname, #email").blur(function()   {
					userInfoLOAD();
				});

				//  Company Industry
				jQuery("#industry").val("Other");

				//  Company Currency Span
				jQuery("#currency").val("R");
				jQuery(".companyCurrency").text("R");

				jQuery("#currency").change(function()   {
					var currency = jQuery(this).val();
					jQuery(".companyCurrency").text(currency);
				});

				//  Project Types
				projectTypeLOAD = function() {
					jQuery("#projectType option").remove();

					jQuery.each(projectTypes, function(index, value) {
						jQuery("#projectType").append(jQuery("<option></option>").attr("value",value).text(value));
					});
				}
				projectTypeLOAD();

				jQuery(".addRow[href='step3Tbl']").click(function()   {
					var counter = jQuery("#step3TblCntr").val();

					for (var i = 0; i < counter; i++)   {
						var value = jQuery("#step3Tbl_0_"+i).val();

						if (value != "")        {
							if (jQuery.inArray(value, projectTypes) == -1)
								projectTypes.push(value);
						}
					}

					projectTypes.sort();
					projectTypeLOAD();
				});

				jQuery(".projectType").blur(function()   {
					var counter = jQuery("#step3TblCntr").val();

					for (var i = 0; i < counter; i++)   {
						var value = jQuery("#step3Tbl_0_"+i).val();

						if (value != "")        {
							if (jQuery.inArray(value, projectTypes) == -1)
								projectTypes.push(value);
						}
					}

					projectTypes.sort();
					projectTypeLOAD();
				});

				//  User Rates
				userRatesLOAD = function() {
					jQuery("#userRates option").remove();

					jQuery.each(userRates, function(index, value) {
						jQuery("#userRates").append(jQuery("<option></option>").attr("value",value).text(value));
					});
				}
				userRatesLOAD();

				jQuery(".addRow[href='step2Tbl']").click(function()   {
					var counter = jQuery("#step2TblCntr").val();

					for (var i = 0; i < counter; i++)   {
						var value = jQuery("#step2Tbl_0_"+i).val();

						if (value != "")        {
							jQuery("#step2Tbl_0_"+i).val(parseFloat(value).toFixed(2));

							if (jQuery.inArray(parseFloat(value).toFixed(2), userRates) == -1)
								userRates.push(parseFloat(value).toFixed(2));
						}
					}

					userRates.sort();
					userRatesLOAD();
				});

				jQuery(".userRates").blur(function()   {
					var counter = jQuery("#step2TblCntr").val();

					for (var i = 0; i < counter; i++)   {
						var value = jQuery("#step2Tbl_0_"+i).val();

						if (value != "")        {
							jQuery("#step2Tbl_0_"+i).val(parseFloat(value).toFixed(2));

							if (jQuery.inArray(parseFloat(value).toFixed(2), userRates) == -1)
								userRates.push(parseFloat(value).toFixed(2));
						}
					}

					userRates.sort();
					userRatesLOAD();
				});

				jQuery("#projectStatus").change(function()   {
					if (jQuery(this).val() == "proposal")       jQuery("#activeDiv").hide();
					else                                        jQuery("#activeDiv").show();

					windowPosition();
				});

				jQuery("#companyName").blur(function()   {
					checkCompany();
				});

				jQuery("#email").blur(function()   {
					checkUser();
				});
			});
		</script>
		<!-- END: JQUERRY -->
	</head>
	<body>
		<form action="" method="post" id="register" name="register">
			<div id="mask"></div>
			<div id="maskNotification"></div>
			<!----- BEGIN: BODY ----->
			<a  class="on-integ-ie" href="http://www.integrityengineering.co.za/" target="_blank"></a> <!----- ITE LOGO all browsers ----->
			<div class="on-log-con"><a  class="on-integ" href="http://www.integrityengineering.co.za/" target="_blank"></a> <!----- ITE LOGO all browsers ----->
				<div class="on-log-bg"></div>
				<div class="on-register">
					<!--  BEGIN: CONTACT ITE MODAL  -->
					<div id="modal_help" name="modal_help" class="on-contact-modal">
						<table>
							<tr>
								<td colspan="100%" style="text-align:center"><a>Help &amp; Support</a><br><br></td>
							</tr>
							<?php
								//global $serverName;

								//if ($serverName == "demo.on-track.ws" || $serverName == "localhost")	{
							?>
							<tr>
								<td class="on-description">From:</td>
								<td><input id="contact_from" name="contact_from" class="on-field" type="text" /></td>
							</tr>
							<?php
								//}
							?>
							<tr>
								<td class="on-description-mail">Subject:</td>
								<td><input id="contact_subject" name="contact_subject" class="on-field" type="text" /></td>
							</tr>
							<tr>
								<td class="on-description-mail">Desciption:</td>
								<td><textarea id="contact_description" name="contact_description" cols="50" rows="6" class="on-field maxLength" ></textarea></td>
							</tr>
							<tr>
								<td></td>
								<td>
									<input id="sendEmail" name="sendEmail" type="button" value="Send" />
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<input id="cancelEmail" name="cancelEmail" type="button" value="Cancel" />
								</td>
							</tr>
						</table>
					</div>
					<!--  END: CONTACT ITE MODAL  -->
					<!-- BEGIN: ERROR MODAL -->
					<div id="modal_error" name="modal_error" class="on-contact-modal">
						<table>
							<tr>
								<td colspan="100%" class="centerdata">
									<img src="images/logo.png" />
									<br/><br/>
									<strong style="color:orange;font-weight:bold;font-size:18px;font-variant:small-caps;">
										Request Unsuccessful
									</strong>
									<br/><br/>
									<table width="300px">
										<tr>
											<td>
												<center id="errorMessage" name="errorMessage" style="font-weight:bold;font-size:14px;color:#d2d2d2;">
												</center>
											</td>
										</tr>
									</table>
									<br/>
									<input id="closeError" name="closeError" type="button" value="Close" />
								</td>
							</tr>
						</table>
					</div>
					<!--  END: ERROR MODAL  -->
					<!-- BEGIN: SUCCESS MODAL -->
					<div id="modal_success" name="modal_success" class="on-contact-modal">
						<table>
							<tr>
								<td colspan="100%" class="centerdata">
									<img src="images/logo.png" />
									<br/><br/>
									<strong style="color:#028200;font-weight:bold;font-size:18px;font-variant:small-caps;">
										Request Successfully Submitted
									</strong>
									<br/><br/>
									<table width="300px">
										<tr>
											<td>
												<center id="successMessage" name="successMessage" style="font-weight:bold;font-size:14px;color:#d2d2d2;">
												</center>
											</td>
										</tr>
									</table>
									<br/>
									<input id="closeSuccess" name="closeSuccess" type="button" value="Close" />
								</td>
							</tr>
						</table>
					</div>
					<!--  END: SUCCESS MODAL  -->
				</div>
			</div>
			<!--  BEGIN: ON-TRACK SETUP WIZARD MODAL  -->
			<div id="modal_wizard" name="modal_wizard" class="on-wizard-modal">
				<!--  BEGIN: HELP!  -->
				<div class="on-help"><a class="on-help-32" title="Help!"></a></div>
				<div class="on-help-menu on-help-menu32">
					<div id="help" name="help">
					</div>
				</div>
				<!--  END: HELP!  -->
				<table width="100%">
					<tr>
						<td align="right" width="20%">
							<img width="140px" src="images/logo.png" />
						</td>
						<td align="center" width="80%">
							<strong style="color:#E6E6E6;font-weight:bold;font-size:18px;font-variant:small-caps;">
								On-Track Setup Wizard
							</strong>
						</td>
					</tr>
					<tr>
						<td align="center"><br/></td>
						<td align="center" rowspan="2" valign="top">
							<div id="step1" name="step1" class="hidden" style="width:100%">
								<?php include("registration/step1.php"); ?>
							</div>
							<div id="step2" name="step2" class="hidden" style="width:100%">
								<?php include("registration/step2.php"); ?>
							</div>
							<div id="step3" name="step3" class="hidden" style="width:100%">
								<?php include("registration/step3.php"); ?>
							</div>
							<div id="step4" name="step4" class="hidden" style="width:100%">
								<?php include("registration/step4.php"); ?>
							</div>
							<div id="step5" name="step5" class="hidden" style="width:100%">
								<?php include("registration/step5.php"); ?>
							</div>
							<div id="step6" name="step6" class="hidden" style="width:100%">
								<?php include("registration/step6.php"); ?>
							</div>
							<div id="step7" name="step7" class="hidden" style="width:100%">
								<?php include("registration/step7.php"); ?>
							</div>
						</td>
					</tr>
					<tr>
						<td align="left">
							<ul id="nav">
								<li class="step1"><a class="on-but"><span><div class="on-but-pad">Step 1: Company Information</div></span></a></li>
								<li class="step2"><a class="on-but"><span><div class="on-but-pad">Step 2: Personal Information</div></span></a></li>
								<li class="step3"><a class="on-but"><span><div class="on-but-pad">Step 3: Project Types</div></span></a></li>
								<li class="step4"><a class="on-but"><span><div class="on-but-pad">Step 4: Activity Types & Activities</div></span></a></li>
								<li class="step5"><a class="on-but"><span><div class="on-but-pad">Step 5: Vehicle(s)</div></span></a></li>
								<li class="step6"><a class="on-but"><span><div class="on-but-pad">Step 6: Project Information</div></span></a></li>
								<li class="step7"><a class="on-but"><span><div class="on-but-pad">Step 7: What Now?</div></span></a></li>
							</ul>
						</td>
					</tr>
				<tr>
					<td colspan="100%">
						<a class="on-but" id="finishBtn" name="finishBtn" title="Finish" tabindex="<?php echo $index; ?>"><span><div class="on-but-pad">Finish</div></span></a>
						<a class="on-but" id="nextBtn" name="nextBtn" title="Next" tabindex="<?php echo $index; ?>"><span><div class="on-but-pad">Next</div></span></a>
						<a class="on-but" id="skipBtn" name="skipBtn" title="Skip" tabindex="<?php echo $index; ?>"><span><div class="on-but-pad">Skip</div></span></a>
						<a class="on-but" id="backBtn" name="backBtn" title="Back" tabindex="<?php echo $index; ?>"><span><div class="on-but-pad">Back</div></span></a>
					</td>
				</tr>
			</table>
		</div>
		<!--  END: ON-TRACK SETUP WIZARD MODAL  -->
	</form>
</body>
<!----- END: BODY ----->
</html>
