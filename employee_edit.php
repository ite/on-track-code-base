<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

	if (!hasAccess("EMP_EDIT") && !hasAccess("EMP_CTC_MANAGE") && !hasAccess("EMP_TARIFF_MANAGE") && !hasAccess("EMP_ADD_REMOVE"))
		header("Location: noaccess.php");
	if (hasAccess("EMP_CTC_MANAGE"))
		$RA_CTC = 1;
	if (hasAccess("EMP_TARIFF_MANAGE"))
		$RA_TAR = 1;
	if (hasAccess("EMP_ADD_REMOVE"))
		$RA_ADD = 1;
	if (hasAccess("EMP_EDIT"))
		$RA_EDIT = 1;


    //  Update Employee Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")
    {
        $errorMessage                                                   = "";

        //  Get Information
        $id                                                             = $_GET["id"];
        $last_name                                                      = addslashes(strip_tags($_POST["last_name"]));
        $first_name                                                     = addslashes(strip_tags($_POST["first_name"]));
        $email_address                                                  = addslashes(strip_tags($_POST["email_address"]));
        $usertype                                                       = $_POST["usertype"];
        $company_id                                                     = $_POST["company"];

        //  Get Employee Rates
        $ratesCounter = addslashes(strip_tags($_POST["empRatesCntr"]));
        unset($rates);

        $rC = 0;

        for ($r = 0; $r <= $ratesCounter; $r++) {
            $rateID = addslashes(strip_tags($_POST["rateID".$r]));
            $rateValue = addslashes(strip_tags($_POST["rateV".$r]));

            if (isset($_POST["rateA".$r]))      $rateActive = 1;
            else                                $rateActive = 0;

            $rateValue = number_format((double)$rateValue,2,".","");

            if (is_numeric($rateValue) && $rateValue > 0)       {
                $rates[$rC][0] = $rateID;
                $rates[$rC][1] = $rateValue;
                $rates[$rC][2] = $rateActive;
                $rC++;
            }
        }

        if (is_array($rates))   $rates = sortArrayByColumn($rates, 1);

        $cost_rate                                                      = addslashes(strip_tags($_POST["cost_rate"]));
        $target                                                         = addslashes(strip_tags($_POST["target"]));

        //  Check If Employee Exists In Database
        $exist                                                          = q("SELECT id FROM Employee WHERE email = '$email_address' AND id != '$id'");

        if (!$exist)
        {
            $primaryCompany = q("SELECT company_id FROM Employee WHERE id = '$id'");
            $update = "UPDATE Employee SET lstname = '$last_name', frstname = '$first_name', email = '$email_address' ";

            if ($RA_ADD && $primaryCompany == $_SESSION["company_id"])  $update .= ", company_id = '$company_id' ";
            if ($RA_CTC && $primaryCompany == $_SESSION["company_id"])  $update .= ", cost = '".number_format($cost_rate, 2, ".", "")."'";
            if ($RA_CTC && $primaryCompany == $_SESSION["company_id"])  $update .= ", target = '".number_format($target, 2, ".", "")."'";

            $update .= "WHERE id = '$id'";

            $update = q($update);

            if ($RA_TAR)    {
                //  Insert User Rates Into Table
                if (is_array($rates))   {
                    foreach ($rates as $r)      {
                        if ($r[0] != "")
                            $update = q("UPDATE user_rates SET rate = '".$r[1]."', active = '".$r[2]."' WHERE id = '".$r[0]."'");
                        else        {
                            if (!exist("user_rates", "companyid = '".$_SESSION["company_id"]."' AND userid = '".$id."' AND rate = '".$r[1]."'"))
                                $insert = q("INSERT INTO user_rates(companyid,userid,rate) VALUES (".$_SESSION["company_id"].",".$id.",'".$r[1]."')");
                        }
                    }
                }
            }

            //  UPDATE User Leave Days In Table
            $leave_types = array("annual", "sick", "special", "study", "unpaid", "maternity", "family");

            if (is_array($leave_types))     {
                foreach ($leave_types as $lt)       {
                    $days = addslashes(strip_tags($_POST[$lt]));

                    $insert = q("UPDATE user_leave SET ".$lt." = '".$days."' WHERE companyid = '".$_SESSION["company_id"]."' AND userid = '".$id."'");
                }
            }


            if ($update)
            {
                $time                                                   = date("H:i:s");

                $logs                                                   = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                                                                            "VALUES ('$email_address updated', 'Update', 'Employee', '".$_SESSION["email"]."', ".
                                                                            "'$today', '$time', '".$_SESSION["company_id"]."')");

                header("Location: employees.php?userType=all");
            }
        }
        else
            $errorMessage                                               = "Employee Already Exists";
    }

    if ($errorMessage != "")
        echo "<p align='center' style='padding:0px;'><strong><font class='on-validate-error'>$errorMessage</font></strong></p>";

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0", "user_edit");
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    jQuery(function()    {
        jQuery("#addRow").click(function()    {
            var tbl = "#"+jQuery(this).attr("href");
            var tblCntr = "#"+jQuery(this).attr("href")+"Cntr";

            var rows = jQuery(tbl + " tbody>tr").length;
            var lastRow = jQuery(tbl + " tbody>tr:nth-child("+(rows-1)+")");
            var counter = jQuery(tblCntr).val();

            var n = lastRow.clone(true);

            n.children().eq(0).children().eq(0).attr("id", "rateID"+counter);
            n.children().eq(0).children().eq(0).attr("name", "rateID"+counter);
            n.children().eq(0).children().eq(0).attr("value", "");
            n.children().eq(0).children().eq(1).attr("id", "rateV"+counter);
            n.children().eq(0).children().eq(1).attr("name", "rateV"+counter);
            n.children().eq(0).children().eq(1).attr("value", "");
            n.children().eq(1).children().eq(0).attr("id", "rateA"+counter);
            n.children().eq(1).children().eq(0).attr("name", "rateA"+counter);
            n.children().eq(1).children().eq(0).attr("value", "");

            n.insertAfter(lastRow);
            counter++;

            jQuery(tblCntr).val(counter);
        });
    });

    function check()
    {
        var valid                                                       = 1;

        //  Check That Last Name Is Entered
        if (document.forms["employee_edit"].last_name.value == "")
        {
            ShowLayer("lastName", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("lastName", "none");

        //  Check That First Name Is Entered
        if (document.forms["employee_edit"].first_name.value == "")
        {
            ShowLayer("firstName", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("firstName", "none");

        //  Check That Email Address Is Entered
        if (document.forms["employee_edit"].email_address.value == "")
        {
            ShowLayer("emailAddress", "none");
            ShowLayer("emailAddress_empty", "block");
            valid                                                       = 0;
        }
        //  Check That Email Address Is Valid
        else
        {
            ShowLayer("emailAddress_empty", "none");

            if (!validation("email", document.forms["employee_edit"].email_address.value))
            {
                ShowLayer("emailAddress", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("emailAddress", "none");
        }

        //  Check That Cost Rate Is Entered
        if (document.forms["employee_edit"].costRate != null)	{
			if (document.forms["employee_edit"].cost_rate.value == "")
			{
				ShowLayer("costRate", "none");
				ShowLayer("costRate_empty", "block");
				valid                                                       = 0;
			}
			//  Check That Cost Rate Is Valid
			else
			{
				ShowLayer("costRate_empty", "none");

				if (!validation("currency", document.forms["employee_edit"].cost_rate.value))
				{
					ShowLayer("costRate", "block");
					valid                                                   = 0;
				}
				else
					ShowLayer("costRate", "none");
			}
		}

        if (document.forms["employee_edit"].target.value != "")
        {
            if (!validation("currency", document.forms["employee_edit"].target.value))
            {
                ShowLayer("targetDiv", "block");
                valid                                                   = 0;
            }
            else
                ShowLayer("targetDiv", "none");
        }
        else
            ShowLayer("targetDiv", "none");

        if (valid == 1) {
            jQuery(".rateCB").removeAttr("disabled");
            document.forms["employee_edit"].save.value                  = 1;
            document.forms["employee_edit"].submit();
        }
    }
</script>
<?php
    $id = $_GET["id"];
    $employee_info = q("SELECT lstname, frstname, email, email, company_id, cost, target FROM Employee WHERE id = '$id'");
    $rates = q("SELECT id,rate,active FROM user_rates WHERE companyid = '".$_SESSION["company_id"]."' AND userid = '".$id."'");
    $rates = sortArrayByColumn($rates, 1);

    //  nCompanies = Number of Companies
    $nCompanies = q("SELECT COUNT(id) FROM Company");
    $companies = q("SELECT id, name, locked FROM Company ORDER BY name");
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="employee_edit">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>Edit Employee</h6>
                            </td>
                        </tr>
                        <tr>
                            <td class="centerdata">
                                <br/>
                            </td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                    Last Name:
                            </td>
                            <td  width="50%">
                                <?php
                                    $readonly = ($employee_info[0][4] != $_SESSION["company_id"]) ? "readonly " : "";
                                    echo "<input class='on-field' name='last_name' tabindex='1' type='text' value='".$employee_info[0][0]."' $readonly>";
                                ?>
                                <div id="lastName" style="display: none;"><font class="on-validate-error">* Last name must be entered</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                    First Name:
                            </td>
                            <td  width="50%">
                                <?php
                                    $readonly = ($employee_info[0][4] != $_SESSION["company_id"]) ? "readonly " : "";
                                    echo "<input class='on-field' name='first_name' tabindex='1' type='text' value='".$employee_info[0][1]."' $readonly>";
                                ?>
                                <div id="firstName" style="display: none;"><font class="on-validate-error">* First name must be entered</font></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="on-description" width="50%">
                                    Email Address:
                            </td>
                            <td  width="50%">
                                <?php
                                    $readonly = ($employee_info[0][4] != $_SESSION["company_id"]) ? "readonly " : "";
                                    if ($RA_EDIT)
                                        echo "<input class='on-field' name='email_address' tabindex='1' type='text' value='".$employee_info[0][2]."' $readonly>";
                                    else
                                        echo "<input class='on-field' name='email_address' readonly type='text' value='".$employee_info[0][2]."'>";
                                ?>
                                <div id="emailAddress_empty" style="display: none;"><font class="on-validate-error">* Email address must be entered</font></div>
                                <div id="emailAddress" style="display: none;"><font class="on-validate-error">* Invalid email address, eg. someone@something.com</font></div>
                            </td>
                        </tr>
                        <?php
                            if ($RA_ADD && $employee_info[0][4] == $_SESSION["company_id"])
                            {
                                echo "<tr>
                                            <td class='on-description' width='50%'>Primary Company:</td>
                                            <td align='left' width='50%'>
                                            <select class='on-field' method='post' name='company' tabindex='1'>";

                                            if ($_SESSION["email"] == "admin")	{
                                                if (is_array($companies))
                                                    foreach ($companies as $company)	{
                                                        if ($employee_info[0][4] == $company[0])
                                                            echo "<option value='".$company[0]."' selected>".$company[1]."&nbsp;&nbsp;</option>";
                                                        else
                                                            echo "<option value='".$company[0]."'>".$company[1]."&nbsp;&nbsp;</option>";
                                                    }
                                            }
                                            else	{
                                                if (is_array($companies))
                                                    foreach ($companies as $company)
                                                        if ($employee_info[0][4] == $company[0])
                                                            echo "<option value='".$company[0]."' selected>".$company[1]."&nbsp;&nbsp;</option>";
                                            }

                                        echo "</select>";
                                    echo "</td>";
                                echo "</tr>";
                            }
                            //else
                            //{
                            //    echo "<input name='usertype' type='hidden' value='".$employee_info[0][3]."'>";
                            //    echo "<input name='company' type='hidden' value='".$employee_info[0][4]."'>";
                            //}
                            if ($RA_CTC && $employee_info[0][4] == $_SESSION["company_id"])	{
                        ?>
                        <tr>
                            <td class="on-description" width="50%">
                                Cost to Company [<?php echo "".$_SESSION["currency"]; ?>/h]:
                            </td>
                            <td  width="50%">
                                <input class="on-field-date" name="cost_rate" style="text-align:right;" tabindex="1" type="text" value="<?php
                                    echo "".number_format((double)$employee_info[0][5], 2, ".", ""); ?>" <?php if ($employee_info[0][4] != $_SESSION["company_id"])
                                    echo "readonly";?>><font class="on-description-left">* Cost to Company = Gross salary / 30 days / 8 hours</font>
                                <div id="costRate_empty" style="display: none;"><font class="on-validate-error">* Cost rate must be entered</font></div>
                                <div id="costRate" style="display: none;"><font class="on-validate-error">* Entered amount must be numeric, eg. 123.45</font></div>
                            </td>
                        </tr>
                        <?php
                            }	//RA_CTC
                            else
                                echo "<input name='cost_rate' type='hidden' value='".number_format((double)$employee_info[0][5], 2, ".", "")."'>";

                            if ($RA_CTC && $employee_info[0][4] == $_SESSION["company_id"])	{
                        ?>
                        <tr>
                            <td class="on-description" width="50%">
                                Target [<?php echo "".$_SESSION["currency"]; ?>/month]:
                            </td>
                            <td align="left" width="50%">
                                <input class="on-field-date" name="target" style="text-align:right;" tabindex="1" type="text" value="<?php
                                    echo "".number_format((double)$employee_info[0][6], 2, ".", ""); ?>" <?php if ($employee_info[0][4] != $_SESSION["company_id"])
                                    echo "readonly";?>>
                                <div id="targetDiv" style="display: none;"><font class="on-validate-error">* Entered amount must be numeric, eg. 123.45</font></div>
                            </td>
                        </tr>
                        <?php
                            }	//RA_CTC
                            else	{
                                echo "<input name='target' type='hidden' value='".number_format((double)$employee_info[0][6], 2, ".", "")."'>";
								echo "<div id='targetDiv' style='display: none;'><font class='on-validate-error'></font></div>";
							}

                        ?>
                    </table>
                    <?php
                        if ($RA_TAR)        {
                    ?>
                    <br/><br/>
                    <table cellpadding="0" cellspacing="2" width="100%">
                        <tr>
                            <td class="centerdata" colspan="2">
                                <h6>Employee Charge-out Rates</h6>
                            </td>
                        </tr>
                        <tr>
                            <td class="centerdata" colspan="2">
                                <table id="empRates" name="empRates" class="on-table-center on-table">
                                    <tr>
                                        <th>Rate [<?php echo "".$_SESSION["currency"]; ?>/h]</th>
                                        <th>Active</th>
                                    </tr>
                                    <?php
                                        $counter = 0;

                                        if (is_array($rates))   {
                                            foreach ($rates as $r)      {
                                                $rateUsed = q("SELECT pu.* FROM (Project_User AS pu INNER JOIN Project AS p ON p.id = pu.project_id) ".
                                                                "WHERE pu.company_id = '".$_SESSION["company_id"]."' AND pu.user_id = '".$id."' AND pu.rateID = '".$r[0]."' AND p.completed = '0'");

                                                echo "<tr>";
                                                    echo "<td class='centerdata'>";
                                                        echo "<input id='rateID".$counter."' name='rateID".$counter."' type='hidden' tabindex='2' value='".$r[0]."' />";
                                                        echo "<input id='rateV".$counter."' name='rateV".$counter."' class='on-field-date' type='text' tabindex='1' value='".$r[1]."' />";
                                                    echo "</td>";
                                                    echo "<td class='centerdata' style='padding-top:12px;'>";
                                                        $checked = ($r[2] == "1") ? "checked " : "";
                                                        $disabled = (is_array($rateUsed)) ? "disabled " : "";
                                                        echo "<input id='rateA".$counter."' name='rateA".$counter."' type='checkbox' tabindex='2' value='1' class='rateCB' $checked $disabled/>";
                                                    echo "</td>";
                                                echo "</tr>";

                                                $counter++;
                                            }
                                        }
                                        else    {
                                            echo "<tr>";
                                                echo "<td class='centerdata'>";
                                                    echo "<input id='rateID".$counter."' name='rateID".$counter."' type='hidden' tabindex='2' value='' />";
                                                    echo "<input id='rateV".$counter."' name='rateV".$counter."' class='on-field-date' type='text' tabindex='1' value='' />";
                                                echo "</td>";
                                                echo "<td class='centerdata' style='padding-top:12px;'>";
                                                    echo "<input id='rateA".$counter."' name='rateA".$counter."' type='checkbox' tabindex='2' value='1' checked />";
                                                echo "</td>";
                                            echo "</tr>";

                                            $counter++;
                                        }
                                    ?>
                                    <tr><td colspan='100%' class='centerdata'><input style="margin-left:5px;" id="addRow" name="addRow" href="empRates" type="button" value="+" tabindex="2" /></td></tr>
                                    <tfoot><tr><td colspan='100%'></td></tr></tfoot>
                                </table>
                                <font class="on-validate-error">* Please note that rates associated with projects have been disabled for deactivation.</font>
                                <input id="empRatesCntr" name="empRatesCntr" type="hidden" value="<?php echo $counter; ?>" />
                            </td>
                        </tr>
                    </table>
                    <?php
                        }	//RA_TAR
                    ?>
                    <br/>
                    <?php
                        $leave_types = array("annual", "sick", "special", "study", "unpaid", "maternity", "family");

                        if (is_array($leave_types))     {
                    ?>
                    <table cellpadding="0" cellspacing="2" width="100%">
                        <tr>
                            <td class="centerdata" colspan="2">
                                <h6>Employee Leave</h6>
                            </td>
                        </tr>
                        <tr>
                            <td class="centerdata" colspan="2">
                                <table class="on-table-center on-table">
                                    <tr>
                                        <th>Leave Type</th>
                                        <th>Days Available (*)</th>
                                    </tr>
                                    <?php
                                        foreach ($leave_types as $lt)   {
                                            echo "<tr>";
                                                echo "<td class='leftdata'>";
                                                    switch ($lt)        {
                                                        case "annual":
                                                            echo "Annual";
                                                            break;
                                                        case "sick":
                                                            echo "Sick";
                                                            break;
                                                        case "special":
                                                            echo "Special";
                                                            break;
                                                        case "study":
                                                            echo "Study";
                                                            break;
                                                        case "unpaid":
                                                            echo "Unpaid";
                                                            break;
                                                        case "maternity":
                                                            echo "Maternity/Paternity";
                                                            break;
                                                         case "family":
                                                            echo "Family Responsibility";
                                                            break;
                                                       default:
                                                            break;
                                                    }
                                                echo "</td>";
                                                echo "<td class='centerdata'>";
                                                    $days = q("SELECT ".$lt." FROM user_leave WHERE companyid = '".$_SESSION["company_id"]."' AND userid = '".$id."'");
                                                    echo "<input id='".$lt."' name='".$lt."' class='on-field-date' style='text-align:right;' type='text' tabindex='3' value='".$days."' />";
                                                echo "</td>";
                                            echo "</tr>";
                                        }
                                    ?>
                                    <tfoot><tr><td colspan='100%'></td></tr></tfoot>
                                </table>
                                <font class="on-validate-error">* Please leave days available blank if no amount of days are specified.</font>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <?php
                        }
                    ?>
                    <input name="btnUpdate" onClick="check();" tabindex="3" type="button" value="Update Employee">
                    <input method="post" name="save" type="hidden" value="0" />
                </form>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
