<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("include/sajax.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");
        
    if (!hasAccess("REP_PROJ_BREAK"))
        header("Location: noaccess.php");

    // Class for table background color
    $colorClass = "OnTrack_#_";

    ///////////////////////////
    //  Sajax
    function getProjects($status, $type) {
        if ($status == "all")   $status = " AND p.completed IN (0,1)";
        else                    $status = " AND p.completed = '".$status."'";

        if ($type == "null")
            $projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p 
                            INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) 
                            WHERE cot.company_id = '".$_SESSION["company_id"]."' 
                            AND p.deleted = '0' $status ORDER BY UPPER(p.name)");
        else
            $projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p 
                            INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) 
                            WHERE cot.company_id = '".$_SESSION["company_id"]."' 
                            AND p.deleted = '0' 
                            AND p.type_id='$type' $status ORDER BY UPPER(p.name)");

        return $projects;
    }

    $sajax_request_type                                                 = "GET";
    sajax_init();
    sajax_export("getProjects");
    sajax_handle_client_request();
    ///////////////////////////

    //  Set Report Generated Status
    $generated                                                          = "0";

    //  Create Display Information Function
    if (isset($_POST["save"]) && $_POST["save"] === "1") {
        $errorMessage                                                   = "";

        $reportOn = $_POST["reportOn"];

        $project_id                                                     = $_POST["project"];
        //$area_id                                                          = $_POST["area"];
        $displayString                                                  = "";

        ///////////////////////////
        //  Get Info From Tables
        ///////////////////////////
        $project_name                                                   = q("SELECT name FROM Project WHERE id = '$project_id'");
        $project_budget                                                 = q("SELECT consulting FROM companiesOnTeam 
                                                                                            WHERE project_id = '$project_id' 
                                                                                            AND company_id = '".$_SESSION["company_id"]."' ");
        $employees                                                      = q("SELECT e.id, e.lstname, e.frstname, pu.user_budget FROM (Employee as e 
                                                                                        INNER JOIN Project_User as pu ON e.id = pu.user_id) 
                                                                                        WHERE pu.project_id = '$project_id' 
                                                                                        AND pu.company_id = '".$_SESSION["company_id"]."' 
                                                                                        AND e.deleted = '0' ORDER BY pu.manager DESC, e.lstname, e.frstname ASC");
        //$area_name                                                      = q("SELECT name FROM areas WHERE id = '$area_id' ");

        $total                                                              = 0;
        
        $row                                                               = 0;
        
        $excelheadings[$row][]                                     = "Report: Consulting Breakdown"; 
        $excelheadings[$row][]                                     = "";
        $excelheadings[$row][]                                     = "";
        $excelheadings[$row][]                                     = "";
        $excelheadings[$row][]                                     = "";
        $excelheadings[$row][]                                     = "";
        $excelheadings[$row][]                                     = "";
            $row++;
        
        $excelheadings[$row][]                                     = $colorClass ."Employee Name"; 
        $excelheadings[$row][]                                     = $colorClass ."Total Budget - Allocated/Person";
        $excelheadings[$row][]                                     = $colorClass ."Total Time - Allocated/Person"; 
        $excelheadings[$row][]                                     = $colorClass ."Total Budget - Used/Person"; 
        $excelheadings[$row][]                                     = $colorClass ."Total Time - Used/Person"; 
        $excelheadings[$row][]                                     = $colorClass ."Total Budget - Left/Person"; 
        $excelheadings[$row][]                                     = $colorClass ."Total Time - Left/Person"; 
            $row++;
            
        $exceldata[$row][]                                     = "Project: ".$project_name;
        $exceldata[$row][]                                     = "";
        $exceldata[$row][]                                     = "";
        $exceldata[$row][]                                     = "";
        $exceldata[$row][]                                     = "";
        $exceldata[$row][]                                     = "";
        $exceldata[$row][]                                     = "";
            $row++;

        ///////////////////////////
        //  Create Table Headers
        $headers                                                        = "<tr>
                                                                                        <th>Employee Name</th>
                                                                                        <th>Total Budget Allocated/Person <i>(".$_SESSION["currency"].")</i></th>
                                                                                        <th>Total Time<br/>Allocated/Person</th>
                                                                                        <th>Total Budget<br/>Used/Person <i>(".$_SESSION["currency"].")</i></th>
                                                                                        <th>Total Time<br/>Used/Person</th>
                                                                                        <th>Total Budget<br/>Left/Person <i>(".$_SESSION["currency"].")</i></th>
                                                                                        <th>Total Time<br/>Left/Person</th>
                                                                                    </tr>";
                                                                                    
        $budgetAPP              = 0;
        $totalTimeAPP          = 0;
        $totalBudgetUPP       = 0;
        $totalTimeUPP          = 0;
        $totalBudgetLPP       = 0;
        $totalTimeLPP          = 0;

        $table = ($reportOn == "approved") ? "ApprovedTime" : "TimeSheet";
        $where = ($reportOn == "approved") ? "AND ts.status = '2' " : "";

        if (is_array($employees)) {
            foreach ($employees as $employee) {
                $budget                                                 = q("SELECT user_budget FROM Project_User 
                                                                                WHERE user_id = '".$employee[0]."' 
                                                                                AND project_id = '".$project_id."' 
                                                                                AND company_id = '".$_SESSION["company_id"]."' LIMIT 1");

	            $rate = q("SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID) WHERE pu.user_id = '".$employee[0]."' ".
                            "AND pu.company_id = '".$_SESSION["company_id"]."' AND pu.project_id = '".$project_id."' AND ur.companyid = '".$_SESSION["company_id"]."' AND ur.userid = '".$employee[0]."' LIMIT 1");

                //$tariff_id = q("SELECT user_tariff FROM Project_User WHERE user_id = '".$employee[0]."' AND project_id = '".$project_id."' AND company_id = '".$_SESSION["company_id"]."'");
                //$rate = q("SELECT tariff".$tariff_id." FROM Employee WHERE id = '".$employee[0]."'");
                $used                                                   = 0;

                //$available                                              = $budget;

                $time                                                   = q("SELECT SUM(ts.time_spent * ts.rate) FROM ($table AS ts 
                                                                                    INNER JOIN companiesOnTeam AS cot ON cot.project_id = ts.project_id) 
                                                                                    WHERE ts.user_id = '".$employee[0]."' 
                                                                                        AND cot.company_id = '".$_SESSION["company_id"]."'
                                                                                        AND ts.company_id = '".$_SESSION["company_id"]."'
                                                                                        AND cot.project_id = '".$project_id."' $where");
//                $expenses                                           = q("SELECT SUM(expense) FROM (ExpenseSheet AS es 
//                                                                                    INNER JOIN companiesOnTeam AS cot ON cot.project_id = es.project_id)
//                                                                                    WHERE es.user_id = '".$employee[0]."' 
//																					AND cot.company_id = '".$_SESSION["company_id"]."'
//																					AND es.company_id = '".$_SESSION["company_id"]."'
 //                                                                                   AND cot.project_id = '".$project_id."'");
//    echo "A: ".$available." - T: ".$time." - E: ".$expenses." - B: ".$budget ." - R: ".$rate;
                if( !$time ){
                    //$available = 0;
                    $used = 0;
                }else{
                    //$available -= ($time + $expenses);
                    $used += $time;
                }

                $displayString                                          .= "<tr>";
                    //  Employee Name
                    $displayString                                      .= "<td>".$employee[1]." ".$employee[2]."</td>";
                    //  Total Budget Allocated/Person
                    $displayString                                      .= "<td class='rightdata'>".number_format((double)$budget, 2, ".", "")."</td>";
                    //  Total Time Allocated/Person
                    $displayString                                      .= "<td class='rightdata'>".number_format((double)($budget / $rate), 2, ".", "")."</td>";
                    //  Total Budget Used/Person
                    $total                                              += number_format((double)($used), 2, ".", "");

                    $displayString                                      .= "<td class='rightdata'>".number_format((double)($used), 2, ".", "")."</td>";
                    //  Total Time Used/Person
                    $displayString                                      .= "<td class='rightdata'>".number_format((double)($used / $rate), 2, ".", "")."</td>";
                    //  Total Budget Left/Person
                    if($budget - $used >= 0)
                        $displayString                                      .= "<td class='rightdata'>".number_format((double)($budget - $used), 2, ".", "")."</td>";
                    else
                        $displayString                                      .= "<td class='rightdata' style='color:#FF5900'>".number_format((double)($budget - $used), 2, ".", "")."</td>";
                    //  Total Time Left/Person
                    if(($budget - $used) >= 0)
                        $displayString                                      .= "<td class='rightdata'>".number_format((double)(($budget - $used) / $rate), 2, ".", "")."</td>";
                    else
                        $displayString                                      .= "<td class='rightdata' style='color:#FF5900'>".number_format((double)(($budget - $used) / $rate), 2, ".", "")."</td>";
                $displayString                                          .= "</tr>";
                
                $exceldata[$row][]                                     = $employee[1]." ".$employee[2];
                $exceldata[$row][]                                     = $budget;
                $exceldata[$row][]                                     = ($budget / $rate);
                $exceldata[$row][]                                     = ($used);
                $exceldata[$row][]                                     = ($used / $rate);
                $exceldata[$row][]                                     = $budget - $used;
                $exceldata[$row][]                                     = (($budget - $used) / $rate);
                    $row++;
                    
                $budgetAPP += $budget;
                $totalTimeAPP += ($budget / $rate);
                $totalBudgetUPP += $used;
                $totalTimeUPP += ($used/$rate);
                $totalBudgetLPP += ($budget - $used);
                $totalTimeLPP += (($budget - $used) / $rate);
            }
            
                $displayString                                              .= "<tr>";
                $displayString                                                  .= "<td class='on-table-total'>Total:</td>";
                $displayString                                                  .= "<td class='on-table-total'>".number_format((double)$budgetAPP,2,".","")."</td>";
                $displayString                                                  .= "<td class='on-table-total'>".number_format((double)$totalTimeAPP,2,".","")."</td>";
                $displayString                                                  .= "<td class='on-table-total'>".number_format((double)$totalBudgetUPP,2,".","")."</td>";
                $displayString                                                  .= "<td class='on-table-total'>".number_format((double)$totalTimeUPP,2,".","")."</td>";
                $displayString                                                  .= "<td class='on-table-total'>".number_format((double)$totalBudgetLPP,2,".","")."</td>";
                $displayString                                                  .= "<td class='on-table-total'>".number_format((double)$totalTimeLPP,2,".","")."</td>";
                $displayString                                              .= "</tr>";  
                
                $exceldata[$row][]                                     = $colorClass."Total: ";
                $exceldata[$row][]                                     = $colorClass.$budgetAPP;
                $exceldata[$row][]                                     = $colorClass.$totalTimeAPP;
                $exceldata[$row][]                                     = $colorClass.$totalBudgetUPP;
                $exceldata[$row][]                                     = $colorClass.$totalTimeUPP;
                $exceldata[$row][]                                     = $colorClass.$totalBudgetLPP;
                $exceldata[$row][]                                     = $colorClass.$totalTimeLPP;
                    $row++;
                
                $displayString                                              .= "<tr>
                                                                                            <td style='background-color:#242220' colspan='100%'>&nbsp;</td>
                                                                                        </tr>";


            //  Total Project Budget
            $displayString                                              .= "<tr>
                                                                                            <td class='on-table-total' colspan='3'>Total Project Budget <i>(".$_SESSION["currency"].")</i>:</td>
                                                                                            <td class='on-table-total'>".number_format((double)$project_budget, 2, ".", "")."</td>
                                                                                            <td class='on-table-total' colspan='3'></td>
                                                                                    </tr>";
            //  Total Budget Left
            $displayString                                              .= "<tr>
                                                                                            <td class='on-table-total' colspan='3'>Total Budget Left <i>(".$_SESSION["currency"].")</i>:</td>
                                                                                            <td class='on-table-total'>".number_format((double)($project_budget - $total), 2, ".", "")."</td>
                                                                                            <td class='on-table-total' colspan='3'></td>
                                                                                    </tr>";
                                                                            
            $exceldata[$row][]                                     = $colorClass."";
            $exceldata[$row][]                                     = $colorClass."";
            $exceldata[$row][]                                     = $colorClass."Total Project Budget (".$_SESSION["currency"]."):";
            $exceldata[$row][]                                     = $colorClass.number_format((double)$project_budget, 2, ".", "");
            $exceldata[$row][]                                     = $colorClass."";
            $exceldata[$row][]                                     = $colorClass."";
            $exceldata[$row][]                                     = $colorClass."";
                $row++;
                
            $exceldata[$row][]                                     = $colorClass. "";
            $exceldata[$row][]                                     = $colorClass. "";
            $exceldata[$row][]                                     = $colorClass."Total Budget Left (".$_SESSION["currency"]."):";
            $exceldata[$row][]                                     = $colorClass.number_format((double)($project_budget - $total), 2, ".", "");
            $exceldata[$row][]                                     = $colorClass."";
            $exceldata[$row][]                                     = $colorClass."";
            $exceldata[$row][]                                     = $colorClass. "";
        }

        if ($displayString != "") {
            $displayString                                              = "<tr>
                                                                                        <td class='on-table-clear' colspan='7'><a>Consulting Breakdown Report</a></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class='on-table-clear' colspan='7'>Project: ".$project_name."</td>
                                                                                    </tr>".$headers.$displayString;

            $generated                                                  = "1";
        }
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("3", "reports");
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    function check()
    {
        var valid                                                       = 1;

        //  Check That Employee Is Selected
        if (document.forms["report_summary"].project.value == "null")
        {
            ShowLayer("projectDiv", "block");
            valid                                                       = 0;
        }
        else
            ShowLayer("projectDiv", "none");

        if (valid == 1)
        {
            document.forms["report_summary"].save.value                 = 1;
            document.forms["report_summary"].submit();
        }
    }

    ///////////////////////////
    //  Sajax
    <?php sajax_show_javascript(); ?>

    function ClearOptions(OptionList) {
        for (x = OptionList.length; x >= 0; x--) {
            OptionList[x] = null;
        }
    }

    function setProjects(data) {
        var c = 0;

        document.forms["report_summary"].project.options[(c++)] = new Option("--  Select A Project  --", "null");

        for (var i in data)     {
            if (""+data[i][1] != "undefined")
                eval("document.forms['report_summary'].project.options["+(c++)+"] = new Option('" + data[i][1] + "', '" + data[i][0] + " ');");
        }
    }

    function getProjects(select1,select2) {
        var status = eval("document.report_summary."+select1+".value;");
        var type = eval("document.report_summary."+select2+".value;");;

        ClearOptions(document.report_summary.project);
        x_getProjects(status, type, setProjects);
    }
    ///////////////////////////
</script>
<?php
    $projectTypes = q("SELECT id, type FROM ProjectTypes WHERE company_id = '".$_SESSION["company_id"]."' ORDER BY type");
    $projectTypes[] = array("0","Shared Projects");
    $projectTypes = sortArrayByColumn($projectTypes, 1);
    
	$projects = q("SELECT DISTINCT(p.id), p.name FROM (Project AS p INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) ".
		"WHERE cot.company_id = '".$_SESSION["company_id"]."' AND p.completed = '0' AND p.deleted = '0' ORDER BY UPPER(p.name)");

?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="report_summary">
                    <div id="" style="<?php if ($generated == "1") echo "display: none;"; else echo "display: block"; ?>">
                        <table width="100%">
                            <tr>
                                <td class="centerdata">
                                    <h6>Consulting Breakdown Report</h6>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Report On:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="reportOn">
                                        <option value="normal">Actual Time</option>
                                        <option value="approved">Approved Time</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Project Status:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="projectStatus" onChange="getProjects('projectStatus','projectType');">
                                        <option value="all">Completed & Uncompleted</option>
                                        <option value="0" selected>Uncompleted</option>
                                        <option value="1">Completed</option>
                                    </select>
                                </td>
                            </tr>
                            <tr><td colspan="100%"><br/></td></tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Project Type:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="projectType" onChange="getProjects('projectStatus','projectType');">
                                        <option value="null">All Project Types</option>
                                        <?php
                                            if (is_array($projectTypes))
                                                foreach ($projectTypes as $projectType)
                                                    if ($_POST["projectType"] == $projectType[0])
                                                        echo "<option value='".$projectType[0]."' selected>".$projectType[1]."</option>";
                                                    else
                                                        echo "<option value='".$projectType[0]."'>".$projectType[1]."</option>";
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Project:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" id="project" name="project">
                                        <option value="null">--  Select A Project  --</option>
                                        <?php
                                            if (is_array($projects))
                                                foreach ($projects as $project)
                                                    if ($_POST["project"] == $project[0])
                                                        echo "<option value='".$project[0]."' selected>".$project[1]."</option>";
                                                    else
                                                        echo "<option value='".$project[0]."'>".$project[1]."</option>";
                                        ?>
                                    </select>
                                    <div id="projectDiv" style="display: none;"><font class="on-validate-error">* Project must be selected</font></div>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <input name="btnGenerate" onClick="check();" type="button" value="Generate Report">
                        <input method="post" name="save" type="hidden" value="0" />
                    </div>
                </form>
                <div id="report" style="<?php if ($generated == "1") echo "display: block;"; else echo "display: none"; ?>">
                    <?php
                        echo "<div class='on-20px'><table class='on-table-center on-table'>";
                            echo "".$displayString;
                             echo  "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        echo "</table></div>";
                        echo "<br/>";

                        ///////////////////////////
                        //  Set Export Information
                        $_SESSION["fileName"] = "Consulting Breakdown Report";
                        $_SESSION["fileData"] = array_merge($excelheadings, $exceldata);
                        ///////////////////////////

                        echo "<input name='btnExport' onClick=\"location.href='_export.php'\" type='button' value='Export to Excel' />";
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
