<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!$_SESSION["eula_agree"] === true)
        header("Location: logout.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    if (!hasAccess("LEAVE_MANAGE_OWN"))
        header("Location: noaccess.php");

    //  Insert Booked Leave Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")        {
        $errorMessage = "";

        $do_insert = false;
        $year_start = date("Y-01-01");
        $year_end = date("Y-12-31");

        //  Get Information
        $leave_type = $_POST["leave"];
        $day = $_POST["day"];

        if ($day == "full")     {
            $date_from = addslashes(strip_tags($_POST["fullDateFrom"]));
            $date_to = addslashes(strip_tags($_POST["fullDateTo"]));
        } else  {
            $date_from = addslashes(strip_tags($_POST["halfDayDate"]));
            $date_to = addslashes(strip_tags($_POST["halfDayDate"]));
        }

        $leave_booked = businessdays($date_from, $date_to);

        if ($day == "half")     $leave_booked = 0.5;

        $public_holidays = q("SELECT COUNT(id) FROM publicHolidays WHERE date BETWEEN '".$date_from."' AND '".$date_to."'");

        $leave_booked -= $public_holidays;

        $do_insert = false;

        $available = q("SELECT ".$leave_type." FROM user_leave WHERE companyid = '".$_SESSION["company_id"]."' AND userid = '".$_SESSION["user_id"]."'");
        $leave_taken = q("SELECT SUM(days_leave) FROM LeaveBooked WHERE company_id = '".$_SESSION["company_id"]."' AND user_id = '".$_SESSION["user_id"]."' AND start_date >= '$year_start' AND end_date <= '$year_end' ".
                            "AND type = '$leave_type'");

        if (!($available != "" && $available > 0))      $do_insert = true;
        else                                            $available -= $leave_taken;

        if ($do_insert == false)        {
            if ($available >= $leave_booked)    $do_insert = true;
            else                                $errorMessage = "You Have Exceeded The Amount Of Days Available";
        }

        if ($do_insert) {
            $insert = q("INSERT INTO LeaveBooked (company_id, user_id, type, days_leave, start_date, end_date, on_date) ".
                        "VALUES ('".$_SESSION["company_id"]."', '".$_SESSION["user_id"]."', '$leave_type', '$leave_booked', '$date_from', '$date_to', '$today')");

            if ($insert)        {
                //  Employee Details
                $employee_info = q("SELECT frstname, lstname FROM Employee WHERE id = '".$_SESSION["user_id"]."'");

                $time = date("H:i:s");

                if ($leave_type == "maternity")
                    $message = "".$employee_info[0][0]." ".$employee_info[0][1]." booked maternity/paternity leave for ".$leave_booked." days.\n\nFrom:\t".$date_from."\nTo:\t".$date_to;
                else
                    $message = "".$employee_info[0][0]." ".$employee_info[0][1]." booked ".$leave_type." leave for ".$leave_booked." days.\n\nFrom:\t".$date_from."\nTo:\t".$date_to;

                $notify_id = q("SELECT id FROM Notify WHERE code = 'Leave'");
                $events = q("INSERT INTO Events (date, time, message, notify_id, processed, user_id, company_id) ".
                            "VALUES ('".$today."', '".$time."', '$message', '$notify_id', '0', '".$_SESSION["user_id"]."', '".$_SESSION["company_id"]."')");

                $logs = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id) ".
                            "VALUES ('".$_SESSION["email"]." booked leave', 'Create', 'LeaveBooked', '".$_SESSION["email"]."', '$today', '$time', '".$_SESSION["company_id"]."')");

                header("Location: book_leave.php");
            }
        }
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0", "bookings", "book_leave");

    if ($errorMessage != "")
    {
        echo "<p style='text-align:center' style='padding:0px;'><font color='orange'> - $errorMessage - </font></p>";
        echo "<br/>";
    }
?>
<link rel="stylesheet" media="screen" type="text/css" href="include/datepicker.css" />
<script type="text/javascript" src="include/datepicker.js"></script>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    //  Calendar
    var day = "full";

    jQuery(function(){
	jQuery('#fullDateFrom').DatePicker({
		format:'Y-m-d',
		date: jQuery('#fullDateFrom').val(),
		current: jQuery('#fullDateFrom').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#fullDateFrom').val()))
			_date = new Date();
			else _date = jQuery('#fullDateFrom').val();
			jQuery('#fullDateFrom').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#fullDateFrom').val(formated);
			jQuery('#fullDateFrom').DatePickerHide();
		}
	});

	jQuery('#fullDateTo').DatePicker({
		format:'Y-m-d',
		date: jQuery('#fullDateTo').val(),
		current: jQuery('#fullDateTo').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#fullDateTo').val()))
			_date = new Date();
			else _date = jQuery('#fullDateTo').val();
			jQuery('#fullDateTo').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#fullDateTo').val(formated);
			jQuery('#fullDateTo').DatePickerHide();
		}
	});

	jQuery('#halfDayDate').DatePicker({
		format:'Y-m-d',
		date: jQuery('#halfDayDate').val(),
		current: jQuery('#halfDayDate').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			var _date;
			if (!validation('date',jQuery('#halfDayDate').val()))
			_date = new Date();
			else _date = jQuery('#halfDayDate').val();
			jQuery('#date_to').DatePickerSetDate(_date, true);
		},
		onChange: function(formated, dates){
			jQuery('#halfDayDate').val(formated);
			jQuery('#halfDayDate').DatePickerHide();
		}
	});

        jQuery("input[id='day']").change(function(){
            day = jQuery(this).val();

            if (day == "full")  {
                jQuery('#fullDayDiv').show();
                jQuery('#halfDayDiv').hide();
            } else      {
                jQuery('#halfDayDiv').show();
                jQuery('#fullDayDiv').hide();
            }
        });
    });

    function check()    {
        var valid = 1;

        if (day == "full")   {
            //  Check That Date From Is Entered
            if (document.forms["book_leave"].fullDateFrom.value == "")
            {
                ShowLayer("fullDateFromEmpty", "block");
                valid = 0;
            }
            //  Check That Entered Date From Is Valid
            else
            {
                ShowLayer("fullDateFromEmpty", "none");

                if (!validation("date", document.forms["book_leave"].fullDateFrom.value))
                {
                    ShowLayer("fullDateFromDiv", "block");
                    valid = 0;
                }
                else
                    ShowLayer("fullDateFromDiv", "none");
            }

            //  Check That Date To Is Entered
            if (document.forms["book_leave"].fullDateTo.value == "")
            {
                ShowLayer("fullDateToEmpty", "block");
                valid = 0;
            }
            //  Check That Entered Date To Is Valid
            else
            {
                ShowLayer("fullDateToEmpty", "none");

                if (!validation("date", document.forms["book_leave"].fullDateTo.value))
                {
                    ShowLayer("fullDateToDiv", "block");
                    valid = 0;
                }
                else
                    ShowLayer("fullDateToDiv", "none");
            }

            //  Check That Date To Is Later Than Date From
            if (document.forms["book_leave"].fullDateFrom.value > document.forms["book_leave"].fullDateTo.value)
            {
                ShowLayer("dateDiv", "block");
                valid = 0;
            }
            else
                ShowLayer("dateDiv", "none");
        }
        else if (day == "half")   {
            //  Check That Date To Is Entered
            if (document.forms["book_leave"].halfDayDate.value == "")
            {
                ShowLayer("halfDayDateEmpty", "block");
                valid = 0;
            }
            //  Check That Entered Date To Is Valid
            else
            {
                ShowLayer("halfDayDateEmpty", "none");

                if (!validation("date", document.forms["book_leave"].halfDayDate.value))
                {
                    ShowLayer("halfDayDateDiv", "block");
                    valid = 0;
                }
                else
                    ShowLayer("halfDayDateDiv", "none");
            }
        }

        if (valid == 1) {
            document.forms["book_leave"].save.value = 1;
            document.forms["book_leave"].submit();
        }
    }
</script>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata">
                <form action="" method="post" name="book_leave">
                    <table width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>
                                    Book Leave
                                </h6>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                    Type of Leave:
                            </td>
                            <td width="50%">
                                <?php
                                    echo "&nbsp;<input method='post' name='leave' tabindex='1' type='radio' value='annual' checked>&nbsp;<a>Annual Leave</a><br/>";
                                    echo "&nbsp;<input method='post' name='leave' tabindex='1' type='radio' value='sick'>&nbsp;<a>Sick Leave</a><br/>";
                                    echo "&nbsp;<input method='post' name='leave' tabindex='1' type='radio' value='special'>&nbsp;<a>Special Leave</a><br/>";
                                    echo "&nbsp;<input method='post' name='leave' tabindex='1' type='radio' value='study'>&nbsp;<a>Study Leave</a><br/>";
                                    echo "&nbsp;<input method='post' name='leave' tabindex='1' type='radio' value='unpaid'>&nbsp;<a>Unpaid Leave</a><br/>";
                                    echo "&nbsp;<input method='post' name='leave' tabindex='1' type='radio' value='maternity'>&nbsp;<a>Maternity/Paternity Leave</a><br/>";
                                    echo "&nbsp;<input method='post' name='leave' tabindex='1' type='radio' value='family'>&nbsp;<a>Family Responsibility Leave</a><br/>";
                                ?>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <table width="100%">
                        <tr>
                            <td class="on-description" width="50%">
                                    Full/Half Day:
                            </td>
                            <td width="50%">
                                <?php
                                    echo "&nbsp;<input method='post' id='day' name='day' tabindex='1' type='radio' value='full' checked>&nbsp;<a>Full Day(s)</a><br/>";
                                    echo "&nbsp;<input method='post' id='day' name='day' tabindex='1' type='radio' value='half'>&nbsp;<a>Half Day</a><br/>";
                                ?>
                            </td>
                        </tr>
                    </table>
                    <div id="halfDayDiv" name="halfDayDiv" class="hidden">
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Date:
                                </td>
                                <td width="50%">
                                    <input class="on-field-date" id="halfDayDate"  name="halfDayDate" tabindex="3" type="text" style="text-align:right;" value="<?php
                                        if ($_POST["halfDayDate"] != "") echo "".$_POST["halfDayDate"]; else echo "".$today; ?>">
                                    <div id="halfDayDateEmpty" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                    <div id="halfDayDateDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="fullDayDiv" name="fullDayDiv" class="">
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Date From:
                                </td>
                                <td width="50%">
                                    <input class="on-field-date" id="fullDateFrom" name="fullDateFrom" tabindex="2" type="text" style="text-align:right;" value="<?php
                                            if ($_POST["fullDateFrom"] != "") echo "".$_POST["fullDateFrom"]; else echo "".$today; ?>">
                                    <div id="fullDateFromEmpty" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                    <div id="fullDateFromDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="on-description" width="50%">
                                    Date To:
                                </td>
                                <td width="50%">
                                    <input class="on-field-date" id="fullDateTo"  name="fullDateTo" tabindex="3" type="text" style="text-align:right;" value="<?php
                                        if ($_POST["date_to"] != "") echo "".$_POST["date_to"]; else echo "".$today; ?>">
                                    <div id="fullDateToEmpty" style="display: none;"><font class="on-validate-error">* Date must be entered</font></div>
                                    <div id="fullDateToDiv" style="display: none;"><font class="on-validate-error">* Date not valid, eg. <?php echo "".date("Y-m-d"); ?></font></div>
                                    <div id="dateDiv" style="display: none;"><font class="on-validate-error">* Date to should be equal or later than date from</font></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <input name="btnAdd" onClick="check();" tabindex="4" type="button" value="Book Leave">
                    <br/>
                    <input method="post" name="save" type="hidden" value="0" />
                </form>
                <?php
                    //  Leave Report
                    $leave_types = array("annual", "sick", "special", "study", "unpaid", "maternity", "family");
                    $year_start = date("Y-01-01");
                    $year_end = date("Y-12-31");

                    $headers = "<tr>
                                    <th width='25%'>Leave Type</th>
                                    <th width='25%'>Date From</th>
                                    <th width='25%'>Date To</th>
                                    <th width='25%'>Days Booked</th>
                                </tr>";
                    $display_string = "";

                    foreach ($leave_types as $leave_type)       {
                        $booked_leave = q("SELECT id, start_date, end_date, days_leave FROM LeaveBooked ".
                                            "WHERE company_id = '".$_SESSION["company_id"]."' AND user_id = '".$_SESSION["user_id"]."' AND type = '".$leave_type."' AND start_date >= '$year_start' AND end_date <= '$year_end' ".
                                            "ORDER BY start_date, end_date");

                        if (is_array($booked_leave))    {
                            $taken = 0;
                            $available = q("SELECT ".$leave_type." FROM user_leave WHERE companyid = '".$_SESSION["company_id"]."' AND userid = '".$_SESSION["user_id"]."'");

                            $string = "<tr>
                                        <td>";

                            switch ($leave_type)    {
                                case "annual":
                                    $string .= "Annual Leave";
                                    break;
                                case "sick":
                                    $string .= "Sick Leave";
                                    break;
                                case "special":
                                    $string .= "Special Leave";
                                    break;
                                case "study":
                                    $string .= "Study Leave";
                                    break;
                                case "unpaid":
                                    $string .= "Unpaid Leave";
                                    break;
                                case "maternity":
                                    $string .= "Maternity/Paternity Leave";
                                    break;
                                case "family":
                                    $string .= "Family Responsibility Leave";
                                    break;
                                default:
                                    break;
                            }

                            $string .= "</td><td>";

                            foreach ($booked_leave as $bl)      $string .= "".$bl[1]."<br/>";

                            $string .= "</td><td class='rightdata'>";

                            foreach ($booked_leave as $bl)      $string .= "".$bl[2]."<br/>";

                            $string .= "</td><td class='rightdata'>";

                            foreach ($booked_leave as $bl)      {
                                $taken += $bl[3];
                                $string .= "".$bl[3]."<br/>";
                            }

                            $msg = ($available != "" && $available > 0) ? "Days Available" : "Days Taken";
                            $days = ($available != "" && $available > 0) ? $available - $taken : $taken;

                            $string .= "</td>
                                        </tr>
                                        <tr>
                                            <td class='on-table-light' colspan='3'>$msg:</td>
                                            <td class='on-table-total'>".$days."</td>
                                        </tr>
                                        <tr>
                                            <td style='background-color:#211f1a' colspan='100%'>&nbsp;</td>
                                        </tr>";

                            $display_string .= $string;
                        } else  {
                            $available = q("SELECT ".$leave_type." FROM user_leave WHERE companyid = '".$_SESSION["company_id"]."' AND userid = '".$_SESSION["user_id"]."'");

                            $string = "<tr>
                                        <td>";

                            switch ($leave_type)    {
                                case "annual":
                                    $string .= "Annual Leave";
                                    break;
                                case "sick":
                                    $string .= "Sick Leave";
                                    break;
                                case "special":
                                    $string .= "Special Leave";
                                    break;
                                case "study":
                                    $string .= "Study Leave";
                                    break;
                                case "unpaid":
                                    $string .= "Unpaid Leave";
                                    break;
                                case "maternity":
                                    $string .= "Maternity/Paternity Leave";
                                    break;
                                case "family":
                                    $string .= "Family Responsibility Leave";
                                    break;
                                default:
                                    break;
                            }

                            $string .= "</td>
                                        <td class='on-table-light' colspan='2'>Available Days:</td>
                                        <td class='on-table-total'>".$available."</td>
                                         </tr>
                                         <tr><td style='background-color:#211f1a' colspan='100%'>&nbsp;</td></tr>";

                            if ($available != "" && $available > 0)     $display_string .= $string;
                        }
                    }

                    if ($display_string != "")
                        $display_string = $headers.$display_string;

                    echo "<br/>";
                    echo "<table class='on-table-center on-table' width='50%'>";
                        echo "".$display_string;
                        echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                    echo "</table>";
                ?>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>
