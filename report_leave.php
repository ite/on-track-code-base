<?php
    session_start();

    include("_db.php");
    include("graphics.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

    if (!isset($_SESSION["company_id"]))
        header("Location: home.php");

    if (!hasAccess("REP_LEAVE"))
        header("Location: noaccess.php");

    // Class for table background color
    $colorClass = "OnTrack_#_";

    //  Set Report Generated Status
    $generated = "0";

    $row = 0;
    $exceldata;

    function createString($id,$eName,$eSurname) {
        global $row;
        global $exceldata;
        global $colorClass;

        $leave_types = array("annual", "sick", "special", "study", "unpaid", "maternity", "family");
        $year_start = date("Y-01-01");
        $year_end = date("Y-12-31");
        $createdString = "";

        //  Add Leave Content For Report
        foreach ($leave_types as $leave_type)       {
            $booked_leave = q("SELECT id, start_date, end_date, days_leave FROM LeaveBooked ".
                                "WHERE company_id = '".$_SESSION["company_id"]."' AND user_id = '".$id."' AND type = '".$leave_type."' AND start_date >= '$year_start' AND end_date <= '$year_end' ".
                                "ORDER BY start_date, end_date");

            if (is_array($booked_leave))    {
                $taken = 0;
                $available = q("SELECT ".$leave_type." FROM user_leave WHERE companyid = '".$_SESSION["company_id"]."' AND userid = '".$id."'");

                $string = "<tr>
                            <td>";

                switch ($leave_type)    {
                    case "annual":
                        $string .= "Annual Leave";
                        $exceldata[$row][] = "Annual Leave";
                        break;
                    case "sick":
                        $string .= "Sick Leave";
                        $exceldata[$row][] = "Sick Leave";
                        break;
                    case "special":
                        $string .= "Special Leave";
                        $exceldata[$row][] = "Special Leave";
                        break;
                    case "study":
                        $string .= "Study Leave";
                        $exceldata[$row][] = "Study Leave";
                        break;
                    case "unpaid":
                        $string .= "Unpaid Leave";
                        $exceldata[$row][] = "Unpaid Leave";
                        break;
                    case "maternity":
                        $string .= "Maternity/Paternity Leave";
                        $exceldata[$row][] = "Maternity/Paternity Leave";
                        break;
                    case "family":
                        $string .= "Family Responsibility Leave";
                        $exceldata[$row][] = "Family Responsibility Leave";
                        break;
                    default:
                        break;
                }

                $string .= "</td><td>";

                unset($tmpArr);

                foreach ($booked_leave as $bl)  {
                    $string .= "".$bl[1]."<br/>";
                    $tmpArr[] = $bl[1];
                }

                $exceldata[$row][] = $tmpArr;

                $string .= "</td><td class='rightdata'>";

                unset($tmpArr);

                foreach ($booked_leave as $bl)  {
                    $string .= "".$bl[2]."<br/>";
                    $tmpArr[] = $bl[2];
                }

                $exceldata[$row][] = $tmpArr;

                $string .= "</td><td class='rightdata'>";

                unset($tmpArr);

                foreach ($booked_leave as $bl)      {
                    $taken += $bl[3];
                    $string .= "".$bl[3]."<br/>";
                    $tmpArr[] = $bl[3];
                }

                $exceldata[$row++][] = $tmpArr;

                $msg = ($available != "" && $available > 0) ? "Days Available" : "Days Taken";
                $days = ($available != "" && $available > 0) ? $available - $taken : $taken;

                $string .= "</td>
                            </tr>
                            <tr>
                                <td class='on-table-light' colspan='3'>$msg:</td>
                                <td class='on-table-total'>".$days."</td>
                            </tr>
                            <tr>
                                <td colspan='100%'>&nbsp;</td>
                            </tr>";

                $exceldata[$row][] = "";
                $exceldata[$row][] = "";
                $exceldata[$row][] = "$msg:";
                $exceldata[$row++][] = $days;

                $createdString .= $string;
            } else  {
                $available = q("SELECT ".$leave_type." FROM user_leave WHERE companyid = '".$_SESSION["company_id"]."' AND userid = '".$id."'");

                if ($available != "" && $available > 0) {
                    $string = "<tr>
                                <td>";

                    switch ($leave_type)    {
                        case "annual":
                            $string .= "Annual Leave";
                            $exceldata[$row][] = "Annual Leave";
                            break;
                        case "sick":
                            $string .= "Sick Leave";
                            $exceldata[$row][] = "Sick Leave";
                            break;
                        case "special":
                            $string .= "Special Leave";
                            $exceldata[$row][] = "Special Leave";
                            break;
                        case "study":
                            $string .= "Study Leave";
                            $exceldata[$row][] = "Study Leave";
                            break;
                        case "unpaid":
                            $string .= "Unpaid Leave";
                            $exceldata[$row][] = "Unpaid Leave";
                            break;
                        case "maternity":
                            $string .= "Maternity/Paternity Leave";
                            $exceldata[$row][] = "Maternity/Paternity Leave";
                            break;
                        case "family":
                            $string .= "Family Responsibility Leave";
                            $exceldata[$row][] = "Family Responsibility Leave";
                            break;
                        default:
                            break;
                    }

                    $string .= "</td>
                                <td class='on-table-light' colspan='2'>Available Days:</td>
                                <td class='on-table-total'>".$available."</td>
                                </tr>
                                <tr><td colspan='100%'>&nbsp;</td></tr>";

                    $exceldata[$row][] = "";
                    $exceldata[$row][] = "Available Days:";
                    $exceldata[$row++][] = $available;

                    $createdString .= $string;
                }
            }
        }

        return $createdString;
    }

    //  Create Display Information Function
    if (isset($_POST["save"]) && $_POST["save"] === "1")        {
        $errorMessage = "";

        $employee = $_POST["employee"];
        $displayString = "";

        $report_heading = "<tr>
                                <td class='on-table-clear' colspan='100%'><a>Leave Report<a></td>
                            </tr><tr>
                                <td class='on-table-clear' colspan='4'>Company Name: ".$_SESSION["company_name"]."</td>
                            </tr>";
        $excelheadings[0][0] = "Leave Report";
        $excelheadings[0][1] = "";
        $excelheadings[0][2] = "";
        $excelheadings[0][3] = "";

        $excelheadings[0][0] = "Company Name: ".$_SESSION["company_name"];
        $excelheadings[0][1] = "";
        $excelheadings[0][2] = "";
        $excelheadings[0][3] = "";

        $exceldata[][] = "";

        if ($employee == "all") {
            $employees = q("SELECT e.id, e.frstname, e.lstname FROM (Employee AS e INNER JOIN Company_Users AS cu ON e.id = cu.user_id)
                            WHERE cu.company_id = '".$_SESSION["company_id"]."' AND e.email != 'admin' AND e.deleted = '0' ORDER BY e.lstname, frstname");

            if (is_array($employees))   {
                foreach ($employees as $employee)       {
                    $headers = "<tr>
                                    <td class='on-table-clear' colspan='100%'>Employee: ".$employee[1]." ".$employee[2]."</td>
                                </tr><tr>
                                    <th width='25%'>Leave Type</th>
                                    <th width='25%'>Date From</th>
                                    <th width='25%'>Date To</th>
                                    <th width='25%'>Days Booked</td>
                                </tr>";

                    $exceldata[$row][] = "Employee: ".$employee[1]." ".$employee[2];
                    $exceldata[$row][] = "";
                    $exceldata[$row][] = "";
                    $exceldata[$row++][] = "";

                    $exceldata[$row][] = "Leave Type";
                    $exceldata[$row][] = "Date From";
                    $exceldata[$row][] = "Date To";
                    $exceldata[$row++][] = "Days Booked";

                    $display_string = createString($employee[0],$employee[1],$employee[2]);

                    $displayString .= $headers.$display_string."<tr><td colspan='100%'></td></tr>";

                    $exceldata[$row][] = "";
                    $exceldata[$row][] = "";
                    $exceldata[$row][] = "";
                    $exceldata[$row++][] = "";
                }
            }
        } else  {
            $employee_details = q("SELECT frstname, lstname FROM Employee WHERE id = '$employee'");
            $headers = "<tr>
                            <td class='on-table-clear' colspan='100%'>Employee: ".$employee_details[0][0]." ".$employee_details[0][1]."</td>
                        </tr><tr>
                            <th width='25%'>Leave Type</th>
                            <th width='25%'>Date From</th>
                            <th width='25%'>Date To</th>
                            <th width='25%'>Days Booked</th>
                        </tr>";

            $exceldata[$row][] = "Employee: ".$employee_details[0][0]." ".$employee_details[0][1];
            $exceldata[$row][] = "";
            $exceldata[$row][] = "";
            $exceldata[$row++][] = "";

            $exceldata[$row][] = "Leave Type";
            $exceldata[$row][] = "Date From";
            $exceldata[$row][] = "Date To";
            $exceldata[$row++][] = "Days Booked";

            $display_string = createString($employee,$employee_details[0][0],$employee_details[0][1]);

            $displayString .= $headers.$display_string;
        }

        if ($displayString != "")
            $generated = "1";
    }

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("4", "reports");

    if ($errorMessage != "")
    {
        echo "<p align='center' style='padding:0px;'><strong><font class='on-validate-error'>$errorMessage</font></strong></p>";
        echo "<br/>";
    }
?>
<script language="JavaScript" src="include/validation.js"></script>
<script language="JavaScript">
    function check()    {
        var valid = 1;

        //  Check That Employee Is Selected
        if (document.forms["report_leave"].employee.value == "null")    {
            ShowLayer("employeeDiv", "block");
            valid = 0;
        }
        else
            ShowLayer("employeeDiv", "none");

        if (valid == 1) {
            document.forms["report_leave"].save.value = 1;
            document.forms["report_leave"].submit();
        }
    }
</script>
<?php
    $employees = q("SELECT e.id, e.lstname, e.frstname FROM (Employee AS e INNER JOIN Company_Users AS cu ON e.id = cu.user_id)
                    WHERE cu.company_id = '".$_SESSION["company_id"]."' AND e.email != 'admin' ORDER BY e.lstname, frstname");
?>
    <table width="100%">
        <tr height="380px">
            <td class="centerdata" valign="top">
                <form action="" method="post" name="report_leave">
                    <div id="" style="<?php if ($generated == "1") echo "display: none;"; else echo "display: block"; ?>">
                        <table width="100%">
                            <tr>
                                <td class="centerdata">
                                    <h6>Leave Report</h6>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <table width="100%">
                            <tr>
                                <td class="on-description" width="50%">
                                    Select Employee:
                                </td>
                                <td width="50%">
                                    <select class="on-field" method="post" name="employee">
                                        <option value="null">--  Select Employee  --</option>
                                        <option value="all">All Employees</option>
                                        <?php
                                            if (is_array($employees))
                                                foreach ($employees as $employee)
                                                    if ($_POST["employee"] == $employee[0])
                                                        echo "<option value='".$employee[0]."' selected>".$employee[1].", ".$employee[2]."</option>";
                                                    else
                                                        echo "<option value='".$employee[0]."'>".$employee[1].", ".$employee[2]."</option>";
                                        ?>
                                    </select>
                                    <div id="employeeDiv" style="display: none;"><font class="on-validate-error">* Employee must be selected</font></div>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <input name="btnGenerate" onClick="check();" type="button" value="Generate Report">
                        <input method="post" name="save" type="hidden" value="0" />
                    </div>
                </form>
                <div id="report" style="<?php if ($generated == "1") echo "display: block;"; else echo "display: none"; ?>">
                    <?php
                        echo "<div class='on-20px'><table class='on-table-center on-table'>";
                            echo "".$report_heading.$displayString;
                            echo "<tfoot><tr><td colspan='100%'></td></tr></tfoot>";
                        echo "</table></div>";

                        ///////////////////////////
                        //  Set Export Information
                        $_SESSION["fileName"] = "Leave Report";
                        $_SESSION["fileData"] = array_merge($excelheadings, $exceldata);
                        ///////////////////////////

                        echo "<input name='btnExport' onClick=\"location.href='_export.php'\" type='button' value='Export to Excel' />";
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>