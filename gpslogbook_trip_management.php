<?php
    session_start();

    include("_db.php");
    include("graphics.php");
    include("_functions.php");

    if (!$_SESSION["logged_in"] === true)
        header("Location: login.php");

	if (!hasAccess("GPS_LOGBOOK_MANAGEMENT"))
        header("Location: noaccess.php");

	if (isset($_POST["save"]) && $_POST["save"] === "1") {
        $errorMessage = "";

		$todayDate = date("Y-m-d");
        $todayTime = date("H:i:s");

        // - Get Information - //
        $project = addslashes(strip_tags($_POST["project"]));
		$projectID = substr($project,3);
		$projectName = q("SELECT name FROM Project WHERE id = '".$projectID."'");
        $projectType = (substr($project,0,3) == "CP_") ? "CP" : "SP";

		$tripDate = addslashes(strip_tags($_POST["tripDate"]));
        $areaID = ($_POST["area2"] != "") && is_numeric($_POST["area2"]) ? addslashes(strip_tags($_POST["area2"])) : 0;      // Check if any AREAs were posted
        $activityID = addslashes(strip_tags($_POST["activity2"]));
		$expenseTypeID = q("SELECT d.id FROM dropdowns AS d INNER JOIN categories AS c ON c.id = d.categoryID WHERE c.code='expenseType' AND d.name = 'Driving' ");
		$vehicleID = addslashes(strip_tags($_POST["vehicle"]));
        $expenseDescr = addslashes(strip_tags($_POST["expenseDescr"]));

		$kilometers = addslashes(strip_tags($_POST["totalDistance"]));
		$travelTime = addslashes(strip_tags($_POST["totalDuration"]));
		$tripIDs = addslashes(strip_tags($_POST["tripIDs"]));

		$trip_start_location = addslashes(strip_tags($_POST["trip_start_location"]));
		$trip_end_location = addslashes(strip_tags($_POST["trip_end_location"]));

		// - Calculations - //
		$timeRate = q("SELECT ur.rate FROM (user_rates AS ur INNER JOIN Project_User AS pu ON ur.id = pu.rateID)
			WHERE pu.user_id = '".$_SESSION["user_id"]."'
			AND pu.company_id = '".$_SESSION["company_id"]."'
			AND pu.project_id = '".$projectID."'
			AND ur.companyid = '".$_SESSION["company_id"]."'
			AND ur.userid = '".$_SESSION["user_id"]."'
		");
		$vehicleRate = q("SELECT rate FROM Vehicle WHERE id = '".$vehicleID."' ");
		$travelRateDropdown = q("SELECT dd.name FROM Project AS p INNER JOIN dropdowns AS dd ON p.travelRate = dd.id WHERE p.id = '".$projectID."'");
		$travelRate = ($travelRateDropdown == "0.5 x Professional Rate") ? 0.5 : (($travelRateDropdown == "1.0 x Professional Rate") ? 1 : 1);
		$travelCost = $vehicleRate * $kilometers;

		// - Insert Time Data - //
		$insert = q("INSERT INTO TimeSheet (company_id, projectType, date, user_id, project_id, area_id, activity_id, descr, rate, time_spent, on_date, on_time, locked, travelTime)
			VALUES (
				'".$_SESSION["company_id"]."',
				'".$projectType."',
				'".$tripDate."',
				'".$_SESSION["user_id"]."',
				'".$projectID."',
				'".$areaID."',
				'".$activityID."',
				'".$expenseDescr."',
				'".number_format(($travelRate * $timeRate), 2, ".", "")."',
				'".$travelTime."',
				'".$todayDate."',
				'".$todayTime."',
				'0',
				'1'
		)");

		if ($insert) {
			$timeSheetID = q("SELECT id FROM TimeSheet
				WHERE company_id = '".$_SESSION["company_id"]."'
				AND projectType = '".$projectType."'
				AND date = '".$tripDate."'
				AND user_id = '".$_SESSION["user_id"]."'
				AND project_id = '".$projectID."'
				AND area_id = '".$areaID."'
				AND activity_id = '".$activityID."'
				AND descr = '".$expenseDescr."'
				AND rate = '".number_format(($travelRate * $timeRate), 2, ".", "")."'
				AND time_spent = '".$travelTime."'
				AND on_date = '".$todayDate."'
				AND on_time = '".$todayTime."'
			");
			$insert = q("INSERT INTO ApprovedTime (company_id, projectType, date, user_id, project_id, area_id, activity_id, descr, rate, time_spent, on_date, on_time, timesheet_id, status, travelTime)
				VALUES (
					'".$_SESSION["company_id"]."',
					'".$projectType."',
					'".$tripDate."',
					'".$_SESSION["user_id"]."',
					'".$projectID."',
					'".$areaID."',
					'".$activityID."',
					'".$expenseDescr."',
					'".number_format(($travelRate * $timeRate), 2, ".", "")."',
					'".$travelTime."',
					'".$todayDate."',
					'".$todayTime."',
					'".$timeSheetID."',
					'0',
					'1'
			)");
			$logs = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id)
				VALUES (
					'".$_SESSION["email"]." booked time on ".$projectName."',
					'Insert',
					'TimeSheet',
					'".$_SESSION["email"]."',
					'".$todayDate."',
					'".$todayTime."',
					'".$_SESSION["company_id"]."'
			)");
		}

		// - Insert Expense Data - //
		$insert = q("INSERT INTO ExpenseSheet (company_id, projectType, date, user_id, project_id, area_id, activity_id, expense_type_id, vehicle_id, descr, kilometers, businessKM, privateKM, rate, expense, on_date, on_time, placeFrom, placeTo)
			VALUES (
			'".$_SESSION["company_id"]."',
			'".$projectType."',
			'".$tripDate."',
			'".$_SESSION["user_id"]."',
			'".$projectID."',
			'".$areaID."',
			'".$activityID."',
			'".$expenseTypeID."',
			'".$vehicleID."',
			'".$expenseDescr."',
			'".number_format($kilometers, 2, ".", "")."',
			'".$kilometers."',
			'0',
			'".number_format($vehicleRate, 2, ".", "")."',
			'".number_format($travelCost, 2, ".", "")."',
			'".$todayDate."',
			'".$todayTime."',
			'".$trip_start_location."',
			'".$trip_end_location."'
		)");
		if($insert) {
			$expenseSheetID = q("SELECT id FROM ExpenseSheet
				WHERE company_id = '".$_SESSION["company_id"]."'
				AND projectType = '".$projectType."'
				AND date = '".$tripDate."'
				AND user_id = '".$_SESSION["user_id"]."'
				AND project_id = '".$projectID."'
				AND area_id = '".$areaID."'
				AND activity_id = '".$activityID."'
				AND expense_type_id = '".$expenseTypeID."'
				AND vehicle_id = '".$vehicleID."'
				AND on_date = '".$todayDate."'
				AND on_time = '".$todayTime."'
				LIMIT 1
			");
			$insert = q("INSERT INTO ApprovedExpense (company_id, projectType, date, user_id, project_id, area_id, activity_id, expense_type_id, descr, on_date, on_time, expensesheet_id, vehicle_id, placeFrom, placeTo, kilometers, privateKM, businessKM, rate, expense)
				VALUES (
					'".$_SESSION["company_id"]."',
					'".$projectType."',
					'".$tripDate."',
					'".$_SESSION["user_id"]."',
					'".$projectID."',
					'".$areaID."',
					'".$activityID."',
					'".$expenseTypeID."',
					'".$expenseDescr."',
					'".$todayDate."',
					'".$todayTime."',
					'".$expenseSheetID."',
					'".$vehicleID."',
					'".$trip_start_location."',
					'".$trip_end_location."',
					'".number_format($kilometers, 2, ".", "")."',
					'0',
					'".$kilometers."',
				'".number_format($vehicleRate, 2, ".", "")."',
				'".number_format($travelCost, 2, ".", "")."'
			)");
			$logs = q("INSERT INTO Logs (what, access, on_table, by_user, on_date, on_time, company_id)
				VALUES (
					'".$_SESSION["email"]." booked expenses on ".$projectName."',
					'Insert',
					'ExpenseSheet',
					'".$_SESSION["email"]."',
					'".$todayDate."',
					'".$todayTime."',
					'".$_SESSION["company_id"]."'
			)");

			// - Update Logbook Entries - //
			$update = q("UPDATE gps_trip_data SET processed = '1', expenseSheetID = '".$expenseSheetID."' WHERE id IN (".$tripIDs.")");
		}

		header("Location: gpslogbook_trip_management.php");
	}

	if ($errorMessage != "")
        echo "<p align='center' style='padding:0px;'><strong><font color='#999999'>$errorMessage</font></strong></p>";

    //  Print Header
    print_header();
    //  Print Menu
    print_menus("0", "home");
?>
	<!-- Get Logbook Data -->
	<script type="text/javascript">
		jQuery(function() {
			jQuery("#btnCheckForNewData").click(function() {

				jQuery("#maskNotification").css({"width":jQuery(document).width(),"height":jQuery(document).height()});
				jQuery('#maskNotification').fadeIn("fast").fadeTo("fast",0.8);   //transition effect
				jQuery("#modal_gps_logbook").fadeIn();

				jQuery.post("_ajax.php", {func: "getGPSLogbookData", user_email: "<?php echo $_SESSION['email']; ?>", user_id: "<?php echo $_SESSION['user_id']; ?>"}, function(data)	{
					data = json_decode(data);
					if(data == "success") {
						window.location = "";
					}else {
						jQuery("#maskNotification").hide();
						jQuery("#modal_gps_logbook").hide();
					}
				});
			});
		});
	</script>

	<!-- Trip Info -->
	<script language="JavaScript" src="include/validation.js"></script>
	<script type="text/javascript">
		jQuery(function() {

			var loadAreas;
			var loadActivityTypes2;
			var loadActivities2;
			var hasAreas = false;

			// Project Change
			jQuery("#project").change(function() {
				if (jQuery(this).val() != "null") {
					loadAreas();
					loadActivityTypes2();
					jQuery("#activityType2").fadeIn();
					jQuery("#projectDiv").hide();
				}else {
					jQuery("#vehicle").fadeOut();
					jQuery("#activityType2").fadeOut();
					jQuery("#activity2").fadeOut();
					jQuery("#area2").fadeOut();
					jQuery("#expenseDescr").fadeOut();
				}
				jQuery("#activity2Div").hide();
				jQuery("#vehicleDiv").hide();

				jQuery("#vehicle").width(jQuery("#project").width()+2);
				jQuery("#expenseDescr").width(jQuery("#project").width());
			});

			//Activity Type Change
			jQuery("#activityType2").change(function() {
				loadActivities2();
			});

			//Activity Change
			jQuery("#activity2").change(function() {
				if (jQuery(this).val() != "null") {
					jQuery("#vehicle").fadeIn();
					jQuery("#expenseDescr").fadeIn();
					jQuery("#activity2Div").hide();
				}else {
					jQuery("#vehicle").fadeOut();
				}
				jQuery("#vehicleDiv").hide();
			});

			//Activity Change
			jQuery("#vehicle").change(function()    {
				if (jQuery(this).val() != "null") {
					jQuery("#expenseDescr").fadeIn();
					jQuery("#vehicleDiv").hide();
				}else {
					jQuery("#expenseDescr").fadeOut();
				}
			});

			//  Load Areas
			loadAreas = function() {
				var projectType = jQuery("#project").val().substring(0, 2);
				var projectID = jQuery("#project").val().substring(3);
				jQuery.post("_ajax.php", {func: "get_area_info", projectType: projectType, projectID: projectID}, function(data)	{
					data = json_decode(data);
					jQuery("#area2 option").remove();
					jQuery("#area2").append(jQuery("<option></option>").attr("value","null").text("--  Select Area --"));
					if (is_array(data)) {   // Display area options
						jQuery("#area2").fadeIn();
						hasAreas = true;
						jQuery.each(data,function(i, v)	{
							jQuery("#area2").append(jQuery("<option></option>").attr("value",v[0]).text(v[1]));
						});
					}else {               // Do not display area options
						jQuery("#area2").fadeOut();
						hasAreas = false;
					}
				});
			};
			// Load Activity Types
			loadActivityTypes2 = function()  {
				var projectType = jQuery("#project").val().substring(0, 2);
				var projectID = jQuery("#project").val().substring(3);
				jQuery.post("_ajax.php", {func: "get_activity_types", projectType: projectType, projectID: projectID}, function(data)	{
					data = json_decode(data);
					jQuery("#activityType2 option").remove();
					jQuery("#activity2 option").remove();
					//jQuery("#activityType2").append(jQuery("<option></option>").attr("value","null").text("--  Select Activity Type  --"));
					if (is_array(data)) {
						jQuery.each(data,function(i, v)	{
							if(v[1] == "Traveling") {
								jQuery("#activityType2").append(jQuery("<option></option>").attr("value",v[0]).text(v[1]));
							}else {
								jQuery("#activityType2").append(jQuery("<option></option>").attr("value",v[0]).text(v[1]));
							}
						});
						jQuery("#activityType2").val(<?php echo $_SESSION["default_activityType"];?>);
						loadActivities2();
					}
				});
			};
			//  Load Activities
			loadActivities2 = function()  {
				var projectType = jQuery("#project").val().substring(0, 2);
				var activityType = jQuery("#activityType2").val();
				jQuery.post("_ajax.php", {func: "get_activities", projectType: projectType, activityType: activityType}, function(data)	{
					data = json_decode(data);
					jQuery("#activity2 option").remove();
					jQuery("#activity2").append(jQuery("<option></option>").attr("value","null").text("--  Select Activity  --"));
					if(is_array(data)) {
						jQuery.each(data,function(i, v)	{
							jQuery("#activity2").append(jQuery("<option></option>").attr("value",v[0]).text(v[1]));
						});
						jQuery("#activity2").fadeIn();
					}else {
						jQuery("#activity2").fadeOut();
					}
					jQuery("#activity2").val(<?php echo $_SESSION["default_activity"];?>);
					if(jQuery("#activity2").val() != "null"){
						jQuery("#expenseDescr").fadeIn();
						jQuery("#vehicle").fadeIn();
					}
				});
			};

		});
	</script>

	<!-- Draggable Table Functions -->
	<script type="text/javascript" src="include/jquery-ui-1.8.5.js"></script>
	<script type='text/javascript'>
		jQuery(function() {

			function bind_draggable_functions() {
				// Identify Draggable Table
				table = jQuery('#draggable_table');
				row_drop_allowed = 1;

				// Make Table Draggable
				table.find('tr:not(".dragged_out") td.drag_marker, tr:not(".dragged_out") td:nth-child(2), tr:not(".dragged_out") td:nth-child(3)').draggable("destroy").bind('mousedown', function () {
					table.disableSelection();
				}).bind('mouseup', function () {
					table.enableSelection();
				}).draggable({
					helper: function (event) {
						return jQuery('<div class="drag-cart-item"><table style="width:1000px;" class="on-table"></table></div>')
						.find('table')
						.append(jQuery(event.target)
						.closest('tr')
						.clone()
						.attr("class", "trip_row"))
						.end();
					},
					cursorAt: {
						left: -10,
						bottom: 34
					},
					cursor: 'move',
					distance: 0,
					delay: 0,
					scope: 'cart-item',
					revert: 'invalid'
				}).css("cursor","move");
			}

			// Identify Draggable Table
			var table = jQuery('#draggable_table');
			var smallest_date = "";
			var row_drop_allowed = 1;

			// Make Table Draggable
			table.find('tr td.drag_marker, tr td:nth-child(2), tr td:nth-child(3)').bind('mousedown', function () {
				table.disableSelection();
			}).bind('mouseup', function () {
				table.enableSelection();
			}).draggable({
				helper: function (event) {
					return jQuery('<div class="drag-cart-item"><table style="width:1000px;" class="on-table"></table></div>')
					.find('table')
					.append(jQuery(event.target)
					.closest('tr')
					.clone()
					.attr("class", "trip_row"))
					.end();
				},
				cursorAt: {
					left: -10,
					bottom: 34
				},
				cursor: 'move',
				distance: 0,
				delay: 0,
				scope: 'cart-item',
				revert: 'invalid'
			}).css("cursor","move");

			// Identify Droppable Table
			jQuery('#droppable_table').droppable({
				scope: 'cart-item',
				activeClass: 'active',
				hoverClass: 'hover',
				tolerance: 'pointer',
				drop: function (event, ui) {

					jQuery("#draggable_description").hide();
					jQuery("#droppable_table_headings").fadeIn();

					var dropped_row = ui.helper.find('tr');
					var dropped_row_date = new Date(dropped_row.children().eq(1).text());

					if(smallest_date != "") {
						if( dropped_row_date.getDate() == (new Date(smallest_date).getDate()) ) {
							row_drop_allowed = 1;
						}else {
							row_drop_allowed = 0;
							jQuery("body").scrollTop(0);
							errorMessage("The date of the entry does not fall on the same day as the start date and is therefore not allowed");
						}
					}else {
						smallest_date = dropped_row_date;
					}

					if(row_drop_allowed == 1) {

						jQuery(".btnAddEditLocation").bind('click', function () {
							handle_location_model(jQuery(this));
						});

						dropped_row.children().eq(0).remove();
						dropped_row.children().eq(1).remove();
						dropped_row.children().eq(5).remove();

						jQuery("#draggable_table #"+dropped_row.attr('id')).addClass('dragged_out');
						jQuery("#draggable_table #"+dropped_row.attr('id')).children().eq(0).css('cursor','pointer').find('img:first').hide().find('img:nth-child(1)').show();
						jQuery("#draggable_table #"+dropped_row.attr('id')).children().eq(0).find('img:nth-child(2)').show();
						jQuery("#draggable_table #"+dropped_row.attr('id')).children().eq(1).removeAttr('style');
						jQuery("#draggable_table #"+dropped_row.attr('id')).children().eq(2).removeAttr('style');

						jQuery('#droppable_table').append(dropped_row);
						jQuery(".dragged_out td").css("background","#333028").css("color","#aaa").removeClass("ui-draggable").unbind('mousedown');
						// Sort/Re-Order Rows
						sort_rows(jQuery('#droppable_table'));
						calculate_totals(jQuery('#droppable_table'));
					}
				}
			});

			function calculate_totals(trip_table) {
				if(trip_table.find(".trip_row").length == 0) {
					jQuery("#droppable_table_headings").hide();
					jQuery("#trip_totals").remove();
					jQuery("#draggable_description").show();
					smallest_date = "";
				}else {
					jQuery("#trip_totals").remove();
					var trip_date = "";
					var start_location = "";
					var end_location = "";
					var total_distance = 0;
					var total_duration = 0;
					var trip_ids = "";

					start_location = trip_table.find('tr.trip_row:first').children().eq(1).html();
					end_location = trip_table.find('tr.trip_row:last').children().eq(2).html();

					trip_table.find('tr.trip_row').each(function() {
						trip_date = jQuery(this).children().eq(0).html().substring(0,10);
						total_distance += parseFloat(jQuery(this).children().eq(3).html());
						total_duration += parseFloat(jQuery(this).children().eq(4).html());
						// Add trip ID to hidden variable
						trip_ids += jQuery(this).attr('id')+",";
					});

					trip_ids = trip_ids.slice(0,-1);
					// Convert to On-Track times
					total_duration = (total_duration/60);
					total_distance = Math.ceil(total_distance);
					var duration2 = Math.floor(total_duration) ;
					var duration3 = total_duration - Math.floor(total_duration);

					if(duration3 <=0.25) {
						if(duration2 >=1)
							duration3 = 0.5;
						else
							duration3 = 0.25;
					}else if(duration3 > 0.25 && duration3 <=0.5) {
						duration3 = 0.5;
					}else if(duration3 > 0.5 && duration3 <= 0.75) {
						if(duration2 >=1) {
							duration3 = 0.0;
							duration2++;
						}else{
							duration3 = 0.75;
						}
					}else {
						duration3 = 0;
						duration2++;
					}

					total_duration = (duration2 + duration3);

					trip_table.append("<tr id='trip_totals'>"+
						"<td class='on-table-total' style='color:orange;'>Summary: <input type='hidden' name='tripDate' value='"+trip_date+"'><input type='button' name='btnBook' id='btnBook' value='Book Trip' style='float:left;'></td>"+
						"<td class='on-table-total'><input type='hidden' name='trip_start_location' value='"+start_location+"'>"+start_location+"</td>"+
						"<td class='on-table-total'><input type='hidden' name='trip_end_location' value='"+end_location+"'><input type='hidden' name='tripIDs' value='"+trip_ids+"'>"+end_location+"</td>"+
						"<td class='on-table-total'><input type='hidden' name='totalDistance' value='"+total_distance+"'>"+total_distance+" km</td>"+
						"<td class='on-table-total'><input type='hidden' name='totalDuration' value='"+total_duration+"'>"+total_duration+" h</td>"
					+"</tr>");
					trip_table.find('tr:last td:first input[type="button"]').bind('click', function () {

						var valid_fields = true;

						var project = jQuery("#project").val();
						var area = jQuery("#area2").val();
						var activity = jQuery("#activity2").val();
						var vehicle = jQuery("#vehicle").val();

						test = validation("required", project);
						if (!test)	jQuery("#projectDiv").show();
						else				jQuery("#projectDiv").hide();
						valid_fields &= test;

						if(jQuery("#area2").is(":visible")) {
							test = validation("required", area);
							if (!test)	jQuery("#area2Div").show();
							else				jQuery("#area2Div").hide();
							valid_fields &= test;
						}
						if(jQuery("#activity2").is(":visible")) {
							test = validation("required", activity);
							if (!test)	jQuery("#activity2Div").show();
							else				jQuery("#activity2Div").hide();
							valid_fields &= test;
						}
						if(jQuery("#vehicle").is(":visible")) {
							test = validation("required", vehicle);
							if (!test)	jQuery("#vehicleDiv").show();
							else				jQuery("#vehicleDiv").hide();
							valid_fields &= test;
						}

						if(valid_fields) {
							jQuery("#save").val("1");
							jQuery("#bookTripForm").submit();
						}else {
							jQuery("#save").val("0");
						}

					});
				}
			}

			function sort_rows(table_to_order) {
				var rows = table_to_order.find('tr.trip_row');
				rows.sort(function(a, b) {
					var keyA = jQuery(a).children().eq(0).text();
					var keyB = jQuery(b).children().eq(0).text();
					var return_value = 0;
					if(keyA > keyB) {
						return_value = 1;
					}
					if(keyA < keyB) {
						return_value -1;
					}
					if(smallest_date != "") {
						if(keyA < smallest_date)
							smallest_date = keyA;
						if(keyB < smallest_date)
							smallest_date = keyB;
					}else {
						if(keyA < keyB)
							smallest_date = keyA;
						else
							smallest_date = keyB;
					}
					return return_value;
				});
				jQuery.each(rows, function(index, row) {
					table_to_order.children('tbody').append(row);
				});
			}

			// - Location Add/Edit Modal and Function - //

			// - Edit Location Link - //
			var clickedObj;
			jQuery(".btnAddEditLocation").click(function() {
				handle_location_model(jQuery(this));
			});

			function handle_location_model(obj) {
				clickedObj = obj;
				var latLon = obj.prev().val().split(', ');
				var location_id = obj.attr('location_id');

				if(location_id > 0 || obj.html() != "Unknown location") {
					jQuery(".on-location-modal #description").val(obj.html());
				}else {
					jQuery(".on-location-modal #description").val("");
				}

				jQuery(".on-location-modal #current_location_id").val(location_id);
				jQuery(".on-location-modal #latitude").val(latLon[0]);
				jQuery(".on-location-modal #longitude").val(latLon[1]);
				jQuery("#mask").css({"width":jQuery(document).width(),"height":jQuery(document).height()});
				jQuery('#mask').fadeIn("fast").fadeTo("fast",0.8);   //transition effect
				jQuery("#modal_locations").fadeIn();
			}

			// - Validate Location Modal Fields - //
			jQuery("#btnAddEdit").click(function() {
				var valid = true;

				var location_id = jQuery("#current_location_id").val();
				var description = jQuery("#description").val();
				var latitude = jQuery("#latitude").val();
				var longitude = jQuery("#longitude").val();

				test = validation("required", description);

				if (!test)	jQuery("#descriptionDiv").show();
				else				jQuery("#descriptionDiv").hide();

				valid &= test;

				test = validation("lat", latitude);

				if (!test)	jQuery("#latitudeDiv").show();
				else				jQuery("#latitudeDiv").hide();

				valid &= test;

				test = validation("long", longitude);

				if (!test)	jQuery("#longitudeDiv").show();
				else				jQuery("#longitudeDiv").hide();

				valid &= test;

				if(valid) {
					jQuery.post("_ajax.php", {func: "locationAddUpdate", location_id: location_id, description: description, latitude: latitude, longitude: longitude}, function(data)  {
						data = json_decode(data);
						if(data.msg == "added" || data.msg == "updated") {
							jQuery.each(jQuery(".btnAddEditLocation"),function() {
								if(coordinate_in_radius(latitude, longitude, jQuery(this).prev().val()) == 1) {
									if(data.added_location_id != "" && data.added_location_id > 0) {
										jQuery(this).attr("location_id", ""+data.added_location_id+"");
									}
									jQuery(this).html(data.description);
									jQuery(this).css("color","#fff");
								}
							});
							handle_ajax_msg("Location "+data.msg+" successfully");
						}else {
							handle_ajax_msg(""+data+"");
						}
					});
				}
			});

			// - Handle AJAX Messages - //
			function handle_ajax_msg(msg) {
				jQuery("#ajax_msg_section strong").html(msg);
				jQuery("#ajax_msg_section").fadeIn().delay(2000).fadeOut(
					function() {
						if(msg == "Location added successfully" || msg == "Location updated successfully") {
							jQuery("#mask").hide();
							jQuery("#modal_locations").hide();
						}
					}
				);
			}

			// - Close Location Modal - //
			jQuery("#mask, #closeError").click(function() {
				jQuery("#mask").hide();
				jQuery("#modal_locations").hide();

				// Pop-up Modal
				jQuery("#ajax_msg_section").hide();
				jQuery("#descriptionDiv").hide();
			});

			// - Mark as Private - //
			jQuery(".btnBusiness").click(function() {
				var dropped_row_id = jQuery(this).parent().parent().attr("id");
				jQuery("#droppable_table").find("#"+dropped_row_id).remove();
				jQuery(this).hide().next().show();
				jQuery("#draggable_table #"+dropped_row_id).addClass('dragged_out').children();
				jQuery("#draggable_table").find("#"+dropped_row_id+" td:first img").hide();
				jQuery(".dragged_out td").css("cursor","auto").css("background","#333028").css("color","#aaa").removeClass("ui-draggable").unbind('mousedown');
				calculate_totals(jQuery('#droppable_table'));

				jQuery.post("_ajax.php", {func: "toggleGPSEntryType", change_type: "Private", gps_entry_id: dropped_row_id}, function(data)	{
					data = json_decode(data);
				});
			});

			// - Mark as Business - //
			jQuery(".btnPrivate").click(function() {
				var dropped_row_id = jQuery(this).parent().parent().attr("id");
				jQuery("#droppable_table").find("#"+dropped_row_id).remove();
				jQuery(this).hide().prev().show();
				jQuery("#draggable_table").find("#"+dropped_row_id+" td:first img:first").show();
				jQuery(this).parent().parent().find('td').removeAttr('style');
				jQuery(this).parent().parent().removeClass("dragged_out");
				calculate_totals(jQuery('#droppable_table'));
				bind_draggable_functions();

				jQuery.post("_ajax.php", {func: "toggleGPSEntryType", change_type: "Business", gps_entry_id: dropped_row_id}, function(data)	{
					data = json_decode(data);
				});

			});

			// - Mark ALL entries as Private - //
			jQuery("#btnMakeAllPrivate").click(function() {
				var makePrivateIDs = "";
				jQuery("#droppable_table .trip_row").remove();
				jQuery("#draggable_table .logbook_row").each(function() {
					jQuery(this).addClass('dragged_out');
					jQuery(this).find("td:last img:first").hide().next().show();
					jQuery(this).find("td:first img").hide();
					makePrivateIDs += jQuery(this).attr('id')+",";
				});
				makePrivateIDs = makePrivateIDs.slice(0,-1);
				jQuery(".dragged_out td").css("cursor","auto").css("background","#333028").css("color","#aaa").removeClass("ui-draggable").unbind('mousedown');
				calculate_totals(jQuery('#droppable_table'));
				jQuery.post("_ajax.php", {func: "toggleGPSEntryType", change_type: "Private", gps_entry_id: "All", rowIDs: makePrivateIDs}, function(data)	{
					data = json_decode(data);
				});
			});

			// - Mark ALL entries as Business - //
			jQuery("#btnMakeAllBusiness").click(function() {
				var makeBusinessIDs = "";
				jQuery("#draggable_table .logbook_row").each(function() {
					jQuery(this).removeClass("dragged_out");
					jQuery(this).find("td:last img:last").hide().prev().show();
					jQuery(this).find("td:first img:first").show();
					makeBusinessIDs += jQuery(this).attr('id')+",";
				});
				makeBusinessIDs = makeBusinessIDs.slice(0,-1);
				jQuery("#draggable_table .logbook_row td").removeAttr('style');
				calculate_totals(jQuery('#droppable_table'));
				bind_draggable_functions();
				jQuery.post("_ajax.php", {func: "toggleGPSEntryType", change_type: "Business", gps_entry_id: "All", rowIDs: makeBusinessIDs}, function(data)	{
					data = json_decode(data);
				});
			});

			// - Undo the Drag and Drop - //
			jQuery(".btnUndo").click(function() {
				var dropped_row_id = jQuery(this).parent().parent().attr("id");
				jQuery("#droppable_table").find("#"+dropped_row_id).remove();
				jQuery(this).parent().parent().find('td').removeAttr('style');
				jQuery(this).parent().parent().removeClass("dragged_out");
				jQuery(this).hide().prev().show();
				calculate_totals(jQuery('#droppable_table'));
				bind_draggable_functions();
			});

			// - Lat, Long in 1 KM Radius - //
			function coordinate_in_radius(lat1, lon1, obj_latLon) {
				var latLon = obj_latLon.split(', ');
				var lat2 = latLon[0];
				var lon2 = latLon[1];

				var constant = 180 / Math.PI;
				var earth_radius = 6378.1; //  KM radius of Earth
				var distance = earth_radius * Math.acos(Math.sin(lat1/constant) * Math.sin(lat2/constant) + Math.cos(lat1/constant) * Math.cos(lat2/constant) *  Math.cos(lon2/constant - lon1/constant)); // KM

				if(distance <= 1) {
					return 1;
				}else {
					return 0;
				}
			}

		});
	</script>

<!--  BEGIN: LOCATION EDIT MODAL  -->
	<div id="modal_locations" name="modal_locations" class="on-location-modal">
		<div id="ajax_msg_section" style="display:none;"><strong style="color:orange;font-weight:bold;font-size:14px;font-variant:small-caps;"></strong></div><br/>
		<div id="location_section">
			<table width="100%">
				<tr>
					<td colspan="100%" class='centerdata'><a>Location Details</a><br><br></td>
				</tr>
				<tr>
					<td class="on-description" width="50%">
						   Location Description:
					</td>
					<td width="50%">
						<input class="on-field" id="description" name="description" tabindex="<?php echo $index++; ?>" type="text" value="" />
						<div id="descriptionDiv" style="display: none;"><font class="on-validate-error">* Location description must be entered</font></div>
					</td>
				</tr>
				<tr>
					<td class="on-description" width="50%">
						Latitude:
					</td>
					<td width="50%">
						<input class="on-field-date" id="latitude" name="latitude" tabindex="<?php echo $index++; ?>" type="text" value="" disabled />
						<div id="latitudeDiv" style="display: none;"><font class="on-validate-error">* Need to fit xx.xxxxxx Example: -23.456324</font></div>
					</td>
				</tr>
				<tr>
					<td class="on-description" width="50%">
						Longitude:
					</td>
					<td width="50%">
						<input class="on-field-date" id="longitude" name="longitude" tabindex="<?php echo $index++; ?>" type="text" value="" disabled />
						<div id="longitudeDiv" style="display: none;"><font class="on-validate-error">* Need to fit xx.xxxxxx Example: 23.456324</font></div>
					</td>
				</tr>
			</table>
			<br/>
			<input id="btnAddEdit" name="btnAddEdit" tabindex="<?php echo $index++; ?>" type="button" value="Add/Update" />
			<input id="current_location_id" name="current_location_id" type="hidden" value="" />
		</div>
	</div>
<!--  END: LOCATION EDIT MODAL  -->

<!--  BEGIN: GPS IMPORT NOTIFICATION MODAL  -->
	<div id="modal_gps_logbook" name="modal_gps_logbook" class="on-gps-logbook-notification-modal">
		<strong style="color:orange;font-weight:bold;font-size:20px;font-variant:small-caps;">Checking for new GPS Logbook Data</strong>
		<br/>
		<br/>
		<br/>
		<img src="images/loading.gif" width="80px" height="80px" />
		<br/>
		<br/>
		<br/>
		<strong style="color:orange;font-weight:bold;font-size:14px;font-variant:small-caps;">This process cannot be skipped - Please be patient</strong>
	</div>
<!--  END: GPS IMPORT NOTIFICATION MODAL  -->

<!-- INPUT PAGE -->
    <table width="100%">
        <tr>
            <td class="centerdata" valign="top">
                <form action="" method="post" id="bookTripForm" name="bookTripForm">
                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>GPS Logbook Trip Management</h6>
                            </td>
                        </tr>
						<tr>
                            <td class="centerdata">
								<br/>
                                <input style="cursor: pointer;" type="button" name="btnCheckForNewData" id="btnCheckForNewData" value="Check for new GPS Logbook data">
                            </td>
                        </tr>
                    </table>

					<?php
						$projects = q("SELECT DISTINCT(p.id), p.name, p_type FROM (Project AS p INNER JOIN Project_User AS pu ON p.id = pu.project_id INNER JOIN companiesOnTeam AS cot ON cot.project_id = p.id) ".
							"WHERE pu.user_id = '".$_SESSION["user_id"]."' AND cot.company_id = '".$_SESSION["company_id"]."' AND pu.company_id = '".$_SESSION["company_id"]."' AND p.completed = '0' AND p.deleted = '0' ".
							"AND pu.deleted = '0' ORDER BY UPPER(p.name)");
						$vehicles = q("SELECT v.id, v.type FROM Vehicle AS v INNER JOIN vehicle_user AS vu ON vu.vehicle_id = v.id WHERE vu.user_id = '".$_SESSION['user_id']."' AND v.company_id = '".$_SESSION['company_id']."' AND v.deleted = '0' ORDER BY v.type");
						$locations = q("SELECT id, description, latitude, longitude FROM locations WHERE company_id = '".$_SESSION['company_id']."' ");
					?>

					<table id="droppable_table" class='on-table on-table-center' style="min-width:1015px; height:25px;">
						<tr><td class="on-table-clear" colspan="100%"><h5>Trip Creator</h5></td></tr>

						<tr>
							<td class="on-table-clear" colspan="100%">

								<table class="on-table-trip-builder">
									<tr>
										<td>
											<select title="Project" id="project" name="project" method="post" class="on-field required" tabindex="<?php echo $index++; ?>">
												<option value="null">--  Select A Project  --</option>
												<?php
													if (is_array($projects))    {
														foreach ($projects as $project) {
															$value = ($project[2] == "CP") ? "CP_".$project[0]: "SP_".$project[0];
															echo "<option value='".$value."'>".$project[1]."</option>";
														}
													}
												?>
											</select>
											<div id="projectDiv" name="projectDiv" class="error">* Project must be selected</div>
										</td>
										<td>
											<select title="Area" id="area2" name="area2" method="post" class="on-field required" tabindex="<?php echo $index++; ?>" style="display:none;"></select>
											<div id="area2Div" name="area2Div" class="error">* Area must be selected</div>
										</td>
										<td>
											<select title="Activity Type" id="activityType2" name="activityType2" method="post" class="on-field required" tabindex="<?php echo $index++; ?>" style="display:none;"></select>
											<div id="activityType2Div" name="activityType2Div" class="error">* Activity Type must be selected</div>
										</td>
										<td>
											<select title="Activity" id="activity2" name="activity2" method="post" class="on-field required" tabindex="<?php echo $index++; ?>" style="display:none;"></select>
											<div id="activity2Div" name="activity2Div" class="error">* Activity must be selected</div>
										</td>
									</tr>
									<tr>
										<td>
											<!--<div class="spacer"></div>-->
											<select title="Vehicle" id="vehicle" name="vehicle" method="post" class="on-field required" tabindex="<?php echo $index++; ?>" style="display:none;">
												<option value="null">--  Select A Vehicle  --</option>
												<?php
													if (is_array($vehicles)) {
														foreach ($vehicles as $vehicle) {
															if(count($vehicles) == 1) {
																echo "<option value='".$vehicle[0]."' selected>".$vehicle[1]."</option>";
															}else {
																if($_SESSION['default_vehicle'] == $vehicle[0]) {
																	echo "<option value='".$vehicle[0]."' selected>".$vehicle[1]."</option>";
																}else {
																	echo "<option value='".$vehicle[0]."'>".$vehicle[1]."</option>";
																}
															}
														}
													}
												?>
											</select>
											<div id="vehicleDiv" name="vehicleDiv" class="error">* Vehicle must be selected</div>
										</td>
									</tr>
									<tr>
										<!--<div class="spacer"></div>-->
										<td>
											<textarea title="Trip Description" placeholder="Trip Description" id="expenseDescr" name="expenseDescr" class="on-field maxLength" rows="1" tabindex="<?php echo $index++; ?>" style="width:218px; resize:none; display:none;"></textarea>
											<div id="expenseDescrDiv" name="expenseDescrDiv" class="error">* Description must be added</div>
										</td>
									</tr>
								</table>

							</td>
						</tr>

						<tr id="droppable_table_headings" style="display:none;"><th>Date</th><th>Start Location</th><th>End Location</th><th>Distance (km)</th><th>Duration (min)</th></tr>
						<tr id='draggable_description'>
							<td colspan="100%">
								<div style="width:100%;">
									<center style="height:120px; line-height:120px;"><h5>- Drag GPS Logbook Entries here to create a trip -</h5></center>
								</div>
							</td>
						</tr>
					</table>

					<table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td class="centerdata">
                                <h6>GPS Logbook Entries</h6>
                            </td>
                        </tr>
                    </table>

					<?php

						$gps_trip_data = q("SELECT * FROM gps_trip_data WHERE deleted = '0' AND processed = '0' AND user_id = '".$_SESSION['user_id']."' AND designation = 'Business' ORDER BY endDateLocal DESC");

						if(is_array($gps_trip_data) > 0) {
							echo "<table id='draggable_table' class='on-table on-table-center'>";
								echo	"<tr>
									<td class='on-table-clear' colspan='100%'>
										<a>GPS Logbook Information: ".$_SESSION['user']."</a>
										<div style='float:right; margin-right:32px;'>
											<p style='float:left; padding-top:5px; margin-right:8px;'>Mark all entries as:</p>
											<img id='btnMakeAllBusiness' title='Mark all as Business' style='cursor: pointer;' src='images/icons/business-man.png' />
											<img id='btnMakeAllPrivate' title='Mark all as Private' style='cursor: pointer;' src='images/icons/private-man.png' />
										</div>
									</td>
								</tr>";
								echo"<tr><th></th><th>Start Time</th><th>End Time</th><th>Start Location</th><th>End Location</th><th>Distance (km)</th><th>Duration (min)</th><th>Private/Business</th></tr>";
							foreach($gps_trip_data as $value) {
								$duration = number_format(($value[4]/60), 2, ".", "");
								$distance = ($value[3] * 0.001);
								$start_location = "";
								$end_location = "";
								$start_location_id = "";
								$end_location_id = "";
								$start_coordinates = $value[11].", ".$value[10];
								$end_coordinates = $value[13].", ".$value[12];
								if($value[2] == "Business")
									$private_business = "<img class='btnBusiness' title='Change to Private' style='cursor: pointer;' src='images/icons/business-man.png' /><img class='btnPrivate' title='Change to Business' style='display:none; cursor: pointer;' src='images/icons/private-man.png' />";
								else
									$private_business = "<img class='btnBusiness' title='Change to Private' style='display:none; cursor: pointer;' src='images/icons/business-man.png' /><img class='btnPrivate' title='Change to Business' style='cursor: pointer;' src='images/icons/private-man.png' />";
								// - Set Location Description - //
								if(is_array($locations)) {
									foreach($locations As $l) {
										// Start Location
										if(distance_between_points($l[2], $l[3], $value[11], $value[10]) <= 1 && $start_location == "") {	// 1 - in radius of 1 km
											$start_location_id = $l[0];
											$start_location = $l[1];
										}
										// End Location
										if(distance_between_points($l[2], $l[3], $value[13], $value[12]) <= 1 && $end_location == "") {	// 1 - in radius of 1 km
											$end_location_id = $l[0];
											$end_location = $l[1];
										}
									}
								}

								// - Handle Location Management based on user role/action - //
								$has_location_management_access = hasAccess("LOCATION_MANAGEMENT") ? 1 : 0;

								// Start Location
								$start_location  = ($start_location == "") ? "Unknown location" : $start_location;
								$text_color  = ($start_location == "Unknown location") ? "#9e8c6f" : "#ffffff";
								if($has_location_management_access)
									$start_location = "<a location_id='".$start_location_id."' style='cursor: pointer; color:".$text_color."' title='Click to edit this location' class='btnAddEditLocation'>".$start_location."</a>";
								else
									$start_location = "<font style='color:#9e8c6f'>".$start_location."<font>";

								// End Location
								$end_location  = ($end_location == "") ? "Unknown location" : $end_location;
								$text_color  = ($end_location == "Unknown location") ? "#9e8c6f" : "#ffffff";
								if($has_location_management_access)
									$end_location = "<a location_id='".$end_location_id."' style='cursor: pointer; color:".$text_color."' title='Click to edit this location' class='btnAddEditLocation'>".$end_location."</a>";
								else
									$end_location = "<font style='color:#9e8c6f'>".$end_location."<font>";

								echo "<tr class='logbook_row' id='".$value[0]."'>";
									echo "<td class='drag_marker'><img class='btnMove' src='images/icons/move.png' title='Click and drag this entry to the Trip Creator Box' /><img class='btnUndo' style='display:none;' src='images/icons/undo.png' title='Undo'/></td>";	// dragable marker
									echo "<td class='do-not-wrap'>".$value[8]."</td>";	// Start Time
									echo "<td class='do-not-wrap'>".$value[9]."</td>";	// End Time
									echo "<td class='do-not-wrap'><input type='hidden' name='start_location' value='".$start_coordinates."'>".$start_location."</td>";	// StartLocation
									echo "<td class='do-not-wrap'><input type='hidden' name='end_location' value='".$end_coordinates."'>".$end_location."</td>";	// End Location
									echo "<td class='rightdata'>".$distance."</td>";	// Distance
									echo "<td class='rightdata'>".$duration."</td>";	// Duration
									echo "<td class='centerdata'>".$private_business."</td>";
								echo "</tr>";
							}
							echo "</table>";
						}else {
							echo "<table class='on-table on-table-center' style='min-width:1015px; height:25px;'>";
								echo "<tr><td colspan='100%' class='centerdata'><h5>- No GPS Logbook data found - </h5></td></tr>";
							echo "</table>";
						}
					?>
				<input id="save" name="save" method="post" type="hidden" value="0" />
                </form>
            </td>
        </tr>
        <tr>
            <td align="center">
                <br/>
            </td>
        </tr>
    </table>
<?php
    //  Print Footer
    print_footer();
?>