<?php
    include("_db.php");
    include("_dates.php");

    function nonActionedEvents($company_id)	{
        $fromDate                                                       = getDates(date("Y-m-d"), -1);

        //$testDays                                                       = 3;
        $testDays                                                       = q("SELECT notifyPeriod FROM Company WHERE id = '".$company_id."'");

        $employees                                                      = q("SELECT e.id, e.frstname, e.lstname, e.email FROM (Employee AS e INNER JOIN Company_Users ".
                                                                            "AS cu ON e.id = cu.user_id) WHERE cu.company_id = '$company_id' AND e.email != 'admin' ".
                                                                            "AND e.deleted = '0' AND e.demo_user = '0' AND e.company_id = '$company_id'");

        if (is_array($employees)) {
            foreach ($employees as $employee) {
                $mail                                                   = 0;

                $date_last_booked                                       = q("SELECT MAX(date) FROM TimeSheet WHERE user_id = '".$employee[0]."'");

                if ($date_last_booked != "")        {
                    if ((businessdays($date_last_booked, $fromDate) - totalPublicHolidays($date_last_booked, $fromDate)) > $testDays && !(checkLeave($date_last_booked, $fromDate, $company_id, $employee[0])))     {
                        $mail                                           = 1;
                    }
                }

                if ($employee[3] == "cvivier@ages-group.com" || $employee[3] == "amelia@ages-group.com" || $employee[3] == "ckriek@ages-group.com" || $employee[3] == "spotgieter@ages-group.com")
                    $mail = 0;

                if ($mail == 1)
                    echo ">>".$employee[1]." ".$employee[2]." [".$date_last_booked."]<<<br/>";
            }
        }

        return;
    }
?>
<html>
    <head>
        <title>
            On-Track - Script...
        </title>
    </head>
    <body>
        <h1 align="center">
            On-Track - Script...
        </h1>
        <?php
            $date = date("Y-m-d H:i:s");
            $user = "admin";

			nonActionedEvents(2);

            echo "<p align='center'>Script completed successfully...</p>";
        ?>
        <form action="index.php" method="post">
            <center><input type="submit" value="Return"/></center>
        </form>
    </body>
</html>
